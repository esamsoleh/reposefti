//                  SMART HELM 
//
//by PT. Pebangunan Perumahan - Reka Tekno Mekanika
#include <WiFi.h>
#include <WiFiClient.h>
#include <WebServer.h>
#include <WiFiAP.h>
#include <esp_wifi.h>

// Set these to your desired credentials.
const char *ssid = "rtmbdg2";
const char *password = "asdfghjkl";
//Ultrasonic
long duration1,duration2;
int distance1,distance2;
byte trig1=33;
byte echo1=25;
byte trig2=26;
byte echo2=27;
//Battery
int delayloop;
byte R=13,G=12,B=14;
int volt=36,led=4;
int read_sen1,read_sen2,read_volt;
//Data
String stat1elm="";
int stat1=0;
int stat0=0;



WiFiServer server(50000);
// Set your Static IP address
IPAddress local_IP(192, 168, 43, 83);
// Set your Gateway IP address
IPAddress gateway(192, 168, 43, 1);
IPAddress subnet(255, 255, 255, 0);
IPAddress primaryDNS(8, 8, 8, 8);   //optional
IPAddress secondaryDNS(8, 8, 4, 4); //optional

void setup() {
  pinMode(echo1,INPUT);
  pinMode(trig1,OUTPUT);
  pinMode(echo2,INPUT);
  pinMode(trig2,OUTPUT);
  pinMode(R,OUTPUT);
  pinMode(G,OUTPUT);
  pinMode(B,OUTPUT);
  pinMode(led,OUTPUT);
  pinMode(2,OUTPUT);
  pinMode(volt,INPUT);
  analogReadResolution(11);
  //analogSetAttenuation(ADC_6db);
  inisialisasiRGB();
  Serial.begin(115200);
  Serial.println();
  WiFi.mode(WIFI_STA);
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

if (!WiFi.config(local_IP, gateway, subnet, primaryDNS, secondaryDNS)) {
    Serial.println("STA Failed to configure");
  }
  
WiFi.begin(ssid, password);
while (WiFi.status() != WL_CONNECTED) {
  digitalWrite(2,HIGH);
  digitalWrite(led,HIGH);
  delay(300);
  Serial.print(".");
}
 digitalWrite(2,LOW);
 digitalWrite(led,LOW);

  Serial.println(WiFi.localIP());
  server.begin();
  Serial.println("Server started");
  delay(2000);
}

void loop() {
  while (WiFi.status() != WL_CONNECTED) {
  digitalWrite(2,HIGH);
  digitalWrite(led,HIGH);
  delay(200);
  Serial.print(".");
  }
  digitalWrite(2,LOW);
  digitalWrite(led,LOW);
  Serial.print(stat0);
  Serial.print("  ");
  Serial.println(stat1);

u1();
u2();
classs();
bacasens();
  
WiFiClient client = server.available();   // listen for incoming clients
 
  if (client) {                             // if you get a client,
    //Serial.println("New Client.");           // print a message out the serial port
    String currentLine = "";                // make a String to hold incoming data from the client
    while (client.connected()) {            // loop while the client's connected
      if (client.available()) {             // if there's bytes to read from the client,
        char c = client.read();             // read a byte, then
        Serial.write(c);                    // print it out the serial monitor

        
        if (c == 'p') {                    // if the byte is a newline character
         // Serial.println("karakter p terkirim DAN ");
          // Serial.println(delayloop);
          if (currentLine.length() == 0) {
           // u1();
           //u2(); 
          if(stat1>3){
            client.println("1");
            stat1=0;
            blink1();
           }
           else if(stat0>3){
            client.println("0");
            stat0=0;
            blink3();
            }
            Serial.print(stat0);
            Serial.print("  ");
            Serial.println(stat1);
 
            Serial.print("Distance: ");
            Serial.print(distance1);
            Serial.print("   ");
            Serial.println(distance2);

            
          
            // The HTTP response ends with another blank line:
            client.println();
            // break out of the while loop:
            break;
          } else {    // if you got a newline, then clear currentLine:
            currentLine = "";
          }
         
        } 

         else if (c == 's') {                    // if the byte is a newline character
          Serial.println("karakter s terkirim");
         //digitalWrite(2,HIGH);
         //delay(800);
         //digitalWrite(2,LOW);
         //delay(500);
         //digitalWrite(2,HIGH);
         //delay(100);
         //digitalWrite(2,LOW);
         
          // if the current line is blank, you got two newline characters in a row.
          // that's the end of the client HTTP request, so send a response:
          if (currentLine.length() == 0) {
            client.println("connected");
            // The HTTP response ends with another blank line:
          
            client.println();
            // break out of the while loop:
            break;
          } else {    // if you got a newline, then clear currentLine:
            currentLine = "";
          }
            //client.print("0"); //
        }
        
        else if (c != '\r') {  // if you got anything else but a carriage return character,
          currentLine += c;      // add it to the end of the currentLine
        digitalWrite(2,LOW);
        digitalWrite(led,LOW);
        }
      }
    }
    // close the connection:
    client.stop();
    Serial.println("Client Disconnected.");
  }
}


void classs(){
  if(distance1<=5 && distance2<=5) {
    //client.println("1");
    //Serial.println("1"); 
    stat1=stat1+1;
    //blink1();
  }

  else if(distance1<=5 && distance2>1000) {
    //client.println("1");
    //Serial.println("1");
    stat1=stat1+1;
    // blink1();
  }
  else if(distance1>1000 && distance2<=5) {
    //client.println("1");
    //Serial.println("1");
    stat1=stat1+1;
    //blink1();
  }
  else if(distance1>1000 && distance2>1000) {
    //client.println("1");
    //Serial.println("1");
    stat1=stat1+1;
    //blink1();
  }
  //setting dilepas terbukaterttutp
  else if(distance1>5 && distance1<=1000 && distance2>5 && distance2<=1000) {
    // client.println("0");
    //Serial.println("0");
    stat0=stat0+1;
    //blink3();
  }

  else if(distance1>5 && distance1<=1000 && distance2<1000) {
    //client.println("0");
    //Serial.println("0");
    stat0=stat0+1;
    // blink3();
  }
  else if(distance2>5 && distance2<=1000 && distance1<1000) {
    //client.println("0");
    //Serial.println("0");
    stat0=stat0+1;
    //blink3();
  }

/*

else if(distance1>5 && distance1<=1000 && distance2>1000) {
  client.println("0");
  //Serial.println("0");
  blink3();
}
else if(distance2>5 && distance2<=1000 && distance1>1000) {
  client.println("0");
  //Serial.println("0");
  blink3();
}
 */
  else { 
    //client.println("0");
    Serial.println("0");
    stat0=stat0+1;
    //blink3();
  }
}


void u1(){
  digitalWrite(trig1, LOW);
  delayMicroseconds(2);
  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(trig1, HIGH);
  delayMicroseconds(10);
  digitalWrite(trig1, LOW);
  // Reads the echoPin, returns the sound wave travel time in microseconds
  duration1 = pulseIn(echo1, HIGH);
  // Calculating the distance
  distance1= duration1*0.034/2;
}

void u2(){
  digitalWrite(trig2, LOW);
  delayMicroseconds(2);
  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(trig2, HIGH);
  delayMicroseconds(10);
  digitalWrite(trig2, LOW);
  // Reads the echoPin, returns the sound wave travel time in microseconds
  duration2 = pulseIn(echo2, HIGH);
  // Calculating the distance
  distance2= duration2*0.034/2;
}

void blink3(){
  digitalWrite(2,HIGH);
  digitalWrite(led,HIGH);
  delay(50);
  digitalWrite(2,LOW);
  digitalWrite(led,LOW);
  delay(50);
  digitalWrite(2,HIGH);
  digitalWrite(led,HIGH);
  delay(50);
  digitalWrite(2,LOW);
  digitalWrite(led,LOW);
  delay(50);
  digitalWrite(2,HIGH);
  digitalWrite(led,HIGH);
  delay(50);
  digitalWrite(2,LOW);
  digitalWrite(led,LOW);
}

void blink2(){
  digitalWrite(2,HIGH);
  delay(200);
  digitalWrite(2,LOW);
  delay(200);
  digitalWrite(2,HIGH);
  delay(200);
  digitalWrite(2,LOW);
  delay(200);
}

void blink1(){
  digitalWrite(2,HIGH);
  digitalWrite(led,HIGH);
  delay(200);
  digitalWrite(2,LOW);
  digitalWrite(led,LOW);
  //delay(200);
}


void inisialisasiRGB(){
  digitalWrite(R,255);
  digitalWrite(G,255);
  digitalWrite(B,255);
  delay(2000);
  digitalWrite(R,0);
  digitalWrite(G,255);
  digitalWrite(B,255);
  delay(2000);
  digitalWrite(R,255);
  digitalWrite(G,0);
  digitalWrite(B,255);
  delay(2000);
  digitalWrite(R,255);
  digitalWrite(G,255);
  digitalWrite(B,0);
  delay(2000);
  digitalWrite(R,0);
  digitalWrite(G,0);
  digitalWrite(B,0);
  delay(2000);
}

void R_ON(){
  digitalWrite(R,0);
  digitalWrite(G,255);
  digitalWrite(B,255);
}

void G_ON(){
  digitalWrite(R,255);
  digitalWrite(G,0);
  digitalWrite(B,255);
}
void B_ON(){
  digitalWrite(R,255);
  digitalWrite(G,255);
  digitalWrite(B,0);
}

void RGB_OFF(){
  digitalWrite(R,255);
  digitalWrite(G,255);
  digitalWrite(B,255);
}
          
void bacasens(){
    if(delayloop>4){
      read_volt=analogRead(volt);
      //Serial.print(read_volt);
      //Serial.print(" = ");
  
    if(read_volt>1000) {
      //Serial.println("4 bar");
      B_ON();
      //delay(300);
      //RGB_OFF(); 
    }
    else if(read_volt>980 && read_volt<=1000){
      //Serial.println("3 bar");
      G_ON();
      //delay(300);
      //RGB_OFF();
    }
    else if(read_volt>950 && read_volt<=980) {
      //Serial.println("2 bar");
      R_ON();
      //delay(100);
      //RGB_OFF();
    }
    else if(read_volt<=950) {
      //Serial.println("1 bar");
      R_ON();
      delay(50);
      RGB_OFF();
      delay(50);
      R_ON();
      delay(50);
      RGB_OFF();    
    }
    else {}
    delayloop=0;
  }
  delayloop=delayloop+1;
}

void bacasens_int(){
  delayloop=0;
  while(delayloop<10){
  read_volt=analogRead(volt);
  Serial.print(read_volt);
  Serial.print(" = ");
  
  if(read_volt>2300) {
    Serial.println("4 bar");
    B_ON();
    //delay(300);
    //RGB_OFF(); 
  }
  else if(read_volt>2190 && read_volt<=2300){
    Serial.println("3 bar");
    G_ON();
    //delay(300);
    //RGB_OFF();
  }
  else if(read_volt>2100 && read_volt<=2190) {
    Serial.println("2 bar");
    R_ON();
    //delay(100);
    //RGB_OFF();
  }
  else if(read_volt<=2100) {
    Serial.println("1 bar");
    R_ON();
    delay(100);
    RGB_OFF();
    delay(100);
    R_ON();
    delay(100);
    RGB_OFF();
  }
  delayloop=delayloop+1;
  }
}
//SAMSOLEH@GMAIL.COM
