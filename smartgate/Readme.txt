Program development menggunakan Arduino IDE, link bisa di download di :
https://www.arduino.cc/en/Main/software

Library yg digunakan :
- Esp 32 : 
tutorial : 
https://randomnerdtutorials.com/installing-the-esp32-board-in-arduino-ide-windows-instructions/
library :
https://github.com/espressif/arduino-esp32

- RTC : DS3231-1.0.2.zip
https://www.arduinolibraries.info/libraries/ds3231


Konfigurasi yang diubah :
replace file HardwareSerial.cpp di path :
C:\Users\[username]\\AppData\Local\Arduino15\packages\esp32\hardware\esp32\1.0.1\cores\esp32\HardwareSerial.cpp

folder AppData biasanya di hide, jadi harap show unhide file di explorer nya

