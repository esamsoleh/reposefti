#include <Arduino.h>

#include <WiFi.h>
#include <WiFiMulti.h>
#include <HTTPClient.h>

#include <stdio.h>
#include <stdlib.h>

#include <DS3231.h>
#include <Wire.h>

//===============================================
#define USE_SERIAL Serial
#define RXD2 16
#define TXD2 17

#define WIFI_ADDRESS "APPLE"
#define WIFI_PASSWORD "executive"
#define LINK_SERVER "https://ppas.rtmekanika.com/getpost/absen"
#define LINK_KALIBRASI "https://ppas.rtmekanika.com/getpost/waktu"
#define LINK_HEARTBEAT "https://ppas.rtmekanika.com/getpost/statusGate"

//indikator
#define LED_QRCODE_ERROR 1
#define LED_GSN_ERROR 2
#define LED_SDCARD_ERROR 3
#define LED_QRREAD_FAIL 4
#define LED_NO_CONNECTION 5
#define LED_READY 6
#define LED_QRCODE_READ 7
#define LED_SEND_DATA 8
#define BUZZER_PIN 12
#define BUZZER_AKTIF LOW
#define BUZZER_MATI HIGH

//pin led
#define LED_HIJAU 25
#define LED_MERAH 26
#define LED_BATRE_MERAH 32
#define LED_BATRE_HIJAU 33

//interval heartbeat satuan ms
#define INTERVAL_HEARTBEAT 60000

//=============================================================================
DS3231 rtc;
WiFiMulti wifiMulti;

//serial event
String inputString = "";
bool stringComplete = false;
String SMARTGATE_ID = "1A2B3C4D";
unsigned long time_now = 0;

//rtc
bool Century = false;
bool h12, PM;

//led
int ledCode = 0;
int ledTimer = 0;

int kedipBatre = 0;
int ms_heartbeat = 0;

//============================================================================
void serial_read() {
  char _bite;

  if (Serial2.available() > 0) {
    while (Serial2.available()) {
      char inChar = (char)Serial2.read();
      inputString += inChar;
      if (inChar == '$') {
        stringComplete = true;
      }
    }
  }
}

void catat(String text, boolean save = false)
{
  USE_SERIAL.println(text);
}

void kirimUlang(String id, String nip, String tanggal)
{
  if (nip != "" && tanggal != "")
  {
    HTTPClient http;
    http.begin(LINK_SERVER);
    http.addHeader("Content-Type", "application/json");
    String datana = "{\"serial_number\": \"" + SMARTGATE_ID + "\", \"nip\": " + nip + ", \"tanggal\": \"" + tanggal + "\"}";
    int httpCode = http.POST(datana);
    catat("repost " + datana, true);
    if (httpCode > 0) {
      if (httpCode > 199 && httpCode < 300) {
        USE_SERIAL.printf("[HTTP] RE POST... code: %d ", httpCode);
        ledCode = LED_SEND_DATA;
        nyalakanLed();
      }
      String payload = http.getString();
      USE_SERIAL.println(payload);
    } else {
      ledCode = LED_QRREAD_FAIL;
      nyalakanLed();
      USE_SERIAL.printf("[HTTP] POST... failed, error: %s\n", http.errorToString(httpCode).c_str());
    }
    http.end();
  }
}

void buzz()
{
  for (int i = 0; i < 3; i++)
  {
    digitalWrite(BUZZER_PIN, BUZZER_AKTIF);
    pause(100);
    digitalWrite(BUZZER_PIN, BUZZER_MATI);
    pause(100);
  }
}

void kirimdata()
{
  String nip, tanggal;
  USE_SERIAL.println(inputString);
  int idxtempAwal = inputString.indexOf('@');
  int idxtempAkhir = inputString.indexOf('#');
  nip = inputString.substring(idxtempAwal + 1, idxtempAkhir);
  tanggal = inputString.substring(idxtempAkhir + 1, inputString.indexOf('$'));
  USE_SERIAL.println("NIP: " + nip);
  USE_SERIAL.println("tgl: " + tanggal);

  if (nip != "" && tanggal != "")
  {
    buzz();
    HTTPClient http;
    http.begin(LINK_SERVER); // ?data="+cSMARTGATE_ID+inputString); //HTTP

    tanggal = getWaktu();
    http.addHeader("Content-Type", "application/json");
    String datana = "{\"serial_number\": \"" + SMARTGATE_ID + "\", \"nip\": " + nip + ", \"tanggal\": \"" + tanggal + "\"}"; //{"serial_number":"sdfzv", "nip":"dskj", "tanggal":"sdfs"}
    String query = "insert into absen (nip,waktu,qrcode,terkirim) values ('" + nip + "','" + tanggal + "','" + inputString + "',0)";
    catat(datana, true);

    int httpCode = http.POST(datana);
    if (httpCode > 0) {
      USE_SERIAL.printf("[HTTP] POST... code: %d ", httpCode);

      if (httpCode > 199 && httpCode < 300) {
        query = "insert into absen (nip,waktu,qrcode,terkirim) values ('" + nip + "','" + tanggal + "','" + inputString + "',1)";
        ledCode = LED_SEND_DATA;
        nyalakanLed();
      }
      String payload = http.getString();
      catat("respon: " + payload, true);
    } else {
      ledCode = LED_QRREAD_FAIL;
      nyalakanLed();
      USE_SERIAL.printf("[HTTP] POST... failed, error: %s\n", http.errorToString(httpCode).c_str());
    }

    http.end();
  }
}

void kalibrasiWaktu()
{
  if ((wifiMulti.run() == WL_CONNECTED)) {
    HTTPClient http;
    http.begin(LINK_KALIBRASI);
    int httpCode = http.GET();

    if (httpCode == HTTP_CODE_OK)
    {
      String payload = http.getString();

      String jam = payload.substring(28, 30);
      String menit = payload.substring(31, 33);
      String detik = payload.substring(34, 36);

      String tgl = payload.substring(25, 27);
      String bln = payload.substring(22, 24);
      String thn = payload.substring(19, 21);

      //YYMMDDwHHMMSSx
      String Temp = thn + bln + tgl + "0" + jam + menit + detik;

      setWaktu(Temp);
    }
    http.end();
  }
}

void setWaktu(String InString)
{
  byte Temp1, Temp2;

  rtc.setClockMode(false);
  // Read Year first
  Temp1 = (byte)InString[0] - 48;
  Temp2 = (byte)InString[1] - 48;
  rtc.setYear(Temp1 * 10 + Temp2);

  // now month
  Temp1 = (byte)InString[2] - 48;
  Temp2 = (byte)InString[3] - 48;
  rtc.setMonth(Temp1 * 10 + Temp2);

  // now date
  Temp1 = (byte)InString[4] - 48;
  Temp2 = (byte)InString[5] - 48;
  rtc.setDate(Temp1 * 10 + Temp2);

  // now Hour
  Temp1 = (byte)InString[7] - 48;
  Temp2 = (byte)InString[8] - 48;
  rtc.setHour(Temp1 * 10 + Temp2);

  // now Minute
  Temp1 = (byte)InString[9] - 48;
  Temp2 = (byte)InString[10] - 48;
  rtc.setMinute(Temp1 * 10 + Temp2);

  // now Second
  Temp1 = (byte)InString[11] - 48;
  Temp2 = (byte)InString[12] - 48;
  rtc.setSecond(Temp1 * 10 + Temp2);
}

String getWaktu()
{
  char chArray[20];

  //YYYY-MM-DD hh:mm:ss
  sprintf(chArray,
          "20%02d-%02d-%02d %02d:%02d:%02d",
          rtc.getYear(),
          rtc.getMonth(Century),
          rtc.getDate(),
          rtc.getHour(h12, PM),
          rtc.getMinute(),
          rtc.getSecond()
         );
  String waktu(chArray);

  return waktu;
}

int batteryValue = 0;
void cekBatre()
{
  batteryValue = analogRead(39);
  USE_SERIAL.print("battery: ");
  USE_SERIAL.println(batteryValue);
  if (batteryValue > 3030) //3.7 nyala ijo
  {
    digitalWrite(LED_BATRE_HIJAU, LOW);
    digitalWrite(LED_BATRE_MERAH, HIGH);
  }
  else if (batteryValue > 2866) //3.5 nyala ijo, merah
  {
    digitalWrite(LED_BATRE_HIJAU, LOW);
    digitalWrite(LED_BATRE_MERAH, LOW);
  }
  else if (batteryValue > 2661) //3.25 ijo mati, merah nyala
  {
    digitalWrite(LED_BATRE_HIJAU, HIGH);
    digitalWrite(LED_BATRE_MERAH, LOW);
  }
  else
  {
    digitalWrite(LED_BATRE_HIJAU, HIGH);
    digitalWrite(LED_BATRE_MERAH, LOW);
  }
}

void setup() {
  Wire.begin();
  USE_SERIAL.begin(115200);
  Serial2.begin(115200, SERIAL_8N1, RXD2, TXD2);
  pinMode(LED_HIJAU, OUTPUT);
  pinMode(LED_MERAH, OUTPUT);
  pinMode(BUZZER_PIN, OUTPUT);
  pinMode(LED_BATRE_HIJAU, OUTPUT);
  pinMode(LED_BATRE_MERAH, OUTPUT);
  digitalWrite(BUZZER_PIN, BUZZER_MATI);
  buzz();

  USE_SERIAL.println();
  USE_SERIAL.println();
  USE_SERIAL.println();

  for (uint8_t t = 4; t > 0; t--) {
    USE_SERIAL.printf("[SETUP] WAIT %d...\n", t);
    USE_SERIAL.flush();
    delay(1000);
  }
  //===================

  USE_SERIAL.println("Serial Started, connecting to wifi");
  wifiMulti.addAP(WIFI_ADDRESS, WIFI_PASSWORD);
  USE_SERIAL.println("addAP");
  inputString.reserve(200);
  if ((wifiMulti.run() == WL_CONNECTED)) {
    USE_SERIAL.println("Connected");
    kalibrasiWaktu();

    ledCode = LED_READY;
    nyalakanLed();
  }
}

void nyalakanLed()
{
  switch (ledCode)
  {
    case 1: //qrcode error
      digitalWrite(LED_HIJAU, 0);
      digitalWrite(LED_MERAH, 1);
      while (true) {} //endless loop
      break;
    case 2: //gsm_error
      digitalWrite(LED_HIJAU, 0);
      while (true) { //endless loop
        digitalWrite(LED_MERAH, 1);
        delay(500);
        digitalWrite(LED_MERAH, 0);
        delay(500);
      }
      break;
    case 3: //sdcard_error
      digitalWrite(LED_HIJAU, 0);
      while (true) { //endless loop
        digitalWrite(LED_MERAH, 1);
        delay(2000);
        digitalWrite(LED_MERAH, 0);
        delay(2000);
      }
      break;
    case 4: //qrread_fail
      digitalWrite(LED_HIJAU, 0);
      for (int i = 0; i < 3; i++) {
        digitalWrite(LED_MERAH, 1);
        delay(300);
        digitalWrite(LED_MERAH, 0);
        delay(300);
      }
      ledCode = 6;
      break;
    case 5: //no_connection
      digitalWrite(LED_HIJAU, 1);
      if (millis() - ledTimer < 1000) {
        digitalWrite(LED_MERAH, 1);
      }
      else if (millis() - ledTimer < 2000) digitalWrite(LED_MERAH, 0);
      else ledTimer = millis();
      break;

    case 6: //ready
      digitalWrite(LED_HIJAU, 1);
      digitalWrite(LED_MERAH, 0);
      break;
    case 7: //qrcode_read
      digitalWrite(LED_MERAH, 0);
      for (int i = 0; i < 2; i++) {
        digitalWrite(LED_HIJAU, 0);
        delay(200);
        digitalWrite(LED_HIJAU, 1);
        delay(200);
      }
      ledCode = 6;
      break;
    case 8: //send_data
      digitalWrite(LED_MERAH, 0);
      for (int i = 0; i < 3; i++) {
        digitalWrite(LED_HIJAU, 0);
        delay(50);
        digitalWrite(LED_HIJAU, 1);
        delay(50);
      }
      ledCode = 6;
      break;
    default:
      break;
  }
}

void pause(int milisecond)
{
  int wkt = millis();
  while (millis() < wkt + milisecond) {}
}

void heartBeat()
{
  HTTPClient http;
  http.begin(LINK_HEARTBEAT);
  http.addHeader("Content-Type", "application/json");
  String datana = "{\"serial_number\": \"" + SMARTGATE_ID + "\"}"; //{"serial_number":"sdfzv"}

  catat(datana, true);
  int httpCode = http.POST(datana);
  if (httpCode > 0) {
    USE_SERIAL.printf("[HTTP] POST... code: %d ", httpCode);

    if (httpCode > 199 && httpCode < 300) {
      ledCode = LED_SEND_DATA;
      nyalakanLed();
    }
    String payload = http.getString();
    catat("respon: " + payload, true);
  } else {
    ledCode = LED_QRREAD_FAIL;
    nyalakanLed();
    USE_SERIAL.printf("[HTTP] POST... failed, error: %s\n", http.errorToString(httpCode).c_str());
  }
  http.end();
  ms_heartbeat = millis();
}

void loop() {
  cekBatre();
  // wait for WiFi connection
  if ((wifiMulti.run() == WL_CONNECTED)) {
    USE_SERIAL.println("Connected");
    ledCode = LED_READY;
    nyalakanLed();
  }
  else
  {
    USE_SERIAL.println("Connecting...");
    ledTimer = millis();
    ledCode = LED_NO_CONNECTION;
    nyalakanLed();
  }

  time_now = millis();
  while (millis() < time_now + 5000)
  {
    nyalakanLed(); //looping led
    if (stringComplete)
    {
      ledCode = LED_QRCODE_READ;
      nyalakanLed();
      kirimdata();

      // clear the string:
      inputString = "";
      stringComplete = false;
    }
    serial_read();
    if (millis() > ms_heartbeat + INTERVAL_HEARTBEAT)
    {
      heartBeat();
    }
  }
}
