-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.25-0ubuntu0.18.04.2 - (Ubuntu)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for sefti
DROP DATABASE IF EXISTS `sefti`;
CREATE DATABASE IF NOT EXISTS `sefti` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `sefti`;

-- Dumping structure for table sefti.absensi_log
DROP TABLE IF EXISTS `absensi_log`;
CREATE TABLE IF NOT EXISTS `absensi_log` (
  `id_log` int(11) NOT NULL AUTO_INCREMENT,
  `serial_number` varchar(50) DEFAULT '0',
  `pin` int(11) DEFAULT NULL,
  `nip` varchar(18) NOT NULL DEFAULT '',
  `tanggal` datetime DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `waktu_masuk` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_log`) USING BTREE,
  KEY `nip` (`nip`),
  KEY `tanggal` (`tanggal`),
  KEY `pin` (`pin`),
  KEY `serial_number` (`serial_number`) USING BTREE,
  CONSTRAINT `FK_absensi_log_d_pegawai` FOREIGN KEY (`nip`) REFERENCES `d_pegawai` (`nip`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table sefti.absensi_log: ~4 rows (approximately)
DELETE FROM `absensi_log`;
/*!40000 ALTER TABLE `absensi_log` DISABLE KEYS */;
INSERT INTO `absensi_log` (`id_log`, `serial_number`, `pin`, `nip`, `tanggal`, `status`, `waktu_masuk`) VALUES
	(1, '1234', NULL, '1234', '2019-04-12 11:31:03', NULL, '2019-04-12 11:32:23'),
	(2, '1234', NULL, '1234', '2019-04-12 13:03:08', NULL, '2019-04-12 14:18:56'),
	(4, '1234', NULL, '1234', '2019-04-12 14:30:00', NULL, '2019-04-12 14:33:23'),
	(5, '1234', NULL, '1234', '2019-04-12 14:40:03', NULL, '2019-04-12 14:42:28');
/*!40000 ALTER TABLE `absensi_log` ENABLE KEYS */;

-- Dumping structure for table sefti.d_absen_harian
DROP TABLE IF EXISTS `d_absen_harian`;
CREATE TABLE IF NOT EXISTS `d_absen_harian` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(18) NOT NULL DEFAULT '0',
  `tanggal` int(11) DEFAULT NULL,
  `bulan` int(11) DEFAULT NULL,
  `tahun` int(11) DEFAULT NULL,
  `jadwal_masuk` datetime DEFAULT NULL,
  `realisasi_masuk` datetime DEFAULT NULL,
  `keterlambatan_masuk` float NOT NULL DEFAULT '0',
  `potongan_terlambat_masuk` float NOT NULL DEFAULT '0',
  `jadwal_keluar` datetime DEFAULT NULL,
  `realisasi_keluar` datetime DEFAULT NULL,
  `kecepatan_keluar` float NOT NULL DEFAULT '0',
  `potongan_pulang_cepat` float NOT NULL DEFAULT '0',
  `jenis_tidak_masuk` int(11) NOT NULL DEFAULT '0',
  `potongan_tidak_masuk` float NOT NULL DEFAULT '0',
  `libur` int(11) NOT NULL DEFAULT '0',
  `persentase_potongan` float NOT NULL DEFAULT '0',
  `update_date` datetime DEFAULT NULL,
  `update_by` varchar(200) NOT NULL DEFAULT '0',
  `added_by` varchar(50) DEFAULT NULL,
  `added_date` datetime DEFAULT NULL,
  `keterangan` text NOT NULL,
  `edited` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `jenis_tidak_masuk` (`jenis_tidak_masuk`),
  KEY `nip` (`nip`),
  KEY `tanggal` (`tanggal`),
  KEY `bulan` (`bulan`),
  KEY `tahun` (`tahun`),
  CONSTRAINT `FK_d_absen_harian_d_pegawai` FOREIGN KEY (`nip`) REFERENCES `d_pegawai` (`nip`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table sefti.d_absen_harian: ~0 rows (approximately)
DELETE FROM `d_absen_harian`;
/*!40000 ALTER TABLE `d_absen_harian` DISABLE KEYS */;
/*!40000 ALTER TABLE `d_absen_harian` ENABLE KEYS */;

-- Dumping structure for table sefti.d_hari_libur
DROP TABLE IF EXISTS `d_hari_libur`;
CREATE TABLE IF NOT EXISTS `d_hari_libur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` date DEFAULT NULL,
  `keterangan` text,
  `added_date` datetime DEFAULT NULL,
  `added_by` varchar(50) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `update_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table sefti.d_hari_libur: ~12 rows (approximately)
DELETE FROM `d_hari_libur`;
/*!40000 ALTER TABLE `d_hari_libur` DISABLE KEYS */;
INSERT INTO `d_hari_libur` (`id`, `tanggal`, `keterangan`, `added_date`, `added_by`, `update_date`, `update_by`) VALUES
	(1, '2017-12-25', 'Libur 25 Desember', '2017-12-20 10:53:30', '1', '2017-12-20 10:54:18', '1'),
	(2, '2017-12-26', 'Cuti Bersama dari 25 Desember 2017', '2017-12-20 10:54:06', '1', NULL, NULL),
	(3, '2018-01-01', 'Tahun Baru', '2018-01-03 16:51:15', '1', '2018-01-19 09:52:40', '1'),
	(4, '2018-02-16', 'Tahun Baru Imlek 2569 kongzili', '2018-01-29 08:33:51', '58', NULL, NULL),
	(5, '2018-03-17', 'Hari Raya Nyepi Tahun Baru Saka 1940', '2018-01-29 08:34:36', '58', NULL, NULL),
	(6, '2018-03-30', 'Wafat Isa Almasih', '2018-01-29 08:34:59', '58', NULL, NULL),
	(7, '2018-05-01', 'Harti Buruh Internasional', '2018-03-12 10:26:30', '58', NULL, NULL),
	(8, '2018-05-01', 'Hari Buruh Internasional ( MAYDAY )', '2018-03-12 10:31:27', '58', NULL, NULL),
	(9, '2018-05-10', 'Kenaikan Isa Almasih', '2018-03-12 10:36:30', '58', NULL, NULL),
	(10, '2018-05-29', 'Hari Raya Waisak Tahun 2562', '2018-03-12 10:37:15', '58', '2018-03-12 10:43:23', '58'),
	(15, '2019-01-01', 'tahun baru', '2019-01-02 14:00:32', '1', '2019-01-02 14:02:12', '1'),
	(16, '2019-03-01', 'isra mi\'raj nabi muhammad', '2019-01-02 14:05:00', '1', '2019-01-02 14:05:09', '1');
/*!40000 ALTER TABLE `d_hari_libur` ENABLE KEYS */;

-- Dumping structure for table sefti.d_jadwal_lembur
DROP TABLE IF EXISTS `d_jadwal_lembur`;
CREATE TABLE IF NOT EXISTS `d_jadwal_lembur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(18) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `is_libur` tinyint(3) NOT NULL DEFAULT '0',
  `jadwal_masuk` datetime DEFAULT NULL,
  `jadwal_keluar` datetime DEFAULT NULL,
  `keterangan` text,
  `added_date` datetime DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `nip` (`nip`),
  CONSTRAINT `FK_d_jadwal_lembur_d_pegawai` FOREIGN KEY (`nip`) REFERENCES `d_pegawai` (`nip`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=398156 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table sefti.d_jadwal_lembur: ~61 rows (approximately)
DELETE FROM `d_jadwal_lembur`;
/*!40000 ALTER TABLE `d_jadwal_lembur` DISABLE KEYS */;
INSERT INTO `d_jadwal_lembur` (`id`, `nip`, `tanggal`, `is_libur`, `jadwal_masuk`, `jadwal_keluar`, `keterangan`, `added_date`, `added_by`, `update_date`, `update_by`) VALUES
	(398095, '1234', '2019-03-01', 0, '2019-03-01 07:00:00', '2019-03-01 14:25:00', '', '2019-03-30 14:18:23', 1, '2019-03-30 14:23:40', 1),
	(398096, '1234', '2019-03-02', 0, NULL, NULL, NULL, '2019-03-30 14:18:23', 1, NULL, NULL),
	(398097, '1234', '2019-03-03', 0, NULL, NULL, NULL, '2019-03-30 14:18:23', 1, NULL, NULL),
	(398098, '1234', '2019-03-04', 0, NULL, NULL, NULL, '2019-03-30 14:18:23', 1, NULL, NULL),
	(398099, '1234', '2019-03-05', 0, NULL, NULL, NULL, '2019-03-30 14:18:23', 1, NULL, NULL),
	(398100, '1234', '2019-03-06', 0, NULL, NULL, NULL, '2019-03-30 14:18:23', 1, NULL, NULL),
	(398101, '1234', '2019-03-07', 0, NULL, NULL, NULL, '2019-03-30 14:18:23', 1, NULL, NULL),
	(398102, '1234', '2019-03-08', 0, NULL, NULL, NULL, '2019-03-30 14:18:23', 1, NULL, NULL),
	(398103, '1234', '2019-03-09', 0, NULL, NULL, NULL, '2019-03-30 14:18:23', 1, NULL, NULL),
	(398104, '1234', '2019-03-10', 0, NULL, NULL, NULL, '2019-03-30 14:18:23', 1, NULL, NULL),
	(398105, '1234', '2019-03-11', 0, NULL, NULL, NULL, '2019-03-30 14:18:23', 1, NULL, NULL),
	(398106, '1234', '2019-03-12', 0, NULL, NULL, NULL, '2019-03-30 14:18:23', 1, NULL, NULL),
	(398107, '1234', '2019-03-13', 0, NULL, NULL, NULL, '2019-03-30 14:18:23', 1, NULL, NULL),
	(398108, '1234', '2019-03-14', 0, NULL, NULL, NULL, '2019-03-30 14:18:23', 1, NULL, NULL),
	(398109, '1234', '2019-03-15', 0, NULL, NULL, NULL, '2019-03-30 14:18:24', 1, NULL, NULL),
	(398110, '1234', '2019-03-16', 0, NULL, NULL, NULL, '2019-03-30 14:18:24', 1, NULL, NULL),
	(398111, '1234', '2019-03-17', 0, NULL, NULL, NULL, '2019-03-30 14:18:24', 1, NULL, NULL),
	(398112, '1234', '2019-03-18', 0, NULL, NULL, NULL, '2019-03-30 14:18:24', 1, NULL, NULL),
	(398113, '1234', '2019-03-19', 0, NULL, NULL, NULL, '2019-03-30 14:18:24', 1, NULL, NULL),
	(398114, '1234', '2019-03-20', 0, NULL, NULL, NULL, '2019-03-30 14:18:24', 1, NULL, NULL),
	(398115, '1234', '2019-03-21', 0, NULL, NULL, NULL, '2019-03-30 14:18:24', 1, NULL, NULL),
	(398116, '1234', '2019-03-22', 0, NULL, NULL, NULL, '2019-03-30 14:18:24', 1, NULL, NULL),
	(398117, '1234', '2019-03-23', 0, NULL, NULL, NULL, '2019-03-30 14:18:24', 1, NULL, NULL),
	(398118, '1234', '2019-03-24', 0, NULL, NULL, NULL, '2019-03-30 14:18:24', 1, NULL, NULL),
	(398119, '1234', '2019-03-25', 0, NULL, NULL, NULL, '2019-03-30 14:18:24', 1, NULL, NULL),
	(398120, '1234', '2019-03-26', 0, NULL, NULL, NULL, '2019-03-30 14:18:24', 1, NULL, NULL),
	(398121, '1234', '2019-03-27', 0, NULL, NULL, NULL, '2019-03-30 14:18:24', 1, NULL, NULL),
	(398122, '1234', '2019-03-28', 0, NULL, NULL, NULL, '2019-03-30 14:18:24', 1, NULL, NULL),
	(398123, '1234', '2019-03-29', 0, NULL, NULL, NULL, '2019-03-30 14:18:24', 1, NULL, NULL),
	(398124, '1234', '2019-03-30', 0, NULL, NULL, NULL, '2019-03-30 14:18:24', 1, NULL, NULL),
	(398125, '1234', '2019-03-31', 0, NULL, NULL, NULL, '2019-03-30 14:18:24', 1, NULL, NULL),
	(398126, '1234', '2019-04-01', 0, NULL, NULL, NULL, '2019-04-06 17:35:53', 1, NULL, NULL),
	(398127, '1234', '2019-04-02', 0, NULL, NULL, NULL, '2019-04-06 17:35:53', 1, NULL, NULL),
	(398128, '1234', '2019-04-03', 0, NULL, NULL, NULL, '2019-04-06 17:35:53', 1, NULL, NULL),
	(398129, '1234', '2019-04-04', 0, NULL, NULL, NULL, '2019-04-06 17:35:54', 1, NULL, NULL),
	(398130, '1234', '2019-04-05', 0, NULL, NULL, NULL, '2019-04-06 17:35:54', 1, NULL, NULL),
	(398131, '1234', '2019-04-06', 0, NULL, NULL, NULL, '2019-04-06 17:35:54', 1, NULL, NULL),
	(398132, '1234', '2019-04-07', 0, NULL, NULL, NULL, '2019-04-06 17:35:54', 1, NULL, NULL),
	(398133, '1234', '2019-04-08', 0, NULL, NULL, NULL, '2019-04-06 17:35:54', 1, NULL, NULL),
	(398134, '1234', '2019-04-09', 0, NULL, NULL, NULL, '2019-04-06 17:35:54', 1, NULL, NULL),
	(398135, '1234', '2019-04-10', 0, NULL, NULL, NULL, '2019-04-06 17:35:54', 1, NULL, NULL),
	(398136, '1234', '2019-04-11', 0, NULL, NULL, NULL, '2019-04-06 17:35:54', 1, NULL, NULL),
	(398137, '1234', '2019-04-12', 0, NULL, NULL, NULL, '2019-04-06 17:35:54', 1, NULL, NULL),
	(398138, '1234', '2019-04-13', 0, NULL, NULL, NULL, '2019-04-06 17:35:54', 1, NULL, NULL),
	(398139, '1234', '2019-04-14', 0, NULL, NULL, NULL, '2019-04-06 17:35:54', 1, NULL, NULL),
	(398140, '1234', '2019-04-15', 0, NULL, NULL, NULL, '2019-04-06 17:35:54', 1, NULL, NULL),
	(398141, '1234', '2019-04-16', 0, NULL, NULL, NULL, '2019-04-06 17:35:54', 1, NULL, NULL),
	(398142, '1234', '2019-04-17', 0, NULL, NULL, NULL, '2019-04-06 17:35:54', 1, NULL, NULL),
	(398143, '1234', '2019-04-18', 0, NULL, NULL, NULL, '2019-04-06 17:35:54', 1, NULL, NULL),
	(398144, '1234', '2019-04-19', 0, NULL, NULL, NULL, '2019-04-06 17:35:55', 1, NULL, NULL),
	(398145, '1234', '2019-04-20', 0, NULL, NULL, NULL, '2019-04-06 17:35:55', 1, NULL, NULL),
	(398146, '1234', '2019-04-21', 0, NULL, NULL, NULL, '2019-04-06 17:35:55', 1, NULL, NULL),
	(398147, '1234', '2019-04-22', 0, NULL, NULL, NULL, '2019-04-06 17:35:55', 1, NULL, NULL),
	(398148, '1234', '2019-04-23', 0, NULL, NULL, NULL, '2019-04-06 17:35:55', 1, NULL, NULL),
	(398149, '1234', '2019-04-24', 0, NULL, NULL, NULL, '2019-04-06 17:35:55', 1, NULL, NULL),
	(398150, '1234', '2019-04-25', 0, NULL, NULL, NULL, '2019-04-06 17:35:55', 1, NULL, NULL),
	(398151, '1234', '2019-04-26', 0, NULL, NULL, NULL, '2019-04-06 17:35:55', 1, NULL, NULL),
	(398152, '1234', '2019-04-27', 0, NULL, NULL, NULL, '2019-04-06 17:35:55', 1, NULL, NULL),
	(398153, '1234', '2019-04-28', 0, NULL, NULL, NULL, '2019-04-06 17:35:55', 1, NULL, NULL),
	(398154, '1234', '2019-04-29', 0, NULL, NULL, NULL, '2019-04-06 17:35:55', 1, NULL, NULL),
	(398155, '1234', '2019-04-30', 0, NULL, NULL, NULL, '2019-04-06 17:35:55', 1, NULL, NULL);
/*!40000 ALTER TABLE `d_jadwal_lembur` ENABLE KEYS */;

-- Dumping structure for table sefti.d_mesin_absensi
DROP TABLE IF EXISTS `d_mesin_absensi`;
CREATE TABLE IF NOT EXISTS `d_mesin_absensi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_project` int(11) NOT NULL DEFAULT '0',
  `ip` varchar(50) NOT NULL DEFAULT '0',
  `serial_number` varchar(50) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_d_mesin_absensi_d_project` (`id_project`),
  CONSTRAINT `FK_d_mesin_absensi_d_project` FOREIGN KEY (`id_project`) REFERENCES `d_project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table sefti.d_mesin_absensi: ~0 rows (approximately)
DELETE FROM `d_mesin_absensi`;
/*!40000 ALTER TABLE `d_mesin_absensi` DISABLE KEYS */;
INSERT INTO `d_mesin_absensi` (`id`, `id_project`, `ip`, `serial_number`, `status`, `updated`) VALUES
	(2, 1, '192.168.1.7', '1234', 1, '2019-03-30 11:36:22');
/*!40000 ALTER TABLE `d_mesin_absensi` ENABLE KEYS */;

-- Dumping structure for table sefti.d_mesin_absensi_user
DROP TABLE IF EXISTS `d_mesin_absensi_user`;
CREATE TABLE IF NOT EXISTS `d_mesin_absensi_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_mesin` int(11) NOT NULL DEFAULT '0',
  `nip` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_d_mesin_absensi_user_d_mesin_absensi` (`id_mesin`),
  CONSTRAINT `FK_d_mesin_absensi_user_d_mesin_absensi` FOREIGN KEY (`id_mesin`) REFERENCES `d_mesin_absensi` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table sefti.d_mesin_absensi_user: ~0 rows (approximately)
DELETE FROM `d_mesin_absensi_user`;
/*!40000 ALTER TABLE `d_mesin_absensi_user` DISABLE KEYS */;
INSERT INTO `d_mesin_absensi_user` (`id`, `id_mesin`, `nip`) VALUES
	(2, 2, '1234');
/*!40000 ALTER TABLE `d_mesin_absensi_user` ENABLE KEYS */;

-- Dumping structure for table sefti.d_pegawai
DROP TABLE IF EXISTS `d_pegawai`;
CREATE TABLE IF NOT EXISTS `d_pegawai` (
  `nip` varchar(50) NOT NULL,
  `nama` varchar(200) DEFAULT NULL,
  `foto` varchar(200) DEFAULT NULL,
  `id_jabatan` int(11) DEFAULT NULL,
  `non_aktif` int(11) DEFAULT '0',
  `added_date` datetime DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`nip`),
  KEY `FK_d_pegawai_ref_jabatan` (`id_jabatan`),
  CONSTRAINT `FK_d_pegawai_ref_jabatan` FOREIGN KEY (`id_jabatan`) REFERENCES `ref_jabatan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table sefti.d_pegawai: ~0 rows (approximately)
DELETE FROM `d_pegawai`;
/*!40000 ALTER TABLE `d_pegawai` DISABLE KEYS */;
INSERT INTO `d_pegawai` (`nip`, `nama`, `foto`, `id_jabatan`, `non_aktif`, `added_date`, `added_by`, `update_date`, `update_by`) VALUES
	('1234', 'eddy', '1234.png', 3, 0, '2019-03-27 08:26:52', 1, '2019-03-30 14:13:24', 1);
/*!40000 ALTER TABLE `d_pegawai` ENABLE KEYS */;

-- Dumping structure for table sefti.d_pegawai_tipe_jadwal
DROP TABLE IF EXISTS `d_pegawai_tipe_jadwal`;
CREATE TABLE IF NOT EXISTS `d_pegawai_tipe_jadwal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(18) DEFAULT NULL,
  `tipe_jadwal` int(11) DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL,
  `added_date` datetime DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nip_2` (`nip`),
  KEY `FK__referensi_tipe_jadwal` (`tipe_jadwal`),
  KEY `nip` (`nip`),
  CONSTRAINT `FK_d_pegawai_tipe_jadwal_d_pegawai` FOREIGN KEY (`nip`) REFERENCES `d_pegawai` (`nip`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_d_pegawai_tipe_jadwal_ref_tipe_jadwal` FOREIGN KEY (`tipe_jadwal`) REFERENCES `ref_tipe_jadwal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11564 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table sefti.d_pegawai_tipe_jadwal: ~0 rows (approximately)
DELETE FROM `d_pegawai_tipe_jadwal`;
/*!40000 ALTER TABLE `d_pegawai_tipe_jadwal` DISABLE KEYS */;
INSERT INTO `d_pegawai_tipe_jadwal` (`id`, `nip`, `tipe_jadwal`, `added_by`, `added_date`, `update_by`, `update_date`) VALUES
	(11563, '1234', 2, 1, '2019-03-30 14:13:24', 1, '2019-03-30 14:15:15');
/*!40000 ALTER TABLE `d_pegawai_tipe_jadwal` ENABLE KEYS */;

-- Dumping structure for table sefti.d_project
DROP TABLE IF EXISTS `d_project`;
CREATE TABLE IF NOT EXISTS `d_project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `lokasi_x` double NOT NULL DEFAULT '0',
  `lokasi_y` double NOT NULL DEFAULT '0',
  `added_date` datetime DEFAULT NULL,
  `added_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime DEFAULT NULL,
  `update_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table sefti.d_project: ~0 rows (approximately)
DELETE FROM `d_project`;
/*!40000 ALTER TABLE `d_project` DISABLE KEYS */;
INSERT INTO `d_project` (`id`, `nama`, `lokasi_x`, `lokasi_y`, `added_date`, `added_by`, `update_date`, `update_by`) VALUES
	(1, 'proyek pembangunan gedung kota bekasi', -6.238009538226919, 106.97869540003467, '2019-03-26 16:57:19', 1, '2019-03-26 21:05:14', 1);
/*!40000 ALTER TABLE `d_project` ENABLE KEYS */;

-- Dumping structure for table sefti.d_site
DROP TABLE IF EXISTS `d_site`;
CREATE TABLE IF NOT EXISTS `d_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_project` int(11) NOT NULL DEFAULT '0',
  `id_place` varchar(50) NOT NULL DEFAULT '0',
  `tingkat` int(11) NOT NULL DEFAULT '0',
  `tinggi` double DEFAULT NULL,
  `added_date` datetime DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_d_site_d_project` (`id_project`),
  CONSTRAINT `FK_d_site_d_project` FOREIGN KEY (`id_project`) REFERENCES `d_project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table sefti.d_site: ~1 rows (approximately)
DELETE FROM `d_site`;
/*!40000 ALTER TABLE `d_site` DISABLE KEYS */;
INSERT INTO `d_site` (`id`, `id_project`, `id_place`, `tingkat`, `tinggi`, `added_date`, `added_by`, `update_date`, `update_by`) VALUES
	(2, 1, '0', 1, 0, '2019-03-26 23:46:38', 1, NULL, NULL);
/*!40000 ALTER TABLE `d_site` ENABLE KEYS */;

-- Dumping structure for table sefti.ref_jabatan
DROP TABLE IF EXISTS `ref_jabatan`;
CREATE TABLE IF NOT EXISTS `ref_jabatan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table sefti.ref_jabatan: ~2 rows (approximately)
DELETE FROM `ref_jabatan`;
/*!40000 ALTER TABLE `ref_jabatan` DISABLE KEYS */;
INSERT INTO `ref_jabatan` (`id`, `nama`) VALUES
	(1, 'General Manager'),
	(2, 'Pimpiman Proyek'),
	(3, 'Pelaksana');
/*!40000 ALTER TABLE `ref_jabatan` ENABLE KEYS */;

-- Dumping structure for table sefti.ref_tipe_jadwal
DROP TABLE IF EXISTS `ref_tipe_jadwal`;
CREATE TABLE IF NOT EXISTS `ref_tipe_jadwal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipe` varchar(50) NOT NULL DEFAULT '0',
  `keterangan` varchar(100) NOT NULL DEFAULT '0',
  `jadwal_jam_masuk` varchar(50) NOT NULL DEFAULT '0',
  `jadwal_menit_masuk` varchar(50) NOT NULL DEFAULT '0',
  `jadwal_jam_keluar` varchar(50) NOT NULL DEFAULT '0',
  `jadwal_menit_keluar` varchar(50) NOT NULL DEFAULT '0',
  `shift` int(11) NOT NULL DEFAULT '0',
  `sabtu_masuk` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table sefti.ref_tipe_jadwal: ~2 rows (approximately)
DELETE FROM `ref_tipe_jadwal`;
/*!40000 ALTER TABLE `ref_tipe_jadwal` DISABLE KEYS */;
INSERT INTO `ref_tipe_jadwal` (`id`, `tipe`, `keterangan`, `jadwal_jam_masuk`, `jadwal_menit_masuk`, `jadwal_jam_keluar`, `jadwal_menit_keluar`, `shift`, `sabtu_masuk`) VALUES
	(1, 'A', 'Jadwal kantor secara umum', '7', '30', '16', '0', 0, 0),
	(2, 'B', 'SHIFT', '', '', '', '', 1, 0);
/*!40000 ALTER TABLE `ref_tipe_jadwal` ENABLE KEYS */;

-- Dumping structure for table sefti.t_groupmenu
DROP TABLE IF EXISTS `t_groupmenu`;
CREATE TABLE IF NOT EXISTS `t_groupmenu` (
  `groupmenuid` int(11) NOT NULL AUTO_INCREMENT,
  `groupid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `menuid` int(3) NOT NULL DEFAULT '0',
  `ad` tinyint(3) unsigned DEFAULT '0',
  `up` tinyint(3) unsigned DEFAULT '0',
  `de` tinyint(3) unsigned DEFAULT '0',
  `vi` tinyint(3) unsigned DEFAULT '0',
  `pr` tinyint(3) unsigned DEFAULT '0',
  PRIMARY KEY (`groupmenuid`),
  KEY `FK_t_groupmenu_t_groups` (`groupid`),
  KEY `FK_t_groupmenu_t_menu` (`menuid`),
  CONSTRAINT `FK_t_groupmenu_t_groups` FOREIGN KEY (`groupid`) REFERENCES `t_groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_t_groupmenu_t_menu` FOREIGN KEY (`menuid`) REFERENCES `t_menu` (`menuid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2323 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table sefti.t_groupmenu: ~23 rows (approximately)
DELETE FROM `t_groupmenu`;
/*!40000 ALTER TABLE `t_groupmenu` DISABLE KEYS */;
INSERT INTO `t_groupmenu` (`groupmenuid`, `groupid`, `menuid`, `ad`, `up`, `de`, `vi`, `pr`) VALUES
	(2298, 1, 1, 1, 1, 1, 1, 1),
	(2299, 1, 2, 1, 1, 1, 1, 1),
	(2300, 1, 3, 1, 1, 1, 1, 1),
	(2301, 1, 22, 1, 1, 1, 1, 1),
	(2302, 1, 23, 1, 1, 1, 1, 1),
	(2303, 1, 61, 1, 1, 1, 1, 1),
	(2304, 1, 103, 1, 1, 1, 1, 1),
	(2305, 1, 39, 1, 1, 1, 1, 1),
	(2306, 1, 40, 1, 1, 1, 1, 1),
	(2307, 1, 41, 1, 1, 1, 1, 1),
	(2308, 1, 42, 1, 1, 1, 1, 1),
	(2309, 1, 102, 1, 1, 1, 1, 1),
	(2310, 1, 43, 1, 1, 1, 1, 1),
	(2311, 1, 45, 1, 1, 1, 1, 1),
	(2312, 1, 46, 1, 1, 1, 1, 1),
	(2313, 1, 67, 1, 1, 1, 1, 1),
	(2316, 1, 47, 1, 1, 1, 1, 1),
	(2317, 1, 68, 1, 1, 1, 1, 1),
	(2318, 1, 69, 1, 1, 1, 1, 1),
	(2319, 1, 74, 1, 1, 1, 1, 1),
	(2320, 1, 83, 1, 1, 1, 1, 1),
	(2321, 1, 86, 1, 1, 1, 1, 1),
	(2322, 1, 87, 1, 1, 1, 1, 1);
/*!40000 ALTER TABLE `t_groupmenu` ENABLE KEYS */;

-- Dumping structure for table sefti.t_groups
DROP TABLE IF EXISTS `t_groups`;
CREATE TABLE IF NOT EXISTS `t_groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table sefti.t_groups: ~7 rows (approximately)
DELETE FROM `t_groups`;
/*!40000 ALTER TABLE `t_groups` DISABLE KEYS */;
INSERT INTO `t_groups` (`id`, `name`, `description`) VALUES
	(1, 'admin', 'Super Admin'),
	(2, 'operator', 'Operator SKPD'),
	(3, 'Operator UPTD/SMP', 'Operator uptd/smp'),
	(4, 'Operator SDN', 'Operator sdn'),
	(5, 'Operator SATPOL ', 'Sub Satpol pp'),
	(6, 'Operator DINKES', 'Sub Operator dinkes'),
	(7, 'eksekutif', '');
/*!40000 ALTER TABLE `t_groups` ENABLE KEYS */;

-- Dumping structure for table sefti.t_login_attempts
DROP TABLE IF EXISTS `t_login_attempts`;
CREATE TABLE IF NOT EXISTS `t_login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table sefti.t_login_attempts: ~0 rows (approximately)
DELETE FROM `t_login_attempts`;
/*!40000 ALTER TABLE `t_login_attempts` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_login_attempts` ENABLE KEYS */;

-- Dumping structure for table sefti.t_log_aktivitas
DROP TABLE IF EXISTS `t_log_aktivitas`;
CREATE TABLE IF NOT EXISTS `t_log_aktivitas` (
  `id_log` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) unsigned NOT NULL DEFAULT '0',
  `aktivitas` text,
  `waktu` datetime DEFAULT NULL,
  `ip_user` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_log`),
  KEY `waktu` (`waktu`),
  KEY `userid` (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=950099 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table sefti.t_log_aktivitas: ~344 rows (approximately)
DELETE FROM `t_log_aktivitas`;
/*!40000 ALTER TABLE `t_log_aktivitas` DISABLE KEYS */;
INSERT INTO `t_log_aktivitas` (`id_log`, `userid`, `aktivitas`, `waktu`, `ip_user`) VALUES
	(949699, 1, 'Login Ke dalam Sistem Absensi', '2019-03-26 12:26:04', '127.0.0.1'),
	(949700, 1, 'Hapus data menu dengan nama menu: 101', '2019-03-26 12:41:10', '127.0.0.1'),
	(949701, 1, 'Hapus data menu dengan nama menu: 92', '2019-03-26 12:41:34', '127.0.0.1'),
	(949702, 1, 'Hapus data menu dengan nama menu: 81', '2019-03-26 12:41:40', '127.0.0.1'),
	(949703, 1, 'Hapus data menu dengan nama menu: 76', '2019-03-26 12:41:46', '127.0.0.1'),
	(949704, 1, 'Hapus data menu dengan nama menu: 98', '2019-03-26 12:41:53', '127.0.0.1'),
	(949705, 1, 'Hapus data menu dengan nama menu: 97', '2019-03-26 12:42:04', '127.0.0.1'),
	(949706, 1, 'Hapus data menu dengan nama menu: 100', '2019-03-26 12:51:06', '127.0.0.1'),
	(949707, 1, 'Hapus data menu dengan nama menu: 90', '2019-03-26 12:51:13', '127.0.0.1'),
	(949708, 1, 'Hapus data menu dengan nama menu: 91', '2019-03-26 12:51:24', '127.0.0.1'),
	(949709, 1, 'Hapus data menu dengan nama menu: 71', '2019-03-26 12:51:39', '127.0.0.1'),
	(949710, 1, 'Hapus data menu dengan nama menu: 82', '2019-03-26 12:51:44', '127.0.0.1'),
	(949711, 1, 'Hapus data menu dengan nama menu: 70', '2019-03-26 12:51:49', '127.0.0.1'),
	(949712, 1, 'Hapus data menu dengan nama menu: 99', '2019-03-26 12:51:59', '127.0.0.1'),
	(949713, 1, 'Hapus data menu dengan nama menu: 88', '2019-03-26 12:52:09', '127.0.0.1'),
	(949714, 1, 'Hapus data menu dengan nama menu: 65', '2019-03-26 12:52:19', '127.0.0.1'),
	(949715, 1, 'Hapus data menu dengan nama menu: 96', '2019-03-26 12:52:27', '127.0.0.1'),
	(949716, 1, 'Hapus data menu dengan nama menu: 50', '2019-03-26 12:52:35', '127.0.0.1'),
	(949717, 1, 'Hapus data menu dengan nama menu: 72', '2019-03-26 12:52:44', '127.0.0.1'),
	(949718, 1, 'Hapus data menu dengan nama menu: 78', '2019-03-26 12:52:49', '127.0.0.1'),
	(949719, 1, 'Hapus data menu dengan nama menu: 48', '2019-03-26 12:53:05', '127.0.0.1'),
	(949720, 1, 'Hapus data menu dengan nama menu: 75', '2019-03-26 12:56:35', '127.0.0.1'),
	(949721, 1, 'Hapus data menu dengan nama menu: 84', '2019-03-26 13:00:31', '127.0.0.1'),
	(949722, 1, 'Hapus data menu dengan nama menu: 85', '2019-03-26 13:00:37', '127.0.0.1'),
	(949723, 1, 'Hapus data menu dengan nama menu: 89', '2019-03-26 13:01:33', '127.0.0.1'),
	(949724, 1, 'Hapus data menu dengan nama menu: 77', '2019-03-26 13:09:18', '127.0.0.1'),
	(949725, 1, 'Hapus data menu dengan nama menu: 73', '2019-03-26 13:09:41', '127.0.0.1'),
	(949726, 1, 'Hapus data menu dengan nama menu: 94', '2019-03-26 13:18:42', '127.0.0.1'),
	(949727, 1, 'Hapus data menu dengan nama menu: 60', '2019-03-26 13:18:56', '127.0.0.1'),
	(949728, 1, 'Hapus data menu dengan nama menu: 59', '2019-03-26 13:19:12', '127.0.0.1'),
	(949729, 1, 'Hapus data menu dengan nama menu: 31', '2019-03-26 13:19:22', '127.0.0.1'),
	(949730, 1, 'Hapus data menu dengan nama menu: 57', '2019-03-26 13:19:30', '127.0.0.1'),
	(949731, 1, 'Hapus data menu dengan nama menu: 58', '2019-03-26 13:19:42', '127.0.0.1'),
	(949732, 1, 'Lihat Data Project', '2019-03-26 14:39:27', '127.0.0.1'),
	(949733, 1, 'Lihat Data Project', '2019-03-26 14:47:30', '127.0.0.1'),
	(949734, 1, 'Lihat Data Project', '2019-03-26 14:47:56', '127.0.0.1'),
	(949735, 1, 'Lihat Data Project', '2019-03-26 14:48:15', '127.0.0.1'),
	(949736, 1, 'Lihat Data Project', '2019-03-26 14:50:13', '127.0.0.1'),
	(949737, 1, 'Lihat Data Project', '2019-03-26 14:56:56', '127.0.0.1'),
	(949738, 1, 'Lihat Data Project', '2019-03-26 15:02:37', '127.0.0.1'),
	(949739, 1, 'Lihat Data Project', '2019-03-26 15:19:40', '127.0.0.1'),
	(949740, 1, 'Lihat Data Project', '2019-03-26 15:20:38', '127.0.0.1'),
	(949741, 1, 'Lihat Data Project', '2019-03-26 15:20:42', '127.0.0.1'),
	(949742, 1, 'Lihat Data Project', '2019-03-26 15:45:03', '127.0.0.1'),
	(949743, 1, 'Lihat Data Project', '2019-03-26 16:38:57', '127.0.0.1'),
	(949744, 1, 'Lihat Data Project', '2019-03-26 16:47:21', '127.0.0.1'),
	(949745, 1, 'Tambah Data project dengan id:1', '2019-03-26 16:57:19', '127.0.0.1'),
	(949746, 1, 'Lihat Data Project', '2019-03-26 16:57:28', '127.0.0.1'),
	(949747, 1, 'Lihat Data Project', '2019-03-26 16:57:47', '127.0.0.1'),
	(949748, 1, 'Login Ke dalam Sistem Absensi', '2019-03-26 20:39:39', '127.0.0.1'),
	(949749, 1, 'Lihat Data Project', '2019-03-26 20:40:32', '127.0.0.1'),
	(949750, 1, 'Lihat Data Project', '2019-03-26 20:47:20', '127.0.0.1'),
	(949751, 1, 'Edit Data project dengan id:1', '2019-03-26 20:48:28', '127.0.0.1'),
	(949752, 1, 'Lihat Data Project', '2019-03-26 20:48:28', '127.0.0.1'),
	(949753, 1, 'Lihat Data Project', '2019-03-26 20:48:37', '127.0.0.1'),
	(949754, 1, 'Edit Data project dengan id:1', '2019-03-26 20:49:32', '127.0.0.1'),
	(949755, 1, 'Lihat Data Project', '2019-03-26 20:49:32', '127.0.0.1'),
	(949756, 1, 'Lihat Data Project', '2019-03-26 20:49:36', '127.0.0.1'),
	(949757, 1, 'Lihat Data Project', '2019-03-26 20:49:42', '127.0.0.1'),
	(949758, 1, 'Lihat Data Project', '2019-03-26 21:01:58', '127.0.0.1'),
	(949759, 1, 'Edit Data project dengan id:1', '2019-03-26 21:05:14', '127.0.0.1'),
	(949760, 1, 'Lihat Data Project', '2019-03-26 21:05:14', '127.0.0.1'),
	(949761, 1, 'Lihat Data Project', '2019-03-26 21:05:28', '127.0.0.1'),
	(949762, 1, 'Lihat Data Project', '2019-03-26 21:07:37', '127.0.0.1'),
	(949763, 1, 'Lihat Data Project', '2019-03-26 21:08:03', '127.0.0.1'),
	(949764, 1, 'Lihat Data rincian project dengan id: 1', '2019-03-26 21:28:28', '127.0.0.1'),
	(949765, 1, 'Lihat Data rincian project dengan id: 1', '2019-03-26 21:28:38', '127.0.0.1'),
	(949766, 1, 'Lihat Data Project', '2019-03-26 21:34:39', '127.0.0.1'),
	(949767, 1, 'Lihat Data Project', '2019-03-26 21:34:51', '127.0.0.1'),
	(949768, 1, 'Lihat Data rincian project dengan id: 1', '2019-03-26 21:34:55', '127.0.0.1'),
	(949769, 1, 'Lihat Data rincian project dengan id: 1', '2019-03-26 21:37:11', '127.0.0.1'),
	(949770, 1, 'Lihat Data rincian project dengan id: 1', '2019-03-26 21:37:28', '127.0.0.1'),
	(949771, 1, 'Lihat Data Project', '2019-03-26 21:43:20', '127.0.0.1'),
	(949772, 1, 'Lihat Data Project', '2019-03-26 21:43:25', '127.0.0.1'),
	(949773, 1, 'Lihat Data rincian project dengan id: 1', '2019-03-26 21:43:28', '127.0.0.1'),
	(949774, 1, 'Lihat Data rincian project dengan id: 1', '2019-03-26 21:43:55', '127.0.0.1'),
	(949775, 1, 'Lihat Data rincian project dengan id: 1', '2019-03-26 21:44:58', '127.0.0.1'),
	(949776, 1, 'Lihat Data rincian project dengan id: 1', '2019-03-26 21:46:45', '127.0.0.1'),
	(949777, 1, 'Lihat Data rincian project dengan id: 1', '2019-03-26 21:47:16', '127.0.0.1'),
	(949778, 1, 'Lihat Data rincian project dengan id: 1', '2019-03-26 21:47:45', '127.0.0.1'),
	(949779, 1, 'Lihat Data rincian project dengan id: 1', '2019-03-26 21:47:50', '127.0.0.1'),
	(949780, 1, 'Lihat Data rincian project dengan id: 1', '2019-03-26 21:49:32', '127.0.0.1'),
	(949781, 1, 'Lihat Data rincian project dengan id: 1', '2019-03-26 21:49:34', '127.0.0.1'),
	(949782, 1, 'Lihat Data rincian project dengan id: 1', '2019-03-26 21:50:21', '127.0.0.1'),
	(949783, 1, 'Lihat Data rincian project dengan id: 1', '2019-03-26 21:50:40', '127.0.0.1'),
	(949784, 1, 'Lihat Data rincian project dengan id: 1', '2019-03-26 21:51:12', '127.0.0.1'),
	(949785, 1, 'Login Ke dalam Sistem Absensi', '2019-03-26 22:59:18', '127.0.0.1'),
	(949786, 1, 'Lihat Data Project', '2019-03-26 22:59:21', '127.0.0.1'),
	(949787, 1, 'Lihat Data rincian project dengan id: 1', '2019-03-26 22:59:23', '127.0.0.1'),
	(949788, 1, 'Lihat Data rincian project dengan id: 1', '2019-03-26 23:01:27', '127.0.0.1'),
	(949789, 1, 'Lihat Data rincian project dengan id: 1', '2019-03-26 23:07:29', '127.0.0.1'),
	(949790, 1, 'Lihat Data rincian project dengan id: 1', '2019-03-26 23:08:22', '127.0.0.1'),
	(949791, 1, 'Lihat Data rincian project dengan id: 1', '2019-03-26 23:08:36', '127.0.0.1'),
	(949792, 1, 'Lihat Data rincian project dengan id: 1', '2019-03-26 23:08:45', '127.0.0.1'),
	(949793, 1, 'Lihat Data rincian project dengan id: 1', '2019-03-26 23:09:16', '127.0.0.1'),
	(949794, 1, 'Tambah Data project dengan id:1', '2019-03-26 23:17:41', '127.0.0.1'),
	(949795, 1, 'Lihat Data Project', '2019-03-26 23:17:41', '127.0.0.1'),
	(949796, 1, 'Lihat Data rincian project dengan id: 1', '2019-03-26 23:17:45', '127.0.0.1'),
	(949797, 1, 'Lihat Data rincian project dengan id: 1', '2019-03-26 23:25:14', '127.0.0.1'),
	(949798, 1, 'Lihat Data rincian project dengan id: 1', '2019-03-26 23:25:30', '127.0.0.1'),
	(949799, 1, 'Lihat Data rincian project dengan id: 1', '2019-03-26 23:28:10', '127.0.0.1'),
	(949800, 1, 'Lihat Data rincian project dengan id: 1', '2019-03-26 23:31:48', '127.0.0.1'),
	(949801, 1, 'Lihat Data rincian project dengan id: 1', '2019-03-26 23:32:10', '127.0.0.1'),
	(949802, 1, 'Lihat Data rincian project dengan id: 1', '2019-03-26 23:32:15', '127.0.0.1'),
	(949803, 1, 'update Data site project dengan id:0', '2019-03-26 23:35:04', '127.0.0.1'),
	(949804, 1, 'Lihat Data rincian project dengan id: 1', '2019-03-26 23:35:04', '127.0.0.1'),
	(949805, 1, 'update Data site project dengan id:0', '2019-03-26 23:35:10', '127.0.0.1'),
	(949806, 1, 'Lihat Data rincian project dengan id: 1', '2019-03-26 23:35:10', '127.0.0.1'),
	(949807, 1, 'update Data site project dengan id:0', '2019-03-26 23:35:16', '127.0.0.1'),
	(949808, 1, 'Lihat Data rincian project dengan id: 1', '2019-03-26 23:35:16', '127.0.0.1'),
	(949809, 1, 'Lihat Data rincian project dengan id: 1', '2019-03-26 23:44:29', '127.0.0.1'),
	(949810, 1, 'Lihat Data rincian project dengan id: 1', '2019-03-26 23:46:31', '127.0.0.1'),
	(949811, 1, 'Tambah Data project dengan id:2', '2019-03-26 23:46:38', '127.0.0.1'),
	(949812, 1, 'Lihat Data rincian project dengan id: 1', '2019-03-26 23:46:38', '127.0.0.1'),
	(949813, 1, 'Lihat Data rincian project dengan id: 1', '2019-03-26 23:46:59', '127.0.0.1'),
	(949814, 1, 'Lihat Data rincian project dengan id: 1', '2019-03-26 23:47:02', '127.0.0.1'),
	(949815, 1, 'Lihat Data Project', '2019-03-26 23:47:04', '127.0.0.1'),
	(949816, 1, 'Lihat Data rincian project dengan id: 1', '2019-03-26 23:47:07', '127.0.0.1'),
	(949817, 1, 'Lihat Data Project', '2019-03-26 23:47:14', '127.0.0.1'),
	(949818, 1, 'Lihat Data Pegawai Proyek', '2019-03-26 23:59:54', '127.0.0.1'),
	(949819, 1, 'Lihat Data Pegawai Proyek', '2019-03-27 00:00:37', '127.0.0.1'),
	(949820, 1, 'Lihat Data Pegawai Proyek', '2019-03-27 00:01:02', '127.0.0.1'),
	(949821, 1, 'Lihat Data Pegawai Proyek', '2019-03-27 00:01:21', '127.0.0.1'),
	(949822, 1, 'Lihat Data Pegawai Proyek', '2019-03-27 00:01:32', '127.0.0.1'),
	(949823, 1, 'Login Ke dalam Sistem Absensi', '2019-03-27 07:56:23', '127.0.0.1'),
	(949824, 1, 'Lihat Data Project', '2019-03-27 07:57:48', '127.0.0.1'),
	(949825, 1, 'Lihat Data Pegawai Proyek', '2019-03-27 07:57:49', '127.0.0.1'),
	(949826, 1, 'Lihat Data Pegawai Proyek', '2019-03-27 08:06:27', '127.0.0.1'),
	(949827, 1, 'Lihat Data Pegawai Proyek', '2019-03-27 08:10:04', '127.0.0.1'),
	(949828, 1, 'Tambah Data pegawai dengan nip:1234', '2019-03-27 08:11:36', '127.0.0.1'),
	(949829, 1, 'Lihat Data Pegawai Proyek', '2019-03-27 08:11:37', '127.0.0.1'),
	(949830, 1, 'Lihat Data Pegawai Proyek', '2019-03-27 08:13:37', '127.0.0.1'),
	(949831, 1, 'Lihat Data Pegawai Proyek', '2019-03-27 08:13:46', '127.0.0.1'),
	(949832, 1, 'Lihat Data Pegawai Proyek', '2019-03-27 08:14:34', '127.0.0.1'),
	(949833, 1, 'Lihat Data Pegawai Proyek', '2019-03-27 08:15:18', '127.0.0.1'),
	(949834, 1, 'Lihat Data Pegawai Proyek', '2019-03-27 08:18:14', '127.0.0.1'),
	(949835, 1, 'Tambah Data pegawai dengan nip:1234', '2019-03-27 08:25:25', '127.0.0.1'),
	(949836, 1, 'Lihat Data Pegawai Proyek', '2019-03-27 08:25:25', '127.0.0.1'),
	(949837, 1, 'Lihat Data Pegawai Proyek', '2019-03-27 08:25:43', '127.0.0.1'),
	(949838, 1, 'Lihat Data Pegawai Proyek', '2019-03-27 08:26:32', '127.0.0.1'),
	(949839, 1, 'Tambah Data pegawai dengan nip:1234', '2019-03-27 08:26:52', '127.0.0.1'),
	(949840, 1, 'Lihat Data Pegawai Proyek', '2019-03-27 08:26:52', '127.0.0.1'),
	(949841, 0, 'Edit Data pegawai dengan nip:1234', '2019-03-27 08:32:05', '127.0.0.1'),
	(949842, 1, 'Lihat Data Pegawai Proyek', '2019-03-27 08:32:05', '127.0.0.1'),
	(949843, 1, 'Lihat Data Pegawai Proyek', '2019-03-27 08:34:25', '127.0.0.1'),
	(949844, 1, 'Lihat Data Pegawai Proyek', '2019-03-27 08:35:23', '127.0.0.1'),
	(949845, 1, 'Lihat Data Pegawai Proyek', '2019-03-27 08:35:28', '127.0.0.1'),
	(949846, 1, 'Lihat Data Pegawai Proyek', '2019-03-27 08:38:11', '127.0.0.1'),
	(949847, 1, 'Lihat Data Pegawai Proyek', '2019-03-27 08:38:16', '127.0.0.1'),
	(949848, 0, 'Edit Data pegawai dengan nip:1234', '2019-03-27 08:38:25', '127.0.0.1'),
	(949849, 1, 'Lihat Data Pegawai Proyek', '2019-03-27 08:38:26', '127.0.0.1'),
	(949850, 1, 'Lihat Data Pegawai Proyek', '2019-03-27 08:38:53', '127.0.0.1'),
	(949851, 1, 'Lihat Data Project', '2019-03-27 08:39:03', '127.0.0.1'),
	(949852, 1, 'Lihat Data Pegawai Proyek', '2019-03-27 08:39:04', '127.0.0.1'),
	(949853, 1, 'Lihat Data Project', '2019-03-27 08:39:16', '127.0.0.1'),
	(949854, 1, 'Lihat Data Pegawai Proyek', '2019-03-27 08:39:18', '127.0.0.1'),
	(949855, 1, 'Lihat Data Pegawai Proyek', '2019-03-27 08:39:36', '127.0.0.1'),
	(949856, 1, 'Login Ke dalam Sistem Absensi', '2019-03-29 17:15:08', '127.0.0.1'),
	(949857, 1, 'Login Ke dalam Sistem Absensi', '2019-03-30 10:01:11', '127.0.0.1'),
	(949858, 1, 'Lihat Data Project', '2019-03-30 10:01:15', '127.0.0.1'),
	(949859, 1, 'Lihat Data Pegawai Proyek', '2019-03-30 10:01:17', '127.0.0.1'),
	(949860, 1, 'Lihat Data Pegawai Proyek', '2019-03-30 10:19:57', '127.0.0.1'),
	(949861, 1, 'Lihat Data Pegawai Proyek', '2019-03-30 10:27:34', '127.0.0.1'),
	(949862, 1, 'Lihat Data Mesin Absensi', '2019-03-30 10:27:36', '127.0.0.1'),
	(949863, 1, 'Lihat Data Mesin Absensi', '2019-03-30 10:27:46', '127.0.0.1'),
	(949864, 1, 'Lihat Data Mesin Absensi', '2019-03-30 10:28:32', '127.0.0.1'),
	(949865, 1, 'Lihat Data Mesin Absensi', '2019-03-30 10:28:39', '127.0.0.1'),
	(949866, 1, 'Lihat Data Pegawai Proyek', '2019-03-30 10:28:40', '127.0.0.1'),
	(949867, 1, 'Lihat Data Mesin Absensi', '2019-03-30 10:28:41', '127.0.0.1'),
	(949868, 1, 'Lihat Data Mesin Absensi', '2019-03-30 10:31:04', '127.0.0.1'),
	(949869, 1, 'Lihat Data Pegawai Proyek', '2019-03-30 10:31:05', '127.0.0.1'),
	(949870, 1, 'Lihat Data Mesin Absensi', '2019-03-30 10:31:06', '127.0.0.1'),
	(949871, 1, 'Lihat Data Mesin Absensi', '2019-03-30 10:31:31', '127.0.0.1'),
	(949872, 1, 'Lihat Data Pegawai Proyek', '2019-03-30 10:32:26', '127.0.0.1'),
	(949873, 1, 'Lihat Data Mesin Absensi', '2019-03-30 10:32:27', '127.0.0.1'),
	(949874, 1, 'Lihat Data Project', '2019-03-30 10:33:36', '127.0.0.1'),
	(949875, 1, 'Lihat Data Pegawai Proyek', '2019-03-30 10:33:38', '127.0.0.1'),
	(949876, 1, 'Lihat Data Mesin Absensi', '2019-03-30 10:33:42', '127.0.0.1'),
	(949877, 1, 'Lihat Data Mesin dan Pegawai', '2019-03-30 10:40:11', '127.0.0.1'),
	(949878, 1, 'Lihat Data Mesin dan Pegawai', '2019-03-30 10:50:56', '127.0.0.1'),
	(949879, 1, 'Lihat Data Mesin dan Pegawai', '2019-03-30 10:51:04', '127.0.0.1'),
	(949880, 1, 'Lihat Data Mesin dan Pegawai', '2019-03-30 10:51:26', '127.0.0.1'),
	(949881, 1, 'Lihat Data Mesin dan Pegawai', '2019-03-30 10:53:08', '127.0.0.1'),
	(949882, 1, 'Lihat Data Mesin dan Pegawai', '2019-03-30 10:53:12', '127.0.0.1'),
	(949883, 1, 'Lihat Data Mesin dan Pegawai', '2019-03-30 10:53:24', '127.0.0.1'),
	(949884, 1, 'Lihat Data Mesin dan Pegawai', '2019-03-30 10:53:32', '127.0.0.1'),
	(949885, 1, 'Lihat Data Mesin dan Pegawai', '2019-03-30 10:57:03', '127.0.0.1'),
	(949886, 1, 'Lihat Data Mesin dan Pegawai', '2019-03-30 10:57:07', '127.0.0.1'),
	(949887, 1, 'Lihat Data Mesin Absensi', '2019-03-30 10:57:31', '127.0.0.1'),
	(949888, 1, 'Lihat Data Mesin Absensi', '2019-03-30 10:58:03', '127.0.0.1'),
	(949889, 1, 'Lihat Data Mesin Absensi', '2019-03-30 10:59:05', '127.0.0.1'),
	(949890, 1, 'Lihat Data Mesin Absensi', '2019-03-30 11:04:36', '127.0.0.1'),
	(949891, 1, 'Lihat Data Mesin Absensi', '2019-03-30 11:12:42', '127.0.0.1'),
	(949892, 1, 'Tambah data mesin pegawai dengan id: 1', '2019-03-30 11:12:58', '127.0.0.1'),
	(949893, 1, 'Lihat Data Mesin dan Pegawai', '2019-03-30 11:12:58', '127.0.0.1'),
	(949894, 1, 'Lihat Data Mesin Absensi', '2019-03-30 11:13:13', '127.0.0.1'),
	(949895, 1, 'Lihat Data Mesin Absensi', '2019-03-30 11:13:29', '127.0.0.1'),
	(949896, 1, 'Lihat Data Mesin Absensi', '2019-03-30 11:13:33', '127.0.0.1'),
	(949897, 1, 'Lihat Data Mesin Absensi', '2019-03-30 11:15:30', '127.0.0.1'),
	(949898, 1, 'Lihat Data Mesin Absensi', '2019-03-30 11:15:35', '127.0.0.1'),
	(949899, 1, 'Lihat Data Mesin Absensi', '2019-03-30 11:15:51', '127.0.0.1'),
	(949900, 1, 'Lihat Data Mesin Absensi', '2019-03-30 11:16:44', '127.0.0.1'),
	(949901, 1, 'Lihat Data Mesin Absensi', '2019-03-30 11:16:52', '127.0.0.1'),
	(949902, 1, 'Lihat Data Mesin Absensi', '2019-03-30 11:17:34', '127.0.0.1'),
	(949903, 1, 'Lihat Data Mesin Absensi', '2019-03-30 11:18:13', '127.0.0.1'),
	(949904, 1, 'Lihat Data Mesin Absensi', '2019-03-30 11:22:34', '127.0.0.1'),
	(949905, 1, 'Lihat Data Mesin dan Pegawai', '2019-03-30 11:22:56', '127.0.0.1'),
	(949906, 1, 'Lihat Data Mesin Absensi', '2019-03-30 11:22:58', '127.0.0.1'),
	(949907, 1, 'Lihat Data Mesin Absensi', '2019-03-30 11:23:04', '127.0.0.1'),
	(949908, 1, 'Lihat Data Mesin Absensi', '2019-03-30 11:23:37', '127.0.0.1'),
	(949909, 1, 'Lihat Data Enroll Mesin Absensi dengan id: 1', '2019-03-30 11:24:09', '127.0.0.1'),
	(949910, 1, 'Lihat Data Enroll Mesin Absensi dengan id: 1', '2019-03-30 11:25:58', '127.0.0.1'),
	(949911, 1, 'Lihat Data Mesin Absensi', '2019-03-30 11:27:37', '127.0.0.1'),
	(949912, 1, 'Lihat Data Mesin Absensi', '2019-03-30 11:27:43', '127.0.0.1'),
	(949913, 1, 'Lihat Data Mesin Absensi', '2019-03-30 11:28:54', '127.0.0.1'),
	(949914, 1, 'Lihat Data Mesin Absensi', '2019-03-30 11:32:48', '127.0.0.1'),
	(949915, 1, 'Lihat Data Mesin Absensi', '2019-03-30 11:33:14', '127.0.0.1'),
	(949916, 1, 'Edit mesin absensi dengan id: 1', '2019-03-30 11:33:21', '127.0.0.1'),
	(949917, 1, 'Lihat Data Mesin Absensi', '2019-03-30 11:33:21', '127.0.0.1'),
	(949918, 1, 'Lihat Data Mesin Absensi', '2019-03-30 11:33:29', '127.0.0.1'),
	(949919, 1, 'Lihat Data Mesin Absensi', '2019-03-30 11:34:39', '127.0.0.1'),
	(949920, 1, 'Lihat Data Mesin Absensi', '2019-03-30 11:34:45', '127.0.0.1'),
	(949921, 1, 'Lihat Data Mesin Absensi', '2019-03-30 11:35:58', '127.0.0.1'),
	(949922, 1, 'Lihat Data Mesin Absensi', '2019-03-30 11:36:04', '127.0.0.1'),
	(949923, 1, 'Tambah data mesin pegawai dengan id: 2', '2019-03-30 11:36:22', '127.0.0.1'),
	(949924, 1, 'Lihat Data Mesin Absensi', '2019-03-30 11:36:22', '127.0.0.1'),
	(949925, 1, 'Lihat Data Mesin Absensi', '2019-03-30 11:36:25', '127.0.0.1'),
	(949926, 1, 'Lihat Data Mesin dan Pegawai', '2019-03-30 11:36:27', '127.0.0.1'),
	(949927, 1, 'Lihat Data Mesin dan Pegawai', '2019-03-30 11:39:08', '127.0.0.1'),
	(949928, 1, 'Lihat Data Mesin dan Pegawai', '2019-03-30 11:45:12', '127.0.0.1'),
	(949929, 1, 'Tambah data mesin pegawai dengan id: 1', '2019-03-30 11:45:38', '127.0.0.1'),
	(949930, 1, 'Lihat Data Mesin dan Pegawai', '2019-03-30 11:45:38', '127.0.0.1'),
	(949931, 1, 'Lihat Data Mesin dan Pegawai', '2019-03-30 11:45:45', '127.0.0.1'),
	(949932, 1, 'Lihat Data Mesin dan Pegawai', '2019-03-30 11:50:10', '127.0.0.1'),
	(949933, 1, 'Lihat Data Mesin dan Pegawai', '2019-03-30 11:50:51', '127.0.0.1'),
	(949934, 1, 'Lihat Data Mesin dan Pegawai', '2019-03-30 11:51:13', '127.0.0.1'),
	(949935, 1, 'Lihat Data Mesin dan Pegawai', '2019-03-30 11:52:00', '127.0.0.1'),
	(949936, 1, 'Lihat Data Mesin dan Pegawai', '2019-03-30 11:53:14', '127.0.0.1'),
	(949937, 1, 'Lihat Data Mesin dan Pegawai', '2019-03-30 11:53:19', '127.0.0.1'),
	(949938, 1, 'Lihat Data Pegawai Proyek', '2019-03-30 11:53:21', '127.0.0.1'),
	(949939, 1, 'Lihat Data Mesin dan Pegawai', '2019-03-30 11:53:24', '127.0.0.1'),
	(949940, 1, 'Lihat Data Mesin dan Pegawai', '2019-03-30 11:53:32', '127.0.0.1'),
	(949941, 1, 'Hapus Data Mesin Pegawai dengan id:1', '2019-03-30 11:53:54', '127.0.0.1'),
	(949942, 1, 'Lihat Data Mesin dan Pegawai', '2019-03-30 11:53:54', '127.0.0.1'),
	(949943, 1, 'Tambah data mesin pegawai dengan id: 2', '2019-03-30 11:54:02', '127.0.0.1'),
	(949944, 1, 'Lihat Data Mesin dan Pegawai', '2019-03-30 11:54:02', '127.0.0.1'),
	(949945, 1, 'Lihat Data Mesin dan Pegawai', '2019-03-30 11:54:05', '127.0.0.1'),
	(949946, 1, 'Lihat Data Project', '2019-03-30 11:54:06', '127.0.0.1'),
	(949947, 1, 'Lihat Data Pegawai Proyek', '2019-03-30 11:54:07', '127.0.0.1'),
	(949948, 1, 'Lihat Data Mesin Absensi', '2019-03-30 11:54:08', '127.0.0.1'),
	(949949, 1, 'Lihat Data Mesin dan Pegawai', '2019-03-30 11:54:09', '127.0.0.1'),
	(949950, 1, 'Login Ke dalam Sistem Absensi', '2019-03-30 13:49:59', '127.0.0.1'),
	(949951, 1, 'Hapus data menu dengan nama menu: 95', '2019-03-30 13:50:27', '127.0.0.1'),
	(949952, 1, 'Hapus data menu dengan nama menu: 93', '2019-03-30 13:50:37', '127.0.0.1'),
	(949953, 1, 'Lihat data hari libur', '2019-03-30 13:50:45', '127.0.0.1'),
	(949954, 1, 'Lihat data hari libur', '2019-03-30 13:51:12', '127.0.0.1'),
	(949955, 1, 'Lihat data hari libur', '2019-03-30 13:51:45', '127.0.0.1'),
	(949956, 1, 'Lihat jadwal pegawai', '2019-03-30 13:55:13', '127.0.0.1'),
	(949957, 1, 'Lihat jadwal pegawai', '2019-03-30 13:56:47', '127.0.0.1'),
	(949958, 1, 'Lihat jadwal pegawai', '2019-03-30 13:59:57', '127.0.0.1'),
	(949959, 1, 'Lihat jadwal pegawai', '2019-03-30 14:00:03', '127.0.0.1'),
	(949960, 1, 'Lihat jadwal pegawai', '2019-03-30 14:00:17', '127.0.0.1'),
	(949961, 1, 'Lihat jadwal pegawai', '2019-03-30 14:00:33', '127.0.0.1'),
	(949962, 1, 'Lihat jadwal pegawai', '2019-03-30 14:00:54', '127.0.0.1'),
	(949963, 1, 'Lihat jadwal pegawai', '2019-03-30 14:01:14', '127.0.0.1'),
	(949964, 1, 'Lihat jadwal pegawai', '2019-03-30 14:01:19', '127.0.0.1'),
	(949965, 1, 'Lihat jadwal pegawai', '2019-03-30 14:01:32', '127.0.0.1'),
	(949966, 1, 'Lihat jadwal pegawai', '2019-03-30 14:02:34', '127.0.0.1'),
	(949967, 1, 'Lihat jadwal pegawai', '2019-03-30 14:02:44', '127.0.0.1'),
	(949968, 1, 'Lihat jadwal pegawai', '2019-03-30 14:03:38', '127.0.0.1'),
	(949969, 1, 'Lihat jadwal pegawai', '2019-03-30 14:03:44', '127.0.0.1'),
	(949970, 1, 'Lihat jadwal pegawai', '2019-03-30 14:04:03', '127.0.0.1'),
	(949971, 1, 'Lihat jadwal pegawai', '2019-03-30 14:04:21', '127.0.0.1'),
	(949972, 1, 'Lihat jadwal pegawai', '2019-03-30 14:04:44', '127.0.0.1'),
	(949973, 1, 'Lihat jadwal pegawai', '2019-03-30 14:05:30', '127.0.0.1'),
	(949974, 1, 'Lihat data hari libur', '2019-03-30 14:05:31', '127.0.0.1'),
	(949975, 1, 'Lihat Data Pegawai Proyek', '2019-03-30 14:05:44', '127.0.0.1'),
	(949976, 1, 'Lihat Data Pegawai Proyek', '2019-03-30 14:13:21', '127.0.0.1'),
	(949977, 0, 'Edit Data pegawai dengan nip:1234', '2019-03-30 14:13:24', '127.0.0.1'),
	(949978, 1, 'Lihat Data Pegawai Proyek', '2019-03-30 14:13:24', '127.0.0.1'),
	(949979, 1, 'Lihat jadwal pegawai', '2019-03-30 14:13:29', '127.0.0.1'),
	(949980, 1, 'Lihat jadwal pegawai', '2019-03-30 14:14:12', '127.0.0.1'),
	(949981, 1, 'Lihat jadwal pegawai', '2019-03-30 14:14:23', '127.0.0.1'),
	(949982, 1, 'Lihat jadwal pegawai', '2019-03-30 14:14:42', '127.0.0.1'),
	(949983, 1, 'edit jadwal pns dengan id:11563', '2019-03-30 14:14:49', '127.0.0.1'),
	(949984, 1, 'Lihat jadwal pegawai', '2019-03-30 14:14:53', '127.0.0.1'),
	(949985, 1, 'edit jadwal pns dengan id:11563', '2019-03-30 14:15:12', '127.0.0.1'),
	(949986, 1, 'Lihat jadwal pegawai', '2019-03-30 14:15:12', '127.0.0.1'),
	(949987, 1, 'edit jadwal pns dengan id:11563', '2019-03-30 14:15:15', '127.0.0.1'),
	(949988, 1, 'Lihat jadwal pegawai', '2019-03-30 14:15:16', '127.0.0.1'),
	(949989, 1, 'Lihat jadwal pegawai', '2019-03-30 14:17:39', '127.0.0.1'),
	(949990, 1, 'atur jadwal pegawai dengan nip:1234', '2019-03-30 14:18:25', '127.0.0.1'),
	(949991, 1, 'atur jadwal pegawai dengan nip:1234', '2019-03-30 14:18:55', '127.0.0.1'),
	(949992, 1, 'atur jadwal pegawai dengan nip:1234', '2019-03-30 14:19:25', '127.0.0.1'),
	(949993, 1, 'edit jadwal piket pns id:398095', '2019-03-30 14:19:58', '127.0.0.1'),
	(949994, 1, 'atur jadwal pegawai dengan nip:1234', '2019-03-30 14:20:05', '127.0.0.1'),
	(949995, 1, 'atur jadwal pegawai dengan nip:1234', '2019-03-30 14:20:30', '127.0.0.1'),
	(949996, 1, 'atur jadwal pegawai dengan nip:1234', '2019-03-30 14:21:46', '127.0.0.1'),
	(949997, 1, 'atur jadwal pegawai dengan nip:1234', '2019-03-30 14:21:50', '127.0.0.1'),
	(949998, 1, 'Lihat jadwal pegawai', '2019-03-30 14:22:21', '127.0.0.1'),
	(949999, 1, 'atur jadwal pegawai dengan nip:1234', '2019-03-30 14:22:24', '127.0.0.1'),
	(950000, 1, 'atur jadwal pegawai dengan nip:1234', '2019-03-30 14:22:32', '127.0.0.1'),
	(950001, 1, 'edit jadwal piket pns id:398095', '2019-03-30 14:22:36', '127.0.0.1'),
	(950002, 1, 'atur jadwal pegawai dengan nip:1234', '2019-03-30 14:22:37', '127.0.0.1'),
	(950003, 1, 'atur jadwal pegawai dengan nip:1234', '2019-03-30 14:22:47', '127.0.0.1'),
	(950004, 1, 'edit jadwal piket pns id:398095', '2019-03-30 14:23:08', '127.0.0.1'),
	(950005, 1, 'atur jadwal pegawai dengan nip:1234', '2019-03-30 14:23:08', '127.0.0.1'),
	(950006, 1, 'edit jadwal piket pns id:398095', '2019-03-30 14:23:40', '127.0.0.1'),
	(950007, 1, 'atur jadwal pegawai dengan nip:1234', '2019-03-30 14:23:40', '127.0.0.1'),
	(950008, 1, 'Lihat jadwal pegawai', '2019-03-30 14:23:45', '127.0.0.1'),
	(950009, 1, 'Lihat data hari libur', '2019-03-30 14:23:46', '127.0.0.1'),
	(950010, 1, 'Lihat Data Log Absensi', '2019-03-30 14:29:11', '127.0.0.1'),
	(950011, 1, 'Lihat Data Log Absensi', '2019-03-30 14:31:12', '127.0.0.1'),
	(950012, 1, 'Lihat Data Log Absensi', '2019-03-30 14:32:11', '127.0.0.1'),
	(950013, 1, 'Lihat Data Log Absensi', '2019-03-30 14:32:18', '127.0.0.1'),
	(950014, 1, 'Lihat Data Absensi harian pegawai', '2019-03-30 14:33:57', '127.0.0.1'),
	(950015, 1, 'Lihat Data Absensi harian pegawai', '2019-03-30 14:34:47', '127.0.0.1'),
	(950016, 1, 'Lihat Data Absensi harian pegawai', '2019-03-30 14:35:24', '127.0.0.1'),
	(950017, 1, 'Lihat Data Absensi harian pegawai', '2019-03-30 14:35:30', '127.0.0.1'),
	(950018, 1, 'Lihat Data Absensi harian pegawai', '2019-03-30 14:38:10', '127.0.0.1'),
	(950019, 1, 'Lihat Data Absensi harian pegawai', '2019-03-30 14:38:51', '127.0.0.1'),
	(950020, 1, 'Lihat Data Absensi harian pegawai', '2019-03-30 14:38:57', '127.0.0.1'),
	(950021, 1, 'Lihat Data Absensi harian pegawai', '2019-03-30 14:39:24', '127.0.0.1'),
	(950022, 1, 'Lihat Data Absensi harian pegawai', '2019-03-30 14:40:43', '127.0.0.1'),
	(950023, 1, 'Lihat Data Absensi harian pegawai', '2019-03-30 14:40:47', '127.0.0.1'),
	(950024, 1, 'Lihat Data bulanan PNS', '2019-03-30 14:42:52', '127.0.0.1'),
	(950025, 1, 'Lihat Data bulanan PNS', '2019-03-30 14:42:59', '127.0.0.1'),
	(950026, 1, 'Lihat Data bulanan PNS', '2019-03-30 14:43:11', '127.0.0.1'),
	(950027, 1, 'Lihat Data bulanan PNS', '2019-03-30 14:43:41', '127.0.0.1'),
	(950028, 1, 'Lihat Data bulanan PNS', '2019-03-30 14:45:35', '127.0.0.1'),
	(950029, 1, 'Lihat Data Absensi harian pegawai', '2019-03-30 14:45:37', '127.0.0.1'),
	(950030, 1, 'Lihat Data bulanan PNS', '2019-03-30 14:45:38', '127.0.0.1'),
	(950031, 1, 'Lihat Data Log Absensi', '2019-03-30 14:45:40', '127.0.0.1'),
	(950032, 1, 'Lihat data hari libur', '2019-03-30 14:50:42', '127.0.0.1'),
	(950033, 1, 'Login Ke dalam Sistem Absensi', '2019-03-30 18:25:14', '127.0.0.1'),
	(950034, 1, 'Lihat Data Project', '2019-03-30 18:25:34', '127.0.0.1'),
	(950035, 1, 'Lihat Data Project', '2019-03-30 18:26:00', '127.0.0.1'),
	(950036, 1, 'Login Ke dalam Sistem Absensi', '2019-03-30 21:06:28', '127.0.0.1'),
	(950037, 1, 'Lihat Data Project', '2019-03-30 21:07:16', '127.0.0.1'),
	(950038, 1, 'Lihat Data Absensi harian pegawai', '2019-03-30 21:12:36', '127.0.0.1'),
	(950039, 1, 'Lihat Data Absensi harian pegawai', '2019-03-30 21:12:52', '127.0.0.1'),
	(950040, 1, 'Login Ke dalam Sistem Absensi', '2019-04-06 09:28:27', '127.0.0.1'),
	(950041, 1, 'Login Ke dalam Sistem Absensi', '2019-04-06 13:16:24', '127.0.0.1'),
	(950042, 1, 'Lihat Data Project', '2019-04-06 13:16:26', '127.0.0.1'),
	(950043, 1, 'Lihat Data Mesin Absensi', '2019-04-06 13:18:38', '127.0.0.1'),
	(950044, 1, 'Logout dari Sistem Absensi', '2019-04-06 13:21:19', '127.0.0.1'),
	(950045, 1, 'Login Ke dalam Sistem Absensi', '2019-04-06 13:23:12', '127.0.0.1'),
	(950046, 1, 'Lihat Data Project', '2019-04-06 13:30:58', '127.0.0.1'),
	(950047, 1, 'Lihat Data Project', '2019-04-06 13:31:50', '127.0.0.1'),
	(950048, 1, 'Lihat Data Pegawai Proyek', '2019-04-06 13:32:47', '127.0.0.1'),
	(950049, 1, 'Lihat Data Mesin Absensi', '2019-04-06 13:33:37', '127.0.0.1'),
	(950050, 1, 'Lihat Data Mesin dan Pegawai', '2019-04-06 13:34:32', '127.0.0.1'),
	(950051, 1, 'Lihat Data Mesin dan Pegawai', '2019-04-06 13:34:44', '127.0.0.1'),
	(950052, 1, 'Lihat Data Mesin dan Pegawai', '2019-04-06 13:34:46', '127.0.0.1'),
	(950053, 1, 'Lihat Data Log Absensi', '2019-04-06 13:43:26', '127.0.0.1'),
	(950054, 1, 'Lihat Data Absensi harian pegawai', '2019-04-06 13:43:29', '127.0.0.1'),
	(950055, 1, 'Lihat Data bulanan PNS', '2019-04-06 13:43:30', '127.0.0.1'),
	(950056, 1, 'Lihat Data Log Absensi', '2019-04-06 13:44:01', '127.0.0.1'),
	(950057, 1, 'Lihat Data Absensi harian pegawai', '2019-04-06 13:45:55', '127.0.0.1'),
	(950058, 1, 'Lihat Data bulanan PNS', '2019-04-06 13:46:49', '127.0.0.1'),
	(950059, 1, 'Lihat data hari libur', '2019-04-06 13:47:43', '127.0.0.1'),
	(950060, 1, 'Login Ke dalam Sistem Absensi', '2019-04-06 16:46:24', '127.0.0.1'),
	(950061, 1, 'Lihat Data Log Absensi', '2019-04-06 17:34:43', '127.0.0.1'),
	(950062, 1, 'Lihat Data Absensi harian pegawai', '2019-04-06 17:34:51', '127.0.0.1'),
	(950063, 1, 'Lihat jadwal pegawai', '2019-04-06 17:35:50', '127.0.0.1'),
	(950064, 1, 'atur jadwal pegawai dengan nip:1234', '2019-04-06 17:35:55', '127.0.0.1'),
	(950065, 1, 'Lihat Data Absensi harian pegawai', '2019-04-06 17:36:39', '127.0.0.1'),
	(950066, 1, 'Lihat Data bulanan PNS', '2019-04-06 17:36:43', '127.0.0.1'),
	(950067, 1, 'Login Ke dalam Sistem Absensi', '2019-04-12 11:11:29', '127.0.0.1'),
	(950068, 1, 'Lihat Data Log Absensi', '2019-04-12 11:13:30', '127.0.0.1'),
	(950069, 1, 'Lihat Data Mesin Absensi', '2019-04-12 11:30:25', '127.0.0.1'),
	(950070, 1, 'Lihat Data Pegawai Proyek', '2019-04-12 11:30:34', '127.0.0.1'),
	(950071, 1, 'Login Ke dalam Sistem Absensi', '2019-04-12 12:27:52', '127.0.0.1'),
	(950072, 1, 'Lihat Data Log Absensi', '2019-04-12 12:27:56', '127.0.0.1'),
	(950073, 1, 'Lihat Data Log Absensi', '2019-04-12 12:28:03', '127.0.0.1'),
	(950074, 1, 'Lihat Data Log Absensi', '2019-04-12 12:30:20', '127.0.0.1'),
	(950075, 1, 'Lihat Data Log Absensi', '2019-04-12 12:31:10', '127.0.0.1'),
	(950076, 1, 'Lihat Data Log Absensi', '2019-04-12 12:31:15', '127.0.0.1'),
	(950077, 1, 'Lihat Data Log Absensi', '2019-04-12 12:32:25', '127.0.0.1'),
	(950078, 1, 'Lihat Data Log Absensi', '2019-04-12 12:33:17', '127.0.0.1'),
	(950079, 1, 'Lihat Data Absensi harian pegawai', '2019-04-12 12:33:22', '127.0.0.1'),
	(950080, 1, 'Lihat Data Log Absensi', '2019-04-12 12:33:25', '127.0.0.1'),
	(950081, 1, 'Login Ke dalam Sistem Absensi', '2019-04-12 14:50:09', '127.0.0.1'),
	(950082, 1, 'Lihat Data Log Absensi', '2019-04-12 14:50:12', '127.0.0.1'),
	(950083, 1, 'Login Ke dalam Sistem Absensi', '2019-04-13 07:35:36', '127.0.0.1'),
	(950084, 1, 'Login Ke dalam Sistem Absensi', '2019-04-13 08:38:30', '127.0.0.1'),
	(950085, 1, 'Login Ke dalam Sistem Absensi', '2019-04-13 10:26:28', '127.0.0.1'),
	(950086, 1, 'Login Ke dalam Sistem Absensi', '2019-04-13 13:25:11', '127.0.0.1'),
	(950087, 1, 'Login Ke dalam Sistem Absensi', '2019-04-13 16:26:47', '127.0.0.1'),
	(950088, 1, 'Login Ke dalam Sistem Absensi', '2019-04-13 17:16:39', '127.0.0.1'),
	(950089, 1, 'Login Ke dalam Sistem Absensi', '2019-04-13 18:11:30', '127.0.0.1'),
	(950090, 1, 'Lihat Data Absensi harian pegawai', '2019-04-13 19:08:14', '127.0.0.1'),
	(950091, 1, 'Lihat Data bulanan PNS', '2019-04-13 19:08:17', '127.0.0.1'),
	(950092, 1, 'Lihat Data Log Absensi', '2019-04-13 19:09:30', '127.0.0.1'),
	(950093, 1, 'Lihat Data Absensi harian pegawai', '2019-04-13 19:09:39', '127.0.0.1'),
	(950094, 1, 'Lihat Data bulanan PNS', '2019-04-13 19:09:40', '127.0.0.1'),
	(950095, 1, 'Lihat Data Log Absensi', '2019-04-13 19:14:41', '127.0.0.1'),
	(950096, 1, 'Lihat Data Absensi harian pegawai', '2019-04-13 19:14:47', '127.0.0.1'),
	(950097, 1, 'Lihat Data Log Absensi', '2019-04-13 19:15:39', '127.0.0.1'),
	(950098, 1, 'Lihat Data bulanan PNS', '2019-04-13 19:15:43', '127.0.0.1');
/*!40000 ALTER TABLE `t_log_aktivitas` ENABLE KEYS */;

-- Dumping structure for table sefti.t_menu
DROP TABLE IF EXISTS `t_menu`;
CREATE TABLE IF NOT EXISTS `t_menu` (
  `menuid` int(3) NOT NULL AUTO_INCREMENT,
  `namamenu` varchar(64) NOT NULL,
  `deskripsi` text,
  `urutan` tinyint(3) unsigned DEFAULT NULL,
  `linkaction` varchar(128) DEFAULT NULL,
  `tgltambah` date DEFAULT NULL,
  `parentid` int(3) unsigned NOT NULL DEFAULT '0',
  `aktif` int(3) NOT NULL DEFAULT '1',
  PRIMARY KEY (`menuid`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table sefti.t_menu: ~25 rows (approximately)
DELETE FROM `t_menu`;
/*!40000 ALTER TABLE `t_menu` DISABLE KEYS */;
INSERT INTO `t_menu` (`menuid`, `namamenu`, `deskripsi`, `urutan`, `linkaction`, `tgltambah`, `parentid`, `aktif`) VALUES
	(1, 'Administrasi', '', 10, '', NULL, 0, 1),
	(2, 'Menu Backend', '', 1, '/utilitas/menu', NULL, 1, 1),
	(3, 'Group', '', 3, '/utilitas/group', NULL, 1, 1),
	(22, 'Pengguna', '', 4, '/utilitas/user', NULL, 1, 1),
	(23, 'Referensi', '', 1, '', NULL, 0, 1),
	(39, 'Data', '', 3, '', NULL, 0, 1),
	(40, 'Pegawai', '', 2, '/data/pegawai', NULL, 39, 1),
	(41, 'Mesin Absensi', '', 3, '/data/mesin_absensi', NULL, 39, 1),
	(42, 'Data Mesin Absensi Pegawai', '', 4, '/data/mesin_pegawai', NULL, 39, 1),
	(43, 'Kehadiran', '', 4, '', NULL, 0, 1),
	(45, 'Harian', '', 3, '/log/harian', NULL, 43, 1),
	(46, 'Bulanan', '', 7, '/log/bulanan', NULL, 43, 1),
	(47, 'Laporan', '', 5, '', NULL, 0, 1),
	(61, 'Tipe Jadwal', '', 6, '/referensi/tipe_jadwal', NULL, 23, 1),
	(67, 'Log Absensi ', '', 1, '/log/absensi', NULL, 43, 1),
	(68, 'Setting', '', 6, '', NULL, 0, 1),
	(69, 'Hari Libur', '', 1, '/setting/hari_libur', NULL, 68, 1),
	(74, 'Jadwal', '', 3, '/setting/jadwal', NULL, 68, 1),
	(83, 'Monitoring', '', 9, '', NULL, 0, 1),
	(86, '	Log Tap Absen', '', 3, '/monitoring/log_tap', NULL, 83, 0),
	(87, 'Aktivitas User', '', 4, '/monitoring/aktivitas_user', NULL, 83, 1),
	(102, 'Project', '', 1, '/data/project', NULL, 39, 1),
	(103, 'Jabatan', '', 2, '/referensi/daftar/1', NULL, 23, 1);
/*!40000 ALTER TABLE `t_menu` ENABLE KEYS */;

-- Dumping structure for table sefti.t_users
DROP TABLE IF EXISTS `t_users`;
CREATE TABLE IF NOT EXISTS `t_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) DEFAULT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned DEFAULT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `skpd_kewenangan` varchar(12) CHARACTER SET latin1 DEFAULT NULL,
  `nip` varchar(18) CHARACTER SET latin1 DEFAULT NULL,
  `foto` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `nip` (`nip`) USING BTREE,
  KEY `skpd_kewenangan` (`skpd_kewenangan`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table sefti.t_users: ~1 rows (approximately)
DELETE FROM `t_users`;
/*!40000 ALTER TABLE `t_users` DISABLE KEYS */;
INSERT INTO `t_users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `skpd_kewenangan`, `nip`, `foto`) VALUES
	(1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', '', NULL, NULL, NULL, 1268889823, 1555153885, 1, 'Admin', 'Utama', 'ADMIN', '028343', '', NULL, 'Untitled.png');
/*!40000 ALTER TABLE `t_users` ENABLE KEYS */;

-- Dumping structure for table sefti.t_users_groups
DROP TABLE IF EXISTS `t_users_groups`;
CREATE TABLE IF NOT EXISTS `t_users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`),
  CONSTRAINT `FK_t_users_groups_t_groups` FOREIGN KEY (`group_id`) REFERENCES `t_groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_t_users_groups_t_users` FOREIGN KEY (`user_id`) REFERENCES `t_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table sefti.t_users_groups: ~0 rows (approximately)
DELETE FROM `t_users_groups`;
/*!40000 ALTER TABLE `t_users_groups` DISABLE KEYS */;
INSERT INTO `t_users_groups` (`id`, `user_id`, `group_id`) VALUES
	(3, 1, 1);
/*!40000 ALTER TABLE `t_users_groups` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
