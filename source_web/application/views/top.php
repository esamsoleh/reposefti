<?php
if($this->ion_auth->user()->row()->foto != '')
{
	$foto = $this->ion_auth->user()->row()->foto;
}
else
{
	$foto = 'no-image.png';
}
?>
<header class="main-header">
    <!-- Logo -->
    <a href="/" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Aplikasi PPAS</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
		<!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning"><div id="total_notifikasi"></div></span>
            </a>
            <ul class="dropdown-menu">
              <li>
                <ul class="menu">
                  
					<div id="rincian_notifikasi">
												
					</div>
                  
                </ul>
              </li>
              
            </ul>
          </li>
		  <script src="/assets/components/jquery/dist/jquery.min.js"></script>
							<script>
							$(document).ready(function() {
								$.ajax({
										url:'/notifikasi/total',	
									}).done(function(html) {
										$("#total_notifikasi").append(html);
									});
								$.ajax({
										url:'/notifikasi/item',	
									}).done(function(html) {
										$("#rincian_notifikasi").append(html);
									});
								//setInterval(timingLoad, 3000);
								function timingLoad() {
									$("#total_notifikasi").empty();
									$.ajax({
										url:'/notifikasi/total',
										
									}).done(function(html) {
										$("#total_notifikasi").append(html);
									});
									
									$("#rincian_notifikasi").empty();
									$.ajax({
										url:'/notifikasi/item',	
									}).done(function(html) {
										$("#rincian_notifikasi").append(html);
									});
								}
							}); 
							</script>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="/assets/foto/<?=$foto;?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?=$this->nama_user;?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="/assets/foto/<?=$foto;?>" class="img-circle" alt="User Image">
				<!--
                <p>
                  Alexander Pierce - Web Developer
                  <small>Member since Nov. 2012</small>
                </p>
				-->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="/dashboard/profil" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="/auth/logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          
        </ul>
      </div>
    </nav>
  </header>