<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
  	
	function get_akses($user_groups,$linkaction)
	{
		$CI =& get_instance();
		$hak_akses = array(
			'ad' => 0,
			'up' => 0,
			'de' => 0,
			'vi' => 0,
			'pr' =>0
		);
		foreach($user_groups as $grup)
		{
			$s = "select a.*
				from t_groupmenu a 
				left join t_menu b on (a.menuid = b.menuid)
				where a.groupid = '$grup[id]'
				and b.linkaction = '$linkaction'
			";
			//	echo $s;
			$query = $CI->db->query($s);
			if($query->num_rows() > 0)
			{
				$hak = $query->row_array();
			}
			else
			{
				$hak = array(
					'ad' => 0,
					'up' => 0,
					'de' => 0,
					'vi' => 0,
					'pr' =>0
				);
			}
			(($hak_akses['ad']==0)?$hak_akses['ad'] = ((isset($hak['ad']) && $hak['ad'] == NULL)?$hak['ad'] = 0:$hak['ad']):$hak_akses['ad'] = $hak_akses['ad']);
			(($hak_akses['up']==0)?$hak_akses['up'] = ((isset($hak['up']) && $hak['up'] == NULL)?$hak['up'] = 0:$hak['up']):$hak_akses['up'] = $hak_akses['up']);
			(($hak_akses['de']==0)?$hak_akses['de'] = ((isset($hak['de']) && $hak['de'] == NULL)?$hak['de'] = 0:$hak['de']):$hak_akses['de'] = $hak_akses['de']);
			(($hak_akses['vi']==0)?$hak_akses['vi'] = ((isset($hak['vi']) && $hak['vi'] == NULL)?$hak['vi'] = 0:$hak['vi']):$hak_akses['vi'] = $hak_akses['vi']);
			(($hak_akses['pr']==0)?$hak_akses['pr'] = ((isset($hak['pr']) && $hak['pr'] == NULL)?$hak['pr'] = 0:$hak['pr']):$hak_akses['pr'] = $hak_akses['pr']);
		}
		
		//print_r($hak_akses);
		return $hak_akses;
	}
	
	function get_langkah($jenis_layanan,$nip)
	{
		$CI =& get_instance();
		$select = "select LANGKAH 
					FROM referensi_langkah
					WHERE ID_JENIS_LAYANAN = '$jenis_layanan'
					AND PELAKSANA = (select concat(kuntp,kunkom,kununit,rincunit1,rincunit2,rincunit3) from d_jakhir where NIP = '$nip' and jnsjab = '1')
					";
	//	echo $select;
		$query = $CI->db->query($select);
		if($query->num_rows() > 0)
		{
			$result = $query->row_array();
			$result = $result['LANGKAH'];
		}
		else
		{
			$result = NULL;
		}
		return $result;
	}
	
	function get_jml_langkah($jenis_layanan)
	{
		$CI =& get_instance();
		$select = "select count(ID) AS jml_langkah
					FROM referensi_langkah
					WHERE ID_JENIS_LAYANAN = '$jenis_layanan'
					";
		$query = $CI->db->query($select);
		if($query->num_rows() > 0)
		{
			$result = $query->row_array();
			$result = $result['jml_langkah'];
		}
		else
		{
			$result = 0;
		}
		return $result;
	}
	
?>