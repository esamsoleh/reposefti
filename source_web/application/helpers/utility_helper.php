<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 	
	
	function input_log($userid,$aktivitas)
	{
		$CI =& get_instance();
		$datetime = date('Y-m-d H:i:s');
		$ip = $_SERVER['REMOTE_ADDR'];
		$sql = "insert IGNORE into t_log_aktivitas values('','$userid','$aktivitas','$datetime','$ip')";
		//echo $sql;
		$result = @$CI->db->query($sql);
		//print_r($result);
	}
	
	function make_captcha()
	{
		$CI =& get_instance();
		//$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$length=6;
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		$CI->session->set_userdata('my_captcha', $randomString);

        $width      = 100;
        $height     = 25;
        $image      = imagecreatetruecolor($width,$height);
        $text_color = imagecolorallocate($image, 130, 130,130);
        $bg_color   = imagecolorallocate($image, 190, 190,190);
        
        imagefilledrectangle($image, 0, 0, $width, $height,$bg_color);        
        imagestring($image, 5, 16, 4, $randomString,$text_color);

        ob_start();
        imagejpeg($image);
        $jpg = ob_get_clean();
        return "data:image/jpeg;base64,". base64_encode($jpg);
	}
	
	function get_foto()
	{
		$CI =& get_instance();
		$userid = $CI->ion_auth->user()->row()->id;
		$s = "select file_foto from d_profil where userid = '$userid' ";
		$r = $CI->db->query($s)->row_array();
		if($r['file_foto'] != NULL && $r['file_foto'] != '')
		{
			$r = $r['file_foto'];
		}
		else
		{
			$r = 'no-image.png';
		}
		return $r;
	}
	
	function PHPtoOrgChart(array $arr,$title='') 
	{
        echo '<table>';
        $size=count($arr);
        if($title!='') 
		{
            //head

            echo '<tr>';
            echo '<td colspan="'.($size*2).'">';
            echo '<div class="charttext">'.$title.'</div>';
            echo '</td>';
            echo '</tr>';
            //head line


            echo '<tr>';
            echo '<td colspan="'.($size*2).'">';
            echo '<table><tr><th class="right width-50"></th><th class="width-50"></th></tr></table>';
            echo '</td>';
            echo '</tr>';

            //line
            if($size>=2)
			{
				$tdWidth=((100)/($size*2));

				echo '<tr>';
				echo '<th class="right" width="'.$tdWidth.'%"></th>';
					echo '<th class="top" width="'.$tdWidth.'%"></th>';
					for($j=1; $j<$size-1; $j++) 
					{
						echo '<th class="right top" width="'.$tdWidth.'%"></th>';
						echo '<th class=" top" width="'.$tdWidth.'%"></th>';
					}
					echo '<th class="right top" width="'.$tdWidth.'%"></th>';
				echo '<th width="'.$tdWidth.'%"></th>';
				echo '</tr>';
            }
        }
        //
        echo '<tr>';
        foreach($arr as $key=>$value) 
		{
            echo '<td colspan="2">';
            if(is_array($value)) 
			{
                PHPtoOrgChart($value,$key);
            } 
			else 
			{
                echo '<div class="charttext">'.$value.'</div>';
            }
            echo '</td>';
        }
        echo '</tr>';
        //
        echo '</table>';
    }
	
	function chartOrganisasi(array $arr,$title='',$level=0,$kode_awal = '') 
	{
		$kode_awal .= $title;
	//	echo 'title: '.$title.'</br>';
	//	echo 'kode awal: '.$kode_awal.'</br>';
		echo '<table>';
        $size=count($arr);
        if($title!='') 
		{
            //head
			//$level = $level + 2;
            echo '<tr>';
            echo '<td colspan="'.($size*2).'">';
            echo '<div class="charttext">'.get_nama_jabatan($title,$level,$kode_awal).'</div>';
            echo '</td>';
            echo '</tr>';
            //head line


            echo '<tr>';
            echo '<td colspan="'.($size*2).'">';
            echo '<table><tr><th class="right width-50"></th><th class="width-50"></th></tr></table>';
            echo '</td>';
            echo '</tr>';

            //line
            if($size>=2)
			{
				$tdWidth=((100)/($size*2));

				echo '<tr>';
				echo '<th class="right" width="'.$tdWidth.'%"></th>';
					echo '<th class="top" width="'.$tdWidth.'%"></th>';
					for($j=1; $j<$size-1; $j++) 
					{
						echo '<th class="right top" width="'.$tdWidth.'%"></th>';
						echo '<th class=" top" width="'.$tdWidth.'%"></th>';
					}
					echo '<th class="right top" width="'.$tdWidth.'%"></th>';
				echo '<th width="'.$tdWidth.'%"></th>';
				echo '</tr>';
            }
        }
        //
		$level = $level + 2;
	//	echo 'level: '.$level.'</br>';
        echo '<tr>';
        foreach($arr as $key=>$value) 
		{
            echo '<td colspan="2">';
            if(is_array($value)) 
			{
                chartOrganisasi($value,$key,$level,$kode_awal);
            } 
			else 
			{
				$kode_awal = substr($value,0,8);
                echo '<div class="charttext">'.get_nama_jabatan($value,$level,$kode_awal).'</div>';
            }
            echo '</td>';
        }
        echo '</tr>';
        //
        echo '</table>';
    }
	
	function get_nama_jabatan($kode,$level,$kode_awal)
	{
		$CI =& get_instance();
		$levelmin = $level+1;
		$jmlmin = 10-$level;
		$min = '';
		for($j=1;$j<=$jmlmin;$j++)
		{
			$min .='0';
		}
		$s = "select a.*,
					b.*,
					CONCAT(IF(c.gelar_depan<>'',CONCAT(c.gelar_depan,' '),''),c.nama_depan,c.nama_belakang,IF(c.gelar_belakang<>'',CONCAT(' ',c.gelar_belakang),'')) as nama_lengkap
			from ref_unit_kerja b  
			left join d_pejabat a on(b.kode_unit_kerja = a.kode_unit_kerja)
			left join d_profil c on(a.userid = c.userid)
			where 1=1
			and mid(b.kode_unit_kerja,1,$level) = '$kode_awal'
			and mid(b.kode_unit_kerja,$levelmin,$jmlmin) = '$min'
		";
		//echo $s;
		$q = $CI->db->query($s);
		$row = $q->row_array();
		$result = '<table>';
		$result .='<tr><td colspan="2">'.$row['nama_unit_kerja'].'</td></tr>';
		if(isset($row['nama_jabatan']) && $row['nama_jabatan'] !='')
		{
			$result .='<tr><td><a href="/personil/edit_jabatan/'.$row['kode_unit_kerja'].'">'.$row['nama_jabatan'].':'.$row['nama_lengkap'].'</a></td></tr>';
		}
		
		$result .='</table>';
		return $result;
	}
	
	function random_password( $length = 8 ) 
	{
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		$password = substr( str_shuffle( $chars ), 0, $length );
		return $password;
	}	
	
	function swap3(&$x, &$y) 
	{
		$tmp=$x;
		$x=$y;
		$y=$tmp;
	}
	
	function jarak($p1, $p2) 
	{ 
		return ($p1->x - $p2->x)*($p1->x - $p2->x) + ($p1->y - $p2->y)*($p1->y - $p2->y); 
	} 
	
	function orientation($p, $q, $r) 
	{ 
		$val = ($q->y - $p->y) * ($r->x - $q->x) - ($q->x - $p->x) * ($r->y - $q->y); 
	  
		if ($val == 0) return 0;  // colinear 
		return ($val > 0)? 1: 2; // clockwise or counterclock wise 
	} 
?>