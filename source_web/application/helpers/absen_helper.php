<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 	
	
	function get_waktu_masuk($nip,$tanggal)
	{
		$CI =& get_instance();
		$tanggal = date('Y-m-d',strtotime($tanggal));
		$s = "select tanggal 
				from absensi_log a
				left join mesin_absensi_user b on(a.pin = b.pin and a.serial_number = b.serial_number)
				where b.nip = '$nip' 
				and date(a.tanggal) = '$tanggal' 
				and HOUR(a.tanggal) = (SELECT MIN(HOUR(tanggal)) from absensi_log where nip=a.nip and pin = a.pin and serial_number = a.serial_number and date(tanggal) = date(a.tanggal))
				and minute(a.tanggal) = (SELECT MIN(minute(tanggal)) from absensi_log where nip=a.nip and pin = a.pin and serial_number = a.serial_number and date(tanggal) = date(a.tanggal) 
											and hour(tanggal) = (SELECT MIN(HOUR(tanggal)) from absensi_log where nip=a.nip and pin = a.pin and serial_number = a.serial_number and date(tanggal) = date(a.tanggal)))
				";
		//echo $s;
		$q = $CI->db->query($s);
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			$res = date('d-M-Y H:i:s',strtotime($r->tanggal));
		}
		else
		{
			$res = '';
		}
		return $res;
	}
	
	function get_waktu_pulang($nip,$tanggal)
	{
		$CI =& get_instance();
		$tanggal = date('Y-m-d',strtotime($tanggal));
		$s = "select tanggal 
				from absensi_log a
				left join mesin_absensi_user b on(a.pin = b.pin and a.serial_number = b.serial_number)
				where b.nip = '$nip' 
				and date(a.tanggal) = '$tanggal' 
				and HOUR(a.tanggal) = (SELECT MAX(HOUR(tanggal)) from absensi_log where nip=a.nip and pin = a.pin and serial_number = a.serial_number and date(tanggal) = date(a.tanggal))
				and minute(a.tanggal) = (SELECT MAX(minute(tanggal)) from absensi_log where nip=a.nip and pin = a.pin and serial_number = a.serial_number and date(tanggal) = date(a.tanggal) 
											and hour(tanggal) = (SELECT MAX(HOUR(tanggal)) from absensi_log where nip=a.nip and pin = a.pin and serial_number = a.serial_number and date(tanggal) = date(a.tanggal)))
				";
		$q = $CI->db->query($s);
		//echo $s;
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			$res = date('d-M-Y H:i:s',strtotime($r->tanggal));
		}
		else
		{
			$res = '';
		}
		return $res;
	}
	
	function get_tunjangan($nip)
	{
		$CI =& get_instance();
		$sp = "select * from referensi_proporsi_tunjangan";
		$qp = $CI->db->query($sp);
		$proporsi = array();
		foreach($qp->result() as $rp)
		{
			$proporsi[$rp->status] = $rp->proporsi;
		}
		$s = "select jnsjab,kstatus from d_pegawai where nip = '$nip' ";
	//	echo $s;
		$jnsjab = $CI->db->query($s)->row()->jnsjab;
		$kstatus = $CI->db->query($s)->row()->kstatus;
		switch($jnsjab)
		{
			case 1:
				$s1 = "select b.tunjangan 
						from d_pegawai a 
						left join referensi_unit_kerja b on(a.kunker = b.kunker)
					where a.nip = '$nip'
						";
				
				$q1 = $CI->db->query($s1);
				if($q1->num_rows() > 0)
				{
					$tunjangan = $q1->row()->tunjangan;
					$result['total_tpp'] = $tunjangan;
					$result['tpp_statis'] = $tunjangan*($proporsi['statis']/100);
					$result['tpp_dinamis'] = $tunjangan*($proporsi['dinamis']/100);					
				}
				else
				{
					$result['total_tpp'] = 0;
					$result['tpp_statis'] = 0;
					$result['tpp_dinamis'] = 0;	
				}
			break;
			
			case 2:
				$s1 = "select b.tunjangan 
						from d_pegawai a 
						left join referensi_jabatan_fungsional_tertentu b on(a.kjab = b.KJAB)
					where a.nip = '$nip'
						";
				$q1 = $CI->db->query($s1);
				if($q1->num_rows() > 0)
				{
					$tunjangan = $q1->row()->tunjangan;
					$result['total_tpp'] = $tunjangan;
					$result['tpp_statis'] = $tunjangan*($proporsi['statis']/100);
					$result['tpp_dinamis'] = $tunjangan*($proporsi['dinamis']/100);					
				}
				else
				{
					$result['total_tpp'] = 0;
					$result['tpp_statis'] = 0;
					$result['tpp_dinamis'] = 0;	
				}
			break;
			
			case 3:
			
			break;
			
			case 4:
				$s1 = "select a.tunjangan 
						from referensi_tunjangan_jfu a 
					where a.status = '$kstatus'
						";
				$q1 = $CI->db->query($s1);
				if($q1->num_rows() > 0)
				{
					$tunjangan = $q1->row()->tunjangan;
					$result['total_tpp'] = $tunjangan;
					$result['tpp_statis'] = $tunjangan*($proporsi['statis']/100);
					$result['tpp_dinamis'] = $tunjangan*($proporsi['dinamis']/100);					
				}
				else
				{
					$result['total_tpp'] = 0;
					$result['tpp_statis'] = 0;
					$result['tpp_dinamis'] = 0;	
				}
			break;
			default:
					$result['total_tpp'] = 0;
					$result['tpp_statis'] = 0;
					$result['tpp_dinamis'] = 0;	
			break;
		}
		return $result;
	}
	
	function get_tunjangan_tkk($nik)
	{
		$CI =& get_instance();
		$sp = "select * from referensi_proporsi_tunjangan";
		$qp = $CI->db->query($sp);
		$proporsi = array();
		foreach($qp->result() as $rp)
		{
			$proporsi[$rp->status] = $rp->proporsi;
		}
		
		$s1 = "select a.tunjangan 
						from referensi_tunjangan_jfu a 
					where a.status = '4'
						";
		$q1 = $CI->db->query($s1);
		if($q1->num_rows() > 0)
		{
			$tunjangan = $q1->row()->tunjangan;
			$result['total_tpp'] = $tunjangan;
			$result['tpp_statis'] = $tunjangan*($proporsi['statis']/100);
			$result['tpp_dinamis'] = $tunjangan*($proporsi['dinamis']/100);					
		}
		else
		{
			$result['total_tpp'] = 0;
			$result['tpp_statis'] = 0;
			$result['tpp_dinamis'] = 0;	
		}
			
		return $result;
	}
	
	function data_pegawai($nip)
{
	$CI =& get_instance();
	$s = "select a.nama,
				c.nama as nama_jabatan
			from d_pegawai a 
			left join ref_jabatan c on(a.id_jabatan = c.id)
			where a.nip = '$nip' ";
//	echo $s;
	$q = $CI->db->query($s);
	if($q->num_rows() > 0)
	{
		$r = $q->row();
		$res['nama'] = $r->nama;
		$res['nama_jabatan'] = $r->nama_jabatan;
		/* $res['eselon'] = $r->eselon;
		$res['golongan'] = $r->golongan;
		$res['pangkat'] = $r->pangkat;
		$res['unit_kerja'] = $r->skpd; */
	}
	else
	{
		$s1 = "select a.nama,
					CASE KUNKOM
					   WHEN 01 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,4) = concat(kuntp,kunkom) and mid(kunker,3,8) = '00000000' )
						WHEN 10 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' )
						WHEN 11 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' ) 
						ELSE (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,4) = concat(kuntp,kunkom) and mid(kunker,5,8) = '00000000' )
					END as skpd
				from d_tkk a
				where nik = '$nip' ";
		$q1 = $CI->db->query($s1);
		if($q1->num_rows() > 0)
		{
			$r = $q1->row();
			$res['nama'] = $r->nama;
			$res['jabatan'] = '';
			$res['eselon'] = '';
			$res['golongan'] = '';
			$res['pangkat'] = '';
			$res['unit_kerja'] = $r->skpd;
		}
		else
		{
			$res['nama'] = '';
			$res['jabatan'] = '';
			$res['eselon'] = '';
			$res['golongan'] = '';
			$res['pangkat'] = '';
			$res['unit_kerja'] = '';
		}
	}
	return $res;
}

	
?>