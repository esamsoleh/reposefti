<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 	
	function make_menu($apps,$s_access)
	{
		
		$CI =& get_instance();
		$menunav = '';
		$menu = $CI->session->userdata('menu'); 
		$query_parent = $CI->db->query("select a.* 
								from t_menu a
								left join t_groupmenu b
								on (a.menuid = b.menuid)
								where a.parentid = 0 
								and b.groupid = $s_access
								and a.aktif = 1
								order by a.urutan asc ");
		foreach($query_parent->result_array() as $parent_menu)
		{
			$active = (($apps == $parent_menu['namamenu']) ? 'active' : '');
			$menunav .= '<li class="dropdown '.$active.' ">';
			$menunav .= '<a role="menu" data-toggle="dropdown" class="dropdown-toggle" data-target="#" href="#">'.$parent_menu['namamenu'].' <span class="caret"></span></a>';
			$menunav .=	formatTree($parent_menu['menuid'],$s_access);	
			$menunav .= '</li>';
			$active = '';
		}
		echo $menunav;
	}
	
	function formatTree($parentid, $s_access)
	{
		$menunav = '';
		$CI =& get_instance();
		
		$sql = "select a.* 
								from t_menu a
								left join t_groupmenu b
								on (a.menuid = b.menuid)
								where a.parentid = ".$parentid." 
								and b.groupid = $s_access
								and a.aktif = 1
								order by a.urutan asc";
		
		$query = $CI->db->query($sql);
		if($query->num_rows() > 0)
		{
			$menunav .= "<ul class='dropdown-menu'>";
			foreach($query->result_array() as $item)
			{
				if(toggle($item['menuid'],$s_access)>0)
				{
					$menunav .= '<li class="dropdown-menu_sub">';
					$menunav .= '<a href="#">'.$item['namamenu'].'</a>';
				}
				else
				{
					$menunav .= '<li>';
					$menunav .= '<a href="'.$item['linkaction'].'">'.$item['namamenu'].'</a>';
				}
				$menunav.= formatTree($item['menuid'],$s_access);
				$menunav.= "</li>";
			}
			$menunav.= "</ul>";
		}
	  return $menunav;
    }
	
	function toggle($parentid, $s_access)
	{
		$CI =& get_instance();
		
		$sql = "select a.* 
								from t_menu a
								left join t_groupmenu b
								on (a.menuid = b.menuid)
								where a.parentid = ".$parentid." 
								and b.groupid = $s_access
								AND a.aktif = 1
								order by a.urutan";
	
		$query = $CI->db->query($sql);
		return $query->num_rows();
    }

	function make_menu_front($apps)
	{
		
		$CI =& get_instance();
		$menunav = '';
		$query_parent = $CI->db->query("select a.* 
								from t_menu_front a
								where a.parentid = 0 
								and a.aktif = 1
								order by a.urutan asc ");
		foreach($query_parent->result_array() as $parent_menu)
		{
			if(toggle_front($parent_menu['menuid'])>0)
			{
				$active = (($apps == $parent_menu['namamenu']) ? 'active' : '');
				$menunav .= '<li class="dropdown '.$active.' ">';
				$menunav .= '<a role="menu" data-toggle="dropdown" class="dropdown-toggle" data-target="#" href="#">'.$parent_menu['namamenu'].' <span class="caret"></span></a>';
				$menunav .=	formatTree_front($parent_menu['menuid']);	
				$menunav .= '</li>';
				$active = '';
			}
			else
			{
				$active = (($apps == $parent_menu['namamenu']) ? 'active' : '');
				$menunav .= '<li '.$active.' ">';
				$menunav .= '<a href="'.$parent_menu['linkaction'].'">'.$parent_menu['namamenu'].'</a>';
				$menunav .= '</li>';
				$active = '';

			}
				
		}
		echo $menunav;
	}
	
	function formatTree_front($parentid)
	{
		$menunav = '';
		$CI =& get_instance();
		
		$sql = "select a.* 
								from t_menu_front a
								where a.parentid = ".$parentid." 
								and a.aktif = 1
								order by a.urutan asc";
		
		$query = $CI->db->query($sql);
		if($query->num_rows() > 0)
		{
			$menunav .= "<ul class='dropdown-menu'>";
			foreach($query->result_array() as $item)
			{
				if(toggle_front($item['menuid'])>0)
				{
					$menunav .= '<li class="dropdown-menu_sub">';
					$menunav .= '<a href="#">'.$item['namamenu'].'</a>';
				}
				else
				{
					$menunav .= '<li>';
					$menunav .= '<a href="'.$item['linkaction'].'">'.$item['namamenu'].'</a>';
				}
				$menunav.= formatTree_front($item['menuid']);
				$menunav.= "</li>";
			}
			$menunav.= "</ul>";
		}
	  return $menunav;
    }
	
	function toggle_front($parentid)
	{
		$CI =& get_instance();
		
		$sql = "select a.* 
								from t_menu_front a
								where a.parentid = ".$parentid." 
								AND a.aktif = 1
								order by a.urutan";
	
		$query = $CI->db->query($sql);
		return $query->num_rows();
    }

	function make_menu_vertikal($apps,$s_access)
	{
		$CI =& get_instance();
		$aplikasi = $CI->uri->segment(2);
		$menunav = '';
		$query_parent = $CI->db->query("select a.* 
								from t_menu a
								left join t_groupmenu b
								on (a.menuid = b.menuid)
								where a.parentid = 0 
								and b.groupid = $s_access
								and a.aktif = 1
								order by a.urutan asc ");
		foreach($query_parent->result_array() as $parent_menu)
		{
			$sql = "select a.* 
								from t_menu a
								left join t_groupmenu b
								on (a.menuid = b.menuid)
								where a.parentid = ".$parent_menu['menuid']." 
								and b.groupid = $s_access
								and a.aktif = 1
								order by a.urutan asc";
		
			$query = $CI->db->query($sql);
			if($query->num_rows() > 0)
			{
				$active = (($apps == $parent_menu['namamenu']) ? 'active' : '');
				$menunav .= '<li class="'.$active.' treeview">';
				$menunav .= '<a href="#"><i class="fa fa-desktop"></i> <span class="title">'.$parent_menu['namamenu'].'</span><i class="icon-arrow"></i></a>';
				$menunav .=	formatTreeVertikal($parent_menu['menuid'],$s_access);	
				$menunav .= '</li>';
			}
			else
			{
				$menunav .= '<li class="'.$active.'">';
				$menunav .= '<a href="'.base_url().$parent_menu['linkaction'].'"><i class="fa fa-desktop"></i> <span class="title">'.$parent_menu['namamenu'].'</span><i class="icon-arrow"></i></a>';
				$menunav .= '</li>';
			}
			
		}
		echo $menunav;
	}
	
	function formatTreeVertikal($parentid, $s_access)
	{
		$menunav = '';
		$CI =& get_instance();
		
		$sql = "select a.* 
								from t_menu a
								left join t_groupmenu b
								on (a.menuid = b.menuid)
								where a.parentid = ".$parentid." 
								and b.groupid = $s_access
								and a.aktif = 1
								order by a.urutan asc";
		
		$query = $CI->db->query($sql);
		if($query->num_rows() > 0)
		{
			$menunav .= '<ul class="treeview-menu" >';
			foreach($query->result_array() as $item)
			{
				if(toggle_vertikal($item['menuid'],$s_access)>0)
				{
					$active = (($parentid == $item['parentid'])? 'active' : ''); 
					$menunav .= '<li class="treeview ">';
					$menunav .= '<a href="#"><i class="fa fa-circle-o"></i>'.$item['namamenu'].'</a>';
				}
				else
				{
					/* $menu = explode('/',$item['linkaction']);
					$menu = ((isset($menu) && count($menu) > 1)?$menu=$menu[1]:$menu='');
					$active = (($apps == $menu) ? 'active' : ''); */
					$active = (($parentid == $item['parentid'])? 'active' : ''); 
				//	$active = 'active';
					$menunav .= '<li class="'.$active.'">';
					$menunav .= '<a href="'.$item['linkaction'].'" ><i class="fa fa-circle-o"></i>'.$item['namamenu'].'</a>';
				}
				$menunav.= formatTreeVertikal($item['menuid'],$s_access);
				$menunav.= "</li>";
			}
			$menunav.= "</ul>";
		}
	  return $menunav;
    }
	
	function toggle_vertikal($parentid, $s_access)
	{
		$CI =& get_instance();
		
		$sql = "select a.* 
								from t_menu a
								left join t_groupmenu b
								on (a.menuid = b.menuid)
								where a.parentid = ".$parentid." 
								and b.groupid = $s_access
								AND a.aktif = 1
								order by a.urutan";
	
		$query = $CI->db->query($sql);
		return $query->num_rows();
    }


	
?>