<?php $this->load->view('header');?>
 
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('top');?>
  <!-- Left side column. contains the logo and sidebar -->
  
  <?php $this->load->view('sidebar');?>
 
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

     <!-- Main content -->
    <section class="content" id="content-section">
      <!-- Small boxes (Stat box) -->
      <div class="row">
		<div class="col-md-12">
          <!-- BAR CHART -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <i class="fa fa-bar-chart-o"></i>

              <h3 class="box-title">Project <?=$proyek[$n]['nama'];?></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
							<div class="row">
									<label  class="col-md-2 control-label">Proyek</label>
								<div class="col-md-4">
									<select class="form-control" name="id_place" id="id_place" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
										<?php
										foreach($proyek as $proy)
										{
											echo '<option value="/dashboard/index/'.$proy['id_place'].'" '.(($n==$proy['id_place'])?'selected':'').'>'.$proy['nama'].'</option>';
										}
										?>
									</select>
								</div>
									<label  class="col-md-2 control-label">Lantai</label>
								<div class="col-md-4">
									<select class="form-control" name="id_place" id="id_place" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
										<?php
										foreach($floor as $flo)
										{
											echo '<option value="/dashboard/index/'.$proy['id_place'].'/'.$flo['id_floor'].'" '.(($nf==$flo['id_floor'])?'selected':'').'>'.$flo['nama'].'</option>';
										}
										?>
									</select>
								</div>
							</div>
							<div class="row">
								<div class="col-md-3">
									<div class="checkbox">
									<label><input type="checkbox" name="denah" id="denah" value="denah" checked>Denah</label>
									</div>
								</div>
								<div class="col-md-3">
									<div class="checkbox">
									<label><input type="checkbox" name="geofence" id="geofence" value="geofence" checked>Geofence</label>
									</div>
								</div>
								<div class="col-md-3">
									<div class="checkbox">
									<label><input type="checkbox" name="beacon" id="beacon" value="Beacon" checked>Beacon</label>
									</div>
								</div>
							</div>
							</br>
				<div id="peta-section">
					<div id="peta"></div>
				</div>
            </div>
            <!-- /.box-body-->
          </div>
          <!-- /.box -->
        </div>
		
		
		</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
 
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('javascript');?>

<style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #peta {
        height: 800px;
      }
	  #peta_floor {
        height: 800px;
      }
    </style>
	<?php		
		if(isset($floor[$nf]['anchors']) && $floor[$nf]['anchors'] != 'null')
		{
			$lat = 0;
			$lng = 0;
			$i=0;
			$floorCoordinate = json_decode($floor[$nf]['anchors'],TRUE);
			
			foreach($floorCoordinate as $floorco)
			{
				$lat = $lat+$floorco['lat'];
				$lng = $lng+$floorco['lng'];
				$i++;
			}
			$lat_baru = $lat/$i;
			$lng_baru = $lng/$i;
		}
	?>
	<script>
	//map pertama
	var markerPosisi = [];
	var marker = [];
	var circle;
	var geojsonLayer;
	//var markers = L.markerClusterGroup(); 
	var mymap = L.map('peta').setView([<?=$lat_baru;?>, <?=$lng_baru;?>], 20);
	L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
		attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		maxZoom: 30,
		id: 'mapbox.streets',
		accessToken: 'pk.eyJ1Ijoic2Ftc29sZWgiLCJhIjoiY2p2a2V1cTU2MG5jdzN5bWw2MDBrODByaSJ9.eLmd2Zo6T_9pt3QJoFDXdw'
	}).addTo(mymap);	
	document.getElementById('peta').style.display = 'block';
	<?php
		$denah = base_url().'denah/'.$floor[$nf]['file_geojson'];
	?>
	
	//	geojsonLayer = new L.GeoJSON.AJAX("<?=$denah;?>");       
	//	geojsonLayer.addTo(mymap);
	
	var kmlLayer = new L.KML("<?=$denah;?>", {async: true});
	//var kmlLayer = new L.KML("<?=base_url();?>denah/doc.kml", {async: true});
	kmlLayer.on("loaded", function(e) { 
        mymap.fitBounds(e.target.getBounds());
    });                                       
    mymap.addLayer(kmlLayer);
	
	mymap.invalidateSize();
	<?php
		$i=0;
		if(isset($lokasi_input))
		{
			foreach($lokasi_input as $data_lok)
			{
				?>
					var inputIcon = L.icon({
						iconUrl: '<?=base_url();?>/assets/ikon/circle.png',
						iconSize: [10, 10],
					});
					marker[<?=$i;?>] = L.marker([<?=$data_lok['lat'];?>, <?=$data_lok['lng'];?>],{icon: inputIcon}).addTo(mymap);
				<?php
				$i++;
			}
		}
	?>
	<?php
		if(isset($posisi_orang) && $posisi_orang != '' && $posisi_orang != NULL )
		{
			$npos = 0;
			foreach($posisi_orang as $pos)
			{
				?>
				var myIcon_<?=$npos;?> = L.icon({
					iconUrl: '<?=base_url();?>/assets/ikon/<?=$pos['ikon'];?>',
					iconSize: [60, 60],
				});
				markerPosisi[<?=$npos;?>] = L.marker([<?=$pos['lat'];?>, <?=$pos['lng'];?>],{icon: myIcon_<?=$npos;?>}).addTo(mymap).bindPopup("<button onclick=\"\">Click</button><?=$pos['nip'];?> - <?=$pos['nama'];?>");
				
				<?php
				$npos++;
			}
		}
	?>
		
		<?php		
		if(isset($geofence[$n]['area']))
		{
			$areaCoordinate = json_decode($geofence[$n]['area'],TRUE);
			?>
			var circle = L.circle([<?=$areaCoordinate['lat'];?>, <?=$areaCoordinate['lng'];?>], {
				color: 'red',
				fillColor: '#f03',
				fillOpacity: 0.5,
				radius: <?=$geofence[$n]['radius'];?>
			}).addTo(mymap);
			<?php
		}
		?>
	
			
		window.addEventListener("load", function() { updatePoints(); });
		
		mymap.on('zoomend', function() {
			mymap.invalidateSize(true);
			var sizezoom = mymap.getZoom();
			//alert(sizezoom);
			mymap.setZoom(sizezoom);
		});
		  function getMarkers(planes) 
		  {
			for (var i = 0; i < planes.length; i++) 
			{
				
				mymap.removeLayer(markerPosisi[i]);
				var myIcon = L.icon({
					iconUrl: '<?=base_url();?>/assets/ikon/'+planes[i].ikon,
					iconSize: [60, 60],
				});
			
			  markerPosisi[i] = L.marker([planes[i].lat,planes[i].lng],{icon: myIcon}).bindPopup(planes[i].nip).addTo(mymap);
			  
			}
			
		  }

		 function updatePoints() 
		 {
			$.post("/dashboard/get_posisi_orang",
			{
							'id_place': '<?=$n;?>', 
							'id_floor': '<?=$nf;?>', 
			}).done(function(planes){ 
				//alert(planes);
				if(planes != '')
				{
					getMarkers(JSON.parse(planes)); 
				}
				
			});
			setTimeout(function(){ updatePoints(); }, <?=$interval_refresh_dashboard;?>000);
		 }

	</script>	
		
<script type="text/javascript">
	$(document).ready(function () {
		var res = 1;
		if(res = 1)
		{
			//setInterval(loadContent, <?=$interval_refresh_dashboard;?>000);
		}
		
			function loadContent() {
			//	$("#content-section").empty();
				$("#peta-section").empty();
					$.ajax({
						url:'/dashboard/section',
						method:'post',
						data: {
							'id_place': '<?=$n;?>', 
							'id_floor': '<?=$nf;?>', 
						}
						}).done(function(html) {
						//	$("#content-section").append(html);
							$("#peta-section").append(html);
					});
									
			}
		
		$("#beacon").change(function() {
			if(this.checked) 
			{
				<?php
					$i=0;
					if(isset($lokasi_input))
					{
						foreach($lokasi_input as $data_lok)
						{
							?>
								var inputIcon = L.icon({
									iconUrl: '<?=base_url();?>/assets/ikon/circle.png',
									iconSize: [10, 10],
								});
								marker[<?=$i;?>] = L.marker([<?=$data_lok['lat'];?>, <?=$data_lok['lng'];?>],{icon: inputIcon}).addTo(mymap);
							<?php
							$i++;
						}
					}
				?>
			}
			else
			{
				<?php
					$i=0;
					if(isset($lokasi_input))
					{
						foreach($lokasi_input as $data_lok)
						{
							?>
								mymap.removeLayer(marker[<?=$i;?>]);
							<?php
							$i++;
						}
					}
				?>
			}
		});
		
		$("#geofence").change(function() {
			if(this.checked) 
			{
				<?php		
				if(isset($geofence[$n]['area']))
				{
					$areaCoordinate = json_decode($geofence[$n]['area'],TRUE);
					?>
					circle = L.circle([<?=$areaCoordinate['lat'];?>, <?=$areaCoordinate['lng'];?>], {
						color: 'red',
						fillColor: '#f03',
						fillOpacity: 0.5,
						radius: <?=$geofence[$n]['radius'];?>
					}).addTo(mymap);
					<?php
				}
				?>
			}
			else
			{
				mymap.removeLayer(circle);
			}
		});
		
		$("#denah").change(function() {
			if(this.checked) 
			{
				//geojsonLayer.addTo(mymap);
				kmlLayer.addTo(mymap);
			}
			else
			{
				//mymap.removeLayer(geojsonLayer);
				mymap.removeLayer(kmlLayer);
			}
		});
		
		setInterval(updateTabelStatus, 5000);
		function updateTabelStatus() {
					$.ajax({
						url:'/dashboard/updateTabelStatus',
						method:'post'
					});
									
		}
		
	});
</script>
</body>
</html>
