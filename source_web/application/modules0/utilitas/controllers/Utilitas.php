<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Utilitas extends CI_Controller {
	public $apps;
	var $nama_user;
	var $file_foto;
	 public function __construct()
       {
            parent::__construct(); 
			$this->load->library(array('ion_auth','form_validation'));
			$this->lang->load('auth');
			if (!$this->ion_auth->logged_in())
			{
				redirect('/auth/login', 'refresh');
			}
			$this->load->model('utilitas_model');
			$this->apps = 'Administrasi';
			$this->nama_user = $this->ion_auth->user()->row()->first_name.' '.$this->ion_auth->user()->row()->last_name;
			//$this->file_foto = get_foto();
			$this->userid = $this->session->userdata('s_username');
			$this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
			$this->output->set_header('Pragma: no-cache');
			$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	}
	
	public function menu()
	{
		$menu_all = $this->utilitas_model->get_menu($page_row=0,$row_id=0);
		//print_r($menu_all);
		
		$data = array(
				'rows' => $menu_all,
				'title' => 'Daftar Menu',
				'subtitle' => ''
		);
		$this->load->view('daftar_menu',$data);
	}
	
	public function menu_front()
	{
		$menu_all = $this->utilitas_model->get_menu_front($page_row=0,$row_id=0);
		//print_r($menu_all);
		
		$data = array(
				'rows' => $menu_all,
				'title' => 'Daftar Menu Front End',
				'subtitle' => ''
		);
		$this->load->view('daftar_menu_front',$data);
	}
	
	public function validate_menu()
	{
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->form_validation->set_rules('namamenu', 'Nama Menu', 'required');
	}
		
	public function menu_edit()
	{
		($this->uri->segment(3) == TRUE)?$menuid = $this->uri->segment(3):$menuid='';
		
		$sql = "select * from t_menu where menuid = '$menuid' ";
		$query = $this->db->query($sql);
		if($query->num_rows()>0)
		{
			$this->validate_menu();
			if ($this->form_validation->run() == FALSE)
			{
				$data = array(
					'row' => $query->row_array(),
					'page_title' => 'Form Pengeditan Menu',
					'title' => 'Menu',
					'subtitle' => ''
					);
				$this->load->view('form_menu',$data);
			}
			else
			{
				//print_r($_POST);
				$namamenu = $this->db->escape_str($_POST['namamenu']);
				$deskripsi = $this->db->escape_str($_POST['deskripsi']);
				$urutan = $this->db->escape_str($_POST['urutan']);
				$linkaction = $this->db->escape_str($_POST['linkaction']);
				$parentid = $this->db->escape_str($_POST['parentid']);
				$aktif = $this->db->escape_str($_POST['aktif']);
				
				$sql = "UPDATE t_menu set namamenu = '$namamenu',
													deskripsi = '$deskripsi',
													urutan = '$urutan',
													linkaction = '$linkaction',
													parentid = '$parentid',
													aktif = '$aktif'
						WHERE menuid = '$menuid'
													";
				//echo $sql;
				$this->db->query($sql);		
				
				if(isset($reqnip) && $reqnip != 'images'){input_log($this->userid,'Edit data menu dengan nama menu: '.$namamenu);}
				redirect('/utilitas/menu/');
			}
		
		}
		else
		{
			redirect('/');
		}
		
	
	}
	
	public function menu_front_edit()
	{
		($this->uri->segment(3) == TRUE)?$menuid = $this->uri->segment(3):$menuid='';
		
		$sql = "select * from t_menu_front where menuid = '$menuid' ";
		$query = $this->db->query($sql);
		if($query->num_rows()>0)
		{
			$this->validate_menu();
			if ($this->form_validation->run() == FALSE)
			{
				$data = array(
					'row' => $query->row_array(),
					'page_title' => 'Form Pengeditan Menu',
					'title' => 'Menu',
					'subtitle' => ''
					);
				$this->load->view('form_menu_front',$data);
			}
			else
			{
				print_r($_POST);
				$namamenu = $this->db->escape_str($_POST['namamenu']);
				$deskripsi = $this->db->escape_str($_POST['deskripsi']);
				$urutan = $this->db->escape_str($_POST['urutan']);
				$linkaction = $this->db->escape_str($_POST['linkaction']);
				$parentid = $this->db->escape_str($_POST['parentid']);
				$aktif = $this->db->escape_str($_POST['aktif']);
				
				$sql = "UPDATE t_menu_front set namamenu = '$namamenu',
													deskripsi = '$deskripsi',
													urutan = '$urutan',
													linkaction = '$linkaction',
													parentid = '$parentid',
													aktif = '$aktif'
						WHERE menuid = '$menuid'
													";
				echo $sql;
				$this->db->query($sql);		
				
				if(isset($reqnip) && $reqnip != 'images'){input_log($this->userid,'Edit data menu dengan nama menu: '.$namamenu);}
				redirect('/utilitas/menu_front/');
			}
		
		}
		else
		{
			redirect('/');
		}
		
	
	}
	
	public function menu_add()
	{		
		$data = array(
			'page_title' => 'Form Penambahan Data Menu',
			'title' => 'Menu',
			'subtitle' => ''
		);	
		 $this->validate_menu();
		 if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('form_menu',$data);
		}
		else
		{
			//print_r($_POST);
			$namamenu = $this->db->escape_str($_POST['namamenu']);
			$deskripsi = $this->db->escape_str($_POST['deskripsi']);
			$urutan = $this->db->escape_str($_POST['urutan']);
			$linkaction = $this->db->escape_str($_POST['linkaction']);
			$parentid = $this->db->escape_str($_POST['parentid']);
			$aktif = $this->db->escape_str($_POST['aktif']);
			
			$sql = "INSERT INTO t_menu set namamenu = '$namamenu',
												deskripsi = '$deskripsi',
												urutan = '$urutan',
												linkaction = '$linkaction',
												parentid = '$parentid',
												aktif = '$aktif'
												";
			//echo $sql;
			$this->db->query($sql);	
			if(isset($reqnip) && $reqnip != 'images'){input_log($this->userid,'Tambah data menu dengan nama menu '.$namamenu);}
			redirect('/utilitas/menu/');
		} 	
				
	}
	
	public function menu_front_add()
	{		
		$data = array(
			'page_title' => 'Form Penambahan Data Menu Front End',
			'title' => 'Menu Front',
			'subtitle' => ''
		);	
		$this->validate_menu();
		 if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('form_menu_front',$data);
		}
		else
		{
			print_r($_POST);
			$namamenu = $this->db->escape_str($_POST['namamenu']);
			$deskripsi = $this->db->escape_str($_POST['deskripsi']);
			$urutan = $this->db->escape_str($_POST['urutan']);
			$linkaction = $this->db->escape_str($_POST['linkaction']);
			$parentid = $this->db->escape_str($_POST['parentid']);
			$aktif = $this->db->escape_str($_POST['aktif']);
			
			$sql = "INSERT INTO t_menu_front set namamenu = '$namamenu',
												deskripsi = '$deskripsi',
												urutan = '$urutan',
												linkaction = '$linkaction',
												parentid = '$parentid',
												aktif = '$aktif'
												";
			echo $sql;
			$this->db->query($sql);	
			if(isset($reqnip) && $reqnip != 'images'){input_log($this->userid,'Tambah data menu dengan nama menu '.$namamenu);}
			redirect('/utilitas/menu_front/');
		}	
				
	}
	
	public function del_menu()
	{
		($this->uri->segment(3) == TRUE)?$menuid = $this->uri->segment(3):$menuid='';
		
		$sql = "select * from t_menu where menuid = '$menuid' ";
		$query = $this->db->query($sql);
		if($query->num_rows()>0)
		{
				$row = $query->row_array();
					$sql = "DELETE FROM t_menu WHERE menuid = '$menuid'	";
					echo $sql;
					$this->db->query($sql);	
					$sql = "DELETE FROM t_groupmenu WHERE menuid = '$menuid'	";
					echo $sql;
					$this->db->query($sql);	
				input_log($this->userid,'Hapus data menu dengan nama menu: '.$menuid);
				redirect('/utilitas/menu/');
		}
		else
		{
			redirect('/');
		}
	}
	
	public function del_menu_front()
	{
		($this->uri->segment(3) == TRUE)?$menuid = $this->uri->segment(3):$menuid='';
		
		$sql = "select * from t_menu_front where menuid = '$menuid' ";
		$query = $this->db->query($sql);
		if($query->num_rows()>0)
		{
			$row = $query->row_array();
			$sql = "DELETE FROM t_menu_front WHERE menuid = '$menuid'	";
			echo $sql;
			$this->db->query($sql);	
			input_log($this->userid,'Hapus data menu front dengan nama menu: '.$menuid);
			redirect('/utilitas/menu_front/');
		}
		else
		{
			redirect('/');
		}
	}
	
	public function user()
	{
		$rows = $this->utilitas_model->get_user($page_row=0,$row_id=0);
		
		$data = array(
				'rows' => $rows,
				'title' => 'Daftar Pengguna',
				'subtitle' => ''
		);
		$this->load->view('daftar_user',$data);
	}
	
	public function validate_user()
	{
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->form_validation->set_rules('userid', 'Username', 'required|is_unique[tuser.userid]');
		$this->form_validation->set_rules('pwd', 'Password', 'required|min_length[6]|max_length[16]|matches[passconf]');
		$this->form_validation->set_rules('passconf', 'Password Confirmation', 'required');
	}
	
	public function validate_user_edit()
	{
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->form_validation->set_rules('pwd', 'Password', 'required|min_length[6]|max_length[16]|matches[passconf]');
		$this->form_validation->set_rules('passconf', 'Password Confirmation', 'required');
	}
	
	public function user_edit()
	{
		($this->uri->segment(3) == TRUE)?$id = $this->uri->segment(3):$id='';
		
		$this->data['title'] = $this->lang->line('edit_user_heading');

		$user = $this->ion_auth->user($id)->row();
		$groups=$this->ion_auth->groups()->result_array();
		$currentGroups = $this->ion_auth->get_users_groups($id)->row()->id;
	
		// validate form input
		$this->form_validation->set_rules('first_name', $this->lang->line('create_user_validation_fname_label'), 'required|trim');
        $this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_lname_label'), 'required|trim');        
        $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|trim');      
        $this->form_validation->set_rules('nip', 'NIP', 'required|trim');
        $this->form_validation->set_rules('phone', $this->lang->line('create_user_validation_phone_label'), 'trim');
		
		if (isset($_POST) && !empty($_POST))
		{
			if ($this->form_validation->run() === TRUE)
			{
				$data = array(
					'first_name' => $this->db->escape_str($this->input->post('first_name')),
					'last_name'  => $this->db->escape_str($this->input->post('last_name')),
					'phone'      => $this->db->escape_str($this->input->post('phone')),
					'email'      => $this->db->escape_str($this->input->post('email')),
					'nip'      => $this->db->escape_str($this->input->post('nip')),
				//	'skpd_kewenangan'      => $this->input->post('skpd_kewenangan'),
				);

				// Only allow updating groups if user is admin
				if ($this->ion_auth->is_admin())
				{
					//Update the groups user belongs to
					$groupData = $this->input->post('groups');

					if (isset($groupData) && !empty($groupData)) {

						$this->ion_auth->remove_from_group('', $id);

						foreach ($groupData as $grp) {
							$this->ion_auth->add_to_group($grp, $id);
						}

					}
				}

			// check to see if we are updating the user
			   if($this->ion_auth->update($user->id, $data))
			    {
			    	// redirect them back to the admin page if admin, or to the base url if non admin
				    $this->session->set_flashdata('message', $this->ion_auth->messages() );
				    if ($this->ion_auth->is_admin())
					{
						redirect('/utilitas/user', 'refresh');
					}
					else
					{
						redirect('/utilitas/user', 'refresh');
					}

			    }
			    else
			    {
			    	// redirect them back to the admin page if admin, or to the base url if non admin
				    $this->session->set_flashdata('message', $this->ion_auth->errors() );
				    if ($this->ion_auth->is_admin())
					{
						redirect('/utilitas/user', 'refresh');
					}
					else
					{
						redirect('/utilitas/user', 'refresh');
					}

			    }

			}
			
		}

		// pass the user to the view
		$this->data['user'] = $user;
		$this->data['groups'] = $groups;
		$this->data['currentGroups'] = $currentGroups;

		$this->data['username'] = array(
			'name'  => 'username',
			'id'    => 'username',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('username', $user->username),
			'class'  => 'form-control',
			'disabled'=>''
		);
		$this->data['email'] = array(
			'name'  => 'email',
			'id'    => 'email',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('email', $user->email),
			'class'  => 'form-control',
		);
		$this->data['first_name'] = array(
			'name'  => 'first_name',
			'id'    => 'first_name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('first_name', $user->first_name),
			'class'  => 'form-control',
		);
		$this->data['last_name'] = array(
			'name'  => 'last_name',
			'id'    => 'last_name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('last_name', $user->last_name),
			'class'  => 'form-control',
		);
		$this->data['phone'] = array(
			'name'  => 'phone',
			'id'    => 'phone',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('phone', $user->phone),
			'class'  => 'form-control',
		);
		$this->data['nip'] = array(
			'name'  => 'nip',
			'id'    => 'nip',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('nip', $user->nip),
			'class'  => 'form-control',
		);
		$this->data['skpd_kewenangan'] = $user->skpd_kewenangan;
		
		$this->data['title'] = 'Daftar User';
		$this->data['subtitle'] = 'Form Pemutakhiran Data User';

		$this->_render_page('edit_user', $this->data);
	}
	
	/* public function user_edit()
	{
		($this->uri->segment(3) == TRUE)?$id = $this->uri->segment(3):$id='';
		
		$this->data['title'] = $this->lang->line('edit_user_heading');

		$user = $this->ion_auth->user($id)->row();
		$groups=$this->ion_auth->groups()->result_array();
		$currentGroups = $this->ion_auth->get_users_groups($id)->result();

		// validate form input
		$this->form_validation->set_rules('first_name', $this->lang->line('edit_user_validation_fname_label'), 'required');
		$this->form_validation->set_rules('last_name', $this->lang->line('edit_user_validation_lname_label'), 'required');
	//	$this->form_validation->set_rules('phone', $this->lang->line('edit_user_validation_phone_label'), 'required');
	//	$this->form_validation->set_rules('company', $this->lang->line('edit_user_validation_company_label'), 'required');
	//	$this->form_validation->set_rules('nomor_induk', 'Nomor Induk', 'required');

		if (isset($_POST) && !empty($_POST))
		{
			// update the password if it was posted
			if ($this->input->post('password'))
			{
				$this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
				$this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');
			}

			if ($this->form_validation->run() === TRUE)
			{
				$data = array(
				//	'first_name' => $this->input->post('first_name'),
				//	'last_name'  => $this->input->post('last_name'),
				//	'company'    => $this->input->post('company'),
				//	'phone'      => $this->input->post('phone'),
				//	'nomor_induk'      => $this->input->post('nomor_induk'),
				);

				// update the password if it was posted
				if ($this->input->post('password'))
				{
					//$data['password'] = $this->input->post('password');
				}



				// Only allow updating groups if user is admin
				if ($this->ion_auth->is_admin())
				{
					//Update the groups user belongs to
					$groupData = $this->input->post('groups');

					if (isset($groupData) && !empty($groupData)) {

						$this->ion_auth->remove_from_group('', $id);

						foreach ($groupData as $grp) {
							$this->ion_auth->add_to_group($grp, $id);
						}

					}
				}

			// check to see if we are updating the user
			   if($this->ion_auth->update($user->id, $data))
			    {
			    	// redirect them back to the admin page if admin, or to the base url if non admin
				    $this->session->set_flashdata('message', $this->ion_auth->messages() );
				    if ($this->ion_auth->is_admin())
					{
						redirect('/utilitas/user', 'refresh');
					}
					else
					{
						redirect('/utilitas/user', 'refresh');
					}

			    }
			    else
			    {
			    	// redirect them back to the admin page if admin, or to the base url if non admin
				    $this->session->set_flashdata('message', $this->ion_auth->errors() );
				    if ($this->ion_auth->is_admin())
					{
						redirect('/utilitas/user', 'refresh');
					}
					else
					{
						redirect('/utilitas/user', 'refresh');
					}

			    }

			}
			
		}

		// display the edit user form
		$this->data['csrf'] = $this->_get_csrf_nonce();

		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
		
		// pass the user to the view
		$this->data['user'] = $user;
		$this->data['groups'] = $groups;
		$this->data['currentGroups'] = $currentGroups;

		$this->data['first_name'] = array(
			'name'  => 'first_name',
			'id'    => 'first_name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('first_name', $user->first_name), 
			'class'  => 'form-control',
		);
		$this->data['last_name'] = array(
			'name'  => 'last_name',
			'id'    => 'last_name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('last_name', $user->last_name),
			'class'  => 'form-control',
		);
		$this->data['company'] = array(
			'name'  => 'company',
			'id'    => 'company',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('company', $user->company),
			'class'  => 'form-control',
		);
		$this->data['phone'] = array(
			'name'  => 'phone',
			'id'    => 'phone',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('phone', $user->phone),
			'class'  => 'form-control',
		);
		
		$this->data['email'] = array(
			'name'  => 'email',
			'id'    => 'email',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('email', $user->email),
			'class'  => 'form-control',
			'disabled'=>''
		);
		/* $this->data['nomor_induk'] = array(
			'name'  => 'nomor_induk',
			'id'    => 'nomor_induk',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('nomor_induk', $user->nomor_induk),
			'class'  => 'form-control',
		); 
		$this->data['password'] = array(
			'name' => 'password',
			'id'   => 'password',
			'type' => 'password',
			'class'  => 'form-control',
		);
		$this->data['password_confirm'] = array(
			'name' => 'password_confirm',
			'id'   => 'password_confirm',
			'type' => 'password',
			'class'  => 'form-control',
		);
		$this->data['title'] = 'Daftar User';
		$this->data['subtitle'] = 'Form Penambahan User';

		$this->_render_page('edit_user', $this->data);
	} */
	
	public function user_add()
	{		
		$data = array(
			'title' =>'Daftar User',
			'subtitle' => 'Form Penambahan Data User'
		);	
		$tables = $this->config->item('tables','ion_auth');
        $identity_column = $this->config->item('identity','ion_auth');
        $this->data['identity_column'] = $identity_column;
		
		$groups=$this->ion_auth->groups()->result_array();
		$this->data['groups'] = $groups;
		
        // validate form input
        if($identity_column!=='email')
        {
            $this->form_validation->set_rules('identity',$this->lang->line('create_user_validation_identity_label'),'required|is_unique['.$tables['users'].'.'.$identity_column.']');
            $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email');
        }
        else
        {
            $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email|is_unique[' . $tables['users'] . '.email]');
        }
        $this->form_validation->set_rules('first_name', $this->lang->line('create_user_validation_last_name_label'), 'required|trim');
		 $this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_last_name_label'), 'required|trim');
		 $this->form_validation->set_rules('nip', 'NIP', 'required|trim');
		  $this->form_validation->set_rules('phone', $this->lang->line('create_user_validation_phone_label'), 'trim');
        $this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
        $this->form_validation->set_rules('password_confirm', $this->lang->line('create_user_validation_password_confirm_label'), 'required');

        if ($this->form_validation->run() == true)
        {
		//	print_r($_REQUEST);
            $email    = strtolower($this->input->post('email'));
            $identity = ($identity_column==='email') ? $email : $this->input->post('identity');
            $password = $this->input->post('password');

            $additional_data = array(
                'first_name' => $this->db->escape_str($this->input->post('first_name')),
                'last_name'  => $this->db->escape_str($this->input->post('last_name')),
                'phone'      => $this->db->escape_str($this->input->post('phone')),
                'nip'      => $this->db->escape_str($this->input->post('nip')),
            );
        }
        if ($this->form_validation->run() == true && $this->ion_auth->register($identity, $password, $email, $additional_data))
        {
			$s = "select id from t_users where $identity_column  = '$identity' ";
			$q = $this->db->query($s);
			$q = $q->row_array();
			$id = $q['id'];
			$active = $this->input->post('active');
			if($active == 1)
			{
				$this->ion_auth->activate($id);
			}
			
			$groupData = $this->input->post('groups');
			if (isset($groupData) && !empty($groupData)) 
			{
				$this->ion_auth->remove_from_group('', $id);
				foreach ($groupData as $grp) 
				{
					$this->ion_auth->add_to_group($grp, $id);
				}
			}
			
            // check to see if we are creating the user
            // redirect them back to the admin page
            $this->session->set_flashdata('message', $this->ion_auth->messages());
            redirect("/utilitas/user", 'refresh');
        }
        else
        {
            // display the create user form
            // set the flash data error message if there is one
            $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
			
            $this->data['first_name'] = array(
                'name'  => 'first_name',
                'id'    => 'first_name',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('first_name'),
                'class'  => 'form-control',
            );
            $this->data['last_name'] = array(
                'name'  => 'last_name',
                'id'    => 'last_name',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('last_name'),
                'class'  => 'form-control',
            );
            $this->data['identity'] = array(
                'name'  => 'identity',
                'id'    => 'identity',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('identity'),
                'class'  => 'form-control',
            );
            $this->data['email'] = array(
                'name'  => 'email',
                'id'    => 'email',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('email'),
                'class'  => 'form-control',
            );
			 $this->data['nip'] = array(
                'name'  => 'nip',
                'id'    => 'nip',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('nip'),
                'class'  => 'form-control',
            );
            $this->data['phone'] = array(
                'name'  => 'phone',
                'id'    => 'phone',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('phone'),
                'class'  => 'form-control',
            );
            $this->data['password'] = array(
                'name'  => 'password',
                'id'    => 'password',
                'type'  => 'password',
                'value' => $this->form_validation->set_value('password'),
                'class'  => 'form-control',
            );
            $this->data['password_confirm'] = array(
                'name'  => 'password_confirm',
                'id'    => 'password_confirm',
                'type'  => 'password',
                'value' => $this->form_validation->set_value('password_confirm'),
                'class'  => 'form-control',
            );
			$this->data['title'] = 'Daftar User';
			$this->data['subtitle'] = 'Form Penambahan User';
			
            $this->load->view('form_user', $this->data);
        }
				
	}
	
	// activate the user
	public function activate($id, $code=false)
	{
		if ($code !== false)
		{
			$activation = $this->ion_auth->activate($id, $code);
		}
		else if ($this->ion_auth->is_admin())
		{
			$activation = $this->ion_auth->activate($id);
		}

		if ($activation)
		{
			/* $user = $this->ion_auth->user($id)->row();
			$target = $user->email;
			$penerima_email = $user->first_name.' '.$user->last_name;
			$subject = "Aktivasi Keanggotaan Sistem Informasi Fakultas Ekonomi Unisba";
			$password = random_password();
			$data = array(
				'password' => $password,
			);
			$this->ion_auth->update($id, $data);
			$data_messages = array(
				'email' => $target,
				'password' => $password,
				'penerima_email' => $penerima_email,
			);
			$this->kirim_email($target,$subject,$data_messages); */
			// redirect them to the auth page
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			redirect("utilitas/user", 'refresh');
		}
		else
		{
			// redirect them to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect("auth/forgot_password", 'refresh');
		}
	}

	// deactivate the user
	
	public function deactivate($id = NULL)
	{
		// do we have the right userlevel?
		if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin())
		{
			$this->ion_auth->deactivate($id);
		}
		// redirect them back to the auth page
		redirect('/utilitas/user', 'refresh');
	}
	
	public function del_user()
	{
		($this->uri->segment(3) == TRUE)?$id = $this->uri->segment(3):$id='';
		
		/* $sql = "delete from d_profil where userid = '$id' ";
		$q = $this->db->query($sql); */
		$sql = "select * from t_users where id = '$id' ";
		$query = $this->db->query($sql);
		if($query->num_rows()>0)
		{
				$row = $query->row_array();
				$s = "DELETE FROM t_users_groups where user_id = '$id' ";
				$q = $this->db->query($s);
				$sql = "DELETE FROM t_users WHERE id = '$id'	";
				echo $sql;
				$this->db->query($sql);	

				if(isset($reqnip) && $reqnip != 'images'){input_log($this->userid,'Hapus data user dengan username: '.$row['email']);}
				redirect('/utilitas/user/');
		}
		else
		{
			redirect('/');
		}
	}
	
	public function get_unkerja_option()
	{
		$sql = "select * from unkerja where mid(kunker,1,2) = '10' 
										and mid(kunker,5,8) = '00000000' ";
		$query = $this->db->query($sql);
		$unkerja = array();
		foreach($query->result_array() as $row)
		{
			$unkerja[] = array(
				'id' => $row['kunker'],
				'nunker' => $row['nunker'],
			);
		}
		$sql = "select * from unkerja where mid(kunker,1,4) = '1001' 
								and mid(kunker,10,3) = '000'
								and mid(kunker,7,2) != '00'
								and keselon != 21 ";
		$query = $this->db->query($sql);
		foreach($query->result_array() as $row)
		{
			$unkerja[] = array(
				'id' => $row['kunker'],
				'nunker' => $row['nunker'],
			);
		}
		//print_r($unkerja);
		$rows = array();
		$rows[] = array(
				'DisplayText' => '----------',
				'Value' => ''
				);
		foreach($unkerja as $row)
		{
			$rows[] = array(
				'DisplayText' => $row['nunker'],
				'Value' => $row['id']
				);
		}
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Options'] = $rows;
		print json_encode($jTableResult);
	}
	
	public function get_kabkota_option()
	{
		$sql = "select * from unkerja where mid(kunker,1,2) != '10' ";
		$query = $this->db->query($sql);
		$rows = array();
		$rows[] = array(
				'DisplayText' => '----------',
				'Value' => ''
				);
		foreach($query->result_array() as $row)
		{
			$rows[] = array(
				'DisplayText' => $row['nunker'],
				'Value' => $row['kunker']
				);
		}
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Options'] = $rows;
		print json_encode($jTableResult);
	}
	
	public function get_grup_option()
	{
		$sql = "select * from tgroup ";
		$query = $this->db->query($sql);
		$rows = array();
		foreach($query->result_array() as $row)
		{
			$rows[] = array(
				'DisplayText' => $row['groupname'],
				'Value' => $row['groupid']
				);
		}
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Options'] = $rows;
		print json_encode($jTableResult);
	}
	
	public function group()
	{
		$rows = $this->utilitas_model->get_group($page_row=0,$row_id=0);
		$data = array(
				'page_title' => 'Daftar Group Pengguna',
				'rows' => $rows,
				'title' => 'Daftar Group',
				'subtitle' => ''
		);
		$this->load->view('utilitas/daftar_group',$data);
	}
	
	public function validate_group()
	{
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Nama Group', 'required');
	}
	
	public function group_edit()
	{
		if ($this->uri->segment(3) == TRUE) $groupid = $this->uri->segment(3);
		$this->validate_group();
		if ($this->form_validation->run() == FALSE)
		{
			$data = array(
			'row' => $this->utilitas_model->get_group_detil($groupid)->row_array(),
			'groupid' => $groupid,
			'page_title' => 'Form Pengeditan Data Group Pengguna',
			'title' => 'Daftar Group',
				'subtitle' => ''
			);
			$this->load->view('utilitas/form_group',$data);
		}
		else
		{
			$posts = $this->input->post();
			//print_r($posts);
			$name = $posts['name'];
			$description = $posts['description'];
			$groupid = $posts['groupid'];
			$sql = 	"UPDATE t_groups SET name = '$name', 
										description = '$description' 
					WHERE id = '$groupid'";
		//	echo $sql.'</br>';
			$this->db->query($sql);
			$request = $this->input->post();
		//	print_r($request);
			$id = $request['id'];
			//Delete all groupmenu
			$sql = "DELETE FROM t_groupmenu WHERE groupid = $groupid";
			$rscheck = $this->db->query($sql);
			
			foreach($id as $row)
			{
				$id_menu = $row;
				$h_a = (isset($request['A'][$row])?$request['A'][$row]:0);
				$h_u = (isset($request['U'][$row])?$request['U'][$row]:0);
				$h_d = (isset($request['D'][$row])?$request['D'][$row]:0);
				$h_v = (isset($request['V'][$row])?$request['V'][$row]:0);
				$h_p = (isset($request['P'][$row])?$request['P'][$row]:0);
				$sql = 'insert into t_groupmenu (groupid, menuid,ad,up,de,vi,pr) values ('.$groupid.','.$id_menu.', '.$h_a.','.$h_u.','.$h_d.','.$h_v.','.$h_p.')';
				// echo $sql.'</br>';
				$this->db->query($sql);
			}
			
			redirect('utilitas/group/','location');
		}
	}
	
	public function group_add()
	{		
		$data = array(
			'title' => 'Daftar Group',
			'subtitle' => 'Form Tambah Group',
		);	
		$this->validate_group();
		 if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('form_group',$data);
		}
		else
		{
			$posts = $this->input->post();
			print_r($posts);
			$name = $posts['name'];
			$description = $posts['description'];
			$sql = 	"insert into t_groups SET name = '$name', 
										description = '$description' 
					";
			echo $sql.'</br>';
			$this->db->query($sql);
			$groupid = $this->db->insert_id();
			$menuvalue = array();
			while (list ($key, $val) = each ($posts)) 
			{
				//if ((substr($key,0,6) == 'menuid'))
					if ((substr($key,0,2) == 'id'))
				{
					array_push($menuvalue, $val);
				}
			}	
			print_r($menuvalue);
			
			if (sizeof($menuvalue) != 0) 
			{		
				for ($i = 0; $i <= sizeof($menuvalue)-1 ; $i++) 
				{
						//Insert
						$sql = "INSERT INTO t_groupmenu (groupid, menuid) VALUES ($groupid, $menuvalue[$i])";
						echo $sql.'</br>';
						$rsmenu = $this->db->query($sql);			
				}
			}	
			if(isset($reqnip) && $reqnip != 'images'){input_log($this->userid,'Tambah data user grup nama grup '.$groupname);}
			redirect('/utilitas/group/');
		}	
				
	}
	
	public function group_del()
	{				
		$id = $this->uri->segment(3);
		$s = "delete from t_groups where id = '$id' ";
		$this->db->query($s);
		$sg = "delete from t_groupmenu where groupid = '$id' ";
		$this->db->query($sg);
		redirect('/utilitas/group/');				
	}
	
	public function list_pendaftaran_user()
	{
		$cari = $this->input->post('cari');
		if ((!isset($cari)) || ($cari == ""))
		{
			$user_all = $this->utilitas_model->get_pendaftaran_user($page_row=0,$row_id=0);
			//print_r($user_all);
			$page_count = $user_all->num_rows(); 
			$page_url = base_url().'index.php/utilitas/list_pendaftaran_user/';
			$page_row = 20;
			
			$paging['base_url'] = $page_url;
			$paging['total_rows'] = $page_count;
			$paging['per_page'] = $page_row;
			$paging['uri_segment'] = 3;
			$paging['num_links'] = 4;

			$this->pagination->initialize($paging);
			if ($this->uri->segment(3) === FALSE) $row_id = 0; else $row_id = $this->uri->segment(3);
			$rows = $this->utilitas_model->get_pendaftaran_user($page_row,$row_id);
		}
		else
		{
			$page_row = 20;
			if ($this->uri->segment(3) === FALSE) $row_id = 0; else $row_id = $this->uri->segment(3);
			$sql_cari_all = "SELECT tuser.userid, pwd, tanyalupapwd, jawablupapwd, email, aktif, tuser.kunker, unkerja1.nunker nunker1, unkerja2.nunker nunker2, nip, tgroup.groupid, tgroup.groupname, tgroup.deskripsi FROM tuser ".
				"LEFT JOIN unkerja unkerja1 ON tuser.kunker = unkerja1.kunker LEFT JOIN unkerja unkerja2 ON tuser.kkabkota = unkerja2.kunker LEFT JOIN truserright ON tuser.userid = truserright.userid LEFT JOIN tgroup ON truserright.groupid = tgroup.groupid ".
				"WHERE tuser.userid LIKE '%". $cari ."%' OR tanyalupapwd LIKE '%". $cari ."%' OR unkerja1.nunker LIKE '%". $cari ."%' OR unkerja1.nunker LIKE '%". $cari ."%' OR tgroup.groupname LIKE '%". $cari ."%' OR ".
				"email LIKE '%". $cari ."%' where tuser.aktif = 0
					and tgroup.groupid = 30 ORDER BY tgroup.groupid, userid";
				
			$query_cari_all = $this->db->query($sql_cari_all);
			$page_count = $query_cari_all->num_rows(); 
			$page_url = base_url().'index.php/utilitas/list_pendaftaran_user/';
			
			$sql_cari = "SELECT tuser.userid, pwd, tanyalupapwd, jawablupapwd, email, aktif, tuser.kunker, unkerja1.nunker nunker1, unkerja2.nunker nunker2, nip, tgroup.groupid, tgroup.groupname, tgroup.deskripsi FROM tuser ".
				"LEFT JOIN unkerja unkerja1 ON tuser.kunker = unkerja1.kunker LEFT JOIN unkerja unkerja2 ON tuser.kkabkota = unkerja2.kunker LEFT JOIN truserright ON tuser.userid = truserright.userid LEFT JOIN tgroup ON truserright.groupid = tgroup.groupid ".
				"WHERE tuser.userid LIKE '%". $cari ."%' OR tanyalupapwd LIKE '%". $cari ."%' OR unkerja1.nunker LIKE '%". $cari ."%' OR unkerja1.nunker LIKE '%". $cari ."%' OR tgroup.groupname LIKE '%". $cari ."%' OR ".
				"email LIKE '%". $cari ."%' where tuser.aktif = 0
					and tgroup.groupid = 30ORDER BY tgroup.groupid, userid";
			//	echo "sql_cari: ".$sql_cari;
			$rows = $this->db->query($sql_cari);
			
			$paging['base_url'] = $page_url;
			$paging['total_rows'] = $page_count;
			$paging['per_page'] = $page_row;
			$paging['uri_segment'] = 3;
			$paging['num_links'] = 4;

			$this->pagination->initialize($paging);
			
		}
		$data = array(
			'title' =>'Sistem Informasi Aparatur Kota Bekasi',
				'welcome_header' => 'Selamat Datang di Sistem Informasi Aparatur Kota Bekasi',
				'rows' => $rows,
				'link' => '#FFFFCC',
				'link_hover' => '#FFFF66',
				'page_url' => $page_url,
				'page_count'=>$page_count,
				'pagination' => $this->pagination->create_links(),
				'numrows' => $page_count,
				'row_id' => $row_id,
		);
		$this->load->view('utilitas/list_pendaftaran_user',$data);
	}
	
	public function pendaftaran_user_detail()
	{
		
		if ($this->uri->segment(3) == TRUE) $userid = $this->uri->segment(3);
		$row_id = $this->uri->segment(4);
		$data = array(
			'title' =>'Sistem Informasi Aparatur Kota Bekasi',
			'welcome_header' => 'Selamat Datang di Sistem Informasi Aparatur Kota Bekasi',
			'link' => '#FFFFCC',
			'link_hover' => '#FFFF66',
			'rows' => $this->utilitas_model->get_pendaftaran_user_detil($userid),
			'id' => $userid,
			'row_id' => $row_id,
			);
		$this->load->view('utilitas/detail_pendaftaran_user',$data);
	}
	
	public function approve_pendaftaran()
	{
		$userid = $_REQUEST['userid'];
		$sql = "update tuser set aktif = 1 where userid = '$userid'";
	//	echo $sql;
		$update = $this->db->query($sql);
		
		echo "<b>Pendaftaran user telah disetujui</b>";
	}
	
	public function validate()
	{
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nip', 'NIP', 'trim|required|min_length[18]|max_length[18]|xss_clean|is_unique[tuser.NIP]');
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('email', 'Alamat Email', 'required|valid_email|is_unique[tuser.email]');
		$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[5]|max_length[16]|xss_clean|is_unique[tuser.userid]');
		$this->form_validation->set_rules('passwd', 'Password', 'required|matches[passconf]');		
		$this->form_validation->set_rules('passconf', 'Konfirmasi Password', 'required');
	}
	
	public function log_pengguna()
	{
		$rows = $this->utilitas_model->get_user($page_row=0,$row_id=0);
		
		$data = array(
				'title' =>'Sistem Informasi Aparatur Kota Bekasi',
				'welcome_header' => 'Selamat Datang di Sistem Informasi Aparatur Kota Bekasi',
				'rows' => $rows,
		);
		$this->load->view('daftar_pengguna_log',$data);
	}
	
	public function list_penggunalog()
	{
		if(isset($_REQUEST['jtSorting']))
		{
			$sorting = $_REQUEST['jtSorting'];
		}
		$row_id = $_REQUEST['jtStartIndex'];
		$page_row = $_REQUEST['jtPageSize'];
		
		$sql = "SELECT tuser.userid, 
						tuser.userid as username,
							tuser.NIP,
						tuser.kunker, 
						unkerja1.nunker nunker1, 
						unkerja2.nunker nunker2,
						tgroup.groupname
						FROM tuser 
				LEFT JOIN unkerja unkerja1 ON tuser.kunker = unkerja1.kunker 
				LEFT JOIN unkerja unkerja2 ON tuser.kkabkota = unkerja2.kunker 
				LEFT JOIN truserright ON tuser.userid = truserright.userid 
				LEFT JOIN tgroup ON truserright.groupid = tgroup.groupid 
				WHERE 1=1 ";
		if(isset($_REQUEST['namacari']) && $_REQUEST['namacari'] != '')
		{
			$sql .= " AND ( tuser.userid like '%".$_REQUEST['namacari']."%' 
							OR  tuser.NIP like '%".$_REQUEST['namacari']."%')";
		}
					//echo $sql;
		$data_semua = $this->db->query($sql);
		$recordCount = $data_semua->num_rows();
		
		if(isset($sorting) && $sorting != NULL)
		{
			$sql .= "order by ".$sorting." ";
		}
		if(($page_row != 0))
		{
			$sql.=" limit $row_id,$page_row";
		}
		//echo $sql;
		$data = $this->db->query($sql);
		$rows = array();
		foreach($data->result_array() as $row)
		{
			$rows[] = $row;
		}
		//print_r($rows);
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Records'] = $rows;
		$jTableResult['TotalRecordCount'] = $recordCount;
		print json_encode($jTableResult);
	}
	
	public function detail_log_pengguna()
	{
		$userid = $this->uri->segment(3);
		if(isset($userid) && $userid != '')
		{			
			$data = array(
					'title' =>'Sistem Informasi Aparatur Kota Bekasi',
					'welcome_header' => 'Selamat Datang di Sistem Informasi Aparatur Kota Bekasi',
					'userid' => $userid,
			);
			$this->load->view('utilitas/detail_log_pengguna',$data);
		}
		else
		{
			redirect('/');
		}
	}
	
	public function list_detail_log()
	{
		$userid = $this->uri->segment(3);
		if(isset($_REQUEST['jtSorting']))
		{
			$sorting = $_REQUEST['jtSorting'];
		}
		$row_id = $_REQUEST['jtStartIndex'];
		$page_row = $_REQUEST['jtPageSize'];
		
		$sql = "SELECT *
						FROM t_log_aktivitas 
				
				WHERE userid='$userid' ";
		if(isset($_REQUEST['namacari']) && $_REQUEST['namacari'] != '')
		{
			$sql .= " AND ( tuser.userid like '%".$_REQUEST['namacari']."%' 
							OR  tuser.NIP like '%".$_REQUEST['namacari']."%')";
		}
					//echo $sql;
		$data_semua = $this->db->query($sql);
		$recordCount = $data_semua->num_rows();
		
		if(isset($sorting) && $sorting != NULL)
		{
			$sql .= "order by ".$sorting." ";
		}
		if(($page_row != 0))
		{
			$sql.=" limit $row_id,$page_row";
		}
		//echo $sql;
		$data = $this->db->query($sql);
		$rows = array();
		foreach($data->result_array() as $row)
		{
			$rows[] = $row;
		}
		//print_r($rows);
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Records'] = $rows;
		$jTableResult['TotalRecordCount'] = $recordCount;
		print json_encode($jTableResult);
	}
	
	public function list_detillog()
	{
		$userid = $this->uri->segment(3);
		if(isset($_REQUEST['jtSorting']))
		{
			$sorting = $_REQUEST['jtSorting'];
		}
		if(isset($_REQUEST['jtStartIndex']))
		{
			$row_id = $_REQUEST['jtStartIndex'];
		}
		if(isset($_REQUEST['jtPageSize']))
		{
			$page_row = $_REQUEST['jtPageSize'];
		}
		/* $row_id = $_REQUEST['jtStartIndex'];
		$page_row = $_REQUEST['jtPageSize']; */
		$sql = "SELECT * FROM t_log_aktivitas where userid = '$userid'";
		//echo $sql;
					//echo $sql;
		$data_semua = $this->db->query($sql);
		$recordCount = $data_semua->num_rows();
		
		if(isset($sorting) && $sorting != NULL)
		{
			$sql .= "order by ".$sorting." ";
		}
		if((isset($page_row) && $page_row != 0))
		{
			$sql.=" limit $row_id,$page_row";
		}
		//echo $sql;
		$data = $this->db->query($sql);
		$rows = array();
		foreach($data->result_array() as $row)
		{
			$rows[] = $row;
		}
		//print_r($rows);
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Records'] = $rows;
		$jTableResult['TotalRecordCount'] = $recordCount;
		print json_encode($jTableResult);
	}
	
	public function daftar_log_pengguna()
	{
			$userid = $this->uri->segment(3);
			$page_row = 20;
			if ($this->uri->segment(4) === FALSE) $row_id = 0; else $row_id = $this->uri->segment(4);
			$sql_cari = "SELECT * FROM t_log_aktivitas where userid = '$userid'";
			//	echo "sql_cari: ".$sql_cari;
			$rows_all = $this->db->query($sql_cari);
			
			$page_count = $rows_all->num_rows(); 
			$page_url = base_url().'index.php/utilitas/daftar_log_pengguna/'.$userid.'/';
			$page_row = 20;
			$paging['base_url'] = $page_url;
			$paging['total_rows'] = $page_count;
			$paging['per_page'] = $page_row;
			$paging['uri_segment'] = 4;
			$paging['num_links'] = 5;
			$sql = "SELECT * FROM t_log_aktivitas where userid = '$userid' order by waktu desc";
			if(($page_row != 0)){
			$sql.=" LIMIT $row_id,$page_row ";
			}
			//$sql.=" order by waktu ";
			$rows = $this->db->query($sql);
			$this->pagination->initialize($paging);
			
		$data = array(
				'title' =>'Sistem Informasi Aparatur Kota Bekasi',
				'welcome_header' => 'Selamat Datang di Sistem Informasi Aparatur Kota Bekasi',
				'rows' => $rows,
				'link' => '#FFFFCC',
				'link_hover' => '#FFFF66',
				'page_url' => $page_url,
				'page_count'=>$page_count,
				'pagination' => $this->pagination->create_links(),
				'numrows' => $page_count,
				'row_id' => $row_id,
		);
		$this->load->view('utilitas/daftar_pengguna_log_detil',$data);
	}
	
	public function cetak_pdf_menu()
	{
		$data = array(		
			'menu_all' => $this->utilitas_model->get_menu($page_row=0,$row_id=0),
		);
		define('FPDF_FONTPATH',$this->config->item('fonts_path'));
		require_once APPPATH."third_party/CetakMenu.php"; 
		$cetak = new CetakMenu("L","mm","legal",$data);
		if(isset($cetak))
		{
			$cetak->AliasNbPages();							
			$cetak->SetMargins(10,10,10);						
			$cetak->AddPage();							
			$cetak->CreateTable();									
			$cetak->Output();	
		}
	}
	
	public function cetak_user()
	{
		$data = array(		
			'user_all' => $this->utilitas_model->get_user($page_row=0,$row_id=0),
		);
		define('FPDF_FONTPATH',$this->config->item('fonts_path'));
		require_once APPPATH."third_party/CetakUser.php"; 
		$cetak = new CetakUser("L","mm","legal",$data);
		if(isset($cetak))
		{
			$cetak->AliasNbPages();							
			$cetak->SetMargins(10,10,10);						
			$cetak->AddPage();							
			$cetak->CreateTable();									
			$cetak->Output();	
		}
	}
	
	public function cetak_group()
	{
		$data = array(		
			'group_all' => $this->utilitas_model->get_group($page_row=0,$row_id=0)
		);
		define('FPDF_FONTPATH',$this->config->item('fonts_path'));
		require_once APPPATH."third_party/CetakGroup.php"; 
		$cetak = new CetakGroup("L","mm","legal",$data);
		if(isset($cetak))
		{
			$cetak->AliasNbPages();							
			$cetak->SetMargins(10,10,10);						
			$cetak->AddPage();							
			$cetak->CreateTable();									
			$cetak->Output();	
		}
	}
	
	public function cetak_log()
	{
		$userid = $_POST['userid'];
		$sql_cari = "SELECT * FROM t_log_aktivitas where userid = '$userid'";
		$rows_all = $this->db->query($sql_cari);
		$data = array(		
			'rows_all' => $rows_all,
			'userid' => $userid,
		);
		define('FPDF_FONTPATH',$this->config->item('fonts_path'));
		require_once APPPATH."third_party/CetakLog.php"; 
		$cetak = new CetakLog("L","mm","legal",$data);
		if(isset($cetak))
		{
			$cetak->AliasNbPages();							
			$cetak->SetMargins(10,10,10);						
			$cetak->AddPage();							
			$cetak->CreateTable();									
			$cetak->Output();	
		}
	}
	
	public function combobox_unit()
	{
		$array = array();
		if ($_GET['_name'] == 'unit') 
		{
			$unit = $_GET['_value'];
			if($unit != '100000000000')
			{
				$sql = "select * 
					from referensi_unit_kerja
					where mid(kunker,1,4) = '".substr($unit,0,4)."'
					and mid(kunker,7,6) = '000000'
					
					order by KUNKER asc
				";
				$sql = "SELECT * 
											FROM referensi_unit_kerja 
											WHERE (IF(MID('$unit',3,2)=10 || MID('$unit',3,2)=11,
												mid(kunker,1,6) = mid('$unit',1,6)  ,
												mid(kunker,1,4) = mid('$unit',1,4)  )
											)
											AND (IF(MID('$unit',3,2)=10 || MID('$unit',3,2)=11,
												mid(kunker,9,4) = '0000' ,
												mid(kunker,7,6) = '000000' )
											)
											
											ORDER BY KUNKER";
				//echo $sql;
				$query = $this->db->query($sql);
				foreach($query->result_array() as $row)
				{
					$array[] = array($row['KUNKER'] => $row['NUNKER']);
				}
			}
			else
			{
				$array[] = array('100000000000' => 'Semua');
			}
			
		}
		elseif ($_GET['_name'] == 'subunit') 
		{
			$subunit = $_GET['_value'];
			if($subunit != '100000000000')
			{
				$sql = "select * 
					from referensi_unit_kerja
					where mid(kunker,1,6) = '".substr($subunit,0,6)."'
					and mid(kunker,9,4) = '0000'
					
					order by KUNKER asc
				";
				$sql = "SELECT * 
											FROM referensi_unit_kerja 
											WHERE (IF(MID('$subunit',3,2)=10 || MID('$subunit',3,2)=11,
												mid(kunker,1,8) = mid('$subunit',1,8)  ,
												mid(kunker,1,6) = mid('$subunit',1,6)  )
											)
											AND (IF(MID('$subunit',3,2)=10 || MID('$subunit',3,2)=11,
												mid(kunker,11,2) = '00' ,
												mid(kunker,9,4) = '0000' )
											)
											
											ORDER BY KUNKER";
				//echo $sql;
				$query = $this->db->query($sql);
				foreach($query->result_array() as $row)
				{
					$array[] = array($row['KUNKER'] => $row['NUNKER']);
				}
			}
			else
			{
				$array[] = array('100000000000' => 'Semua');
			}
			
		}
		elseif ($_GET['_name'] == 'ssubunit') 
		{
			$ssubunit = $_GET['_value'];
			if($ssubunit != '100000000000')
			{
				$sql = "select * 
					from referensi_unit_kerja
					where mid(kunker,1,8) = '".substr($ssubunit,0,8)."'
					and mid(kunker,11,2) = '00'
					
					order by KUNKER asc
				";
				$sql = "SELECT * 
											FROM referensi_unit_kerja 
											WHERE (IF(MID('$ssubunit',3,2)=10 || MID('$ssubunit',3,2)=11,
												mid(kunker,1,10) = mid('$ssubunit',1,10)  ,
												mid(kunker,1,8) = mid('$ssubunit',1,8)  )
											)
											AND (IF(MID('$ssubunit',3,2)=10 || MID('$ssubunit',3,2)=11,
												1=1,
												mid(kunker,11,2) = '00' )
											)
											
											ORDER BY KUNKER";
				//echo $sql;
				$query = $this->db->query($sql);
				foreach($query->result_array() as $row)
				{
					$array[] = array($row['KUNKER'] => $row['NUNKER']);
				}
			}
			else
			{
				$array[] = array('100000000000' => 'Semua');
			}
			
		}
		
		echo json_encode( $array );
	}
	
	public function _get_csrf_nonce()
	{
		$this->load->helper('string');
		$key   = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$this->session->set_flashdata('csrfkey', $key);
		$this->session->set_flashdata('csrfvalue', $value);

		return array($key => $value);
	}

	public function _valid_csrf_nonce()
	{
		if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE &&
			$this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue'))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	public function _render_page($view, $data=null, $returnhtml=false)//I think this makes more sense
	{

		$this->viewdata = (empty($data)) ? $this->data: $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);

		if ($returnhtml) return $view_html;//This will return html on 3rd argument being true
	}
	
	function kirim_email($target,$subject,$data_messages)
	{
		$this->load->library('email');
		$config['protocol']    = email_protocol;
        $config['smtp_host']    = email_host;
        $config['smtp_port']    = email_port;
        $config['smtp_timeout'] = '7';
        $config['smtp_user']    = email_user;
        $config['smtp_pass']    = email_pass;
        $config['charset']    = 'utf-8';
        $config['newline']    = "\r\n";
        $config['mailtype'] = 'html'; // or html
        $config['validation'] = TRUE; // bool whether to validate email or not      

        $this->email->initialize($config);

        $this->email->from(email_from,header_email_from);
        $this->email->to($target); 

        $this->email->subject($subject);
		
        $this->email->message($this->load->view('aktivasi',$data_messages,TRUE));

        $this->email->send();
	}
	
	function kirim_email_tolak($target,$subject,$data_messages)
	{
		$this->load->library('email');
		$config['protocol']    = email_protocol;
        $config['smtp_host']    = email_host;
        $config['smtp_port']    = email_port;
        $config['smtp_timeout'] = '7';
        $config['smtp_user']    = email_user;
        $config['smtp_pass']    = email_pass;
        $config['charset']    = 'utf-8';
        $config['newline']    = "\r\n";
        $config['mailtype'] = 'html'; // or html
        $config['validation'] = TRUE; // bool whether to validate email or not      

        $this->email->initialize($config);

        $this->email->from(email_from,header_email_from);
        $this->email->to($target); 

        $this->email->subject($subject);
		
        $this->email->message($this->load->view('tolak_aktivasi',$data_messages,TRUE));

        $this->email->send();
	}
	
	function kirim_email_aktivasi($target,$subject,$data_messages)
	{
		$this->load->library('email');
		$config['protocol']    = email_protocol;
        $config['smtp_host']    = email_host;
        $config['smtp_port']    = email_port;
        $config['smtp_timeout'] = '7';
        $config['smtp_user']    = email_user;
        $config['smtp_pass']    = email_pass;
        $config['charset']    = 'utf-8';
        $config['newline']    = "\r\n";
        $config['mailtype'] = 'html'; // or html
        $config['validation'] = TRUE; // bool whether to validate email or not      

        $this->email->initialize($config);

        $this->email->from(email_from,header_email_from);
        $this->email->to($target); 

        $this->email->subject($subject);
		
        $this->email->message($this->load->view('terima_aktivasi',$data_messages,TRUE));

        $this->email->send();
	}
	
	public function registrasi()
	{
		$rows = $this->utilitas_model->get_registrasi($page_row=0,$row_id=0);
		
		$data = array(
				'rows' => $rows,
				'title' => 'Daftar Registrasi Pengguna',
				'subtitle' => ''
		);
		$this->load->view('daftar_registrasi',$data);
	}
	
	public function registrar_edit()
	{
		$keputusan = $this->input->post('keputusan');
		$id = $this->uri->segment(3);
		if(isset($keputusan))
		{
			if($keputusan == 1)
			{
				$activation = $this->ion_auth->activate($id);
		

				if ($activation)
				{
					$user = $this->ion_auth->user($id)->row();
					$target = $user->email;
					$penerima_email = $user->first_name.' '.$user->last_name;
					$subject = "Aktivasi Keanggotaan Sistem Informasi Fakultas Ekonomi Unisba";
					$data = array(
						'processed' => '1',
					);
					$this->ion_auth->update($id, $data);
					$data_messages = array(
						'email' => $target,
						'penerima_email' => $penerima_email,
					);
					$this->kirim_email_aktivasi($target,$subject,$data_messages);
					// redirect them to the auth page
					$this->session->set_flashdata('message', $this->ion_auth->messages());
					redirect("utilitas/registrasi", 'refresh');
				}
			}
			else
			{
					$user = $this->ion_auth->user($id)->row();
					$target = $user->email;
					$penerima_email = $user->first_name.' '.$user->last_name;
					$subject = "Aktivasi Keanggotaan Sistem Informasi Fakultas Ekonomi Unisba";
					$data = array(
						'processed' => '1',
					);
					$this->ion_auth->update($id, $data);
					$data_messages = array(
						'email' => $target,
						'penerima_email' => $penerima_email,
					);
					$this->kirim_email_tolak($target,$subject,$data_messages);
					// redirect them to the auth page
					$this->session->set_flashdata('message', $this->ion_auth->messages());
					redirect("utilitas/registrasi", 'refresh');
			}
		}
		else
		{
			
		}
		($this->uri->segment(3) == TRUE)?$id = $this->uri->segment(3):$id='';		
		$this->data['title'] = $this->lang->line('edit_user_heading');
		$user = $this->ion_auth->user($id)->row();
		$currentGroups = $this->ion_auth->get_users_groups($id)->result();
		$groups=$this->ion_auth->groups()->result_array();
		$s = "select * from d_profil where userid = '$id' ";
	//	echo $s;
		$profil = $this->db->query($s)->row();
	//	print_r($profil);
		$data = array(
			'title' => 'Proses Registrasi Pengguna',
			'subtitle' => '',
			'user' => $user,
			'profil' => $profil,
			'currentGroups' => $currentGroups,
			'groups' => $groups
		);
		
		$this->load->view('registrar_edit',$data);
	}
	
	public function ganti_password()
	{
		($this->uri->segment(3) == TRUE)?$id = $this->uri->segment(3):$id='';
		
		$this->data['title'] = $this->lang->line('edit_user_heading');
		
		$tables = $this->config->item('tables','ion_auth');
        $identity_column = $this->config->item('identity','ion_auth');
        $this->data['identity_column'] = $identity_column;
		
		$user = $this->ion_auth->user($id)->row();
		if(isset($user) && count($user) != 0)
		{
		    $this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
		    $this->form_validation->set_rules('password_confirm', $this->lang->line('create_user_validation_password_confirm_label'), 'required');

			if (isset($_POST) && !empty($_POST))
			{
				// update the password if it was posted
				if ($this->input->post('password'))
				{
					$this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
					$this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');
				}

				if ($this->form_validation->run() === TRUE)
				{
					// update the password if it was posted
					if ($this->input->post('password'))
					{
						$data['password'] = $this->input->post('password');
					}

				// check to see if we are updating the user
				   if($this->ion_auth->update($user->id, $data))
					{
						// redirect them back to the admin page if admin, or to the base url if non admin
						$this->session->set_flashdata('message', $this->ion_auth->messages() );
						if ($this->ion_auth->is_admin())
						{
							redirect('/utilitas/user', 'refresh');
						}
						else
						{
							redirect('/utilitas/user', 'refresh');
						}

					}
					else
					{
						// redirect them back to the admin page if admin, or to the base url if non admin
						$this->session->set_flashdata('message', $this->ion_auth->errors() );
						if ($this->ion_auth->is_admin())
						{
							redirect('/utilitas/user', 'refresh');
						}
						else
						{
							redirect('/utilitas/user', 'refresh');
						}

					}

				}
				
			}

			// display the edit user form
			$this->data['csrf'] = $this->_get_csrf_nonce();

			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
			
			// pass the user to the view
			$this->data['user'] = $user;

			$this->data['password'] = array(
				'name' => 'password',
				'id'   => 'password',
				'type' => 'password',
				'class'  => 'form-control',
			);
			$this->data['password_confirm'] = array(
				'name' => 'password_confirm',
				'id'   => 'password_confirm',
				'type' => 'password',
				'class'  => 'form-control',
			);
			 $this->data['identity'] = array(
					'name'  => 'identity',
					'id'    => 'identity',
					'type'  => 'text',
					'value' => $this->form_validation->set_value('identity', $user->username),
					'class'  => 'form-control',
					'disabled' =>''
				);
			$this->data['title'] = 'Daftar User';
			$this->data['subtitle'] = 'Form Ganti Password';

			$this->load->view('ganti_password', $this->data);
		}
		else
		{
			redirect('/');
		}
	}
	
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */