<!DOCTYPE html>
<!-- Template Name: Rapido - Responsive Admin Template build with Twitter Bootstrap 3.x Version: 1.2 Author: ClipTheme -->
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<?php $this->load->view('header');?> 

    <!-- start: BODY -->
	<body>
		<?php //$this->load->view('sliding-bar');?>
		<div class="main-wrapper">
			<?php $this->load->view('topbar');?>
			<?php $this->load->view('vertikal-menu');?>
			<?php //$this->load->view('page-slide-right');?>
			<!-- start: MAIN CONTAINER -->
			<div class="main-container inner">
				<!-- start: PAGE -->
				<div class="main-content">					
					<!-- end: SPANEL CONFIGURATION MODAL FORM -->
					<div class="container">
						<!-- start: PAGE HEADER -->
						<!-- start: TOOLBAR -->
						<div class="toolbar row">
							<div class="col-sm-6 hidden-xs">
								<div class="page-header">
									<h1><?=$title;?> <small><?=$subtitle;?></small></h1>
								</div>
							</div>
							
						</div>
						<!-- end: TOOLBAR -->
						<!-- end: PAGE HEADER -->
						<!-- start: BREADCRUMB -->
						<div class="row">
							<div class="col-md-12">
								<ol class="breadcrumb">
									<li class="active">
										<a href="/dashboard">
											Dashboard
										</a>
									</li>
								</ol>
							</div>
						</div>
						<!-- end: BREADCRUMB -->
						<!-- start: PAGE CONTENT -->
						
						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-white">
								  <div class="panel-body">
									<div class="row">
												<div class="col-xs-12">
												<!--
												<button id="add" type="button" class="btn btn-success">
												  <span class="glyphicon glyphicon-plus"></span> Tambah Data
												</button>
												-->
												<table id="tabel" class="table table-bordered table-condensed table-responsive table-striped">
												<thead>
													<th style="text-align: center;">No</th>
													<th style="text-align: center;">Username/Email</th>
													<th style="text-align: center;">Nama Lengkap</th>
													<th style="text-align: center;">Grup</th>
													<th style="text-align: center;">Waktu Pendaftaran</th>
													<!--<th style="text-align: center;">Keaktifan</th>-->
													<th style="text-align: center;">Proses</th>
													<!--<th style="text-align: center;">Hapus</th>-->
												</thead>
												<tbody>
												<?php
												$no=1;
												foreach($rows->result_array() as $row)
												{
														
													$url_edit= site_url().'/utilitas/registrar_edit/'.$row['id'];
													$url_del = site_url().'/utilitas/del_user/'.$row['id'];	
														
														
													echo '<tr>';
													echo '<td>'.$no.'</td>';
													echo '<td>'.$row['email'].'</td>';
													echo '<td>'.$row['first_name'].'-'.$row['last_name'].'</td>';
													echo '<td>'.grup_pengguna($row['id']).'</td>';
													echo '<td>'.date('d-m-Y H:i:s',($row['created_on'])).'</td>';
													/* echo '<td>';
																	if(isset($row['active']) && $row['active']==1)
																	{
																		echo '<a class="activate" href="/utilitas/deactivate/'.$row['id'].'">Aktif</a>';
																	}
																	else
																	{
																		echo '<a class="activate" href="/utilitas/activate/'.$row['id'].'">Tidak Aktif</a>';
																	}
													echo '</td>'; */
													echo '<td>
																	<a href="'.$url_edit.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="EDIT">
																		<i class="glyphicon glyphicon-edit"></i>
																	</a>
															</td>';
													/* echo '<td>
																	<a href="'.$url_del.'" class="btn btn-danger del" type="button" data-toggle="tooltip" data-placement="top" title="HAPUS">
																		<i class="glyphicon glyphicon-trash"></i>
																	</a>
															</td>'; */
													echo '</tr>';
													$no++;								
												}
												?>
												</tbody>
												</table>
												</div>
									</div>
								  </div>
								</div>
							</div>
         
						</div>
						<!-- end: PAGE CONTENT-->
					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>
				</div>
				<!-- end: PAGE -->
			</div>
			<!-- end: MAIN CONTAINER -->
			<?php $this->load->view('footer');?>
		</div>
		
		
	</body>
	<!-- end: BODY -->
	
	<?php $this->load->view('javascript')?>
	<script src="/assets/js/dataTables/jquery.dataTables.js"></script>
	<script type="text/javascript">
	$(document).ready(function () {
		$('#add').click(function(){
				window.location.assign("/utilitas/user_add/")
		});	
		
		$(".del").click(function(){
			if (!confirm("Do you want to delete")){
			  return false;
			}
		});
	  
	  $('#tabel').dataTable({
							"bFilter": true, 
							"bInfo": true,
							"bLengthChange": false,
							"pageLength":5
		});
		
		$(".activate").click(function(){
			if (!confirm("Yakin Merubah Status")){
			  return false;
			}
		});
							
	});
	</script>
</html>
<?php
function grup_pengguna($id)
{
	$CI = $CI =& get_instance();
	$s = "select a.*,b.name 
			from t_users_groups a
			left join t_groups b on(a.group_id = b.id)
			where a.user_id = '$id'
		";
	$q = $CI->db->query($s);
	$res = '';
	if($q->num_rows() > 0)
	{
		$res .= '<ul>';
		foreach($q->result_array() as $row)
		{
			$res .= '<li>'.$row['name'].'</li>';
		}
		$res .='</ul>';
	}
	return $res;
}
?>