<?php $this->load->view('header');?> 
<script type="text/javascript">
$(document).ready(function () {
	$('#add').click(function(){
			window.location.assign("<?=site_url();?>/utilitas/user_add/")
	});			
	$(".del").click(function(){
    if (!confirm("Do you want to delete")){
      return false;
    }
  });
});
function view_detail(userid)
{
	window.location.assign("<?=site_url();?>/utilitas/detail_log_pengguna/"+userid);
}
</script>
    <div class="container-fluid documents">
    <!--- content -->
     <!-- CENTER Content -->
		<div class="col-xs-12">
			<div class="panel panel-success">
				  <div class="panel-heading">
					<h3 class="panel-title">Daftar Menu</h3>
				  </div>
				  <div class="panel-body">
					<div class="row">
								<div class="col-xs-12">
								<table class="table table-bordered table-condensed table-responsive table-striped">
								<thead>
								<tr>
									<th style="text-align: center;">No</th>
									<th style="text-align: center;">Username</th>
									<th style="text-align: center;">NIP</th>
									<th style="text-align: center;">Nama</th>
									<th style="text-align: center;">Email</th>
									<th style="text-align: center;">SKPD Kewenangan</th>
									<th style="text-align: center;">Waktu Login Terakhir</th>
								</tr>
								<?php
								$no=1;
								foreach($rows->result_array() as $row)
								{
									echo '<tr onclick="view_detail(\''.$row['userid'].'\')">';
									echo '<td>'.$no.'</td>';
									echo '<td>'.$row['userid'].'</td>';
									echo '<td>'.$row['nip'].'</td>';
									echo '<td>'.$row['nama'].'</td>';
									echo '<td>'.$row['email'].'</td>';
									echo '<td></td>';
									echo '<td>'.date('d-m-Y H:i:s',strtotime($row['userlastvisit'])).'</td>';
									
									$no++;								
								}
								?>
								</table>
								</div>
					</div>
				  </div>
			</div>
		</div>
         
	</div>
<?php $this->load->view('footer');?> 
