<html>
<?php $this->load->view('header');?> 
<body>
		<?php $this->load->view('sliding-bar');?>
		<div class="main-wrapper">
			<?php $this->load->view('topbar');?>
			<?php $this->load->view('vertikal-menu');?>
			<?php $this->load->view('page-slide-right');?>
			<!-- start: MAIN CONTAINER -->
			<div class="main-container inner">
				<!-- start: PAGE -->
				<div class="main-content">					
					<!-- end: SPANEL CONFIGURATION MODAL FORM -->
					<div class="container">
						<!-- start: PAGE HEADER -->
						<!-- start: TOOLBAR -->
						<div class="toolbar row">
							<div class="col-sm-6 hidden-xs">
								<div class="page-header">
									<h1><?=$title;?> <small><?=$subtitle;?></small></h1>
								</div>
							</div>
							
						</div>
						<!-- end: TOOLBAR -->
						<!-- end: PAGE HEADER -->
						<!-- start: BREADCRUMB -->
						<div class="row">
							<div class="col-md-12">
								<ol class="breadcrumb">
									<li class="active">
										<a href="/dashboard">
											Dashboard
										</a>
									</li>
								</ol>
							</div>
						</div>
						<!-- end: BREADCRUMB -->
						<!-- start: PAGE CONTENT -->
						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-white">
								
									<div class="panel-body">
										<?php echo validation_errors(); ?>
											<?php echo form_open("utilitas/deactivate/".$user->id);?>

											  <p>
												<?php echo lang('deactivate_confirm_y_label', 'confirm');?>
												<input type="radio" name="confirm" value="yes" checked="checked" />
												<?php echo lang('deactivate_confirm_n_label', 'confirm');?>
												<input type="radio" name="confirm" value="no" />
											  </p>

											  <?php echo form_hidden($csrf); ?>
											  <?php echo form_hidden(array('id'=>$user->id)); ?>

											  <p><?php echo form_submit('submit', 'Deaktivasi');?></p>

											<?php echo form_close();?>
									</div>
								</div>
							</div>
						<div class="col-xs-1">
						</div>
						</div>
					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>
				</div>
				<!-- end: PAGE -->
			</div>
			<!-- end: MAIN CONTAINER -->
			<?php $this->load->view('footer');?>
		</div>
		<?php $this->load->view('javascript')?>
		<script src="<?=base_url();?>assets/js/jquery.chainedSelects.js"></script>
		<script type="text/javascript">
		$(document).ready(function () {
			required = ["first_name","last_name","email","password","password_confirm"];
			errornotice = $("#error");
			emptyerror = "Mohon field ini diisi.";
			$('#simpan_form').click(function(){
				//Validate required fields
				for (i=0;i<required.length;i++) {
					var input = $('#'+required[i]);
					if ((input.val() == "") || (input.val() == emptyerror)) {
						input.addClass("needsfilled");
						input.val(emptyerror);
						errornotice.fadeIn(750);
					} else {
						input.removeClass("needsfilled");
					}
				}
					
				//if any inputs on the page have the class 'needsfilled' the form will not submit
				if ($(":input").hasClass("needsfilled")) {
					return false;
				} else {
					errornotice.hide();
					$('#frm_util').submit();
				}
												  
			});
											
			$(":input").focus(function(){		
			   if ($(this).hasClass("needsfilled") ) {
					$(this).val("");
					$(this).removeClass("needsfilled");
				}
			});
			
			$('#batal').click(function(){
				parent.history.back();
				return false;
			});
			
				$('#unit').chainSelect('#skpd_kewenangan','<?=site_url();?>/utilitas/combobox_unit',
				{ 
					before:function (target) //before request hide the target combobox and display the loading message
					{ 
						$("#loading").css("display","block");
						$(target).css("display","none");
					},
					after:function (target) //after request show the target combobox and hide the loading message
					{ 
						$("#loading").css("display","none");
						$(target).css("display","inline");
					}
				});
		});
		</script>
</html>