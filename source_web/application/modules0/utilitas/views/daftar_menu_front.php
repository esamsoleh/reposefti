<!DOCTYPE html>
<!-- Template Name: Rapido - Responsive Admin Template build with Twitter Bootstrap 3.x Version: 1.2 Author: ClipTheme -->
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<?php $this->load->view('header');?>
	
	<!-- start: BODY -->
	<body>
		<?php $this->load->view('sliding-bar');?>
		<div class="main-wrapper">
			<?php $this->load->view('topbar');?>
			<?php $this->load->view('vertikal-menu');?>
			<?php $this->load->view('page-slide-right');?>
			<!-- start: MAIN CONTAINER -->
			<div class="main-container inner">
				<!-- start: PAGE -->
				<div class="main-content">					
					<!-- end: SPANEL CONFIGURATION MODAL FORM -->
					<div class="container">
						<!-- start: PAGE HEADER -->
						<!-- start: TOOLBAR -->
						<div class="toolbar row">
							<div class="col-sm-6 hidden-xs">
								<div class="page-header">
									<h1><?=$title;?> <small><?=$subtitle;?></small></h1>
								</div>
							</div>
							
						</div>
						<!-- end: TOOLBAR -->
						<!-- end: PAGE HEADER -->
						<!-- start: BREADCRUMB -->
						<div class="row">
							<div class="col-md-12">
								<ol class="breadcrumb">
									<li class="active">
										<a href="/dashboard">
											Dashboard
										</a>
									</li>
								</ol>
							</div>
						</div>
						<!-- end: BREADCRUMB -->
						<!-- start: PAGE CONTENT -->
						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-white">
									<div class="panel-body">
										<div class="row">
											<div class="col-xs-12">
											<button id="add" type="button" class="btn btn-success">
											  <span class="glyphicon glyphicon-plus"></span> Tambah Data
											</button>
											</br>
											</br>
											<table id="tabel" class="table table-bordered table-condensed table-responsive table-striped">
											<thead>
												<th style="text-align: center;">No</th>
												<th style="text-align: center;">Nama Menu</th>
												<th style="text-align: center;">Deskripsi Menu</th>
												<th style="text-align: center;">Link</th>
												<th style="text-align: center;">Parent</th>
												<th style="text-align: center;">Urutan</th>
												<th style="text-align: center;">Keaktifan</th>
												<th style="text-align: center;" >Edit</th>
												<th style="text-align: center;" >Hapus</th>
											</thead>
											<tbody>
											<?php
											$no=1;
											foreach($rows->result_array() as $row)
											{
													
												$url_edit= '/utilitas/menu_front_edit/'.$row['menuid'];
												$url_del = '/utilitas/del_menu_front/'.$row['menuid'];	
													
													
												echo '<tr>';
												echo '<td>'.$no.'</td>';
												echo '<td>'.$row['namamenu'].'</td>';
												echo '<td>'.$row['deskripsi'].'</td>';
												echo '<td>'.$row['linkaction'].'</td>';
												echo '<td>'.$row['parent'].'</td>';
												echo '<td>'.$row['urutan'].'</td>';
												echo '<td>'.((isset($row['aktif']) && $row['aktif']==1)?'Aktif':'Tidak Aktif').'</td>';
												echo '<td>
																<a href="'.$url_edit.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="EDIT">
																	<i class="glyphicon glyphicon-edit"></i>
																</a>
													</td>';
												echo '<td>		
																<a href="'.$url_del.'" class="btn btn-danger del" type="button" data-toggle="tooltip" data-placement="top" title="HAPUS">
																	<i class="glyphicon glyphicon-trash"></i>
																</a>
														</td>';
												echo '</tr>';
												$no++;								
											}
											?>
											</tbody>
											</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- end: PAGE CONTENT-->
					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>
				</div>
				<!-- end: PAGE -->
			</div>
			<!-- end: MAIN CONTAINER -->
			<?php $this->load->view('footer');?>
		</div>
		
	</body>
	<!-- end: BODY -->
	<?php $this->load->view('javascript')?>
		
		<script src="/assets/js/dataTables/jquery.dataTables.js"></script>
		<script src="/assets/js/dataTables/dataTables.bootstrap.js"></script>
		<script type="text/javascript">
		$(document).ready(function () {
			$('#add').click(function(){
					window.location.assign("<?=site_url();?>/utilitas/menu_front_add/")
			});			
			$(".del").click(function(){
			if (!confirm("Do you want to delete")){
			  return false;
			}
		  });
		  
		   $('#tabel').dataTable({
							"bFilter": true, 
							"bInfo": true,
							"bLengthChange": false,
							});
		});
		</script>
		
</html>
