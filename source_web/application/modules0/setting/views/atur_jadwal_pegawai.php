<?php $this->load->view('header');?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('top');?>
  <!-- Left side column. contains the logo and sidebar -->
  
  <?php $this->load->view('sidebar');?>
 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title;?>
        <small><?=$subtitle;?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-white">
					<div class="panel-body">
						<div class="row">
							<form name="formPegawai" id="formPegawai" method="post" action="/log/harian" class="form-horizontal">
							
							<div class="row">
									<label class="col-sm-2 control-label">Bulan</label>
								<div class="col-xs-4">
									<select class="form-control" name="bulan" id="bulan">
										<option value="1" <?=($bulan == 1?'selected':'');?>>Januari</option>
										<option value="2" <?=($bulan == 2?'selected':'');?>>Februari</option>
										<option value="3" <?=($bulan == 3?'selected':'');?>>Maret</option>
										<option value="4" <?=($bulan == 4?'selected':'');?>>April</option>
										<option value="5" <?=($bulan == 5?'selected':'');?>>Mei</option>
										<option value="6" <?=($bulan == 6?'selected':'');?>>Juni</option>
										<option value="7" <?=($bulan == 7?'selected':'');?>>Juli</option>
										<option value="8" <?=($bulan == 8?'selected':'');?>>Agustus</option>
										<option value="9" <?=($bulan == 9?'selected':'');?>>September</option>
										<option value="10" <?=($bulan == 10?'selected':'');?>>Oktober</option>
										<option value="11" <?=($bulan == 11?'selected':'');?>>Nopember</option>
										<option value="12" <?=($bulan == 12?'selected':'');?>>Desember</option>
									</select>
								</div>
								
									<label  class="col-sm-2 control-label">Tahun</label>
								<div class="col-xs-4">
									<input class="form-control" type="text" name="tahun" id="tahun" value="<?=$tahun;?>">
								</div>
							</div>
							
							</br>
							
							</form>					
						</div>										
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">		
			<div class="col-xs-12">
				<div class="panel panel-primary">
                    <div class="panel-heading">
                        Daftar Pegawai
                    </div>
					<div class="panel-body">
						<div class="row">
								<label class="col-sm-2 control-label">NIP</label>
								<div class="col-xs-4">
									<?=$nip;?>
								</div>						
						</div>
						<div class="row">
								<label class="col-sm-2 control-label">Nama</label>
								<div class="col-xs-4">
									<?=$nama;?>
									
								</div>						
						</div>
						<div class="row">
								<label class="col-sm-2 control-label">Jabatan</label>
								<div class="col-xs-4">
									<?=$nama_jabatan;?>
									
								</div>						
						</div>
					<hr>
                        <div class="table-responsive">
							<table class="table table-striped table-bordered table-hover" id="tabel">
							<thead>
							<tr>
								<th>No</th>
								<th>Hari</th>
								<th>Tanggal</th>
								<th>Jadwal Masuk</th>
								<th>Jadwal Keluar</th>
								<th>Libur</th>
								<th>Keterangan</th>
								<th>Edit</th>
							</tr>
							</thead>
							<tbody>
							
							</tbody>
							</table>
						</div>
					
					</div>
				</div>
			</div>
		
		</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
 
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('javascript');?>

<script src="/assets/js/dataTables/jquery.dataTables.js"></script>
<script src="/assets/js/dataTables/dataTables.bootstrap.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
		
		var oTable = $("#tabel").dataTable({
			"oLanguage" : {
				"sSearch" : "Search",
				"oPaginate" : {
				"sFirst":"First",
				"sPrevious" : "Previous",
				"sNext" : "Next",
				"sLast":"Last"
			}
			}, 
			"ordering": false,
			"iDisplayLength" : 100,
			"bFilter" : false,               
			"bLengthChange":false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": "/setting/get_jadwal_piket_pegawai",
				"type": "POST",
				"data": {
							'tahun':function(){return $("#tahun").val(); },
							'bulan': function(){return $("#bulan").val(); },
							'id_tipe_jadwal': '<?=$id_tipe_jadwal;?>',
							'nip':'<?=$nip;?>',
							
						}
			},
			"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                    if ( aData[5] == "Ya" )
                    {
                        $('td', nRow).css('background-color', 'Red');
                    }
                    
                }
		});  
		
		$('#tahun,#bulan').on( 'change', function () {
			oTable.fnDraw();
		} );
		
		
		});
</script>
</body>

</html>
