<?php $this->load->view('header');?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('top');?>
  <!-- Left side column. contains the logo and sidebar -->
  
  <?php $this->load->view('sidebar');?>
 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title;?>
        <small><?=$subtitle;?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-white">
					<div class="panel-body">
						<div class="row">
							<form name="formPegawai" id="formPegawai" method="post" action="/log/harian" class="form-horizontal">
							<input type="hidden" name="cari" value="1">
							<div class="row">
									<label class="col-sm-2 control-label">Project</label>
								<div class="col-xs-10">
									<select class="form-control" name="id_place" id="id_place">
									<?php
										$sql = "select id_place,nama 
												from d_project
												where status = 0
										";
										$query = $this->db->query($sql);
										//echo '<option value="0" '.((isset($id_place) && ($id_place == '0'))?"selected":"").'>Semua</option>';			
										foreach ($query->result() as $row)
										{ 
											echo '<option value ="'.$row->id_place.'" '.((isset($id_place) && $id_place == $row->id_place)?"selected":"").'>'.($row->nama).'</option>';
										} 								
									
									?>
									
										
									</select>
								</div>
							</div>
							<div class="row">
									<label class="col-sm-2 control-label">Floor</label>
								<div class="col-xs-10">
									<select class="form-control" name="id_floor" id="id_floor">
									<?php 
									
											$sql = "SELECT id_floor,nama
													from d_site
													";
											if(isset($id_place) && $id_place != '')
											{
												$sql .='where id_place = "'.$id_place.'" ';
											}
											
											//echo $sql;
											$query = $this->db->query($sql);
											foreach ($query->result() as $row)
											{ 
												echo '<option value ="'.$row->id_floor.'" '.((isset($id_floor) && $id_floor == $row->id_floor)?"selected":"").'>'.($row->nama).'</option>';
											}
																		
									?>	
									</select>
								</div>
							</div>
							</br>
							</br>
							<div class="form-group row">
								<div class="col-md-2">
								</div>
								<?php 
									$s_access = $this->session->userdata('s_access');
									if($s_access == 1)
									{
									?>
										<div class="col-md-2">
											<button id="add" class="btn btn-block btn-success" type="button">Tambah Data</button>
										</div>
									<?php
									}
									?>
												
							</div>
							</br>
														
							</form>					
						</div>										
					</div>
				</div>
			</div>
			<div class="col-xs-1">
			</div>
		</div>
		<div class="row">
		
			<div class="col-xs-12">
				<div class="panel panel-primary">
                    <div class="panel-heading">
                        Daftar Denah
                    </div>
					<div class="panel-body">
					
					<hr>
                        <div class="table-responsive">
							<table class="table table-striped table-bordered table-hover" id="tabel">
							<thead>
							<tr>
								<th>No</th>
								<th>PROYEK</th>
								<th>FLOOR</th>
								<th>DENAH</th>
								<th>KML</th>
								<th>Gambar</th>
								<th>Edit</th>
								<th>Hapus</th>
							</tr>
							</thead>
							<tbody>
							
							</tbody>
							</table>
						</div>
					
					</div>
				</div>
			</div>
		
		</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
 
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('javascript');?>
<script src="/assets/js/jquery.chainedSelects.js"></script>
<script src="/assets/js/dataTables/jquery.dataTables.js"></script>
<script src="/assets/js/dataTables/dataTables.bootstrap.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$('#add').click(function(){
			window.location.assign("/setting/tambah_denah/")
		});	
		
		var oTable = $("#tabel").dataTable({
			"oLanguage" : {
				"sSearch" : "Search",
				"oPaginate" : {
				"sFirst":"First",
				"sPrevious" : "Previous",
				"sNext" : "Next",
				"sLast":"Last"
			}
			}, 
			"ordering": false,
			"iDisplayLength" : 10,
			"bFilter" : false,               
			"bLengthChange": true,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": "/setting/get_denah",
				"type": "POST",
				"data": {
							'id_place':function(){return $("#id_place").val(); },
							'id_floor': function(){return $("#id_floor").val(); } ,
						}
			}
		});  
		
		/* $('#nama,#reqnip').on( 'keyup', function () {
			oTable.fnDraw();
		} );
		 */
		$('#id_floor').on( 'change', function () {
			oTable.fnDraw();
		} );
		
		$('#id_place').chainSelect('#id_floor','/setting/combo_floor',
		{ 
			before:function (target) //before request hide the target combobox and display the loading message
			{ 
				$("#loading").css("display","block");
				$(target).css("display","none");
			},
			after:function (target) //after request show the target combobox and hide the loading message
			{ 
				$("#loading").css("display","none");
				$(target).css("display","inline");
			}
		});
		
		});
		</script>
</body>
</html>
