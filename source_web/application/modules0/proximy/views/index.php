<?php $this->load->view('header');?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('top');?>
  <!-- Left side column. contains the logo and sidebar -->
  
  <?php $this->load->view('sidebar');?>
 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

     <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
		<div class="col-md-12">
          <!-- BAR CHART -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <i class="fa fa-bar-chart-o"></i>

              <h3 class="box-title">Grafik</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              
            </div>
            <!-- /.box-body-->
          </div>
          <!-- /.box -->
        </div>
		</div>
		
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
 
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('javascript');?>
<!-- FLOT CHARTS -->
<script src="/assets/components/Flot/jquery.flot.js"></script>
<script src="/assets/components/Flot/jquery.flot.valuelabels.js"></script>
<!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
<script src="/assets/components/Flot/jquery.flot.resize.js"></script>
<!-- FLOT PIE PLUGIN - also used to draw donut charts -->
<script src="/assets/components/Flot/jquery.flot.pie.js"></script>
<!-- FLOT CATEGORIES PLUGIN - Used to draw bar charts -->
<script src="/assets/components/Flot/jquery.flot.categories.js"></script>
<script type="text/javascript">
		$(document).ready(function () {
		data =
        [{ label: 'Pegawai Terlambat Masuk',  data:[[1,<?=$jml_terlambat;?>]], color: '#ea0442' },
			    { label: 'Pegawai Tidak Masuk', data:[[2,<?=$jml_tidak_masuk;?>]], color: '#e841f4' },
				{ label: 'Pegawai Belum Masuk',  data:[[3,<?=$jml_blm_masuk;?>]], color: '#f48641' },
				{ label: 'Pegawai Pulang Cepat',  data:[[4,<?=$jml_pulang_cepat;?>]], color: '#f46841' },
		];
		
		
         options =
         {
			  grid  : {
				borderWidth: 1,
				borderColor: '#f3f3f3',
				tickColor  : '#f3f3f3'
			  },
            series:
            {
               bars: {show: true},
               valueLabels:
               {
                  show: true
               }
            },
            bars:  {show    : true,
				  barWidth: 0.5,
				  align   : 'center'},
			legend: {
			show: true
			},
			xaxis : {
				mode      : 'categories',
				tickLength: 0
			  }
         };

         $.plot($('#cakupan_chart'), data, options);
		 
		 data =
        [{ label: 'TKK Terlambat Masuk',  data:[[1,<?=$jml_tkk_terlambat;?>]], color: '#ea0442' },
			    { label: 'TKK Tidak Masuk', data:[[2,<?=$jml_tkk_tidak_masuk;?>]], color: '#e841f4' },
				{ label: 'TKK Belum Masuk',  data:[[3,<?=$jml_tkk_blm_masuk;?>]], color: '#f48641' },
				{ label: 'TKK Pulang Cepat',  data:[[4,<?=$jml_tkk_pulang_cepat;?>]], color: '#f46841' },
		];
		
		
         options =
         {
			  grid  : {
				borderWidth: 1,
				borderColor: '#f3f3f3',
				tickColor  : '#f3f3f3'
			  },
            series:
            {
               bars: {show: true},
               valueLabels:
               {
                  show: true
               }
            },
            bars:  {show    : true,
				  barWidth: 0.5,
				  align   : 'center'},
			legend: {
			show: true
			},
			xaxis : {
				mode      : 'categories',
				tickLength: 0
			  }
         };

         $.plot($('#tkk_chart'), data, options);
		 
		});
	function labelFormatter(label, series) {
		return '<div style="font-size:13px; text-align:center; padding:2px; color: #050405; font-weight: 600;">'
      + label
      + '<br>'
      + Math.round(series.percent) + '%</div>'
	}
		</script>
</body>
</html>
