<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Proximy extends CI_Controller {
	public $apps;
	var $nama_user;
	var $file_foto;
	var $s_biro;
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
		if (!$this->ion_auth->logged_in())
			{
				redirect('/auth/login', 'refresh');
			}
		$this->apps = 'dashboard';
		$this->s_biro = $this->session->userdata('s_biro');
		$this->nama_user = $this->ion_auth->user()->row()->first_name.' '.$this->ion_auth->user()->row()->last_name;
		$this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	}
	
	public function index()
	{
		
		$data = array(
			'title' => 'Dashboard',
		);
		$this->load->view('index',$data);
	}
	
	public function login()
	{
		$fields = array(
			'email' => 'denameidina97@gmail.com',
			'password' => 'awaspanas',
		);
		$fields_string = '';
		foreach($fields as $key=>$value) 
		{ 
			$fields_string .= $key.'='.$value.'&'; 
		}
		rtrim($fields_string, '&');
		
		//login
		$ch = curl_init();
		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL, 'https://api.proximi.fi/core_auth/login');
		curl_setopt($ch,CURLOPT_POST, count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, TRUE);

		//execute post
		$result = curl_exec($ch);
		curl_close($ch);
		
		$result = json_decode($result,TRUE);
		echo '</br>';
	//	print_r($result);
		echo '</br>';
		echo $result['user']['name'];
		echo '</br>';
		echo $result['token'];
		$token = $result['token'];
		
		//cek visitor overview
		$ch = curl_init();
		$authorization = "Authorization: Bearer ".$token;
		curl_setopt($ch,CURLOPT_URL, 'http://api.proximi.fi/analytics/overview?date_from=2016-01-01&date_to=2016-04-01');
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, TRUE);  // Prepare the authorisation token
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization )); // Inject the token into the header
       
		$result = curl_exec($ch);
		$result = json_decode($result,TRUE);
		/* echo '</br>';
		echo '</br>';
		print_r($result); */
		//close connection
		curl_close($ch);
		
		//cek aplikasi
		$ch = curl_init();
		$authorization = "Authorization: Bearer ".$token;
		curl_setopt($ch,CURLOPT_URL, 'http://api.proximi.fi/core/applications');
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, TRUE);  // Prepare the authorisation token
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization )); // Inject the token into the header
       
		$result = curl_exec($ch);
		$result = json_decode($result,TRUE);
	//	echo '</br>';
	//	echo '</br>';
		foreach($result as $res)
		{
			/* print_r($res);
			echo '</br>';
			echo '</br>'; */
		}
		//close connection
		curl_close($ch);
		
		//cek event
		$ch = curl_init();
		$authorization = "Authorization: Bearer ".$token;
		curl_setopt($ch,CURLOPT_URL, 'http://api.proximi.fi/core/events');
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, TRUE);  // Prepare the authorisation token
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization )); // Inject the token into the header
       
		$result = curl_exec($ch);
		$result = json_decode($result,TRUE);
		//echo '</br>';
		//echo '</br>';
		//print_r($result);
		echo '</br>';
		foreach($result as $res)
		{
			/* print_r($res);
			echo '</br>';
			echo '</br>'; */
			/* if(isset($res['geopoint']) and count($res['geopoint']) != 0)
			{
				print_r($res['geopoint']);
				echo '</br>';
			} */
		}
		//close connection
		curl_close($ch);
		
		//cek geofences
		$ch = curl_init();
		$authorization = "Authorization: Bearer ".$token;
		curl_setopt($ch,CURLOPT_URL, 'http://api.proximi.fi/core/geofences');
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, TRUE);  // Prepare the authorisation token
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization )); // Inject the token into the header
       
		$result = curl_exec($ch);
		$result = json_decode($result,TRUE);
	//	echo '</br>';
	//	echo '</br>';
	//	print_r($result);
		//close connection
		curl_close($ch);
		
		//cek places
		$ch = curl_init();
		$authorization = "Authorization: Bearer ".$token;
		curl_setopt($ch,CURLOPT_URL, 'http://api.proximi.fi/core/places');
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, TRUE);  // Prepare the authorisation token
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization )); // Inject the token into the header
       
		$result = curl_exec($ch);
		$result = json_decode($result,TRUE);
	//	echo '</br>';
	//	echo '</br>';
		foreach($result as $res)
		{
			/* print_r($res);
			echo '</br>';
			echo '</br>'; */
		}
		
		//close connection
		curl_close($ch);
		
		//cek visitor
		$ch = curl_init();
		$authorization = "Authorization: Bearer ".$token;
		curl_setopt($ch,CURLOPT_URL, 'https://api.proximi.fi/core/visitors/');
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, TRUE);  // Prepare the authorisation token
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization )); // Inject the token into the header
       
		$result = curl_exec($ch);
		$result = json_decode($result,TRUE);
		
		foreach($result as $res)
		{
			/* print_r($res);
			echo '</br>';
			echo '</br>';
			if($res['id'] != '')
			{
				$visitors[] = $res['id'];
			} */
			
		}
		//close connection
		curl_close($ch);
		
		/* foreach($visitors as $vis)
		{
			//echo 'id visitor:'.$vis.'</br>';
			//cek visitor
			$ch = curl_init();
			$authorization = "Authorization: Bearer ".$token;
			curl_setopt($ch,CURLOPT_URL, 'https://api.proximi.fi/analytics/visitor/'.$vis);
			curl_setopt($ch,CURLOPT_RETURNTRANSFER, TRUE);  // Prepare the authorisation token
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization )); // Inject the token into the header
		   
			$result = curl_exec($ch);
			$result = json_decode($result,TRUE);
			
			//foreach($result as $res)
		//	{
				echo 'analisis visitor:';
				print_r($result);
				echo '</br>';
				echo '</br>';
		//	}
			//close connection
			curl_close($ch);
		} */
		
		 //cek input
		$ch = curl_init();
		$authorization = "Authorization: Bearer ".$token;
		curl_setopt($ch,CURLOPT_URL, 'https://api.proximi.fi/core/inputs');
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, TRUE);  // Prepare the authorisation token
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization )); // Inject the token into the header
       
		$result = curl_exec($ch);
		$result = json_decode($result,TRUE);
		foreach($result as $res)
		{
			print_r($res);
			echo '</br>';
			echo '</br>';
		}
		//close connection
		curl_close($ch);
		/*
		//cek user
		$ch = curl_init();
		$authorization = "Authorization: Bearer ".$token;
		curl_setopt($ch,CURLOPT_URL, 'https://api.proximi.fi/core/current_user');
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, TRUE);  // Prepare the authorisation token
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization )); // Inject the token into the header
       
		$result = curl_exec($ch);
		$result = json_decode($result,TRUE);
		//print_r($result);
		//close connection
		curl_close($ch); */
		
		//start GEOFENCE
		$ch = curl_init();
		$authorization = "Authorization: Bearer ".$token;
		curl_setopt($ch,CURLOPT_URL, 'https://api.proximi.fi/core/geofences');
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, TRUE);  
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));        
		$result = curl_exec($ch);
		$result = json_decode($result);
		foreach($result as $rest)
		{
			$data = array(
				'address' => $rest->address,
				'area' => json_encode($rest->area),
				'createdAt' => date('Y-m-d H:i:s',strtotime($rest->createdAt)),
				'department_id' => ($rest->department_id),
				'id_geofence' => ($rest->id),
				'name' => ($rest->name),
				'organization_id' => ($rest->organization_id),
				'place_id' => ($rest->place_id),
				'radius' => ($rest->radius),
				'updatedAt' => date('Y-m-d H:i:s',strtotime($rest->updatedAt)),
			//	'id_project' => $this->get_id_project($rest->place_id),
			);
			
		}
		print_r($data);
		curl_close($ch);
		//end update GEOFENCE
	}
}
