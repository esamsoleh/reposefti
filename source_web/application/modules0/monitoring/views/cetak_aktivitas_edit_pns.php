<?php
$array_bulan = array(
	'1' => 'Januari',
	'2' => 'Februari',
	'3' => 'Maret',
	'4' => 'April',
	'5' => 'Mei',
	'6' => 'Juni',
	'7' => 'Juli',
	'8' => 'Agustus',
	'9' => 'September',
	'10' => 'Oktober',
	'11' => 'Nopember',
	'12' => 'Desember',
);
?>
<html>
<head>
<style>
table.tableu {
  border-collapse: collapse;
}
table.tableu td , th  {
  border: 1px solid #999;
  padding: 0.5rem;
  text-align: left;
  vertical-align:top;
}

table.table1 {
  border-collapse: collapse;
}
table.table1 td , th  {
  padding: 0.5rem;
  text-align: left;
  vertical-align:top;
}
</style>
</head>
<body>
<h3>Data Aktivitas Edit Kehadiran Harian Pegawai PNS di <?=$unit_kerja;?> pada Bulan <?=$array_bulan[$bulan];?> Tahun <?=$tahun;?></h3>
<table class="tableu" id="tableu">
	<tr><td>No</td>
		<td>Operator Edit</td>
		<td>IP pengeditan</td>
		<td>Waktu Pengeditan</td>
		<td>NIP / Nama Pegawai Diedit</td>
		<td>Unit Kerja Pegawai</td>
		<td>Perubahan Data</td>
		<td>Log Absen</td>
	</tr>
	<?php
	$no=1;
	foreach($q->result() as $sheet)
	{
		echo '<tr>';
		echo '<td>'.$no.'</td>';
		echo '<td>'.$sheet->nip_pengedit.'</br>'.$sheet->nama_pengedit.'</br>'.$sheet->username_pengedit.'</td>';
		echo '<td>'.$sheet->ip.'</td>';
		echo '<td>'.$sheet->update_date.'</td>';
		echo '<td>'.$sheet->nip_pegawai.'</br>'.$sheet->nama_pegawai.'</td>';
		echo '<td>'.$sheet->unit_kerja_pegawai.'</td>';
		echo '<td>'.getPerubahanDataAbsen($sheet->id_data,$sheet->data_lama).'</td>';
		echo '<td>'.getLog($sheet->nip_pegawai,$sheet->tgl_data).'</td>';
		echo '</tr>';
		$no++;
	}
	?>
</table>
</body>
</html> 
<?php
function getPerubahanDataAbsen($id,$data)
	{
		$CI =& get_instance();
		$row = $CI->db->get_where('d_absen_harian',array('id'=>$id))->row();
		$rowdatalama = unserialize($data);
		$res = '<table class="table1">';
		$res .='<tr><td>No</td><td>Komponen</td><td>Data Baru</td><td>Data Lama</td></tr>';
		$res .= '<tr><td>1</td><td>Jadwal Masuk</td><td>'.($row->jadwal_masuk).'</td><td>'.($rowdatalama->jadwal_masuk).'</td></tr>';
		$res .= '<tr><td>2</td><td>Realisasi Masuk</td><td>'.($row->realisasi_masuk).'</td><td>'.($rowdatalama->realisasi_masuk).'</td></tr>';
		$res .= '<tr><td>3</td><td>Keterlambatan Masuk</td><td>'.($row->keterlambatan_masuk).'</td><td>'.($rowdatalama->keterlambatan_masuk).'</td></tr>';
		$res .= '<tr><td>4</td><td>Potongan Terlambat Masuk</td><td>'.($row->potongan_terlambat_masuk).'</td><td>'.($rowdatalama->potongan_terlambat_masuk).'</td></tr>';
		$res .= '<tr><td>5</td><td>Jadwal Keluar</td><td>'.($row->jadwal_keluar).'</td><td>'.($rowdatalama->jadwal_keluar).'</td></tr>';
		$res .= '<tr><td>6</td><td>Realisasi Keluar</td><td>'.($row->realisasi_keluar).'</td><td>'.($rowdatalama->realisasi_keluar).'</td></tr>';
		$res .= '<tr><td>7</td><td>Kecepatan Keluar</td><td>'.($row->kecepatan_keluar).'</td><td>'.($rowdatalama->kecepatan_keluar).'</td></tr>';
		$res .= '<tr><td>8</td><td>Potongan Pulang Cepat</td><td>'.($row->potongan_pulang_cepat).'</td><td>'.($rowdatalama->potongan_pulang_cepat).'</td></tr>';
		$res .= '<tr><td>9</td><td>Jenis Tidak Masuk</td><td>'.($row->jenis_tidak_masuk).'</td><td>'.($rowdatalama->jenis_tidak_masuk).'</td></tr>';
		$res .= '<tr><td>10</td><td>Potongan Tidak Masuk</td><td>'.($row->potongan_tidak_masuk).'</td><td>'.($rowdatalama->potongan_tidak_masuk).'</td></tr>';
		$res .= '<tr><td>11</td><td>Libur</td><td>'.($row->libur).'</td><td>'.($rowdatalama->libur).'</td></tr>';
		$res .= '<tr><td>12</td><td>Persentase Potongan</td><td>'.($row->persentase_potongan).'</td><td>'.($rowdatalama->persentase_potongan).'</td></tr>';
		$res .= '</table>';
		return $res;
	}
	
	function getLog($nip,$tanggal)
	{
		$CI =& get_instance();
		$s = "select * from absensi_log where nip = '$nip' and date(tanggal) = '$tanggal'";
		$q = $CI->db->query($s);
		$res = '<table>';
		foreach($q->result() as $row)
		{
			$res .='<tr><td>'.$row->tanggal.'</td></tr>';
		}
		$res .='</table>';
		return $res;
	}
	
	?>