<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Monitoring extends CI_Controller {
	public $apps;
	var $nama_user;
	var $file_foto;
	var $uri_string;
	var $s_biro;
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
		if (!$this->ion_auth->logged_in())
			{
				redirect('/auth/login', 'refresh');
			}
		$this->apps = 'Monitoring';
		$this->load->model('data_model');
		$this->nama_user = $this->ion_auth->user()->row()->first_name.' '.$this->ion_auth->user()->row()->last_name;
		$this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		$this->uri_string = '/'.$this->uri->uri_string();
		$this->s_biro = $this->session->userdata('s_biro');
	}
	
	public function log_mesin()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		
		$data = array(
				'title' => 'Data Log Mesin',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'sssubunit' => $this->session->userdata('sssubunit_mesin_pegawai'),
				'id_mesin' => $this->session->userdata('id_mesin_mesin_pegawai'),
		);
		$this->load->view('log_mesin',$data);
	}
	
	public function get_log_mesin()
	{
		set_time_limit(0);
		//print_r($_REQUEST);
		$order = $_REQUEST['order'][0];
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/data/mesin_pegawai');
		//$aColumns = array( 'nip', 'nip','nama', 'nip','nip');
		$sssubunit = $this->input->post('sssubunit');
		$this->session->set_userdata('sssubunit_mesin_pegawai',$sssubunit);
		$serial_number = $this->input->post('serial_number');
		$this->session->set_userdata('serial_number',$serial_number);
		$sSearch = '';
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		if(isset($select) && $select != '')
		{
			if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND MID(lokasi,1,10) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND mid(lokasi,1,8) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND mid(lokasi,1,6) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND mid(lokasi,1,4) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			
		}
		
		if(isset($serial_number) && $serial_number != '')
		{
			$sSearch .= " and (a.serial_number = '".$serial_number."') ";
		}
			
		//limit
		if ( isset( $_REQUEST['start'] ) && $_REQUEST['length'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_REQUEST['start'] ).", ".
				intval( $_REQUEST['length'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "`".$aColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]."` ".
						($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		//$sOrder = " order by ".$aColumns[$order['column']]." ".$order['dir']." ";
		$s0 = "select count(a.id) as jml from log_mesin a
				where 1 = 1
				";
		$s0 .= $sSearch;
		
		$sheet_total = $this->db->query($s0)->row();
		$sheet_total = $sheet_total->jml;
	
		$s1 = "SELECT DISTINCT a.*
			FROM log_mesin a
			WHERE 1=1";
		$s1 .= $sSearch;
		$s1 .= " order by id desc ";
		
		$s1 .= $sLimit;
		//echo $s1;
		$sheet1 = $this->db->query($s1);
		$result = array();
		$i=0;
		$no=1;
		foreach($sheet1->result() as $sheet)
		{
			
			$result[$i]['0'] = $sheet->id;
			$result[$i]['1'] = $sheet->serial_number;
			$result[$i]['2'] = $sheet->keterangan;
			$result[$i]['3'] = date('d-M-Y H:i:s',strtotime($sheet->waktu));
			$result[$i]['4'] = $this->get_skpd($sheet->serial_number);
			$no++;
			$i++;
		}
		
		$data = $result;
		$results = array(
			"iTotalRecords" => ($sheet_total),
			"iTotalDisplayRecords" => ($sheet_total),
			"aaData"=>$data);
			
		echo json_encode($results);
	}
	
	public function log_mesin_user()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		
		$data = array(
				'title' => 'Data Log Mesin User',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'sssubunit' => $this->session->userdata('sssubunit_mesin_pegawai'),
				'id_mesin' => $this->session->userdata('id_mesin_mesin_pegawai'),
		);
		$this->load->view('log_mesin_user',$data);
	}
	
	public function get_log_mesin_user()
	{
		set_time_limit(0);
		//print_r($_REQUEST);
		$order = $_REQUEST['order'][0];
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/data/mesin_pegawai');
		//$aColumns = array( 'nip', 'nip','nama', 'nip','nip');
		$sssubunit = $this->input->post('sssubunit');
		$this->session->set_userdata('sssubunit_mesin_pegawai',$sssubunit);
		$serial_number = $this->input->post('serial_number');
		$this->session->set_userdata('serial_number',$serial_number);
		$sSearch = '';
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		if(isset($select) && $select != '')
		{
			if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND MID(lokasi,1,10) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND mid(lokasi,1,8) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND mid(lokasi,1,6) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND mid(lokasi,1,4) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			
		}
		
		if(isset($serial_number) && $serial_number != '')
		{
			$sSearch .= " and (a.serial_number = '".$serial_number."') ";
		}
			
		//limit
		if ( isset( $_REQUEST['start'] ) && $_REQUEST['length'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_REQUEST['start'] ).", ".
				intval( $_REQUEST['length'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "`".$aColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]."` ".
						($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		//$sOrder = " order by ".$aColumns[$order['column']]." ".$order['dir']." ";
		$s0 = "select count(a.id) as jml from log_mesin_user a
				where 1 = 1
				";
		$s0 .= $sSearch;
		
		$sheet_total = $this->db->query($s0)->row();
		$sheet_total = $sheet_total->jml;
	
		$s1 = "SELECT DISTINCT a.*
			FROM log_mesin_user a
			WHERE 1=1";
		$s1 .= $sSearch;
		$s1 .= " order by id desc ";
		
		$s1 .= $sLimit;
		//echo $s1;
		$sheet1 = $this->db->query($s1);
		$result = array();
		$i=0;
		$no=1;
		foreach($sheet1->result() as $sheet)
		{
			
			$result[$i]['0'] = $sheet->id;
			$result[$i]['1'] = $sheet->serial_number;
			$result[$i]['2'] = $sheet->nip;
			$result[$i]['3'] = $sheet->keterangan;
			$result[$i]['4'] = date('d-M-Y H:i:s',strtotime($sheet->waktu));
			$result[$i]['5'] = $this->get_skpd($sheet->serial_number);
			$no++;
			$i++;
		}
		
		$data = $result;
		$results = array(
			"iTotalRecords" => ($sheet_total),
			"iTotalDisplayRecords" => ($sheet_total),
			"aaData"=>$data);
			
		echo json_encode($results);
	}
	
	public function log_tap()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		
		$data = array(
				'title' => 'Data Log Tap',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'sssubunit' => $this->session->userdata('sssubunit_mesin_pegawai'),
				'id_mesin' => $this->session->userdata('id_mesin_mesin_pegawai'),
		);
		$this->load->view('log_tap',$data);
	}
	
	public function get_log_tap()
	{
		set_time_limit(0);
		//print_r($_REQUEST);
		$order = $_REQUEST['order'][0];
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/monitoring/log_tap');
		//$aColumns = array( 'nip', 'nip','nama', 'nip','nip');
		$sssubunit = $this->input->post('sssubunit');
		$this->session->set_userdata('sssubunit_mesin_pegawai',$sssubunit);
		$serial_number = $this->input->post('serial_number');
		$this->session->set_userdata('serial_number',$serial_number);
		$nip = $this->input->post('nip');
		$tanggal = $this->input->post('tanggal');
		$sSearch = '';
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		if(isset($select) && $select != '')
		{
			if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND MID(lokasi,1,10) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND mid(lokasi,1,8) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND mid(lokasi,1,6) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND mid(lokasi,1,4) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			
		}
		
		if(isset($serial_number) && $serial_number != '')
		{
			$sSearch .= " and (a.serial_number = '".$serial_number."') ";
		}
		if(isset($nip) && $nip != '')
		{
			$sSearch .= " and (a.nip = '".$nip."') ";
		}
		if(isset($tanggal) && $tanggal != '')
		{
			$sSearch .= " and (date(a.tanggal) = '".date('Y-m-d',strtotime($tanggal))."') ";
		}
			
		//limit
		if ( isset( $_REQUEST['start'] ) && $_REQUEST['length'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_REQUEST['start'] ).", ".
				intval( $_REQUEST['length'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "`".$aColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]."` ".
						($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		//$sOrder = " order by ".$aColumns[$order['column']]." ".$order['dir']." ";
		$s0 = "select count(a.id_log) as jml from absensi_log a
				where 1 = 1
				";
		$s0 .= $sSearch;
		
		$sheet_total = $this->db->query($s0)->row();
		$sheet_total = $sheet_total->jml;
	
		$s1 = "SELECT DISTINCT a.*
			FROM absensi_log a
			WHERE 1=1";
		$s1 .= $sSearch;
		$s1 .= " order by tanggal desc ";
		
		$s1 .= $sLimit;
		//echo $s1;
		$sheet1 = $this->db->query($s1);
		$result = array();
		$i=0;
		$no=1;
		foreach($sheet1->result() as $sheet)
		{
			
			$result[$i]['0'] = $sheet->id_log;
			$result[$i]['1'] = $sheet->serial_number;
			$result[$i]['2'] = $sheet->pin;
			$result[$i]['3'] = $sheet->nip;
			$result[$i]['4'] = $this->get_nama($sheet->nip);
			$result[$i]['5'] = date('d-M-Y H:i:s',strtotime($sheet->tanggal));
			$result[$i]['6'] = $this->get_skpd($sheet->serial_number);
			$no++;
			$i++;
		}
		
		$data = $result;
		$results = array(
			"iTotalRecords" => ($sheet_total),
			"iTotalDisplayRecords" => ($sheet_total),
			"aaData"=>$data);
			
		echo json_encode($results);
	}
	
	function get_skpd($serial_number)
	{
		$s = "select a.lokasi,
					b.nunker
				from mesin_absensi a
				left join referensi_unit_kerja b on(a.lokasi = b.kunker)
				where serial_number = '$serial_number'
		";
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			$res = $q->row()->nunker;
		}
		else
		{
			$res = '';
		}
		return $res;
	}
	
	function get_nama($nip)
	{
		$s = "select nama from d_pegawai where nip = '$nip' ";
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			$r = $q->row()->nama;
		}
		else
		{
			$s1 = "select nama from d_tkk where nik = '$nip' ";
			$q1 = $this->db->query($s1);
			if($q1->num_rows() > 0)
			{
				$r = $q1->row()->nama;
			}
			else
			{
				$r = '';
			}
		}
		return $r;
	}
	
	public function aktivitas_user()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		
		$data = array(
				'title' => 'Aktivitas User',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'sssubunit' => $this->session->userdata('sssubunit_aktivitas'),
				'nama' => $this->session->userdata('nama_aktivitas'),
				'reqnip' => $this->session->userdata('reqnip_aktivitas'),
		);
		$this->load->view('aktivitas_user',$data);
	}
	
	public function get_aktivitas_user()
	{
		//print_r($_REQUEST);
		$order = $_REQUEST['order'][0];
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/monitoring/aktivitas_user');
		//$aColumns = array( 'nip', 'nip','nama', 'nip','nip');
		$sssubunit = $this->input->post('sssubunit');
		$this->session->set_userdata('sssubunit_aktivitas',$sssubunit);
		$nama = $this->input->post('nama');
		$this->session->set_userdata('nama_aktivitas',$nama);
		$reqnip = $this->input->post('reqnip');
		$this->session->set_userdata('reqnip_aktivitas',$reqnip);
		
		$sSearch = '';
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		if(isset($select) && $select != '')
		{
			if(!empty($s_biro)) 
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
			}
			else
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			}
		}
		
		if(isset($nama) && $nama != '')
		{
			$sSearch .= " and (username like '%".$nama."%' or first_name like '%".$nama."%' or last_name like '%".$nama."%' ) ";
		}
		if(isset($reqnip) && $reqnip != '')
		{
			$sSearch .= " and (a.nip = '".$reqnip."') ";
		}
		
		//limit
		if ( isset( $_REQUEST['start'] ) && $_REQUEST['length'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_REQUEST['start'] ).", ".
				intval( $_REQUEST['length'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "`".$aColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]."` ".
						($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		//$sOrder = " order by ".$aColumns[$order['column']]." ".$order['dir']." ";
		$s0 = "select count(a.nip) as jml from t_users a
		LEFT JOIN d_pegawai b on(a.nip = b.nip)
				where 1 = 1
				";
		$s0 .= $sSearch;
		
		$sheet_total = $this->db->query($s0)->row();
		$sheet_total = $sheet_total->jml;
	
		$s1 = "SELECT 	a.id,
						a.username,
						a.email,
						a.nip,
						a.first_name,
						a.last_name,
						(select nunker from referensi_unit_kerja where kunker = a.skpd_kewenangan) as kewenangan,
						(select count(id_log) from t_log_aktivitas where day(waktu) = day(curdate()) and userid = a.id) as jumlah_aktivitas,
						(select name from t_groups where id = c.group_id) as grup,
						a.last_login
			FROM t_users a
			LEFT JOIN d_pegawai b on(a.nip = b.nip)
			LEFT JOIN t_users_groups c on(a.id = c.user_id)
			WHERE 1=1";
		$s1 .= $sSearch;
		$s1 .= " order by jumlah_aktivitas DESC ";
		
		$s1 .= $sLimit;
	//	echo $s1;
		$sheet1 = $this->db->query($s1);
		$result = array();
		$i=0;
		$no=1;
		foreach($sheet1->result() as $sheet)
		{
			
			$result[$i]['0'] = $no;
			$result[$i]['1'] = $sheet->username;
			$result[$i]['2'] = $sheet->first_name.' '.$sheet->last_name;
			$result[$i]['3'] = $sheet->nip;
			$result[$i]['4'] = $sheet->email;
			$result[$i]['5'] = $sheet->grup;	
			$result[$i]['6'] = $sheet->kewenangan;
			$result[$i]['7'] = date('d-m-Y',$sheet->last_login);
			$result[$i]['8'] = 	$sheet->jumlah_aktivitas;	
			$result[$i]['9'] = '<a target="_blank" href="/monitoring/rincian_aktivitas/'.$sheet->id.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="View">
									<i class="glyphicon glyphicon-search"></i>
								</a>';
			$no++;
			$i++;
		}
		
		$data = $result;
		$results = array(
			"iTotalRecords" => ($sheet_total),
			"iTotalDisplayRecords" => ($sheet_total),
			"aaData"=>$data);
			
		echo json_encode($results);
	}
	
	public function rincian_aktivitas()
	{
		$userid = $this->uri->segment(3);
		$s = "select * from t_users where id = '$userid'";
		$row = $this->db->query($s)->row();
		$data = array(
				'title' => 'Rincian Aktivitas User',
				'subtitle' => '',
				'userid' => $userid,
				'row' => $row,
		);
		$this->load->view('rincian_aktivitas_user',$data);
	}
	
	public function get_rincian_aktivitas_user()
	{
		//print_r($_REQUEST);
		$order = $_REQUEST['order'][0];
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/monitoring/aktivitas_user');
		//$aColumns = array( 'nip', 'nip','nama', 'nip','nip');
		$userid = $this->input->post('userid');
		$tanggal = $this->input->post('tanggal');
		$tanggal = date('Y-m-d',strtotime($tanggal));
		$sSearch = '';
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		if(isset($select) && $select != '')
		{
			if(!empty($s_biro)) 
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
			}
			else
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			}
		}
		
		if(isset($tanggal) && $tanggal != '')
		{
			$sSearch .= " and (date(waktu) = '$tanggal' ) ";
		}
		if(isset($userid) && $userid != '')
		{
			$sSearch .= " and (a.userid = '$userid') ";
		}
		//limit
		if ( isset( $_REQUEST['start'] ) && $_REQUEST['length'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_REQUEST['start'] ).", ".
				intval( $_REQUEST['length'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "`".$aColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]."` ".
						($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		//$sOrder = " order by ".$aColumns[$order['column']]." ".$order['dir']." ";
		$s0 = "select count(a.id_log) as jml from t_log_aktivitas a
				where 1 = 1
				";
		$s0 .= $sSearch;
		
		$sheet_total = $this->db->query($s0)->row();
		$sheet_total = $sheet_total->jml;
	
		$s1 = "SELECT 	a.*
			FROM t_log_aktivitas a
			WHERE 1=1";
		$s1 .= $sSearch;
		$s1 .= " order by waktu desc ";
		
		$s1 .= $sLimit;
	//	echo $s1;
		$sheet1 = $this->db->query($s1);
		$result = array();
		$i=0;
		$no=1;
		foreach($sheet1->result() as $sheet)
		{
			
			$result[$i]['0'] = $no;
			$result[$i]['1'] = $sheet->aktivitas;
			$result[$i]['2'] = date('d-M-Y H:i:s',strtotime($sheet->waktu));
			$result[$i]['3'] = $sheet->ip_user;
			$no++;
			$i++;
		}
		
		$data = $result;
		$results = array(
			"iTotalRecords" => ($sheet_total),
			"iTotalDisplayRecords" => ($sheet_total),
			"aaData"=>$data);
			
		echo json_encode($results);
	}
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */