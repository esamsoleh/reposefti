<?php $this->load->view('header');?>
<style type="text/css">
    #map {
      width: 100%;
      height: 240px;
    }
</style>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('top');?>
  <!-- Left side column. contains the logo and sidebar -->
  
  <?php $this->load->view('sidebar');?>
 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title;?>
        <small><?=$subtitle;?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-success">
					<div class="panel-heading">
                        Data Project
                    </div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group row">
									<div class="col-md-4"><label>Nama Project</label></div>
									<div class="col-md-8">
											<?php echo $r->nama;?>
									</div>
								</div>
								<div class="form-group row">
									<div class="col-md-4"><label>Latitude</label></div>
										<div class="col-md-8">
											<?php echo $r->lat;?>
										</div>
								</div>
								<div class="form-group row">
									<div class="col-md-4"><label>Longitude</label></div>
										<div class="col-md-8">
											<?php echo $r->lng;?>
										</div>
								</div>
							</div>
							<div class="col-md-6">
								<div id="map"></div>
							</div>
									
						</div>										
					</div>
				</div>
			</div>
			<div class="col-xs-1">
			</div>
		</div>
		<div class="row">
		
			<div class="col-xs-12">
				<div class="panel panel-success">
                    <div class="panel-heading">
                        Data Site Project
                    </div>
					<div class="panel-body">
						<div class="form-group row">
																	
						</div>
						<table class="table table-borderd table-responsive">
							<tr>
								<td>No</td>
								<td>Nama</td>
							<!--	<td>Edit</td>
								<td>Hapus</td> -->
							</tr>
							<?php
							$no=1;
							foreach($rs->result() as $site)
							{
								echo '<tr>';
								echo '<td>'.$no.'</td>';
								echo '<td>'.$site->nama.'</td>';
								/* echo '<td>
										<a href="/data/edit_site/'.$id_project.'/'.$site->id.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Edit">
											<i class="glyphicon glyphicon-edit"></i>
										</a>
										</td>';
								echo '<td>
										<a href="/data/hapus_site/'.$id_project.'/'.$site->id.'" class="btn btn-danger del" data-toggle="tooltip" data-placement="top" title="Hapus">
										<i class="glyphicon glyphicon-trash"></i>
										</a>
										</td>'; */
								echo '</tr>';
								$no++;
							}
							?>
						</table>
					</div>
				</div>
			</div>
		
		</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
 
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('javascript');?>
<script type="text/javascript">
	
	var mymap = L.map('map').setView([<?php echo ((isset($r->lat) && $r->lat != '')?$r->lat:-6.175469329632223);?>, <?php echo ((isset($r->lng) && $r->lng != '')?$r->lng:106.82729225921626);?>], 20);
	L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
		attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		maxZoom: 30,
		id: 'mapbox.streets',
		accessToken: 'pk.eyJ1Ijoic2Ftc29sZWgiLCJhIjoiY2p2a2V1cTU2MG5jdzN5bWw2MDBrODByaSJ9.eLmd2Zo6T_9pt3QJoFDXdw'
	}).addTo(mymap);	
	document.getElementById('map').style.display = 'block';
	mymap.invalidateSize();
	
	var marker = L.marker([<?php echo ((isset($r->lat) && $r->lat != '')?$r->lat:-6.175469329632223);?>, <?php echo ((isset($r->lng) && $r->lng != '')?$r->lng:106.82729225921626);?>]).addTo(mymap);
</script>
</body>
</html>
