<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Data extends CI_Controller {
	public $apps;
	var $nama_user;
	var $file_foto;
	var $uri_string;
	var $s_biro;
	var $userid;
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
		if (!$this->ion_auth->logged_in())
			{
				redirect('/auth/login', 'refresh');
			}
		$this->apps = 'Data';
		$this->load->model('data_model');
		$this->nama_user = $this->ion_auth->user()->row()->first_name.' '.$this->ion_auth->user()->row()->last_name;
		$this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		$this->uri_string = '/'.$this->uri->uri_string();
		$this->s_biro = $this->session->userdata('s_biro');
		$this->userid = $this->ion_auth->user()->row()->id;
	}
	
	public function project()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		
		$data = array(
				'title' => 'Data Project',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'nama' => $this->session->userdata('nama_project'),
		);
		$this->load->view('project',$data);
		input_log($this->userid,'Lihat Data Project');
	}
	
	public function get_project()
	{
		//print_r($_REQUEST);
		//$order = $_REQUEST['order'][0];
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/data/project');
		//$aColumns = array( 'nip', 'nip','nama', 'nip','nip');
		$nama = $this->input->post('nama');
		$this->session->set_userdata('nama_project',$nama);
		
		$sSearch = '';
		
		if(isset($nama) && $nama != '')
		{
			$sSearch .= " and (nama like '%".$nama."%') ";
		}
		
		//limit
		if ( isset( $_REQUEST['start'] ) && $_REQUEST['length'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_REQUEST['start'] ).", ".
				intval( $_REQUEST['length'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "`".$aColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]."` ".
						($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		//$sOrder = " order by ".$aColumns[$order['column']]." ".$order['dir']." ";
		$s0 = "SELECT count(id) as jml
				FROM d_project a
				WHERE 1=1
				";
		$s0 .= $sSearch;
		
		$sheet_total = $this->db->query($s0)->row();
		$sheet_total = $sheet_total->jml;
	
		$s1 = "SELECT a.*,
				CASE a.status
									   WHEN 0 then  'Aktif'
									   WHEN 1 then  'Tidak Aktif'
				END as status_project
			FROM d_project a
			WHERE 1=1
			";
		$s1 .= $sSearch;
		$s1 .= " order by id asc ";
		
		$s1 .= $sLimit;
	//	echo $s1;
		$sheet1 = $this->db->query($s1);
		$result = array();
		$i=0;
		$no=1;
		foreach($sheet1->result() as $sheet)
		{
			$result[$i]['0'] = $no;
			$result[$i]['1'] = $sheet->nama;
			$result[$i]['2'] = '<a href="/data/edit_status_project/'.$sheet->id.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Edit">
									<i class="glyphicon glyphicon-edit">'.$sheet->status_project.'</i>
								</a>';
			if($hak_akses['vi'] == 1)
			{
				$result[$i]['3'] = '<a href="/data/view_project/'.$sheet->id.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="View">
									<i class="glyphicon glyphicon-search"></i>
								</a>';
			}
			else
			{
				$result[$i]['3'] = '';
			}			
			
			/* if($hak_akses['up'] == 1)
			{
				$result[$i]['4'] = '<a href="/data/edit_project/'.$sheet->id.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Edit">
									<i class="glyphicon glyphicon-edit"></i>
								</a>';
			}
			else
			{
				$result[$i]['4'] = '';
			}	
			if($hak_akses['de'] == 1)
			{
				$result[$i]['5'] = '<a id="del_'.$no.'" href="/data/hapus_project/'.$sheet->id.'" class="btn btn-danger del" data-toggle="tooltip" data-placement="top" title="Hapus">
								<i class="glyphicon glyphicon-trash"></i>
								</a>
								<script>
								$(document).ready(function () {
									$("#del_'.$no.'").click(function(){
										if (!confirm("Do you want to delete")){
										  return false;
										}
									});
								});
								</script>
								';
			}
			else
			{
				$result[$i]['5'] = '';
			} */
					
			$no++;
			$i++;
		}
		
		$data = $result;
		$results = array(
			"iTotalRecords" => ($sheet_total),
			"iTotalDisplayRecords" => ($sheet_total),
			"aaData"=>$data);
			
		echo json_encode($results);
	}
	
	public function tambah_project()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
			
			if ($this->form_validation->run() === TRUE)
			{
				$posts = $this->input->post();
				//print_r($posts);
				//	echo '</br>';
				$nama = $this->db->escape_str($posts['nama']);
				$lokasi_x = $this->db->escape_str($posts['lokasi_x']);
				$lokasi_y = $this->db->escape_str($posts['lokasi_y']);
				
				$added_date = date('Y-m-d H:i:s');
				$added_by = $this->userid;
				
				$sql = 	"insert into d_project 
							SET nama = '$nama', 
								lokasi_x = '$lokasi_x',
								lokasi_y = '$lokasi_y',
								added_date = '$added_date',
								added_by = '$added_by'
								";
			///	echo $sql.'</br>';
				$this->db->query($sql);	
				$id = $this->db->insert_id();
				
				input_log($this->userid,'Tambah Data project dengan id:'.$id);				
				redirect('/data/project');
			}
			else
			{
				$this->data['nama'] = array(
						'name'  => 'nama',
						'id'    => 'nama',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('nama'),
						'class'  => 'form-control',
					);
				$this->data['lokasi_x'] = array(
						'name'  => 'lokasi_x',
						'id'    => 'lokasi_x',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('lokasi_x'),
						'class'  => 'form-control',
					);
				$this->data['lokasi_y'] = array(
						'name'  => 'lokasi_y',
						'id'    => 'lokasi_y',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('lokasi_y'),
						'class'  => 'form-control',
					);
					
				$this->data['title'] = 'Data Project';
				$this->data['subtitle'] = 'Form Penambahan Data Project';
				$this->data['error'] = '';
				$this->load->view('form_project',$this->data);
			}
			
	}
	
	public function edit_project()
	{
		$id = $this->uri->segment(3);
		$s = "SELECT DISTINCT a.*
			FROM d_project a
			WHERE 1=1
			and id = '$id' 
				";
		//echo $s;
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			$this->load->library('form_validation');
			$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
			
			if ($this->form_validation->run() === TRUE)
			{
				$posts = $this->input->post();
			//	print_r($posts);
			//	echo '</br>';
				$nama = $this->db->escape_str($posts['nama']);
				$lat = $this->db->escape_str($posts['lat']);
				$lng = $this->db->escape_str($posts['lng']);
				$update_date = date('Y-m-d H:i:s');
				$update_by = $this->userid;
				$sql = 	"update d_project 
							SET nama = '$nama', 
								lat = '$lat',
								lng = '$lng',
								update_date = '$update_date',
								update_by = '$update_by'
							where id = '$id'
								";
				//echo $sql.'</br>';
				$this->db->query($sql);
				input_log($this->userid,'Edit Data project dengan id:'.$id);
				redirect('/data/project');
			}
			else
			{
				
				$this->data['nama'] = array(
						'name'  => 'nama',
						'id'    => 'nama',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('nama',$r->nama),
						'class'  => 'form-control',
					);
				$this->data['lat'] = array(
						'name'  => 'lat',
						'id'    => 'lat',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('lat',$r->lat),
						'class'  => 'form-control',
					);
				$this->data['lng'] = array(
						'name'  => 'lng',
						'id'    => 'lng',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('lng',$r->lng),
						'class'  => 'form-control',
					);
					
				$this->data['title'] = 'Data Project';
				$this->data['subtitle'] = '';
				$this->data['page_title'] = 'Form Pemutakhiran Data Project';
				$this->data['error'] = '';
				$this->data['r'] = $r;
				//print_r($this->data);
				$this->load->view('form_project',$this->data);
			}
		
		}
		else
		{
			redirect('/');
		}
		
	}
	
	public function edit_status_project()
	{
		$id = $this->uri->segment(3);
		$s = "SELECT a.*
			FROM d_project a
			WHERE 1=1
			and id = '$id' 
				";
		//echo $s;
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			$this->load->library('form_validation');
			$this->form_validation->set_rules('status', 'Status', 'trim|required');
			
			if ($this->form_validation->run() === TRUE)
			{
				$posts = $this->input->post();
			//	print_r($posts);
			//	echo '</br>';
				$status = $this->db->escape_str($posts['status']);
				$updated_at = date('Y-m-d H:i:s');
				$update_by = $this->userid;
				$sql = 	"update d_project 
							SET status = '$status', 
								updated_at = '$updated_at'
							where id = '$id'
								";
				//echo $sql.'</br>';
				$this->db->query($sql);
				input_log($this->userid,'Edit Data Status project dengan id:'.$id);
				redirect('/data/project');
			}
			else
			{
				
				$this->data['status'] = array(
						'name'  => 'status',
						'id'    => 'status',
						'class'  => 'form-control',
				);
				
				$this->data['options_status'] = array(
					'0' => 'Aktif',
					'1' => 'Tidak Aktif'					
				);
				$this->data['selected_status'] = $this->form_validation->set_value('status',$r->status);
					
				$this->data['title'] = 'Data Status Project';
				$this->data['subtitle'] = '';
				$this->data['page_title'] = 'Form Pemutakhiran Data Status Project';
				$this->data['error'] = '';
				$this->data['r'] = $r;
				//print_r($this->data);
				$this->load->view('form_edit_status_project',$this->data);
			}
		
		}
		else
		{
			redirect('/');
		}
		
	}
	
	public function view_project()
	{
		$id = $this->uri->segment(3);
		$s = "select * from d_project where id = '$id' ";
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			$ss = "select * from d_site where id_project = '$id' ";
			$rs = $this->db->query($ss);
			$data = array(
				'title' => 'Data Project',
				'subtitle' => '',
				'r' => $q->row(),
				'rs' => $rs,
				'id_project' => $id
			);
			$this->load->view('view_project',$data);
			input_log($this->userid,'Lihat Data rincian project dengan id: '.$id);
		}
	}
	
	public function tambah_site()
	{
		$id_project = $this->uri->segment(3);
		$s = "select * from d_project where id = '$id_project' ";
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			$this->load->library('form_validation');
			$this->form_validation->set_rules('tingkat', 'Tingkat', 'trim|required');
			
			if ($this->form_validation->run() === TRUE)
			{
				$posts = $this->input->post();
				//print_r($posts);
				//	echo '</br>';
				$tingkat = $this->db->escape_str($posts['tingkat']);
				$tinggi = $this->db->escape_str($posts['tinggi']);				
				$added_date = date('Y-m-d H:i:s');
				$added_by = $this->userid;
				
				$sql = 	"insert into d_site 
							SET id_project = '$id_project', 
								tingkat = '$tingkat',
								tinggi = '$tinggi',
								added_date = '$added_date',
								added_by = '$added_by'
								";
			///	echo $sql.'</br>';
				$this->db->query($sql);	
				$id = $this->db->insert_id();
				
				input_log($this->userid,'Tambah Data project dengan id:'.$id);				
				redirect('/data/view_project/'.$id_project);
			}
			else
			{
				$this->data['tingkat'] = array(
						'name'  => 'tingkat',
						'id'    => 'tingkat',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('tingkat'),
						'class'  => 'form-control',
					);
				$this->data['tinggi'] = array(
						'name'  => 'tinggi',
						'id'    => 'tinggi',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('tinggi'),
						'class'  => 'form-control',
					);
					
				$this->data['title'] = 'Data Site Project';
				$this->data['subtitle'] = 'Form Penambahan Data Site Project';
				$this->data['error'] = '';
				$this->load->view('form_site',$this->data);
			}
			
		}
		else
		{
			redirect('/');
		}
		
	}
	
	public function edit_site()
	{
		$id_project = $this->uri->segment(3);
		$id = $this->uri->segment(4);
		$s = "select * from d_site where id = '$id' and id_project = '$id_project' ";
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			$this->load->library('form_validation');
			$this->form_validation->set_rules('tingkat', 'Tingkat', 'trim|required');
			
			if ($this->form_validation->run() === TRUE)
			{
				$posts = $this->input->post();
				//print_r($posts);
				//	echo '</br>';
				$tingkat = $this->db->escape_str($posts['tingkat']);
				$tinggi = $this->db->escape_str($posts['tinggi']);				
				$update_date = date('Y-m-d H:i:s');
				$update_by = $this->userid;
				
				$sql = 	"update d_site 
							SET tingkat = '$tingkat',
								tinggi = '$tinggi',
								update_date = '$update_date',
								update_by = '$update_by'
						where id_project = '$id_project'
						and id = '$id'
								";
			///	echo $sql.'</br>';
				$this->db->query($sql);	
				$id = $this->db->insert_id();
				
				input_log($this->userid,'update Data site project dengan id:'.$id);				
				redirect('/data/view_project/'.$id_project);
			}
			else
			{
				$this->data['tingkat'] = array(
						'name'  => 'tingkat',
						'id'    => 'tingkat',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('tingkat',$r->tingkat),
						'class'  => 'form-control',
					);
				$this->data['tinggi'] = array(
						'name'  => 'tinggi',
						'id'    => 'tinggi',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('tinggi',$r->tinggi),
						'class'  => 'form-control',
					);
					
				$this->data['title'] = 'Data Site Project';
				$this->data['subtitle'] = 'Form Penambahan Data Site Project';
				$this->data['error'] = '';
				$this->load->view('form_site',$this->data);
			}
			
		}
		else
		{
			redirect('/');
		}
		
	}
	
	public function hapus_site()
	{
		$id_project = $this->uri->segment(3);
		$id = $this->uri->segment(4);
		$sc = "select id from d_site where id_project = '$id_project' and id = '$id' ";
		$qc = $this->db->query($sc);
		if($qc->num_rows() > 0)
		{
			$sd = "delete from d_site where id_project = '$id_project' and id = '$id' ";
			$this->db->query($sd);
			redirect('/data/view_project/'.$id_project);
		}
		else
		{
			redirect('/');
		}
		
	}
	
	public function pegawai()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		
		$data = array(
				'title' => 'Data Pegawai',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'id_project' => $this->session->userdata('id_project_pegawai'),
				'nama' => $this->session->userdata('nama_pegawai'),
				'reqnip' => $this->session->userdata('reqnip_pegawai'),
		);
		$this->load->view('pegawai',$data);
		input_log($this->userid,'Lihat Data Pegawai Proyek');
	}
	
	public function get_pegawai()
	{
		//print_r($_REQUEST);
	//	$order = $_REQUEST['order'][0];
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/data/pegawai');
		//$aColumns = array( 'nip', 'nip','nama', 'nip','nip');
		$nama = $this->input->post('nama');
		$this->session->set_userdata('nama_pegawai',$nama);
		$reqnip = $this->input->post('reqnip');
		$this->session->set_userdata('reqnip_pegawai',$reqnip);
		$sSearch = '';
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		if(isset($select) && $select != '')
		{
			if(!empty($s_biro)) 
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
			}
			else
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			}
		}
		
		if(isset($nama) && $nama != '')
		{
			$sSearch .= " and (nama like '%".$nama."%') ";
		}
		if(isset($reqnip) && $reqnip != '')
		{
			$sSearch .= " and (a.nip like '%".$reqnip."%') ";
		}
		
		//limit
		if ( isset( $_REQUEST['start'] ) && $_REQUEST['length'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_REQUEST['start'] ).", ".
				intval( $_REQUEST['length'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "`".$aColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]."` ".
						($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		//$sOrder = " order by ".$aColumns[$order['column']]." ".$order['dir']." ";
		$s0 = "SELECT count(nip) as jml
				FROM d_pegawai a
				WHERE 1=1
				";
		$s0 .= $sSearch;
		
		$sheet_total = $this->db->query($s0)->row();
		$sheet_total = $sheet_total->jml;
	
		$s1 = "SELECT DISTINCT a.*,
				(select nama from ref_jabatan where id = a.id_jabatan) as jabatan
			FROM d_pegawai a
			WHERE 1=1
			";
		$s1 .= $sSearch;
		$s1 .= " order by nip asc ";
		
		$s1 .= $sLimit;
	//	echo $s1;
		$sheet1 = $this->db->query($s1);
		$result = array();
		$i=0;
		$no=1;
		foreach($sheet1->result() as $sheet)
		{
			$result[$i]['0'] = $no;
			$result[$i]['1'] = $sheet->nip;
			$result[$i]['2'] = $sheet->nama;
			$result[$i]['3'] = 	$sheet->jabatan;
			if($hak_akses['up'] == 1)
			{
				$result[$i]['4'] = '<a href="/data/edit_kualifikasi/'.$sheet->nip.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Edit">
									<i class="glyphicon glyphicon-edit"></i>
								</a>';
			}
			else
			{
				$result[$i]['4'] = '';
			}
			
			$result[$i]['5'] = 	'<a target="_blank" href="/data/view_foto/'.$sheet->nip.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="View Foto">
									<i class="glyphicon glyphicon-search"></i>
								</a>';
			if($hak_akses['up'] == 1)
			{
				$result[$i]['6'] = '<a href="/data/edit_pegawai/'.$sheet->nip.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Edit">
									<i class="glyphicon glyphicon-edit"></i>
								</a>';
			}
			else
			{
				$result[$i]['6'] = '';
			}
			if($hak_akses['de'] == 1)
			{
				$result[$i]['7'] = '<a id="del_'.$no.'" href="/data/hapus_pegawai/'.$sheet->nip.'" class="btn btn-danger del" data-toggle="tooltip" data-placement="top" title="Hapus">
								<i class="glyphicon glyphicon-trash"></i>
								</a>
								<script>
								$(document).ready(function () {
									$("#del_'.$no.'").click(function(){
										if (!confirm("Do you want to delete")){
										  return false;
										}
									});
								});
								</script>
								';
			}
			else
			{
				$result[$i]['7'] = '';
			}
			$no++;
			$i++;
		}
		
		$data = $result;
		$results = array(
			"iTotalRecords" => ($sheet_total),
			"iTotalDisplayRecords" => ($sheet_total),
			"aaData"=>$data);
			
		echo json_encode($results);
	}
	
	public function view_foto()
	{
		$nip = $this->uri->segment(3);
		$s = "select foto from d_pegawai where nip = '$nip' ";
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			foreach($q->result() as $row)
			{	
				if(isset($row) && $row->foto != '')
				{
					echo '&nbsp; <img src="/assets/foto/'.$row->foto.'"></br>';
					echo '<hr>';
				}
				else
				{
					echo '<img src="/assets/foto/no-image.png">';
				}
				
			}
			
		}
		else
		{
			echo '<img src="/assets/foto/no-image.png">';
		}
	}
	
	public function tambah_pegawai()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nip', 'NIP', 'trim|required');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
			
			if ($this->form_validation->run() === TRUE)
			{
				$posts = $this->input->post();
				//print_r($posts);
				//	echo '</br>';
				$nip = $this->db->escape_str($posts['nip']);
				$nama = $this->db->escape_str($posts['nama']);
				$id_jabatan = $this->db->escape_str($posts['id_jabatan']);
				$update_date = date('Y-m-d H:i:s');
				$update_by = $this->userid;
				//upload foto
				//$config['upload_path'] = 'assets/foto/';
				$path = 'assets/foto/';
				$config['upload_path'] = $path;
				$config['allowed_types'] = 'gif|jpg|jpeg|png';
				$this->load->library('upload', $config);
				$field_name = 'userfile';
				if ($this->upload->do_upload($field_name))
				{
					$data = array('upload_data' => $this->upload->data());
					$file_1 = $path.$nip.$data['upload_data']['file_ext'];
					if (file_exists($file_1)) {unlink($file_1);}       
					$org_1 = $path.$data['upload_data']['file_name'];
					rename($org_1, $file_1);	
					$file_gambar = $nip.$data['upload_data']['file_ext'];
				}
				else
				{
					$this->session->set_flashdata('message', $this->upload->display_errors());	
					$file_gambar = '';
				}	
				
				$sql = 	"insert into d_pegawai 
							SET nip = '$nip', 
								nama = '$nama',
								id_jabatan = '$id_jabatan',
								foto = '$file_gambar',
								added_date = '$added_date',
								added_by = '$added_by'
								";
			///	echo $sql.'</br>';
				$this->db->query($sql);	
				
				$this->update_jadwal($nip);
				input_log($this->userid,'Tambah Data pegawai dengan nip:'.$nip);				
				redirect('/data/pegawai');
			}
			else
			{
				$this->data['nip'] = array(
						'name'  => 'nip',
						'id'    => 'nip',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('nip'),
						'class'  => 'form-control',
					);
				$this->data['nama'] = array(
						'name'  => 'nama',
						'id'    => 'nama',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('nama'),
						'class'  => 'form-control',
					);
				$this->data['id_jabatan'] = array(
					'name'  => 'id_jabatan',
					'id'    => 'id_jabatan',
					'class'  => 'form-control',
					);
				$s = "select * from ref_jabatan where 1 = 1";
				$q = $this->db->query($s);
				//$options_jabatan[''] = '----------';
				foreach($q->result_array() as $rq)
				{
					$options_jabatan[($rq['id'])] = $rq['nama'];
				}
					
				$this->data['options_jabatan'] = $options_jabatan;
				$this->data['selected_jabatan'] = $this->form_validation->set_value('id_jabatan');
					
				$this->data['title'] = 'Data Pegawai';
				$this->data['subtitle'] = 'Form Penambahan Data Pegawai';
				$this->data['error'] = '';
				$this->load->view('form_pegawai',$this->data);
			}
			
	}
	
	public function edit_pegawai()
	{
		$nip = $this->uri->segment(3);
		$s = "SELECT DISTINCT a.*
			FROM d_pegawai a
			WHERE 1=1
			and nip = '$nip' 
				";
		//echo $s;
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			$this->load->library('form_validation');
			$this->form_validation->set_rules('nip', 'NIP', 'trim|required');
			$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
			
			if ($this->form_validation->run() === TRUE)
			{
				$posts = $this->input->post();
			//	print_r($posts);
			//	echo '</br>';
				
				$nip = $this->db->escape_str($posts['nip']);
				$nama = $this->db->escape_str($posts['nama']);
				$id_jabatan = $this->db->escape_str($posts['id_jabatan']);
				$update_date = date('Y-m-d H:i:s');
				$update_by = $this->userid;
				//upload foto
				//$config['upload_path'] = 'assets/foto/';
				$path = 'assets/foto/';
				$config['upload_path'] = $path;
				$config['allowed_types'] = 'gif|jpg|jpeg|png';
				$this->load->library('upload', $config);
				$field_name = 'userfile';
				if ($this->upload->do_upload($field_name))
				{
					$data = array('upload_data' => $this->upload->data());
					$file_1 = $path.$nip.$data['upload_data']['file_ext'];
					if (file_exists($file_1)) {unlink($file_1);}       
					$org_1 = $path.$data['upload_data']['file_name'];
					rename($org_1, $file_1);	
					$file_gambar = $nip.$data['upload_data']['file_ext'];
				}
				else
				{
					$this->session->set_flashdata('message', $this->upload->display_errors());	
					$file_gambar = $r->foto;
				}	
				$sql = 	"update d_pegawai 
							SET nip = '$nip', 
								nama = '$nama',
								id_jabatan = '$id_jabatan',
								foto = '$file_gambar',
								update_date = '$update_date',
								update_by = '$update_by'
							where nip = '$nip'
								";
				//echo $sql.'</br>';
				$this->db->query($sql);
				
				$this->update_jadwal($nip);
				input_log($this->userid,'Edit Data pegawai dengan nip:'.$nip);
				redirect('/data/pegawai');
			}
			else
			{
				$this->data['nip'] = array(
						'name'  => 'nip',
						'id'    => 'nip',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('nip',$r->nip),
						'class'  => 'form-control',
					);
				$this->data['nama'] = array(
						'name'  => 'nama',
						'id'    => 'nama',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('nama',$r->nama),
						'class'  => 'form-control',
					);
				$this->data['id_jabatan'] = array(
					'name'  => 'id_jabatan',
					'id'    => 'id_jabatan',
					'class'  => 'form-control',
					);
				$s = "select * from ref_jabatan where 1 = 1";
				$q = $this->db->query($s);
				//$options_jabatan[''] = '----------';
				foreach($q->result_array() as $rq)
				{
					$options_jabatan[($rq['id'])] = $rq['nama'];
				}
					
				$this->data['options_jabatan'] = $options_jabatan;
				$this->data['selected_jabatan'] = $this->form_validation->set_value('id_jabatan',$r->id_jabatan);
				
				
				$this->data['title'] = 'Data Pegawai';
				$this->data['subtitle'] = '';
				$this->data['page_title'] = 'Form Pemutakhiran Data Pegawai';
				$this->data['error'] = '';
				$this->data['r'] = $r;
				//print_r($this->data);
				$this->load->view('form_pegawai',$this->data);
			}
		
		}
		else
		{
			redirect('/');
		}
		
	}
	
	public function edit_kualifikasi()
	{
		$nip = $this->uri->segment(3);
		$update = $this->input->post('update');
		if(isset($update) && $update == 1)
		{
			$sd = "delete from d_kualifikasi_pegawai where nip = '$nip' ";
			$qc = $this->db->query($sd);
			$posts = $this->input->post();
			$ids = $this->input->post('id');
			if(isset($ids) && !empty($ids))
			{
				foreach($ids as $id)
				{
					$si = "insert into d_kualifikasi_pegawai 
							set nip = '$nip',
							kualifikasi = '$id' 
							";
					$this->db->query($si);
				}
			}
			
		}
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/data/pegawai');
		$s = "SELECT a.*
			FROM d_pegawai a
			WHERE 1=1
			and nip = '$nip' 
				";
		//echo $s;
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			$sck = "select * from ref_kualifikasi ";
			$qck = $this->db->query($sck);
			$skual = "select * from d_kualifikasi_pegawai where nip = '$nip' ";
			$qkual = $this->db->query($skual);
			$data = array(
				'title' => 'Data Kualifikasi Pegawai',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'qck' => $qck,
				'qkual' => $qkual,
			);
			$this->load->view('form_kualifikasi',$data);
		}
		else
		{
			redirect('/');
		}
		
	}
	
	function update_jadwal($nip)
	{
		$sc = "select id from d_pegawai_tipe_jadwal where nip = '$nip' ";
		$qc = $this->db->query($sc);
		$date = date('Y-m-d H:i:s');
		$by = $this->userid;
		if($qc->num_rows() == 0)
		{
			$si = "insert into d_pegawai_tipe_jadwal
					SET nip = '$nip', 
						tipe_jadwal = '1',
						added_date = '$date',
						added_by = '$by'
						";
			$this->db->query($si);
		}
	}
	
	public function hapus_pegawai()
	{
		$nip = $this->uri->segment(3);
		$sc = "select * from d_pegawai where nip = '$nip' ";
		$q = $this->db->query($sc);
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			unlink('assets/foto/'.$r->foto);		
			$s = "delete from d_pegawai where nip = '$nip' ";
			$this->db->query($s);
			redirect('/data/pegawai');
		}
		else
		{
			redirect('/');
		}
		
	}
	
	public function mesin_absensi()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		
		$data = array(
				'title' => 'Data Mesin Absensi',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
		);
		$this->load->view('mesin_absensi',$data);
		input_log($this->userid,'Lihat Data Mesin Absensi');
	}
	
	public function get_mesin_absensi()
	{
		//print_r($_REQUEST);
	//	$order = $_REQUEST['order'][0];
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/log/harian');
		//$aColumns = array( 'id', 'serial_number','ip_mesin', 'lokasi','updated','id','id');
		$serial_number = $this->input->post('serial_number');
		$this->session->set_userdata('serial_number',$serial_number);
		$sSearch = '';
		
		if(isset($serial_number) && $serial_number != '')
		{
			$sSearch .= " and (a.serial_number like '%".$serial_number."%') ";
		}
				
		//limit
		if ( isset( $_REQUEST['start'] ) && $_REQUEST['length'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_REQUEST['start'] ).", ".
				intval( $_REQUEST['length'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "`".$aColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]."` ".
						($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		//$sOrder = " order by ".$aColumns[$order['column']]." ".$order['dir']." ";
		$s0 = "select count(a.id) as jml from d_mesin_absensi a
				where 1 = 1
				";
		$s0 .= $sSearch;
		
		$sheet_total = $this->db->query($s0)->row();
		$sheet_total = $sheet_total->jml;
	
		$s1 = "SELECT DISTINCT a.id,
									a.serial_number,
									a.ip,
									a.id_project,
									a.status,
									CASE a.status
									   WHEN 0 then  'Tidak Aktif'
									   WHEN 1 then  'Aktif'
									END as status_mesin,
									b.nama as nama_project,
									a.updated
			FROM d_mesin_absensi a
			left join d_project b on(a.id_project = b.id)
			WHERE 1=1";
		$s1 .= $sSearch;
		$s1 .= " order by a.id asc ";
		
		$s1 .= $sLimit;
		//echo $s1;
		$sheet1 = $this->db->query($s1);
		$result = array();
		$i=0;
		$no=1;
		foreach($sheet1->result() as $sheet)
		{
			
			$result[$i]['0'] = $no;
			$result[$i]['1'] = $sheet->serial_number;
			$result[$i]['2'] = $sheet->ip;
			$result[$i]['3'] = $sheet->nama_project;
			$result[$i]['4'] = ((isset($sheet->updated) && $sheet->updated != '0000-00-00 00:00:00' && $sheet->updated != '' && $sheet->updated != NULL)?date('d-M-Y H:i:s', strtotime($sheet->updated)):'-');
			$result[$i]['5'] = '<a target="_blank" href="/data/view_enroll/'.$sheet->id.'">
								'.$this->get_enroll($sheet->id).'
								</a>';	;
			if($hak_akses['up'] == 1)
			{
				$result[$i]['6'] = 	'<a href="/data/edit_mesin_absensi/'.$sheet->id.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="EDIT">
											<i class="glyphicon glyphicon-edit"></i>
								</a>';	
			}
			else
			{
				$result[$i]['6'] = '';
			}
			if($hak_akses['de'] == 1)
			{
				$result[$i]['7'] = '<a id="del_'.$no.'" href="/data/hapus_mesin_absensi/'.$sheet->id.'" class="btn btn-danger del" data-toggle="tooltip" data-placement="top" title="Hapus">
								<i class="glyphicon glyphicon-trash"></i>
								</a>
								<script>
								$(document).ready(function () {
									$("#del_'.$no.'").click(function(){
										if (!confirm("Do you want to delete")){
										  return false;
										}
									});
								});
								</script>
								';
			}
			else
			{
				$result[$i]['7'] = '';
			}
			
			$no++;
			$i++;
		}
		
		$data = $result;
		$results = array(
			"iTotalRecords" => ($sheet_total),
			"iTotalDisplayRecords" => ($sheet_total),
			"aaData"=>$data);
			
		echo json_encode($results);
	}
	
	function get_enroll($id_mesin)
	{
		$s = "select count(nip) as jml
				from d_mesin_absensi_user a
				where a.id_mesin = '$id_mesin'
				";
		$q = $this->db->query($s);
		return $q->row()->jml;
	}
	
	public function view_enroll()
	{
		$id = $this->uri->segment(3);
		$c = $this->db->query("select id from d_mesin_absensi where id = '$id' ")->num_rows();
		if($c > 0)
		{
			$s = "select a.nip, 
						b.nama
					from d_mesin_absensi_user a
					left join d_pegawai b on(a.nip = b.nip)
					where a.id_mesin = '$id' 
					order by nip asc";
			$q = $this->db->query($s);
			$data = array(
				'title' => 'Data Mesin Absensi',
				'subtitle' => '',
				'q' => $q
			);
			$this->load->view('daftar_enrol',$data);
			input_log($this->userid,'Lihat Data Enroll Mesin Absensi dengan id: '.$id);
		}
	}
	
	public function tambah_mesin_absensi()
	{
			$data = array(
			'title' =>'Data Mesin',
			'subtitle' => '',
			'page_title' => 'Form Tambah Data Mesin Absensi',
			);	
			$this->load->helper(array('form', 'url'));
			$this->load->library('form_validation');
			$this->form_validation->set_rules('id_project', 'ID Project', 'required');
			$this->form_validation->set_rules('ip', 'IP', 'required');
			$this->form_validation->set_rules('serial_number', 'Serial Number', 'required');
			
			 if ($this->form_validation->run() == FALSE)
			{
				$this->load->view('form_mesin_absensi',$data);
			}
			else
			{	
				$posts = $this->input->post();
				$id_project = $this->db->escape_str($posts['id_project']);
				$ip = $this->db->escape_str($posts['ip']);
				$serial_number = $this->db->escape_str($posts['serial_number']);
				$updated = date('Y-m-d H:i:s');
				$sql = "insert into d_mesin_absensi 
						set id_project = '$id_project',
							ip = '$ip',
							serial_number = '$serial_number',
							updated = '$updated'
						";
				//echo $sql;
				$this->db->query($sql);	
				$id = $this->db->insert_id();
				input_log($this->userid,'Tambah data mesin pegawai dengan id: '.$id);
				redirect('/data/mesin_absensi');
			}	
		
	}
	
	public function edit_mesin_absensi()
	{
		$id = $this->uri->segment(3);
		$id = (int) $id;
		if(isset($id) && $id != 0)
		{
			$row = $this->data_model->get_data_mesin($id)->row_array();
			$data = array(
				'title' =>'Data Mesin',
				'subtitle' => '',
				'page_title' => 'Form Edit Mesin Absensi',
				'id' => $id,
				'row' => $row,
			);	
			
			$this->load->helper(array('form', 'url'));
			$this->load->library('form_validation');
			
			$this->form_validation->set_rules('ip', 'IP', 'required');
			$this->form_validation->set_rules('serial_number', 'Serial Number', 'required');
			
			 if ($this->form_validation->run() == FALSE)
			{
				$this->load->view('form_mesin_absensi',$data);
			}
			else
			{	
				$posts = $this->input->post();
				
				if(isset($posts['id_project']) && $posts['id_project'] != ''  && $posts['id_project'] != NULL)
				{
					$id_project = $this->db->escape_str($posts['id_project']);
				}
				else
				{
					$id_project = $row['id_project'];;
				}
				$ip = $this->db->escape_str($posts['ip']);
				$serial_number = $this->db->escape_str($posts['serial_number']);
				$updated = date('Y-m-d H:i:s');
				
				$sql = "UPDATE d_mesin_absensi 
						set id_project = '$id_project',
							ip = '$ip',
							serial_number = '$serial_number',
							updated = '$updated'
						where id = '$id'
													";
				//echo $sql;
				$this->db->query($sql);	
				
				input_log($this->userid,'Edit mesin absensi dengan id: '.$id);
				redirect('/data/mesin_absensi');
			}	
		
		}
		
	}
	
	public function hapus_mesin_absensi()
	{
		$id = $this->uri->segment(3);
		$sc = "select * from d_mesin_absensi where id = '$id' ";
		$q = $this->db->query($sc);
		if($q->num_rows() > 0)
		{
			$r = $q->row();	
			$s = "delete from d_mesin_absensi where id = '$id' ";
			$this->db->query($s);
			redirect('/data/mesin_absensi');
		}
		else
		{
			redirect('/');
		}
		
	}
	
	public function validate_mesin_absensi()
	{
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->form_validation->set_rules('sssubunit', 'lokasi', 'required');
	}
	
	public function mesin_pegawai()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		
		$data = array(
				'title' => 'Data Mesin Pegawai',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'id_mesin' => $this->session->userdata('id_mesin_mesin_pegawai'),
		);
		$this->load->view('mesin_pegawai',$data);
		input_log($this->userid,'Lihat Data Mesin dan Pegawai');
	}
	
	public function get_mesin_pegawai()
	{
		//print_r($_REQUEST);
	//	$order = $_REQUEST['order'][0];
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/data/mesin_pegawai');
		//$aColumns = array( 'nip', 'nip','nama', 'nip','nip');
		$id_mesin = $this->input->post('id_mesin');
		$this->session->set_userdata('id_mesin_mesin_pegawai',$id_mesin);
		$sSearch = '';
		
		$sSearch .= " and (a.id_mesin like '%".$id_mesin."%') ";
		
		//limit
		if ( isset( $_REQUEST['start'] ) && $_REQUEST['length'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_REQUEST['start'] ).", ".
				intval( $_REQUEST['length'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "`".$aColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]."` ".
						($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		//$sOrder = " order by ".$aColumns[$order['column']]." ".$order['dir']." ";
		$s0 = "select count(a.id) as jml 
				from d_mesin_absensi_user a
				LEFT JOIN d_mesin_absensi b on(a.id_mesin = b.id)
				where 1 = 1
				";
		$s0 .= $sSearch;
		
		$sheet_total = $this->db->query($s0)->row();
		$sheet_total = $sheet_total->jml;
	
		$s1 = "SELECT DISTINCT a.*,
						b.serial_number,
					(select nama from d_project where id = b.id_project ) as nama_project,
					(select nama from d_pegawai where nip = a.nip) as nama_pegawai
			FROM d_mesin_absensi_user a
			left join d_mesin_absensi b on(a.id_mesin = b.id)
			WHERE 1=1";
		$s1 .= $sSearch;
		$s1 .= " order by nip asc ";
		
		$s1 .= $sLimit;
	//	echo $s1;
		$sheet1 = $this->db->query($s1);
		$result = array();
		$i=0;
		$no=1;
		foreach($sheet1->result() as $sheet)
		{
			$result[$i]['0'] = $no;
			$result[$i]['1'] = $sheet->serial_number;
			$result[$i]['2'] = $sheet->nama_project;
			$result[$i]['3'] = $sheet->nip;
			$result[$i]['4'] = $sheet->nama_pegawai;
			
			if($hak_akses['de'] == 1)
			{
				$result[$i]['5'] = '<a id="del_'.$no.'" href="/data/hapus_mesin_pegawai/'.$sheet->id.'" class="btn btn-danger del" data-toggle="tooltip" data-placement="top" title="Hapus">
								<i class="glyphicon glyphicon-trash"></i>
								</a>
								<script>
								$(document).ready(function () {
									$("#del_'.$no.'").click(function(){
										if (!confirm("Do you want to delete")){
										  return false;
										}
									});
								});
								</script>
								';
			}
			else
			{
				$result[$i]['5'] = '';
			}
			
			$no++;
			$i++;
		}
		
		$data = $result;
		$results = array(
			"iTotalRecords" => ($sheet_total),
			"iTotalDisplayRecords" => ($sheet_total),
			"aaData"=>$data);
			
		echo json_encode($results);
	}
	
	public function edit_mesin_pegawai()
	{
		$id = $this->uri->segment(3);
		$id = (int) $id;
		if(isset($id) && $id != 0)
		{
			$data = array(
			'title' =>'Data Mesin Pegawai',
			'subtitle' => '',
			'page_title' => 'Form Edit Mesin Pegawai',
			'id' => $id,
			'row' => $this->data_model->get_data_mesin_pegawai($id)->row_array(),
			);	
			$this->validate_mesin_absensi_pegawai();
			 if ($this->form_validation->run() == FALSE)
			{
				$this->load->view('form_edit_mesin_pegawai',$data);
			}
			else
			{	
				$posts = $this->input->post();
				$pin = $this->db->escape_str($posts['pin']);
				$nip = $this->db->escape_str($posts['nip']);
				
				$sql = "update mesin_absensi_user 
						set pin = '$pin',
							nip = '$nip'
						where id = '$id'
						";
				//echo $sql;
				$this->db->query($sql);	
				
				if(isset($id) && $id != 'images'){input_log($this->userid,'Edit data mesin absensi pegawai dengan id: '.$id);}
				redirect('/data/mesin_pegawai');
			}	
		
		}
		
	}
	
	public function hapus_mesin_pegawai()
	{
		$id = $this->uri->segment(3);
		$s = "delete from d_mesin_absensi_user where id = '$id' ";
		$this->db->query($s);
		input_log($this->userid,'Hapus Data Mesin Pegawai dengan id:'.$id);
		redirect('/data/mesin_pegawai');
	}
	
	public function validate_mesin_absensi_pegawai()
	{
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->form_validation->set_rules('pin', 'pin', 'required');
		$this->form_validation->set_rules('nip', 'NIP', 'required');
	}
	
	public function tambah_mesin_pegawai()
	{
			$data = array(
			'title' =>'Data Mesin',
			'subtitle' => '',
			'page_title' => 'Form Tambah Data Mesin Pegawai',
			);	
			$this->validate_mesin_pegawai();
			 if ($this->form_validation->run() == FALSE)
			{
				$this->load->view('form_mesin_pegawai',$data);
			}
			else
			{	
				$posts = $this->input->post();
				$id_mesin = $this->db->escape_str($posts['id_mesin']);
				$nip = $this->db->escape_str($posts['nip']);
				
				$sql = "insert into d_mesin_absensi_user 
						set id_mesin = '$id_mesin',
							nip = '$nip'
						";
				//echo $sql;
				$this->db->query($sql);	
				$id = $this->db->insert_id();
				input_log($this->userid,'Tambah data mesin pegawai dengan id: '.$id);
				redirect('/data/mesin_pegawai');
			}	
		
	}
	
	public function serial_number($id_mesin)
	{
		$s = "select serial_number from mesin_absensi where id = '$id_mesin' ";
		$r = $this->db->query($s)->row();
		return $r->serial_number;
	}
	
	public function validate_mesin_pegawai()
	{
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->form_validation->set_rules('id_mesin', 'ID Mesin', 'required');
		//$this->form_validation->set_rules('pin', 'pin', 'required');
		$this->form_validation->set_rules('nip', 'NIP', 'required');
	}
	
	public function sinkronisasi_pegawai()
	{
		set_time_limit(0);
		$pegawais =	file_get_contents(alamataplikasi.'restserver/index.php/pegawai/absensis/format/json');
		$pegawais = json_decode($pegawais,TRUE);
		//print_r($pegawais);
		if(isset($pegawais) && count($pegawais) > 0)
		{
			$this->db->truncate('d_pegawai');
			foreach($pegawais as $pegawai)
			{
				//echo $pegawai['NIP'].'</br>';
				$data_pegawai =	file_get_contents('http://siap-kota-bekasi/restserver/index.php/pegawai/absensi/nip/'.$pegawai['NIP'].'/format/json');
				$data_pegawai = json_decode($data_pegawai,TRUE);
				foreach($data_pegawai as $peg)
				{
					$q = $this->db->insert('d_pegawai', $peg); 
				}
				
			}
		}
		input_log($this->userid,'Sinkronisasi Data Pegawai PNS');
		redirect('/data/pegawai');
	}
	
	public function sinkronisasi_tkk()
	{
		set_time_limit(0);
		$tkks =	file_get_contents(alamataplikasi.'restserver/index.php/pegawai/tkks/format/json');
		$tkks = json_decode($tkks,TRUE);
		//print_r($tkks);
		if(isset($tkks) && count($tkks) > 0)
		{
			$this->db->truncate('d_tkk');
			foreach($tkks as $tkk)
			{
				//echo 'pegawai: '.$tkk['nik'].'</br>';
				if($tkk['nik'] != '')
				{
					$data_tkk =	file_get_contents('http://siap-kota-bekasi/restserver/index.php/pegawai/tkk/nik/'.$tkk['nik'].'/format/json');
					$data_tkk = json_decode($data_tkk,TRUE);
					foreach($data_tkk as $peg)
					{
						$q = $this->db->insert('d_tkk', $peg); 
					}				
				}
				
			}
		}
		input_log($this->userid,'Sinkronisasi Data Pegawai TKK');
		redirect('/data/tkk');
	}
	
	public function profil()
	{
		$r = $this->ion_auth->user()->row();
		$this->load->library('form_validation');
		$this->form_validation->set_rules('first_name', 'Nama Depan', 'trim|required');
		$this->form_validation->set_rules('last_name', 'Nama Belakang', 'trim|required');
		$this->form_validation->set_rules('phone', 'Telepon', 'trim');
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		
		if ($this->form_validation->run() === TRUE)
		{
			$posts = $this->input->post();
			print_r($posts);
			echo '</br>';
			$first_name = $this->db->escape_str($posts['first_name']);
			$last_name = $this->db->escape_str($posts['last_name']);
			$phone = $this->db->escape_str($posts['phone']);
			$email = $this->db->escape_str($posts['email']);
			
			//upload foto
			$config['upload_path'] = 'assets/foto/';
			$config['allowed_types'] = 'gif|jpg|png';
			$this->load->library('upload', $config);
			$field_name = 'userfile';
			if ($this->upload->do_upload($field_name))
			{
				$data = $this->upload->data();
				$foto = $data['file_name'];
			}
			else
			{
				$foto = $r->foto;
				$this->session->set_flashdata('message', $this->upload->display_errors());	
			}	
				$sql = 	"update t_users 
						SET first_name = '$first_name', 
							last_name = '$last_name',
							phone = '$phone',
							email = '$email',
							foto = '$foto'
						WHERE id = '$r->id'
						";
				echo $sql.'</br>';
				$this->db->query($sql);	
				
			redirect('/dashboard/profil');
		}
		else
		{
			$this->data['first_name'] = array(
                'name'  => 'first_name',
                'id'    => 'first_name',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('first_name',$r->first_name),
                'class'  => 'form-control',
				'disabled' => '',
            );
            $this->data['last_name'] = array(
                'name'  => 'last_name',
                'id'    => 'last_name',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('last_name',$r->last_name),
                'class'  => 'form-control',
				'disabled' => '',
            );
			
            $this->data['email'] = array(
                'name'  => 'email',
                'id'    => 'email',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('email',$r->email),
                'class'  => 'form-control',
				'disabled' => '',
            );
			
			 $this->data['username'] = array(
                'name'  => 'username',
                'id'    => 'username',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('username',$r->username),
                'class'  => 'form-control',
				'disabled' => '',
            );
			
            $this->data['phone'] = array(
                'name'  => 'phone',
                'id'    => 'phone',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('phone',$r->phone),
                'class'  => 'form-control',
				'disabled' => '',
            );
										
			$this->data['title'] = 'Profil';
			$this->data['subtitle'] = '';
			$this->data['foto'] = $r->foto;
			
			$this->load->view('profil',$this->data);
		}
		
	}
	
	public function ganti_password()
	{
		$user = $this->ion_auth->user()->row();
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
		$this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');
		
		if ($this->form_validation->run() === TRUE)
		{
			$posts = $this->input->post();
			$password = $this->db->escape_str($posts['password']);
			$data = array(
						'password' => $password,
						 );
			$this->ion_auth->update($user->id, $data);
			
			$this->session->set_flashdata('message', $this->ion_auth->messages() );			
			redirect('/dashboard/ganti_password');
		}
		else
		{
			 $this->data['email'] = array(
                'name'  => 'email',
                'id'    => 'email',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('email',$user->email),
                'class'  => 'form-control',
				'disabled' => '',
            );
			
			$this->data['password'] = array(
			'name' => 'password',
			'id'   => 'password',
			'type' => 'password',
			'class'  => 'form-control',
			);
			$this->data['password_confirm'] = array(
				'name' => 'password_confirm',
				'id'   => 'password_confirm',
				'type' => 'password',
				'class'  => 'form-control',
			);
							
			$this->data['title'] = 'Form Ganti Password';
			$this->data['subtitle'] = '';
			$this->data['message']='';
			
			$this->load->view('ganti_password',$this->data);
		}
		
	}
	
	public function combo_wilayah()
	{
		$array = array();
		if ($_GET['_name'] == 'kode_provinsi' OR $_GET['_name'] == 'KINSPROP' OR $_GET['_name'] == 'KPROP') 
		{
			$kode_provinsi = substr($_GET['_value'],0,2);
			
				$sql = "SELECT KWIL, 
							NWIL
					FROM ref_wilayah 
					WHERE MID(KWIL,1,2) = '$kode_provinsi' 
					AND		TWIL = '2'
					ORDER BY KWIL";
				$query = $this->db->query($sql);
				//$array[] = array('0' => '----------');
				foreach($query->result_array() as $row)
				{
					$array[] = array($row['KWIL'] => $row['NWIL']);
				}
		}
		elseif ($_GET['_name'] == 'kode_kabupaten' OR $_GET['_name'] == 'KINSKAB' OR $_GET['_name'] == 'KKAB') 
		{
			$kode_kabupaten = substr($_GET['_value'],0,4);
			$sql = "SELECT KWIL, 
							NWIL
					FROM ref_wilayah 
					WHERE MID(KWIL,1,4) = '$kode_kabupaten' 
					AND		TWIL = '3'
					ORDER BY KWIL";
				$query = $this->db->query($sql);
				//$array[] = array('0' => '----------');
				foreach($query->result_array() as $row)
				{
					$array[] = array($row['KWIL'] => $row['NWIL']);
				}
		} 
		elseif ($_GET['_name'] == 'kode_kecamatan' OR $_GET['_name'] == 'KINSKEC') 
		{
			$kode_kecamatan = substr($_GET['_value'],0,6);
			$sql = "SELECT KWIL, 
							NWIL
					FROM ref_wilayah 
					WHERE MID(KWIL,1,6) = '$kode_kecamatan' 
					AND		TWIL = '4'
					ORDER BY KWIL";
				$query = $this->db->query($sql);
			//	$array[] = array('0' => '----------');
				foreach($query->result_array() as $row)
				{
					$array[] = array($row['KWIL'] => $row['NWIL']);
				}
		} 
		
		echo json_encode( $array );
	}
	
	public function combo_unit()
	{
		$array = array();
		if ($_GET['_name'] == 'unit') 
		{
			$unit = $_GET['_value'];
			if($unit != '100000000000')
			{
				$sql = "select * 
					from referensi_unit_kerja
					where mid(kunker,1,4) = '".substr($unit,0,4)."'
					and mid(kunker,7,6) = '000000'
					
					order by KUNKER asc
				";
				$sql = "SELECT * 
											FROM referensi_unit_kerja 
											WHERE (IF(MID('$unit',3,2)=10 || MID('$unit',3,2)=11,
												mid(kunker,1,6) = mid('$unit',1,6)  ,
												mid(kunker,1,4) = mid('$unit',1,4)  )
											)
											AND (IF(MID('$unit',3,2)=10 || MID('$unit',3,2)=11,
												mid(kunker,9,4) = '0000' ,
												mid(kunker,7,6) = '000000' )
											)
											
											ORDER BY KUNKER";
				//echo $sql;
				$query = $this->db->query($sql);
				foreach($query->result_array() as $row)
				{
					$array[] = array($row['KUNKER'] => $row['NUNKER']);
				}
			}
			else
			{
				$array[] = array('100000000000' => 'Semua');
			}
			
		}
		elseif ($_GET['_name'] == 'subunit') 
		{
			$subunit = $_GET['_value'];
			if($subunit != '100000000000')
			{
				$sql = "select * 
					from referensi_unit_kerja
					where mid(kunker,1,6) = '".substr($subunit,0,6)."'
					and mid(kunker,9,4) = '0000'
					
					order by KUNKER asc
				";
				$sql = "SELECT * 
											FROM referensi_unit_kerja 
											WHERE (IF(MID('$subunit',3,2)=10 || MID('$subunit',3,2)=11,
												mid(kunker,1,8) = mid('$subunit',1,8)  ,
												mid(kunker,1,6) = mid('$subunit',1,6)  )
											)
											AND (IF(MID('$subunit',3,2)=10 || MID('$subunit',3,2)=11,
												mid(kunker,11,2) = '00' ,
												mid(kunker,9,4) = '0000' )
											)
											
											ORDER BY KUNKER";
				//echo $sql;
				$query = $this->db->query($sql);
				foreach($query->result_array() as $row)
				{
					$array[] = array($row['KUNKER'] => $row['NUNKER']);
				}
			}
			else
			{
				$array[] = array('100000000000' => 'Semua');
			}
			
		}
		elseif ($_GET['_name'] == 'ssubunit') 
		{
			$ssubunit = $_GET['_value'];
			if($ssubunit != '100000000000')
			{
				$sql = "select * 
					from referensi_unit_kerja
					where mid(kunker,1,8) = '".substr($ssubunit,0,8)."'
					and mid(kunker,11,2) = '00'
					
					order by KUNKER asc
				";
				$sql = "SELECT * 
											FROM referensi_unit_kerja 
											WHERE (IF(MID('$ssubunit',3,2)=10 || MID('$ssubunit',3,2)=11,
												mid(kunker,1,10) = mid('$ssubunit',1,10)  ,
												mid(kunker,1,8) = mid('$ssubunit',1,8)  )
											)
											AND (IF(MID('$ssubunit',3,2)=10 || MID('$ssubunit',3,2)=11,
												1=1,
												mid(kunker,11,2) = '00' )
											)
											
											ORDER BY KUNKER";
				//echo $sql;
				$query = $this->db->query($sql);
				foreach($query->result_array() as $row)
				{
					$array[] = array($row['KUNKER'] => $row['NUNKER']);
				}
			}
			else
			{
				$array[] = array('100000000000' => 'Semua');
			}
			
		}
		elseif ($_GET['_name'] == 'sssubunit') 
		{
			$sssubunit = $_GET['_value'];
			if($sssubunit != '100000000000')
			{
				$sql = "SELECT a.id,
							a.serial_number,
							(select nunker from referensi_unit_kerja where kunker = a.lokasi ) as lokasi_mesin
						FROM mesin_absensi a
						where 1=1
						and a.lokasi = '$sssubunit'
						ORDER BY id";
				//echo $sql;
				$query = $this->db->query($sql);
				foreach($query->result_array() as $row)
				{
					$array[] = array($row['id'] => $row['serial_number'].'-'.$row['lokasi_mesin']);
				}
			}
			
		}
		
		echo json_encode( $array );
	}
	
	public function cari_mesin()
	{
		$q = $this->db->escape_str($_REQUEST['q']);
		$s = "select a.id,
					a.serial_number,
					CONCAT(a.serial_number,
							'-',
							b.nama
							)  AS serial_nunker
				FROM d_mesin_absensi a
				LEFT JOIN d_project b on(b.id=a.id_project)
				where 1=1
				";
		$s .=" and (a.serial_number like '%".$q."%' or b.nama like '%".$q."%' )";
		$q = $this->db->query($s);
		$res = array();
		foreach($q->result_array() as $r)
		{
			$res[] = $r;
		}
		echo json_encode($res);
	}
	
	public function cari_project()
	{
		$q = $this->db->escape_str($_REQUEST['q']);
		$s = "select a.*
				FROM d_project a
				where 1=1
				";
		$s .=" and (a.nama like '%".$q."%' )";
		$q = $this->db->query($s);
		$res = array();
		foreach($q->result_array() as $r)
		{
			$res[] = $r;
		}
		echo json_encode($res);
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */