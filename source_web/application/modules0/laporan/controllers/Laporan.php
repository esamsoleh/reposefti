<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan extends CI_Controller {
	public $apps;
	var $nama_user;
	var $file_foto;
	var $uri_string;
	var $s_biro;
	var $user_id;
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
		if (!$this->ion_auth->logged_in())
			{
				redirect('/auth/login', 'refresh');
			}
		$this->apps = 'Laporan';
		$this->load->model('log_model');
		$this->nama_user = $this->ion_auth->user()->row()->first_name.' '.$this->ion_auth->user()->row()->last_name;
		$this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		$this->userid = $this->session->userdata('s_username');
		$this->uri_string = '/'.$this->uri->uri_string();
		$this->s_biro = $this->session->userdata('s_biro');
		$this->user_id = $this->ion_auth->user()->row()->id;
	}
	
	public function rekap_status_helm()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		$bulan = $this->session->userdata('bulan_rekap_status_helm');
		$tahun = $this->session->userdata('tahun_rekap_status_helm');
		$data = array(
				'title' => 'Data Rekap Status Helm',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'nama' => $this->session->userdata('nama_rekap_status_helm'),
				'reqnip' => $this->session->userdata('reqnip_rekap_status_helm'),
				'bulan' => ((isset($bulan) && $bulan != '')?$bulan:date('n')),
				'tahun' => ((isset($tahun) && $tahun != '')?$tahun:date('Y')),
		);
		$this->load->view('rekap_status_helm',$data);
		input_log($this->user_id,'Lihat Data Rekap Status Helm');
	}
	
	public function get_rekap_status_helm()
	{
		set_time_limit(0);
		//print_r($_REQUEST);
		//$order = $_REQUEST['order'][0];
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/laporan/rekap_status_helm');
		//$aColumns = array( 'nip', 'nip','nama', 'nip','nip');
		$nama = $this->input->post('nama');
		$this->session->set_userdata('nama_rekap_status_helm',$nama);
		$reqnip = $this->input->post('reqnip');
		$this->session->set_userdata('reqnip_rekap_status_helm',$reqnip);
		$bulan = $this->input->post('bulan');
		$this->session->set_userdata('bulan_rekap_status_helm',$bulan);
		$tahun = $this->input->post('tahun');
		$this->session->set_userdata('tahun_rekap_status_helm',$tahun);
		
		$sSearch = '';
		
		if(isset($nama) && $nama != '')
		{
			$sSearch .= " and (a.nama like '%".$nama."%') ";
		}
		if(isset($reqnip) && $reqnip != '')
		{
			$sSearch .= " and (a.nip like '%".$reqnip."%') ";
		}
		
		//limit
		if ( isset( $_REQUEST['start'] ) && $_REQUEST['length'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_REQUEST['start'] ).", ".
				intval( $_REQUEST['length'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "`".$aColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]."` ".
						($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		//$sOrder = " order by ".$aColumns[$order['column']]." ".$order['dir']." ";
		$s0 = "select count(nip) as jml
				from d_pegawai a 
				where 1 = 1
				";
		$s0 .= $sSearch;
		
		$sheet_total = $this->db->query($s0)->row();
		$sheet_total = $sheet_total->jml;
		$s1 = "set @rownum  = 0;";
	//	$this->db->query($s1);
		$s1 = "SELECT a.*,
						a.nip,
						c.nama as nama_jabatan
			FROM d_pegawai a
			left join ref_jabatan c on(a.id_jabatan = c.id)
			WHERE 1=1
			";
		$s1 .= $sSearch;
		$s1 .= " order by a.nip asc ";
		
		$s1 .= $sLimit;
		//echo $s1;
		$sheet1 = $this->db->query($s1);
		$result = array();
		$i=0;
		$no=1;
		foreach($sheet1->result() as $sheet)
		{
			$data_pegawai = data_pegawai($sheet->nip);
			$result[$i]['0'] = $no;
			$result[$i]['1'] = $sheet->nip;
			$result[$i]['2'] = $data_pegawai['nama'];
			$result[$i]['3'] = $data_pegawai['nama_jabatan'];	
			$result[$i]['4'] = '<a target="_blank" href="/laporan/r_status_helm/'.$sheet->nip.'/'.$bulan.'/'.$tahun.'">'.$this->get_tidak_pakai_helm($sheet->nip,$bulan,$tahun).'</a>';	
			$no++;
			$i++;
		}
		
		$data = $result;
		$results = array(
			"iTotalRecords" => ($sheet_total),
			"iTotalDisplayRecords" => ($sheet_total),
			"aaData"=>$data);
			
		echo json_encode($results);
	}
	
	function get_tidak_pakai_helm($nip,$bulan,$tahun)
	{
		$s = "select count(id) as jml_tidak_pakai
				from r_status_helm 
				where month(waktu) = '$bulan'
				and year(waktu) = '$tahun' 
				and nip = '$nip'
				";
		
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			$res = $q->row()->jml_tidak_pakai;;
		}
		else
		{
			$res = 0;
		}
		return $res;
	}
	
	function r_status_helm()
	{
		$nip = $this->uri->segment(3);
		$bulan = $this->uri->segment(4);
		$tahun = $this->uri->segment(5);
		$s = "select a.*,
					(if(a.status_helm = 0,'Lepas',if(a.status_helm = 1, 'Terpakai',if(a.status_helm = -1,'Tidak Konek','-')))) as status
				from r_status_helm a
				where a.nip = '$nip' 
				and month(a.waktu) = '$bulan' 
				and year(a.waktu) = '$tahun' 
				";
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			$data = array(
				'title' => 'Data Rekap Status Helm',
				'subtitle' => '',
				'q' => $q,
			);
			$this->load->view('r_status_helm',$data);
			input_log($this->user_id,'Lihat Data Riwayat Status Helm');
		}
		else
		{
			redirect('/');
		}
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */