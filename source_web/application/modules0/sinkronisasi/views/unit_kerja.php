<?php $this->load->view('header');?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('top');?>
  <!-- Left side column. contains the logo and sidebar -->
  
  <?php $this->load->view('sidebar');?>
 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title;?>
        <small><?=$subtitle;?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
							<div class="col-md-12">
								<div class="panel panel-white">
								<div class="panel-body">
									<div class="row">
										<form  id="form_sinkronisasi" method="post" action="/sinkronisasi/unit_kerja">
										<input type="hidden" name="sinkron" value="1">
										<div class="col-lg-4">
											<button class="btn btn-success" id="sinkron_data">Sinkronisasi Data</button>
										</div>
										</form>
									</div>
									</br>
									<div class="row">
										<form>
										<div class="col-lg-4">
											<input class="form-control" type="text" name="namacari" id="namacari" placeholder="Kata Kunci">
										</div>
										<div class="col-lg-4">
											<button class="btn btn-success" type="submit" id="load_cari_data">Cari</button>
										</div>
										</form>
									</div>
									</br>
									<div id="ReferensiContainer"></div>
								</div>
								</div>
							</div>
         
						</div>
						

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
 
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('javascript');?>
<link href="/assets/js/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
<script src="/assets/js/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<link href="/assets/js/jtable/themes/metro/green/jtable.min.css" rel="stylesheet" type="text/css" />
<script src="/assets/js/jtable/jquery.jtable.min.js" type="text/javascript"></script>

<script type="text/javascript">
         $(document).ready(function () {
			$('#ReferensiContainer').jtable({
                    title: "Daftar Unit Kerja",
                    paging: true, //Enable paging
                    pageSize: 10, //Set page size (default: 10)
                    sorting: true, //Enable sorting
                    defaultSorting: 'KUNKER ASC', //Set default sorting
                    actions: {
                        listAction: '/sinkronisasi/list_unit_kerja',
                      //  createAction: '/sinkronisasi/add_unit_kerja',
                        updateAction: '/sinkronisasi/update_unit_kerja',
                        deleteAction: '/sinkronisasi/del_unit_kerja',
					
                    },
                    fields: {
						KUNKER:{
						key:true,
						create:false,
						edit:false,
						list:true,	
						title:'Kode Unit Kerja',				
					},
					NUNKER:{
						create:false,
						edit:false,
						list:true,	
						title:'Nama Unit Kerja',				
					},
					NJAB:{
						create:false,
						edit:false,
						list:true,	
						title:'Nama Jabatan',				
					},
					KESELON:{		
						title:'Eselon',	
						create:false,
						edit:false,
						list:true,	
						options:'/sinkronisasi/eselon_options'
						
					},
						TUNJANGAN:{
						create:true,
						edit:true,
						list:true,	
						title:'Tunjangan (Rp)',				
					},
						TKT:{
						create:true,
						edit:true,
						list:true,	
						title:'TKT (Rp)',				
					},
						
				}
			});
			
			$('#load_cari_data').click(function (e) {
				e.preventDefault();
				$('#ReferensiContainer').jtable('load', {
					namacari: $('#namacari').val()
				});
			});
			//Load list from server
			$('#ReferensiContainer').jtable('load');
				
        });
    </script>
</html>
