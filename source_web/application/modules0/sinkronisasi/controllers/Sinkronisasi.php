<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sinkronisasi extends MX_Controller {
	
	private $field_selected = array(
		
		'1'=>array(
				'0'=> 'ID',
				'1'=> 'KGOLRU',
				'2'=> 'NGOLRU',
				'3'=> 'PANGKAT',
				'4'=> 'IGOLRU',
				'5'=> 'PPH',
			),
		'2'=>array(
				'0'=> 'ID',
				'1'=> 'KESELON',
				'2'=> 'NESELON',
				'3'=> 'MINKGOLRU',
				'3'=> 'MAXKGOLRU'
			),
		'3'=>array(
				'0'=> 'JNSJAB',
				'1'=> 'NJNSJAB',
			),
		'4'=>array(
				'0'=> 'KSTATUS',
				'1'=> 'NSTATUS',
			),
	);
	private $alias_meta_table = array(
		
		'1' => array(
				'0' => 'ID',
				'1' => 'Kode Golongan Ruang',
				'2' => 'Golongan',
				'3' => 'Pangkat',
				'4' => 'IGOLRU',
				'5' => 'PPH',
				),
		'2' => array(
				'0' => 'ID',
				'1' => 'Kode Eselon',
				'2' => 'Eselon',
				'3' => 'Golongan Minimal',
				'4' => 'Golongan Maksimal',
				),
		'3' => array(
				'0' => 'ID',
				'1' => 'Jenis Jabatan',
				),
		'4' => array(
				'0' => 'ID',
				'1' => 'Status',
				),
	);
	private $type_referensi = array(
		'Golongan Ruang',
		'Eselon',
		'Jenis Jabatan',
		'Status Pegawai',
	);
	
	
	public $apps;
	var $nama_user;
	var $file_foto;
	public function __construct()
     {
            parent::__construct();
			$this->load->database();
			$this->load->library(array('ion_auth'));
			$this->apps = 'Sinkronisasi';
			if (!$this->ion_auth->logged_in())
			{
				redirect('/auth/login', 'refresh');
			}
			$this->load->model('sinkronisasi_model');
			$this->nama_user = $this->ion_auth->user()->row()->first_name.' '.$this->ion_auth->user()->row()->last_name;
		//	$this->file_foto = get_foto();
			$this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
			$this->output->set_header('Pragma: no-cache');
			$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
			
       }
	   
	public function index()
	{
		
	}
	
	public function list_referensi()
	{
		//print_r($_REQUEST);
		if(isset($_REQUEST['namacari']))
		{
			$namacari = $_REQUEST['namacari'];
		}
		else
		{
			$namacari = '';
		}
		$referensi_id = $this->uri->segment(3);
		$sorting = $_REQUEST['jtSorting'];
		$index = $_REQUEST['jtStartIndex'];
		$size = $_REQUEST['jtPageSize'];
		$data = $this->sinkronisasi_model->get_referensi($referensi_id,$this->field_selected[$referensi_id],$sorting,$index,$size,$namacari);
		$data_semua = $this->sinkronisasi_model->get_referensi($referensi_id,$this->field_selected[$referensi_id],$sorting=NULL,$index=0,$size=0,$namacari);
		$recordCount = $data_semua->num_rows();
		$rows = array();
		foreach($data->result_array() as $row)
		{
			$rows[] = $row;
		}
		//print_r($rows);
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Records'] = $rows;
		$jTableResult['TotalRecordCount'] = $recordCount;
		print json_encode($jTableResult);
	}
	
	public function update_referensi()
	{
		$referensi_id = $this->uri->segment(3);
		//print_r($_REQUEST);
		//Update record in database
		// $result = mysql_query("UPDATE people SET Name = '" . $_POST["Name"] . "', Age = " . $_POST["Age"] . " WHERE PersonId = " . $_POST["PersonId"] . ";");

		$table = $this->sinkronisasi_model->getTable($referensi_id);
		//$fields = $this->sinkronisasi_model->getFields($referensi_id);
		$fields = $this->field_selected[$referensi_id];
		//print_r($fields);
		//echo count($fields);
		$sql = "update ".$table." set ";
		for($i=0;$i<count($fields);$i++)
		{
			if($i==(count($fields)-1))
			{
				$sql .= $fields[$i]." = '".$_POST[$fields[$i]]."' ";
			}
			else
			{
				$sql .= $fields[$i]." = '".$_POST[$fields[$i]]."', ";
			}
		}
		
		//		set ".$fields[1]->name." = '".$_POST[$fields[1]->name]."'
		$sql .= "where ".$fields[0]." = '".$_POST[$fields[0]]."' ";
	//	echo $sql;
		$this->db->query($sql);
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
	
	public function add_referensi()
	{
		$referensi_id = $this->uri->segment(3);
		//print_r($_REQUEST);
		//Update record in database
		// $result = mysql_query("UPDATE people SET Name = '" . $_POST["Name"] . "', Age = " . $_POST["Age"] . " WHERE PersonId = " . $_POST["PersonId"] . ";");

		$table = $this->sinkronisasi_model->getTable($referensi_id);
		//$fields = $this->sinkronisasi_model->getFields($referensi_id);
		$fields = $this->field_selected[$referensi_id];
		$sql = "insert into ".$table." set ";
		for($i=1;$i<count($fields);$i++)
		{
			if($i==(count($fields)-1))
			{
				$sql .= $fields[$i]." = '".$_POST[$fields[$i]]."' ";
			}
			else
			{
				$sql .= $fields[$i]." = '".$_POST[$fields[$i]]."', ";
			}
		}
		/*
		//		(".$fields[1]->name.")
		//		values ('".$_POST[$fields[1]->name]."') ";
		*/
		//echo $sql;
		$this->db->query($sql);
		
		//Get last inserted record (to return to jTable)
		$result = $this->db->query("SELECT * FROM ".$table." WHERE ".$fields[0]." = LAST_INSERT_ID();");
		$check = $result->row_array();
		if(isset($check) && count($check)>0)
		{
			$row = $check;
		}
		else
		{
			$result = $this->db->query("select * from ".$table." WHERE ".$fields[0]." = '".$_POST[$fields[0]]."' ");
			$row = $result->row_array();
		}
	//	print_r($row);
		
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Record'] = $row;
		print json_encode($jTableResult);
	}
	
	public function del_referensi()
	{
		$referensi_id = $this->uri->segment(3);
		$table = $this->sinkronisasi_model->getTable($referensi_id);
	//	$fields = $this->sinkronisasi_model->getFields($referensi_id);
		$fields = $this->field_selected[$referensi_id];
		$result = $this->db->query("DELETE FROM ".$table." WHERE ".$fields[0]." = '".$_POST[$fields[0]]."' ");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
	
	public function daftar()
	{
		$referensi_id = $this->uri->segment(3);
		
		if ($this->uri->segment(4) === FALSE) $row_id = 0; else $row_id = $this->uri->segment(4);
				
		$data = array(
			'title' =>'Referensi',
			'subtitle' => $this->type_referensi[$referensi_id-1],	
			'fields' => $this->sinkronisasi_model->getFields($referensi_id),
			'fields_alias' => $this->alias_meta_table[$referensi_id],
			'type_referensi' => $this->type_referensi[$referensi_id-1],
			'referensi_id' => $referensi_id,
			'field_selected' => $this->field_selected[$referensi_id],
				);
		
		$this->load->view('daftar',$data);
	}
	
	public function unit_kerja()
	{
		$sinkron = $this->input->post('sinkron');
		if(isset($sinkron) && $sinkron == 1)
		{
			$datareferensi = @file_get_contents(alamataplikasi.'restserver/index.php/absensi/unker/json');
			
			if($datareferensi != FALSE)
			{
				$datareferensi = json_decode($datareferensi,true);
				foreach($datareferensi as $ref)
				{
					$KUNKER = $ref['KUNKER'];
					$sc = "select KUNKER from referensi_unit_kerja where KUNKER = '$KUNKER' ";
					$qc = $this->db->query($sc);
					if($qc->num_rows() > 0)
					{
						$this->db->update('referensi_unit_kerja',$ref,array('KUNKER' => $KUNKER));
					}
					else
					{
						$this->db->insert('referensi_unit_kerja',$ref);
					}
				}
				
			}
		}
		$data = array(
			'title' =>'Referensi',
			'subtitle' => 'Unit Kerja',	
				);
		
		$this->load->view('unit_kerja',$data);
	}
	
	public function list_unit_kerja()
	{
		//print_r($_REQUEST);
		if(isset($_REQUEST['namacari']))
		{
			$namacari = $_REQUEST['namacari'];
		}
		else
		{
			$namacari = '';
		}
		$sorting = $_REQUEST['jtSorting'];
		$index = $_REQUEST['jtStartIndex'];
		$size = $_REQUEST['jtPageSize'];
		
		$sql = "select a.*, 
							b.NESELON
					from referensi_unit_kerja a
					left join referensi_eselon b
					on (a.KESELON = b.KESELON)
					where 1=1
					";
			if(isset($namacari) && $namacari != '')
			{
				$sql .= " AND (a.NUNKER like '%".$namacari."%' || a.NJAB like '%".$namacari."%' ) ";
			}
		$data_semua = $this->db->query($sql);
			if($sorting != NULL)
			{
				$sql .= "order by ".$sorting." ";
			}
			if(($size != 0))
			{
				$sql.=" limit $index,$size";
			}
		
		$data = $this->db->query($sql);
		$recordCount = $data_semua->num_rows();
		$rows = array();
		foreach($data->result_array() as $row)
		{
			$rows[] = $row;
		}
		//print_r($rows);
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Records'] = $rows;
		$jTableResult['TotalRecordCount'] = $recordCount;
		print json_encode($jTableResult);
	}
	
	public function eselon_options()
	{
		$sql = "select * from referensi_eselon";
		$query = $this->db->query($sql);
		$rows = array();
		
		foreach($query->result_array() as $row)
		{
			$rows[] = array(
				'DisplayText' => $row['NESELON'],
				'Value' => $row['KESELON']
				);
		}
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Options'] = $rows;
		print json_encode($jTableResult);
	}
	
	public function update_unit_kerja()
	{
		$posts = $this->input->post();
	//	print_r($posts);
		$sql = "update referensi_unit_kerja 
				set TUNJANGAN = '$posts[TUNJANGAN]',
					TKT = '$posts[TKT]'
				";
		
		$sql .= "where KUNKER = '".$posts['KUNKER']."' ";
	//	echo $sql;
		$this->db->query($sql);
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
	
	public function del_unit_kerja()
	{
		$result = $this->db->query("DELETE FROM referensi_unit_kerja WHERE KUNKER = '".$_POST['KUNKER']."' ");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
	
	public function jft()
	{
		$sinkron = $this->input->post('sinkron');
		if(isset($sinkron) && $sinkron == 1)
		{
			$datareferensi = @file_get_contents(alamataplikasi.'restserver/index.php/absensi/jft/json');
			if($datareferensi != FALSE)
			{
				$datareferensi = json_decode($datareferensi,true);
				foreach($datareferensi as $ref)
				{
					$KJAB = $ref['KJAB'];
					$sc = "select KJAB from referensi_jabatan_fungsional_tertentu where KJAB = '$KJAB' ";
					$qc = $this->db->query($sc);
					if($qc->num_rows() > 0)
					{
						$this->db->update('referensi_jabatan_fungsional_tertentu',$ref,array('KJAB' => $KJAB));
					}
					else
					{
						$this->db->insert('referensi_jabatan_fungsional_tertentu',$ref);
					}
				}
				
			}
		}
		$data = array(
			'title' =>'Referensi',
			'subtitle' => 'Jabatan Fungsional Tertentu',	
				);
		
		$this->load->view('jft',$data);
	}
	
	public function list_jft()
	{
		//print_r($_REQUEST);
		if(isset($_REQUEST['namacari']))
		{
			$namacari = $_REQUEST['namacari'];
		}
		else
		{
			$namacari = '';
		}
		$sorting = $_REQUEST['jtSorting'];
		$index = $_REQUEST['jtStartIndex'];
		$size = $_REQUEST['jtPageSize'];
		
		$sql = "select a.*
					from referensi_jabatan_fungsional_tertentu a
					where 1=1
					";
			if(isset($namacari) && $namacari != '')
			{
				$sql .= " AND (a.NJAB like '%".$namacari."%' || a.NJAB like '%".$namacari."%' ) ";
			}
		$data_semua = $this->db->query($sql);
			if($sorting != NULL)
			{
				$sql .= "order by ".$sorting." ";
			}
			if(($size != 0))
			{
				$sql.=" limit $index,$size";
			}
		
		$data = $this->db->query($sql);
		$recordCount = $data_semua->num_rows();
		$rows = array();
		foreach($data->result_array() as $row)
		{
			$rows[] = $row;
		}
		//print_r($rows);
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Records'] = $rows;
		$jTableResult['TotalRecordCount'] = $recordCount;
		print json_encode($jTableResult);
	}
	
	public function update_jft()
	{
		$posts = $this->input->post();
	//	print_r($posts);
		$sql = "update referensi_jabatan_fungsional_tertentu 
				set TUNJANGAN = '$posts[TUNJANGAN]',
					TKT = '$posts[TKT]'
				";
		
		$sql .= "where KJAB = '".$posts['KJAB']."' ";
	//	echo $sql;
		$this->db->query($sql);
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
	
	public function jenis_tabel_referensi()
	{
		$tables = $this->db->list_tables();
		$s = "show tables like 'ref_%' ";
		$s = "SELECT TABLE_NAME as tabel 
				FROM INFORMATION_SCHEMA.TABLES 
				WHERE TABLE_SCHEMA='ekonomi-unisba' 
				and TABLE_NAME like 'ref_%'
				order by tabel
				";
		$tables = $this->db->query($s);
		$rows = array();
		$rows[] = array(
			'DisplayText' => '----------',
			'Value' => '----------'
		);
		foreach ($tables->result_array() as $row)
		{
			$rows[] = array(
				'DisplayText' => $row['tabel'],
				'Value' => $row['tabel']
				);
		}
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Options'] = $rows;
		print json_encode($jTableResult);
	}
	
	public function sinkronisasi_referensi()
	{
		//print_r($this->input->post());
		$referensi_id = $this->input->post('referensi_id');
		//echo $referensi_id;
		$datareferensi = @file_get_contents(alamataplikasi.'restserver/index.php/absensi/referensi/referensi_id/$referensi_id/json');
		if($datareferensi != FALSE)
		{
			$datareferensi = json_decode($datareferensi,true);
			//print_r($datareferensi);
			$referensi = array(
				'referensi_golongan_ruang',
				'referensi_eselon',
				'referensi_jenis_jabatan',
				'referensi_status_pegawai',
			);
			
			$this->db->truncate($referensi[$referensi_id-1]);
			foreach($datareferensi as $ref)
			{
				print_r($ref);
				echo '</br>';
				$this->db->insert($referensi[$referensi_id-1],$ref);
			}
			redirect('/sinkronisasi/daftar/'.$referensi_id);
		}
		
	}
	
	public function impor_tunjangan()
	{
		$this->load->library('excel');
		$objReader =  PHPExcel_IOFactory::createReader('Excel2007');//excel2007
		$objReader->setReadDataOnly(true);
		$upfile = 'assets/impor/Tabel_Referensi_Tunjangan.xlsx';
		$objPHPExcel = $objReader->load($upfile); 
							
		$objPHPExcel->setActiveSheetIndex(0);
		$objWorksheet = $objPHPExcel->getActiveSheet();
		$highestRow = $objWorksheet->getHighestRow(); // e.g. 
		$kunker = '';			
		for ($row = 2; $row <=$highestRow; ++$row) 
		{
			$KUNKER = $objWorksheet->getCellByColumnAndRow(1, $row)->getValue();
			$KUNKER = $this->db->escape_str($KUNKER);
			$NJAB1 = $objWorksheet->getCellByColumnAndRow(2, $row)->getValue();
			$NJAB1 = $this->db->escape_str($NJAB1);
			$NJAB2 = $objWorksheet->getCellByColumnAndRow(3, $row)->getValue();
			$NJAB2 = $this->db->escape_str($NJAB2);
			$NJAB3 = $objWorksheet->getCellByColumnAndRow(4, $row)->getValue();
			$NJAB3 = $this->db->escape_str($NJAB3);
			$NJAB4 = $objWorksheet->getCellByColumnAndRow(5, $row)->getValue();
			$NJAB4 = $this->db->escape_str($NJAB4);
			$NJAB5 = $objWorksheet->getCellByColumnAndRow(6, $row)->getValue();
			$NJAB5 = $this->db->escape_str($NJAB5);
			$KESELON = $objWorksheet->getCellByColumnAndRow(7, $row)->getValue();
			$KESELON = $this->db->escape_str($KESELON);
			$TUNJANGAN = $objWorksheet->getCellByColumnAndRow(8, $row)->getValue();
			$TUNJANGAN = $this->db->escape_str($TUNJANGAN);
			$TKT = $objWorksheet->getCellByColumnAndRow(9, $row)->getValue();
			$TKT = $this->db->escape_str($TKT);
			$NUNKER = $objWorksheet->getCellByColumnAndRow(10, $row)->getValue();
			$NUNKER = $this->db->escape_str($NUNKER);
			if($NJAB1 != '')
			{
				$NJAB = $NJAB1;
			}
			elseif($NJAB2 != '')
			{
				$NJAB = $NJAB2;
			}
			elseif($NJAB3 != '')
			{
				$NJAB = $NJAB3;
			}
			elseif($NJAB4 != '')
			{
				$NJAB = $NJAB4;
			}
			elseif($NJAB5 != '')
			{
				$NJA5 = $NJAB5;
			}
			$sql_c = "select KUNKER from referensi_unit_kerja where KUNKER = '$KUNKER' ";
			$query = $this->db->query($sql_c);
					
			if($query->num_rows() == 0)
			{
				$sql = "insert into referensi_unit_kerja 
						set KUNKER = '$KUNKER',
							NUNKER = '$NUNKER',
							NJAB = '$NJAB',
							KESELON = '$KESELON',
							TUNJANGAN = '$TUNJANGAN',
							TKT = '$TKT'
					";
							  
				echo $sql."</BR>";
				$this->db->query($sql);									
			}
			else
			{
				$sql = "UPDATE referensi_unit_kerja 
						set TUNJANGAN = '$TUNJANGAN',
							TKT = '$TKT'
						WHERE KUNKER = '$KUNKER'
				";
					  
				echo $sql."</BR>";
				echo '</br>';
				$this->db->query($sql);
				
			}
								
		}
		
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */