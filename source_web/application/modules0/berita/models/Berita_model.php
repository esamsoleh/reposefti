<?php 

class berita_model extends CI_Model{

	protected $table = "berita";
	protected $key = "id_berita";
	
	public function __construct()
    {
            parent::__construct();
	}

	//untuk single query
	public function single_query($id){

		$query = $this->db->where($this->key,$id)->get($this->table)->row();
		return $query;
	
	}

	//untuk query tanpa paging dengan datatable
	public function get_all(){

		$query = $this->db->order_by($this->key,"desc")->get($this->table);
		return $query;

	}

	//untuk queyr alla
	public function get_query_all(){

		$query = $this->db->order_by($this->key,"desc")->limit(10,3)->get($this->table);
		return $query;

	}

	//untuk tambah data
	function tambah_data($data=array()){
		$tambah = $this->db->insert($this->table,$data);
	}

	//untuk edit edata 
	function edit_data($data=array(),$id){
		$edit = $this->db->where($this->key,$id)->update($this->table,$data);
		return $edit;
	}

	//untuk headline data
	function get_headline(){

		$query = $this->db->order_by($this->key,"desc")->where("gambar !=","")->limit(3,0)->get($this->table);
		return $query;
	}


	//untuk hapus data
	function hapus($id){
		$hapus = $this->db->where($this->key,$id)->delete($this->table);
	}


	
}

?>