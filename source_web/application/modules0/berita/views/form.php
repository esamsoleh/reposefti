<?php $this->load->view('header');?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('top');?>
  <!-- Left side column. contains the logo and sidebar -->
  
  <?php $this->load->view('sidebar');?>
 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

     <!-- Main content -->
    <section class="content">
		<div class="row">
		<div class="col-xs-12">
				<div class="panel panel-primary">
                    <div class="panel-heading">
                        Form Pengisian Gambar
                    </div>
					<div class="panel-body">

					  <?php if($this->session->flashdata("message_success")): ?>

						<div class="alert alert-success"><?php echo $this->session->flashdata("message_success");?></div>

					  <?php endif; ?>

					  <?php if($this->session->flashdata("message_error")): ?>

						<div class="alert alert-danger"><?php echo $this->session->flashdata("message_error");?></div>

					  <?php endif; ?>

					<form role="form" enctype="multipart/form-data" method="post" action="<?php echo current_url();?>">
					  <div class="form-group">
						<label for="exampleInputJudul">Judul *</label>
						<input type="text" class="form-control" required="required" name="judul" value="<?php echo set_value("judul",isset($rows->judul)?$rows->judul:"");?>" id="exampleInputJudul" placeholder="Enter Judul">
					  </div>
					  <div class="form-group">
						<label for="exampleInputPassword1">Deskripsi *</label>
						<textarea class="form-control mceEditor" name="deskripsi" ><?php echo set_value("deskripsi",isset($rows->deskripsi)?$rows->deskripsi:"");?></textarea>
					  </div>
					  <div class="form-group">
						<label for="exampleInputFile">Gambar</label>
						<input type="file" name="gambar" id="exampleInputFile">
						<p class="help-block">Gambar harus berektensi JPG,PNG atau GIF</p>

						<?php if(isset($rows->gambar)): ?>
						<p><img src="<?php echo base_url();?>assets/slider/<?php echo $rows->gambar;?>" style="width:200px;"></p>
					  <?php endif; ?>

					  </div>
					  
					  
					 
					  <button type="submit" name="simpan" class="btn btn-primary">simpan</button>
					  <a href="<?php echo site_url("berita/index");?>" class="btn btn-warning">Batal</a>
					</form>


					</div>
				</div>
			</div>

		</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
 
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('javascript');?>
<!-- FLOT CHARTS -->
<script src="/assets/components/Flot/jquery.flot.js"></script>
<script src="/assets/components/Flot/jquery.flot.valuelabels.js"></script>
<!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
<script src="/assets/components/Flot/jquery.flot.resize.js"></script>
<!-- FLOT PIE PLUGIN - also used to draw donut charts -->
<script src="/assets/components/Flot/jquery.flot.pie.js"></script>
<!-- FLOT CATEGORIES PLUGIN - Used to draw bar charts -->
<script src="/assets/components/Flot/jquery.flot.categories.js"></script>
<script src="<?=base_url();?>assets/js/dataTables/jquery.dataTables.js"></script>
<script src="<?=base_url();?>assets/js/dataTables/dataTables.bootstrap.js"></script>
<script language="javascript" type="text/javascript" src="<?=base_url();?>assets/js/tiny_mce/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">
	tinyMCE.init({
		mode : "textareas",
		editor_selector : "mceEditor",
		theme : "advanced",
		plugins : "table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,zoom,media,searchreplace,print,contextmenu,paste,directionality,fullscreen",
		theme_advanced_buttons1_add_before : "save,newdocument,separator",
		theme_advanced_buttons1_add : "fontselect,fontsizeselect",
		theme_advanced_buttons2_add : "separator,insertdate,inserttime,preview,zoom,separator,forecolor,backcolor",
		theme_advanced_buttons2_add_before: "cut,copy,paste,pastetext,pasteword,separator,search,replace,separator",
		theme_advanced_buttons3_add_before : "tablecontrols,separator",
		theme_advanced_buttons3_add : "emotions,iespell,media,advhr,separator,print,separator,ltr,rtl,separator,fullscreen",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		content_css : "example_word.css",
	    plugi2n_insertdate_dateFormat : "%Y-%m-%d",
	    plugi2n_insertdate_timeFormat : "%H:%M:%S",
		external_link_list_url : "example_link_list.js",
		external_image_list_url : "example_image_list.js",
		media_external_list_url : "example_media_list.js",
		file_browser_callback : "fileBrowserCallBack",
		paste_use_dialog : false,
		theme_advanced_resizing : true,
		theme_advanced_resize_horizontal : false,
		theme_advanced_link_targets : "_something=My somthing;_something2=My somthing2;_something3=My somthing3;",
		paste_auto_cleanup_on_paste : true,
		paste_convert_headers_to_strong : false,
		paste_strip_class_attributes : "all",
		paste_remove_spans : false,
		paste_remove_styles : false		
	});

	function fileBrowserCallBack(field_name, url, type, win) {
		// This is where you insert your custom filebrowser logic
		alert("Filebrowser callback: field_name: " + field_name + ", url: " + url + ", type: " + type);

		// Insert new URL, this would normaly be done in a popup
		win.document.forms[0].elements[field_name].value = "someurl.htm";
	}
</script>
</body>
</html>