<?php $this->load->view('header');?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('top');?>
  <!-- Left side column. contains the logo and sidebar -->
  
  <?php $this->load->view('sidebar');?>
 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

     <!-- Main content -->
    <section class="content">
	<div class="row">
			<div class="col-md-12">
				<div class="panel panel-white">
					<div class="panel-body">
					<div class="row">		
						<div class="col-xs-12">
							<div class="panel panel-primary">
								<div class="panel-heading">
									Daftar Gambar


									<a href="<?php echo site_url("berita/tambah");?>" class="btn btn-warning btn-mini pull-right">Tambah</a>
									<div style="clear:both"></div>
								</div>
								<div class="panel-body">
					<?php
					
					if(isset($rows) && $rows != NULL)
					{	
						if($rows->num_rows() > 0)
						{
						?>
						<?php if($this->session->flashdata("message_success")): ?>

						<div class="alert alert-success"><?php echo $this->session->flashdata("message_success");?></div>

					  <?php endif; ?>

					  <?php if($this->session->flashdata("message_error")): ?>

						<div class="alert alert-danger"><?php echo $this->session->flashdata("message_error");?></div>

					  <?php endif; ?>
								
									<div class="table-responsive">
										<table class="table table-striped table-bordered table-hover" id="tabel">
										<thead>
										<tr>
										<th>No</th>
										<th>Judul</th>
										<th>Tanggal</th>
										<th colspan="2">Aksi</th>
										</tr>
										</thead>
										<tbody>
										<?php
										$i=1;
										foreach($rows->result_array() as $row)
										{
											echo '<tr onclick="page_edit(\''.$row['id_berita'].'\')" >';
											echo '<td style="width:40px;">'.$i.'</td>';
											echo '<td>'.$row['judul'].'</td>';
											echo '<td>'.date("d M Y",strtotime($row['tanggal'])).'</td>';
											echo '<td style="width:100px;"><a href="'.site_url("berita/hapus/".$row["id_berita"]).'" onclick="return confirm(\'apakah yakin akan dihapus ?\')" class="btn btn-primary btn-danger">Hapus</a></td>';
											echo '<td style="width:100px;"><a href="'.site_url("berita/edit/".$row["id_berita"]).'" class="btn btn-primary btn-warning">Edit</a></td>';
											$i++;
										}
										?>
										</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<?php
						}
						else
						{
							?>
							<div class="col-xs-12">
							<div class="alert alert-success alert-dismissable">
								<strong>Data yang dicari tidak ada!</strong>				
							</div>
							</div>
							<?php
						}
					}
					?>
					</div>
					</div>
				</div>
			</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
 
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('javascript');?>
<!-- FLOT CHARTS -->
<script src="/assets/components/Flot/jquery.flot.js"></script>
<script src="/assets/components/Flot/jquery.flot.valuelabels.js"></script>
<!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
<script src="/assets/components/Flot/jquery.flot.resize.js"></script>
<!-- FLOT PIE PLUGIN - also used to draw donut charts -->
<script src="/assets/components/Flot/jquery.flot.pie.js"></script>
<!-- FLOT CATEGORIES PLUGIN - Used to draw bar charts -->
<script src="/assets/components/Flot/jquery.flot.categories.js"></script>
<script src="<?=base_url();?>assets/js/dataTables/jquery.dataTables.js"></script>
<script src="<?=base_url();?>assets/js/dataTables/dataTables.bootstrap.js"></script>
<script type="text/javascript">
         $(document).ready(function () {
			
			$('#tabel').dataTable({
					"bFilter": false, 
					"bInfo": true,
					"bLengthChange": false,
					});
		 });
		
</script>

</body>
</html>