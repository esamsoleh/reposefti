<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Koneksi extends CI_Controller {
	public $apps;
	var $nama_user;
	var $file_foto;
	var $uri_string;
	var $s_biro;
	var $userid;
	var $user_id;
	var $groupid;
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
		if (!$this->ion_auth->logged_in())
			{
				redirect('/auth/login', 'refresh');
			}
		$this->apps = 'Status Koneksi';
		$this->nama_user = $this->ion_auth->user()->row()->first_name.' '.$this->ion_auth->user()->row()->last_name;
		$this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		$this->userid = $this->session->userdata('s_username');
		$this->uri_string = '/'.$this->uri->uri_string();
		$this->user_id = $this->ion_auth->user()->row()->id;
		$this->groupid = $this->ion_auth->get_users_groups($this->user_id)->row()->id;
	}
	 
	public function smartgate()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		
		$data = array(
				'title' => 'Data Koneksi Smartgate',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
		);
		$this->load->view('smartgate',$data);
		input_log($this->user_id,'Lihat Data Koneksi Smartgate');
	}
	
	public function get_smartgate()
	{
		set_time_limit(0);
		//print_r($_REQUEST);
		//$order = $_REQUEST['order'][0];
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/koneksi/smartgate');
		//$aColumns = array( 'nip', 'nip','nama', 'nip','nip');
		
		$sSearch = '';
		
	/*	if(isset($nama) && $nama != '')
		{
			$sSearch .= " and (nama like '%".$nama."%') ";
		}
		if(isset($reqnip) && $reqnip != '')
		{
			$sSearch .= " and (a.nip = '".$reqnip."') ";
		}
		 if(isset($tanggal) && $tanggal != '')
		{
			$sSearch .= " and (date(b.tanggal) = '".date('Y-m-d',strtotime($tanggal))."') ";
		} */
	
		//limit
		if ( isset( $_REQUEST['start'] ) && $_REQUEST['length'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_REQUEST['start'] ).", ".
				intval( $_REQUEST['length'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "`".$aColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]."` ".
						($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		//$sOrder = " order by ".$aColumns[$order['column']]." ".$order['dir']." ";
		$s0 = "select count(id) as jml
				from d_mesin_absensi b 
				where 1 = 1
				";
		$s0 .= $sSearch;
		
		$sheet_total = $this->db->query($s0)->row();
		$sheet_total = $sheet_total->jml;
		$s1 = "set @rownum  = 0;";
	//	$this->db->query($s1);
		$s1 = "SELECT a.serial_number,
						a.ip,
						b.waktu,
						c.nama as nama_proyek
			FROM d_mesin_absensi a
			left join d_project c on(a.id_project = c.id)
			left join d_status_gate b on(a.serial_number = b.serial_number)
			WHERE 1=1
			";
		$s1 .= $sSearch;
		$s1 .= " order by a.id asc ";
		
		$s1 .= $sLimit;
		//echo $s1;
		$sheet1 = $this->db->query($s1);
		$result = array();
		$i=0;
		$no=1;
		foreach($sheet1->result() as $sheet)
		{
			$time_now = time();
			$time_waktu = strtotime($sheet->waktu);
			if(($time_now - $time_waktu) > 60)
			{
				$status = 'Off';
			}
			else
			{
				$status = 'On';
			}
			$result[$i]['0'] = $no;
			$result[$i]['1'] = $sheet->nama_proyek;
			$result[$i]['2'] = $sheet->ip;
			$result[$i]['3'] = $sheet->serial_number;
			$result[$i]['4'] = ((isset($sheet->waktu) && $sheet->waktu != NULL && $sheet->waktu != '')?date('d-M-Y H:i:s',strtotime($sheet->waktu)):'-');
			$result[$i]['5'] = $status;	
			$no++;
			$i++;
		}
		
		$data = $result;
		$results = array(
			"iTotalRecords" => ($sheet_total),
			"iTotalDisplayRecords" => ($sheet_total),
			"aaData"=>$data);
			
		echo json_encode($results);
	}
	
	public function smartwatch()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		
		$data = array(
				'title' => 'Data Koneksi Smart Watch',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'nama' => $this->session->userdata('nama_harian'),
				'reqnip' => $this->session->userdata('reqnip_harian'),
				'tanggal' => $this->session->userdata('tanggal_harian'),
		);
		$this->load->view('smartwatch',$data);
		input_log($this->user_id,'Lihat Data koneksi smart watch');
	}
	
	public function get_smartwatch()
	{
		
		//print_r($_REQUEST);
		//$order = $_REQUEST['order'][0];
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/koneksi/smartwatch');
		//$aColumns = array( 'nip', 'nip','nama', 'nip','nip');
		$nama = $this->input->post('nama');
		$this->session->set_userdata('nama_harian',$nama);
		$reqnip = $this->input->post('reqnip');
		$this->session->set_userdata('reqnip_harian',$reqnip);
		$tanggal = $this->input->post('tanggal');
		$this->session->set_userdata('tanggal_harian',$tanggal);
		$tgl = date('j',strtotime($tanggal));
		$bln = date('n',strtotime($tanggal));
		$thn = date('Y',strtotime($tanggal));
		$hak_edit = 1;
		//cek utk pencegatan
		/* $tgl_max_lapor = $this->db->query('select tanggal from ref_waktu_lapor')->row()->tanggal;
		$tgl_sekarang = date('j');
		if($tgl_sekarang < $tgl_max_lapor)
		{
			$bln_compare = date('n')-1;
			if($bln >= $bln_compare)
			{
				$hak_edit = 1;
			}
			elseif($bln < $bln_compare)
			{
				$hak_edit = 0;
			}
		}
		else
		{
			$bln_compare = date('n');
			if($bln == $bln_compare)
			{
				$hak_edit = 1;
			}
			elseif($bln < $bln_compare)
			{
				$hak_edit = 0;
			}
		} */
		
		$sSearch = '';
	/*	
		if(isset($nama) && $nama != '')
		{
			$sSearch .= " and (nama like '%".$nama."%') ";
		}
		if(isset($reqnip) && $reqnip != '')
		{
			$sSearch .= " and (a.nip = '".$reqnip."') ";
		}
		 if(isset($tgl) && $tgl != '')
		{
			$sSearch .= " and (b.tanggal = '".$tgl."') ";
		}
		if(isset($bln) && $bln != '')
		{
			$sSearch .= " and (b.bulan = '".$bln."') ";
		}
		if(isset($thn) && $thn != '')
		{
			$sSearch .= " and (b.tahun = '".$thn."') ";
		}
	 */
		//limit
		if ( isset( $_REQUEST['start'] ) && $_REQUEST['length'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_REQUEST['start'] ).", ".
				intval( $_REQUEST['length'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "`".$aColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]."` ".
						($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		//$sOrder = " order by ".$aColumns[$order['column']]." ".$order['dir']." ";
		$s0 = "select count(a.nip) as jml 
				from d_pegawai a
				where 1 = 1
				and a.non_aktif = 0
				";
		$s0 .= $sSearch;
		
		$sheet_total = $this->db->query($s0)->row();
		$sheet_total = $sheet_total->jml;
	
		$s1 = "SELECT a.nip,a.nama,
					if(b.status_helm = 0,'Lepas',if(b.status_helm = 1,'Terpakai','Tidak Konek')) as helm,
					if(c.status = 0,'Out',if(c.status = 1,'In','-')) as status_geofence,
					b.update_date
			FROM d_pegawai a
			left join d_status_helm b on(a.nip = b.nip)
			left join d_status_geofence c on(a.nip = c.nip)
			WHERE 1=1
			and a.non_aktif = 0";
		$s1 .= $sSearch;
		$s1 .= " order by nip asc ";		
		$s1 .= $sLimit;
	//	echo $s1;
		$sheet1 = $this->db->query($s1);
		$result = array();
		$i=0;
		$no=1;
		foreach($sheet1->result() as $sheet)
		{
			$time_now = time();
			$time_waktu = strtotime($sheet->update_date);
			if(($time_now - $time_waktu) > 10)
			{
				$status = 'Off';
			}
			else
			{
				$status = 'On';
			}
			$result[$i]['0'] = $no;
			$result[$i]['1'] = $sheet->nip.' </br> '.$sheet->nama;
			$result[$i]['2'] = $status;
			$result[$i]['3'] = $sheet->helm;
			$result[$i]['4'] = $sheet->status_geofence;
						
			$no++;
			$i++;
		}
		
		$data = $result;
		$results = array(
			"iTotalRecords" => ($sheet_total),
			"iTotalDisplayRecords" => ($sheet_total),
			"aaData"=>$data);
			
		echo json_encode($results);
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */