<?php $this->load->view('header');?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('top');?>
  <!-- Left side column. contains the logo and sidebar -->
  
  <?php $this->load->view('sidebar');?>
 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title;?>
        <small><?=$subtitle;?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-white">
					<div class="panel-body">
													
						<div class="row">
							<form name="formPegawai" id="formPegawai" method="post" action="/log/harian" class="form-horizontal">
							<input type="hidden" name="cari" value="1">
							
							<div class="row">
									<label  class="col-sm-2 control-label">Nama</label>
								<div class="col-xs-4">
									<input class="form-control" type="text" name="nama" id="nama" value="<?=$nama;?>">
								</div>
									<label  class="col-sm-2 control-label">NIP</label>
								<div class="col-xs-4">
									<input class="form-control" type="text" name="reqnip" id="reqnip" value="<?=$reqnip;?>">
								</div>
							</div>
							<div class="row">
									<label  class="col-sm-2 control-label">Tanggal</label>
								<div class="col-xs-4">
									<input class="form-control pull-right datepicker" id="tanggal" type="text" name="tanggal" value="<?=((isset($tanggal) && $tanggal!='')?$tanggal:date('d-m-Y'));?>">
								</div>
							</div>
							</br>
							</br>
							</br>
														
							</form>					
						</div>										
					</div>
				</div>
			</div>
			<div class="col-xs-1">
			</div>
		</div>
		<div class="row">
		
			<div class="col-xs-12">
				<div class="panel panel-success">
                    <div class="panel-heading">
                        Daftar Pegawai
                    </div>
					<div class="panel-body">
						<!--
						<button id="eksport_xls" type="button" class="btn btn-success">
						  Export XLSX
						</button>
						-->
						<button id="eksport_pdf" type="button" class="btn btn-success">
						  Export PDF
						</button>
						</br>
						</br>
					<hr>
                        <div class="table-responsive">
							<table class="table table-bordered" id="tabel">
							<thead>
							<tr>
								<th>No</th>
								<th>NIP / Nama</th>
								<th>Jabatan </th>
								<th>Jadwal Masuk</th>
								<th>Waktu Masuk</th>
								<th>Keterlambatan (Menit)</th>
								<th>Jadwal Pulang</th>
								<th>Waktu Pulang</th>
								<th>Pulang Cepat (Menit)</th>
							<!--	<th>Tidak Masuk</th>
								
								<th>Persentase Pemotongan (%)</th>
								
								<th>Keterangan</th>
								<th>Edit</th>
								-->
							</tr>
							</thead>
							<tbody>
							
							</tbody>
							</table>
						</div>
					
					</div>
				</div>
			</div>
		
		</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
 
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<form id="frm_ekspor_pdf" name="frm_ekspor_pdf" method="post" action="/log/ekspor_absen_harian_pns_pdf" target="_blank">
		<input type="hidden" name="nama_pdf" id="nama_pdf" value="<?=$nama;?>">
		<input type="hidden" name="reqnip_pdf" id="reqnip_pdf" value="<?=$reqnip;?>">
		<input type="hidden" name="tanggal_pdf" id="tanggal_pdf" value="<?=((isset($tanggal) && $tanggal!='')?$tanggal:date('d-m-Y'));?>">
</form>
<style>
tr.terlambat {
        font-weight: bold;
        color: pink;
		background-color: red;
    }
tr.edited{
	
	font-weight: bold;
        color: purple;
		background-color: red;
}
</style>
<?php $this->load->view('javascript');?>
<script src="/assets/js/jquery.chainedSelects.js"></script>
<script src="/assets/js/dataTables/jquery.dataTables.js"></script>
<script src="/assets/js/dataTables/dataTables.bootstrap.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
	
		 //Date picker
		$('.datepicker').datepicker({
		  autoclose: true,
		  format:'dd-mm-yyyy'
		})
		
		var oTable = $("#tabel").dataTable({
			"oLanguage" : {
				"sSearch" : "Search",
				"oPaginate" : {
				"sFirst":"First",
				"sPrevious" : "Previous",
				"sNext" : "Next",
				"sLast":"Last"
			}
			}, 
			"ordering": false,
			"iDisplayLength" : 10,
			"bFilter" : false,               
			"bLengthChange": true,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": "/log/get_harian",
				"type": "POST",
				"data": {
							'nama': function(){return $("#nama").val(); }, 
							'reqnip': function(){return $("#reqnip").val(); }, 
							'tanggal': function(){return $("#tanggal").val(); }, 
						}
			},
			"createdRow": function ( row, data, index ) {
				if ( (data[5] >= 1) ) 
				{
				   $(row).addClass('terlambat');
				}
				if ( (data[13] == 1) ) 
				{
				   $(row).addClass('edited');
				}
				
			}
		});  
		
		$('#nama,#reqnip,#tanggal').on( 'keyup', function () {
			oTable.fnDraw();
		} );
		
		$('#sssubunit,#jnsjab,#keselon,#tanggal').on( 'change', function () {
			oTable.fnDraw();
		} );
		
		$('#eksport_xls').click(function (e) {
				$('#frm_ekspor_xls').submit();
		});
		$('#eksport_pdf').click(function (e) {
				$('#frm_ekspor_pdf').submit();
		});

		$('#sssubunit').on( 'change', function () {
			frm_ekspor_xls.sssubunit_xls.value = $('#sssubunit').val();
			frm_ekspor_pdf.sssubunit_pdf.value = $('#sssubunit').val();
		} );
		$('#jnsjab').on( 'change', function () {
			frm_ekspor_xls.jnsjab_xls.value = $('#jnsjab').val();
			frm_ekspor_pdf.jnsjab_pdf.value = $('#jnsjab').val();
		} );
		$('#keselon').on( 'change', function () {
			frm_ekspor_xls.keselon_xls.value = $('#keselon').val();
			frm_ekspor_pdf.keselon_pdf.value = $('#keselon').val();
		} );
		$('#nama').on( 'change', function () {
			frm_ekspor_xls.nama_xls.value = $('#nama').val();
			frm_ekspor_pdf.nama_pdf.value = $('#nama').val();
		} );
		$('#reqnip').on( 'change', function () {
			frm_ekspor_xls.reqnip_xls.value = $('#reqnip').val();
			frm_ekspor_pdf.reqnip_pdf.value = $('#reqnip').val();
		} );
		$('#tanggal').on( 'change', function () {
			frm_ekspor_xls.tanggal_xls.value = $('#tanggal').val();
			frm_ekspor_pdf.tanggal_pdf.value = $('#tanggal').val();
		} );
		
		});
		</script>
</body>
</html>
