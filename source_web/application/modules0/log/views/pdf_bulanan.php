							<table>
								<tr>
									<td>NIP</td><td>:</td><td><?=$nip;?></td>
								</tr>
								<tr>
									<td>Nama</td><td>:</td><td><?=$data['nama'];?></td>
								</tr>
								<tr>
									<td>Jabatan</td><td>:</td><td><?=$data['nama_jabatan'];?></td>
								</tr>
								<tr>
									<td>Periode</td><td>:</td><td><?=$nama_bulan[$bulan];?>-<?=$tahun;?></td>
								</tr>
							</table>
							<table border="1" cellpadding="2" cellspacing="0" style="border:1px #000000 solid; font-family:Arial, Helvetica, sans-serif; font-size:11px;">
							<thead>
							<tr>
								<th>No</th>
								<th>Tanggal</th>
								<th>Jadwal Masuk</th>
								<th>Waktu Masuk</th>
								<th>Keterlambatan (Menit)</th>
								<th>Jadwal Pulang</th>
								<th>Waktu Pulang</th>
								<th>Pulang Cepat (Menit)</th>
								<th>Tidak Masuk</th>
								<th>Persentase Pemotongan (%)</th>
							</tr>
							</thead>
							<tbody>
							<?php
							$i= 1;
							$total_pemotongan = 0;
							foreach($rows->result_array() as $row)
							{
								if($row['libur'] == 1)
								{
									$ket='style="background:red"';
								}
								else
								{
									$ket = '';
								}
								
								echo '<tr '.$ket.'>';
								echo '<td>'.$i.'</td>';
								echo '<td>'.date('d-M-Y',strtotime($row['tahun'].'-'.$row['bulan'].'-'.$row['tanggal'])).'</td>';
								echo '<td style="text-align:right">'.((isset($row['jadwal_masuk']) && $row['jadwal_masuk'] != NULL && $row['jadwal_masuk'] != '' && $row['jadwal_masuk'] != '0000-00-00')?date('d-M-Y H:i:s',strtotime($row['jadwal_masuk'])):'-').'</td>';
								echo '<td style="text-align:right">'.((isset($row['realisasi_masuk']) && $row['realisasi_masuk'] != NULL && $row['realisasi_masuk'] != '' && $row['realisasi_masuk'] != '0000-00-00')?date('d-M-Y H:i:s',strtotime($row['realisasi_masuk'])):'-').'</td>';
								echo '<td style="text-align:right">'.$row['keterlambatan_masuk'].'</td>';
								echo '<td style="text-align:right">'.((isset($row['jadwal_keluar']) && $row['jadwal_keluar'] != NULL && $row['jadwal_keluar'] != '' && $row['jadwal_keluar'] != '0000-00-00')?date('d-M-Y H:i:s',strtotime($row['jadwal_keluar'])):'-').'</td>';
								echo '<td style="text-align:right">'.((isset($row['realisasi_keluar']) && $row['realisasi_keluar'] != NULL && $row['realisasi_keluar'] != '' && $row['realisasi_keluar'] != '0000-00-00')?date('d-M-Y H:i:s',strtotime($row['realisasi_keluar'])):'-').'</td>';
								echo '<td style="text-align:right">'.$row['kecepatan_keluar'].'</td>';
								echo '<td>'.$row['ketidakhadiran'].'</td>';
								echo '<td style="text-align:right">'.$row['persentase_potongan'].'</td>';
								echo '</tr>';
								$i++;
								$total_pemotongan = $total_pemotongan + $row['persentase_potongan'];
							}
							?>
							<tr>
								<td colspan="9"  style="text-align:right">Total</td>
								<td  style="text-align:right"><?=$total_pemotongan;?></td>
							</tr>
							</tbody>
							</table>