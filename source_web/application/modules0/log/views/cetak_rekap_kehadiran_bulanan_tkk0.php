<html>
<head>
</head>
<body>
<h3>Data Rekapitulasi Kehadiran Bulanan Pegawai TKK di <?=$unit_kerja;?>, pada bulan <?=get_bulan($bulan);?> Tahun <?=$tahun;?></h3>
<table border="1" cellpadding="2" cellspacing="0" style="border:1px #000000 solid; font-family:Arial, Helvetica, sans-serif; font-size:11px;">
	<tr><td>No</td>
		<td>NIK / Nama</td>
		<td>SKPD</td>
		<td>Jumlah Keterlambatan (Menit)</td>
		<td>Jumlah Pulang Cepat (Menit)</td>
		<td>Jumlah Tidak Hadir (Hari)</td>
		<td>Jumlah Potongan (%)</td>
	</tr>
	<?php
	$no=1;
	foreach($q->result() as $sheet)
	{
		echo '<tr>';
		echo '<td>'.$no.'</td>';
		echo '<td>'.$sheet->nik.'<br>'.$sheet->nama.'</td>';
		echo '<td>'.$sheet->skpd.'</td>';
		echo '<td style="text-align:right">'.number_format($sheet->jumlah_telat_masuk,1,',','.').'</td>';
		echo '<td style="text-align:right">'.number_format($sheet->jumlah_pulang_cepat,1,',','.').'</td>';
		echo '<td style="text-align:right">'.number_format($sheet->jumlah_tidak_masuk,1,',','.').'</td>';
		echo '<td style="text-align:right">'.number_format($sheet->jumlah_potongan,1,',','.').'</td>';
		echo '</tr>';
		$no++;
	}
	?>
</table>
</body>
</html> 
<?php
function get_bulan($bulan)
{
	switch($bulan)
	{
		case 1:
			$res = 'Januari';
		break;
		case 2:
			$res = 'Februari';
		break;
		case 3:
			$res = 'Maret';
		break;
		case 4:
			$res = 'April';
		break;
		case 5:
			$res = 'Mei';
		break;
		case 6:
			$res = 'Juni';
		break;
		case 7:
			$res = 'Juli';
		break;
		case 8:
			$res = 'Agustus';
		break;
		case 9:
			$res = 'September';
		break;
		case 10:
			$res = 'Oktober';
		break;
		case 11:
			$res = 'November';
		break;
		case 12:
			$res = 'Desember';
		break;
	}
	return $res;
}
?>