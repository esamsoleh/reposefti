<?php $this->load->view('header');?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('top');?>
  <!-- Left side column. contains the logo and sidebar -->
  
  <?php $this->load->view('sidebar');?>
 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title;?>
        <small><?=$subtitle;?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="panel panel-white">
									<div class="panel-body">
									<?php echo validation_errors(); ?>
									<?=$this->session->userdata('message');?>
												<form name="frm" id="frm" method="post" action="/<?=$this->uri->uri_string();?>" enctype="multipart/form-data">													
												<div class="row">
														<label class="col-sm-2 control-label">Unit Kerja</label>
													<div class="col-xs-10">
														<select class="form-control" name="unit" id="unit">
														<?php
														$s_biro = $this->session->userdata('s_biro');
														//echo 'ssssssssssssssssbiro: '.$s_biro.'</br>';
														if (!empty($s_biro) && $s_biro != '100000000000') 
														{
															$sql = "SELECT * 
																	FROM referensi_unit_kerja 
																	WHERE (IF(MID('$s_biro',3,2)=10 || MID('$s_biro',3,2)=11,
																		mid(kunker,1,6) = mid('$s_biro',1,6)  ,
																		mid(kunker,1,4) = mid('$s_biro',1,4)  )
																	)
																	AND (IF(MID('$s_biro',3,2)=10 || MID('$s_biro',3,2)=11,
																		mid(kunker,7,6) = '000000' ,
																		mid(kunker,5,8) = '00000000' )
																	)												
																	ORDER BY KUNKER";
															//echo $sql.'</br>';
															$query = $this->db->query($sql);
															foreach ($query->result_array() as $row)
															{ 
																echo '<option value ="'.$row['KUNKER'].'" '.((isset($unit) && $unit == $row['KUNKER'])?"selected":"").'>'.($row['NUNKER']).'</option>';
															}
															
														} 											
														else 
														{
															$sql = "SELECT * FROM referensi_unit_kerja 
																	WHERE (IF(MID(kunker,3,2)=10 || MID(kunker,3,2)=11,
																		mid(kunker,7,6) = '000000' ,
																		mid(kunker,5,8) = '00000000' )
																	)
																	
																	ORDER BY NUNKER";
															$query = $this->db->query($sql);
															echo '<option value="100000000000" '.((isset($unit) && ($unit == '100000000000'))?"selected":"").'>Semua</option>';			
															foreach ($query->result_array() as $row)
															{ 
																echo '<option value ="'.$row['KUNKER'].'" '.((isset($unit) && $unit == $row['KUNKER'])?"selected":"").'>'.($row['NUNKER']).'</option>';
															} 								
														}
														?>								
														</select>
													</div>
												</div>
												<div class="row">
														<label class="col-sm-2 control-label">Sub Unit Kerja</label>
													<div class="col-xs-10">
														<select class="form-control" name="subunit" id="subunit">
														<?php 
														if (!empty($s_biro) && $s_biro != '100000000000') 
														{
															//$subunit = $s_biro;
															$rincunit1 = substr($s_biro,6,2);
															if(isset($subunit) && $subunit != NULL && $subunit != '')
															{
																if($rincunit1 != '00')
																{
																	$sql = "SELECT * 
																		FROM referensi_unit_kerja 
																		WHERE (IF(MID('$subunit',3,2)=10 || MID('$subunit',3,2)=11,
																			mid(kunker,1,8) = mid('$subunit',1,8),
																			mid(kunker,1,6) = mid('$subunit',1,6)  )
																		)
																		AND (IF(MID('$subunit',3,2)=10 || MID('$subunit',3,2)=11,
																			mid(kunker,9,4) = '0000' ,
																			mid(kunker,7,6) = '000000' )
																		)
																		ORDER BY KUNKER";
																}
																else
																{
																	$sql = "SELECT * 
																		FROM referensi_unit_kerja 
																		WHERE (IF(MID('$subunit',3,2)=10 || MID('$subunit',3,2)=11,
																			mid(kunker,1,6) = mid('$subunit',1,6),
																			mid(kunker,1,4) = mid('$subunit',1,4)  )
																		)
																		AND (IF(MID('$subunit',3,2)=10 || MID('$subunit',3,2)=11,
																			mid(kunker,9,4) = '0000' ,
																			mid(kunker,7,6) = '000000' )
																		)
																		ORDER BY KUNKER";
																}
																
																$query = $this->db->query($sql);
																echo $sql;
																foreach ($query->result_array() as $row)
																{ 
																	echo '<option value ="'.$row['KUNKER'].'" '.((isset($subunit) && $subunit == $row['KUNKER'])?"selected":"").'>'.($row['NUNKER']).'</option>';
																}
															}
															else
															{
																if($rincunit1 != '00')
																{
																	$sql = "SELECT * 
																		FROM referensi_unit_kerja 
																		WHERE (IF(MID('$s_biro',3,2)=10 || MID('$s_biro',3,2)=11,
																			mid(kunker,1,8) = mid('$s_biro',1,8),
																			mid(kunker,1,6) = mid('$s_biro',1,6)  )
																		)
																		AND (IF(MID('$s_biro',3,2)=10 || MID('$s_biro',3,2)=11,
																			mid(kunker,9,4) = '0000' ,
																			mid(kunker,7,6) = '000000' )
																		)
																		ORDER BY KUNKER";
																}
																else
																{
																	$sql = "SELECT * 
																		FROM referensi_unit_kerja 
																		WHERE (IF(MID('$s_biro',3,2)=10 || MID('$s_biro',3,2)=11,
																			mid(kunker,1,6) = mid('$s_biro',1,6),
																			mid(kunker,1,4) = mid('$s_biro',1,4)  )
																		)
																		AND (IF(MID('$s_biro',3,2)=10 || MID('$s_biro',3,2)=11,
																			mid(kunker,9,4) = '0000' ,
																			mid(kunker,7,6) = '000000' )
																		)
																		ORDER BY KUNKER";
																}
																
																		//echo $sql;
																$query = $this->db->query($sql);
																foreach ($query->result_array() as $row)
																{ 
																	echo '<option value ="'.$row['KUNKER'].'" '.((isset($subunit) && $subunit == $row['KUNKER'])?"selected":"").'>'.($row['NUNKER']).'</option>';
																}
															}
														}
														else
														{
															//$subunit = $s_biro;
															if(isset($subunit) && $subunit != NULL)
															{
																			$sql = "SELECT * 
																				FROM referensi_unit_kerja 
																				WHERE MID(KUNKER,1,4) = '".substr($subunit,0,4)."' 										
																				AND MID(KUNKER,7,6) = '000000'
																				
																				ORDER BY KUNKER";
																$sql = "SELECT * 
																	FROM referensi_unit_kerja 
																	WHERE (IF(MID('$subunit',3,2)=10 || MID('$subunit',3,2)=11,
																		mid(kunker,1,6) = mid('$subunit',1,6)  ,
																		mid(kunker,1,4) = mid('$subunit',1,4)  )
																	)
																	AND (IF(MID('$subunit',3,2)=10 || MID('$subunit',3,2)=11,
																		mid(kunker,9,4) = '0000' ,
																		mid(kunker,7,6) = '000000' )
																	)
																	ORDER BY KUNKER";
																			//echo $sql;
																			$query = $this->db->query($sql);
																			if($query->num_rows() > 0)
																			{
																				foreach ($query->result_array() as $row)
																				{ 
																					echo '<option value ="'.$row['KUNKER'].'" '.((isset($subunit) && $subunit == $row['KUNKER'])?"selected":"").'>'.($row['NUNKER']).'</option>';
																				}
																			}
																			else
																			{
																				echo '<option value="100000000000" '.((isset($subunit) && ($subunit == '100000000000'))?"selected":"").'>Semua</option>';			
																			}
															}
															else
															{
																echo '<option value="100000000000">Semua</option>';
															}
														}
														?>	
														</select>
													</div>
												</div>
												<div class="row">
														<label  class="col-sm-2 control-label">Sub Sub Unit Kerja</label>
													<div class="col-xs-10">
														<select class="form-control" name="ssubunit" id="ssubunit">
															<?php 
														if (!empty($s_biro) && $s_biro != '100000000000') 
														{
														//	$ssubunit = $s_biro;
															$rincunit2 = substr($s_biro,8,2);
															if(isset($ssubunit) && $ssubunit != NULL && $ssubunit != '')
															{
																if($rincunit1 != '00')
																{
																	$sql = "SELECT * 
																	FROM referensi_unit_kerja 
																	WHERE (IF(MID('$ssubunit',3,2)=10 || MID('$ssubunit',3,2)=11,
																		mid(kunker,1,10) = mid('$ssubunit',1,10)  ,
																		mid(kunker,1,8) = mid('$ssubunit',1,8)  )
																	)
																	AND (IF(MID('$ssubunit',3,2)=10 || MID('$ssubunit',3,2)=11,
																		mid(kunker,11,2) = '00' ,
																		mid(kunker,9,4) = '0000' )
																	)												
																	ORDER BY NUNKER";
																}
																else
																{
																	$sql = "SELECT * 
																	FROM referensi_unit_kerja 
																	WHERE (IF(MID('$ssubunit',3,2)=10 || MID('$ssubunit',3,2)=11,
																		mid(kunker,1,8) = mid('$ssubunit',1,8)  ,
																		mid(kunker,1,6) = mid('$ssubunit',1,6)  )
																	)
																	AND (IF(MID('$ssubunit',3,2)=10 || MID('$ssubunit',3,2)=11,
																		mid(kunker,11,2) = '00' ,
																		mid(kunker,9,4) = '0000' )
																	)												
																	ORDER BY NUNKER";
																}
																
																
																$query = $this->db->query($sql);
																foreach ($query->result_array() as $row)
																{ 
																	echo '<option value ="'.$row['KUNKER'].'" '.((isset($ssubunit) && $ssubunit == $row['KUNKER'])?"selected":"").'>'.($row['NUNKER']).'</option>';
																}
															}
															else
															{
																if($rincunit1 != '00')
																{
																	$sql = "SELECT * 
																	FROM referensi_unit_kerja 
																	WHERE (IF(MID('$s_biro',3,2)=10 || MID('$s_biro',3,2)=11,
																		mid(kunker,1,10) = mid('$s_biro',1,10)  ,
																		mid(kunker,1,8) = mid('$s_biro',1,8)  )
																	)
																	AND (IF(MID('$s_biro',3,2)=10 || MID('$s_biro',3,2)=11,
																		mid(kunker,11,2) = '00' ,
																		mid(kunker,9,4) = '0000'  )
																	)
																	
																	ORDER BY NUNKER";
																}
																else
																{
																	$sql = "SELECT * 
																	FROM referensi_unit_kerja 
																	WHERE (IF(MID('$s_biro',3,2)=10 || MID('$s_biro',3,2)=11,
																		mid(kunker,1,8) = mid('$s_biro',1,8)  ,
																		mid(kunker,1,6) = mid('$s_biro',1,6)  )
																	)
																	AND (IF(MID('$s_biro',3,2)=10 || MID('$s_biro',3,2)=11,
																		mid(kunker,11,2) = '00' ,
																		mid(kunker,9,4) = '0000'  )
																	)
																	
																	ORDER BY NUNKER";
																}
																
																		//echo $sql;
																$query = $this->db->query($sql);
																foreach ($query->result_array() as $row)
																{ 
																	echo '<option value ="'.$row['KUNKER'].'" '.((isset($ssubunit) && $ssubunit == $row['KUNKER'])?"selected":"").'>'.($row['NUNKER']).'</option>';
																}
															}
															
														}
														else
														{
															if(isset($ssubunit) && $ssubunit != NULL)
																		{
																			$sql = "SELECT * 
																				FROM referensi_unit_kerja 
																				WHERE (IF(MID('$ssubunit',3,2)=10 || MID('$ssubunit',3,2)=11,
																					mid(kunker,1,8) = mid('$ssubunit',1,8)  ,
																					mid(kunker,1,6) = mid('$ssubunit',1,6)  )
																				)
																				AND (IF(MID('$ssubunit',3,2)=10 || MID('$ssubunit',3,2)=11,
																					mid(kunker,11,2) = '00' ,
																					mid(kunker,9,4) = '0000' )
																				)
																				
																				ORDER BY NUNKER";
																			//echo $sql;
																			$query = $this->db->query($sql);
																			if($query->num_rows() > 0)
																			{
																				foreach ($query->result_array() as $row)
																				{ 
																					echo '<option value ="'.$row['KUNKER'].'" '.((isset($ssubunit) && $ssubunit == $row['KUNKER'])?"selected":"").'>'.($row['NUNKER']).'</option>';
																				}
																			}
																			else
																			{
																				echo '<option value="100000000000" '.((isset($ssubunit) && ($ssubunit == '100000000000'))?"selected":"").'>Semua</option>';
																			}
																		}
																		else
																		{
																			echo '<option value="100000000000">Semua</option>';
																		}
														}
														?>	
														</select>
													</div>
												</div>
												<div class="row">
														<label class="col-sm-2 control-label">Sub Sub Sub Unit Kerja</label>
													<div class="col-xs-10">
														<select class="form-control" name="sssubunit" id="sssubunit">
														<?php 
														if (!empty($s_biro) && $s_biro != '100000000000') 
														{
														//	$sssubunit = $s_biro;
															$rincunit3 = substr($s_biro,10,2);
															if(isset($sssubunit) && $sssubunit != NULL)
															{
																if($rincunit2 != '00')
																{
																	$sql = "SELECT * 
																	FROM referensi_unit_kerja 
																	WHERE (IF(MID('$sssubunit',3,2)=10 || MID('$sssubunit',3,2)=11,
																		mid(kunker,1,12) = mid('$sssubunit',1,12)  ,
																		mid(kunker,1,10) = mid('$sssubunit',1,10)  )
																	)
																	AND (IF(MID('$sssubunit',3,2)=10 || MID('$sssubunit',3,2)=11,
																		1=1,
																		mid(kunker,11,2) = '00' )
																	)
																	
																	ORDER BY NUNKER";
																}
																else
																{
																	$sql = "SELECT * 
																	FROM referensi_unit_kerja 
																	WHERE (IF(MID('$sssubunit',3,2)=10 || MID('$sssubunit',3,2)=11,
																		mid(kunker,1,10) = mid('$sssubunit',1,10)  ,
																		mid(kunker,1,8) = mid('$sssubunit',1,8)  )
																	)
																	AND (IF(MID('$sssubunit',3,2)=10 || MID('$sssubunit',3,2)=11,
																		1=1,
																		mid(kunker,11,2) = '00' )
																	)
																	
																	ORDER BY NUNKER";
																}
																
																echo $sql;
																$query = $this->db->query($sql);
																foreach ($query->result_array() as $row)
																{ 
																	echo '<option value ="'.$row['KUNKER'].'" '.((isset($sssubunit) && $sssubunit == $row['KUNKER'])?"selected":"").'>'.($row['NUNKER']).'</option>';
																}
															}
															else
															{
																if($rincunit1 != '00')
																{
																	$sql = "SELECT * 
																	FROM referensi_unit_kerja 
																	WHERE (IF(MID('$s_biro',3,2)=10 || MID('$s_biro',3,2)=11,
																		mid(kunker,1,6) = mid('$s_biro',1,6)  ,
																		mid(kunker,1,4) = mid('$s_biro',1,4)  )
																	)
																	AND (IF(MID('$s_biro',3,2)=10 || MID('$s_biro',3,2)=11,
																		1=1,
																		mid(kunker,11,2) = '00'  )
																	)
																	
																	ORDER BY NUNKER";
																}
																else
																{
																	$sql = "SELECT * 
																	FROM referensi_unit_kerja 
																	WHERE (IF(MID('$s_biro',3,2)=10 || MID('$s_biro',3,2)=11,
																		mid(kunker,1,10) = mid('$s_biro',1,10)  ,
																		mid(kunker,1,8) = mid('$s_biro',1,8)  )
																	)
																	AND (IF(MID('$s_biro',3,2)=10 || MID('$s_biro',3,2)=11,
																		1=1,
																		mid(kunker,11,2) = '00'  )
																	)
																	
																	ORDER BY NUNKER";
																}
																
																//		echo $sql;
																$query = $this->db->query($sql);
																foreach ($query->result_array() as $row)
																{ 
																	echo '<option value ="'.$row['KUNKER'].'" '.((isset($ssubunit) && $ssubunit == $row['KUNKER'])?"selected":"").'>'.($row['NUNKER']).'</option>';
																}
															}
															
														}
														else
														{
															if(isset($sssubunit) && $sssubunit != NULL)
																		{
																			$sql = "SELECT * 
																	FROM referensi_unit_kerja 
																	WHERE (IF(MID('$sssubunit',3,2)=10 || MID('$sssubunit',3,2)=11,
																		mid(kunker,1,10) = mid('$sssubunit',1,10)  ,
																		mid(kunker,1,8) = mid('$sssubunit',1,8)  )
																	)
																	AND (IF(MID('$sssubunit',3,2)=10 || MID('$sssubunit',3,2)=11,
																		1=1,
																		mid(kunker,11,2) = '00' )
																	)
																	
																	ORDER BY NUNKER";
																			$query = $this->db->query($sql);
																			if($query->num_rows() > 0)
																			{
																				foreach ($query->result_array() as $row)
																				{ 
																					echo '<option value ="'.$row['KUNKER'].'" '.((isset($sssubunit) && $sssubunit == $row['KUNKER'])?"selected":"").'>'.($row['NUNKER']).'</option>';
																				}
																			}
																			else
																			{
																				echo '<option value="100000000000" '.((isset($sssubunit) && ($sssubunit == '100000000000'))?"selected":"").'>Semua</option>';
																			}
																		}
																		else
																		{
																			echo '<option value="100000000000">Semua</option>';
																		}
														}
														?>	
														</select>
													</div>
												</div>
												<div class="row">
														<label class="col-sm-2 control-label">Mesin - Unit Kerja</label>
													<div class="col-xs-10">
														<select class="form-control" name="id_mesin" id="id_mesin">
															<?php
															$sql = "SELECT a.id,
																		a.serial_number,
																		(select nunker from referensi_unit_kerja where kunker = a.lokasi ) as lokasi_mesin
																	FROM mesin_absensi a
																	where 1=1
																	and a.lokasi = '$sssubunit'
																	ORDER BY id";
															//echo $sql;
															$query = $this->db->query($sql);
															foreach($query->result_array() as $row)
															{
																echo '<option value ="'.$row['id'].'" '.((isset($id_mesin) && $id_mesin == $row['id'])?"selected":"").'>'.($row['serial_number'].'-'.$row['lokasi_mesin']).'</option>';
															}
															?>
														</select>
													</div>
												</div>
												<div class="row">
														<div class="col-xs-2"><label>File Log</label></div>
														<div class="col-xs-10">
															<input type="file" name="userfile" id="userfile" class="form-control">
														</div>
													</div>
													<div class="row">
														<div class="col-xs-2"><label>Keterangan</label></div>
														<div class="col-xs-10">
															 <?php echo form_textarea($keterangan);?>
														</div>
													</div>
													
													
													</br>
													<div class="row">
														<div class="col-xs-6"><button id="simpan_form" class="btn btn-block btn-success" type="button">Simpan</button></div>
														<div class="col-xs-6"><button id="batal" class="btn btn-block btn-success" type="button">Batal</button></div>
													</div>
													</form>
									</div>
				</div>
					
			</div>
		</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
 
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('javascript');?>
<script src="/assets/js/jquery.chainedSelects.js"></script>
<script src="/assets/js/dataTables/jquery.dataTables.js"></script>
<script src="/assets/js/dataTables/dataTables.bootstrap.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
			var jml_row = 0;
			required = ["userfile","id_mesin"];
			errornotice = $("#error");
			emptyerror = "Mohon field ini diisi.";
			$('#simpan_form').click(function(){
				//Validate required fields
				for (i=0;i<required.length;i++) {
					var input = $('#'+required[i]);
					if ((input.val() == "") || (input.val() == emptyerror)) {
						input.addClass("needsfilled");
						input.val(emptyerror);
						errornotice.fadeIn(750);
					} else {
						input.removeClass("needsfilled");
					}
				}
					
				//if any inputs on the page have the class 'needsfilled' the form will not submit
				if ($(":input").hasClass("needsfilled")) {
					return false;
				} else {
					errornotice.hide();
					$('#frm').submit();
				}
												  
			});
											
			$(":input").focus(function(){		
			   if ($(this).hasClass("needsfilled") ) {
					$(this).val("");
					$(this).removeClass("needsfilled");
				}
			});
			
			$('#batal').click(function(){
				parent.history.back();
				return false;
			});
			
			 //Date picker
			$('.datepicker').datepicker({
			  autoclose: true,
			  format:'dd-mm-yyyy'
			});
			
			$( "#nip" ).select2({   
				placeholder: "Ketik NIP/NAMA",
				allowClear: false,
				ajax: {
					url: "/log/cari_nip",
					dataType: 'json',
					delay: 250,
					data: function (params) {
						return {
							q: params.term // search term
						};
					},
					processResults: function (data) {
						return {
							//results: data
							 results: $.map(data, function (item) {
							return {
								text: item.NIPNAMA,
								id: item.NIP
							}
							 })
						};
					},
					cache: true
				},
				minimumInputLength: 2
			});
		
			$("#nip").keyup(function(){
				var that = this,
				value = $(this).val();

				$.post('/log/view_identitas',
				{
					'NIP': frm.nip.value,
				},
				function(data) {
					$('#data_pegawai').html(data);
				}
						
				);
			}); 
			
			$("#nip").change(function(){
				var that = this,
				value = $(this).val();

				$.post('/log/view_identitas',
				{
					'NIP': frm.nip.value,
				},
				function(data) {
					$('#data_pegawai').html(data);
				}
						
				);
			}); 
			
			$("#add_field_button").click(function(e){ //on add input button click
				e.preventDefault();				
				jml_row++;
				$("#input_syarat").append('<div class="row">'+
											'<div class="col-xs-6">'+
											'<input class="form-control input_'+jml_row+'" id="input_'+jml_row+'" type="file" name="input_'+jml_row+'" >'+
											'</div>'+
											'<div class="col-xs-3"><button class="remove_field btn">Hapus</button></div>'+
											'</div>'); 		
				frm.jml_file.value = jml_row;
													
			});
			
			$("#input_syarat").on("click",".remove_field", function(e){ //user click on remove text
					e.preventDefault(); 
					$(this).parent('div').parent('div').remove(); 
					jml_row--;
					frm.jml_file.value = jml_row; 
			});
			
			$('#unit').chainSelect('#subunit','/log/combo_unit',
		{ 
			before:function (target) //before request hide the target combobox and display the loading message
			{ 
				$("#loading").css("display","block");
				$(target).css("display","none");
			},
			after:function (target) //after request show the target combobox and hide the loading message
			{ 
				$("#loading").css("display","none");
				$(target).css("display","inline");
			}
		});
		$('#subunit').chainSelect('#ssubunit','/log/combo_unit',
		{ 
			before:function (target) //before request hide the target combobox and display the loading message
			{ 
				$("#loading").css("display","block");
				$(target).css("display","none");
			},
			after:function (target) //after request show the target combobox and hide the loading message
			{ 
				$("#loading").css("display","none");
				$(target).css("display","inline");
			}
		});
		$('#ssubunit').chainSelect('#sssubunit','/log/combo_unit',
		{ 
			before:function (target) //before request hide the target combobox and display the loading message
			{ 
				$("#loading").css("display","block");
				$(target).css("display","none");
			},
			after:function (target) //after request show the target combobox and hide the loading message
			{ 
				$("#loading").css("display","none");
				$(target).css("display","inline");
			}
		});
		$('#sssubunit').chainSelect('#id_mesin','/log/combo_unit',
		{ 
			before:function (target) //before request hide the target combobox and display the loading message
			{ 
				$("#loading").css("display","block");
				$(target).css("display","none");
			},
			after:function (target) //after request show the target combobox and hide the loading message
			{ 
				$("#loading").css("display","none");
				$(target).css("display","inline");
			}
		});
		
			
});
		</script>
</body>
</html>
