<?php $this->load->view('header');?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('top');?>
  <!-- Left side column. contains the logo and sidebar -->
  
  <?php $this->load->view('sidebar');?>
 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title;?>
        <small><?=$subtitle;?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="panel panel-white">
									<div class="panel-body">
									<?php echo validation_errors(); ?>

												<form name="frm" id="frm" method="post" action="/<?=$this->uri->uri_string();?>" enctype="multipart/form-data">													
													<div class="row">
														<div class="col-xs-4"><label>NIK</label></div>
														<div class="col-xs-8">
															<select id="nik" class="form-control" name="nik" id="nik">
																<option value="">&nbsp;</option>
															</select>
														</div>
													</div>
													<div id="data_pegawai">
														<div class="row">
															<div class="col-xs-4"><label>Nama</label></div>
															<div class="col-xs-8">														
															</div>
														</div>
														<div class="row">
															<div class="col-xs-4"><label>SKPD</label></div>
															<div class="col-xs-8">
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-4"><label>Jenis Ketidakhadiran</label></div>
														<div class="col-xs-8">
														<?php
															echo form_dropdown($jenis_ketidakhadiran,$options_jenis_ketidakhadiran,$selected_ketidakhadiran);
														?>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-4"><label>Tanggal Mulai</label></div>
														<div class="col-xs-8">
															<?php echo form_input($tanggal_mulai);?>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-4"><label>Tanggal Selesai</label></div>
														<div class="col-xs-8">
															<?php echo form_input($tanggal_selesai);?>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-4"><label>Keterangan</label></div>
														<div class="col-xs-8">
															 <?php echo form_textarea($keterangan);?>
														</div>
													</div>
													
													<div class="row">
														<div class="col-xs-4"><label>Bukti (JPEG/PNG)</label></div>
														<div class="col-xs-8">
															<button id="add_field_button" class="btn">Tambah Bukti</button>
															<div id="input_syarat">
															
															</div>
														</div>
													</div>
													<input type="hidden" id="jml_file" name="jml_file" value="0">
													</br>
													<div class="row">
														<div class="col-xs-6"><button id="simpan_form" class="btn btn-block btn-success" type="button">Simpan</button></div>
														<div class="col-xs-6"><button id="batal" class="btn btn-block btn-success" type="button">Batal</button></div>
													</div>
													</form>
									</div>
				</div>
					
			</div>
		</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
 
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('javascript');?>
<script src="/assets/js/jquery.chainedSelects.js"></script>
<script src="/assets/js/dataTables/jquery.dataTables.js"></script>
<script src="/assets/js/dataTables/dataTables.bootstrap.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
			var jml_row = 0;
		required = ["fid","point_x","point_y"];
			errornotice = $("#error");
			emptyerror = "Mohon field ini diisi.";
			$('#simpan_form').click(function(){
				//Validate required fields
				for (i=0;i<required.length;i++) {
					var input = $('#'+required[i]);
					if ((input.val() == "") || (input.val() == emptyerror)) {
						input.addClass("needsfilled");
						input.val(emptyerror);
						errornotice.fadeIn(750);
					} else {
						input.removeClass("needsfilled");
					}
				}
					
				//if any inputs on the page have the class 'needsfilled' the form will not submit
				if ($(":input").hasClass("needsfilled")) {
					return false;
				} else {
					errornotice.hide();
					$('#frm').submit();
				}
												  
			});
											
			$(":input").focus(function(){		
			   if ($(this).hasClass("needsfilled") ) {
					$(this).val("");
					$(this).removeClass("needsfilled");
				}
			});
			
			$('#batal').click(function(){
				parent.history.back();
				return false;
			});
			
			 //Date picker
			$('.datepicker').datepicker({
			  autoclose: true,
			  format:'dd-mm-yyyy'
			});
			
			$( "#nik" ).select2({   
				placeholder: "Ketik NIK/NAMA",
				allowClear: false,
				ajax: {
					url: "/log/cari_nik",
					dataType: 'json',
					delay: 250,
					data: function (params) {
						return {
							q: params.term // search term
						};
					},
					processResults: function (data) {
						return {
							//results: data
							 results: $.map(data, function (item) {
							return {
								text: item.niknama,
								id: item.nik
							}
							 })
						};
					},
					cache: true
				},
				minimumInputLength: 2
			});
		
			$("#nik").keyup(function(){
				var that = this,
				value = $(this).val();

				$.post('/log/view_identitas_tkk',
				{
					'nik': frm.nik.value,
				},
				function(data) {
					$('#data_pegawai').html(data);
				}
						
				);
			}); 
			
			$("#nik").change(function(){
				var that = this,
				value = $(this).val();

				$.post('/log/view_identitas_tkk',
				{
					'nik': frm.nik.value,
				},
				function(data) {
					$('#data_pegawai').html(data);
				}
						
				);
			}); 
			
			$("#add_field_button").click(function(e){ //on add input button click
				e.preventDefault();				
				jml_row++;
				$("#input_syarat").append('<div class="row">'+
											'<div class="col-xs-6">'+
											'<input class="form-control input_'+jml_row+'" id="input_'+jml_row+'" type="file" name="input_'+jml_row+'" >'+
											'</div>'+
											'<div class="col-xs-3"><button class="remove_field btn">Hapus</button></div>'+
											'</div>'); 		
				frm.jml_file.value = jml_row;
													
			});
			
			$("#input_syarat").on("click",".remove_field", function(e){ //user click on remove text
					e.preventDefault(); 
					$(this).parent('div').parent('div').remove(); 
					jml_row--;
					frm.jml_file.value = jml_row; 
			});
	
});
		</script>
</body>
</html>
