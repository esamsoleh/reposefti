<?php $this->load->view('header');?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('top');?>
  <!-- Left side column. contains the logo and sidebar -->
  
  <?php $this->load->view('sidebar');?>
 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title;?>
        <small><?=$subtitle;?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="panel panel-white">
									<div class="panel-body">
									<?php echo validation_errors(); ?>

												<form name="frm" id="frm" method="post" action="/<?=$this->uri->uri_string();?>" enctype="multipart/form-data">													
													<div class="row">
														<div class="col-xs-4"><label>NIP</label></div>
														<div class="col-xs-8">
															<?=$r->nip;?>
														</div>
													</div>
													<div id="data_pegawai">
														<div class="row">
															<div class="col-xs-4"><label>Nama</label></div>
															<div class="col-xs-8">	
																	<?=$r->nama_lengkap;?>
															</div>
														</div>
														<div class="row">
															<div class="col-xs-4"><label>Golongan</label></div>
															<div class="col-xs-8">
																<?=$r->golongan;?>
															</div>
														</div>
														<div class="row">
															<div class="col-xs-4"><label>Jabatan</label></div>
															<div class="col-xs-8">
																<?=$r->jabatan;?>
															</div>
														</div>
														<div class="row">
															<div class="col-xs-4"><label>SKPD</label></div>
															<div class="col-xs-8">
																<?=$r->skpd;?>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-4"><label>Jadwal Masuk</label></div>
														<div class="col-xs-8">
														<?php
															//echo date('d-m-Y H:i',strtotime($r->jadwal_masuk));
														?>
														<?php echo form_input($jadwal_masuk);?>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-4"><label>Realisasi Masuk</label></div>
														<div class="col-xs-8">
															<?php echo form_input($realisasi_masuk);?>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-4"><label>Jadwal Keluar</label></div>
														<div class="col-xs-8">
															<?php //echo date('d-m-Y H:i',strtotime($r->jadwal_keluar));?>
															<?php echo form_input($jadwal_keluar);?>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-4"><label>Realisasi Keluar</label></div>
														<div class="col-xs-8">
															 <?php echo form_input($realisasi_keluar);?>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-4"><label>Keterangan</label></div>
														<div class="col-xs-8">
															 <?php echo form_textarea($keterangan);?>
														</div>
													</div>
													</br>
													<div class="row">
														<div class="col-xs-6"><button id="simpan_form" class="btn btn-block btn-success" type="button">Simpan</button></div>
														<div class="col-xs-6"><button id="batal" class="btn btn-block btn-success" type="button">Batal</button></div>
													</div>
													</form>
									</div>
				</div>
					
			</div>
		</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
 
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('javascript');?>
<script src="/assets/js/jquery.chainedSelects.js"></script>
<script src="/assets/js/dataTables/jquery.dataTables.js"></script>
<script src="/assets/js/dataTables/dataTables.bootstrap.js"></script>
<link href='/assets/components/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css' rel='stylesheet'/>
<script src='/assets/components/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js'></script>
<script type="text/javascript">
	$(document).ready(function () {
		required = ["fid","point_x","point_y"];
			errornotice = $("#error");
			emptyerror = "Mohon field ini diisi.";
			$('#simpan_form').click(function(){
				//Validate required fields
				for (i=0;i<required.length;i++) {
					var input = $('#'+required[i]);
					if ((input.val() == "") || (input.val() == emptyerror)) {
						input.addClass("needsfilled");
						input.val(emptyerror);
						errornotice.fadeIn(750);
					} else {
						input.removeClass("needsfilled");
					}
				}
					
				//if any inputs on the page have the class 'needsfilled' the form will not submit
				if ($(":input").hasClass("needsfilled")) {
					return false;
				} else {
					errornotice.hide();
					$('#frm').submit();
				}
												  
			});
											
			$(":input").focus(function(){		
			   if ($(this).hasClass("needsfilled") ) {
					$(this).val("");
					$(this).removeClass("needsfilled");
				}
			});
			
			$('#batal').click(function(){
				parent.history.back();
				return false;
			});
			
			 //Date picker
			$('.datepicker').datetimepicker(
				{
					format: "dd-mm-yyyy hh:ii",
					autoclose:true,
                }
			 );
			
			$( "#nip" ).select2({   
				placeholder: "Ketik NIP/NAMA",
				allowClear: false,
				ajax: {
					url: "/log/cari_nip",
					dataType: 'json',
					delay: 250,
					data: function (params) {
						return {
							q: params.term // search term
						};
					},
					processResults: function (data) {
						return {
							//results: data
							 results: $.map(data, function (item) {
							return {
								text: item.NIPNAMA,
								id: item.NIP
							}
							 })
						};
					},
					cache: true
				},
				minimumInputLength: 2
			});
		
			$("#nip").keyup(function(){
				var that = this,
				value = $(this).val();

				$.post('/log/view_identitas',
				{
					'NIP': frm.nip.value,
				},
				function(data) {
					$('#data_pegawai').html(data);
				}
						
				);
			}); 
			
			$("#nip").change(function(){
				var that = this,
				value = $(this).val();

				$.post('/log/view_identitas',
				{
					'NIP': frm.nip.value,
				},
				function(data) {
					$('#data_pegawai').html(data);
				}
						
				);
			}); 
	
});
		</script>
</body>
</html>
