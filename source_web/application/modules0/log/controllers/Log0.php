<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Log extends CI_Controller {
	public $apps;
	var $nama_user;
	var $file_foto;
	var $uri_string;
	var $s_biro;
	var $userid;
	var $user_id;
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
		if (!$this->ion_auth->logged_in())
			{
				redirect('/auth/login', 'refresh');
			}
		$this->apps = 'Kehadiran';
		$this->load->model('log_model');
		$this->nama_user = $this->ion_auth->user()->row()->first_name.' '.$this->ion_auth->user()->row()->last_name;
		$this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		$this->userid = $this->session->userdata('s_username');
		$this->uri_string = '/'.$this->uri->uri_string();
		$this->s_biro = $this->session->userdata('s_biro');
		$this->user_id = $this->ion_auth->user()->row()->id;
	}
	 
	public function absensi()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		
		$data = array(
				'title' => 'Data Log Absensi',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'sssubunit' => $this->session->userdata('sssubunit_log_absensi'),
				'jnsjab' => $this->session->userdata('jnsjab_log_absensi'),
				'keselon' => $this->session->userdata('keselon_log_absensi'),
				'nama' => $this->session->userdata('nama_log_absensi'),
				'reqnip' => $this->session->userdata('reqnip_log_absensi'),
				'tanggal' => $this->session->userdata('tanggal_log_absensi'),
		);
		$this->load->view('absensi',$data);
		input_log($this->user_id,'Lihat Data Log Absensi');
	}
	
	public function get_absensi()
	{
		//print_r($_REQUEST);
		$order = $_REQUEST['order'][0];
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/log/absensi');
		//$aColumns = array( 'nip', 'nip','nama', 'nip','nip');
		$sssubunit = $this->input->post('sssubunit');
		$this->session->set_userdata('sssubunit_log_absensi',$sssubunit);
		$jnsjab = $this->input->post('jnsjab');
		$this->session->set_userdata('jnsjab_log_absensi',$jnsjab);
		$keselon = $this->input->post('keselon');
		$this->session->set_userdata('keselon_log_absensi',$keselon);
		$nama = $this->input->post('nama');
		$this->session->set_userdata('nama_log_absensi',$nama);
		$reqnip = $this->input->post('reqnip');
		$this->session->set_userdata('reqnip_log_absensi',$reqnip);
		$tanggal = $this->input->post('tanggal');
		$this->session->set_userdata('tanggal_log_absensi',$tanggal);
		
		$sSearch = '';
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		if(isset($select) && $select != '')
		{
			if(!empty($s_biro)) 
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
			}
			else
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			}
		}
		
		if(isset($nama) && $nama != '')
		{
			$sSearch .= " and (nama like '%".$nama."%') ";
		}
		if(isset($reqnip) && $reqnip != '')
		{
			$sSearch .= " and (a.nip = '".$reqnip."') ";
		}
		if(isset($jnsjab) && $jnsjab != '0')
		{
			$sSearch .= " and (jnsjab = '".$jnsjab."') ";
		}
		if(isset($keselon) && $keselon != '0')
		{
			$sSearch .= " and (keselon = '".$keselon."') ";
		}
		if(isset($tanggal) && $tanggal != '')
		{
			$sSearch .= " and (date(b.tanggal) = '".date('Y-m-d',strtotime($tanggal))."') ";
		}
	
		//limit
		if ( isset( $_REQUEST['start'] ) && $_REQUEST['length'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_REQUEST['start'] ).", ".
				intval( $_REQUEST['length'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "`".$aColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]."` ".
						($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		//$sOrder = " order by ".$aColumns[$order['column']]." ".$order['dir']." ";
		$s0 = "select count(id_log) as jml from absensi_log b left join d_pegawai a on(a.nip = b.nip)
				where 1 = 1
				";
		$s0 .= $sSearch;
		
		$sheet_total = $this->db->query($s0)->row();
		$sheet_total = $sheet_total->jml;
		$s1 = "set @rownum  = 0;";
		$this->db->query($s1);
		$s1 = "SELECT b.*
			FROM absensi_log b
			left join d_pegawai a on(a.nip = b.nip)
			WHERE 1=1";
		$s1 .= $sSearch;
		$s1 .= " order by b.tanggal desc ";
		
		$s1 .= $sLimit;
	//	echo $s1;
		$sheet1 = $this->db->query($s1);
		$result = array();
		$i=0;
		$no=1;
		foreach($sheet1->result() as $sheet)
		{
			$data_pegawai = data_pegawai($sheet->nip);
			$result[$i]['0'] = $sheet->id_log;
			$result[$i]['1'] = $sheet->nip;
			$result[$i]['2'] = $data_pegawai['nama'];
			$result[$i]['3'] = $data_pegawai['golongan'];
			$result[$i]['4'] = $data_pegawai['jabatan'].' </br>'.$data_pegawai['unit_kerja'];
			$result[$i]['5'] = date('d-M-Y H:i:s', strtotime($sheet->tanggal));		
			$no++;
			$i++;
		}
		
		$data = $result;
		$results = array(
			"iTotalRecords" => ($sheet_total),
			"iTotalDisplayRecords" => ($sheet_total),
			"aaData"=>$data);
			
		echo json_encode($results);
	}
	
	public function harian()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		
		$data = array(
				'title' => 'Data Absensi Harian',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'sssubunit' => $this->session->userdata('sssubunit_harian'),
				'jnsjab' => $this->session->userdata('jnsjab_harian'),
				'keselon' => $this->session->userdata('keselon_harian'),
				'nama' => $this->session->userdata('nama_harian'),
				'reqnip' => $this->session->userdata('reqnip_harian'),
				'tanggal' => $this->session->userdata('tanggal_harian'),
		);
		$this->load->view('harian',$data);
		input_log($this->user_id,'Lihat Data Absensi harian pns');
	}
	
	public function get_harian()
	{
		//print_r($_REQUEST);
		$order = $_REQUEST['order'][0];
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/log/harian');
		//$aColumns = array( 'nip', 'nip','nama', 'nip','nip');
		$sssubunit = $this->input->post('sssubunit');
		$this->session->set_userdata('sssubunit_harian',$sssubunit);
		$jnsjab = $this->input->post('jnsjab');
		$this->session->set_userdata('jnsjab_harian',$jnsjab);
		$keselon = $this->input->post('keselon');
		$this->session->set_userdata('keselon_harian',$keselon);
		$nama = $this->input->post('nama');
		$this->session->set_userdata('nama_harian',$nama);
		$reqnip = $this->input->post('reqnip');
		$this->session->set_userdata('reqnip_harian',$reqnip);
		$tanggal = $this->input->post('tanggal');
		$this->session->set_userdata('tanggal_harian',$tanggal);
		$tgl = date('j',strtotime($tanggal));
		$bln = date('n',strtotime($tanggal));
		$thn = date('Y',strtotime($tanggal));
		
		$sSearch = '';
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		if(isset($select) && $select != '')
		{
			if(!empty($s_biro)) 
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
			}
			else
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			}
		}
		
		if(isset($nama) && $nama != '')
		{
			$sSearch .= " and (nama like '%".$nama."%') ";
		}
		if(isset($reqnip) && $reqnip != '')
		{
			$sSearch .= " and (a.nip = '".$reqnip."') ";
		}
		if(isset($jnsjab) && $jnsjab != '0')
		{
			$sSearch .= " and (jnsjab = '".$jnsjab."') ";
		}
		if(isset($keselon) && $keselon != '0')
		{
			$sSearch .= " and (keselon = '".$keselon."') ";
		}
		if(isset($tgl) && $tgl != '')
		{
			$sSearch .= " and (b.tanggal = '".$tgl."') ";
		}
		if(isset($bln) && $bln != '')
		{
			$sSearch .= " and (b.bulan = '".$bln."') ";
		}
		if(isset($thn) && $thn != '')
		{
			$sSearch .= " and (b.tahun = '".$thn."') ";
		}
	
		//limit
		if ( isset( $_REQUEST['start'] ) && $_REQUEST['length'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_REQUEST['start'] ).", ".
				intval( $_REQUEST['length'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "`".$aColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]."` ".
						($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		//$sOrder = " order by ".$aColumns[$order['column']]." ".$order['dir']." ";
		$s0 = "select count(a.nip) as jml from d_pegawai a
		LEFT JOIN d_absen_harian b on(a.nip = b.nip)
				where 1 = 1
				";
		$s0 .= $sSearch;
		
		$sheet_total = $this->db->query($s0)->row();
		$sheet_total = $sheet_total->jml;
	
		$s1 = "SELECT DISTINCT a.nama,
									a.keselon,
									a.kgolru,
									a.kuntp,
									a.kunkom,
									a.kununit,
									a.tanggal_lahir,
									a.jabatan,
									(select neselon from referensi_eselon where keselon = a.keselon) as eselon,
									(select ngolru from referensi_golongan_ruang where KGOLRU = a.KGOLRU) as golongan,
									(select pangkat from referensi_golongan_ruang where KGOLRU = a.KGOLRU) as pangkat,
									b.*,
									if(b.jenis_tidak_masuk != 0,(select jenis_ketidakhadiran from referensi_persentase_ketidakhadiran where id = b.jenis_tidak_masuk),'-') as ketidakhadiran
			FROM d_pegawai a
			LEFT JOIN d_absen_harian b on(a.nip = b.nip)
			WHERE 1=1";
		$s1 .= $sSearch;
		$s1 .= " order by keselon asc, kgolru desc, nip asc ";
		
		$s1 .= $sLimit;
	//	echo $s1;
		$sheet1 = $this->db->query($s1);
		$result = array();
		$i=0;
		$no=1;
		foreach($sheet1->result() as $sheet)
		{
			
			$result[$i]['0'] = $no;
			$result[$i]['1'] = $sheet->nip.' </br> '.$sheet->nama.' </br> '.$sheet->golongan;
			$result[$i]['2'] = $sheet->jabatan.' </br> '.$sheet->eselon;
			$result[$i]['3'] = ((isset($sheet->jadwal_masuk) && $sheet->jadwal_masuk != '' && $sheet->jadwal_masuk != '0000-00-00 00:00:00' && $sheet->jadwal_masuk != NULL)?date('d-m-Y H:i',strtotime($sheet->jadwal_masuk)):'shift');
			$result[$i]['4'] = ((isset($sheet->realisasi_masuk) && $sheet->realisasi_masuk != '' && $sheet->realisasi_masuk != NULL)?date('d-m-Y H:i',strtotime($sheet->realisasi_masuk)):'');
			$result[$i]['5'] = 	$sheet->keterlambatan_masuk;	
			$result[$i]['6'] = ((isset($sheet->jadwal_keluar) && $sheet->jadwal_keluar != '' && $sheet->jadwal_keluar != '0000-00-00 00:00:00' && $sheet->jadwal_keluar != NULL)?date('d-m-Y H:i',strtotime($sheet->jadwal_keluar)):'shift');
			$result[$i]['7'] = ((isset($sheet->realisasi_keluar) && $sheet->realisasi_keluar != '' && $sheet->realisasi_keluar != NULL)?date('d-m-Y H:i',strtotime($sheet->realisasi_keluar)):'');
			$result[$i]['8'] = 	$sheet->kecepatan_keluar;	
			$result[$i]['9'] = 	$sheet->ketidakhadiran;	
			$result[$i]['10'] = 	$sheet->persentase_potongan;	
			$result[$i]['11'] = 	$sheet->keterangan;	
			$result[$i]['12'] = '<a href="/log/edit_harian/'.$sheet->id.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Edit">
									<i class="glyphicon glyphicon-edit"></i>
								</a>';
			$no++;
			$i++;
		}
		
		$data = $result;
		$results = array(
			"iTotalRecords" => ($sheet_total),
			"iTotalDisplayRecords" => ($sheet_total),
			"aaData"=>$data);
			
		echo json_encode($results);
	}
	
	public function edit_harian()
	{
		$id = $this->uri->segment(3);
		$s = "select a.*,
					if(b.kunkom = 10 || b.kunkom = 11,concat(b.kuntp,b.kunkom,b.kununit,'000000'),concat(b.kuntp,b.kunkom,'00000000')) as biro,
					b.nama as nama_lengkap,
					(select ngolru from referensi_golongan_ruang where kgolru = b.kgolru) as golongan,
					b.jabatan,
					CASE KUNKOM
								   WHEN 10 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' 	)
								   WHEN 11 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' 	) 
								   ELSE (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,4) = concat(kuntp,kunkom) and mid(kunker,5,8) = '00000000' 	)
					END as skpd
				from d_absen_harian a 
				left join d_pegawai b on(a.nip = b.nip)
			where id = '$id'
				";
		//echo $s;
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			$biro = $r->biro;
			if($this->s_biro != '100000000000' && $this->s_biro != '' && $this->s_biro != $biro)
			{
				redirect('/');
			}
			
			$this->load->library('form_validation');
			$this->form_validation->set_rules('jadwal_masuk', 'Jadwal Masuk', 'trim|required');
			$this->form_validation->set_rules('realisasi_masuk', 'Realisasi Masuk', 'trim|required');
			$this->form_validation->set_rules('jadwal_keluar', 'Jadwal Masuk', 'trim|required');
			$this->form_validation->set_rules('realisasi_keluar', 'Realisasi Keluar', 'trim|required');
			
			if ($this->form_validation->run() === TRUE)
			{
				$posts = $this->input->post();
			//	print_r($posts);
			//	echo '</br>';
				$jadwal_masuk = $this->db->escape_str($posts['jadwal_masuk']);				
				$realisasi_masuk = $this->db->escape_str($posts['realisasi_masuk']);
				$realisasi_masuk = date('Y-m-d H:i:s',strtotime($realisasi_masuk));
				$keterlambatan_masuk = (strtotime($realisasi_masuk) - strtotime($jadwal_masuk))/60;
				if($keterlambatan_masuk > 0)
				{
					$keterlambatan_masuk = $keterlambatan_masuk;
					$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
				}
				else
				{
					$keterlambatan_masuk = 0;
					$potongan_terlambat_masuk=0;
				}
				
				$jadwal_keluar = $this->db->escape_str($posts['jadwal_keluar']);	
				$realisasi_keluar = $this->db->escape_str($posts['realisasi_keluar']);
				$realisasi_keluar = date('Y-m-d H:i:s',strtotime($realisasi_keluar));
				$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($realisasi_keluar))/60;
				if($kecepatan_keluar > 0)
				{
					$kecepatan_keluar = $kecepatan_keluar;
					$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
				}
				else
				{
					$kecepatan_keluar = 0;
					$potongan_pulang_cepat=0;
				}
				$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat;
											
				$update_date = date('Y-m-d H:i:s');
				$update_by = $this->userid;
						
				$jadwal_masuk = date('Y-m-d H:i:s',strtotime($jadwal_masuk));	
				$jadwal_keluar = date('Y-m-d H:i:s',strtotime($jadwal_keluar));	
				
				$keterangan = $this->db->escape_str($posts['keterangan']);	
				$sql = 	"update d_absen_harian 
							SET jadwal_masuk = '$jadwal_masuk',
								realisasi_masuk = '$realisasi_masuk',
								keterlambatan_masuk = '$keterlambatan_masuk',
								potongan_terlambat_masuk = '$potongan_terlambat_masuk',
								jadwal_keluar = '$jadwal_keluar',
								realisasi_keluar = '$realisasi_keluar',
								kecepatan_keluar = '$kecepatan_keluar',
								potongan_pulang_cepat = '$potongan_pulang_cepat',
								persentase_potongan = '$persentase_potongan',
								keterangan = '$keterangan',
								update_date = '$update_date',
								jenis_tidak_masuk = '0',
								potongan_tidak_masuk = '0',
								update_by = '$update_by'
							where id = '$id'
								";
			//	echo $sql.'</br>';
				$this->db->query($sql);	
				$this->update_data_rekap_keuangan($r->nip,$jadwal_masuk,$persentase_potongan);		
				input_log($this->user_id,'Edit Data Absensi dengan id:'.$id);
				redirect('/log/harian');
			}
			else
			{
				$this->data['jadwal_masuk'] = array(
						'name'  => 'jadwal_masuk',
						'id'    => 'jadwal_masuk',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('jadwal_masuk',(isset($r->jadwal_masuk)?date('d-m-Y H:i',strtotime($r->jadwal_masuk)):'')),
						'class'  => 'form-control datepicker',
					);
				$this->data['realisasi_masuk'] = array(
						'name'  => 'realisasi_masuk',
						'id'    => 'realisasi_masuk',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('realisasi_masuk',(isset($r->realisasi_masuk)?date('d-m-Y H:i',strtotime($r->realisasi_masuk)):'')),
						'class'  => 'form-control datepicker',
					);	
				$this->data['jadwal_keluar'] = array(
						'name'  => 'jadwal_keluar',
						'id'    => 'jadwal_keluar',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('jadwal_keluar',(isset($r->jadwal_keluar)?date('d-m-Y H:i',strtotime($r->jadwal_keluar)):'')),
						'class'  => 'form-control datepicker',
					);	
				$this->data['realisasi_keluar'] = array(
						'name'  => 'realisasi_keluar',
						'id'    => 'realisasi_keluar',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('realisasi_keluar',(isset($r->realisasi_keluar)?date('d-m-Y H:i',strtotime($r->realisasi_keluar)):'')),
						'class'  => 'form-control datepicker',
					);	
				$this->data['keterangan'] = array(
						'name'  => 'keterangan',
						'id'    => 'keterangan',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('keterangan',(($r->keterangan))),
						'class'  => 'form-control',
				);	
				$this->data['title'] = 'Data Kehadiran Harian Pegawai';
				$this->data['subtitle'] = 'Form Pemutakhiran Data Kehadiran Harian';
				$this->data['error'] = '';
				$this->data['r'] = $r;
				$this->load->view('form_edit_harian',$this->data);
			}
		
		}
		else
		{
			redirect('/');
		}
		
	}
	
	public function ekspor_absen_harian_pns_pdf()
	{
		set_time_limit(0);
		$this->load->library('pdf');
		$pdf = $this->pdf->load('format => A-2 L');
	//	print_r($_REQUEST);
		$sssubunit = $this->input->post('sssubunit_pdf');
		$jnsjab = $this->input->post('jnsjab_pdf');
		$keselon = $this->input->post('keselon_pdf');
		$nama = $this->input->post('nama_pdf');
		$reqnip = $this->input->post('reqnip_pdf');
		$tanggal = $this->input->post('tanggal_pdf');
		$tgl = date('j',strtotime($tanggal));
		$bln = date('n',strtotime($tanggal));
		$thn = date('Y',strtotime($tanggal));
		$sSearch = '';
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		if(isset($select) && $select != '')
		{
			if(!empty($s_biro)) 
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
			}
			else
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			}
		}
		
		if(isset($nama) && $nama != '')
		{
			$sSearch .= " and (nama like '%".$nama."%') ";
		}
		if(isset($reqnip) && $reqnip != '')
		{
			$sSearch .= " and (a.nip = '".$reqnip."') ";
		}
		if(isset($jnsjab) && $jnsjab != '0' && $jnsjab != '')
		{
			$sSearch .= " and (jnsjab = '".$jnsjab."') ";
		}
		if(isset($keselon) && $keselon != '0' && $keselon != '')
		{
			$sSearch .= " and (keselon = '".$keselon."') ";
		}
		if(isset($tgl) && $tgl != '')
		{
			$sSearch .= " and (b.tanggal = '".$tgl."') ";
		}
		if(isset($bln) && $bln != '')
		{
			$sSearch .= " and (b.bulan = '".$bln."') ";
		}
		if(isset($thn) && $thn != '')
		{
			$sSearch .= " and (b.tahun = '".$thn."') ";
		}
	
		$s1 = "SELECT DISTINCT a.nama,
									a.keselon,
									a.kgolru,
									a.kuntp,
									a.kunkom,
									a.kununit,
									a.tanggal_lahir,
									a.jabatan,
									(select neselon from referensi_eselon where keselon = a.keselon) as eselon,
									(select ngolru from referensi_golongan_ruang where KGOLRU = a.KGOLRU) as golongan,
									(select pangkat from referensi_golongan_ruang where KGOLRU = a.KGOLRU) as pangkat,
									b.*,
									if(b.jenis_tidak_masuk != 0,(select jenis_ketidakhadiran from referensi_persentase_ketidakhadiran where id = b.jenis_tidak_masuk),'-') as ketidakhadiran
			FROM d_pegawai a
			LEFT JOIN d_absen_harian b on(a.nip = b.nip)
			WHERE 1=1";
		$s1 .= $sSearch;
		$s1 .= " order by keselon asc, kgolru desc, nip asc ";
		echo $s1;
		$q= $this->db->query($s1);
		$data['q'] = $q;
		$data['tanggal'] = $tanggal;
		$data['unit_kerja'] = $this->get_unit_kerja($sssubunit);
		$html = $this->load->view('cetak_absen_harian_pns',$data,true);
		input_log($this->user_id,'Ekspor absen harian pns ke dalam bentuk PDF');
		$pdf->WriteHTML($html);
		$pdf->Output();
			
	}
	
	function get_unit_kerja($sssubunit)
	{
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		if(isset($select) && $select != '')
		{
			if(!empty($s_biro)) 
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
			}
			else
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			}
		}
		
		$s = "select nunker from referensi_unit_kerja where 1=1 and kunker = '$sssubunit' ";
	//	$s .= $sSearch;
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			$row = $q->row()->nunker;
		}
		else
		{
			$row = '';
		}
		return $row;
	}
	
	function get_potongan_terlambat_masuk($keterlambatan_masuk)
	{
		$sc = "select * from referensi_persentase_keterlambatan where 1=1";
		
		$qc = $this->db->query($sc);
		$persentase_pemotongan=0;
		foreach($qc->result() as $row)
		{
			$awal = $row->awal;
			$akhir = $row->akhir;
			$presentase = $row->presentase;
			if($keterlambatan_masuk >= $awal && $keterlambatan_masuk <= $akhir)
			{
				$persentase_pemotongan = $presentase;
			}
		}
		
		$persentase_pemotongan = ((isset($persentase_pemotongan) && $persentase_pemotongan > 0)?$persentase_pemotongan:0);
		return $persentase_pemotongan;
	}
	
	function get_potongan_pulang_cepat($kecepatan_keluar)
	{
		$sc = "select * from referensi_persentase_pulang_cepat where 1=1";
		$qc = $this->db->query($sc);
		foreach($qc->result() as $row)
		{
			$awal = $row->awal;
			$akhir = $row->akhir;
			$presentase = $row->presentase;
			if($kecepatan_keluar >= $awal && $kecepatan_keluar <= $akhir)
			{
				$persentase_pemotongan = $presentase;
			}
		}
		$persentase_pemotongan = ((isset($persentase_pemotongan) && $persentase_pemotongan > 0)?$persentase_pemotongan:0);
		return $persentase_pemotongan;
	}
	
	function update_data_rekap_keuangan($nip,$tanggal,$pemotongan_per_hari)
	{
		$tanggal = date('Y-m-d',strtotime($tanggal));
		$update_date = date('Y-m-d H:i:s');
		$s = "select id from d_rekap_keuangan_harian where tanggal = '$tanggal' and nip = '$nip' ";
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			$id = $q->row()->id;
			$tpp = $this->get_tpp($nip);
			$si = "update d_rekap_keuangan_harian 
					set tanggal = '$tanggal',
						nip = '$nip',
						total_tpp = '$tpp[total_tpp]',
						tpp_statis = '$tpp[tpp_statis]',
						tpp_dinamis = '$tpp[tpp_dinamis]',
						pemotongan_per_hari = '$pemotongan_per_hari',
						update_date = '$update_date',	
						update_by = 'system'
					where id = '$id'
			";
			$this->db->query($si);
		}
		else
		{
			$tpp = $this->get_tpp($nip);
			$si = "insert into d_rekap_keuangan_harian 
					set tanggal = '$tanggal',
						nip = '$nip',
						total_tpp = '$tpp[total_tpp]',
						tpp_statis = '$tpp[tpp_statis]',
						tpp_dinamis = '$tpp[tpp_dinamis]',
						pemotongan_per_hari = '$pemotongan_per_hari',
						update_date = '$update_date',	
						update_by = 'system'					
			";
			$this->db->query($si);
		}
		$this->db->insert('coba_coba',array('keterangan'=>$si,'waktu'=>date('Y-m-d H:i:s')));
	}
	
	function get_tpp($nip)
	{
		$sp = "select * from referensi_proporsi_tunjangan";
		$qp = $this->db->query($sp);
		$proporsi = array();
		foreach($qp->result() as $rp)
		{
			$proporsi[$rp->status] = $rp->proporsi;
		}
		$s = "select jnsjab,kstatus from d_pegawai where nip = '$nip' ";
		
		$jnsjab = $this->db->query($s)->row()->jnsjab;
		$kstatus = $this->db->query($s)->row()->kstatus;
		switch($jnsjab)
		{
			case 1:
				$s1 = "select b.tunjangan 
						from d_pegawai a 
						left join referensi_unit_kerja b on(a.kunker = b.kunker)
					where a.nip = '$nip'
						";
				
				$q1 = $this->db->query($s1);
				if($q1->num_rows() > 0)
				{
					$tunjangan = $q1->row()->tunjangan;
					$result['total_tpp'] = $tunjangan;
					$result['tpp_statis'] = $tunjangan*($proporsi['statis']/100);
					$result['tpp_dinamis'] = $tunjangan*($proporsi['dinamis']/100);					
				}
				else
				{
					$result['total_tpp'] = 0;
					$result['tpp_statis'] = 0;
					$result['tpp_dinamis'] = 0;	
				}
			break;
			
			case 2:
				$s1 = "select b.tunjangan 
						from d_pegawai a 
						left join referensi_jabatan_fungsional_tertentu b on(a.kjab = b.KJAB)
					where a.nip = '$nip'
						";
				$q1 = $this->db->query($s1);
				if($q1->num_rows() > 0)
				{
					$tunjangan = $q1->row()->tunjangan;
					$result['total_tpp'] = $tunjangan;
					$result['tpp_statis'] = $tunjangan*($proporsi['statis']/100);
					$result['tpp_dinamis'] = $tunjangan*($proporsi['dinamis']/100);					
				}
				else
				{
					$result['total_tpp'] = 0;
					$result['tpp_statis'] = 0;
					$result['tpp_dinamis'] = 0;	
				}
			break;
			
			case 3:
			
			break;
			
			case 4:
				$s1 = "select a.tunjangan 
						from referensi_tunjangan_jfu a 
					where a.status = '$kstatus'
						";
				$q1 = $this->db->query($s1);
				if($q1->num_rows() > 0)
				{
					$tunjangan = $q1->row()->tunjangan;
					$result['total_tpp'] = $tunjangan;
					$result['tpp_statis'] = $tunjangan*($proporsi['statis']/100);
					$result['tpp_dinamis'] = $tunjangan*($proporsi['dinamis']/100);					
				}
				else
				{
					$result['total_tpp'] = 0;
					$result['tpp_statis'] = 0;
					$result['tpp_dinamis'] = 0;	
				}
			break;
		}
		return $result;
	}

	public function harian_tkk()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		
		$data = array(
				'title' => 'Data Absensi Harian TKK',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'sssubunit' => $this->session->userdata('sssubunit_harian_tkk'),
				'nama' => $this->session->userdata('nama_harian_tkk'),
				'nik' => $this->session->userdata('nik_harian_tkk'),
				'tanggal' => $this->session->userdata('tanggal_harian_tkk'),
		);
		$this->load->view('harian_tkk',$data);
		input_log($this->user_id,'Lihat Data absens harian tkk');
	}
	
	public function get_harian_tkk()
	{
		//print_r($_REQUEST);
		$order = $_REQUEST['order'][0];
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/log/harian');
		//$aColumns = array( 'nip', 'nip','nama', 'nip','nip');
		$sssubunit = $this->input->post('sssubunit');
		$this->session->set_userdata('sssubunit_harian_tkk',$sssubunit);
		$nama = $this->input->post('nama');
		$this->session->set_userdata('nama_harian_tkk',$nama);
		$nik = $this->input->post('nik');
		$this->session->set_userdata('nik_harian_tkk',$nik);
		$tanggal = $this->input->post('tanggal');
		$this->session->set_userdata('tanggal_harian_tkk',$tanggal);
		$tgl = date('j',strtotime($tanggal));
		$bln = date('n',strtotime($tanggal));
		$thn = date('Y',strtotime($tanggal));
		
		$sSearch = '';
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		if(isset($select) && $select != '')
		{
			if(!empty($s_biro)) 
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
			}
			else
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			}
		}
		
		if(isset($nama) && $nama != '')
		{
			$sSearch .= " and (nama like '%".$nama."%') ";
		}
		if(isset($nik) && $nik != '')
		{
			$sSearch .= " and (a.nik = '".$nik."') ";
		}
		
		if(isset($tgl) && $tgl != '')
		{
			$sSearch .= " and (b.tanggal = '".$tgl."') ";
		}
		if(isset($bln) && $bln != '')
		{
			$sSearch .= " and (b.bulan = '".$bln."') ";
		}
		if(isset($thn) && $thn != '')
		{
			$sSearch .= " and (b.tahun = '".$thn."') ";
		}
	
		//limit
		if ( isset( $_REQUEST['start'] ) && $_REQUEST['length'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_REQUEST['start'] ).", ".
				intval( $_REQUEST['length'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "`".$aColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]."` ".
						($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		//$sOrder = " order by ".$aColumns[$order['column']]." ".$order['dir']." ";
		$s0 = "select count(a.nik) as jml from d_tkk a
		LEFT JOIN d_absen_harian_tkk b on(a.nik = b.nik)
				where 1 = 1
				";
		$s0 .= $sSearch;
		
		$sheet_total = $this->db->query($s0)->row();
		$sheet_total = $sheet_total->jml;
	
		$s1 = "SELECT DISTINCT a.*,
								concat(kuntp,kunkom,kununit,rincunit1,rincunit2,rincunit3) as kunker,
								b.*,									
								if(b.jenis_tidak_masuk != 0,(select jenis_ketidakhadiran from referensi_persentase_ketidakhadiran where id = b.jenis_tidak_masuk),'-') as ketidakhadiran
			FROM d_tkk a
			LEFT JOIN d_absen_harian_tkk b on(a.nik = b.nik)
			WHERE 1=1";
		$s1 .= $sSearch;
		$s1 .= " order by a.nik desc ";
		
		$s1 .= $sLimit;
	//	echo $s1;
		$sheet1 = $this->db->query($s1);
		$result = array();
		$i=0;
		$no=1;
		foreach($sheet1->result() as $sheet)
		{
			
			$result[$i]['0'] = $no;
			$result[$i]['1'] = $sheet->nik.' </br> '.$sheet->nama;
			$result[$i]['2'] = $this->get_skpd($sheet->kunker);
			$result[$i]['3'] = $sheet->jadwal_masuk;
			$result[$i]['4'] = $sheet->realisasi_masuk;
			$result[$i]['5'] = 	$sheet->keterlambatan_masuk;	
			$result[$i]['6'] = $sheet->jadwal_keluar;
			$result[$i]['7'] = $sheet->realisasi_keluar;
			$result[$i]['8'] = 	$sheet->kecepatan_keluar;	
			$result[$i]['9'] = 	$sheet->ketidakhadiran;	
			$result[$i]['10'] = 	$sheet->persentase_potongan;	
			$result[$i]['11'] = 	$sheet->keterangan;	
			$result[$i]['12'] = '<a href="/log/edit_harian_tkk/'.$sheet->id.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Edit">
									<i class="glyphicon glyphicon-edit"></i>
								</a>';
			$no++;
			$i++;
		}
		
		$data = $result;
		$results = array(
			"iTotalRecords" => ($sheet_total),
			"iTotalDisplayRecords" => ($sheet_total),
			"aaData"=>$data);
			
		echo json_encode($results);
	}
	
	public function edit_harian_tkk()
	{
		$id = $this->uri->segment(3);
		$s = "select a.*,
					if(b.kunkom = 10 || b.kunkom = 11,concat(b.kuntp,b.kunkom,b.kununit,'000000'),concat(b.kuntp,b.kunkom,'00000000')) as biro,
					b.nama as nama_lengkap,
					CASE KUNKOM
								   WHEN 10 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' 	)
								   WHEN 11 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' 	) 
								   ELSE (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,4) = concat(kuntp,kunkom) and mid(kunker,5,8) = '00000000' 	)
					END as skpd
				from d_absen_harian_tkk a 
				left join d_tkk b on(a.nik = b.nik)
			where a.id = '$id'
				";
		//echo $s;
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			$biro = $r->biro;
			if($this->s_biro != '100000000000' && $this->s_biro != '' && $this->s_biro != $biro)
			{
				redirect('/');
			}
			
			$this->load->library('form_validation');
			$this->form_validation->set_rules('jadwal_masuk', 'Jadwal Masuk', 'trim|required');
			$this->form_validation->set_rules('realisasi_masuk', 'Realisasi Masuk', 'trim|required');
			$this->form_validation->set_rules('jadwal_keluar', 'Jadwal Masuk', 'trim|required');
			$this->form_validation->set_rules('realisasi_keluar', 'Realisasi Keluar', 'trim|required');
			if ($this->form_validation->run() === TRUE)
			{
				$posts = $this->input->post();
			//	print_r($posts);
			//	echo '</br>';
				$jadwal_masuk = $this->db->escape_str($posts['jadwal_masuk']);		
				$realisasi_masuk = $this->db->escape_str($posts['realisasi_masuk']);
				$realisasi_masuk = date('Y-m-d H:i:s',strtotime($realisasi_masuk));
				$keterlambatan_masuk = (strtotime($realisasi_masuk) - strtotime($jadwal_masuk))/60;
				if($keterlambatan_masuk > 0)
				{
					$keterlambatan_masuk = $keterlambatan_masuk;
					$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
				}
				else
				{
					$keterlambatan_masuk = 0;
					$potongan_terlambat_masuk=0;
				}
				
				$jadwal_keluar = $this->db->escape_str($posts['jadwal_keluar']);					
				$realisasi_keluar = $this->db->escape_str($posts['realisasi_keluar']);
				$realisasi_keluar = date('Y-m-d H:i:s',strtotime($realisasi_keluar));
				$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($realisasi_keluar))/60;
				if($kecepatan_keluar > 0)
				{
					$kecepatan_keluar = $kecepatan_keluar;
					$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
				}
				else
				{
					$kecepatan_keluar = 0;
					$potongan_pulang_cepat=0;
				}
				$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat;
											
				$update_date = date('Y-m-d H:i:s');
				$update_by = $this->userid;
				
				$jadwal_masuk = date('Y-m-d H:i:s',strtotime($jadwal_masuk));	
				$jadwal_keluar = date('Y-m-d H:i:s',strtotime($jadwal_keluar));	
				
				$keterangan = $this->db->escape_str($posts['keterangan']);	
				$sql = 	"update d_absen_harian_tkk 
							SET jadwal_masuk = '$jadwal_masuk',
								realisasi_masuk = '$realisasi_masuk',
								keterlambatan_masuk = '$keterlambatan_masuk',
								potongan_terlambat_masuk = '$potongan_terlambat_masuk',
								jadwal_keluar = '$jadwal_keluar',
								realisasi_keluar = '$realisasi_keluar',
								kecepatan_keluar = '$kecepatan_keluar',
								potongan_pulang_cepat = '$potongan_pulang_cepat',
								persentase_potongan = '$persentase_potongan',
								keterangan = '$keterangan',
								update_date = '$update_date',
								update_by = '$update_by'
							where id = '$id'
								";
			//	echo $sql.'</br>';
				$this->db->query($sql);	
				$this->update_data_rekap_keuangan_tkk($r->nik,$jadwal_masuk,$persentase_potongan);		
				input_log($this->user_id,'edit data absensi harian tkk dengan id:'.$id);
				redirect('/log/harian_tkk');
			}
			else
			{
				$this->data['jadwal_masuk'] = array(
						'name'  => 'jadwal_masuk',
						'id'    => 'jadwal_masuk',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('jadwal_masuk',(isset($r->jadwal_masuk)?date('d-m-Y H:i',strtotime($r->jadwal_masuk)):'')),
						'class'  => 'form-control datepicker',
					);
				$this->data['realisasi_masuk'] = array(
						'name'  => 'realisasi_masuk',
						'id'    => 'realisasi_masuk',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('realisasi_masuk',(isset($r->realisasi_masuk)?date('d-m-Y H:i',strtotime($r->realisasi_masuk)):'')),
						'class'  => 'form-control datepicker',
					);	
				$this->data['jadwal_keluar'] = array(
						'name'  => 'jadwal_keluar',
						'id'    => 'jadwal_keluar',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('jadwal_keluar',(isset($r->jadwal_keluar)?date('d-m-Y H:i',strtotime($r->jadwal_keluar)):'')),
						'class'  => 'form-control datepicker',
					);	
				$this->data['realisasi_keluar'] = array(
						'name'  => 'realisasi_keluar',
						'id'    => 'realisasi_keluar',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('realisasi_keluar',(isset($r->realisasi_keluar)?date('d-m-Y H:i',strtotime($r->realisasi_keluar)):'')),
						'class'  => 'form-control datepicker',
					);	
				$this->data['keterangan'] = array(
						'name'  => 'keterangan',
						'id'    => 'keterangan',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('keterangan',(($r->keterangan))),
						'class'  => 'form-control',
				);	
				$this->data['title'] = 'Data Kehadiran Harian Pegawai';
				$this->data['subtitle'] = 'Form Pemutakhiran Data Kehadiran Harian';
				$this->data['error'] = '';
				$this->data['r'] = $r;
				$this->load->view('form_edit_harian_tkk',$this->data);
			}
		
		}
		else
		{
			redirect('/');
		}
		
	}
	
	public function ekspor_absen_harian_tkk_pdf()
	{
		set_time_limit(0);
		$this->load->library('pdf');
		$pdf = $this->pdf->load('format => A-2 L');
	//	print_r($_REQUEST);
		$sssubunit = $this->input->post('sssubunit_pdf');
		$nama = $this->input->post('nama_pdf');
		$nik = $this->input->post('nik_pdf');
		$tanggal = $this->input->post('tanggal_pdf');
		$tgl = date('j',strtotime($tanggal));
		$bln = date('n',strtotime($tanggal));
		$thn = date('Y',strtotime($tanggal));
		$sSearch = '';
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		if(isset($select) && $select != '')
		{
			if(!empty($s_biro)) 
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
			}
			else
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			}
		}
		
		if(isset($nama) && $nama != '')
		{
			$sSearch .= " and (nama like '%".$nama."%') ";
		}
		if(isset($nik) && $nik != '')
		{
			$sSearch .= " and (a.nik = '".$nik."') ";
		}
		
		if(isset($tgl) && $tgl != '')
		{
			$sSearch .= " and (b.tanggal = '".$tgl."') ";
		}
		if(isset($bln) && $bln != '')
		{
			$sSearch .= " and (b.bulan = '".$bln."') ";
		}
		if(isset($thn) && $thn != '')
		{
			$sSearch .= " and (b.tahun = '".$thn."') ";
		}
	
		$s1 = "SELECT DISTINCT a.*,
								b.*,
									CASE KUNKOM
								   WHEN 10 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' 	)
								   WHEN 11 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' 	) 
								   ELSE (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,4) = concat(kuntp,kunkom) and mid(kunker,5,8) = '00000000' 	)
								END as skpd,
								if(b.jenis_tidak_masuk != 0,(select jenis_ketidakhadiran from referensi_persentase_ketidakhadiran where id = b.jenis_tidak_masuk),'-') as ketidakhadiran
			FROM d_tkk a
			LEFT JOIN d_absen_harian_tkk b on(a.nik = b.nik)
			WHERE 1=1";
		$s1 .= $sSearch;
		$s1 .= " order by a.nik desc ";
	//	echo $s1;
		$q= $this->db->query($s1);
		$data['q'] = $q;
		$data['tanggal'] = $tanggal;
		$data['unit_kerja'] = $this->get_unit_kerja($sssubunit);
		$html = $this->load->view('cetak_absen_harian_tkk',$data,true);
		input_log($this->user_id,'ekspor absensi harian tkk ke dalam bentuk pdf');
		$pdf->WriteHTML($html);
		$pdf->Output();
			
	}
	
	function update_data_rekap_keuangan_tkk($nik,$tanggal,$pemotongan_per_hari)
	{
		$this->db->insert('coba_coba',array('keterangan'=>'update data keuangan','waktu'=>date('Y-m-d H:i:s')));
		$tanggal = date('Y-m-d',strtotime($tanggal));
		$update_date = date('Y-m-d H:i:s');
		$s = "select id from d_rekap_keuangan_harian_tkk where tanggal = '$tanggal' and nik = '$nik' ";
		$this->db->insert('coba_coba',array('keterangan'=>$s,'waktu'=>date('Y-m-d H:i:s')));
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			$id = $q->row()->id;
			$tpp = $this->get_tpp_tkk($nik);
			$si = "update d_rekap_keuangan_harian_tkk 
					set tanggal = '$tanggal',
						nik = '$nik',
						total_tpp = '$tpp[total_tpp]',
						tpp_statis = '$tpp[tpp_statis]',
						tpp_dinamis = '$tpp[tpp_dinamis]',
						pemotongan_per_hari = '$pemotongan_per_hari',
						update_date = '$update_date',	
						update_by = 'system'
					where id = '$id'
			";
			$this->db->query($si);
		}
		else
		{
			$tpp = $this->get_tpp_tkk($nik);
			$this->db->insert('coba_coba',array('keterangan'=>$tpp['total_tpp'],'waktu'=>date('Y-m-d H:i:s')));
			$si = "insert into d_rekap_keuangan_harian_tkk 
					set tanggal = '$tanggal',
						nik = '$nik',
						total_tpp = '$tpp[total_tpp]',
						tpp_statis = '$tpp[tpp_statis]',
						tpp_dinamis = '$tpp[tpp_dinamis]',
						pemotongan_per_hari = '$pemotongan_per_hari',
						update_date = '$update_date',	
						update_by = 'system'					
			";
			$this->db->query($si);
		}
		$this->db->insert('coba_coba',array('keterangan'=>$si,'waktu'=>date('Y-m-d H:i:s')));
	}
	
	function get_tpp_tkk($nik)
	{
		$sp = "select * from referensi_proporsi_tunjangan";
		$qp = $this->db->query($sp);
		$proporsi = array();
		foreach($qp->result() as $rp)
		{
			$proporsi[$rp->status] = $rp->proporsi;
		}
		$s1 = "select a.tunjangan 
						from referensi_tunjangan_jfu a 
					where a.status = '4'
						";
		$q1 = $this->db->query($s1);
		if($q1->num_rows() > 0)
		{
			$tunjangan = $q1->row()->tunjangan;
			$result['total_tpp'] = $tunjangan;
			$result['tpp_statis'] = $tunjangan*($proporsi['statis']/100);
			$result['tpp_dinamis'] = $tunjangan*($proporsi['dinamis']/100);					
		}
		else
		{
			$result['total_tpp'] = 0;
			$result['tpp_statis'] = 0;
			$result['tpp_dinamis'] = 0;	
		}
		
		return $result;
	}
	
	public function bulanan()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		
		$data = array(
				'title' => 'Data Absensi Bulanan',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'sssubunit' => $this->session->userdata('sssubunit_bulanan'),
				'jnsjab' => $this->session->userdata('jnsjab_bulanan'),
				'keselon' => $this->session->userdata('keselon_bulanan'),
				'nama' => $this->session->userdata('nama_bulanan'),
				'reqnip' => $this->session->userdata('reqnip_bulanan'),
				'bulan' => $this->session->userdata('bulan_bulanan'),
				'tahun' => $this->session->userdata('tahun_bulanan'),
		);
		$this->load->view('bulanan',$data);
		input_log($this->user_id,'Lihat Data bulanan PNS');
	}
	
	public function get_bulanan()
	{
		//print_r($_REQUEST);
		$order = $_REQUEST['order'][0];
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/log/bulanan');
		//$aColumns = array( 'nip', 'nip','nama', 'nip','nip');
		$sssubunit = $this->input->post('sssubunit');
		$this->session->set_userdata('sssubunit_bulanan',$sssubunit);
		$jnsjab = $this->input->post('jnsjab');
		$this->session->set_userdata('jnsjab_bulanan',$jnsjab);
		$keselon = $this->input->post('keselon');
		$this->session->set_userdata('keselon_bulanan',$keselon);
		$nama = $this->input->post('nama');
		$this->session->set_userdata('nama_bulanan',$nama);
		$reqnip = $this->input->post('reqnip');
		$this->session->set_userdata('reqnip_bulanan',$reqnip);
		$bulan = $this->input->post('bulan');
		$this->session->set_userdata('bulan_bulanan',$bulan);
		$tahun = $this->input->post('tahun');
		$this->session->set_userdata('tahun_bulanan',$tahun);
		
		$sSearch = '';
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		if(isset($select) && $select != '')
		{
			if(!empty($s_biro)) 
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
			}
			else
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			}
		}
		
		if(isset($nama) && $nama != '')
		{
			$sSearch .= " and (nama like '%".$nama."%') ";
		}
		if(isset($reqnip) && $reqnip != '')
		{
			$sSearch .= " and (a.nip = '".$reqnip."') ";
		}
		if(isset($jnsjab) && $jnsjab != '0')
		{
			$sSearch .= " and (jnsjab = '".$jnsjab."') ";
		}
		if(isset($keselon) && $keselon != '0')
		{
			$sSearch .= " and (keselon = '".$keselon."') ";
		}
		/* if(isset($bulan) && $bulan != '')
		{
			$sSearch .= " and (b.bulan = '".$bulan."') ";
		}
		if(isset($tahun) && $tahun != '')
		{
			$sSearch .= " and (b.tahun = '".$tahun."') ";
		} */
	
		//limit
		if ( isset( $_REQUEST['start'] ) && $_REQUEST['length'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_REQUEST['start'] ).", ".
				intval( $_REQUEST['length'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "`".$aColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]."` ".
						($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		//$sOrder = " order by ".$aColumns[$order['column']]." ".$order['dir']." ";
		$s0 = "select count(a.nip) as jml 
				from d_pegawai a
				where 1 = 1
				";
		$s0 .= $sSearch;
		//echo $s0;
		$sheet_total = $this->db->query($s0)->row();
		$sheet_total = $sheet_total->jml;
	
		$s1 = "SELECT DISTINCT a.nip,a.nama,
									a.keselon,
									a.kgolru,
									a.kuntp,
									a.kunkom,
									a.kununit,
									a.tanggal_lahir,
									a.jabatan,
									(select neselon from referensi_eselon where keselon = a.keselon) as eselon,
									(select ngolru from referensi_golongan_ruang where KGOLRU = a.KGOLRU) as golongan,
									(select pangkat from referensi_golongan_ruang where KGOLRU = a.KGOLRU) as pangkat
				FROM d_pegawai a
				WHERE 1=1";
		$s1 .= $sSearch;
		$s1 .= " order by keselon asc, kgolru desc, nip asc ";
		
		$s1 .= $sLimit;
	//	echo $s1;
		$sheet1 = $this->db->query($s1);
		$result = array();
		$i=0;
		$no=1;
		foreach($sheet1->result() as $sheet)
		{
			
			$result[$i]['0'] = $no;
			$result[$i]['1'] = $sheet->nip;
			$result[$i]['2'] = $sheet->nama;
			$result[$i]['3'] = $sheet->golongan;
			$result[$i]['4'] = $sheet->jabatan;
			$result[$i]['5'] = 	$sheet->eselon;	
			$result[$i]['6'] = 	'<a target="_blank" href="/log/view_bulanan/'.$sheet->nip.'/'.$bulan.'/'.$tahun.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="View">
									<i class="glyphicon glyphicon-search"></i>
									</a>';	
			$result[$i]['7'] = 	'<a target="_blank" href="/log/pdf_bulanan/'.$sheet->nip.'/'.$bulan.'/'.$tahun.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="View">
									<i class="glyphicon glyphicon-print"></i>
									</a>';	
			$no++;
			$i++;
		}
		
		$data = $result;
		$results = array(
			"iTotalRecords" => ($sheet_total),
			"iTotalDisplayRecords" => ($sheet_total),
			"aaData"=>$data);
			
		echo json_encode($results);
	}
	
	public function bulanan_tkk()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		
		$data = array(
				'title' => 'Data Absensi Bulanan TKK',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'sssubunit' => $this->session->userdata('sssubunit_bulanan_tkk'),
				'nama' => $this->session->userdata('nama_bulanan_tkk'),
				'nik' => $this->session->userdata('nik_bulanan_tkk'),
				'bulan' => $this->session->userdata('bulan_bulanan_tkk'),
				'tahun' => $this->session->userdata('tahun_bulanan_tkk'),
		);
		$this->load->view('bulanan_tkk',$data);
		input_log($this->user_id,'Lihat Data bulanan tkk');
	}
	
	public function get_bulanan_tkk()
	{
		//print_r($_REQUEST);
		$order = $_REQUEST['order'][0];
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/log/bulanan');
		//$aColumns = array( 'nip', 'nip','nama', 'nip','nip');
		$sssubunit = $this->input->post('sssubunit');
		$this->session->set_userdata('sssubunit_bulanan_tkk',$sssubunit);
		$nama = $this->input->post('nama');
		$this->session->set_userdata('nama_bulanan_tkk',$nama);
		$nik = $this->input->post('nik');
		$this->session->set_userdata('nik_bulanan_tkk',$nik);
		$bulan = $this->input->post('bulan');
		$this->session->set_userdata('bulan_bulanan_tkk',$bulan);
		$tahun = $this->input->post('tahun');
		$this->session->set_userdata('tahun_bulanan_tkk',$tahun);
		
		$sSearch = '';
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		if(isset($select) && $select != '')
		{
			if(!empty($s_biro)) 
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
			}
			else
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			}
		}
		
		if(isset($nama) && $nama != '')
		{
			$sSearch .= " and (nama like '%".$nama."%') ";
		}
		if(isset($nik) && $nik != '')
		{
			$sSearch .= " and (a.nik = '".$nik."') ";
		}
			
		//limit
		if ( isset( $_REQUEST['start'] ) && $_REQUEST['length'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_REQUEST['start'] ).", ".
				intval( $_REQUEST['length'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "`".$aColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]."` ".
						($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		//$sOrder = " order by ".$aColumns[$order['column']]." ".$order['dir']." ";
		$s0 = "select count(a.nik) as jml from d_tkk a
		LEFT JOIN d_absen_harian_tkk b on(a.nik = b.nik)
				where 1 = 1
				";
		$s0 .= $sSearch;
		
		$sheet_total = $this->db->query($s0)->row();
		$sheet_total = $sheet_total->jml;
	
		$s1 = "SELECT DISTINCT a.nik,a.nama,
								concat(kuntp,kunkom,kununit,rincunit1,rincunit2,rincunit3) as kunker
				FROM d_tkk a
				WHERE 1=1";
		$s1 .= $sSearch;
		$s1 .= " order by nik desc ";
		
		$s1 .= $sLimit;
	//	echo $s1;
		$sheet1 = $this->db->query($s1);
		$result = array();
		$i=0;
		$no=1;
		foreach($sheet1->result() as $sheet)
		{
			
			$result[$i]['0'] = $no;
			$result[$i]['1'] = $sheet->nik;
			$result[$i]['2'] = $sheet->nama;
			$result[$i]['3'] = $this->get_skpd($sheet->kunker);
			$result[$i]['4'] = 	'<a target="_blank" href="/log/view_bulanan_tkk/'.str_replace(' ','',$sheet->nik).'/'.$bulan.'/'.$tahun.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="View">
									<i class="glyphicon glyphicon-search"></i>
									</a>';	
			$result[$i]['5'] = 	'<a target="_blank" href="/log/pdf_bulanan_tkk/'.str_replace(' ','',$sheet->nik).'/'.$bulan.'/'.$tahun.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="View">
									<i class="glyphicon glyphicon-print"></i>
									</a>';	
			$no++;
			$i++;
		}
		
		$data = $result;
		$results = array(
			"iTotalRecords" => ($sheet_total),
			"iTotalDisplayRecords" => ($sheet_total),
			"aaData"=>$data);
			
		echo json_encode($results);
	}
	
	public function cari_nip()
	{
		$where = "";
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		$q = $this->db->escape_str($_REQUEST['q']);
		$s = "select a.NIP,a.NAMA,
					CONCAT(a.nip,
							'-',
							a.nama,
							'-',
							a.jabatan
							)  AS NIPNAMA
				FROM d_pegawai a
				where 1=1
				";
		if(!empty($s_biro)) 
		{
			$select = $s_biro;
			if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$where = " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$where = " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$where = " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$where = " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$where = " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
		}
		$s .= $where;
		$s .=" and (a.nama like '%".$q."%' OR a.nip like '%".$q."%' )";
		//echo $s;
		$q = $this->db->query($s);
		$res = array();
		foreach($q->result_array() as $r)
		{
			$res[] = $r;
		}
		echo json_encode($res);
	}
	
	public function view_log_absensi()
	{
		$nip = $_REQUEST['nip'];
		$bulan = $_REQUEST['bulan'];
		$tahun = $_REQUEST['tahun'];
		$jml_hari = cal_days_in_month(CAL_GREGORIAN,$bulan,$tahun);
		$res = '';
		$res .='<div class="row">';
		$res .='<div class="col-xs-12">
				<div class="panel panel-success">
                    <div class="panel-heading">
                        Daftar Absensi Pegawai Dengan NIP: '.$nip.'
                    </div>
					<div class="panel-body">';
		$res .='<table class="table">';
		$res .='<tr><td>No</td><td>Tanggal</td><td>Masuk</td><td>Pulang</td></tr>';
		for($i=1;$i<=$jml_hari;$i++)
		{
			$tgl = date('d-M-Y',strtotime($tahun.'-'.$bulan.'-'.$i));
			$tanggal = date('d-m-Y',strtotime($tahun.'-'.$bulan.'-'.$i));
			$res .='<tr><td>'.$i.'</td><td>'.$tgl.'</td><td>'.get_waktu_masuk($nip,$tanggal).'</td><td>'.get_waktu_pulang($nip,$tanggal).'</td></tr>';
		}
		$res .='</table>';
		$res .='</div>
				</div>
				</div>
				</div>';
		echo $res;
	}
	
	public function view_bulanan()
	{
		$nip = $this->uri->segment(3);
		$bulan = $this->uri->segment(4);
		$tahun = $this->uri->segment(5);
		$sc = "select a.*,
					if(a.jenis_tidak_masuk != 0,(select jenis_ketidakhadiran from referensi_persentase_ketidakhadiran where id = a.jenis_tidak_masuk),'-') as ketidakhadiran
				from d_absen_harian a
				where nip = '$nip' 
				and bulan = '$bulan' 
				and tahun = '$tahun' ";
		$qc = $this->db->query($sc);
		if($qc->num_rows() > 0)
		{
			$sql = "SELECT DISTINCT a.nama,
									a.keselon,
									a.kgolru,
									a.kuntp,
									a.kunkom,
									a.kununit,
									a.tanggal_lahir,
									a.jabatan,
									(select neselon from referensi_eselon where keselon = a.keselon) as eselon,
									(select ngolru from referensi_golongan_ruang where KGOLRU = a.KGOLRU) as golongan,
									(select pangkat from referensi_golongan_ruang where KGOLRU = a.KGOLRU) as pangkat
			FROM d_pegawai a
			where nip = '$nip' ";
			$data = $this->db->query($sql)->row_array();
			$nama_bulan = array(
				'1' => 'Januari',
				'2' => 'Februari',
				'3' => 'Maret',
				'4' => 'April',
				'5' => 'Mei',
				'6' => 'Juni',
				'7' => 'Juli',
				'8' => 'Agustus',
				'9' => 'September',
				'10' => 'Oktober',
				'11' => 'November',
				'12' => 'Desember',
			);
			$data = array(
				'title' => 'Daftar Hadir Bulanan',
				'subtitle' => '',
				'rows' => $qc,
				'nip' => $nip,
				'bulan' => $bulan,
				'tahun' => $tahun,
				'data' => $data,
				'nama_bulan' => $nama_bulan
			);
			$this->load->view('view_bulanan',$data);
			input_log($this->user_id,'Lihat Data Absensi Bulanan dengan nip:'.$nip.' bulan:'.$bulan.' tahun:'.$tahun);
		}
	}
	
	public function pdf_bulanan()
	{
		$this->load->library('pdf');
		$pdf = $this->pdf->load();
		$nip = $this->uri->segment(3);
		$bulan = $this->uri->segment(4);
		$tahun = $this->uri->segment(5);
		$sc = "select a.*,
					if(a.jenis_tidak_masuk != 0,(select jenis_ketidakhadiran from referensi_persentase_ketidakhadiran where id = a.jenis_tidak_masuk),'-') as ketidakhadiran
				from d_absen_harian a
				where nip = '$nip' 
				and bulan = '$bulan' 
				and tahun = '$tahun' ";
		$qc = $this->db->query($sc);
		if($qc->num_rows() > 0)
		{
			$sql = "SELECT DISTINCT a.nama,
									a.keselon,
									a.kgolru,
									a.kuntp,
									a.kunkom,
									a.kununit,
									a.tanggal_lahir,
									a.jabatan,
									(select neselon from referensi_eselon where keselon = a.keselon) as eselon,
									(select ngolru from referensi_golongan_ruang where KGOLRU = a.KGOLRU) as golongan,
									(select pangkat from referensi_golongan_ruang where KGOLRU = a.KGOLRU) as pangkat
			FROM d_pegawai a
			where nip = '$nip' ";
			$data = $this->db->query($sql)->row_array();
			$nama_bulan = array(
				'1' => 'Januari',
				'2' => 'Februari',
				'3' => 'Maret',
				'4' => 'April',
				'5' => 'Mei',
				'6' => 'Juni',
				'7' => 'Juli',
				'8' => 'Agustus',
				'9' => 'September',
				'10' => 'Oktober',
				'11' => 'November',
				'12' => 'Desember',
			);
			$data = array(
				'title' => 'Daftar Hadir Bulanan',
				'subtitle' => '',
				'rows' => $qc,
				'nip' => $nip,
				'bulan' => $bulan,
				'tahun' => $tahun,
				'data' => $data,
				'nama_bulan' => $nama_bulan
			);
			$html = $this->load->view('pdf_bulanan',$data,true);
			input_log($this->user_id,'Cetak Data Absensi Bulanan ke dalam bentuk pdf dengan nip:'.$nip.' bulan:'.$bulan.' tahun:'.$tahun);
			$pdf->WriteHTML($html);
			$pdf->Output();
		}
	}
	
	public function view_bulanan_tkk()
	{
		$nik = $this->uri->segment(3);
		$bulan = $this->uri->segment(4);
		$tahun = $this->uri->segment(5);
		$sc = "select a.*,
					if(a.jenis_tidak_masuk != 0,(select jenis_ketidakhadiran from referensi_persentase_ketidakhadiran where id = a.jenis_tidak_masuk),'-') as ketidakhadiran
				from d_absen_harian_tkk a
				where nik = '$nik' 
				and bulan = '$bulan' 
				and tahun = '$tahun' ";
		//echo $sc;
		$qc = $this->db->query($sc);
		if($qc->num_rows() > 0)
		{
			$sql = "SELECT DISTINCT a.*,(select nunker from referensi_unit_kerja where kunker = concat(kuntp,kunkom,kununit,rincunit1,rincunit2,rincunit3)) as skpd
			FROM d_tkk a
			where nik = '$nik' ";
			$data = $this->db->query($sql)->row_array();
			$nama_bulan = array(
				'1' => 'Januari',
				'2' => 'Februari',
				'3' => 'Maret',
				'4' => 'April',
				'5' => 'Mei',
				'6' => 'Juni',
				'7' => 'Juli',
				'8' => 'Agustus',
				'9' => 'September',
				'10' => 'Oktober',
				'11' => 'November',
				'12' => 'Desember',
			);
			$data = array(
				'title' => 'Daftar Hadir Bulanan TKK',
				'subtitle' => '',
				'rows' => $qc,
				'nik' => $nik,
				'bulan' => $bulan,
				'tahun' => $tahun,
				'data' => $data,
				'nama_bulan' => $nama_bulan
			);
			$this->load->view('view_bulanan_tkk',$data);
			input_log($this->user_id,'Lihat Data Absensi Bulanan TKK dengan nik:'.$nik.' bulan:'.$bulan.' tahun:'.$tahun);
		}
	}
	
	public function pdf_bulanan_tkk()
	{
		$this->load->library('pdf');
		$pdf = $this->pdf->load();
		$nik = $this->uri->segment(3);
		$bulan = $this->uri->segment(4);
		$tahun = $this->uri->segment(5);
		$sc = "select a.*,
					if(a.jenis_tidak_masuk != 0,(select jenis_ketidakhadiran from referensi_persentase_ketidakhadiran where id = a.jenis_tidak_masuk),'-') as ketidakhadiran
				from d_absen_harian_tkk a
				where nik = '$nik' 
				and bulan = '$bulan' 
				and tahun = '$tahun' ";
		$qc = $this->db->query($sc);
		if($qc->num_rows() > 0)
		{
			$sql = "SELECT DISTINCT a.*,(select nunker from referensi_unit_kerja where kunker = concat(kuntp,kunkom,kununit,rincunit1,rincunit2,rincunit3)) as skpd
			FROM d_tkk a
			where nik = '$nik'  ";
			$data = $this->db->query($sql)->row_array();
			$nama_bulan = array(
				'1' => 'Januari',
				'2' => 'Februari',
				'3' => 'Maret',
				'4' => 'April',
				'5' => 'Mei',
				'6' => 'Juni',
				'7' => 'Juli',
				'8' => 'Agustus',
				'9' => 'September',
				'10' => 'Oktober',
				'11' => 'November',
				'12' => 'Desember',
			);
			$data = array(
				'title' => 'Daftar Hadir Bulanan',
				'subtitle' => '',
				'rows' => $qc,
				'nik' => $nik,
				'bulan' => $bulan,
				'tahun' => $tahun,
				'data' => $data,
				'nama_bulan' => $nama_bulan
			);
			$html = $this->load->view('pdf_bulanan_tkk',$data,true);
			input_log($this->user_id,'Cetak Data Absensi Bulanan tkk ke dalam bentuk pdf dengan nik:'.$nik.' bulan:'.$bulan.' tahun:'.$tahun);
			$pdf->WriteHTML($html);
			$pdf->Output();
		}
	}
	
	public function manual()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		
		$data = array(
				'title' => 'Data Absensi Manual',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'sssubunit' => $this->session->userdata('sssubunit_manual'),
				'jnsjab' => $this->session->userdata('jnsjab_manual'),
				'keselon' => $this->session->userdata('keselon_manual'),
				'nama' => $this->session->userdata('nama_manual'),
				'reqnip' => $this->session->userdata('reqnip_manual'),
		);
		$this->load->view('manual',$data);
		input_log($this->user_id,'Lihat konfirmasi manual pns');
	}
	
	public function get_manual()
	{
		//print_r($_REQUEST);
		$order = $_REQUEST['order'][0];
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/log/manual');
		//$aColumns = array( 'nip', 'nip','nama', 'nip','nip');
		$sssubunit = $this->input->post('sssubunit');
		$this->session->set_userdata('sssubunit_manual',$sssubunit);
		$jnsjab = $this->input->post('jnsjab');
		$this->session->set_userdata('jnsjab_manual',$jnsjab);
		$keselon = $this->input->post('keselon');
		$this->session->set_userdata('keselon_manual',$keselon);
		$nama = $this->input->post('nama');
		$this->session->set_userdata('nama_manual',$nama);
		$reqnip = $this->input->post('reqnip');
		$this->session->set_userdata('reqnip_manual',$reqnip);
		
		$sSearch = '';
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		if(isset($select) && $select != '')
		{
			if(!empty($s_biro)) 
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
			}
			else
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			}
		}
		
		if(isset($nama) && $nama != '')
		{
			$sSearch .= " and (nama like '%".$nama."%') ";
		}
		if(isset($reqnip) && $reqnip != '')
		{
			$sSearch .= " and (b.nip = '".$reqnip."') ";
		}
		if(isset($jnsjab) && $jnsjab != '0')
		{
			$sSearch .= " and (jnsjab = '".$jnsjab."') ";
		}
		if(isset($keselon) && $keselon != '0')
		{
			$sSearch .= " and (keselon = '".$keselon."') ";
		}
			
		//limit
		if ( isset( $_REQUEST['start'] ) && $_REQUEST['length'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_REQUEST['start'] ).", ".
				intval( $_REQUEST['length'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "`".$aColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]."` ".
						($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		//$sOrder = " order by ".$aColumns[$order['column']]." ".$order['dir']." ";
		$s0 = "select count(a.nip) as jml from d_ketidakhadiran b
		LEFT JOIN d_pegawai a on(a.nip = b.nip)
				where 1 = 1
				";
		$s0 .= $sSearch;
		
		$sheet_total = $this->db->query($s0)->row();
		$sheet_total = $sheet_total->jml;
	
		$s1 = "SELECT DISTINCT a.nama,
									a.keselon,
									a.kgolru,
									a.kuntp,
									a.kunkom,
									a.kununit,
									a.tanggal_lahir,
									a.jabatan,
									(select neselon from referensi_eselon where keselon = a.keselon) as eselon,
									(select ngolru from referensi_golongan_ruang where KGOLRU = a.KGOLRU) as golongan,
									(select pangkat from referensi_golongan_ruang where KGOLRU = a.KGOLRU) as pangkat,
									b.*,
									(select jenis_ketidakhadiran from referensi_persentase_ketidakhadiran where id = b.jenis_ketidakhadiran) as ketidakhadiran
			FROM d_ketidakhadiran b
			LEFT JOIN d_pegawai a on(b.nip = a.nip)
			WHERE 1=1";
		$s1 .= $sSearch;
		$s1 .= " order by id desc ";
		
		$s1 .= $sLimit;
	//	echo $s1;
		$sheet1 = $this->db->query($s1);
		$result = array();
		$i=0;
		$no=1;
		foreach($sheet1->result() as $sheet)
		{
			
			$result[$i]['0'] = $no;
			$result[$i]['1'] = $sheet->nip.' </br> '.$sheet->nama.' </br> '.$sheet->golongan;
			$result[$i]['2'] = $sheet->jabatan.' </br> '.$sheet->eselon;
			$result[$i]['3'] = $sheet->ketidakhadiran;
			$result[$i]['4'] = date('d-M-Y',strtotime($sheet->tanggal_mulai));
			$result[$i]['5'] = date('d-M-Y',strtotime($sheet->tanggal_selesai));
			$result[$i]['6'] = 	$sheet->keterangan;	
			$result[$i]['7'] = '<a target="_blank" href="/log/view_bukti/'.$sheet->id.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="View">
									<i class="glyphicon glyphicon-search"></i>
								</a>';
			if($hak_akses['up'] == 1)
			{
				$result[$i]['8'] = '<a href="/log/edit_manual/'.$sheet->id.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Edit">
									<i class="glyphicon glyphicon-edit"></i>
								</a>';
			}
			else
			{
				$result[$i]['8'] = '';
			}
			if($hak_akses['de'] == 1)
			{
				$result[$i]['9'] = '<a id="del_'.$no.'" href="/log/hapus_manual/'.$sheet->id.'" class="btn btn-danger del" data-toggle="tooltip" data-placement="top" title="Hapus">
								<i class="glyphicon glyphicon-trash"></i>
								</a>
								<script>
								$(document).ready(function () {
									$("#del_'.$no.'").click(function(){
										if (!confirm("Do you want to delete")){
										  return false;
										}
									});
								});
								</script>
								';
			}
			else
			{
				$result[$i]['9'] = '';
			}
			
			$no++;
			$i++;
		}
		
		$data = $result;
		$results = array(
			"iTotalRecords" => ($sheet_total),
			"iTotalDisplayRecords" => ($sheet_total),
			"aaData"=>$data);
			
		echo json_encode($results);
	}
	
	public function tambah_manual()
	{
		$this->load->library('form_validation');
			$this->form_validation->set_rules('nip', 'NIP', 'trim|required');
			$this->form_validation->set_rules('tanggal_mulai', 'Tanggal Mulai', 'trim|required');
			$this->form_validation->set_rules('tanggal_selesai', 'Tanggal Selesai', 'trim|required');
			
			if ($this->form_validation->run() === TRUE)
			{
				$posts = $this->input->post();
				print_r($posts);
				echo '</br>';
				$nip = $this->db->escape_str($posts['nip']);
				$jenis_ketidakhadiran = $this->db->escape_str($posts['jenis_ketidakhadiran']);
				$tanggal_mulai = $this->db->escape_str($posts['tanggal_mulai']);
				$tanggal_mulai = date('Y-m-d',strtotime($tanggal_mulai));
				$tanggal_selesai = $this->db->escape_str($posts['tanggal_selesai']);
				$tanggal_selesai = date('Y-m-d',strtotime($tanggal_selesai));
				$keterangan = $this->db->escape_str($posts['keterangan']);	
				$added_date = date('Y-m-d H:i:s');
				$added_by = $this->userid;
				
				$sql = 	"insert into d_ketidakhadiran 
							SET nip = '$nip', 
								jenis_ketidakhadiran = '$jenis_ketidakhadiran',
								tanggal_mulai = '$tanggal_mulai',
								tanggal_selesai = '$tanggal_selesai',
								keterangan = '$keterangan',
								added_date = '$added_date',
								added_by = '$added_by'
								";
				echo $sql.'</br>';
				$this->db->query($sql);	
				$id = $this->db->insert_id();
				//upload foto
				//$config['upload_path'] = 'assets/foto/';
				$path = 'assets/bukti/';
				$config['upload_path'] = $path;
				$config['allowed_types'] = 'gif|jpg|jpeg|png';
				$this->load->library('upload', $config);
				$field_name = 'userfile';
				if ($this->upload->do_upload($field_name))
				{
					$data = array('upload_data' => $this->upload->data());
					$file_1 = $path.$id.$data['upload_data']['file_ext'];
					if (file_exists($file_1)) {unlink($file_1);}       
					$org_1 = $path.$data['upload_data']['file_name'];
					rename($org_1, $file_1);	
					$file_gambar = $id.$data['upload_data']['file_ext'];
				}
				else
				{
					$this->session->set_flashdata('message', $this->upload->display_errors());	
					$file_gambar = '';
				}	
				
				$sql = "update d_ketidakhadiran
						set bukti = '$file_gambar'
						where id = '$id'
				";
				$this->db->query($sql);				
				$this->update_absen($id);	
				input_log($this->user_id,'Tambah konfirmasi manual pns dengan id:'.$id);				
				redirect('/log/manual');
			}
			else
			{
				$this->data['nip'] = array(
						'name'  => 'nip',
						'id'    => 'nip',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('nip'),
						'class'  => 'form-control',
					);
				$this->data['tanggal_mulai'] = array(
						'name'  => 'tanggal_mulai',
						'id'    => 'tanggal_mulai',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('tanggal_mulai'),
						'class'  => 'form-control datepicker',
					);
				$this->data['tanggal_selesai'] = array(
						'name'  => 'tanggal_selesai',
						'id'    => 'tanggal_selesai',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('tanggal_selesai'),
						'class'  => 'form-control datepicker',
					);
					$this->data['keterangan'] = array(
						'name'  => 'keterangan',
						'id'    => 'keterangan',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('keterangan'),
						'class'  => 'form-control',
					);
								
					
					$this->data['jenis_ketidakhadiran'] = array(
					'name'  => 'jenis_ketidakhadiran',
					'id'    => 'jenis_ketidakhadiran',
					'class'  => 'form-control',
					);
					$s = "select * from referensi_persentase_ketidakhadiran where 1 = 1";
					$q = $this->db->query($s);
					$options_jenis_ketidakhadiran[''] = '----------';
					foreach($q->result_array() as $rq)
					{
						$options_jenis_ketidakhadiran[($rq['id'])] = $rq['jenis_ketidakhadiran'];
					}
					
					$this->data['options_jenis_ketidakhadiran'] = $options_jenis_ketidakhadiran;
					$this->data['selected_ketidakhadiran'] = $this->form_validation->set_value('jenis_ketidakhadiran');
														
					$this->data['title'] = 'Data Kehadiran Manual';
					$this->data['subtitle'] = 'Form Penambahan Data Kehadiran Manual';
					$this->data['error'] = '';
					$this->load->view('form_manual',$this->data);
			}
			
	}
	
	public function edit_manual()
	{
		$id = $this->uri->segment(3);
		$s = "select a.*,
					b.nama as nama_lengkap,
					(select ngolru from referensi_golongan_ruang where kgolru = b.kgolru) as golongan,
					b.jabatan,
					CASE KUNKOM
								   WHEN 10 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' 	)
								   WHEN 11 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' 	) 
								   ELSE (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,4) = concat(kuntp,kunkom) and mid(kunker,5,8) = '00000000' 	)
					END as skpd
				from d_ketidakhadiran a
				left join d_pegawai b on(a.nip = b.nip)
				where a.id = '$id' ";
				//echo $s;
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			$this->load->library('form_validation');
			//$this->form_validation->set_rules('nip', 'NIP', 'trim|required');
			$this->form_validation->set_rules('tanggal_mulai', 'Tanggal Mulai', 'trim|required');
			$this->form_validation->set_rules('tanggal_selesai', 'Tanggal Selesai', 'trim|required');
			
			if ($this->form_validation->run() === TRUE)
			{
				
				$posts = $this->input->post();
				print_r($posts);
				echo '</br>';
				//$nip = $this->db->escape_str($posts['nip']);
				$jenis_ketidakhadiran = $this->db->escape_str($posts['jenis_ketidakhadiran']);
				$tanggal_mulai = $this->db->escape_str($posts['tanggal_mulai']);
				$tanggal_mulai = date('Y-m-d',strtotime($tanggal_mulai));
				$tanggal_selesai = $this->db->escape_str($posts['tanggal_selesai']);
				$tanggal_selesai = date('Y-m-d',strtotime($tanggal_selesai));
				$keterangan = $this->db->escape_str($posts['keterangan']);	
				$update_date = date('Y-m-d H:i:s');
				$update_by = $this->userid;
				
				$sql = 	"update d_ketidakhadiran 
							SET jenis_ketidakhadiran = '$jenis_ketidakhadiran',
								tanggal_mulai = '$tanggal_mulai',
								tanggal_selesai = '$tanggal_selesai',
								keterangan = '$keterangan',
								update_date = '$update_date',
								update_by = '$update_by'
							where id = '$id'
								";
				echo $sql.'</br>';
				$this->db->query($sql);	
				//upload foto
				//$config['upload_path'] = 'assets/foto/';
				$path = 'assets/bukti/';
				$config['upload_path'] = $path;
				$config['allowed_types'] = 'gif|jpg|jpeg|png';
				$this->load->library('upload', $config);
				$field_name = 'userfile';
				if ($this->upload->do_upload($field_name))
				{
					$data = array('upload_data' => $this->upload->data());
					$file_1 = $path.$id.$data['upload_data']['file_ext'];
					if (file_exists($file_1)) {unlink($file_1);}       
					$org_1 = $path.$data['upload_data']['file_name'];
					rename($org_1, $file_1);	
					$file_gambar = $id.$data['upload_data']['file_ext'];
				}
				else
				{
					$this->session->set_flashdata('message', $this->upload->display_errors());	
					$file_gambar = $r->bukti;
				}	
				
				$sql = "update d_ketidakhadiran
						set bukti = '$file_gambar'
						where id = '$id'
				";
				$this->db->query($sql);				
				$this->update_absen($id);		
				input_log($this->user_id,'Edit konfirmasi manual pns dengan id:'.$id);
				redirect('/log/manual');
			}
			else
			{
				$this->data['tanggal_mulai'] = array(
						'name'  => 'tanggal_mulai',
						'id'    => 'tanggal_mulai',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('tanggal_mulai',(isset($r->tanggal_mulai)?date('d-m-Y',strtotime($r->tanggal_mulai)):'')),
						'class'  => 'form-control datepicker',
					);
				$this->data['tanggal_selesai'] = array(
						'name'  => 'tanggal_selesai',
						'id'    => 'tanggal_selesai',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('tanggal_selesai',(isset($r->tanggal_selesai)?date('d-m-Y',strtotime($r->tanggal_selesai)):'')),
						'class'  => 'form-control datepicker',
					);
					$this->data['keterangan'] = array(
						'name'  => 'keterangan',
						'id'    => 'keterangan',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('keterangan',$r->keterangan),
						'class'  => 'form-control',
					);
								
					
					$this->data['jenis_ketidakhadiran'] = array(
					'name'  => 'jenis_ketidakhadiran',
					'id'    => 'jenis_ketidakhadiran',
					'class'  => 'form-control',
					);
					$s = "select * from referensi_persentase_ketidakhadiran where 1 = 1";
					$q = $this->db->query($s);
					$options_jenis_ketidakhadiran[''] = '----------';
					foreach($q->result_array() as $rq)
					{
						$options_jenis_ketidakhadiran[($rq['id'])] = $rq['jenis_ketidakhadiran'];
					}
					
					$this->data['options_jenis_ketidakhadiran'] = $options_jenis_ketidakhadiran;
					$this->data['selected_ketidakhadiran'] = $this->form_validation->set_value('jenis_ketidakhadiran',$r->jenis_ketidakhadiran);
														
					$this->data['title'] = 'Data Kehadiran Manual';
					$this->data['subtitle'] = 'Form Penambahan Data Kehadiran Manual';
					$this->data['error'] = '';
					$this->data['r'] = $r;
					$this->load->view('form_edit_manual',$this->data);
			}
		}
		else
		{
			redirect('/');
		}
			
	}
	
	public function hapus_manual()
	{
		$id = $this->uri->segment(3);
		$sc = "select bukti from d_ketidakhadiran where id = '$id' ";
		$q = $this->db->query($sc)->row_array();
		unlink('assets/bukti/'.$q['bukti']);		
		$s = "delete from d_ketidakhadiran where id = '$id' ";
		$this->db->query($s);
		input_log($this->user_id,'Hapus konfirmasi manual pns dengan id:'.$id);
		redirect('/log/manual');
	}
	
	public function view_bukti()
	{
		$id = $this->uri->segment(3);
		$s = "select bukti from d_ketidakhadiran where id = '$id' ";
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			if($r->bukti != '' and !empty($r->bukti) and !is_null($r->bukti))
			{
				echo '<img src="/assets/bukti/'.$r->bukti.'">';
			}
			else
			{
				echo '<img src="/assets/bukti/noimage.png">';
			}
			
		}
		else
		{
			echo '<img src="/assets/bukti/noimage.png">';
		}
	}
	
	public function manual_tkk()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		
		$data = array(
				'title' => 'Data Absensi Manual TKK',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'sssubunit' => $this->session->userdata('sssubunit_manual_tkk'),
				'jnsjab' => $this->session->userdata('jnsjab_manual_tkk'),
				'keselon' => $this->session->userdata('keselon_manual_tkk'),
				'nama' => $this->session->userdata('nama_manual_tkk'),
				'reqnip' => $this->session->userdata('reqnip_manual_tkk'),
		);
		$this->load->view('manual_tkk',$data);
		input_log($this->user_id,'Lihat konfirmasi manual tkk');
	}
	
	public function get_manual_tkk()
	{
		//print_r($_REQUEST);
		$order = $_REQUEST['order'][0];
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/log/manual_tkk');
		//$aColumns = array( 'nip', 'nip','nama', 'nip','nip');
		$sssubunit = $this->input->post('sssubunit');
		$this->session->set_userdata('sssubunit_manual_tkk',$sssubunit);
		$nama = $this->input->post('nama');
		$this->session->set_userdata('nama_manual_tkk',$nama);
		$nik = $this->input->post('nik');
		$this->session->set_userdata('nik_manual_tkk',$nik);
		
		$sSearch = '';
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		if(isset($select) && $select != '')
		{
			if(!empty($s_biro)) 
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
			}
			else
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			}
		}
		
		if(isset($nama) && $nama != '')
		{
			$sSearch .= " and (nama like '%".$nama."%') ";
		}
		if(isset($nik) && $nik != '')
		{
			$sSearch .= " and (b.nik = '".$nik."') ";
		}
			
		//limit
		if ( isset( $_REQUEST['start'] ) && $_REQUEST['length'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_REQUEST['start'] ).", ".
				intval( $_REQUEST['length'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "`".$aColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]."` ".
						($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		//$sOrder = " order by ".$aColumns[$order['column']]." ".$order['dir']." ";
		$s0 = "select count(a.nik) as jml from d_ketidakhadiran_tkk b
		LEFT JOIN d_tkk a on(a.nik = b.nik)
				where 1 = 1
				";
		$s0 .= $sSearch;
		
		$sheet_total = $this->db->query($s0)->row();
		$sheet_total = $sheet_total->jml;
	
		$s1 = "SELECT DISTINCT a.nik,a.nama,									
									b.*,
									concat(kuntp,kunkom,kununit,rincunit1,rincunit2,rincunit3) as kunker,
									(select jenis_ketidakhadiran from referensi_persentase_ketidakhadiran where id = b.jenis_ketidakhadiran) as ketidakhadiran
			FROM d_ketidakhadiran_tkk b
			LEFT JOIN d_tkk a on(b.nik = a.nik)
			WHERE 1=1";
		$s1 .= $sSearch;
		$s1 .= " order by b.id desc ";
		
		$s1 .= $sLimit;
	//	echo $s1;
		$sheet1 = $this->db->query($s1);
		$result = array();
		$i=0;
		$no=1;
		foreach($sheet1->result() as $sheet)
		{
			
			$result[$i]['0'] = $no;
			$result[$i]['1'] = $sheet->nik.' </br> '.$sheet->nama;
			$result[$i]['2'] = $this->get_skpd($sheet->kunker);
			$result[$i]['3'] = $sheet->ketidakhadiran;
			$result[$i]['4'] = date('d-M-Y',strtotime($sheet->tanggal_mulai));
			$result[$i]['5'] = date('d-M-Y',strtotime($sheet->tanggal_selesai));
			$result[$i]['6'] = 	$sheet->keterangan;	
			$result[$i]['7'] = '<a target="_blank" href="/log/view_bukti_tkk/'.$sheet->id.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="View">
									<i class="glyphicon glyphicon-search"></i>
								</a>';
			if($hak_akses['up'] == 1)
			{
				$result[$i]['8'] = '<a href="/log/edit_manual_tkk/'.$sheet->id.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Edit">
									<i class="glyphicon glyphicon-edit"></i>
								</a>';
			}
			else
			{
				$result[$i]['8'] = '';
			}
			if($hak_akses['de'] == 1)
			{
				$result[$i]['9'] = '<a id="del_'.$no.'" href="/log/hapus_manual_tkk/'.$sheet->id.'" class="btn btn-danger del" data-toggle="tooltip" data-placement="top" title="Hapus">
								<i class="glyphicon glyphicon-trash"></i>
								</a>
								<script>
								$(document).ready(function () {
									$("#del_'.$no.'").click(function(){
										if (!confirm("Do you want to delete")){
										  return false;
										}
									});
								});
								</script>
								';
			}
			else
			{
				$result[$i]['9'] = '';
			}
			
			$no++;
			$i++;
		}
		
		$data = $result;
		$results = array(
			"iTotalRecords" => ($sheet_total),
			"iTotalDisplayRecords" => ($sheet_total),
			"aaData"=>$data);
			
		echo json_encode($results);
	}
	
	public function tambah_manual_tkk()
	{
		$this->load->library('form_validation');
			$this->form_validation->set_rules('nik', 'NIK', 'trim|required');
			$this->form_validation->set_rules('tanggal_mulai', 'Tanggal Mulai', 'trim|required');
			$this->form_validation->set_rules('tanggal_selesai', 'Tanggal Selesai', 'trim|required');
			
			if ($this->form_validation->run() === TRUE)
			{
				$posts = $this->input->post();
				print_r($posts);
				echo '</br>';
				$nik = $this->db->escape_str($posts['nik']);
				$jenis_ketidakhadiran = $this->db->escape_str($posts['jenis_ketidakhadiran']);
				$tanggal_mulai = $this->db->escape_str($posts['tanggal_mulai']);
				$tanggal_mulai = date('Y-m-d',strtotime($tanggal_mulai));
				$tanggal_selesai = $this->db->escape_str($posts['tanggal_selesai']);
				$tanggal_selesai = date('Y-m-d',strtotime($tanggal_selesai));
				$keterangan = $this->db->escape_str($posts['keterangan']);	
				$added_date = date('Y-m-d H:i:s');
				$added_by = $this->userid;
				
				$sql = 	"insert into d_ketidakhadiran_tkk 
							SET nik = '$nik', 
								jenis_ketidakhadiran = '$jenis_ketidakhadiran',
								tanggal_mulai = '$tanggal_mulai',
								tanggal_selesai = '$tanggal_selesai',
								keterangan = '$keterangan',
								added_date = '$added_date',
								added_by = '$added_by'
								";
				echo $sql.'</br>';
				$this->db->query($sql);	
				$id = $this->db->insert_id();
				//upload foto
				//$config['upload_path'] = 'assets/foto/';
				$path = 'assets/bukti_tkk/';
				$config['upload_path'] = $path;
				$config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
				$this->load->library('upload', $config);
				$field_name = 'userfile';
				if ($this->upload->do_upload($field_name))
				{
					$data = array('upload_data' => $this->upload->data());
					$file_1 = $path.$id.$data['upload_data']['file_ext'];
					if (file_exists($file_1)) {unlink($file_1);}       
					$org_1 = $path.$data['upload_data']['file_name'];
					rename($org_1, $file_1);	
					$file_gambar = $id.$data['upload_data']['file_ext'];
				}
				else
				{
					$this->session->set_flashdata('message', $this->upload->display_errors());	
					$file_gambar = '';
				}	
				
				$sql = "update d_ketidakhadiran_tkk
						set bukti = '$file_gambar'
						where id = '$id'
				";
				$this->db->query($sql);				
				$this->update_absen_tkk($id);	
				input_log($this->user_id,'tambah konfirmasi manual tkk dengan id:'.$id);
				redirect('/log/manual_tkk');
			}
			else
			{
				$this->data['nik'] = array(
						'name'  => 'nik',
						'id'    => 'nik',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('nik'),
						'class'  => 'form-control',
					);
				$this->data['tanggal_mulai'] = array(
						'name'  => 'tanggal_mulai',
						'id'    => 'tanggal_mulai',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('tanggal_mulai'),
						'class'  => 'form-control datepicker',
					);
				$this->data['tanggal_selesai'] = array(
						'name'  => 'tanggal_selesai',
						'id'    => 'tanggal_selesai',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('tanggal_selesai'),
						'class'  => 'form-control datepicker',
					);
					$this->data['keterangan'] = array(
						'name'  => 'keterangan',
						'id'    => 'keterangan',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('keterangan'),
						'class'  => 'form-control',
					);
								
					
					$this->data['jenis_ketidakhadiran'] = array(
					'name'  => 'jenis_ketidakhadiran',
					'id'    => 'jenis_ketidakhadiran',
					'class'  => 'form-control',
					);
					$s = "select * from referensi_persentase_ketidakhadiran where 1 = 1";
					$q = $this->db->query($s);
					$options_jenis_ketidakhadiran[''] = '----------';
					foreach($q->result_array() as $rq)
					{
						$options_jenis_ketidakhadiran[($rq['id'])] = $rq['jenis_ketidakhadiran'];
					}
					
					$this->data['options_jenis_ketidakhadiran'] = $options_jenis_ketidakhadiran;
					$this->data['selected_ketidakhadiran'] = $this->form_validation->set_value('jenis_ketidakhadiran');
														
					$this->data['title'] = 'Data Kehadiran Manual TKK';
					$this->data['subtitle'] = 'Form Penambahan Data Kehadiran Manual TKK';
					$this->data['error'] = '';
					$this->load->view('form_manual_tkk',$this->data);
			}
			
	}
	
	public function edit_manual_tkk()
	{
		$id = $this->uri->segment(3);
		$s = "select a.*,
					b.nama as nama_lengkap,
					CASE KUNKOM
								   WHEN 10 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' 	)
								   WHEN 11 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' 	) 
								   ELSE (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,4) = concat(kuntp,kunkom) and mid(kunker,5,8) = '00000000' 	)
					END as skpd
				from d_ketidakhadiran_tkk a
				left join d_tkk b on(a.nik = b.nik)
				where a.id = '$id' ";
				//echo $s;
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			$this->load->library('form_validation');
			//$this->form_validation->set_rules('nip', 'NIP', 'trim|required');
			$this->form_validation->set_rules('tanggal_mulai', 'Tanggal Mulai', 'trim|required');
			$this->form_validation->set_rules('tanggal_selesai', 'Tanggal Selesai', 'trim|required');
			
			if ($this->form_validation->run() === TRUE)
			{
				$posts = $this->input->post();
				print_r($posts);
				echo '</br>';
				//$nip = $this->db->escape_str($posts['nip']);
				$jenis_ketidakhadiran = $this->db->escape_str($posts['jenis_ketidakhadiran']);
				$tanggal_mulai = $this->db->escape_str($posts['tanggal_mulai']);
				$tanggal_mulai = date('Y-m-d',strtotime($tanggal_mulai));
				$tanggal_selesai = $this->db->escape_str($posts['tanggal_selesai']);
				$tanggal_selesai = date('Y-m-d',strtotime($tanggal_selesai));
				$keterangan = $this->db->escape_str($posts['keterangan']);	
				$update_date = date('Y-m-d H:i:s');
				$update_by = $this->userid;
				
				$sql = 	"update d_ketidakhadiran_tkk 
							SET jenis_ketidakhadiran = '$jenis_ketidakhadiran',
								tanggal_mulai = '$tanggal_mulai',
								tanggal_selesai = '$tanggal_selesai',
								keterangan = '$keterangan',
								update_date = '$update_date',
								update_by = '$update_by'
							where id = '$id'
								";
				echo $sql.'</br>';
				$this->db->query($sql);	
				//upload foto
				//$config['upload_path'] = 'assets/foto/';
				$path = 'assets/bukti_tkk/';
				$config['upload_path'] = $path;
				$config['allowed_types'] = 'gif|jpg|jpeg|png';
				$this->load->library('upload', $config);
				$field_name = 'userfile';
				if ($this->upload->do_upload($field_name))
				{
					$data = array('upload_data' => $this->upload->data());
					$file_1 = $path.$id.$data['upload_data']['file_ext'];
					if (file_exists($file_1)) {unlink($file_1);}       
					$org_1 = $path.$data['upload_data']['file_name'];
					rename($org_1, $file_1);	
					$file_gambar = $id.$data['upload_data']['file_ext'];
				}
				else
				{
					$this->session->set_flashdata('message', $this->upload->display_errors());	
					$file_gambar = $r->bukti;
				}	
				
				$sql = "update d_ketidakhadiran_tkk
						set bukti = '$file_gambar'
						where id = '$id'
				";
				$this->db->query($sql);				
				$this->update_absen_tkk($id);	
				input_log($this->user_id,'edit konfirmasi manual tkk dengan id:'.$id);
				redirect('/log/manual_tkk');
			}
			else
			{
				$this->data['tanggal_mulai'] = array(
						'name'  => 'tanggal_mulai',
						'id'    => 'tanggal_mulai',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('tanggal_mulai',(isset($r->tanggal_mulai)?date('d-m-Y',strtotime($r->tanggal_mulai)):'')),
						'class'  => 'form-control datepicker',
					);
				$this->data['tanggal_selesai'] = array(
						'name'  => 'tanggal_selesai',
						'id'    => 'tanggal_selesai',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('tanggal_selesai',(isset($r->tanggal_selesai)?date('d-m-Y',strtotime($r->tanggal_selesai)):'')),
						'class'  => 'form-control datepicker',
					);
					$this->data['keterangan'] = array(
						'name'  => 'keterangan',
						'id'    => 'keterangan',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('keterangan',$r->keterangan),
						'class'  => 'form-control',
					);
								
					
					$this->data['jenis_ketidakhadiran'] = array(
					'name'  => 'jenis_ketidakhadiran',
					'id'    => 'jenis_ketidakhadiran',
					'class'  => 'form-control',
					);
					$s = "select * from referensi_persentase_ketidakhadiran where 1 = 1";
					$q = $this->db->query($s);
					$options_jenis_ketidakhadiran[''] = '----------';
					foreach($q->result_array() as $rq)
					{
						$options_jenis_ketidakhadiran[($rq['id'])] = $rq['jenis_ketidakhadiran'];
					}
					
					$this->data['options_jenis_ketidakhadiran'] = $options_jenis_ketidakhadiran;
					$this->data['selected_ketidakhadiran'] = $this->form_validation->set_value('jenis_ketidakhadiran',$r->jenis_ketidakhadiran);
														
					$this->data['title'] = 'Data Kehadiran Manual';
					$this->data['subtitle'] = 'Form Penambahan Data Kehadiran Manual';
					$this->data['error'] = '';
					$this->data['r'] = $r;
					$this->load->view('form_edit_manual_tkk',$this->data);
			}
		}
		else
		{
			redirect('/');
		}
			
	}
	
	public function hapus_manual_tkk()
	{
		$id = $this->uri->segment(3);
		$sc = "select bukti from d_ketidakhadiran_tkk where id = '$id' ";
		$q = $this->db->query($sc)->row_array();
		unlink('assets/bukti_tkk/'.$q['bukti']);		
		$s = "delete from d_ketidakhadiran_tkk where id = '$id' ";
		$this->db->query($s);
		input_log($this->user_id,'hapus konfirmasi manual tkk dengan id:'.$id);
		redirect('/log/manual_tkk');
	}
	
	public function view_bukti_tkk()
	{
		$id = $this->uri->segment(3);
		$s = "select bukti from d_ketidakhadiran_tkk where id = '$id' ";
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			if($r->bukti != '' and !empty($r->bukti) and !is_null($r->bukti))
			{
				echo '<img src="/assets/bukti_tkk/'.$r->bukti.'">';
			}
			else
			{
				echo '<img src="/assets/bukti_tkk/noimage.png">';
			}
			
		}
		else
		{
			echo '<img src="/assets/bukti/noimage.png">';
		}
	}
	
	public function cari_nik()
	{
		$where = "";
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		$q = $this->db->escape_str($_REQUEST['q']);
		$s = "select a.nik,a.nama,
					CONCAT(a.nik,
							'-',
							a.nama,
							'-',
							CASE KUNKOM
								   WHEN 10 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' 	)
								   WHEN 11 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' 	) 
								   ELSE (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,4) = concat(kuntp,kunkom) and mid(kunker,5,8) = '00000000' 	)
							END
							)  AS niknama
				FROM d_tkk a
				where 1=1
				";
		if(!empty($s_biro)) 
		{
			$select = $s_biro;
			if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$where = " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$where = " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$where = " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$where = " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$where = " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
		}
		$s .= $where;
		$s .=" and (a.nama like '%".$q."%' OR a.nik like '%".$q."%' )";
		//echo $s;
		$q = $this->db->query($s);
		$res = array();
		foreach($q->result_array() as $r)
		{
			$res[] = $r;
		}
		echo json_encode($res);
	}
	
	public function view_identitas()
	{
		$biro = substr($this->s_biro,0,4);
		$NIP = $_REQUEST['NIP'];
		$sql_id = "select a.nip,
							a.nama,
							(select ngolru from referensi_golongan_ruang where kgolru = a.kgolru) as golongan,
							a.jabatan,
								CASE KUNKOM
								   WHEN 10 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' 	)
								   WHEN 11 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' 	) 
								   ELSE (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,4) = concat(kuntp,kunkom) and mid(kunker,5,8) = '00000000' 	)
								END as skpd
							from d_pegawai a 
							WHERE a.NIP = '$NIP'
							
							";
		if(isset($biro) && $biro!='')
		{
			$sql_id .=" and concat(kuntp,kunkom) = '$biro' ";
		}
		//echo $sql_id.'</br>';
		$query_id = $this->db->query($sql_id);
		$result = '';
		if($query_id->num_rows() > 0)
		{
			$row = $query_id->row_array();
			$result .='<div class="row">
								<div class="col-xs-4"><label>Nama</label></div>
								<div class="col-xs-8">
									<a>'.$row['nama'].'</a>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-4"><label>Golongan</label></div>
								<div class="col-xs-8">
									<a>'.$row['golongan'].'</a>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-4"><label>Jabatan</label></div>
								<div class="col-xs-8">
									<a>'.$row['jabatan'].'</a>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-4"><label>SKPD</label></div>
								<div class="col-xs-8">
									<a>'.$row['skpd'].'</a>
								</div>
							</div>
							
							';
							
		}
		else
		{
			$result .='<div class="row">
								<div class="col-xs-6"><label>Nama Lengkap</label></div>
								<div class="col-xs-6">
									
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6"><label>SKPD</label></div>
								<div class="col-xs-6">
									
								</div>
							</div>
							';
		}
		echo $result;
	}
	
	public function view_identitas_tkk()
	{
		$biro = substr($this->s_biro,0,4);
		$nik = $_REQUEST['nik'];
		$sql_id = "select a.nik,
							a.nama,
								CASE KUNKOM
								   WHEN 10 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' 	)
								   WHEN 11 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' 	) 
								   ELSE (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,4) = concat(kuntp,kunkom) and mid(kunker,5,8) = '00000000' 	)
								END as skpd
							from d_tkk a 
							WHERE a.nik = '$nik'
							
							";
		if(isset($biro) && $biro!='')
		{
			$sql_id .=" and concat(kuntp,kunkom) = '$biro' ";
		}
		//echo $sql_id.'</br>';
		$query_id = $this->db->query($sql_id);
		$result = '';
		if($query_id->num_rows() > 0)
		{
			$row = $query_id->row_array();
			$result .='<div class="row">
								<div class="col-xs-4"><label>Nama</label></div>
								<div class="col-xs-8">
									<a>'.$row['nama'].'</a>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-4"><label>SKPD</label></div>
								<div class="col-xs-8">
									<a>'.$row['skpd'].'</a>
								</div>
							</div>
							
							';
							
		}
		else
		{
			$result .='<div class="row">
								<div class="col-xs-6"><label>Nama Lengkap</label></div>
								<div class="col-xs-6">
									
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6"><label>SKPD</label></div>
								<div class="col-xs-6">
									
								</div>
							</div>
							';
		}
		echo $result;
	}
	
	function update_absen($id)
	{
		$s = "select a.*,
					(select persentase from referensi_persentase_ketidakhadiran where id = a.jenis_ketidakhadiran) as persentase
				from d_ketidakhadiran a
				where a.id = '$id' ";
		$r = $this->db->query($s)->row();
		$nip = $r->nip;
		$tanggal_mulai = $r->tanggal_mulai;
		echo $tanggal_mulai.'</br>';
		$tanggal_selesai = $r->tanggal_selesai;
		echo $tanggal_selesai.'</br>';
		$jenis_ketidakhadiran = $r->jenis_ketidakhadiran;
		$persentase = $r->persentase;
		$selisih = (strtotime($tanggal_selesai) - strtotime($tanggal_mulai))/(60*60*24);
		if($selisih == 0)
		{
			$tgl = date('j',strtotime($tanggal_mulai));
			$bln = date('n',strtotime($tanggal_mulai));
			$thn = date('Y',strtotime($tanggal_mulai));
			$update_date = date('Y-m-d H:i:s');
			$libur = $this->cek_libur($tanggal_mulai);
			if($libur == 0)
			{
				$sc = "select id from d_absen_harian where nip = '$nip' and tanggal = '$tgl' and bulan = '$bln' and tahun = '$thn' ";
				$qc = $this->db->query($sc);
				if($qc->num_rows() > 0)
				{
					$id_absen = $qc->row()->id;
					$su = "update d_absen_harian 
							set jenis_tidak_masuk = '$jenis_ketidakhadiran',
								potongan_tidak_masuk = '$persentase',
								persentase_potongan = '$persentase',
								update_date = '$update_date',
								update_by = 'system'
							where id = '$id_absen'
							";
					$this->db->query($su);
				}
				else
				{
					$si = "insert into d_absen_harian
											set nip = '$nip',
												tanggal = '$tgl',
												bulan = '$bln',
												tahun = '$thn',
												jenis_tidak_masuk = '$jenis_ketidakhadiran',
												potongan_tidak_masuk = '$persentase',
												persentase_potongan = '$persentase',
												added_date = '$update_date',
												added_by = 'system'
											";
					echo $si.'</br>';
					$qi = $this->db->query($si);
				}
				
				$sc = "select id from d_rekap_keuangan_harian where nip = '$nip' and tanggal = '$tanggal_mulai' ";
				$qc = $this->db->query($sc);
				if($qc->num_rows() > 0)
				{
					$id_uang = $qc->row()->id;
					$tpp = $this->get_tpp($nip);
					$si = "update d_rekap_keuangan_harian 
							set total_tpp = '$tpp[total_tpp]',
								tpp_statis = '$tpp[tpp_statis]',
								tpp_dinamis = '$tpp[tpp_dinamis]',
								pemotongan_per_hari = '$persentase',
								update_date = '$update_date',	
								update_by = 'system'
							where id = '$id_uang'
					";
					$this->db->query($si);
				}
				else
				{
					$tpp = $this->get_tpp($nip);
					$si = "insert into d_rekap_keuangan_harian 
							set tanggal = '$tanggal_mulai',
								nip = '$nip',
								total_tpp = '$tpp[total_tpp]',
								tpp_statis = '$tpp[tpp_statis]',
								tpp_dinamis = '$tpp[tpp_dinamis]',
								pemotongan_per_hari = '$persentase',
								update_date = '$update_date',	
								update_by = 'system'					
					";
					$this->db->query($si);
				}
				
			}
			elseif($libur == 1)
			{
				$sc = "select id from d_absen_harian where nip = '$nip' and tanggal = '$tgl' and bulan = '$bln' and tahun = '$thn' ";
				$qc = $this->db->query($sc);
				if($qc->num_rows() > 0)
				{
					$id_absen = $qc->row()->id;
					$su = "update d_absen_harian 
							set libur = '1',
								update_date = '$update_date',
								update_by = 'system'
							where id = '$id_absen'
							";
					$this->db->query($su);
				}
				else
				{
					
					$si = "insert into d_absen_harian
											set nip = '$nip',
												tanggal = '$tgl',
												bulan = '$bln',
												tahun = '$thn',
												libur = '1',
												update_date = '$update_date',
												update_by = 'system'
											";
					echo $si.'</br>';
					$qi = $this->db->query($si);
				}
				
				$sc = "select id from d_rekap_keuangan_harian where nip = '$nip' and tanggal = '$tanggal_mulai' ";
				$qc = $this->db->query($sc);
				if($qc->num_rows() > 0)
				{
					$id_uang = $qc->row()->id;
					$tpp = $this->get_tpp($nip);
					$si = "update d_rekap_keuangan_harian 
							set total_tpp = '$tpp[total_tpp]',
								tpp_statis = '$tpp[tpp_statis]',
								tpp_dinamis = '$tpp[tpp_dinamis]',
								pemotongan_per_hari = '0',
								update_date = '$update_date',	
								update_by = 'system'
							where id = '$id_uang'
					";
					$this->db->query($si);
				}
				else
				{
					$tpp = $this->get_tpp($nip);
					$si = "insert into d_rekap_keuangan_harian 
							set tanggal = '$tanggal_mulai',
								nip = '$nip',
								total_tpp = '$tpp[total_tpp]',
								tpp_statis = '$tpp[tpp_statis]',
								tpp_dinamis = '$tpp[tpp_dinamis]',
								pemotongan_per_hari = '0',
								update_date = '$update_date',	
								update_by = 'system'					
					";
					$this->db->query($si);
				}
				
			}
			
		}
		else
		{
			for($i=0;$i<=$selisih;$i++)
			{
				$penambah = $i*60*60*24;
				$tanggal_time = strtotime($tanggal_mulai) + $penambah;
				$tanggal = date('Y-m-d',$tanggal_time);
				$tgl = date('j',$tanggal_time);
				$bln = date('n',$tanggal_time);
				$thn = date('Y',$tanggal_time);
				$update_date = date('Y-m-d H:i:s');
				$libur = $this->cek_libur($tanggal);
				if($libur == 0)
				{
					$sc = "select id from d_absen_harian where nip = '$nip' and tanggal = '$tgl' and bulan = '$bln' and tahun = '$thn' ";
					echo $sc.'</br>';
					$qc = $this->db->query($sc);
					if($qc->num_rows() > 0)
					{
						$id_absen = $qc->row()->id;
						$su = "update d_absen_harian 
								set jenis_tidak_masuk = '$jenis_ketidakhadiran',
									potongan_tidak_masuk = '$persentase',
									persentase_potongan = '$persentase',
									update_date = '$update_date',
									update_by = 'system'
								where id = '$id_absen'
								";
						//echo $su.'</br>';
						$this->db->query($su);
					}
					else
					{
						$si = "insert into d_absen_harian
												set nip = '$nip',
													tanggal = '$tgl',
													bulan = '$bln',
													tahun = '$thn',
													jenis_tidak_masuk = '$jenis_ketidakhadiran',
													potongan_tidak_masuk = '$persentase',
													persentase_potongan = '$persentase',
													added_date = '$update_date',
													added_by = 'system'
												";
						echo $si.'</br>';
						$qi = $this->db->query($si);
					}
					
					$sc = "select id from d_rekap_keuangan_harian where nip = '$nip' and tanggal = '$tanggal' ";
					$qc = $this->db->query($sc);
					if($qc->num_rows() > 0)
					{
						$id_uang = $qc->row()->id;
						$tpp = $this->get_tpp($nip);
						$si = "update d_rekap_keuangan_harian 
								set total_tpp = '$tpp[total_tpp]',
									tpp_statis = '$tpp[tpp_statis]',
									tpp_dinamis = '$tpp[tpp_dinamis]',
									pemotongan_per_hari = '$persentase',
									update_date = '$update_date',	
									update_by = 'system'
								where id = '$id_uang'
						";
						//echo $si.'</br>';
						$this->db->query($si);
					}
					else
					{
						$tpp = $this->get_tpp($nip);
						$si = "insert into d_rekap_keuangan_harian 
								set tanggal = '$tanggal',
									nip = '$nip',
									total_tpp = '$tpp[total_tpp]',
									tpp_statis = '$tpp[tpp_statis]',
									tpp_dinamis = '$tpp[tpp_dinamis]',
									pemotongan_per_hari = '$persentase',
									update_date = '$update_date',	
									update_by = 'system'					
						";
						$this->db->query($si);
					}
				
				}
				elseif($libur == 1)
				{
					$sc = "select id from d_absen_harian where nip = '$nip' and tanggal = '$tgl' and bulan = '$bln' and tahun = '$thn' ";
					echo $sc.'</br>';
					$qc = $this->db->query($sc);
					if($qc->num_rows() > 0)
					{
						$id_absen = $qc->row()->id;
						$su = "update d_absen_harian 
								set libur = '1',
									update_date = '$update_date',
									update_by = 'system'
								where id = '$id_absen'
								";
						//echo $su.'</br>';
						$this->db->query($su);
					}
					else
					{
						$si = "insert into d_absen_harian
												set nip = '$nip',
													tanggal = '$tgl',
													bulan = '$bln',
													tahun = '$thn',
													libur = '1',
													added_date = '$update_date',
													added_by = 'system'
												";
						echo $si.'</br>';
						$qi = $this->db->query($si);
					}
					
					$sc = "select id from d_rekap_keuangan_harian where nip = '$nip' and tanggal = '$tanggal' ";
					$qc = $this->db->query($sc);
					if($qc->num_rows() > 0)
					{
						$id_uang = $qc->row()->id;
						$tpp = $this->get_tpp($nip);
						$si = "update d_rekap_keuangan_harian 
								set total_tpp = '$tpp[total_tpp]',
									tpp_statis = '$tpp[tpp_statis]',
									tpp_dinamis = '$tpp[tpp_dinamis]',
									pemotongan_per_hari = '0',
									update_date = '$update_date',	
									update_by = 'system'
								where id = '$id_uang'
						";
						//echo $si.'</br>';
						$this->db->query($si);
					}
					else
					{
						$tpp = $this->get_tpp($nip);
						$si = "insert into d_rekap_keuangan_harian 
								set tanggal = '$tanggal',
									nip = '$nip',
									total_tpp = '$tpp[total_tpp]',
									tpp_statis = '$tpp[tpp_statis]',
									tpp_dinamis = '$tpp[tpp_dinamis]',
									pemotongan_per_hari = '0',
									update_date = '$update_date',	
									update_by = 'system'					
						";
						$this->db->query($si);
					}
				
				}
			}
		}
	}
	
	function update_absen_tkk($id)
	{
		$s = "select a.*,
					(select persentase from referensi_persentase_ketidakhadiran where id = a.jenis_ketidakhadiran) as persentase
				from d_ketidakhadiran_tkk a
				where a.id = '$id' ";
		$r = $this->db->query($s)->row();
		$nik = $r->nik;
		$tanggal_mulai = $r->tanggal_mulai;
		echo $tanggal_mulai.'</br>';
		$tanggal_selesai = $r->tanggal_selesai;
		echo $tanggal_selesai.'</br>';
		$jenis_ketidakhadiran = $r->jenis_ketidakhadiran;
		$persentase = $r->persentase;
		$selisih = (strtotime($tanggal_selesai) - strtotime($tanggal_mulai))/(60*60*24);
		if($selisih == 0)
		{
			$tgl = date('j',strtotime($tanggal_mulai));
			$bln = date('n',strtotime($tanggal_mulai));
			$thn = date('Y',strtotime($tanggal_mulai));
			$update_date = date('Y-m-d H:i:s');
			$libur = $this->cek_libur($tanggal_mulai);
			if($libur == 0)
			{
				$sc = "select id from d_absen_harian_tkk where nik = '$nik' and tanggal = '$tgl' and bulan = '$bln' and tahun = '$thn' ";
				$qc = $this->db->query($sc);
				if($qc->num_rows() > 0)
				{
					$id_absen = $qc->row()->id;
					$su = "update d_absen_harian_tkk 
							set jenis_tidak_masuk = '$jenis_ketidakhadiran',
								potongan_tidak_masuk = '$persentase',
								persentase_potongan = '$persentase',
								update_date = '$update_date',
								update_by = 'system'
							where id = '$id_absen'
							";
					$this->db->query($su);
				}
				else
				{
					$st = "select a.*,
								b.*
								from d_pegawai_tipe_jadwal_tkk a
								left join referensi_tipe_jadwal b on(a.tipe_jadwal = b.id)
								where nik = '$nik' 
								
					";
					$rst = $this->db->query($st)->row_array();
					
					$jadwal_jam_masuk = $rst['jadwal_jam_masuk'];
					$jadwal_menit_masuk = $rst['jadwal_menit_masuk'];
					$jadwal_masuk = date('Y-m-d H:i',strtotime($tgl.'-'.$bln.'-'.$thn.' '.$jadwal_jam_masuk.':'.$jadwal_menit_masuk));
					$jadwal_jam_keluar = $rst['jadwal_jam_keluar'];
					$jadwal_menit_keluar = $rst['jadwal_menit_keluar'];
					$jadwal_keluar = date('Y-m-d H:i',strtotime($tgl.'-'.$bln.'-'.$thn.' '.$jadwal_jam_keluar.':'.$jadwal_menit_keluar));		
					
					$si = "insert into d_absen_harian_tkk
											set nik = '$nik',
												tanggal = '$tgl',
												bulan = '$bln',
												tahun = '$thn',
												jadwal_masuk = '$jadwal_masuk',
												jadwal_keluar = '$jadwal_keluar', 
												jenis_tidak_masuk = '$jenis_ketidakhadiran',
												potongan_tidak_masuk = '$persentase',
												persentase_potongan = '$persentase',
												update_date = '$update_date',
												update_by = 'system'
											";
					echo $si.'</br>';
					$qi = $this->db->query($si);
				}
				
				$sc = "select id from d_rekap_keuangan_harian_tkk where nik = '$nik' and tanggal = '$tanggal_mulai' ";
				$qc = $this->db->query($sc);
				if($qc->num_rows() > 0)
				{
					$id_uang = $qc->row()->id;
					$tpp = $this->get_tpp_tkk($nip);
					$si = "update d_rekap_keuangan_harian 
							set total_tpp = '$tpp[total_tpp]',
								tpp_statis = '$tpp[tpp_statis]',
								tpp_dinamis = '$tpp[tpp_dinamis]',
								pemotongan_per_hari = '$persentase',
								update_date = '$update_date',	
								update_by = 'system'
							where id = '$id_uang'
					";
					$this->db->query($si);
				}
				else
				{
					$tpp = $this->get_tpp_tkk($nik);
					$si = "insert into d_rekap_keuangan_harian_tkk 
							set tanggal = '$tanggal_mulai',
								nik = '$nik',
								total_tpp = '$tpp[total_tpp]',
								tpp_statis = '$tpp[tpp_statis]',
								tpp_dinamis = '$tpp[tpp_dinamis]',
								pemotongan_per_hari = '$persentase',
								update_date = '$update_date',	
								update_by = 'system'					
					";
					$this->db->query($si);
				}
				
				
			}
			elseif($libur == 1)
			{
				$sc = "select id from d_absen_harian_tkk where nik = '$nik' and tanggal = '$tgl' and bulan = '$bln' and tahun = '$thn' ";
				$qc = $this->db->query($sc);
				if($qc->num_rows() > 0)
				{
					$id_absen = $qc->row()->id;
					$su = "update d_absen_harian_tkk 
							set libur = '1',
								potongan_tidak_masuk = '$persentase',
								persentase_potongan = '$persentase',
								update_date = '$update_date',
								update_by = 'system'
							where id = '$id_absen'
							";
					$this->db->query($su);
				}
				else
				{
					$si = "insert into d_absen_harian_tkk
											set nik = '$nik',
												tanggal = '$tgl',
												bulan = '$bln',
												tahun = '$thn',
												libur = '1',
												update_date = '$update_date',
												update_by = 'system'
											";
					echo $si.'</br>';
					$qi = $this->db->query($si);
				}
				
				$sc = "select id from d_rekap_keuangan_harian_tkk where nik = '$nik' and tanggal = '$tanggal_mulai' ";
				$qc = $this->db->query($sc);
				if($qc->num_rows() > 0)
				{
					$id_uang = $qc->row()->id;
					$tpp = $this->get_tpp_tkk($nip);
					$si = "update d_rekap_keuangan_harian 
							set total_tpp = '$tpp[total_tpp]',
								tpp_statis = '$tpp[tpp_statis]',
								tpp_dinamis = '$tpp[tpp_dinamis]',
								pemotongan_per_hari = '0',
								update_date = '$update_date',	
								update_by = 'system'
							where id = '$id_uang'
					";
					$this->db->query($si);
				}
				else
				{
					$tpp = $this->get_tpp_tkk($nik);
					$si = "insert into d_rekap_keuangan_harian_tkk 
							set tanggal = '$tanggal_mulai',
								nik = '$nik',
								total_tpp = '$tpp[total_tpp]',
								tpp_statis = '$tpp[tpp_statis]',
								tpp_dinamis = '$tpp[tpp_dinamis]',
								pemotongan_per_hari = '0',
								update_date = '$update_date',	
								update_by = 'system'					
					";
					$this->db->query($si);
				}
				
			}
						
		}
		else
		{
			for($i=0;$i<=$selisih;$i++)
			{
				$penambah = $i*60*60*24;
				$tanggal_time = strtotime($tanggal_mulai) + $penambah;
				$tanggal = date('Y-m-d',$tanggal_time);
				$tgl = date('j',$tanggal_time);
				$bln = date('n',$tanggal_time);
				$thn = date('Y',$tanggal_time);
				$update_date = date('Y-m-d H:i:s');
				$libur = $this->cek_libur($tanggal);
				if($libur == 0)
				{
					$sc = "select id from d_absen_harian_tkk where nik = '$nik' and tanggal = '$tgl' and bulan = '$bln' and tahun = '$thn' ";
					echo $sc.'</br>';
					$qc = $this->db->query($sc);
					if($qc->num_rows() > 0)
					{
						$id_absen = $qc->row()->id;
						$su = "update d_absen_harian_tkk 
								set jenis_tidak_masuk = '$jenis_ketidakhadiran',
									potongan_tidak_masuk = '$persentase',
									persentase_potongan = '$persentase',
									update_date = '$update_date',
									update_by = 'system'
								where id = '$id_absen'
								";
						//echo $su.'</br>';
						$this->db->query($su);
					}
					else
					{
						$st = "select a.*,
									b.*
									from d_pegawai_tipe_jadwal_tkk a
									left join referensi_tipe_jadwal b on(a.tipe_jadwal = b.id)
									where nik = '$nik' 
									
						";
						$rst = $this->db->query($st)->row_array();
						
						$jadwal_jam_masuk = $rst['jadwal_jam_masuk'];
						$jadwal_menit_masuk = $rst['jadwal_menit_masuk'];
						$jadwal_masuk = date('Y-m-d H:i',strtotime($tgl.'-'.$bln.'-'.$thn.' '.$jadwal_jam_masuk.':'.$jadwal_menit_masuk));
						$jadwal_jam_keluar = $rst['jadwal_jam_keluar'];
						$jadwal_menit_keluar = $rst['jadwal_menit_keluar'];
						$jadwal_keluar = date('Y-m-d H:i',strtotime($tgl.'-'.$bln.'-'.$thn.' '.$jadwal_jam_keluar.':'.$jadwal_menit_keluar));		
						
						$si = "insert into d_absen_harian_tkk
												set nik = '$nik',
													tanggal = '$tgl',
													bulan = '$bln',
													tahun = '$thn',
													jadwal_masuk = '$jadwal_masuk',
													jadwal_keluar = '$jadwal_keluar', 
													jenis_tidak_masuk = '$jenis_ketidakhadiran',
													potongan_tidak_masuk = '$persentase',
													persentase_potongan = '$persentase',
													update_date = '$update_date',
													update_by = 'system'
												";
						echo $si.'</br>';
						$qi = $this->db->query($si);
					}
					
					$sc = "select id from d_rekap_keuangan_harian_tkk where nik = '$nik' and tanggal = '$tanggal' ";
					$qc = $this->db->query($sc);
					if($qc->num_rows() > 0)
					{
						$id_uang = $qc->row()->id;
						$tpp = $this->get_tpp_tkk($nik);
						$si = "update d_rekap_keuangan_harian_tkk 
								set total_tpp = '$tpp[total_tpp]',
									tpp_statis = '$tpp[tpp_statis]',
									tpp_dinamis = '$tpp[tpp_dinamis]',
									pemotongan_per_hari = '$persentase',
									update_date = '$update_date',	
									update_by = 'system'
								where id = '$id_uang'
						";
						//echo $si.'</br>';
						$this->db->query($si);
					}
					else
					{
						$tpp = $this->get_tpp_tkk($nik);
						$si = "insert into d_rekap_keuangan_harian_tkk 
								set tanggal = '$tanggal',
									nik = '$nik',
									total_tpp = '$tpp[total_tpp]',
									tpp_statis = '$tpp[tpp_statis]',
									tpp_dinamis = '$tpp[tpp_dinamis]',
									pemotongan_per_hari = '$persentase',
									update_date = '$update_date',	
									update_by = 'system'					
						";
						$this->db->query($si);
					}
				
				}
				elseif($libur == 1)
				{
					$sc = "select id from d_absen_harian_tkk where nik = '$nik' and tanggal = '$tgl' and bulan = '$bln' and tahun = '$thn' ";
					echo $sc.'</br>';
					$qc = $this->db->query($sc);
					if($qc->num_rows() > 0)
					{
						$id_absen = $qc->row()->id;
						$su = "update d_absen_harian_tkk 
								set libur = '1',
									update_date = '$update_date',
									update_by = 'system'
								where id = '$id_absen'
								";
						//echo $su.'</br>';
						$this->db->query($su);
					}
					else
					{
						$si = "insert into d_absen_harian_tkk
												set nik = '$nik',
													tanggal = '$tgl',
													bulan = '$bln',
													tahun = '$thn',
													libur = '1',
													update_date = '$update_date',
													update_by = 'system'
												";
						echo $si.'</br>';
						$qi = $this->db->query($si);
					}
					
					$sc = "select id from d_rekap_keuangan_harian_tkk where nik = '$nik' and tanggal = '$tanggal' ";
					$qc = $this->db->query($sc);
					if($qc->num_rows() > 0)
					{
						$id_uang = $qc->row()->id;
						$tpp = $this->get_tpp_tkk($nik);
						$si = "update d_rekap_keuangan_harian_tkk 
								set total_tpp = '$tpp[total_tpp]',
									tpp_statis = '$tpp[tpp_statis]',
									tpp_dinamis = '$tpp[tpp_dinamis]',
									pemotongan_per_hari = '0',
									update_date = '$update_date',	
									update_by = 'system'
								where id = '$id_uang'
						";
						//echo $si.'</br>';
						$this->db->query($si);
					}
					else
					{
						$tpp = $this->get_tpp_tkk($nik);
						$si = "insert into d_rekap_keuangan_harian_tkk 
								set tanggal = '$tanggal',
									nik = '$nik',
									total_tpp = '$tpp[total_tpp]',
									tpp_statis = '$tpp[tpp_statis]',
									tpp_dinamis = '$tpp[tpp_dinamis]',
									pemotongan_per_hari = '0',
									update_date = '$update_date',	
									update_by = 'system'					
						";
						$this->db->query($si);
					}
				}
			}
		}
	}
	
	public function rekap_kehadiran_bulanan()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		
		$data = array(
				'title' => 'Data Rekap Kehadiran Bulanan',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'sssubunit' => $this->session->userdata('sssubunit_rekap'),
				'jnsjab' => $this->session->userdata('jnsjab_rekap'),
				'keselon' => $this->session->userdata('keselon_rekap'),
				'nama' => $this->session->userdata('nama_rekap'),
				'reqnip' => $this->session->userdata('reqnip_rekap'),
				'bulan' => $this->session->userdata('bulan_rekap'),
				'tahun' => $this->session->userdata('tahun_rekap'),
		);
		$this->load->view('rekap_kehadiran_bulanan',$data);
		input_log($this->user_id,'Lihat kehadiran bulanan pns');
	}
	
	public function get_rekap_kehadiran_bulanan()
	{
		//print_r($_REQUEST);
		$order = $_REQUEST['order'][0];
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/log/rekap_kehadiran_bulanan');
		//$aColumns = array( 'nip', 'nip','nama', 'nip','nip');
		$sssubunit = $this->input->post('sssubunit');
		$this->session->set_userdata('sssubunit_rekap',$sssubunit);
		$jnsjab = $this->input->post('jnsjab');
		$this->session->set_userdata('jnsjab_rekap',$jnsjab);
		$keselon = $this->input->post('keselon');
		$this->session->set_userdata('keselon_rekap',$keselon);
		$nama = $this->input->post('nama');
		$this->session->set_userdata('nama_rekap',$nama);
		$reqnip = $this->input->post('reqnip');
		$this->session->set_userdata('reqnip_rekap',$reqnip);
		$bulan = $this->input->post('bulan');
		$this->session->set_userdata('bulan_rekap',$bulan);
		$tahun = $this->input->post('tahun');
		$this->session->set_userdata('tahun_rekap',$tahun);
		
		$sSearch = '';
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		if(isset($select) && $select != '')
		{
			if(!empty($s_biro)) 
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
			}
			else
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			}
		}
		
		if(isset($nama) && $nama != '')
		{
			$sSearch .= " and (nama like '%".$nama."%') ";
		}
		if(isset($reqnip) && $reqnip != '')
		{
			$sSearch .= " and (a.nip = '".$reqnip."') ";
		}
		if(isset($jnsjab) && $jnsjab != '0')
		{
			$sSearch .= " and (jnsjab = '".$jnsjab."') ";
		}
		if(isset($keselon) && $keselon != '0')
		{
			$sSearch .= " and (keselon = '".$keselon."') ";
		}
		/* if(isset($bulan) && $bulan != '')
		{
			$sSearch .= " and (b.bulan = '".$bulan."') ";
		}
		if(isset($tahun) && $tahun != '')
		{
			$sSearch .= " and (b.tahun = '".$tahun."') ";
		} */
	
		//limit
		if ( isset( $_REQUEST['start'] ) && $_REQUEST['length'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_REQUEST['start'] ).", ".
				intval( $_REQUEST['length'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "`".$aColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]."` ".
						($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		//$sOrder = " order by ".$aColumns[$order['column']]." ".$order['dir']." ";
		$s0 = "select count(a.nip) as jml from d_pegawai a
				where 1 = 1
				";
		$s0 .= $sSearch;
		
		$sheet_total = $this->db->query($s0)->row();
		$sheet_total = $sheet_total->jml;
	
		$s1 = "SELECT DISTINCT a.nip,a.nama,
									a.keselon,
									a.kgolru,
									a.kuntp,
									a.kunkom,
									a.kununit,
									a.tanggal_lahir,
									a.jabatan,
									a.kunker,
									(select neselon from referensi_eselon where keselon = a.keselon) as eselon,
									(select ngolru from referensi_golongan_ruang where KGOLRU = a.KGOLRU) as golongan,
									(select pangkat from referensi_golongan_ruang where KGOLRU = a.KGOLRU) as pangkat,									
									(select sum(keterlambatan_masuk) from d_absen_harian where nip = a.nip and bulan = '$bulan' and tahun = '$tahun' ) as jumlah_telat_masuk,
									(select sum(kecepatan_keluar) from d_absen_harian where nip = a.nip and bulan = '$bulan' and tahun = '$tahun' ) as jumlah_pulang_cepat,
									(select count(jenis_tidak_masuk) from d_absen_harian where nip = a.nip and bulan = '$bulan' and tahun = '$tahun' and jenis_tidak_masuk != 0) as jumlah_tidak_masuk,
									(select sum(persentase_potongan) from d_absen_harian where nip = a.nip and bulan = '$bulan' and tahun = '$tahun' ) as jumlah_potongan
				FROM d_pegawai a
				WHERE 1=1";
		$s1 .= $sSearch;
		$s1 .= " order by keselon asc, kgolru desc, nip asc ";
		
		$s1 .= $sLimit;
	//	echo $s1;
		$sheet1 = $this->db->query($s1);
		$result = array();
		$i=0;
		$no=1;
		foreach($sheet1->result() as $sheet)
		{
			$result[$i]['0'] = $no;
			$result[$i]['1'] = $sheet->nip.' </br> '.$sheet->nama;
			$result[$i]['2'] = $sheet->pangkat.' </br> '.$sheet->golongan;
			$result[$i]['3'] = $sheet->jabatan.' </br> '.$sheet->eselon;
			$result[$i]['4'] = $this->get_skpd($sheet->kunker);	
			$result[$i]['5'] =	number_format($sheet->jumlah_telat_masuk,1,',','.');	
			$result[$i]['6'] =	number_format($sheet->jumlah_pulang_cepat,1,',','.');	
			$result[$i]['7'] =	$sheet->jumlah_tidak_masuk;	
			$result[$i]['8'] = $sheet->jumlah_potongan;	
			$no++;
			$i++;
		}
		
		$data = $result;
		$results = array(
			"iTotalRecords" => ($sheet_total),
			"iTotalDisplayRecords" => ($sheet_total),
			"aaData"=>$data);
			
		echo json_encode($results);
	}
	
	public function ekspor_rekap_kehadiran_bulanan_pns_pdf()
	{
		set_time_limit(0);
		$this->load->library('pdf');
		$pdf = $this->pdf->load('format => A-2 L');
		//print_r($_REQUEST);
		$sssubunit = $this->input->post('sssubunit_pdf');
		$jnsjab = $this->input->post('jnsjab_pdf');
		$keselon = $this->input->post('keselon_pdf');
		$nama = $this->input->post('nama_pdf');
		$reqnip = $this->input->post('reqnip_pdf');
		$bulan = $this->input->post('bulan_pdf');
		$tahun = $this->input->post('tahun_pdf');
		$sSearch = '';
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		if(isset($select) && $select != '')
		{
			if(!empty($s_biro)) 
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
			}
			else
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			}
		}
		
		if(isset($nama) && $nama != '')
		{
			$sSearch .= " and (nama like '%".$nama."%') ";
		}
		if(isset($reqnip) && $reqnip != '')
		{
			$sSearch .= " and (a.nip = '".$reqnip."') ";
		}
		if(isset($jnsjab) && $jnsjab != '0' && $jnsjab != '')
		{
			$sSearch .= " and (jnsjab = '".$jnsjab."') ";
		}
		if(isset($keselon) && $keselon != '0' && $keselon != '')
		{
			$sSearch .= " and (keselon = '".$keselon."') ";
		}
		
		$s1 = "SELECT DISTINCT a.nip,a.nama,
									a.keselon,
									a.kgolru,
									a.kuntp,
									a.kunkom,
									a.kununit,
									a.tanggal_lahir,
									a.jabatan,
									(select neselon from referensi_eselon where keselon = a.keselon) as eselon,
									(select ngolru from referensi_golongan_ruang where KGOLRU = a.KGOLRU) as golongan,
									(select pangkat from referensi_golongan_ruang where KGOLRU = a.KGOLRU) as pangkat,
									CASE KUNKOM
									   WHEN 01 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,4) = concat(kuntp,kunkom) and mid(kunker,3,8) = '00000000' )
									   WHEN 10 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' )
									   WHEN 11 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' ) 
									   ELSE (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,4) = concat(kuntp,kunkom) and mid(kunker,5,8) = '00000000' )
									END as skpd,
									(select sum(keterlambatan_masuk) from d_absen_harian where nip = a.nip and bulan = '$bulan' and tahun = '$tahun' ) as jumlah_telat_masuk,
									(select sum(kecepatan_keluar) from d_absen_harian where nip = a.nip and bulan = '$bulan' and tahun = '$tahun' ) as jumlah_pulang_cepat,
									(select count(jenis_tidak_masuk) from d_absen_harian where nip = a.nip and bulan = '$bulan' and tahun = '$tahun' and jenis_tidak_masuk != 0) as jumlah_tidak_masuk,
									(select sum(persentase_potongan) from d_absen_harian where nip = a.nip and bulan = '$bulan' and tahun = '$tahun' ) as jumlah_potongan
				FROM d_pegawai a
				WHERE 1=1";
		$s1 .= $sSearch;
		$s1 .= " order by keselon asc, kgolru desc, nip asc ";
	//	echo $s1;
		$q= $this->db->query($s1);
		$data['q'] = $q;
		$data['unit_kerja'] = $this->get_unit_kerja($sssubunit);
		$data['bulan'] = $bulan;
		$data['tahun'] = $tahun;
		$html = $this->load->view('cetak_rekap_kehadiran_bulanan_pns',$data,true);
		input_log($this->user_id,'cetak kehadiran bulanan pns');
		$pdf->WriteHTML($html);
		$pdf->Output();
			
	}
	
	public function rekap_kehadiran_bulanan_tkk()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		
		$data = array(
				'title' => 'Data Rekap Kehadiran Bulanan TKK',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'sssubunit' => $this->session->userdata('sssubunit_rekap'),
				'nama' => $this->session->userdata('nama_rekap'),
				'nik' => $this->session->userdata('nik'),
				'bulan' => $this->session->userdata('bulan_rekap'),
				'tahun' => $this->session->userdata('tahun_rekap'),
		);
		$this->load->view('rekap_kehadiran_bulanan_tkk',$data);
		input_log($this->user_id,'Lihat rekap kehadiran bulanan tkk');
	}
	
	public function get_rekap_kehadiran_bulanan_tkk()
	{
		//print_r($_REQUEST);
		$order = $_REQUEST['order'][0];
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/log/rekap_kehadiran_bulanan_tkk');
		//$aColumns = array( 'nip', 'nip','nama', 'nip','nip');
		$sssubunit = $this->input->post('sssubunit');
		$this->session->set_userdata('sssubunit_rekap',$sssubunit);
		$nama = $this->input->post('nama');
		$this->session->set_userdata('nama_rekap',$nama);
		$nik = $this->input->post('nik');
		$this->session->set_userdata('nik_rekap',$nik);
		$bulan = $this->input->post('bulan');
		$this->session->set_userdata('bulan_rekap',$bulan);
		$tahun = $this->input->post('tahun');
		$this->session->set_userdata('tahun_rekap',$tahun);
		
		$sSearch = '';
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		if(isset($select) && $select != '')
		{
			if(!empty($s_biro)) 
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
			}
			else
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			}
		}
		
		if(isset($nama) && $nama != '')
		{
			$sSearch .= " and (nama like '%".$nama."%') ";
		}
		if(isset($nik) && $nik != '')
		{
			$sSearch .= " and (a.nik = '".$nik."') ";
		}
			
		//limit
		if ( isset( $_REQUEST['start'] ) && $_REQUEST['length'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_REQUEST['start'] ).", ".
				intval( $_REQUEST['length'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "`".$aColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]."` ".
						($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		//$sOrder = " order by ".$aColumns[$order['column']]." ".$order['dir']." ";
		$s0 = "select count(a.nik) as jml from d_tkk a
		LEFT JOIN d_absen_harian_tkk b on(a.nik = b.nik)
				where 1 = 1
				";
		$s0 .= $sSearch;
		
		$sheet_total = $this->db->query($s0)->row();
		$sheet_total = $sheet_total->jml;
	
		$s1 = "SELECT DISTINCT a.nik,a.nama,
								concat(kuntp,kunkom,kununit,rincunit1,rincunit2,rincunit3) as kunker,
								(select sum(keterlambatan_masuk) from d_absen_harian_tkk where nik = a.nik and bulan = '$bulan' and tahun = '$tahun' ) as jumlah_telat_masuk,
								(select sum(kecepatan_keluar) from d_absen_harian_tkk where nik = a.nik and bulan = '$bulan' and tahun = '$tahun' ) as jumlah_pulang_cepat,
								(select count(jenis_tidak_masuk) from d_absen_harian_tkk where nik = a.nik and bulan = '$bulan' and tahun = '$tahun' and jenis_tidak_masuk != 0) as jumlah_tidak_masuk,
								(select sum(persentase_potongan) from d_absen_harian_tkk where nik = a.nik and bulan = '$bulan' and tahun = '$tahun' ) as jumlah_potongan
				FROM d_tkk a
				WHERE 1=1";
		$s1 .= $sSearch;
		$s1 .= " order by nik desc ";
		
		$s1 .= $sLimit;
	//	echo $s1;
		$sheet1 = $this->db->query($s1);
		$result = array();
		$i=0;
		$no=1;
		foreach($sheet1->result() as $sheet)
		{
			
			$result[$i]['0'] = $no;
			$result[$i]['1'] = $sheet->nik.' </br>'.$sheet->nama;
			$result[$i]['2'] = $this->get_skpd($sheet->kunker);
			$result[$i]['3'] = number_format($sheet->jumlah_telat_masuk,1,',','.');
			$result[$i]['4'] = number_format($sheet->jumlah_pulang_cepat,1,',','.');
			$result[$i]['5'] = $sheet->jumlah_tidak_masuk;
			$result[$i]['6'] = $sheet->jumlah_potongan;
			$no++;
			$i++;
		}
		
		$data = $result;
		$results = array(
			"iTotalRecords" => ($sheet_total),
			"iTotalDisplayRecords" => ($sheet_total),
			"aaData"=>$data);
			
		echo json_encode($results);
	}
	
	public function ekspor_rekap_kehadiran_bulanan_tkk_pdf()
	{
		set_time_limit(0);
		$this->load->library('pdf');
		$pdf = $this->pdf->load('format => A-2 L');
		//print_r($_REQUEST);
		$sssubunit = $this->input->post('sssubunit_pdf');
		$nama = $this->input->post('nama_pdf');
		$nik = $this->input->post('nik_pdf');
		$bulan = $this->input->post('bulan_pdf');
		$tahun = $this->input->post('tahun_pdf');
		$sSearch = '';
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		if(isset($select) && $select != '')
		{
			if(!empty($s_biro)) 
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
			}
			else
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			}
		}
		
		if(isset($nama) && $nama != '')
		{
			$sSearch .= " and (nama like '%".$nama."%') ";
		}
		if(isset($nik) && $nik != '')
		{
			$sSearch .= " and (a.nik = '".$nik."') ";
		}
		
		$s1 = "SELECT DISTINCT a.nik,a.nama,
								CASE KUNKOM
								   WHEN 10 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' 	)
								   WHEN 11 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' 	) 
								   ELSE (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,4) = concat(kuntp,kunkom) and mid(kunker,5,8) = '00000000' 	)
								END as skpd,
								(select sum(keterlambatan_masuk) from d_absen_harian_tkk where nik = a.nik and bulan = '$bulan' and tahun = '$tahun' ) as jumlah_telat_masuk,
								(select sum(kecepatan_keluar) from d_absen_harian_tkk where nik = a.nik and bulan = '$bulan' and tahun = '$tahun' ) as jumlah_pulang_cepat,
								(select count(jenis_tidak_masuk) from d_absen_harian_tkk where nik = a.nik and bulan = '$bulan' and tahun = '$tahun' and jenis_tidak_masuk != 0) as jumlah_tidak_masuk,
								(select sum(persentase_potongan) from d_absen_harian_tkk where nik = a.nik and bulan = '$bulan' and tahun = '$tahun' ) as jumlah_potongan
				FROM d_tkk a
				WHERE 1=1";
		$s1 .= $sSearch;
		$s1 .= " order by nik desc ";
	//	echo $s1;
		$q= $this->db->query($s1);
		$data['q'] = $q;
		$data['unit_kerja'] = $this->get_unit_kerja($sssubunit);
		$data['bulan'] = $bulan;
		$data['tahun'] = $tahun;
		$html = $this->load->view('cetak_rekap_kehadiran_bulanan_tkk',$data,true);
		input_log($this->user_id,'cetak rekap kehadiran bulanan tkk');
		$pdf->WriteHTML($html);
		$pdf->Output();
			
	}
	
	function get_skpd($kunker)
	{
		$kuntp = substr($kunker,0,2);
		$kunkom = substr($kunker,2,2);
		$kununit = substr($kunker,4,2);
		switch($kunkom)
		{
			case '10':
			case '11':
				$s = "select nunker from referensi_unit_kerja where substr(kunker,1,2) = '$kuntp' 
																and substr(kunker,3,2) = '$kunkom' 
																and substr(kunker,5,8) = '00000000'";
				$q = $this->db->query($s);
				if($q->num_rows() > 0)
				{
					$nunker = $q->row()->nunker;
				}
				else
				{
					$nunker = '';
				}
			break;
			
			default:
				$s = "select nunker from referensi_unit_kerja where substr(kunker,1,2) = '$kuntp' 
																and substr(kunker,3,2) = '$kunkom' 
																and substr(kunker,5,2) = '$kununit' 
																and substr(kunker,7,6) = '000000'
																";
				$q = $this->db->query($s);
				if($q->num_rows() > 0)
				{
					$nunker = $q->row()->nunker;
				}
				else
				{
					$nunker = '';
				}
			break;
		}
		return $nunker;
	}
	
	function cek_libur($hari)
	{
		$tgl = date('Y-m-d',strtotime($hari));
		$urutan_hari = date('w');
		$sc = "select * from d_hari_libur where tanggal = '$tgl' ";
		$qc = $this->db->query($sc);
		if($qc->num_rows() > 0)
		{
			$tanggal_merah = 1;
		}
		if((isset($tanggal_merah) && $tanggal_merah == 1) or ($urutan_hari == 0 || $urutan_hari == 6))
		{
			$libur = 1;
		}
		else
		{
			$libur = 0;
		}
		return $libur;
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */