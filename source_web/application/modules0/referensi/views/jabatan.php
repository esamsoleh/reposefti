<?php $this->load->view('header');?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('top');?>
  <!-- Left side column. contains the logo and sidebar -->
  
  <?php $this->load->view('sidebar');?>
 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title;?>
        <small><?=$subtitle;?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
							<div class="col-md-12">
								<div class="panel panel-white">
								<div class="panel-body">
									<form>
									<div class="col-lg-4">
										<input class="form-control" type="text" name="namacari" id="namacari" placeholder="Kata Kunci">
									</div>
									<div class="col-lg-4">
										<button class="btn btn-success" type="submit" id="load_cari_data">Cari</button>
									</div>
									</form>
									</br>
									</br>
									<div id="ReferensiContainer"></div>
								</div>
								</div>
							</div>
         
						</div>
						

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
 
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('javascript');?>

<script type="text/javascript">
         $(document).ready(function () {
			$('#ReferensiContainer').jtable({
                    title: "Daftar jabatan",
                    paging: true, //Enable paging
                    pageSize: 10, //Set page size (default: 10)
                    sorting: true, //Enable sorting
                    defaultSorting: 'id ASC', //Set default sorting
                    actions: {
                        listAction: '/referensi/list_jabatan',
                        createAction: function (data) {
							var deferred = $.Deferred();

							// Capture form submit result from the hidden iframe
							//$("#postiframe").load(function () {
								$("#postiframe").on('load',function () {
								iframeContents = $("#postiframe").contents().find("body").text();
								var result = $.parseJSON(iframeContents);
								deferred.resolve(result);
							});

							// Submit form with file upload settings
							var form = $('#jtable-create-form');
							form.unbind("submit");
							form.attr("action", "/referensi/add_jabatan");
							form.attr("method", "post");
							form.attr("enctype", "multipart/form-data");
							form.attr("encoding", "multipart/form-data");
							form.attr("target", "postiframe");
							form.submit();

							return deferred;
						},
                        updateAction: function (data) {
							var deferred = $.Deferred();

							// Capture form submit result from the hidden iframe
						//	$("#postiframe").load(function () {
							$("#postiframe").on('load',function () {
								iframeContents = $("#postiframe").contents().find("body").text();
								var result = $.parseJSON(iframeContents);
								deferred.resolve(result);
							});

							// Submit form with file upload settings
							var form = $('#jtable-edit-form');
							form.unbind("submit");
							form.attr("action", "/referensi/update_jabatan");
							form.attr("method", "post");
							form.attr("enctype", "multipart/form-data");
							form.attr("encoding", "multipart/form-data");
							form.attr("target", "postiframe");
							form.submit();

							return deferred;
						},
                        deleteAction: '/referensi/del_jabatan',
                    },
					recordUpdated:function(event, data){
						$('#ReferensiContainer').jtable('reload');
					},
                    fields: {
					id:{
						key: true,
						width:'10%',
						create:false,
						edit:false,
						title:'ID',
						list:true,					
					},
					nama:{
						width:'50%',
						create:true,
						edit:true,
						title:'Nama',
						list:true,					
					},
					ikon: {
						title: "Ikon Aktif",
						list: true,
						create: true,
						input: function (data) {
							return '<input id="ikon" type="file" name="ikon" /><iframe name="postiframe" id="postiframe" style="display: none" />';
						},
						 display: function (data) {
								return '<img width="80" height="80" src=/assets/ikon/' + data.record.ikon + ' />';
						   }
					},
					ikon_lepas_helm: {
						title: "Ikon Helm Lepas",
						list: true,
						create: true,
						input: function (data) {
							return '<input id="ikon" type="file" name="ikon_lepas_helm" /><iframe name="postiframe" id="postiframe" style="display: none" />';
						},
						 display: function (data) {
								return '<img width="80" height="80" src=/assets/ikon/' + data.record.ikon_lepas_helm + ' />';
						   }
					},
					ikon_keluar_department: {
						title: "Ikon Keluar Department",
						list: true,
						create: true,
						input: function (data) {
							return '<input id="ikon" type="file" name="ikon_keluar_department" /><iframe name="postiframe" id="postiframe" style="display: none" />';
						},
						 display: function (data) {
								return '<img width="80" height="80" src=/assets/ikon/' + data.record.ikon_keluar_department + ' />';
						   }
					},
					ikon_emergency: {
						title: "Ikon Emergency",
						list: true,
						create: true,
						input: function (data) {
							return '<input id="ikon" type="file" name="ikon_emergency" /><iframe name="postiframe" id="postiframe" style="display: none" />';
						},
						 display: function (data) {
								return '<img width="80" height="80" src=/assets/ikon/' + data.record.ikon_emergency + ' />';
						   }
					},
					}
			});
			
			$('#load_cari_data').click(function (e) {
				e.preventDefault();
				$('#ReferensiContainer').jtable('load', {
					namacari: $('#namacari').val()
				});
			});
			//Load list from server
			$('#ReferensiContainer').jtable('load');
				
        });
    </script>
</body>
</html>