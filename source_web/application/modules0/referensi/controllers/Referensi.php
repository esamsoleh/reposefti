<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Referensi extends MX_Controller {
	
	private $field_selected = array(
		
		'1'=>array(
				'0'=> 'id',
				'1'=> 'nama',
			),
		'2'=>array(
				'0'=> 'id',
				'1'=> 'awal',
				'2'=> 'akhir',
				'3'=> 'presentase',
			),
		'3'=>array(
				'0'=> 'id',
				'1'=> 'jenis_ketidakhadiran',
				'2'=> 'persentase',
				'3'=> 'periode',
			),
		'4'=>array(
				'0'=> 'id',
				'1'=> 'awal',
				'2'=> 'akhir',
				'3'=> 'presentase',
			),
		'5'=>array(
				'0'=> 'id',
				'1'=> 'status',
				'1'=> 'proporsi',
			),
		'6'=>array(
				'0'=> 'id',
				'1'=> 'tipe',
				'2'=> 'keterangan',
				'3'=> 'jadwal_jam_masuk',
				'4'=> 'jadwal_menit_masuk',
				'5'=> 'jadwal_jam_keluar',
				'6'=> 'jadwal_menit_keluar',
			),
		'7'=>array(
				'0'=> 'id',
				'1'=> 'pph',
			),
		
	);
	private $alias_meta_table = array(
		
		'1' => array(
				'0' => 'ID',
				'1' => 'Jenis Kualifikasi',
				),
		'2' => array(
				'0' => 'ID',
				'1' => 'Awal',
				'2' => 'Akhir',
				'3' => 'Persentase Pemotongan',
				),
		'3' => array(
				'0' => 'ID',
				'1' => 'Jenis Ketidakhadiran',
				'2' => 'Persentase',
				'3' => 'Periode',
				),
		'4' => array(
				'0' => 'ID',
				'1' => 'Awal',
				'2' => 'Akhir',
				'3' => 'Persentase Pemotongan',
				),
		'5' => array(
				'0' => 'ID',
				'1' => 'Status',
				'2' => 'Proporsi',
				),
		'6' => array(
				'0' => 'ID',
				'1' => 'Tipe',
				'2' => 'Keterangan',
				'3' => 'Jadwal Jam Masuk',
				'4' => 'Jadwal Menit Masuk',
				'5' => 'Jadwal Jam Keluar',
				'6' => 'Jadwal Menit Keluar',
				),
		'7' => array(
				'0' => 'ID',
				'1' => 'PPH',
				),
								
	);
	private $type_referensi = array(
		'Jenis Kualifikasi',
		'Persentase Pemotongan Akibat Datang Terlambat',
		'Persentase Pemotongan Akibat Ketidakhadiran',
		'Persentase Pemotongan Akibat Pulang Cepat',
		'Persentase Proporsi Tunjangan',
		'Tipe Jadwal',
		'PPH',
	);
	
	
	public $apps;
	var $nama_user;
	var $file_foto;
	var $userid;
	public function __construct()
     {
            parent::__construct();
			$this->load->database();
			$this->load->library(array('ion_auth'));
			$this->apps = 'Referensi';
			if (!$this->ion_auth->logged_in())
			{
				redirect('/auth/login', 'refresh');
			}
			$this->load->model('referensi_model');
			$this->nama_user = $this->ion_auth->user()->row()->first_name.' '.$this->ion_auth->user()->row()->last_name;
		//	$this->file_foto = get_foto();
			$this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
			$this->output->set_header('Pragma: no-cache');
			$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
			$this->userid = $this->ion_auth->user()->row()->id;
			
       }
	   
	public function index()
	{
		
	}
	
	public function list_referensi()
	{
		//print_r($_REQUEST);
		if(isset($_REQUEST['namacari']))
		{
			$namacari = $_REQUEST['namacari'];
		}
		else
		{
			$namacari = '';
		}
		$referensi_id = $this->uri->segment(3);
		$sorting = $_REQUEST['jtSorting'];
		$index = $_REQUEST['jtStartIndex'];
		$size = $_REQUEST['jtPageSize'];
		$data = $this->referensi_model->get_referensi($referensi_id,$this->field_selected[$referensi_id],$sorting,$index,$size,$namacari);
		$data_semua = $this->referensi_model->get_referensi($referensi_id,$this->field_selected[$referensi_id],$sorting=NULL,$index=0,$size=0,$namacari);
		$recordCount = $data_semua->num_rows();
		$rows = array();
		foreach($data->result_array() as $row)
		{
			$rows[] = $row;
		}
		//print_r($rows);
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Records'] = $rows;
		$jTableResult['TotalRecordCount'] = $recordCount;
		print json_encode($jTableResult);
	}
	
	public function update_referensi()
	{
		$referensi_id = $this->uri->segment(3);
		//print_r($_REQUEST);
		//Update record in database
		// $result = mysql_query("UPDATE people SET Name = '" . $_POST["Name"] . "', Age = " . $_POST["Age"] . " WHERE PersonId = " . $_POST["PersonId"] . ";");

		$table = $this->referensi_model->getTable($referensi_id);
		//$fields = $this->referensi_model->getFields($referensi_id);
		$fields = $this->field_selected[$referensi_id];
		//print_r($fields);
		//echo count($fields);
		$sql = "update ".$table." set ";
		for($i=0;$i<count($fields);$i++)
		{
			if($i==(count($fields)-1))
			{
				$sql .= $fields[$i]." = '".$_POST[$fields[$i]]."' ";
			}
			else
			{
				$sql .= $fields[$i]." = '".$_POST[$fields[$i]]."', ";
			}
		}
		
		//		set ".$fields[1]->name." = '".$_POST[$fields[1]->name]."'
		$sql .= "where ".$fields[0]." = '".$_POST[$fields[0]]."' ";
	//	echo $sql;
		$this->db->query($sql);
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
	
	public function add_referensi()
	{
		$referensi_id = $this->uri->segment(3);
		//print_r($_REQUEST);
		//Update record in database
		// $result = mysql_query("UPDATE people SET Name = '" . $_POST["Name"] . "', Age = " . $_POST["Age"] . " WHERE PersonId = " . $_POST["PersonId"] . ";");

		$table = $this->referensi_model->getTable($referensi_id);
		//$fields = $this->referensi_model->getFields($referensi_id);
		$fields = $this->field_selected[$referensi_id];
		$sql = "insert into ".$table." set ";
		for($i=1;$i<count($fields);$i++)
		{
			if($i==(count($fields)-1))
			{
				$sql .= $fields[$i]." = '".$_POST[$fields[$i]]."' ";
			}
			else
			{
				$sql .= $fields[$i]." = '".$_POST[$fields[$i]]."', ";
			}
		}
		/*
		//		(".$fields[1]->name.")
		//		values ('".$_POST[$fields[1]->name]."') ";
		*/
		//echo $sql;
		$this->db->query($sql);
		
		//Get last inserted record (to return to jTable)
		$result = $this->db->query("SELECT * FROM ".$table." WHERE ".$fields[0]." = LAST_INSERT_ID();");
		$check = $result->row_array();
		if(isset($check) && count($check)>0)
		{
			$row = $check;
		}
		else
		{
			$result = $this->db->query("select * from ".$table." WHERE ".$fields[0]." = '".$_POST[$fields[0]]."' ");
			$row = $result->row_array();
		}
	//	print_r($row);
		
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Record'] = $row;
		print json_encode($jTableResult);
	}
	
	public function del_referensi()
	{
		$referensi_id = $this->uri->segment(3);
		$table = $this->referensi_model->getTable($referensi_id);
	//	$fields = $this->referensi_model->getFields($referensi_id);
		$fields = $this->field_selected[$referensi_id];
		$result = $this->db->query("DELETE FROM ".$table." WHERE ".$fields[0]." = '".$_POST[$fields[0]]."' ");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
	
	public function daftar()
	{
		$referensi_id = $this->uri->segment(3);
		
		if ($this->uri->segment(4) === FALSE) $row_id = 0; else $row_id = $this->uri->segment(4);
				
		$data = array(
			'title' =>'Referensi',
			'subtitle' => $this->type_referensi[$referensi_id-1],	
			'fields' => $this->referensi_model->getFields($referensi_id),
			'fields_alias' => $this->alias_meta_table[$referensi_id],
			'type_referensi' => $this->type_referensi[$referensi_id-1],
			'referensi_id' => $referensi_id,
			'field_selected' => $this->field_selected[$referensi_id],
				);
		
		$this->load->view('daftar',$data);
	}
	
	public function tunjangan_jfu()
	{
		$data = array(
			'title' =>'Referensi',
			'subtitle' => 'Tunjangan JFU',	
				);
		
		$this->load->view('tunjangan_jfu',$data);
	}
	
	public function list_tunjangan_jfu()
	{
		//print_r($_REQUEST);
		if(isset($_REQUEST['namacari']))
		{
			$namacari = $_REQUEST['namacari'];
		}
		else
		{
			$namacari = '';
		}
		$sorting = $_REQUEST['jtSorting'];
		$index = $_REQUEST['jtStartIndex'];
		$size = $_REQUEST['jtPageSize'];
		
		$sql = "select a.*
					from referensi_tunjangan_jfu a
					
					where 1=1
					";
		$data_semua = $this->db->query($sql);
			if($sorting != NULL)
			{
				$sql .= "order by ".$sorting." ";
			}
			if(($size != 0))
			{
				$sql.=" limit $index,$size";
			}
		
		$data = $this->db->query($sql);
		$recordCount = $data_semua->num_rows();
		$rows = array();
		foreach($data->result_array() as $row)
		{
			$rows[] = $row;
		}
		//print_r($rows);
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Records'] = $rows;
		$jTableResult['TotalRecordCount'] = $recordCount;
		print json_encode($jTableResult);
	}
	
	public function update_tunjangan_jfu()
	{
		$posts = $this->input->post();
		$sql = "update referensi_tunjangan_jfu 
				set status = '$posts[status]',
					tunjangan = '$posts[tunjangan]'
				";
		
		$sql .= "where ID = '".$posts['id']."' ";
	//	echo $sql;
		$this->db->query($sql);
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
	
	public function add_tunjangan_jfu()
	{
		$posts = $this->input->post();
		$sql = "insert into referensi_tunjangan_jfu 
				set status = '$posts[status]',
					tunjangan = '$posts[tunjangan]'
				";
		
		//echo $sql;
		$this->db->query($sql);
		
		//Get last inserted record (to return to jTable)
		$result = $this->db->query("SELECT * FROM referensi_tunjangan_jfu WHERE ID = LAST_INSERT_ID();");
		$check = $result->row_array();
		if(isset($check) && count($check)>0)
		$row = $check;
		
	//	print_r($row);
		
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Record'] = $row;
		print json_encode($jTableResult);
	}
	
	public function del_tunjangan_jfu()
	{
		$result = $this->db->query("DELETE FROM referensi_tunjangan_jfu WHERE id = '".$_POST['id']."' ");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
	
	public function status_option()
	{
		$sql = "select * from referensi_status_pegawai";
		$query = $this->db->query($sql);
		$rows = array();
		
		foreach($query->result_array() as $row)
		{
			$rows[] = array(
				'DisplayText' => $row['NSTATUS'],
				'Value' => $row['KSTATUS']
				);
		}
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Options'] = $rows;
		print json_encode($jTableResult);
	}
	
	public function data_layanan()
	{
		$data = array(
			'title' =>'Referensi',
			'subtitle' => 'Data Layanan',	
				);
		
		$this->load->view('data_layanan',$data);
	}
	
	public function list_data_layanan()
	{
		//print_r($_REQUEST);
		if(isset($_REQUEST['namacari']))
		{
			$namacari = $_REQUEST['namacari'];
		}
		else
		{
			$namacari = '';
		}
		$sorting = $_REQUEST['jtSorting'];
		$index = $_REQUEST['jtStartIndex'];
		$size = $_REQUEST['jtPageSize'];
		
		$sql = "select a.*, 
							b.jenis_layanan
					from ref_data_layanan a
					left join ref_jenis_layanan b
					on (a.id_jenis_layanan = b.id)
					where 1=1
					";
			if(isset($namacari) && $namacari != '')
			{
				$sql .= " AND (a.kolom like '%".$namacari."%' ) ";
			}
		$data_semua = $this->db->query($sql);
			if($sorting != NULL)
			{
				$sql .= "order by ".$sorting." ";
			}
			if(($size != 0))
			{
				$sql.=" limit $index,$size";
			}
		
		$data = $this->db->query($sql);
		$recordCount = $data_semua->num_rows();
		$rows = array();
		foreach($data->result_array() as $row)
		{
			$rows[] = $row;
		}
		//print_r($rows);
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Records'] = $rows;
		$jTableResult['TotalRecordCount'] = $recordCount;
		print json_encode($jTableResult);
	}
	
	public function update_data_layanan()
	{
		$posts = $this->input->post();
		$sql = "update ref_data_layanan 
				set id_jenis_layanan = '$posts[id_jenis_layanan]',
					kolom = '$posts[kolom]',
					tipe_data = '$posts[tipe_data]',
					urutan = '$posts[urutan]',
					tipe_input = '$posts[tipe_input]',
					mandatory = '$posts[mandatory]',
					tabel_referensi = '$posts[tabel_referensi]',
					alias = '$posts[alias]'
				";
		
		$sql .= "where ID = '".$posts['id']."' ";
	//	echo $sql;
		$this->db->query($sql);
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
	
	public function add_data_layanan()
	{
		$posts = $this->input->post();
		$sql = "insert into ref_data_layanan 
				set id_jenis_layanan = '$posts[id_jenis_layanan]',
					kolom = '$posts[kolom]',
					tipe_data = '$posts[tipe_data]',
					urutan = '$posts[urutan]',
					tipe_input = '$posts[tipe_input]',
					mandatory = '$posts[mandatory]',
					tabel_referensi = '$posts[tabel_referensi]',
					alias = '$posts[alias]'
				";
		
		//echo $sql;
		$this->db->query($sql);
		
		//Get last inserted record (to return to jTable)
		$result = $this->db->query("SELECT * FROM ref_data_layanan WHERE ID = LAST_INSERT_ID();");
		$check = $result->row_array();
		if(isset($check) && count($check)>0)
		$row = $check;
		
	//	print_r($row);
		
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Record'] = $row;
		print json_encode($jTableResult);
	}
	
	public function del_data_layanan()
	{
		$result = $this->db->query("DELETE FROM ref_data_layanan WHERE id = '".$_POST['id']."' ");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
	
	public function jenis_layanan_options()
	{
		$sql = "select * from ref_jenis_layanan";
		$query = $this->db->query($sql);
		$rows = array();
		
		foreach($query->result_array() as $row)
		{
			$rows[] = array(
				'DisplayText' => $row['jenis_layanan'],
				'Value' => $row['id']
				);
		}
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Options'] = $rows;
		print json_encode($jTableResult);
	}
	
	public function langkah()
	{
		$data = array(
			'title' =>'Referensi',
			'subtitle' => 'Langkah Layanan',	
				);
		
		$this->load->view('langkah',$data);
	}
	
	public function list_langkah()
	{
		//print_r($_REQUEST);
		if(isset($_REQUEST['namacari']))
		{
			$namacari = $_REQUEST['namacari'];
		}
		else
		{
			$namacari = '';
		}
		$sorting = $_REQUEST['jtSorting'];
		$index = $_REQUEST['jtStartIndex'];
		$size = $_REQUEST['jtPageSize'];
		
		$sql = "select a.*, 
							b.jenis_layanan,
							c.name
					from ref_langkah a
					left join ref_jenis_layanan b on (a.ID_JENIS_LAYANAN = b.id)
					left join t_groups c on(a.PELAKSANA = c.id)
					where 1=1
					";
			if(isset($namacari) && $namacari != '')
			{
				$sql .= " AND (a.NAMA_LANGKAH like '%".$namacari."%' ) ";
			}
		$data_semua = $this->db->query($sql);
			if($sorting != NULL)
			{
				$sql .= "order by ".$sorting." ";
			}
			if(($size != 0))
			{
				$sql.=" limit $index,$size";
			}
		
		$data = $this->db->query($sql);
		$recordCount = $data_semua->num_rows();
		$rows = array();
		foreach($data->result_array() as $row)
		{
			$rows[] = $row;
		}
		//print_r($rows);
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Records'] = $rows;
		$jTableResult['TotalRecordCount'] = $recordCount;
		print json_encode($jTableResult);
	}
	
	public function update_langkah()
	{
		$posts = $this->input->post();
		$sql = "update ref_langkah 
				set id_jenis_layanan = '$posts[id_jenis_layanan]',
					langkah = '$posts[langkah]',
					nama_langkah = '$posts[nama_langkah]',
					pelaksana = '$posts[pelaksana]'
				";
		
		$sql .= "where id = '".$posts['id']."' ";
	//	echo $sql;
		$this->db->query($sql);
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
	
	public function add_langkah()
	{
		$posts = $this->input->post();
		$sql = "insert into ref_langkah 
				set id_jenis_layanan = '$posts[id_jenis_layanan]',
					langkah = '$posts[langkah]',
					nama_langkah = '$posts[nama_langkah]',
					pelaksana = '$posts[pelaksana]'
				";
		
		//echo $sql;
		$this->db->query($sql);
		
		//Get last inserted record (to return to jTable)
		$result = $this->db->query("SELECT * FROM ref_langkah WHERE ID = LAST_INSERT_ID();");
		$check = $result->row_array();
		if(isset($check) && count($check)>0)
		$row = $check;
		
	//	print_r($row);
		
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Record'] = $row;
		print json_encode($jTableResult);
	}
	
	public function del_langkah()
	{
		$result = $this->db->query("DELETE FROM ref_langkah WHERE ID = '".$_POST['id']."' ");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
	
	public function jenis_pelaksana()
	{
		$sql = "select a.* 
				from ref_unit_kerja a
				where 1=1
				and mid(kode_unit_kerja,1,4) = '0101'
				and nama_jabatan != ''
				ORDER BY kode_unit_kerja ASC
				";
		$query = $this->db->query($sql);
		$rows = array();
		
		foreach($query->result_array() as $row)
		{
			$rows[] = array(
				'DisplayText' => $row['nama_jabatan'],
				'Value' => $row['kode_unit_kerja']
				);
		}
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Options'] = $rows;
		print json_encode($jTableResult);
	}
	
	public function jenis_tabel_referensi()
	{
		$tables = $this->db->list_tables();
		$s = "show tables like 'ref_%' ";
		$s = "SELECT TABLE_NAME as tabel 
				FROM INFORMATION_SCHEMA.TABLES 
				WHERE TABLE_SCHEMA='ekonomi-unisba' 
				and TABLE_NAME like 'ref_%'
				order by tabel
				";
		$tables = $this->db->query($s);
		$rows = array();
		$rows[] = array(
			'DisplayText' => '----------',
			'Value' => '----------'
		);
		foreach ($tables->result_array() as $row)
		{
			$rows[] = array(
				'DisplayText' => $row['tabel'],
				'Value' => $row['tabel']
				);
		}
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Options'] = $rows;
		print json_encode($jTableResult);
	}
	
	public function jadwal_khusus()
	{
		$data = array(
			'title' =>'Referensi',
			'subtitle' => 'Jadwal Khusus',	
				);
		
		$this->load->view('jadwal_khusus',$data);
	}
	
	public function list_jadwal_khusus()
	{
		//print_r($_REQUEST);
		if(isset($_REQUEST['namacari']))
		{
			$namacari = $_REQUEST['namacari'];
		}
		else
		{
			$namacari = '';
		}
		$sorting = $_REQUEST['jtSorting'];
		$index = $_REQUEST['jtStartIndex'];
		$size = $_REQUEST['jtPageSize'];
		
		$sql = "select a.*
					from ref_jadwal_khusus a
					where 1=1
					";
		$data_semua = $this->db->query($sql);
			if($sorting != NULL)
			{
				$sql .= "order by ".$sorting." ";
			}
			if(($size != 0))
			{
				$sql.=" limit $index,$size";
			}
		
		$data = $this->db->query($sql);
		$recordCount = $data_semua->num_rows();
		$rows = array();
		foreach($data->result_array() as $row)
		{
			$rows[] = $row;
		}
		//print_r($rows);
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Records'] = $rows;
		$jTableResult['TotalRecordCount'] = $recordCount;
		print json_encode($jTableResult);
	}
	
	public function update_jadwal_khusus()
	{
		$update_by = $this->userid;
		$update_date = date('Y-m-d H:i:s');
		$posts = $this->input->post();
		$sql = "update ref_jadwal_khusus 
				set hari = '$posts[hari]',
					jadwal_jam_masuk = '$posts[jadwal_jam_masuk]',
					jadwal_menit_masuk = '$posts[jadwal_menit_masuk]',
					jadwal_jam_keluar = '$posts[jadwal_jam_keluar]',
					jadwal_menit_keluar = '$posts[jadwal_menit_keluar]',
					aktif = '$posts[aktif]',
					tipe_jadwal = '$posts[tipe_jadwal]',
					keterangan = '$posts[keterangan]',
					update_date = '$update_date',
					update_by = '$update_by'
				";
		
		$sql .= "where ID = '".$posts['id']."' ";
	//	echo $sql;
		$this->db->query($sql);
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
	
	public function add_jadwal_khusus()
	{
		$added_by = $this->userid;
		$added_date = date('Y-m-d H:i:s');
		$posts = $this->input->post();
		$sql = "insert into ref_jadwal_khusus 
				set hari = '$posts[hari]',
					jadwal_jam_masuk = '$posts[jadwal_jam_masuk]',
					jadwal_menit_masuk = '$posts[jadwal_menit_masuk]',
					jadwal_jam_keluar = '$posts[jadwal_jam_keluar]',
					jadwal_menit_keluar = '$posts[jadwal_menit_keluar]',
					aktif = '$posts[aktif]',
					tipe_jadwal = '$posts[tipe_jadwal]',
					keterangan = '$posts[keterangan]',
					added_date = '$added_date',
					added_by = '$added_by'
				";
		
		//echo $sql;
		$this->db->query($sql);
		
		//Get last inserted record (to return to jTable)
		$result = $this->db->query("SELECT * FROM ref_jadwal_khusus WHERE ID = LAST_INSERT_ID();");
		$check = $result->row_array();
		if(isset($check) && count($check)>0)
		$row = $check;
		
	//	print_r($row);
		
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Record'] = $row;
		print json_encode($jTableResult);
	}
	
	public function del_jadwal_khusus()
	{
		$result = $this->db->query("DELETE FROM ref_jadwal_khusus WHERE id = '".$_POST['id']."' ");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
	
	public function tipe_jadwal_option()
	{
		$sql = "select * from ref_tipe_jadwal";
		$query = $this->db->query($sql);
		$rows = array();
		
		foreach($query->result_array() as $row)
		{
			$rows[] = array(
				'DisplayText' => $row['keterangan'],
				'Value' => $row['id']
				);
		}
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Options'] = $rows;
		print json_encode($jTableResult);
	}
	
	//tipe jadwal
	public function tipe_jadwal()
	{
		$data = array(
			'title' =>'Referensi',
			'subtitle' => 'Tipe Jadwal',	
				);
		
		$this->load->view('tipe_jadwal',$data);
	}
	
	public function list_tipe_jadwal()
	{
		//print_r($_REQUEST);
		if(isset($_REQUEST['namacari']))
		{
			$namacari = $_REQUEST['namacari'];
		}
		else
		{
			$namacari = '';
		}
		$sorting = $_REQUEST['jtSorting'];
		$index = $_REQUEST['jtStartIndex'];
		$size = $_REQUEST['jtPageSize'];
		
		$sql = "select a.*
					from ref_tipe_jadwal a
					where 1=1
					";
		$data_semua = $this->db->query($sql);
			if($sorting != NULL)
			{
				$sql .= "order by ".$sorting." ";
			}
			if(($size != 0))
			{
				$sql.=" limit $index,$size";
			}
		
		$data = $this->db->query($sql);
		$recordCount = $data_semua->num_rows();
		$rows = array();
		foreach($data->result_array() as $row)
		{
			$rows[] = $row;
		}
		//print_r($rows);
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Records'] = $rows;
		$jTableResult['TotalRecordCount'] = $recordCount;
		print json_encode($jTableResult);
	}
	
	public function update_tipe_jadwal()
	{
		$update_by = $this->userid;
		$update_date = date('Y-m-d H:i:s');
		$posts = $this->input->post();
		$sql = "update ref_tipe_jadwal 
				set jadwal_jam_masuk = '$posts[jadwal_jam_masuk]',
					jadwal_menit_masuk = '$posts[jadwal_menit_masuk]',
					jadwal_jam_keluar = '$posts[jadwal_jam_keluar]',
					jadwal_menit_keluar = '$posts[jadwal_menit_keluar]',
					tipe = '$posts[tipe]',
					shift = '$posts[shift]',
					sabtu_masuk = '$posts[sabtu_masuk]',
					keterangan = '$posts[keterangan]'
				";
		
		$sql .= "where ID = '".$posts['id']."' ";
	//	echo $sql;
		$this->db->query($sql);
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
	
	public function add_tipe_jadwal()
	{
		$added_by = $this->userid;
		$added_date = date('Y-m-d H:i:s');
		$posts = $this->input->post();
		$sql = "insert into ref_tipe_jadwal 
				set jadwal_jam_masuk = '$posts[jadwal_jam_masuk]',
					jadwal_menit_masuk = '$posts[jadwal_menit_masuk]',
					jadwal_jam_keluar = '$posts[jadwal_jam_keluar]',
					jadwal_menit_keluar = '$posts[jadwal_menit_keluar]',
					tipe = '$posts[tipe]',
					shift = '$posts[shift]',
					sabtu_masuk = '$posts[sabtu_masuk]',
					keterangan = '$posts[keterangan]'
				";
		
		//echo $sql;
		$this->db->query($sql);
		
		//Get last inserted record (to return to jTable)
		$result = $this->db->query("SELECT * FROM ref_tipe_jadwal WHERE ID = LAST_INSERT_ID();");
		$check = $result->row_array();
		if(isset($check) && count($check)>0)
		$row = $check;
		
	//	print_r($row);
		
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Record'] = $row;
		print json_encode($jTableResult);
	}
	
	public function del_tipe_jadwal()
	{
		$result = $this->db->query("DELETE FROM ref_tipe_jadwal WHERE id = '".$_POST['id']."' ");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
	
	public function jabatan()
	{
		$data = array(
			'title' =>'Referensi',
			'subtitle' => 'Data jabatan',	
				);
		
		$this->load->view('jabatan',$data);
	}
	
	public function list_jabatan()
	{
		//print_r($_REQUEST);
		if(isset($_REQUEST['namacari']))
		{
			$namacari = $_REQUEST['namacari'];
		}
		else
		{
			$namacari = '';
		}
		$sorting = $_REQUEST['jtSorting'];
		$index = $_REQUEST['jtStartIndex'];
		$size = $_REQUEST['jtPageSize'];
		$sorting = " id asc ";
		$sql = "select a.*
					from ref_jabatan a
					where 1=1
					";
		
		$data_semua = $this->db->query($sql);
			if($sorting != NULL)
			{
				$sql .= "order by ".$sorting." ";
			}
			if(($size != 0))
			{
				$sql.=" limit $index,$size";
			}
		
		$data = $this->db->query($sql);
		$recordCount = $data_semua->num_rows();
		$rows = array();
		foreach($data->result_array() as $row)
		{
			$rows[] = $row;
		}
		//print_r($rows);
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Records'] = $rows;
		$jTableResult['TotalRecordCount'] = $recordCount;
		print json_encode($jTableResult);
	}
	
	public function update_jabatan()
	{
		$posts = $this->input->post();
		$sc = $this->db->query("select ikon,ikon_lepas_helm,ikon_keluar_department,ikon_emergency from ref_jabatan where id = '".$posts['id']."' ");
		if($sc->num_rows() > 0)
		{
			$file_ikon = $sc->row()->ikon;
			$file_ikon_helm_lepas = $sc->row()->ikon_lepas_helm;
			$file_ikon_helm_keluar_department =  $sc->row()->ikon_keluar_department;
			$ikon_emergency =  $sc->row()->ikon_emergency;
		}
		else
		{
			$file_ikon = '';
			$file_ikon_helm_lepas = '';
			$file_ikon_helm_keluar_department =  '';
		}
		$config['upload_path'] = 'assets/ikon/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$this->load->library('upload', $config);
		
		$field_name = 'ikon';
		if ($this->upload->do_upload($field_name))
		{
			$data = $this->upload->data();
			$ikon = $data['file_name'];
		}
		else
		{
			$ikon = $file_ikon;
		}	
		
		$field_name = 'ikon_lepas_helm';
		if ($this->upload->do_upload($field_name))
		{
			$data = $this->upload->data();
			$ikon_lepas_helm = $data['file_name'];
		}
		else
		{
			$ikon_lepas_helm = $file_ikon_helm_lepas;
		}	
		
		$field_name = 'ikon_keluar_department';
		if ($this->upload->do_upload($field_name))
		{
			$data = $this->upload->data();
			$ikon_keluar_department = $data['file_name'];
		}
		else
		{
			$ikon_keluar_department = $file_ikon_helm_keluar_department;
		}	
		
		$field_name = 'ikon_emergency';
		if ($this->upload->do_upload($field_name))
		{
			$data = $this->upload->data();
			$ikon_emergency = $data['file_name'];
		}
		else
		{
			$ikon_emergency = $ikon_emergency;
		}	
		
		$sql = "update ref_jabatan 
				set nama = '$posts[nama]',
					ikon = '$ikon',
					ikon_lepas_helm = '$ikon_lepas_helm',
					ikon_emergency = '$ikon_emergency',
					ikon_keluar_department = '$ikon_keluar_department'
				";
		
		$sql .= "where ID = '".$posts['id']."' ";
	//	echo $sql;
		$this->db->query($sql);
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
	
	public function add_jabatan()
	{
		$posts = $this->input->post();
		$config['upload_path'] = 'assets/ikon/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$this->load->library('upload', $config);
		$field_name = 'ikon';
		if ($this->upload->do_upload($field_name))
		{
			$data = $this->upload->data();
			$ikon = $data['file_name'];
		}
		else
		{
			$ikon = '';
			$this->session->set_flashdata('message', $this->upload->display_errors());	
		}
		$field_name = 'ikon_lepas_helm';
		if ($this->upload->do_upload($field_name))
		{
			$data = $this->upload->data();
			$ikon_lepas_helm = $data['file_name'];
		}
		else
		{
			$ikon_lepas_helm = '';
			$this->session->set_flashdata('message', $this->upload->display_errors());	
		}	
		$field_name = 'ikon_keluar_department';
		if ($this->upload->do_upload($field_name))
		{
			$data = $this->upload->data();
			$ikon_keluar_department = $data['file_name'];
		}
		else
		{
			$ikon_keluar_department = '';
			$this->session->set_flashdata('message', $this->upload->display_errors());	
		}
		$field_name = 'ikon_emergency';
		if ($this->upload->do_upload($field_name))
		{
			$data = $this->upload->data();
			$ikon_emergency = $data['file_name'];
		}
		else
		{
			$ikon_emergency = '';
			$this->session->set_flashdata('message', $this->upload->display_errors());	
		}		
		$sql = "insert into ref_jabatan 
				set nama = '$posts[nama]',
					ikon = '$ikon',
					ikon_lepas_helm = '$ikon_lepas_helm',
					ikon_emergency = '$ikon_emergency',
					ikon_keluar_department = '$ikon_keluar_department'
				";
		
		//echo $sql;
		$this->db->query($sql);
		
		//Get last inserted record (to return to jTable)
		$result = $this->db->query("SELECT * FROM ref_jabatan WHERE ID = LAST_INSERT_ID();");
		$check = $result->row_array();
		if(isset($check) && count($check)>0)
		$row = $check;
		
	//	print_r($row);
		
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Record'] = $row;
		print json_encode($jTableResult);
	}
	
	public function del_jabatan()
	{
		$result = $this->db->query("DELETE FROM ref_jabatan WHERE id = '".$_POST['id']."' ");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
			
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */