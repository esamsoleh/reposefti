<!DOCTYPE html>
<!-- Template Name: Rapido - Responsive Admin Template build with Twitter Bootstrap 3.x Version: 1.2 Author: ClipTheme -->
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
	<!--<![endif]-->
	<!-- start: HEAD -->
	<head>
		<title><?=$title;?></title>
		<!-- start: META -->
		<meta charset="utf-8" />
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta content="" name="description" />
		<meta content="" name="author" />
		<!-- end: META -->
		<!-- start: MAIN CSS -->
		<link rel="stylesheet" href="/assets/backend/plugins/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="/assets/backend/plugins/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="/assets/backend/plugins/animate.css/animate.min.css">
		<link rel="stylesheet" href="/assets/backend/plugins/iCheck/skins/all.css">
		<link rel="stylesheet" href="/assets/backend/css/styles.css">
		<link rel="stylesheet" href="/assets/backend/css/styles-responsive.css">
		<link rel="stylesheet" href="/assets/backend/plugins/iCheck/skins/all.css">
		<!--[if IE 7]>
		<link rel="stylesheet" href="/assets/backend/plugins/font-awesome/css/font-awesome-ie7.min.css">
		<![endif]-->
		<!-- end: MAIN CSS -->
		<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
		<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
	</head>
	<!-- end: HEAD -->
	<!-- start: BODY -->
	<body class="login">
		<div class="row">
			<div class="main-login col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
				<div class="logo">
					<img src="/assets/backend/images/logo.png">
				</div>				
				<!-- start: FORGOT BOX -->
				<div class="box-login">
					<h3>Forget Password?</h3>
					<p>
						Enter your e-mail address below to reset your password.
					</p>
					<form class="form-forgot" method="post" action="/auth/kirim_forget_password">
						<div class="errorHandler alert alert-danger no-display">
							<i class="fa fa-remove-sign"></i> You have some form errors. Please check below.
						</div>
						<?=$message;?>
						<fieldset>
							<div class="form-group">
								<span class="input-icon">
									<input type="email" class="form-control" name="identity" placeholder="Email">
									<i class="fa fa-envelope"></i> </span>
							</div>
							<div class="form-actions">
								<a class="btn btn-light-grey go-back">
									<i class="fa fa-chevron-circle-left"></i> Log-In
								</a>
								<button type="submit" class="btn btn-green pull-right">
									Submit <i class="fa fa-arrow-circle-right"></i>
								</button>
							</div>
						</fieldset>
					</form>
					<!-- start: COPYRIGHT -->
					<div class="copyright">
						2014 &copy; Rapido by cliptheme.
					</div>
					<!-- end: COPYRIGHT -->
				</div>
				<!-- end: FORGOT BOX -->
				
			</div>
		</div>
		<!-- start: MAIN JAVASCRIPTS -->
		<!--[if lt IE 9]>
		<script src="/assets/backend/plugins/respond.min.js"></script>
		<script src="/assets/backend/plugins/excanvas.min.js"></script>
		<script type="text/javascript" src="/assets/backend/plugins/jQuery/jquery-1.11.1.min.js"></script>
		<![endif]-->
		<!--[if gte IE 9]><!-->
		<script src="/assets/backend/plugins/jQuery/jquery-2.1.1.min.js"></script>
		<!--<![endif]-->
		<script src="/assets/backend/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
		<script src="/assets/backend/plugins/bootstrap/js/bootstrap.min.js"></script>
		<script src="/assets/backend/plugins/iCheck/jquery.icheck.min.js"></script>
		<script src="/assets/backend/plugins/jquery.transit/jquery.transit.js"></script>
		<script src="/assets/backend/plugins/TouchSwipe/jquery.touchSwipe.min.js"></script>
		<script src="/assets/backend/js/main.js"></script>
		<!-- end: MAIN JAVASCRIPTS -->
		<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script src="/assets/backend/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
		<script src="/assets/backend/js/login.js"></script>
		<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script>
			jQuery(document).ready(function() {
				Main.init();
				Login.init();
				
				required = ["first_name","status","program_studi","nomor_induk"];
				errornotice = $("#error");
				emptyerror = "Mohon field ini diisi.";
				$('#simpan_form').click(function(){
					//Validate required fields
					for (i=0;i<required.length;i++) {
						var input = $('#'+required[i]);
						if ((input.val() == "") || (input.val() == emptyerror)) {
							input.addClass("needsfilled");
							input.val(emptyerror);
							errornotice.fadeIn(750);
						} else {
							input.removeClass("needsfilled");
						}
					}
						
					//if any inputs on the page have the class 'needsfilled' the form will not submit
					if ($(":input").hasClass("needsfilled")) {
						return false;
					} else {
						errornotice.hide();
						$('#frm_pegawai').submit();
					}
													  
				});
												
				$(":input").focus(function(){		
				   if ($(this).hasClass("needsfilled") ) {
						$(this).val("");
						$(this).removeClass("needsfilled");
					}
				});
				
			$("#emailReg").keyup(function(){
				var that = this,
				value = $(this).val();

				$.post('<?=site_url();?>/auth/cek_email',
				{
					'email': formRegister.email.value,
				},
				function(data) {
					$('#cek_email').html(data);
				}
						
				);
			});	
			
			$("#emailReg").focus(function(){
				var that = this,
				value = $(this).val();

				$.post('<?=site_url();?>/auth/cek_email',
				{
					'email': formRegister.email.value,
				},
				function(data) {
					$('#cek_email').html(data);
				}
						
				);
			});	
			
			});
		</script>
	</body>
	<!-- end: BODY -->
</html>