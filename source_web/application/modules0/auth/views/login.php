<?php
$this->load->helper('text');
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Aplikasi PPAS</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="/assets/components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="/assets/components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="/assets/components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/assets/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="/assets/plugins/iCheck/square/blue.css">
  <link rel="stylesheet" href="/0assets/css/modern-business.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
   <style type="text/css"> 
      body { background-color: Gray; } 
      #clocks-container { border: 1px groove White; border-radius: 15px; padding: 10px; width: 50%; margin: 30px auto; background-color: LightGray; text-align: center; } 
      /* SAMPLE CSS STYLES FOR JQUERY CLOCK PLUGIN */ 
      .jqclock { text-align:center; border: 2px #369 ridge; background-color: #69B; padding: 10px; margin:20px auto; width: 40%; box-shadow: 5px 5px 15px #005; } 
      .clockdate { color: DarkRed; font-weight: bold; background-color: #7AC; margin-bottom: 10px; font-size: 18px; display: block; padding: 5px 0; } 
      .clocktime { border: 2px inset DarkBlue; background-color: #444; padding: 5px 0; font-size: 14px; font-family: "Courier"; color: LightGreen; margin: 2px; display: block; font-weight:bold; text-shadow: 1px 1px 1px Black; } 
    </style> 
	<style type="text/css"> 
	html,
body {
    height: 100%;
}

body {
    padding-top: 50px; /* Required padding for .navbar-fixed-top. Remove if using .navbar-static-top. Change if height of navigation changes. */
}

.img-portfolio {
    margin-bottom: 30px;
}

.img-hover:hover {
    opacity: 0.8;
}

/* Home Page Carousel */

header.carousel {
    height: 80%;
}

header.carousel .item,
header.carousel .item.active,
header.carousel .carousel-inner {
    height: 100%;
}

header.carousel .fill {
    width: 50%;
    height: 50%;
    background-position: center;
    background-size: cover;
}

/* 404 Page Styles */

.error-404 {
    font-size: 100px;
}

/* Pricing Page Styles */

.price {
    display: block;
    font-size: 50px;
    line-height: 50px;
}

.price sup {
    top: -20px;
    left: 2px;
    font-size: 20px;
}

.period {
    display: block;
    font-style: italic;
}

/* Footer Styles */

footer {
    margin: 50px 0;
}

/* Responsive Styles */

@media(max-width:991px) {
    .customer-img,
    .img-related {
        margin-bottom: 30px;
    }
}

@media(max-width:767px) {
    .img-portfolio {
        margin-bottom: 15px;
    }

    header.carousel .carousel {
        height: 70%;
    }
}
	</style>
</head>
<body class="login-page">

	<a href="/"><h1 style="text-align:center"><b>Aplikasi PPAS</b></h1></a>
	<br>
<div class="row">
	<div class="col-md-12">
		
		<div class="col-md-4">	
		</div>
		<div class="col-md-4">		
					<div class="login-box-body">
						<div id="jqclock" class="jqclock"></div> 
						<p class="login-box-msg">Login</p>
						<form id="frm_login" class="form-login" method="post" action="/auth/login">
						  <div class="form-group has-feedback">
							<?php echo form_input($identity);?>
							<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
						  </div>
						  <div class="form-group has-feedback">
						   <?php echo form_input($password);?>
							<span class="glyphicon glyphicon-lock form-control-feedback"></span>
						  </div>
							<div class="form-group">
											<label for="exampleInputCaptcha">Kode Keamanan</label>
											<div class="row">
											<div class="col-md-6">
												<input required="required" type="text" name="captcha" class="form-control" id="exampleInputPassword1" placeholder="" value="" autocomplete="off"  >
											</div>
											<div class="col-md-6">
											<?php 
											$cap = make_captcha();
											?>
											<img src="<?=$cap;?>">
											</div>
											</div>
							</div>
							<div class="form-group">        
											  <div class="col-sm-12">
												<div id="error"><?=$message;?></div>
											  </div>
											</div>
						  <div class="row">
							<div class="col-xs-8">
							</div>
							<!-- /.col -->
							<div class="col-xs-4">
							  <a href="#" id="masuk" class="btn btn-primary btn-block btn-flat">Sign In</a>
							</div>
							<!-- /.col -->
						  </div>
						</form>					
					</div>
		</div>
		<div class="col-md-4">	
		</div>
	</div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="/assets/components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="/assets/components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="/assets/plugins/iCheck/icheck.min.js"></script>
<script type="text/javascript" src="/assets/plugins/jQuery-Clock-Plugin-master/jqClock.min.js"></script> 
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
<script language="JavaScript" type="text/javascript">
$(document).ready(function()
{
					$('#masuk').click(function(){
						$.post('<?=site_url();?>/auth/cek_captcha',
						{
							'captcha': frm_login.captcha.value,
						},
						function(data) {
						if(data == 'sukses')
							{
								$('#frm_login').submit();
							}
							else
							{
								$('#error').empty();
								$('#error').append('<div class="alert alert-danger">Kode Captcha Salah.</div>');
							}
							}
						); 
						
						//$('#frm_login').submit();
					});
	
    $("#jqclock").clock(
			{
				"langSet":"id"
			}
			
			); 
	
});
</script>
<script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>
</body>
</html>
