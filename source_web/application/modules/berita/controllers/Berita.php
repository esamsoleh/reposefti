<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Berita extends MX_Controller{

	var $userid;
	var $NIP;
	var $s_biro;
	var $s_access;
	public $apps;

	public function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
		if (!$this->ion_auth->logged_in())
			{
				redirect('/auth/login', 'refresh');
			}
		$this->apps = 'Administrasi';
		$this->s_biro = $this->session->userdata('s_biro');
		$this->nama_user = $this->ion_auth->user()->row()->first_name.' '.$this->ion_auth->user()->row()->last_name;
		$this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		$this->load->model('berita_model');

			
	}

	public function index(){
		
		$query=$this->berita_model->get_all();

		$data = array(
				'title' =>'Sistem Informasi Aparatur Kota Bekasi',
				'welcome_header' => 'Selamat Datang di Sistem Informasi Aparatur Kota Bekasi',
				'rows' => $query,
		);
		$this->load->view("index",$data);

	}

	public function tambah(){

		$query="";

		if(isset($_POST["simpan"])){

			$this->form_validation->set_rules("judul","Judul","required");
			$this->form_validation->set_rules("deskripsi","Deskripsi","required");

			if($this->form_validation->run()==FALSE){

					$this->session->set_flashdata('message_error', 'value');

			}else{

				$datainput["judul"] =  $this->input->post("judul");
				$datainput["deskripsi"] = $this->input->post("deskripsi");
				$datainput["penulis"] = $this->userid;
				$datainput["tanggal"] = date("Y-m-d H:i:s");	

				$config['upload_path'] = './assets/slider/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
			//	$config['max_width']  = '1724';
			//	$config['max_height']  = '1768';

				$this->load->library('upload', $config);

				//gambar upload 
				if(@$_FILES['gambar']['name'] != '') { 

					if (!$this->upload->do_upload("gambar"))
					{
						
						$this->session->set_flashdata('message_error',$this->upload->display_errors());
						redirect("berita/tambah");
					}
					else
					{
						$file = $this->upload->data();
						$datainput["gambar"] = $file["file_name"];
					}

				}

				$this->berita_model->tambah_data($datainput);

				$this->session->set_flashdata('message_success', 'data berhasil disimpan');

				redirect("berita/index");


			}


		}

		$data = array(
				'title' =>'Sistem Informasi Aparatur Kota Bekasi',
				'welcome_header' => 'Selamat Datang di Sistem Informasi Aparatur Kota Bekasi',
				'rows' => $query,
		);
		$this->load->view("form",$data);


	}

	public function edit($id){


		$query=$this->berita_model->single_query($id);

		if(isset($_POST["simpan"])){

			$this->form_validation->set_rules("judul","judul","required");
			$this->form_validation->set_rules("deskripsi","Deskripsi","required");

			if($this->form_validation->run()==FALSE){

					$this->session->set_flashdata("message_error",validation_errors());

			}else{


				$datainput["judul"] = $this->input->post("judul");
				$datainput["deskripsi"] = $this->input->post("deskripsi");
				$datainput["penulis"] = $this->userid;
				$datainput["tanggal"] = date("Y-m-d H:i:s");
				$this->session->set_flashdata("message_success","data berhasil disimpan");
				$update = $this->berita_model->edit_data($datainput,$id);	
				redirect("berita/index");

			}


		}	

		$data = array(
				'title' =>'Sistem Informasi Aparatur Kota Bekasi',
				'welcome_header' => 'Selamat Datang di Sistem Informasi Aparatur Kota Bekasi',
				'rows' => $query,
		);

		$this->load->view("form",$data);


	}

	public function hapus($id){

		$this->berita_model->hapus($id);
		$this->session->set_flashdata("message_success","data berhasil dihapus");
		redirect("berita/index");

	}


	
}