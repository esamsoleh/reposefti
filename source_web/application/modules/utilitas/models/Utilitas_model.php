<?php
class Utilitas_model extends CI_Model {
	
	
	
	public function __construct()
    {
            parent::__construct();
	}
	
	public function get_menu($page_row,$row_id)
	{	
		$sql = 	"SELECT a.*,
						(select namamenu from t_menu where menuid = a.parentid) as parent
					FROM t_menu a
				ORDER BY parentid asc, 
						urutan asc";
			if(($page_row != 0)){
			$sql.=" LIMIT $row_id,$page_row";
			}
		//print_r($sql);
		$query = $this->db->query($sql);
		return $query;
	}
	
	public function get_menu_front($page_row,$row_id)
	{	
		$sql = 	"SELECT a.*,
						(select namamenu from t_menu_front where menuid = a.parentid) as parent
					FROM t_menu_front a
				ORDER BY parentid asc, 
						urutan asc";
			if(($page_row != 0)){
			$sql.=" LIMIT $row_id,$page_row";
			}
		//print_r($sql);
		$query = $this->db->query($sql);
		return $query;
	}

	public function get_menu_detil($menuid){
		$sql = 	"SELECT menuid, namamenu, deskripsi, urutan, linkaction, tgltambah, parentid, aplikasiid FROM t_menu ".
				"WHERE menuid = $menuid";
		$query = $this->db->query($sql);
		//print_r($query->result_array());
		return $query;
	}
	
	/* public function get_user($page_row,$row_id)
	{	
		$sql = 	"SELECT a.*,c.name as groupname
					FROM t_users a
					LEFT JOIN t_users_groups b on(a.id = b.user_id)
					LEFT JOIN t_groups c on(b.group_id = c.id)
					ORDER BY c.id,a.id";
	
			if(($page_row != 0))
			{
				$sql.=" LIMIT $row_id,$page_row";
			}
		//print_r($sql);
		$query = $this->db->query($sql);
		return $query;
	
	} */
	
	public function get_user($page_row,$row_id)
	{	
		$sql = 	"SELECT a.*
					FROM t_users a
					ORDER BY a.id";
	
			if(($page_row != 0))
			{
				$sql.=" LIMIT $row_id,$page_row";
			}
		//print_r($sql);
		$query = $this->db->query($sql);
		return $query;
	
	}
	
	public function get_pendaftaran_user($page_row,$row_id)
	{	
		$sql = 	"SELECT tuser.userid, 
						pwd, 
						tanyalupapwd, 
						jawablupapwd, 
						email, 
						aktif, 
						tuser.kunker, 
						unkerja1.nunker nunker1, 
						unkerja2.nunker nunker2, 
						nip, 
						t_groups.groupid, 
						t_groups.groupname, 
						t_groups.deskripsi
					FROM tuser
					LEFT JOIN unkerja unkerja1 
					ON tuser.kunker = unkerja1.kunker 
					LEFT JOIN unkerja unkerja2 
					ON tuser.kkabkota = unkerja2.kunker 
					LEFT JOIN truserright 
					ON tuser.userid = truserright.userid 
					LEFT JOIN t_groups 
					ON truserright.groupid = t_groups.groupid
					where tuser.aktif = 0
					and t_groups.groupid = 36 
					ORDER BY t_groups.groupid,userid";
	
			if(($page_row != 0)){
			$sql.=" LIMIT $row_id,$page_row";
			}
		//print_r($sql);
		$query = $this->db->query($sql);
		return $query;
	
	}
	
	public function get_user_detil($id){
		$sql = 	"SELECT userid, pwd, tanyalupapwd, jawablupapwd, email, aktif, kunker,kkabkota FROM tuser ".
				"WHERE userid = '$id'";
		$query = $this->db->query($sql);
		//print_r($query->result_array());
		return $query;
	}
	
	public function get_pendaftaran_user_detil($id){
		$sql = 	"SELECT tuser.*,t_groups.groupname 
				FROM tuser 
				left join truserright 
				on (tuser.userid = truserright.userid)
				left join t_groups 
				on(truserright.groupid = t_groups.groupid)
				WHERE tuser.userid = '$id' and tuser.aktif = 0
					and t_groups.groupid = 36 ";
		$query = $this->db->query($sql);
		//print_r($query->result_array());
		return $query;
	}
	
	public function get_group($page_row,$row_id)
	{	
		$sql = 	"SELECT id, name, description FROM t_groups";
			if(($page_row != 0)){
			$sql.=" LIMIT $row_id,$page_row";
			}
		//print_r($sql);
		$query = $this->db->query($sql);
		return $query;
	
	}
	
	public function get_group_detil($id){
		$sql = 	"SELECT * FROM t_groups ".
				"WHERE id = '$id'";
		$query = $this->db->query($sql);
		//print_r($query->result_array());
		return $query;
	}
	
	public function get_data_user($page_row,$row_id)
	{	
		$sql = 	"SELECT tuser.userid,
						tuser.email,
						unkerja1.nunker nunker1, 
						unkerja2.nunker nunker2, 
						nip, 
						t_groups.groupname
					FROM tuser
					LEFT JOIN unkerja unkerja1 
					ON tuser.kunker = unkerja1.kunker 
					LEFT JOIN unkerja unkerja2 
					ON tuser.kkabkota = unkerja2.kunker 
					LEFT JOIN truserright 
					ON tuser.userid = truserright.userid 
					LEFT JOIN t_groups 
					ON truserright.groupid = t_groups.groupid
					ORDER BY t_groups.groupid,userid";
	
			if(($page_row != 0)){
			$sql.=" LIMIT $row_id,$page_row";
			}
		//print_r($sql);
		$query = $this->db->query($sql);
		return $query;
	
	}
	
	public function get_registrasi($page_row,$row_id)
	{	
		$sql = 	"SELECT a.*
					FROM t_users a
					where registrasi = '1'
					and processed = '0'
					and active = '0'
					ORDER BY a.id";
	
			if(($page_row != 0))
			{
				$sql.=" LIMIT $row_id,$page_row";
			}
		//print_r($sql);
		$query = $this->db->query($sql);
		return $query;
	
	}
	
}
