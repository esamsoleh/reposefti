<html>
<?php $this->load->view('header');?> 
<?php 
if(isset($row['namamenu']))
{
	$namamenu = $row['namamenu'];
}
elseif(isset($_POST['namamenu']))
{
	$namamenu = $this->db->escape_str($_POST['namamenu']);
}
else
{
	$namamenu = '';
}

if(isset($row['deskripsi']))
{
	$deskripsi = $row['deskripsi'];
}
elseif(isset($_POST['deskripsi']))
{
	$deskripsi = $this->db->escape_str($_POST['deskripsi']);
}
else
{
	$deskripsi = '';
}

if(isset($row['linkaction']))
{
	$linkaction = $row['linkaction'];
}
elseif(isset($_POST['linkaction']))
{
	$linkaction = $this->db->escape_str($_POST['linkaction']);
}
else
{
	$linkaction = '';
}

if(isset($row['urutan']))
{
	$urutan = $row['urutan'];
}
elseif(isset($_POST['urutan']))
{
	$urutan = $this->db->escape_str($_POST['urutan']);
}
else
{
	$urutan = '';
}

if(isset($row['parentid']))
{
	$parentid = $row['parentid'];
}
elseif(isset($_POST['parentid']))
{
	$parentid = $this->db->escape_str($_POST['parentid']);
}
else
{
	$parentid = '';
}

if(isset($row['aktif']))
{
	$aktif = $row['aktif'];
}
elseif(isset($_POST['aktif']))
{
	$aktif = $this->db->escape_str($_POST['aktif']);
}
else
{
	$aktif = 1;
}

?>

<body>
		<?php $this->load->view('sliding-bar');?>
		<div class="main-wrapper">
			<?php $this->load->view('topbar');?>
			<?php $this->load->view('vertikal-menu');?>
			<?php $this->load->view('page-slide-right');?>
			<!-- start: MAIN CONTAINER -->
			<div class="main-container inner">
				<!-- start: PAGE -->
				<div class="main-content">					
					<!-- end: SPANEL CONFIGURATION MODAL FORM -->
					<div class="container">
						<!-- start: PAGE HEADER -->
						<!-- start: TOOLBAR -->
						<div class="toolbar row">
							<div class="col-sm-6 hidden-xs">
								<div class="page-header">
									<h1><?=$title;?> <small><?=$subtitle;?></small></h1>
								</div>
							</div>
							
						</div>
						<!-- end: TOOLBAR -->
						<!-- end: PAGE HEADER -->
						<!-- start: BREADCRUMB -->
						<div class="row">
							<div class="col-md-12">
								<ol class="breadcrumb">
									<li class="active">
										<a href="/dashboard">
											Dashboard
										</a>
									</li>
								</ol>
							</div>
						</div>
						<!-- end: BREADCRUMB -->
						<!-- start: PAGE CONTENT -->
						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-white">
									<div class="panel-body">
										<?php echo validation_errors(); ?>
										<form name="frm_menu" id="frm_menu" method="post" action="<?=site_url().'/'.$this->uri->uri_string();?>">
										<div class="row">
											<div class="col-xs-3"><label>Nama Menu</label></div>
											<div class="col-xs-9">
												<input type="text" class="form-control" id="namamenu" name="namamenu" value="<?=set_value('namamenu',$namamenu);?>">
											</div>
										</div>
										<div class="row">
											<div class="col-xs-3"><label>Deskripsi Menu</label></div>
											<div class="col-xs-9">
												<input type="text" class="form-control" id="deskripsi" name="deskripsi" value="<?=set_value('deskripsi',($deskripsi));?>">
											</div>
										</div>
										<div class="row">
											<div class="col-xs-3"><label>Link</label></div>
											<div class="col-xs-9">
												<input type="text" class="form-control" id="linkaction" name="linkaction" value="<?=set_value('linkaction',($linkaction));?>">
											</div>
										</div>
										<div class="row">
											<div class="col-xs-3"><label>Parent</label></div>
											<div class="col-xs-9">
												<select name="parentid" id="parentid" class="selecter_1 form-control" >
													<option value="0">Root</option>
													<?php 
														get_option_parent(0,$parentid,5);
														function get_option_parent($menuid,$parentid,$fx)
														{
															$f=$fx+5;
															$nbsp = '';
															for($it = 1;$it<=$f;$it++)
															{
																$nbsp .= '&nbsp;';
															}
															$CI =& get_instance();
															$sql_menu = "select * from t_menu_front where parentid = $menuid ";
															$query_menu = $CI->db->query($sql_menu);
															foreach($query_menu->result_array() as $menu)
															{
																($menu['menuid'] == $parentid)?$selected = 'selected':$selected='';
																echo '<option value="'.$menu['menuid'].'" '.$selected.' >'.$nbsp.$menu['namamenu'].'</option>';
																get_option_parent($menu['menuid'],$parentid,$f);
															}
														}
													?>
												</select>		
											</div>
										</div>
										<div class="row">
											<div class="col-xs-3"><label>Urutan</label></div>
											<div class="col-xs-9">
												<input type="text" class="form-control datepick" id="urutan" name="urutan" value="<?=set_value('urutan',($urutan));?>">
											</div>
										</div>
										
										<div class="row">
											<div class="col-xs-3"><label>Keaktifan</label></div>
											<div class="col-xs-9">
												<select name="aktif" id="aktif" class="selecter_1 form-control" >
													<option value="1" <?=(($aktif==1)?'selected':'');?>>Aktif</option>
													<option value="0" <?=(($aktif==0)?'selected':'');?>>Tidak Aktif</option>
												</select>								
											</div>
										</div>
										
										</br>
										<div class="row">
											<div class="col-xs-6"><button id="simpan_form" class="btn btn-block btn-success" type="button">Simpan</button></div>
											<div class="col-xs-6"><button id="batal" class="btn btn-block btn-success" type="button">Batal</button></div>
										</div>
												</div>
											
								</div>
							</div>
						</div>
						<!-- end: PAGE CONTENT-->
					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>
				</div>
				<!-- end: PAGE -->
			</div>
			<!-- end: MAIN CONTAINER -->
			<?php $this->load->view('footer');?>
		</div>
		<?php $this->load->view('javascript')?>
		
		<script type="text/javascript">
			$(document).ready(function () {
				required = ["namamenu"];
				errornotice = $("#error");
				emptyerror = "Mohon field ini diisi.";
				$('#simpan_form').click(function(){
					//Validate required fields
					for (i=0;i<required.length;i++) {
						var input = $('#'+required[i]);
						if ((input.val() == "") || (input.val() == emptyerror)) {
							input.addClass("needsfilled");
							input.val(emptyerror);
							errornotice.fadeIn(750);
						} else {
							input.removeClass("needsfilled");
						}
					}
						
					//if any inputs on the page have the class 'needsfilled' the form will not submit
					if ($(":input").hasClass("needsfilled")) {
						return false;
					} else {
						errornotice.hide();
						$('#frm_menu').submit();
					}
													  
				});
												
				$(":input").focus(function(){		
				   if ($(this).hasClass("needsfilled") ) {
						$(this).val("");
						$(this).removeClass("needsfilled");
					}
				});
				
				$('#batal').click(function(){
					parent.history.back();
					return false;
				});
						
			});
			</script>
		
	</body>
	<!-- end: BODY -->
</html>


