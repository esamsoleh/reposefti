<?php 
if(isset($row['name']))
{
	$name = $row['name'];
}
elseif(isset($_POST['name']))
{
	$name = $this->db->escape_str($_POST['name']);
}
else
{
	$name = '';
}

if(isset($row['description']))
{
	$description = $row['description'];
}
elseif(isset($_POST['description']))
{
	$description = $this->db->escape_str($_POST['description']);
}
else
{
	$description = '';
}

if(isset($row['id']))
{
	$id = $row['id'];
}
elseif(isset($_POST['id']))
{
	$id = $this->db->escape_str($_POST['id']);
}
else
{
	$id = '';
}

?>

<?php $this->load->view('header');?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('top');?>
  <!-- Left side column. contains the logo and sidebar -->
  
  <?php $this->load->view('sidebar');?>
 
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
							<div class="col-md-12">
								<div class="panel panel-white">
									<div class="panel-body">
									<?php echo validation_errors(); ?>
									<form name="frm_pegawai" id="frm_pegawai" method="post" action="<?=site_url().'/'.$this->uri->uri_string();?>">
									<input type="hidden" name="groupid" value="<?=$id;?>">
									<div class="row">
										<div class="col-xs-6"><label>Nama Group</label></div>
										<div class="col-xs-6">
											<input type="text" class="form-control" id="name" name="name" value="<?=set_value('name',$name);?>">
										</div>
									</div>
									<div class="row">
										<div class="col-xs-6"><label>Deskripsi Group</label></div>
										<div class="col-xs-6">
											<textarea class="form-control" id="description" name="description"><?=set_value('description',$description);?></textarea>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-6"><label>Hak akses menu:</label></div>								
									</div>
									<div class="row">
										<div class="col-xs-12" id="group_table" style="overflow:scroll; height:350px">
										<table class="table table-bordered">
										<tr>
										<th rowspan="2" class="center">
											<div class="checkbox-table">
											<label>
												<input type="checkbox" class="flat-grey selectall" id="checkAll">
											</label>
											</div>
										</th>
										<th rowspan="2" style="text-align:center">Menu</th>
										<th colspan="5" style="text-align:center">Hak Akses</th>
										</tr>
										<tr>
										<th  style="text-align:center">Tambah</th>
										<th style="text-align:center">Update</th>
										<th style="text-align:center">Delete</th>
										<th style="text-align:center">View</th>
										<th style="text-align:center">Print</th>
										</tr>
										<?=group_tree(0,0,$id); ?>
										</table>
										<?php 
										function group_tree($fx,$parent,$kel)
												{
													$CI =& get_instance();
													$f=$fx+5;
													$result = '';
													$menus = $CI->db->query("select * from t_menu where parentid = $parent");
													foreach($menus->result_object() as $menu)
													{
														//print_r($menu);
														$sql1 = "SELECT * FROM t_groupmenu WHERE groupid = '" . $kel . "' AND menuid = ". $menu->menuid;
														//echo $sql1.'</br>';
														$stats = $CI->db->query($sql1);
														$numtemp = ($stats->num_rows());				
														if ($numtemp == 0) 
														{
															$status = "";
															$ad = "";
															$up = "";
															$de = "";
															$vi = "";
															$pr = "";
														} 
														else 
														{
															$status = " checked";
															foreach($stats->result_object() as $stat)
															{
																($stat->ad==1)?$ad = " checked":$ad="";
																($stat->up==1)?$up = " checked":$up="";
																($stat->de==1)?$de = " checked":$de="";
																($stat->vi==1)?$vi = " checked":$vi="";
																($stat->pr==1)?$pr = " checked":$pr="";
															}
														}
														$result .='<tr>';
														$result .='<td class="center">';
														
														$result .='<div class="checkbox-table">
																	<label>
																		<input name="id['.$menu->menuid.']" type="checkbox" value="'.$menu->menuid.'" '.$status.' class="flat-grey foocheck">
																	</label>
																	</div>
																</td>';
														$result .='<td>';
														for($it = 1;$it<=$f;$it++)
														{
															$result .= '&nbsp;';
														}
														$result .=$menu->namamenu.'</td>';
														$result .='<td class="center"><label><input name="A['.$menu->menuid.']" value="1" type="checkbox" '.$ad.' class="flat-grey foocheck">&nbsp;Tambah</label></td>';
														$result .='<td class="center"><label><input name="U['.$menu->menuid.']" value="1" type="checkbox" '.$up.' class="flat-grey foocheck">&nbsp;Update</label></td>';
														$result .='<td class="center"><label><input name="D['.$menu->menuid.']" value="1" type="checkbox" '.$de.' class="flat-grey foocheck">&nbsp;Hapus</label></td>';												
														$result .='<td class="center"><label><input name="V['.$menu->menuid.']" value="1" type="checkbox" '.$vi.' class="flat-grey foocheck">&nbsp;View</label></td>';
														$result .='<td class="center"><label><input name="P['.$menu->menuid.']" value="1" type="checkbox" '.$pr.' class="flat-grey foocheck">&nbsp;Print</label></td>';
														$result .='</tr>';
														$result .= group_tree($f,$menu->menuid,$kel);
													}
													return $result;
												}
												
										?>
										</div>								
									</div>
									<br>
									<div class="row">
										<div class="col-xs-6"><button id="simpan_form" class="btn btn-block btn-success" type="button">Simpan</button></div>
										<div class="col-xs-6"><button id="batal" class="btn btn-block btn-success" type="button">Batal</button></div>
									</div>
									</form>
								</div>
											
								</div>
							</div>
						</div>			

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
 
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('javascript');?>
	<script type="text/javascript">
		$(document).ready(function () {
			required = ["name"];
			errornotice = $("#error");
			emptyerror = "Mohon field ini diisi.";
			$('#simpan_form').click(function(){
				//Validate required fields
				for (i=0;i<required.length;i++) {
					var input = $('#'+required[i]);
					if ((input.val() == "") || (input.val() == emptyerror)) {
						input.addClass("needsfilled");
						input.val(emptyerror);
						errornotice.fadeIn(750);
					} else {
						input.removeClass("needsfilled");
					}
				}
					
				//if any inputs on the page have the class 'needsfilled' the form will not submit
				if ($(":input").hasClass("needsfilled")) {
					return false;
				} else {
					errornotice.hide();
					$('#frm_pegawai').submit();
				}
												  
			});
											
			$(":input").focus(function(){		
			   if ($(this).hasClass("needsfilled") ) {
					$(this).val("");
					$(this).removeClass("needsfilled");
				}
			});
			
			$('#batal').click(function(){
				parent.history.back();
				return false;
			});
			
			$("#group_table").scroll();
			
			$("#checkAll").change(function () {
				$("input:checkbox").prop('checked', $(this).prop("checked"));
			});	
		});
		</script>
</body>
</html>
