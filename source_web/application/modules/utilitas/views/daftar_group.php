<?php $this->load->view('header');?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('top');?>
  <!-- Left side column. contains the logo and sidebar -->
  
  <?php $this->load->view('sidebar');?>
 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title;?>
        <small> <?=$subtitle;?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
							<div class="col-md-12">
								<div class="panel panel-white">
									<div class="panel-body">
										<div class="row">
											<div class="col-xs-12">
											<button id="add" type="button" class="btn btn-success">
											  <span class="glyphicon glyphicon-plus"></span> Tambah Data
											</button>
											</br>
											</br>
											<table id="tabel" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
									
											<thead>
												<th style="text-align: center;">ID Group</th>
												<th style="text-align: center;">Nama Group</th>
												<th style="text-align: center;">Deskripsi Group</th>
												<th style="text-align: center;">Edit</th>
												<th style="text-align: center;">Hapus</th>
											</thead>
											<tbody>
											<?php
											$no=1;
											foreach($rows->result_array() as $row)
											{
												$url_edit= '/utilitas/group_edit/'.$row['id'];
												$url_del = '/utilitas/group_del/'.$row['id'];	
											
												echo '<tr>';
												echo '<td>'.$row['id'].'</td>';
												echo '<td>'.$row['name'].'</td>';
												echo '<td>'.$row['description'].'</td>';
												echo '<td>
																<a href="'.$url_edit.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="EDIT">
																	<i class="glyphicon glyphicon-edit"></i>
																</a>
														</td>';
														echo '<td>
																<a href="'.$url_del.'" class="btn btn-danger del" type="button" data-toggle="tooltip" data-placement="top" title="HAPUS">
																	<i class="glyphicon glyphicon-trash"></i>
																</a>
														</td>';
													echo '</tr>';
													$no++;								
											}
											?>
											</tbody>
											</table>
											</div>
										</div>
						
									</div>
								</div>
							</div>
						</div>
						

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
 
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('javascript');?>
<!-- DataTables -->
        <link href="/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/datatables/fixedHeader.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/datatables/scroller.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/datatables/dataTables.colVis.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/datatables/fixedColumns.dataTables.min.css" rel="stylesheet" type="text/css"/>
		<script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="/assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>

        <script src="/assets/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="/assets/plugins/datatables/buttons.bootstrap4.min.js"></script>

        <script src="/assets/plugins/datatables/jszip.min.js"></script>
        <script src="/assets/plugins/datatables/pdfmake.min.js"></script>
        <script src="/assets/plugins/datatables/vfs_fonts.js"></script>

        <script src="/assets/plugins/datatables/buttons.html5.min.js"></script>
        <script src="/assets/plugins/datatables/buttons.print.min.js"></script>

        <script src="/assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
        <script src="/assets/plugins/datatables/dataTables.keyTable.min.js"></script>
        <script src="/assets/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="/assets/plugins/datatables/responsive.bootstrap4.min.js"></script>
        <script src="/assets/plugins/datatables/dataTables.scroller.min.js"></script>
        <script src="/assets/plugins/datatables/dataTables.colVis.js"></script>
        <script src="/assets/plugins/datatables/dataTables.fixedColumns.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			$('#add').click(function(){
					window.location.assign("<?=site_url();?>/utilitas/group_add")
			});			
			$(".del").click(function(){
			if (!confirm("Do you want to delete")){
			  return false;
			}
			});
			
			$('#tabel').dataTable({
							"bFilter": true, 
							"bInfo": true,
							"bLengthChange": true,
							"pageLength":10
			});
		});
		</script>
</body>
</html>