<?php $this->load->view('header');?>
<!--documents-->
    <div class="container documents">
    <!--- content -->
     <!-- CENTER Content -->
		<div class="col-lg-12">
			<div class="panel panel-success" >
				  <div class="panel-heading">
					<h3 class="panel-title">Pengisian Data Pejabat Penanda Tangan Laporan </h3>
				  </div>
				  <?php $row = $rows->row_array();?>
				  <div class="panel-body">
						<form class="form-horizontal" name="fmrekap" id="fmrekap" method="post" action="<?=site_url();?>/utilitas/pejabat_save">
						<input type="hidden" name="id" value="<?=$row['id'];?>">
						<fieldset>
						<div class="row">
							<div class="col-md-8">
								<span class="label">Pejabat Penanda Tangan</span>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-md-4">
								<span class="label">Nama Jabatan</span>
							</div>
							<div class="col-md-8">
								<input name ="nama_jabatan" class="form-control" type="text" value="<?=$row['nama_jabatan'];?>">
							</div>
						</div>	
						<div class="row">
							<div class="col-md-4">
								<span class="label">Atas Nama </span>
							</div>
							<div class="col-md-8">
								<input name ="atas_nama_jabatan" class="form-control" type="text" value="<?=$row['atas_nama_jabatan'];?>">
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<span class="label">Nama Pejabat</span>
							</div>
							<div class="col-md-8">
								<input name ="nama_pejabat" class="form-control" type="text" value="<?=$row['nama_pejabat'];?>">
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<span class="label">Pangkat</span>
							</div>
							<div class="col-md-8">
								<input name ="pangkat" class="form-control" type="text" value="<?=$row['pangkat'];?>">
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<span class="label">Golongan Ruang </span>
							</div>
							<div class="col-md-8">
								<input name ="golruang" class="form-control" type="text" value="<?=$row['golruang'];?>">
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<span class="label">NIP</span>
							</div>
							<div class="col-md-8">
								<input name ="nip" class="form-control" type="text" value="<?=$row['nip'];?>">
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-md-8">
								<span class="label">Setting Penerbit Laporan </span>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-md-4">
								<span class="label">Tempat</span>
							</div>
							<div class="col-md-8">
								<input name ="tempat" class="form-control" type="text" value="<?=$row['tempat'];?>">
							</div>
						</div>	
						<div class="row">
							<div class="col-md-4">
								<span class="label">Penerbit Laporan Badan/Bagian/Biro Kepegawaian  </span>
							</div>
							<div class="col-md-8">
								<input name ="penerbit_laporan" class="form-control" type="text" value="<?=$row['penerbit_laporan'];?>">
							</div>
						</div>
						<div class="row">
							<div class="col-md-2">
								<input id="button1" class="btn btn-success btn-block" type="submit" value="Simpan">
							</div>
							<div class="col-md-2">
								<button id="button2" class="btn btn-success btn-block" type="button" value="Batal" onclick="window.history.back()">Batal
							</div>
						</div>
						</fieldset>
						</form>
						
				  </div>
			</div>			
		</div>
	</div>
<?php $this->load->view('footer');?>