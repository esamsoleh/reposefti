<?php $this->load->view('header');?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('top');?>
  <!-- Left side column. contains the logo and sidebar -->
  
  <?php $this->load->view('sidebar');?>
 
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-white">								
									<div class="panel-body">
										<?php echo validation_errors(); ?>
										<?php //echo ((isset($message) && $message != '')?$message:''); ?>
										<form name="frm_util" id="frm_util" method="post" action="<?=site_url().'/'.$this->uri->uri_string();?>">
										<div class="row">
											<div class="col-xs-6"><label>Username</label></div>
											<div class="col-xs-6">
												<?php echo form_input($identity);?>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-6"><label>Nama Depan</label></div>
											<div class="col-xs-6">
												<?php echo form_input($first_name);?>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-6"><label>Nama Belakang</label></div>
											<div class="col-xs-6">
												 <?php echo form_input($last_name);?>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-6"><label>No Telepon</label></div>
											<div class="col-xs-6">
											<?php 
												echo form_input($phone);
											?>
											</div>
										</div>
										
										<div class="row">
											<div class="col-xs-6"><label>Email</label></div>
											<div class="col-xs-6">
											<?php
												echo form_input($email);
											?>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-6"><label>NIP</label></div>
											<div class="col-xs-6">
											<?php
												echo form_input($nip);
											?>
											</div>
										</div>
										
										<div class="row">
											<div class="col-xs-6"><label>Password</label></div>
											<div class="col-xs-6">
											<?php echo form_input($password);?>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-6"><label>Kofirmasi Password </label></div>
											<div class="col-xs-6">
											 <?php echo form_input($password_confirm);?>
											</div>
										</div>
										
										<div class="row">
											<div class="col-xs-6"><label>Grup</label></div>
											<div class="col-xs-6">
												<select name="groups[]" class="form-control">
												<?php foreach ($groups as $group):?>
													<option value="<?php echo $group['id'];?>"><?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?></option>												
												<?php endforeach?>
												</select>
											</div>
										</div>
										
										<div class="row">
											<div class="col-xs-6"><label>Keaktifan</label></div>
											<div class="col-xs-6">
												<select name="active" id="active" class="form-control" >
													<option value="0" >Tidak Aktif</option>
													<option value="1" >Aktif</option>
												</select>								
											</div>
										</div>
										</br>
										<div class="row">
											<div class="col-xs-6"><button id="simpan_form" class="btn btn-block btn-success" type="button">Simpan</button></div>
											<div class="col-xs-6"><button id="batal" class="btn btn-block btn-success" type="button">Batal</button></div>
										</div>
									</div>
								</div>
			</div>
			<div class="col-xs-1">
			</div>
		</div>			

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
 
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('javascript');?>
	<script src="<?=base_url();?>assets/js/jquery.chainedSelects.js"></script>
		<script type="text/javascript">
		$(document).ready(function () {
			required = ["identity","nip","email","first_name","last_name","password","password_confirm"];
			errornotice = $("#error");
			emptyerror = "Mohon field ini diisi.";
			$('#simpan_form').click(function(){
				//Validate required fields
				for (i=0;i<required.length;i++) {
					var input = $('#'+required[i]);
					if ((input.val() == "") || (input.val() == emptyerror)) {
						input.addClass("needsfilled");
						input.val(emptyerror);
						errornotice.fadeIn(750);
					} else {
						input.removeClass("needsfilled");
					}
				}
					
				//if any inputs on the page have the class 'needsfilled' the form will not submit
				if ($(":input").hasClass("needsfilled")) {
					return false;
				} else {
					errornotice.hide();
					$('#frm_util').submit();
				}
												  
			});
											
			$(":input").focus(function(){		
			   if ($(this).hasClass("needsfilled") ) {
					$(this).val("");
					$(this).removeClass("needsfilled");
				}
			});
			
			$('#batal').click(function(){
				parent.history.back();
				return false;
			});
			
		$('#unit').chainSelect('#subunit','<?=site_url();?>/utilitas/combobox_unit',
		{ 
			before:function (target) //before request hide the target combobox and display the loading message
			{ 
				$("#loading").css("display","block");
				$(target).css("display","none");
			},
			after:function (target) //after request show the target combobox and hide the loading message
			{ 
				$("#loading").css("display","none");
				$(target).css("display","inline");
			}
		});
		$('#subunit').chainSelect('#ssubunit','<?=site_url();?>/utilitas/combobox_unit',
		{ 
			before:function (target) //before request hide the target combobox and display the loading message
			{ 
				$("#loading").css("display","block");
				$(target).css("display","none");
			},
			after:function (target) //after request show the target combobox and hide the loading message
			{ 
				$("#loading").css("display","none");
				$(target).css("display","inline");
			}
		});
		$('#ssubunit').chainSelect('#skpd_kewenangan','<?=site_url();?>/utilitas/combobox_unit',
		{ 
			before:function (target) //before request hide the target combobox and display the loading message
			{ 
				$("#loading").css("display","block");
				$(target).css("display","none");
			},
			after:function (target) //after request show the target combobox and hide the loading message
			{ 
				$("#loading").css("display","none");
				$(target).css("display","inline");
			}
		});
		});
		</script>
</body>
</html>
