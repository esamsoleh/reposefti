<?php $this->load->view('header');?> 
<link href="<?=base_url();?>assets/js/jtable/themes/metro/green/jtable.min.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url();?>assets/js/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
<script src="<?=base_url();?>assets/js/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>assets/js/jtable/jquery.jtable.min.js" type="text/javascript"></script>
<script type="text/javascript">
         $(document).ready(function () {
			$('#ReferensiContainer').jtable({
                    title: "Detail Log Pengguna <b><?=$userid;?></b>",
                    paging: true, //Enable paging
                    pageSize: 10, //Set page size (default: 10)
                    sorting: true, //Enable sorting
                    defaultSorting: 'groupid desc', //Set default sorting
					selecting: true,
					selectOnRowClick: true, 
                    actions: {
                        listAction: '<?=site_url();?>/utilitas/list_detail_log/<?=$userid;?>',
                    },
                    fields: {
						userid: {
						title: 'Userid',
						width: '5%',
						paging: true, //Enable paging
						pageSize: 10, //Set page size (default: 10)
						sorting: true, //Enable sorting						
						defaultSorting: 'id_log desc', //Set default sorting
						},
						
						aktivitas: {
								title: 'Aktivitas',
								list: true
							},
						waktu: {
								title: 'Waktu',
								list: true,
							},
						ip_user: {
								title: 'IP',
								list: true,
							},
						
					},
					
					//Register to selectionChanged event to hanlde events
					selectionChanged: function () {
						//Get all selected rows
						var $selectedRows = $('#ReferensiContainer').jtable('selectedRows');		 
					//	$('#SelectedRowList').empty();
						if ($selectedRows.length > 0) {
							//Show selected rows
							$selectedRows.each(function () {
								var record = $(this).data('record');
								window.open('<?=site_url();?>/utilitas/detail_log_pengguna/'+record.userid);
							});
						}
					},
					
				});
			
			$('#load_cari_data').click(function (e) {
				e.preventDefault();
				$('#ReferensiContainer').jtable('load', {
					namacari: $('#namacari').val()
				});
			});
			//Load list from server
			$('#ReferensiContainer').jtable('load');
				
        });
    </script>
    <div class="container-fluid documents">
    <!--- content -->
     <!-- CENTER Content -->
		<div class="col-lg-12">
			<div class="panel panel-success">
				  <div class="panel-heading">
					<h3 class="panel-title">Daftar Log Pengguna</h3>
				  </div>
				  <div class="panel-body">
					<form>
					<div class="col-xs-4">
						<input class="form-control" type="text" name="namacari" id="namacari" placeholder="Nama/NIP Pegawai"/>
					</div>
					<div class="col-xs-4">
						<button class="btn btn-success" type="submit" id="load_cari_data">Cari</button>
					</div>
					</form>
					</br>
					</br>
					<div id="ReferensiContainer"></div>
				  </div>
			</div>
		</div>
         
	</div>
<?php $this->load->view('footer');?> 
