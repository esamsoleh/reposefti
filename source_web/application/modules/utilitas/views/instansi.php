<?php $this->load->view('header');?>
<script type="text/javascript" src="<?=base_url();?>assets/js/jquery.chainedSelects.js"></script>
<script language="JavaScript" type="text/javascript">
$(document).ready(function()
{			
	$('#unit').chainSelect('#subunit','<?php echo base_url();?>index.php/utilitas/combobox_unit',
	{ 
		before:function (target) //before request hide the target combobox and display the loading message
		{ 
			$("#loading").css("display","block");
			$(target).css("display","none");
		},
		after:function (target) //after request show the target combobox and hide the loading message
		{ 
			$("#loading").css("display","none");
			$(target).css("display","inline");
		}
	});	
	
	$('#button1').click(function(){
					$.ajax({
							 type: "GET",
							url: "<?=base_url();?>index.php/laporan/view_rekap",
							 beforeSend: function () {
								$("#loadlog").html('<img src=<?=base_url();?>assets/images/wait.gif />');
								$("#hasil_rekap").hide();
								},
							data: 
							{
							'unit1': fmrekap.unit1.value,
							'unit2': fmrekap.unit2.value,
							'unit3': fmrekap.unit3.value,
							'tahun': fmrekap.tahun.value,
							'tipe': 1
							}
							}).done(function(data) {
								$("#loadlog").html(''); 
								$('#hasil_rekap').html(data);
								$("#hasil_rekap").show();
							});		
	});
				
	$('#button2').click(function(){
					$.ajax({
							 type: "GET",
							url: "<?=base_url();?>index.php/laporan/view_rekap",
							 beforeSend: function () {
								$("#loadlog").html('<img src=<?=base_url();?>assets/images/wait.gif />');
								$("#hasil_rekap").hide();
								},
							data: 
							{
							'unit1': fmrekap.unit1.value,
							'unit2': fmrekap.unit2.value,
							'unit3': fmrekap.unit3.value,
							'tahun': fmrekap.tahun.value,
							'tipe': 2
							}
							}).done(function(data) {
								$("#loadlog").html(''); 
								$('#hasil_rekap').html(data);
								$("#hasil_rekap").show();
							});
	});
				
});
</script>
<!--documents-->
    <div class="container documents">
    <!--- content -->
     <!-- CENTER Content -->
		<div class="col-xs-12">
			<div class="panel panel-success" >
				  <div class="panel-heading">
					<h3 class="panel-title">Provinsi Jawa Barat</h3>
				  </div>
				  <div class="panel-body">
						<form class="form-horizontal" name="fmrekap" id="fmrekap">
						<fieldset>
						<div class="row">
							<div class="col-xs-2">
								<label>Unit</label>
							</div>
								<?php
									$s_biro = $this->session->userdata('s_biro');
									if (!empty($s_biro)) 
										{
											$sql = "SELECT kunker,nunker FROM unkerja WHERE kunker = '$unit'";
										} 
										else 
										{
											$sql = "SELECT kunker,nunker FROM unkerja WHERE MID(kunker,1,2) = '10' AND MID(kunker,5,6) = '000000'";
										}
										$rs = $this->db->query($sql);
								?>
							<div class="col-xs-4">
								<select class="form-control" name="unit" id="unit" >
								 <?	if (empty($s_biro)) 
									{ ?>
										<option id="1000000000" value="1000000000">Semua</option>
								<? } ?>
								<?
									foreach($rs->result_array() as $field)
									{
										echo ("<option id=\"$field[kunker]\" value=\"$field[kunker]\">$field[nunker]</option>\n");
									}
								?>
								 </select>
							</div>
						</div>	
						<div class="row">
							<div class="col-xs-2">						
								<label>Sub Unit</label>
							</div>
							<div class="col-xs-4">
								<select class="form-control" name="subunit" id="subunit" >
								<option>Semua</option>
								</select>
							</div>
						</div>
						</br>
						<div class="row">
							<div class="col-xs-2">
								<button id="button1" class="btn btn-success btn-block" type="button">Simpan</button>
							</div>
							<div class="col-xs-2">
								<button id="button2" class="btn btn-success btn-block" type="button">Batal</button>
							</div>
						</div>
						</fieldset>
						</form>
						
				  </div>
			</div>			
		</div>
	</div>
<?php $this->load->view('footer');?>