<html>
<?php $this->load->view('header');?> 
<body>
		<?php $this->load->view('sliding-bar');?>
		<div class="main-wrapper">
			<?php $this->load->view('topbar');?>
			<?php $this->load->view('vertikal-menu');?>
			<?php $this->load->view('page-slide-right');?>
			<!-- start: MAIN CONTAINER -->
			<div class="main-container inner">
				<!-- start: PAGE -->
				<div class="main-content">					
					<!-- end: SPANEL CONFIGURATION MODAL FORM -->
					<div class="container">
						<!-- start: PAGE HEADER -->
						<!-- start: TOOLBAR -->
						<div class="toolbar row">
							<div class="col-sm-6 hidden-xs">
								<div class="page-header">
									<h1><?=$title;?> <small><?=$subtitle;?></small></h1>
								</div>
							</div>
							
						</div>
						<!-- end: TOOLBAR -->
						<!-- end: PAGE HEADER -->
						<!-- start: BREADCRUMB -->
						<div class="row">
							<div class="col-md-12">
								<ol class="breadcrumb">
									<li class="active">
										<a href="/dashboard">
											Dashboard
										</a>
									</li>
								</ol>
							</div>
						</div>
						<!-- end: BREADCRUMB -->
						<!-- start: PAGE CONTENT -->
						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-white">
								
									<div class="panel-body">
										<?php echo validation_errors(); ?>
										
										<div class="row">
											<div class="col-xs-3"><label>Nama Depan</label></div>
											<div class="col-xs-6">
												<?php echo $user->first_name;?>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-3"><label>Nama Belakang</label></div>
											<div class="col-xs-6">
												 <?php echo $user->last_name;?>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-3"><label>Nomor Induk (Dosen / Tenaga Non Kependidikan / Mahasiswa / Alumni</label></div>
											<div class="col-xs-6">
												 <?php echo $user->nomor_induk;?>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-3"><label>Email</label></div>
											<div class="col-xs-6">
												<?php echo $user->email;?>
											</div>
										</div>										
																				
										<div class="row">
											<div class="col-xs-3"><label><?php echo lang('edit_user_groups_heading');?></label></div>
											<div class="col-xs-6">
												
												<?php foreach ($groups as $group):?>													
													<?php
													  $gID=$group['id'];
													  $checked = null;
													  $item = null;
													  foreach($currentGroups as $grp) 
													  {
														  if ($gID == $grp->id) 
														  {
																$checked= ' checked';
																break;
														  }
													  }
													?>
													<div class="checkbox">
													<label>
													<input disabled class="grey" type="checkbox" name="groups[]" value="<?php echo $group['id'];?>"<?php echo $checked;?>>
													 <?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?>
													 </label>
													 </div>
													<?php endforeach?>
												
											</div>
										</div>
										<?php echo form_hidden('id', $user->id);?>
										</br>
										<form name="frm_util" id="frm_util" method="post" action="<?=site_url().'/'.$this->uri->uri_string();?>">
										<div class="row">
											<div class="col-xs-3"><label>Keputusan</label></div>
											<div class="col-xs-6">
												 <select name="keputusan" class="form-control" id="form-control">
													<option value="1">Disetujui</option>
													<option value="0">Tidak Disetujui</option>
												 </select>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-3"><button id="simpan_form" class="btn btn-block btn-success" type="button">Simpan</button></div>
											<div class="col-xs-6"><button id="batal" class="btn btn-block btn-success" type="button">Batal</button></div>
										</div>
										</form>
									</div>
								</div>
							</div>
						<div class="col-xs-1">
						</div>
						</div>
					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>
				</div>
				<!-- end: PAGE -->
			</div>
			<!-- end: MAIN CONTAINER -->
			<?php $this->load->view('footer');?>
		</div>
		<?php $this->load->view('javascript')?>
		<script src="<?=base_url();?>assets/js/jquery.chainedSelects.js"></script>
		<script type="text/javascript">
		$(document).ready(function () {
			required = ["first_name","last_name","email","password","password_confirm"];
			errornotice = $("#error");
			emptyerror = "Mohon field ini diisi.";
			$('#simpan_form').click(function(){
				//Validate required fields
				for (i=0;i<required.length;i++) {
					var input = $('#'+required[i]);
					if ((input.val() == "") || (input.val() == emptyerror)) {
						input.addClass("needsfilled");
						input.val(emptyerror);
						errornotice.fadeIn(750);
					} else {
						input.removeClass("needsfilled");
					}
				}
					
				//if any inputs on the page have the class 'needsfilled' the form will not submit
				if ($(":input").hasClass("needsfilled")) {
					return false;
				} else {
					errornotice.hide();
					$('#frm_util').submit();
				}
												  
			});
											
			$(":input").focus(function(){		
			   if ($(this).hasClass("needsfilled") ) {
					$(this).val("");
					$(this).removeClass("needsfilled");
				}
			});
			
			$('#batal').click(function(){
				parent.history.back();
				return false;
			});
			
				/* $('#unit').chainSelect('#skpd_kewenangan','<?=site_url();?>/utilitas/combobox_unit',
				{ 
					before:function (target) //before request hide the target combobox and display the loading message
					{ 
						$("#loading").css("display","block");
						$(target).css("display","none");
					},
					after:function (target) //after request show the target combobox and hide the loading message
					{ 
						$("#loading").css("display","none");
						$(target).css("display","inline");
					}
				}); */
		});
		</script>
</html>