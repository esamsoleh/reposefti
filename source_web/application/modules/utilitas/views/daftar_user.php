<?php $this->load->view('header');?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('top');?>
  <!-- Left side column. contains the logo and sidebar -->
  
  <?php $this->load->view('sidebar');?>
 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title;?>
        <small> <?=$subtitle;?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
							<div class="col-md-12">
								<div class="panel panel-white">
								  <div class="panel-body">
									<div class="row">
												<div class="col-xs-12">
												
												<button id="add" type="button" class="btn btn-success">
												  <span class="glyphicon glyphicon-plus"></span> Tambah Data
												</button>
												
												<table id="tabel" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
									
												<thead>
													<th style="text-align: center;">No</th>
													<th style="text-align: center;">Username</th>
													<th style="text-align: center;">Nama Lengkap</th>
													<th style="text-align: center;">NIP</th>
													<th style="text-align: center;">Email</th>
													<th style="text-align: center;">Grup</th>
													<th style="text-align: center;">Waktu Login Terakhir</th>
													<th style="text-align: center;">Keaktifan</th>
													<th style="text-align: center;">Edit</th>
													<th style="text-align: center;">Ganti Password</th>
													<th style="text-align: center;">Hapus</th>
												</thead>
												<tbody>
												<?php
												$no=1;
												foreach($rows->result_array() as $row)
												{
														
													$url_edit= '/utilitas/user_edit/'.$row['id'];
													$url_del = '/utilitas/del_user/'.$row['id'];	
													$url_pass = '/utilitas/ganti_password/'.$row['id'];															
														
													echo '<tr>';
													echo '<td>'.$no.'</td>';
													echo '<td>'.$row['username'].'</td>';
													echo '<td>'.$row['first_name'].'-'.$row['last_name'].'</td>';
													echo '<td>'.$row['nip'].'</td>';
													echo '<td>'.$row['email'].'</td>';
													echo '<td>'.grup_pengguna($row['id']).'</td>';
													echo '<td>'.(($row['last_login'] != NULL)?date('d-m-Y H:i:s',($row['last_login'])):'').'</td>';
													echo '<td>';
																	if(isset($row['active']) && $row['active']==1)
																	{
																		echo '<a class="activate" href="/utilitas/deactivate/'.$row['id'].'">Aktif</a>';
																	}
																	else
																	{
																		echo '<a class="activate" href="/utilitas/activate/'.$row['id'].'">Tidak Aktif</a>';
																	}
													echo '</td>';
													echo '<td>
																	<a href="'.$url_edit.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="EDIT">
																		<i class="glyphicon glyphicon-edit"></i>
																	</a>
															</td>';
													echo '<td>
																	<a href="'.$url_pass.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="EDIT">
																		<i class="glyphicon glyphicon-edit"></i>
																	</a>
															</td>';
													echo '<td>
																	<a href="'.$url_del.'" class="btn btn-danger del" type="button" data-toggle="tooltip" data-placement="top" title="HAPUS">
																		<i class="glyphicon glyphicon-trash"></i>
																	</a>
															</td>';
													echo '</tr>';
													$no++;								
												}
												?>
												</tbody>
												</table>
												</div>
									</div>
								  </div>
								</div>
							</div>
         
						</div>
						

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
 
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('javascript');?>
	<!-- DataTables -->
        <link href="/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/datatables/fixedHeader.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/datatables/scroller.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/datatables/dataTables.colVis.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/datatables/fixedColumns.dataTables.min.css" rel="stylesheet" type="text/css"/>
		<script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="/assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>

        <script src="/assets/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="/assets/plugins/datatables/buttons.bootstrap4.min.js"></script>

        <script src="/assets/plugins/datatables/jszip.min.js"></script>
        <script src="/assets/plugins/datatables/pdfmake.min.js"></script>
        <script src="/assets/plugins/datatables/vfs_fonts.js"></script>

        <script src="/assets/plugins/datatables/buttons.html5.min.js"></script>
        <script src="/assets/plugins/datatables/buttons.print.min.js"></script>

        <script src="/assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
        <script src="/assets/plugins/datatables/dataTables.keyTable.min.js"></script>
        <script src="/assets/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="/assets/plugins/datatables/responsive.bootstrap4.min.js"></script>
        <script src="/assets/plugins/datatables/dataTables.scroller.min.js"></script>
        <script src="/assets/plugins/datatables/dataTables.colVis.js"></script>
        <script src="/assets/plugins/datatables/dataTables.fixedColumns.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function () {
		$('#add').click(function(){
				window.location.assign("/utilitas/user_add/")
		});	
		
		$(".del").click(function(){
			if (!confirm("Do you want to delete")){
			  return false;
			}
		});
	  
	  $('#tabel').dataTable({
							"bFilter": true, 
							"bInfo": true,
							"bLengthChange": true,
							"pageLength":10
		});
		
		$(".activate").click(function(){
			if (!confirm("Yakin Merubah Status")){
			  return false;
			}
		});
							
	});
	</script>
</body>
</html>

<?php
function grup_pengguna($id)
{
	$CI = $CI =& get_instance();
	$s = "select a.*,b.name 
			from t_users_groups a
			left join t_groups b on(a.group_id = b.id)
			where a.user_id = '$id'
		";
	$q = $CI->db->query($s);
	$res = '';
	if($q->num_rows() > 0)
	{
		foreach($q->result_array() as $row)
		{
			$res .= $row['name'];
		}
	}
	return $res;
}

function get_kewenangan($id)
{
	$CI = $CI =& get_instance();
	$s = "select a.skpd_kewenangan,
				CASE mid(a.skpd_kewenangan,3,2)
					WHEN 10 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = mid(a.skpd_kewenangan,1,6) and mid(kunker,7,6) = '000000' 	)
					 WHEN 11 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = mid(a.skpd_kewenangan,1,6) and mid(kunker,7,6) = '000000' ) 
					 WHEN 00 then 'Semua'
					 ELSE (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,4) = mid(a.skpd_kewenangan,1,4) and mid(kunker,5,8) = '00000000' )
				END as nama_unit_kerja
			from t_users a
			where a.id = '$id'
		";
	$q = $CI->db->query($s);
	$res = '';
	if($q->num_rows() > 0)
	{
		foreach($q->result_array() as $row)
		{
			$res .= $row['nama_unit_kerja'];
		}
	}
	return $res;
}
?>