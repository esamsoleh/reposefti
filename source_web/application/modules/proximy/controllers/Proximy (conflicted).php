<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Proximy extends CI_Controller {
	public $apps;
	var $nama_user;
	var $file_foto;
	var $s_biro;
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
		if (!$this->ion_auth->logged_in())
			{
				redirect('/auth/login', 'refresh');
			}
		$this->apps = 'dashboard';
		$this->s_biro = $this->session->userdata('s_biro');
		$this->nama_user = $this->ion_auth->user()->row()->first_name.' '.$this->ion_auth->user()->row()->last_name;
		$this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	}
	
	public function index()
	{
		
		$data = array(
			'title' => 'Dashboard',
		);
		$this->load->view('index',$data);
	}
	
	public function login()
	{
		$url = 'https://api.proximi.fi/core_auth/login';
		$fields = array(
			'email' => 'arwah.design.001@gmail.com',
			'password' => 'Rtm1234',
		);
		$fields_string = '';
		foreach($fields as $key=>$value) 
		{ 
			$fields_string .= $key.'='.$value.'&'; 
		}
		rtrim($fields_string, '&');

		$ch = curl_init();

		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, TRUE);

		//execute post
		$result = curl_exec($ch);

		//close connection
		curl_close($ch);
		
		$result = json_decode($result,TRUE);
		echo '</br>';
		print_r($result);
		echo '</br>';
		echo $result['user']['name'];
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */