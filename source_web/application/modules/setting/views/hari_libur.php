<?php $this->load->view('header');?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('top');?>
  <!-- Left side column. contains the logo and sidebar -->
  
  <?php $this->load->view('sidebar');?>
 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title;?>
        <small><?=$subtitle;?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">		
		<div class="row">
		
			<div class="col-xs-12">
				<div class="panel panel-success">
                    <div class="panel-heading">
                        Daftar Hari Libur
                    </div>
					<div class="panel-body">
						<form>
									<div class="col-lg-4">
										<input class="form-control" type="text" name="namacari" id="namacari" placeholder="Kata Kunci">
									</div>
									<div class="col-lg-4">
										<button class="btn btn-success" type="submit" id="load_cari_data">Cari</button>
									</div>
						</form>
						</br>
						</br>
						<div id="ReferensiContainer"></div>					
					</div>
				</div>
			</div>
		
		</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
 
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('javascript');?>

<script src="/assets/js/jtable/jquery.jtable.min.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$('#ReferensiContainer').jtable({
                    title: "Daftar Hari Libur",
                    paging: true, //Enable paging
                    pageSize: 100, //Set page size (default: 10)
                    sorting: true, //Enable sorting
                    defaultSorting: 'id desc', //Set default sorting
                    actions: {
                        listAction: '/setting/list_libur/',
                        createAction: '/setting/add_libur/',
                        updateAction: '/setting/update_libur/',
                        deleteAction: '/setting/del_libur/',
					
                    },
                    fields: {
					
					id:{
						key: true,
						width:'10%',
						create:false,
						edit:false,
						title:'ID',
						list:true,
					},
					tanggal:{
						create:true,
						edit:true,
						title:'Tanggal',
						list:true,
						type: 'date',
						displayFormat: 'dd-mm-yy'
					},
					keterangan:{
						create:true,
						edit:true,
						title:'Keterangan',
						list:true,
						type: 'textarea',
					},
			
					},
					
			});
			
			$('#load_cari_data').click(function (e) {
				e.preventDefault();
				$('#ReferensiContainer').jtable('load', {
					namacari: $('#namacari').val()
				});
			});
			//Load list from server
			$('#ReferensiContainer').jtable('load');
			
		});
		</script>
</body>
</html>
