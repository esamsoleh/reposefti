<?php $this->load->view('header');?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('top');?>
  <!-- Left side column. contains the logo and sidebar -->
  
  <?php $this->load->view('sidebar');?>
 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title;?>
        <small><?=$subtitle;?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-white">
					<div class="panel-body">
						<?php 
						if($hak_akses['ad'] == 1 || $hak_akses['up'] == 1)
						{
						?>
							<span id="edit_form" class="glyphicon glyphicon-edit btn btn-warning" aria-hidden="true">EDIT</span>
						<?php 
						}
						?>
						<span id="batal_edit" class="glyphicon glyphicon-edit btn btn-info" aria-hidden="true"> BATAL</span>
						<span id="simpan_form" class="glyphicon glyphicon-floppy-disk btn btn-success" aria-hidden="true"> SIMPAN</span>
						</br>
						</br>
						<form name="formIdentitas" id="formIdentitas" method="post" action="<?=site_url().'/'.$this->uri->uri_string();?>" class="form-horizontal">
							<input type="hidden" name="cari" value="1">
							<div class="row">
								<div class="col-xs-3">
									<label>Tanggal Maksimal Pelaporan Per Bulan</label>
								</div>
								<div class="col-xs-9">
									<input disabled class="form-control" type="text" id="tanggal" name="tanggal" value="<?=$tanggal;?>">
								</div>
							</div>									
						</form>	
							
					</div>
				</div>
			</div>
			<div class="col-xs-1">
			</div>
		</div>
		
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
 
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('javascript');?>
<script src="/assets/js/jquery.chainedSelects.js"></script>
<script src="/assets/js/dataTables/jquery.dataTables.js"></script>
<script src="/assets/js/dataTables/dataTables.bootstrap.js"></script>
<script type="text/javascript">
$(document).ready(function () {
	$("#batal_edit").hide();
	$("#simpan_form").hide();
	$('#edit_form').click(function (e) {
			$("#edit_form").hide();
			$("#batal_edit").show();
			$("#simpan_form").show();
			$("#tanggal").attr("disabled", false);
		});
	$('#batal_edit').click(function (e) {
			document.formIdentitas.reset();
			$("#edit_form").show();
			$("#batal_edit").hide();
			$("#simpan_form").hide();
			$("#tanggal").attr("disabled", true);
		});
		
	required = ["tanggal"];
	errornotice = $("#error");
	emptyerror = "Mohon field ini diisi.";
	$('#simpan_form').click(function(){
		//Validate required fields
		for (i=0;i<required.length;i++) {
			var input = $('#'+required[i]);
			if ((input.val() == "") || (input.val() == emptyerror)) {
				input.addClass("needsfilled");
				input.val(emptyerror);
				errornotice.fadeIn(750);
			} else {
				input.removeClass("needsfilled");
			}
		}
		//if any inputs on the page have the class 'needsfilled' the form will not submit
		if ($(":input").hasClass("needsfilled")) {
			return false;
		} else {
			errornotice.hide();
			$('#formIdentitas').submit();
		}
										  
	});
									
	$(":input").focus(function(){		
	   if ($(this).hasClass("needsfilled") ) {
			$(this).val("");
			$(this).removeClass("needsfilled");
		}
	});
	
		
});
</script>
</body>
</html>
