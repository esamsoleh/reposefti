<?php $this->load->view('header');?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('top');?>
  <!-- Left side column. contains the logo and sidebar -->
  
  <?php $this->load->view('sidebar');?>
 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title;?>
        <small><?=$subtitle;?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-white">
									<div class="panel-body">
									<?php echo validation_errors(); ?>

												<form name="frm" id="frm" method="post" action="/<?=$this->uri->uri_string();?>" enctype="multipart/form-data">													
													<div class="form-group row">
														<div class="col-md-4"><label>Nama Project</label></div>
														<div class="col-md-8">
															<?php echo form_dropdown($id_place,$options_place,$selected_place);?>
														</div>
													</div>
													<div class="form-group row">
														<div class="col-md-4"><label>Floor</label></div>
														<div class="col-md-8">
															<?php echo form_dropdown($id_floor,$options_floor,$selected_floor);?>
														</div>
													</div>
													<div class="form-group row">
														<div class="col-md-4"><label>Nama Denah</label></div>
														<div class="col-md-8">
															<?php echo form_input($denah);?>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-4"><label>File Denah (KMZ)</label></div>
														<div class="col-xs-8">
														<input type="file" id="file_kmz" name="file_kmz" class="form-control">
														</div>
													</div>
													</br>
													<div class="form-group row">
														<div class="col-md-6"><button id="simpan_form" class="btn btn-block btn-success" type="button">Simpan</button></div>
														<div class="col-md-6"><button id="batal" class="btn btn-block btn-success" type="button">Batal</button></div>
													</div>
												</form>
									</div>
				</div>
					
			</div>
		</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
 
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('javascript');?>
<script src="/assets/js/jquery.chainedSelects.js"></script>
<script src="/assets/js/dataTables/jquery.dataTables.js"></script>
<script src="/assets/js/dataTables/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC5Jrp9PtHe0WapppUzxbIpMDWMAcV3qE4"></script>
<script src="https://unpkg.com/location-picker/dist/location-picker.min.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
		required = ["nama","lokasi_x","lokasi_y"];
			errornotice = $("#error");
			emptyerror = "Mohon field ini diisi.";
			$('#simpan_form').click(function(){
				//Validate required fields
				for (i=0;i<required.length;i++) {
					var input = $('#'+required[i]);
					if ((input.val() == "") || (input.val() == emptyerror)) {
						input.addClass("needsfilled");
						input.val(emptyerror);
						errornotice.fadeIn(750);
					} else {
						input.removeClass("needsfilled");
					}
				}
					
				//if any inputs on the page have the class 'needsfilled' the form will not submit
				if ($(":input").hasClass("needsfilled")) {
					return false;
				} else {
					errornotice.hide();
					$('#frm').submit();
				}
												  
			});
											
			$(":input").focus(function(){		
			   if ($(this).hasClass("needsfilled") ) {
					$(this).val("");
					$(this).removeClass("needsfilled");
				}
			});
			
			$('#batal').click(function(){
				parent.history.back();
				return false;
			});
			
			$('#id_place').chainSelect('#id_floor','/setting/combo_floor',
			{ 
				before:function (target) //before request hide the target combobox and display the loading message
				{ 
					$("#loading").css("display","block");
					$(target).css("display","none");
				},
				after:function (target) //after request show the target combobox and hide the loading message
				{ 
					$("#loading").css("display","none");
					$(target).css("display","inline");
				}
			});
});
		</script>
		<script>
  // Get element references
  

</script>
</body>
</html>
