<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setting extends CI_Controller {
	public $apps;
	var $nama_user;
	var $file_foto;
	var $uri_string;
	var $s_biro;
	var $userid;
	var $user_id;
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
		if (!$this->ion_auth->logged_in())
			{
				redirect('/auth/login', 'refresh');
			}
		$this->apps = 'Setting';
		$this->load->model('log_model');
		$this->nama_user = $this->ion_auth->user()->row()->first_name.' '.$this->ion_auth->user()->row()->last_name;
		$this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		$this->userid = $this->session->userdata('s_username');
		$this->uri_string = '/'.$this->uri->uri_string();
		$this->s_biro = $this->session->userdata('s_biro');
		$this->user_id = $this->ion_auth->user()->row()->id;
	}
	 
	public function hari_libur()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		
		$data = array(
				'title' => 'Data Hari Libur',
				'subtitle' => ''
		);
		$this->load->view('hari_libur',$data);
		input_log($this->user_id,'Lihat data hari libur');
	}
	
	public function list_libur()
	{
		//print_r($_REQUEST);
		if(isset($_REQUEST['namacari']))
		{
			$namacari = $_REQUEST['namacari'];
		}
		else
		{
			$namacari = '';
		}
		$sorting = $_REQUEST['jtSorting'];
		$index = $_REQUEST['jtStartIndex'];
		$size = $_REQUEST['jtPageSize'];
		
		$sql = "select a.*
					from d_hari_libur a
					
					where 1=1
					";
		$data_semua = $this->db->query($sql);
			if($sorting != NULL)
			{
				$sql .= "order by ".$sorting." ";
			}
			if(($size != 0))
			{
				$sql.=" limit $index,$size";
			}
		
		$data = $this->db->query($sql);
		$recordCount = $data_semua->num_rows();
		$rows = array();
		foreach($data->result_array() as $row)
		{
			$rows[] = $row;
		}
		//print_r($rows);
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Records'] = $rows;
		$jTableResult['TotalRecordCount'] = $recordCount;
		print json_encode($jTableResult);
	}
	
	public function update_libur()
	{
		$posts = $this->input->post();
		$tanggal = date('Y-m-d',strtotime($posts['tanggal']));
		$update_date = date('Y-m-d H:i:s');
		$update_by = $this->ion_auth->user()->row()->id;
		$sql = "update d_hari_libur 
				set tanggal = '$tanggal',
					keterangan = '$posts[keterangan]',
					update_date = '$update_date',
					update_by = '$update_by'
				";
		
		$sql .= "where ID = '".$posts['id']."' ";
		input_log($this->user_id,'update data hari libur dengan tanggal:'.$tanggal);
	//	echo $sql;
		$this->db->query($sql);
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
	
	public function add_libur()
	{
		$posts = $this->input->post();
		$tanggal = date('Y-m-d',strtotime($posts['tanggal']));
		$keterangan = $posts['keterangan'];
		$added_date = date('Y-m-d H:i:s');
		$added_by = $this->ion_auth->user()->row()->id;
		$sql = "insert into d_hari_libur 
				set tanggal = '$tanggal',
					keterangan = '$keterangan',
					added_date = '$added_date',
					added_by = '$added_by'
				";
		
		//echo $sql;
		$this->db->query($sql);
		input_log($this->user_id,'Tambah data hari libur dengan tanggal:'.$tanggal);
		//Get last inserted record (to return to jTable)
		$result = $this->db->query("SELECT * FROM d_hari_libur WHERE ID = LAST_INSERT_ID();");
		$check = $result->row_array();
		if(isset($check) && count($check)>0)
		$row = $check;
		
	//	print_r($row);
		
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Record'] = $row;
		print json_encode($jTableResult);
	}
	
	public function del_libur()
	{
		$result = $this->db->query("DELETE FROM d_hari_libur WHERE id = '".$_POST['id']."' ");
		input_log($this->user_id,'hapus data hari libur dengan id:'.$_POST['id']);
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
	
	public function jadwal_pns()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		
		$data = array(
				'title' => 'Jadwal PNS',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'sssubunit' => $this->session->userdata('sssubunit_setting_pns'),
				'jnsjab' => $this->session->userdata('jnsjab_setting_pns'),
				'keselon' => $this->session->userdata('keselon_setting_pns'),
				'nama' => $this->session->userdata('nama_setting_pns'),
				'reqnip' => $this->session->userdata('reqnip_setting_pns'),
		);
		$this->load->view('jadwal_pns',$data);
		input_log($this->user_id,'Lihat jadwal pns');
	}
	
	public function get_jadwal_pns()
	{
		//print_r($_REQUEST);
		$order = $_REQUEST['order'][0];
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/setting/jadwal_pns');
		//$aColumns = array( 'nip', 'nip','nama', 'nip','nip');
		$sssubunit = $this->input->post('sssubunit');
		$this->session->set_userdata('sssubunit_setting_pns',$sssubunit);
		$jnsjab = $this->input->post('jnsjab');
		$this->session->set_userdata('jnsjab_setting_pns',$jnsjab);
		$keselon = $this->input->post('keselon');
		$this->session->set_userdata('keselon_setting_pns',$keselon);
		$nama = $this->input->post('nama');
		$this->session->set_userdata('nama_setting_pns',$nama);
		$reqnip = $this->input->post('reqnip');
		$this->session->set_userdata('reqnip_setting_pns',$reqnip);
		$sSearch = '';
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		if(isset($select) && $select != '')
		{
			if(!empty($s_biro)) 
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
			}
			else
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			}
		}
		
		if(isset($nama) && $nama != '')
		{
			$sSearch .= " and (nama like '%".$nama."%') ";
		}
		if(isset($reqnip) && $reqnip != '')
		{
			$sSearch .= " and (a.nip = '".$reqnip."') ";
		}
		if(isset($jnsjab) && $jnsjab != '0')
		{
			$sSearch .= " and (jnsjab = '".$jnsjab."') ";
		}
		if(isset($keselon) && $keselon != '0')
		{
			$sSearch .= " and (keselon = '".$keselon."') ";
		}
		
		//limit
		if ( isset( $_REQUEST['start'] ) && $_REQUEST['length'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_REQUEST['start'] ).", ".
				intval( $_REQUEST['length'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "`".$aColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]."` ".
						($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		//$sOrder = " order by ".$aColumns[$order['column']]." ".$order['dir']." ";
		$s0 = "select count(a.nip) as jml from d_pegawai a
		LEFT JOIN d_pegawai_tipe_jadwal b on(a.nip = b.nip)
				where 1 = 1
				";
		$s0 .= $sSearch;
		
		$sheet_total = $this->db->query($s0)->row();
		$sheet_total = $sheet_total->jml;
	
		$s1 = "SELECT DISTINCT a.nama,
									a.keselon,
									a.kgolru,
									a.kuntp,
									a.kunkom,
									a.kununit,
									a.tanggal_lahir,
									a.jabatan,
									(select neselon from referensi_eselon where keselon = a.keselon) as eselon,
									(select ngolru from referensi_golongan_ruang where KGOLRU = a.KGOLRU) as golongan,
									(select pangkat from referensi_golongan_ruang where KGOLRU = a.KGOLRU) as pangkat,
									b.*,
									if(b.tipe_jadwal != 0,(select keterangan from referensi_tipe_jadwal where id = b.tipe_jadwal),'-') as tipe_jadwal_keterangan
			FROM d_pegawai a
			LEFT JOIN d_pegawai_tipe_jadwal b on(a.nip = b.nip)
			WHERE 1=1";
		$s1 .= $sSearch;
		$s1 .= " order by keselon asc, kgolru desc, nip asc ";
		
		$s1 .= $sLimit;
	//	echo $s1;
		$sheet1 = $this->db->query($s1);
		$result = array();
		$i=0;
		$no=1;
		foreach($sheet1->result() as $sheet)
		{
			
			$result[$i]['0'] = $no;
			$result[$i]['1'] = $sheet->nip.' </br> '.$sheet->nama.' </br> '.$sheet->golongan;
			$result[$i]['2'] = $sheet->jabatan.' </br> '.$sheet->eselon;
			$result[$i]['3'] = $sheet->tipe_jadwal_keterangan;
			if($sheet->tipe_jadwal != 1)
			{
				$result[$i]['4'] = '<a target="_blank" href="/setting/atur_jadwal_pns/'.$sheet->id.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="View">
									<i class="glyphicon glyphicon-search"></i>
								</a>';
			}
			else
			{
				$result[$i]['4'] = '';
			}
			if($hak_akses['up'] == 1)
			{
				$result[$i]['5'] = '<a href="/setting/edit_jadwal_pns/'.$sheet->id.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Edit">
									<i class="glyphicon glyphicon-edit"></i>
								</a>';
			}
			else
			{
				$result[$i]['5'] = '';
			}
			/*
			if($hak_akses['de'] == 1)
			{
				$result[$i]['6'] = '<a id="del_'.$no.'" href="/setting/hapus_jadwal_pns/'.$sheet->id.'" class="btn btn-danger del" data-toggle="tooltip" data-placement="top" title="Hapus">
								<i class="glyphicon glyphicon-trash"></i>
								</a>
								<script>
								$(document).ready(function () {
									$("#del_'.$no.'").click(function(){
										if (!confirm("Do you want to delete")){
										  return false;
										}
									});
								});
								</script>
								';
			}
			else
			{
				$result[$i]['6'] = '';
			}
			*/
			$no++;
			$i++;
		}
		
		$data = $result;
		$results = array(
			"iTotalRecords" => ($sheet_total),
			"iTotalDisplayRecords" => ($sheet_total),
			"aaData"=>$data);
			
		echo json_encode($results);
	}
	
	public function edit_jadwal_pns()
	{
		$id = $this->uri->segment(3);
		$s = "select a.*
				from d_pegawai_tipe_jadwal a
				where id = '$id' ";
				//echo $s;
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			$this->load->library('form_validation');
			//$this->form_validation->set_rules('nip', 'NIP', 'trim|required');
			//$this->form_validation->set_rules('tanggal_mulai', 'Tanggal Mulai', 'trim|required');
			$this->form_validation->set_rules('tipe_jadwal', 'Tipe Jadwal', 'trim|required');
			
			if ($this->form_validation->run() === TRUE)
			{
				$posts = $this->input->post();
				print_r($posts);
				echo '</br>';
				//$nip = $this->db->escape_str($posts['nip']);
				$tipe_jadwal = $this->db->escape_str($posts['tipe_jadwal']);
				$update_date = date('Y-m-d H:i:s');
				$update_by = $this->userid;
				
				$sql = 	"update d_pegawai_tipe_jadwal 
							SET tipe_jadwal = '$tipe_jadwal',
								update_date = '$update_date',
								update_by = '$update_by'
							where id = '$id'
								";
				echo $sql.'</br>';
				$this->db->query($sql);	
				input_log($this->user_id,'edit jadwal pns dengan id:'.$id);
				redirect('/setting/jadwal_pns');
			}
			else
			{
				$this->data['nip'] = array(
						'name'  => 'nip',
						'id'    => 'nip',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('nip',(($r->nip))),
						'class'  => 'form-control',
						'disabled'=>'disabled'
					);
				
					$this->data['tipe_jadwal'] = array(
						'name'  => 'tipe_jadwal',
						'id'    => 'tipe_jadwal',
						'class'  => 'form-control',
					);
					$s = "select * from referensi_tipe_jadwal where 1 = 1";
					$q = $this->db->query($s);
					$options_tipe_jadwal[''] = '----------';
					foreach($q->result_array() as $rq)
					{
						$options_tipe_jadwal[($rq['id'])] = $rq['keterangan'];
					}
					
					$this->data['options_tipe_jadwal'] = $options_tipe_jadwal;
					$this->data['selected_tipe_jadwal'] = $this->form_validation->set_value('tipe_jadwal',$r->tipe_jadwal);
														
					$this->data['title'] = 'Data Tipe Jadwal Pegawai';
					$this->data['subtitle'] = 'Form Edit Data Tipe Jadwal Pegawai';
					$this->data['error'] = '';
					$this->data['r'] = $r;
					$this->load->view('form_jadwal_pegawai',$this->data);
			}
		}
		else
		{
			redirect('/');
		}
			
	}
	
	public function atur_jadwal_pns()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/setting/jadwal_pns');
		$bulan = $this->session->userdata('bulan');
		$bulan = ((isset($bulan) && $bulan != '')?$bulan:date('n'));
		$tahun = $this->session->userdata('tahun');		
		$tahun = ((isset($tahun) && $tahun != '')?$tahun:date('Y'));
		$jml_hari = date('t',strtotime('1-'.$bulan.'-'.$tahun));
		$id = $this->uri->segment(3);
		$s = "select a.*,
					b.nama as nama,
					(select ngolru from referensi_golongan_ruang where kgolru = b.kgolru) as golongan,
					b.jabatan,
					CASE KUNKOM
								   WHEN 10 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' 	)
								   WHEN 11 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' 	) 
								   ELSE (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,4) = concat(kuntp,kunkom) and mid(kunker,5,8) = '00000000' 	)
					END as skpd
				from d_pegawai_tipe_jadwal a 
				left join d_pegawai b on(a.nip = b.nip)
				where a.id = '$id' ";
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			$nip = $q->row()->nip;
			for($i=1;$i<=$jml_hari;$i++)
			{
				$sc = "select id 
					from d_jadwal_lembur 
					where nip = '$nip' 
					and day(tanggal) = '$i'
					and month(tanggal) = '$bulan'
					and year(tanggal) = '$tahun'
				";
				$qc = $this->db->query($sc);
				if($qc->num_rows() == 0)
				{
					$tanggal = date('Y-m-d',strtotime($i.'-'.$bulan.'-'.$tahun));
					$data = array(
							'nip' => $nip,
							'tanggal' => $tanggal,
							'added_date' => date('Y-m-d H:i:s'),
							'added_by' => '1'
					);
					$this->db->insert('d_jadwal_lembur',$data);
				}
			}
			$data = array(
				'title' => 'Atur Jadwal PNS',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'nip' => $q->row()->nip,
				'nama' => $q->row()->nama,
				'golongan' => $q->row()->golongan,
				'jabatan' => $q->row()->jabatan,
				'skpd' => $q->row()->skpd,
				'bulan' => $bulan,
				'tahun' => $tahun,
				'id_tipe_jadwal' => $id,
			);
			$this->load->view('atur_jadwal_pns',$data);
			input_log($this->user_id,'atur jadwal pns dengan nip:'.$q->row()->nip);
		}
		else
		{
			redirect('/');
		}
		
	}
	
	public function get_jadwal_piket_pns()
	{
		//print_r($_REQUEST);
		$order = $_REQUEST['order'][0];
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/setting/atur_jadwal_pns');
		$nip = $this->input->post('nip');
		$bulan = $this->input->post('bulan');
		
		$this->session->set_userdata('bulan',$bulan);
		$tahun = $this->input->post('tahun');
		$jml_hari = date('t',strtotime('1-'.$bulan.'-'.$tahun));
		$this->session->set_userdata('tahun',$tahun);
		$id_tipe_jadwal = $this->input->post('id_tipe_jadwal');
		//input jadwal piket
		for($i=1;$i<=$jml_hari;$i++)
			{
				$sc = "select id 
					from d_jadwal_lembur 
					where nip = '$nip' 
					and day(tanggal) = '$i'
					and month(tanggal) = '$bulan'
					and year(tanggal) = '$tahun'
				";
				$qc = $this->db->query($sc);
				if($qc->num_rows() == 0)
				{
					$tanggal = date('Y-m-d',strtotime($i.'-'.$bulan.'-'.$tahun));
					$data = array(
							'nip' => $nip,
							'tanggal' => $tanggal,
							'added_date' => date('Y-m-d H:i:s'),
							'added_by' => '1'
					);
					$this->db->insert('d_jadwal_lembur',$data);
				}
			}
			
		$sSearch = '';
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		if(isset($nip) && $nip != '')
		{
			$sSearch .= " and (a.nip = '".$nip."') ";
		}
		if(isset($bulan) && $bulan != '0')
		{
			$sSearch .= " and (month(tanggal) = '".$bulan."') ";
		}
		if(isset($tahun) && $tahun != '0')
		{
			$sSearch .= " and (year(tanggal) = '".$tahun."') ";
		}
		
		//limit
		if ( isset( $_REQUEST['start'] ) && $_REQUEST['length'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_REQUEST['start'] ).", ".
				intval( $_REQUEST['length'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "`".$aColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]."` ".
						($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		//$sOrder = " order by ".$aColumns[$order['column']]." ".$order['dir']." ";
		$s0 = "select count(a.id) as jml from d_jadwal_lembur a			
				where 1 = 1
				";
		$s0 .= $sSearch;
		
		$sheet_total = $this->db->query($s0)->row();
		$sheet_total = $sheet_total->jml;
	
		$s1 = "SELECT a.* 
			FROM d_jadwal_lembur a
			WHERE 1=1";
		$s1 .= $sSearch;
		$s1 .= " order by day(tanggal) asc, month(tanggal) asc, year(tanggal) asc ";
		
		$s1 .= $sLimit;
	//	echo $s1;
		$sheet1 = $this->db->query($s1);
		$result = array();
		$i=0;
		$no=1;
		foreach($sheet1->result() as $sheet)
		{
			
			$result[$i]['0'] = $no;
			$result[$i]['1'] = date('D',strtotime($sheet->tanggal));
			$result[$i]['2'] = date('d-M-Y',strtotime($sheet->tanggal));
			$result[$i]['3'] = ((isset($sheet->jadwal_masuk) && $sheet->jadwal_masuk != '' && $sheet->jadwal_masuk != '0000-00-00 00:00:00')?date('d-M-Y H:i:s',strtotime($sheet->jadwal_masuk)):'-');
			$result[$i]['4'] = ((isset($sheet->jadwal_keluar) && $sheet->jadwal_keluar != '' && $sheet->jadwal_keluar != '0000-00-00 00:00:00')?date('d-M-Y H:i:s',strtotime($sheet->jadwal_keluar)):'-');
			$result[$i]['5'] = (($sheet->is_libur==1)?'Ya':'-');
			$result[$i]['6'] = ($sheet->keterangan);
			$result[$i]['7'] = '<a href="/setting/edit_jadwal_piket_pns/'.$id_tipe_jadwal.'/'.$sheet->id.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Edit">
									<i class="glyphicon glyphicon-edit"></i>
								</a>';
			$no++;
			$i++;
		}
		
		$data = $result;
		$results = array(
			"iTotalRecords" => ($sheet_total),
			"iTotalDisplayRecords" => ($sheet_total),
			"aaData"=>$data);
			
		echo json_encode($results);
	}
	
	public function edit_jadwal_piket_pns()
	{
		$id_tipe_jadwal = $this->uri->segment(3);
		$id = $this->uri->segment(4);
		$s = "select a.*
				from d_jadwal_lembur a
				where a.id = '$id' ";
				//echo $s;
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			$this->load->library('form_validation');
			$this->form_validation->set_rules('nip', 'NIP', 'trim|required');
			$this->form_validation->set_rules('tanggal', 'tanggal', 'trim|required');
			
			if ($this->form_validation->run() === TRUE)
			{
				$posts = $this->input->post();
				print_r($posts);
				echo '</br>';
				$jadwal_masuk = $this->db->escape_str($posts['jadwal_masuk']);
				$jadwal_masuk = ((isset($jadwal_masuk) && $jadwal_masuk != '')?date('Y-m-d H:i:s',strtotime($jadwal_masuk)):'');
				$jadwal_keluar = $this->db->escape_str($posts['jadwal_keluar']);
				$jadwal_keluar = ((isset($jadwal_keluar) && $jadwal_keluar != '')?date('Y-m-d H:i:s',strtotime($jadwal_keluar)):'');
				$is_libur = $this->db->escape_str($posts['is_libur']);
				$keterangan = $this->db->escape_str($posts['keterangan']);	
				$update_date = date('Y-m-d H:i:s');
				$update_by = $this->userid;
				
				$sql = 	"update d_jadwal_lembur 
							SET jadwal_masuk = '$jadwal_masuk',
								jadwal_keluar = '$jadwal_keluar',
								is_libur = '$is_libur',
								keterangan = '$keterangan',
								update_date = '$update_date',
								update_by = '$update_by'
							where id = '$id'
								";
				echo $sql.'</br>';
				$this->db->query($sql);	
				input_log($this->user_id,'edit jadwal piket pns id:'.$id);
				redirect('/setting/atur_jadwal_pns/'.$id_tipe_jadwal);
			}
			else
			{
				$this->data['jadwal_masuk'] = array(
						'name'  => 'jadwal_masuk',
						'id'    => 'jadwal_masuk',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('jadwal_masuk',((isset($r->jadwal_masuk) && $r->jadwal_masuk != '' && $r->jadwal_masuk != '0000-00-00 00:00:00')?date('d-m-Y H:i:s',strtotime($r->jadwal_masuk)):'')),
						'class'  => 'form-control datepicker',
					);
				$this->data['jadwal_keluar'] = array(
						'name'  => 'jadwal_keluar',
						'id'    => 'jadwal_keluar',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('jadwal_keluar',((isset($r->jadwal_keluar) && $r->jadwal_keluar != '' && $r->jadwal_keluar != '0000-00-00 00:00:00')?date('d-m-Y H:i:s',strtotime($r->jadwal_keluar)):'')),
						'class'  => 'form-control datepicker',
					);
					$this->data['keterangan'] = array(
						'name'  => 'keterangan',
						'id'    => 'keterangan',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('keterangan',$r->keterangan),
						'class'  => 'form-control',
					);
					$this->data['is_libur'] = array(
					'name'  => 'is_libur',
					'id'    => 'is_libur',
					'class'  => 'form-control',
					);
					$options_libur['0'] = 'Tidak Libur';
					$options_libur['1'] = 'Libur';
					
					$this->data['options_libur'] = $options_libur;
					$this->data['selected_libur'] = $this->form_validation->set_value('is_libur',$r->is_libur);			
					$this->data['title'] = 'Data Jadwal Piket';
					$this->data['subtitle'] = 'Form Update Data Piket';
					$this->data['error'] = '';
					$this->data['r'] = $r;
					$this->load->view('form_edit_piket_pns',$this->data);
			}
		}
		else
		{
			redirect('/');
		}
			
	}
	
	public function jadwal_tkk()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		
		$data = array(
				'title' => 'Jadwal TKK',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'sssubunit' => $this->session->userdata('sssubunit_setting_tkk'),
				'jnsjab' => $this->session->userdata('jnsjab_setting_pns'),
				'nama' => $this->session->userdata('nama_setting_tkk'),
				'nik' => $this->session->userdata('nik_setting_tkk'),
		);
		$this->load->view('jadwal_tkk',$data);
		input_log($this->user_id,'lihat jadwal tkk');
	}
	
	public function get_jadwal_tkk()
	{
		//print_r($_REQUEST);
		$order = $_REQUEST['order'][0];
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/setting/jadwal_tkk');
		//$aColumns = array( 'nip', 'nip','nama', 'nip','nip');
		$sssubunit = $this->input->post('sssubunit');
		$this->session->set_userdata('sssubunit_setting_tkk',$sssubunit);
		$nama = $this->input->post('nama');
		$this->session->set_userdata('nama_setting_tkk',$nama);
		$nik = $this->input->post('nik');
		$this->session->set_userdata('nik_setting_tkk',$nik);
		
		$sSearch = '';
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		if(isset($select) && $select != '')
		{
			if(!empty($s_biro)) 
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
			}
			else
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			}
		}
		
		if(isset($nama) && $nama != '')
		{
			$sSearch .= " and (nama like '%".$nama."%') ";
		}
		if(isset($nik) && $nik != '')
		{
			$sSearch .= " and (a.nik = '".$nik."') ";
		}
			
		//limit
		if ( isset( $_REQUEST['start'] ) && $_REQUEST['length'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_REQUEST['start'] ).", ".
				intval( $_REQUEST['length'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "`".$aColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]."` ".
						($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		//$sOrder = " order by ".$aColumns[$order['column']]." ".$order['dir']." ";
		$s0 = "select count(a.nik) as jml from d_tkk a
		LEFT JOIN d_pegawai_tipe_jadwal_tkk b on(a.nik = b.nik)
				where 1 = 1
				";
		$s0 .= $sSearch;
		
		$sheet_total = $this->db->query($s0)->row();
		$sheet_total = $sheet_total->jml;
	
		$s1 = "SELECT DISTINCT a.*,
									b.*,
									concat(kuntp,kunkom,kununit,rincunit1,rincunit2,rincunit3) as kunker,
									if(b.tipe_jadwal != 0,(select keterangan from referensi_tipe_jadwal where id = b.tipe_jadwal),'-') as tipe_jadwal_keterangan
			FROM d_tkk a
			LEFT JOIN d_pegawai_tipe_jadwal_tkk b on(a.nik = b.nik)
			WHERE 1=1";
		$s1 .= $sSearch;
		$s1 .= " order by a.nik asc ";
		
		$s1 .= $sLimit;
	//	echo $s1;
		$sheet1 = $this->db->query($s1);
		$result = array();
		$i=0;
		$no=1;
		foreach($sheet1->result() as $sheet)
		{
			
			$result[$i]['0'] = $no;
			$result[$i]['1'] = $sheet->nik;
			$result[$i]['2'] = $sheet->nama;
			$result[$i]['3'] = $this->get_skpd($sheet->kunker);
			$result[$i]['4'] = $sheet->tipe_jadwal_keterangan;
			if($sheet->tipe_jadwal != 1)
			{
				$result[$i]['5'] = '<a target="_blank" href="/setting/atur_jadwal_tkk/'.$sheet->id.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="View">
									<i class="glyphicon glyphicon-search"></i>
								</a>';
			}
			else
			{
				$result[$i]['5'] = '';
			}
			if($hak_akses['up'] == 1)
			{
				$result[$i]['6'] = '<a href="/setting/edit_jadwal_tkk/'.$sheet->id.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Edit">
									<i class="glyphicon glyphicon-edit"></i>
								</a>';
			}
			else
			{
				$result[$i]['6'] = '';
			}
			/*
			if($hak_akses['de'] == 1)
			{
				$result[$i]['6'] = '<a id="del_'.$no.'" href="/setting/hapus_jadwal_pns/'.$sheet->id.'" class="btn btn-danger del" data-toggle="tooltip" data-placement="top" title="Hapus">
								<i class="glyphicon glyphicon-trash"></i>
								</a>
								<script>
								$(document).ready(function () {
									$("#del_'.$no.'").click(function(){
										if (!confirm("Do you want to delete")){
										  return false;
										}
									});
								});
								</script>
								';
			}
			else
			{
				$result[$i]['6'] = '';
			}
			*/
			$no++;
			$i++;
		}
		
		$data = $result;
		$results = array(
			"iTotalRecords" => ($sheet_total),
			"iTotalDisplayRecords" => ($sheet_total),
			"aaData"=>$data);
			
		echo json_encode($results);
	}
	
	public function edit_jadwal_tkk()
	{
		$id = $this->uri->segment(3);
		$s = "select a.*
				from d_pegawai_tipe_jadwal_tkk a
				where id = '$id' ";
				//echo $s;
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			$this->load->library('form_validation');
			//$this->form_validation->set_rules('nip', 'NIP', 'trim|required');
			//$this->form_validation->set_rules('tanggal_mulai', 'Tanggal Mulai', 'trim|required');
			$this->form_validation->set_rules('tipe_jadwal', 'Tipe Jadwal', 'trim|required');
			
			if ($this->form_validation->run() === TRUE)
			{
				$posts = $this->input->post();
				print_r($posts);
				echo '</br>';
				//$nip = $this->db->escape_str($posts['nip']);
				$tipe_jadwal = $this->db->escape_str($posts['tipe_jadwal']);
				$update_date = date('Y-m-d H:i:s');
				$update_by = $this->userid;
				
				$sql = 	"update d_pegawai_tipe_jadwal_tkk 
							SET tipe_jadwal = '$tipe_jadwal',
								update_date = '$update_date',
								update_by = '$update_by'
							where id = '$id'
								";
				echo $sql.'</br>';
				$this->db->query($sql);	
				input_log($this->user_id,'edit jadwal tkk dengan id:'.$id);
				redirect('/setting/jadwal_tkk');
			}
			else
			{
				$this->data['nik'] = array(
						'name'  => 'nik',
						'id'    => 'nik',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('nik',(($r->nik))),
						'class'  => 'form-control',
						'disabled'=>'disabled'
					);
				
					$this->data['tipe_jadwal'] = array(
						'name'  => 'tipe_jadwal',
						'id'    => 'tipe_jadwal',
						'class'  => 'form-control',
					);
					$s = "select * from referensi_tipe_jadwal where 1 = 1";
					$q = $this->db->query($s);
					$options_tipe_jadwal[''] = '----------';
					foreach($q->result_array() as $rq)
					{
						$options_tipe_jadwal[($rq['id'])] = $rq['keterangan'];
					}
					
					$this->data['options_tipe_jadwal'] = $options_tipe_jadwal;
					$this->data['selected_tipe_jadwal'] = $this->form_validation->set_value('tipe_jadwal',$r->tipe_jadwal);
														
					$this->data['title'] = 'Data Tipe Jadwal TKK';
					$this->data['subtitle'] = 'Form Edit Data Tipe Jadwal TKK';
					$this->data['error'] = '';
					$this->data['r'] = $r;
					$this->load->view('form_jadwal_tkk',$this->data);
			}
		}
		else
		{
			redirect('/');
		}
			
	}
	
	public function atur_jadwal_tkk()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/setting/jadwal_tkk');
		$bulan = $this->session->userdata('bulan');
		$bulan = ((isset($bulan) && $bulan != '')?$bulan:date('n'));
		$tahun = $this->session->userdata('tahun');		
		$tahun = ((isset($tahun) && $tahun != '')?$tahun:date('Y'));
		$jml_hari = date('t',strtotime('1-'.$bulan.'-'.$tahun));
		$id = $this->uri->segment(3);
		$s = "select a.*,
					b.nama as nama,
					CASE KUNKOM
								   WHEN 10 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' 	)
								   WHEN 11 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' 	) 
								   ELSE (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,4) = concat(kuntp,kunkom) and mid(kunker,5,8) = '00000000' 	)
					END as skpd
				from d_pegawai_tipe_jadwal_tkk a 
				left join d_tkk b on(a.nik = b.nik)
				where a.id = '$id' ";
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			$nik = $q->row()->nik;
			for($i=1;$i<=$jml_hari;$i++)
			{
				$sc = "select id 
					from d_jadwal_lembur_tkk
					where nik = '$nik' 
					and day(tanggal) = '$i'
					and month(tanggal) = '$bulan'
					and year(tanggal) = '$tahun'
				";
				$qc = $this->db->query($sc);
				if($qc->num_rows() == 0)
				{
					$tanggal = date('Y-m-d',strtotime($i.'-'.$bulan.'-'.$tahun));
					$data = array(
							'nik' => $nik,
							'tanggal' => $tanggal,
							'added_date' => date('Y-m-d H:i:s'),
							'added_by' => '1'
					);
					$this->db->insert('d_jadwal_lembur_tkk',$data);
				}
			}
			$data = array(
				'title' => 'Atur Jadwal TKK',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'nik' => $q->row()->nik,
				'nama' => $q->row()->nama,
				'skpd' => $q->row()->skpd,
				'bulan' => $bulan,
				'tahun' => $tahun,
				'id_tipe_jadwal' => $id,
			);
			$this->load->view('atur_jadwal_tkk',$data);
			input_log($this->user_id,'atur jadwal tkk dengan nik:'.$q->row()->nik);
		}
		else
		{
			redirect('/');
		}
		
	}
	
	public function get_jadwal_piket_tkk()
	{
		//print_r($_REQUEST);
		$order = $_REQUEST['order'][0];
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/setting/atur_jadwal_tkk');
		$nik = $this->input->post('nik');
		$bulan = $this->input->post('bulan');
		$this->session->set_userdata('bulan',$bulan);
		$tahun = $this->input->post('tahun');
		$this->session->set_userdata('tahun',$tahun);
		$id_tipe_jadwal = $this->input->post('id_tipe_jadwal');
		$jml_hari = date('t',strtotime('1-'.$bulan.'-'.$tahun));
		//input jadwal piket
		for($i=1;$i<=$jml_hari;$i++)
			{
				$sc = "select id 
					from d_jadwal_lembur_tkk 
					where nik = '$nik' 
					and day(tanggal) = '$i'
					and month(tanggal) = '$bulan'
					and year(tanggal) = '$tahun'
				";
				$qc = $this->db->query($sc);
				if($qc->num_rows() == 0)
				{
					$tanggal = date('Y-m-d',strtotime($i.'-'.$bulan.'-'.$tahun));
					$data = array(
							'nik' => $nik,
							'tanggal' => $tanggal,
							'added_date' => date('Y-m-d H:i:s'),
							'added_by' => '1'
					);
					$this->db->insert('d_jadwal_lembur_tkk',$data);
				}
			}
			
		$sSearch = '';
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		if(isset($nik) && $nik != '')
		{
			$sSearch .= " and (a.nik = '".$nik."') ";
		}
		if(isset($bulan) && $bulan != '0')
		{
			$sSearch .= " and (month(tanggal) = '".$bulan."') ";
		}
		if(isset($tahun) && $tahun != '0')
		{
			$sSearch .= " and (year(tanggal) = '".$tahun."') ";
		}
		
		//limit
		if ( isset( $_REQUEST['start'] ) && $_REQUEST['length'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_REQUEST['start'] ).", ".
				intval( $_REQUEST['length'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "`".$aColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]."` ".
						($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		//$sOrder = " order by ".$aColumns[$order['column']]." ".$order['dir']." ";
		$s0 = "select count(a.id) as jml from d_jadwal_lembur_tkk a			
				where 1 = 1
				";
		$s0 .= $sSearch;
		
		$sheet_total = $this->db->query($s0)->row();
		$sheet_total = $sheet_total->jml;
	
		$s1 = "SELECT a.* 
			FROM d_jadwal_lembur_tkk a
			WHERE 1=1";
		$s1 .= $sSearch;
		$s1 .= " order by day(tanggal) asc, month(tanggal) asc, year(tanggal) asc ";
		
		$s1 .= $sLimit;
	//	echo $s1;
		$sheet1 = $this->db->query($s1);
		$result = array();
		$i=0;
		$no=1;
		foreach($sheet1->result() as $sheet)
		{
			
			$result[$i]['0'] = $no;
			$result[$i]['1'] = date('D',strtotime($sheet->tanggal));
			$result[$i]['2'] = date('d-M-Y',strtotime($sheet->tanggal));
			$result[$i]['3'] = ((isset($sheet->jadwal_masuk) && $sheet->jadwal_masuk != '' && $sheet->jadwal_masuk != '0000-00-00 00:00:00')?date('d-M-Y H:i:s',strtotime($sheet->jadwal_masuk)):'-');
			$result[$i]['4'] = ((isset($sheet->jadwal_keluar) && $sheet->jadwal_keluar != '' && $sheet->jadwal_keluar != '0000-00-00 00:00:00')?date('d-M-Y H:i:s',strtotime($sheet->jadwal_keluar)):'-');
			$result[$i]['5'] = (($sheet->is_libur==1)?'Ya':'-');
			$result[$i]['6'] = $sheet->keterangan;
			$result[$i]['7'] = '<a href="/setting/edit_jadwal_piket_tkk/'.$id_tipe_jadwal.'/'.$sheet->id.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Edit">
									<i class="glyphicon glyphicon-edit"></i>
								</a>';
			$no++;
			$i++;
		}
		
		$data = $result;
		$results = array(
			"iTotalRecords" => ($sheet_total),
			"iTotalDisplayRecords" => ($sheet_total),
			"aaData"=>$data);
			
		echo json_encode($results);
	}
	
	public function edit_jadwal_piket_tkk()
	{
		$id_tipe_jadwal = $this->uri->segment(3);
		$id = $this->uri->segment(4);
		$s = "select a.*
				from d_jadwal_lembur_tkk a
				where a.id = '$id' ";
				//echo $s;
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			$this->load->library('form_validation');
			$this->form_validation->set_rules('nik', 'NIK', 'trim|required');
			$this->form_validation->set_rules('tanggal', 'tanggal', 'trim|required');
			
			if ($this->form_validation->run() === TRUE)
			{
				$posts = $this->input->post();
				print_r($posts);
				echo '</br>';
				$jadwal_masuk = $this->db->escape_str($posts['jadwal_masuk']);
				$jadwal_masuk = ((isset($jadwal_masuk) && $jadwal_masuk != '')?date('Y-m-d H:i:s',strtotime($jadwal_masuk)):'');
				$jadwal_keluar = $this->db->escape_str($posts['jadwal_keluar']);
				$jadwal_keluar = ((isset($jadwal_keluar) && $jadwal_keluar != '')?date('Y-m-d H:i:s',strtotime($jadwal_keluar)):'');
				$is_libur = $this->db->escape_str($posts['is_libur']);
				$keterangan = $this->db->escape_str($posts['keterangan']);	
				$update_date = date('Y-m-d H:i:s');
				$update_by = $this->userid;
				
				$sql = 	"update d_jadwal_lembur_tkk 
							SET jadwal_masuk = '$jadwal_masuk',
								jadwal_keluar = '$jadwal_keluar',
								is_libur = '$is_libur',
								keterangan = '$keterangan',
								update_date = '$update_date',
								update_by = '$update_by'
							where id = '$id'
								";
				echo $sql.'</br>';
				$this->db->query($sql);	
				input_log($this->user_id,'edit jadwal tkk dengan id:'.$id);
				redirect('/setting/atur_jadwal_tkk/'.$id_tipe_jadwal);
			}
			else
			{
				$this->data['jadwal_masuk'] = array(
						'name'  => 'jadwal_masuk',
						'id'    => 'jadwal_masuk',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('jadwal_masuk',((isset($r->jadwal_masuk) && $r->jadwal_masuk != '' && $r->jadwal_masuk != '0000-00-00 00:00:00')?date('d-m-Y H:i:s',strtotime($r->jadwal_masuk)):'')),
						'class'  => 'form-control datepicker',
					);
				$this->data['jadwal_keluar'] = array(
						'name'  => 'jadwal_keluar',
						'id'    => 'jadwal_keluar',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('jadwal_keluar',((isset($r->jadwal_keluar) && $r->jadwal_keluar != '' && $r->jadwal_keluar != '0000-00-00 00:00:00')?date('d-m-Y H:i:s',strtotime($r->jadwal_keluar)):'')),
						'class'  => 'form-control datepicker',
					);
					$this->data['keterangan'] = array(
						'name'  => 'keterangan',
						'id'    => 'keterangan',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('keterangan',$r->keterangan),
						'class'  => 'form-control',
					);
					$this->data['is_libur'] = array(
					'name'  => 'is_libur',
					'id'    => 'is_libur',
					'class'  => 'form-control',
					);
					$options_libur['0'] = 'Tidak Libur';
					$options_libur['1'] = 'Libur';
					
					$this->data['options_libur'] = $options_libur;
					$this->data['selected_libur'] = $this->form_validation->set_value('is_libur',$r->is_libur);			
					$this->data['title'] = 'Data Jadwal Piket TKK';
					$this->data['subtitle'] = 'Form Update Data Piket TKK';
					$this->data['error'] = '';
					$this->data['r'] = $r;
					$this->load->view('form_edit_piket_tkk',$this->data);
			}
		}
		else
		{
			redirect('/');
		}
			
	}
	
	function get_skpd($kunker)
	{
		$kuntp = substr($kunker,0,2);
		$kunkom = substr($kunker,2,2);
		$kununit = substr($kunker,4,2);
		switch($kunkom)
		{
			case '10':
			case '11':
				$s = "select nunker from referensi_unit_kerja where substr(kunker,1,2) = '$kuntp' 
																and substr(kunker,3,2) = '$kunkom' 
																and substr(kunker,5,8) = '00000000'";
				$q = $this->db->query($s);
				if($q->num_rows() > 0)
				{
					$nunker = $q->row()->nunker;
				}
				else
				{
					$nunker = '';
				}
			break;
			
			default:
				$s = "select nunker from referensi_unit_kerja where substr(kunker,1,2) = '$kuntp' 
																and substr(kunker,3,2) = '$kunkom' 
																and substr(kunker,5,2) = '$kununit' 
																and substr(kunker,7,6) = '000000'
																";
				$q = $this->db->query($s);
				if($q->num_rows() > 0)
				{
					$nunker = $q->row()->nunker;
				}
				else
				{
					$nunker = '';
				}
			break;
		}
		return $nunker;
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */