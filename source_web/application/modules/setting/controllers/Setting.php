<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setting extends CI_Controller {
	public $apps;
	var $nama_user;
	var $file_foto;
	var $uri_string;
	var $s_biro;
	var $userid;
	var $user_id;
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
		if (!$this->ion_auth->logged_in())
			{
				redirect('/auth/login', 'refresh');
			}
		$this->apps = 'Setting';
		$this->load->model('log_model');
		$this->nama_user = $this->ion_auth->user()->row()->first_name.' '.$this->ion_auth->user()->row()->last_name;
		$this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		$this->userid = $this->session->userdata('s_username');
		$this->uri_string = '/'.$this->uri->uri_string();
		$this->s_biro = $this->session->userdata('s_biro');
		$this->user_id = $this->ion_auth->user()->row()->id;
	}
	 
	public function hari_libur()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		
		$data = array(
				'title' => 'Data Hari Libur',
				'subtitle' => ''
		);
		$this->load->view('hari_libur',$data);
		input_log($this->user_id,'Lihat data hari libur');
	}
	
	public function list_libur()
	{
		//print_r($_REQUEST);
		if(isset($_REQUEST['namacari']))
		{
			$namacari = $_REQUEST['namacari'];
		}
		else
		{
			$namacari = '';
		}
		$sorting = $_REQUEST['jtSorting'];
		$index = $_REQUEST['jtStartIndex'];
		$size = $_REQUEST['jtPageSize'];
		
		$sql = "select a.*
					from d_hari_libur a
					
					where 1=1
					";
		$data_semua = $this->db->query($sql);
			if($sorting != NULL)
			{
				$sql .= "order by ".$sorting." ";
			}
			if(($size != 0))
			{
				$sql.=" limit $index,$size";
			}
		
		$data = $this->db->query($sql);
		$recordCount = $data_semua->num_rows();
		$rows = array();
		foreach($data->result_array() as $row)
		{
			$rows[] = $row;
		}
		//print_r($rows);
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Records'] = $rows;
		$jTableResult['TotalRecordCount'] = $recordCount;
		print json_encode($jTableResult);
	}
	
	public function update_libur()
	{
		$posts = $this->input->post();
		$tanggal = date('Y-m-d',strtotime($posts['tanggal']));
		$update_date = date('Y-m-d H:i:s');
		$update_by = $this->ion_auth->user()->row()->id;
		$keterangan = $this->db->escape_str($posts['keterangan']);
		$sql = "update d_hari_libur 
				set tanggal = '$tanggal',
					keterangan = '$keterangan',
					update_date = '$update_date',
					update_by = '$update_by'
				";
		
		$sql .= "where ID = '".$posts['id']."' ";
		
	//	echo $sql;
		$this->db->query($sql);
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
		input_log($this->user_id,'update data hari libur dengan tanggal:'.$tanggal);
	}
	
	public function add_libur()
	{
		$posts = $this->input->post();
		$tanggal = date('Y-m-d',strtotime($posts['tanggal']));
		$keterangan = $this->db->escape_str($posts['keterangan']);
		$added_date = date('Y-m-d H:i:s');
		$added_by = $this->ion_auth->user()->row()->id;
		$sql = "insert into d_hari_libur 
				set tanggal = '$tanggal',
					keterangan = '$keterangan',
					added_date = '$added_date',
					added_by = '$added_by'
				";
		
		//echo $sql;
		$this->db->query($sql);
		
		//Get last inserted record (to return to jTable)
		$result = $this->db->query("SELECT * FROM d_hari_libur WHERE ID = LAST_INSERT_ID();");
		$check = $result->row_array();
		$row = $check;
		if(isset($check) && count($check)>0)
		{
			$row = $check;
		}
	//	print_r($row);
		
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Record'] = $row;
		print json_encode($jTableResult);
		input_log($this->user_id,'Tambah data hari libur dengan tanggal:'.$tanggal);
	}
	
	public function del_libur()
	{
		$result = $this->db->query("DELETE FROM d_hari_libur WHERE id = '".$_POST['id']."' ");
		input_log($this->user_id,'hapus data hari libur dengan id:'.$_POST['id']);
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
	
	public function jadwal()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		
		$data = array(
				'title' => 'Jadwal Pegawai',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'nama' => $this->session->userdata('nama_setting_pns'),
				'reqnip' => $this->session->userdata('reqnip_setting_pns'),
		);
		$this->load->view('jadwal_pegawai',$data);
		input_log($this->user_id,'Lihat jadwal pegawai');
	}
	
	public function get_jadwal_pegawai()
	{
		//print_r($_REQUEST);
		//$order = $_REQUEST['order'][0];
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/setting/jadwal');
		//$aColumns = array( 'nip', 'nip','nama', 'nip','nip');
		$nama = $this->input->post('nama');
		$this->session->set_userdata('nama_setting_pns',$nama);
		$reqnip = $this->input->post('reqnip');
		$this->session->set_userdata('reqnip_setting_pns',$reqnip);
		$sSearch = '';
		
		if(isset($nama) && $nama != '')
		{
			$sSearch .= " and (nama like '%".$nama."%') ";
		}
		if(isset($reqnip) && $reqnip != '')
		{
			$sSearch .= " and (a.nip = '".$reqnip."') ";
		}
		
		//limit
		if ( isset( $_REQUEST['start'] ) && $_REQUEST['length'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_REQUEST['start'] ).", ".
				intval( $_REQUEST['length'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "`".$aColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]."` ".
						($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		//$sOrder = " order by ".$aColumns[$order['column']]." ".$order['dir']." ";
		$s0 = "select count(a.nip) as jml from d_pegawai a
				LEFT JOIN d_pegawai_tipe_jadwal b on(a.nip = b.nip)
				LEFT JOIN ref_tipe_jadwal c on(b.tipe_jadwal = c.id)
				where 1 = 1
				";
		$s0 .= $sSearch;
		
		$sheet_total = $this->db->query($s0)->row();
		$sheet_total = $sheet_total->jml;
	
		$s1 = "SELECT DISTINCT a.nip as nip_pegawai,
							a.*,
							(select nama from ref_jabatan where id = a.id_jabatan) as nama_jabatan,
							b.*,
							c.shift,
							if(b.tipe_jadwal != 0,(select keterangan from ref_tipe_jadwal where id = b.tipe_jadwal),'-') as tipe_jadwal_keterangan
			FROM d_pegawai a
			LEFT JOIN d_pegawai_tipe_jadwal b on(a.nip = b.nip)
			LEFT JOIN ref_tipe_jadwal c on(b.tipe_jadwal = c.id)
			WHERE 1=1";
		$s1 .= $sSearch;
		$s1 .= " order by a.nip asc ";
		
		$s1 .= $sLimit;
	//	echo $s1;
		$sheet1 = $this->db->query($s1);
		$result = array();
		$i=0;
		$no=1;
		foreach($sheet1->result() as $sheet)
		{
			
			$result[$i]['0'] = $no;
			$result[$i]['1'] = $sheet->nip_pegawai.' </br> '.$sheet->nama;
			$result[$i]['2'] = $sheet->nama_jabatan;
			$result[$i]['3'] = $sheet->tipe_jadwal_keterangan;
			if($sheet->shift == 1)
			{
				$result[$i]['4'] = '<a target="_blank" href="/setting/atur_jadwal_pegawai/'.$sheet->id.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="View">
									<i class="glyphicon glyphicon-search"></i>
								</a>';
			}
			else
			{
				$result[$i]['4'] = '';
			}
			if($hak_akses['up'] == 1)
			{
				$result[$i]['5'] = '<a href="/setting/edit_jadwal_pegawai/'.$sheet->id.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Edit">
									<i class="glyphicon glyphicon-edit"></i>
								</a>';
			}
			else
			{
				$result[$i]['5'] = '';
			}
			
			$no++;
			$i++;
		}
		
		$data = $result;
		$results = array(
			"iTotalRecords" => ($sheet_total),
			"iTotalDisplayRecords" => ($sheet_total),
			"aaData"=>$data);
			
		echo json_encode($results);
	}
	
	public function edit_jadwal_pegawai()
	{
		$id = $this->uri->segment(3);
		$s = "select a.*
				from d_pegawai_tipe_jadwal a
				where id = '$id' ";
				//echo $s;
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			$this->load->library('form_validation');
			//$this->form_validation->set_rules('nip', 'NIP', 'trim|required');
			//$this->form_validation->set_rules('tanggal_mulai', 'Tanggal Mulai', 'trim|required');
			$this->form_validation->set_rules('tipe_jadwal', 'Tipe Jadwal', 'trim|required');
			
			if ($this->form_validation->run() === TRUE)
			{
				$posts = $this->input->post();
				print_r($posts);
				echo '</br>';
				//$nip = $this->db->escape_str($posts['nip']);
				$tipe_jadwal = $this->db->escape_str($posts['tipe_jadwal']);
				$update_date = date('Y-m-d H:i:s');
				$update_by = $this->userid;
				
				$sql = 	"update d_pegawai_tipe_jadwal 
							SET tipe_jadwal = '$tipe_jadwal',
								update_date = '$update_date',
								update_by = '$update_by'
							where id = '$id'
								";
				echo $sql.'</br>';
				$this->db->query($sql);	
				input_log($this->user_id,'edit jadwal pns dengan id:'.$id);
				redirect('/setting/jadwal');
			}
			else
			{
				$this->data['nip'] = array(
						'name'  => 'nip',
						'id'    => 'nip',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('nip',(($r->nip))),
						'class'  => 'form-control',
						'disabled'=>'disabled'
					);
				
					$this->data['tipe_jadwal'] = array(
						'name'  => 'tipe_jadwal',
						'id'    => 'tipe_jadwal',
						'class'  => 'form-control',
					);
					$s = "select * from ref_tipe_jadwal where 1 = 1";
					$q = $this->db->query($s);
					$options_tipe_jadwal[''] = '----------';
					foreach($q->result_array() as $rq)
					{
						$options_tipe_jadwal[($rq['id'])] = $rq['keterangan'];
					}
					
					$this->data['options_tipe_jadwal'] = $options_tipe_jadwal;
					$this->data['selected_tipe_jadwal'] = $this->form_validation->set_value('tipe_jadwal',$r->tipe_jadwal);
														
					$this->data['title'] = 'Data Tipe Jadwal Pegawai';
					$this->data['subtitle'] = 'Form Edit Data Tipe Jadwal Pegawai';
					$this->data['error'] = '';
					$this->data['r'] = $r;
					$this->load->view('form_jadwal_pegawai',$this->data);
			}
		}
		else
		{
			redirect('/');
		}
			
	}
	
	public function atur_jadwal_pegawai()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/setting/jadwal');
		$bulan = $this->session->userdata('bulan');
		$bulan = ((isset($bulan) && $bulan != '')?$bulan:date('n'));
		$tahun = $this->session->userdata('tahun');		
		$tahun = ((isset($tahun) && $tahun != '')?$tahun:date('Y'));
		$jml_hari = date('t',strtotime('1-'.$bulan.'-'.$tahun));
		$id = $this->uri->segment(3);
		$s = "select a.*,
					b.nama as nama,
					(select nama from ref_jabatan where id = b.id_jabatan) as nama_jabatan
				from d_pegawai_tipe_jadwal a 
				left join d_pegawai b on(a.nip = b.nip)
				where a.id = '$id' ";
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			$nip = $q->row()->nip;
			for($i=1;$i<=$jml_hari;$i++)
			{
				$sc = "select id 
					from d_jadwal_lembur 
					where nip = '$nip' 
					and day(tanggal) = '$i'
					and month(tanggal) = '$bulan'
					and year(tanggal) = '$tahun'
				";
				$qc = $this->db->query($sc);
				if($qc->num_rows() == 0)
				{
					$tanggal = date('Y-m-d',strtotime($i.'-'.$bulan.'-'.$tahun));
					$data = array(
							'nip' => $nip,
							'tanggal' => $tanggal,
							'added_date' => date('Y-m-d H:i:s'),
							'added_by' => $this->user_id
					);
					$this->db->insert('d_jadwal_lembur',$data);
				}
			}
			$data = array(
				'title' => 'Atur Jadwal Pegawai',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'nip' => $q->row()->nip,
				'nama' => $q->row()->nama,
				'nama_jabatan' => $q->row()->nama_jabatan,
				'bulan' => $bulan,
				'tahun' => $tahun,
				'id_tipe_jadwal' => $id,
			);
			$this->load->view('atur_jadwal_pegawai',$data);
			input_log($this->user_id,'atur jadwal pegawai dengan nip:'.$q->row()->nip);
		}
		else
		{
			redirect('/');
		}
		
	}
	
	public function get_jadwal_piket_pegawai()
	{
		//print_r($_REQUEST);
		$order = $_REQUEST['order'][0];
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/setting/atur_jadwal_pns');
		$nip = $this->input->post('nip');
		$bulan = $this->input->post('bulan');
		
		$this->session->set_userdata('bulan',$bulan);
		$tahun = $this->input->post('tahun');
		$jml_hari = date('t',strtotime('1-'.$bulan.'-'.$tahun));
		$this->session->set_userdata('tahun',$tahun);
		$id_tipe_jadwal = $this->input->post('id_tipe_jadwal');
		//input jadwal piket
		for($i=1;$i<=$jml_hari;$i++)
			{
				$sc = "select id 
					from d_jadwal_lembur 
					where nip = '$nip' 
					and day(tanggal) = '$i'
					and month(tanggal) = '$bulan'
					and year(tanggal) = '$tahun'
				";
				$qc = $this->db->query($sc);
				if($qc->num_rows() == 0)
				{
					$tanggal = date('Y-m-d',strtotime($i.'-'.$bulan.'-'.$tahun));
					$data = array(
							'nip' => $nip,
							'tanggal' => $tanggal,
							'added_date' => date('Y-m-d H:i:s'),
							'added_by' => '1'
					);
					$this->db->insert('d_jadwal_lembur',$data);
				}
			}
			
		$sSearch = '';
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		if(isset($nip) && $nip != '')
		{
			$sSearch .= " and (a.nip = '".$nip."') ";
		}
		if(isset($bulan) && $bulan != '0')
		{
			$sSearch .= " and (month(tanggal) = '".$bulan."') ";
		}
		if(isset($tahun) && $tahun != '0')
		{
			$sSearch .= " and (year(tanggal) = '".$tahun."') ";
		}
		
		//limit
		if ( isset( $_REQUEST['start'] ) && $_REQUEST['length'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_REQUEST['start'] ).", ".
				intval( $_REQUEST['length'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "`".$aColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]."` ".
						($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		//$sOrder = " order by ".$aColumns[$order['column']]." ".$order['dir']." ";
		$s0 = "select count(a.id) as jml from d_jadwal_lembur a			
				where 1 = 1
				";
		$s0 .= $sSearch;
		
		$sheet_total = $this->db->query($s0)->row();
		$sheet_total = $sheet_total->jml;
	
		$s1 = "SELECT a.* 
			FROM d_jadwal_lembur a
			WHERE 1=1";
		$s1 .= $sSearch;
		$s1 .= " order by day(tanggal) asc, month(tanggal) asc, year(tanggal) asc ";
		
		$s1 .= $sLimit;
	//	echo $s1;
		$sheet1 = $this->db->query($s1);
		$result = array();
		$i=0;
		$no=1;
		foreach($sheet1->result() as $sheet)
		{
			
			$result[$i]['0'] = $no;
			$result[$i]['1'] = date('D',strtotime($sheet->tanggal));
			$result[$i]['2'] = date('d-M-Y',strtotime($sheet->tanggal));
			$result[$i]['3'] = ((isset($sheet->jadwal_masuk) && $sheet->jadwal_masuk != '' && $sheet->jadwal_masuk != '0000-00-00 00:00:00')?date('d-M-Y H:i:s',strtotime($sheet->jadwal_masuk)):'-');
			$result[$i]['4'] = ((isset($sheet->jadwal_keluar) && $sheet->jadwal_keluar != '' && $sheet->jadwal_keluar != '0000-00-00 00:00:00')?date('d-M-Y H:i:s',strtotime($sheet->jadwal_keluar)):'-');
			$result[$i]['5'] = (($sheet->is_libur==1)?'Ya':'-');
			$result[$i]['6'] = ($sheet->keterangan);
			$result[$i]['7'] = '<a href="/setting/edit_jadwal_piket_pegawai/'.$id_tipe_jadwal.'/'.$sheet->id.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Edit">
									<i class="glyphicon glyphicon-edit"></i>
								</a>';
			$no++;
			$i++;
		}
		
		$data = $result;
		$results = array(
			"iTotalRecords" => ($sheet_total),
			"iTotalDisplayRecords" => ($sheet_total),
			"aaData"=>$data);
			
		echo json_encode($results);
	}
	
	public function edit_jadwal_piket_pegawai()
	{
		$id_tipe_jadwal = $this->uri->segment(3);
		$id = $this->uri->segment(4);
		$s = "select a.*
				from d_jadwal_lembur a
				where a.id = '$id' ";
				//echo $s;
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			$this->load->library('form_validation');
			$this->form_validation->set_rules('nip', 'NIP', 'trim|required');
			$this->form_validation->set_rules('tanggal', 'tanggal', 'trim|required');
			
			if ($this->form_validation->run() === TRUE)
			{
				$posts = $this->input->post();
				print_r($posts);
				echo '</br>';
				$jadwal_masuk = $this->db->escape_str($posts['jadwal_masuk']);
				$jadwal_masuk = ((isset($jadwal_masuk) && $jadwal_masuk != '')?date('Y-m-d H:i:s',strtotime($jadwal_masuk)):'');
				$jadwal_keluar = $this->db->escape_str($posts['jadwal_keluar']);
				$jadwal_keluar = ((isset($jadwal_keluar) && $jadwal_keluar != '')?date('Y-m-d H:i:s',strtotime($jadwal_keluar)):'');
				$is_libur = $this->db->escape_str($posts['is_libur']);
				$keterangan = $this->db->escape_str($posts['keterangan']);	
				$update_date = date('Y-m-d H:i:s');
				$update_by = $this->userid;
				
				$sql = 	"update d_jadwal_lembur 
							SET jadwal_masuk = '$jadwal_masuk',
								jadwal_keluar = '$jadwal_keluar',
								is_libur = '$is_libur',
								keterangan = '$keterangan',
								update_date = '$update_date',
								update_by = '$update_by'
							where id = '$id'
								";
				echo $sql.'</br>';
				$this->db->query($sql);	
				input_log($this->user_id,'edit jadwal piket pns id:'.$id);
				redirect('/setting/atur_jadwal_pegawai/'.$id_tipe_jadwal);
			}
			else
			{
				$this->data['jadwal_masuk'] = array(
						'name'  => 'jadwal_masuk',
						'id'    => 'jadwal_masuk',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('jadwal_masuk',((isset($r->jadwal_masuk) && $r->jadwal_masuk != '' && $r->jadwal_masuk != '0000-00-00 00:00:00')?date('d-m-Y H:i:s',strtotime($r->jadwal_masuk)):'')),
						'class'  => 'form-control datepicker',
					);
				$this->data['jadwal_keluar'] = array(
						'name'  => 'jadwal_keluar',
						'id'    => 'jadwal_keluar',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('jadwal_keluar',((isset($r->jadwal_keluar) && $r->jadwal_keluar != '' && $r->jadwal_keluar != '0000-00-00 00:00:00')?date('d-m-Y H:i:s',strtotime($r->jadwal_keluar)):'')),
						'class'  => 'form-control datepicker',
					);
					$this->data['keterangan'] = array(
						'name'  => 'keterangan',
						'id'    => 'keterangan',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('keterangan',$r->keterangan),
						'class'  => 'form-control',
					);
					$this->data['is_libur'] = array(
					'name'  => 'is_libur',
					'id'    => 'is_libur',
					'class'  => 'form-control',
					);
					$options_libur['0'] = 'Tidak Libur';
					$options_libur['1'] = 'Libur';
					
					$this->data['options_libur'] = $options_libur;
					$this->data['selected_libur'] = $this->form_validation->set_value('is_libur',$r->is_libur);			
					$this->data['title'] = 'Data Jadwal Piket';
					$this->data['subtitle'] = 'Form Update Data Piket';
					$this->data['error'] = '';
					$this->data['r'] = $r;
					$this->load->view('form_edit_piket_pns',$this->data);
			}
		}
		else
		{
			redirect('/');
		}
			
	}
	
	public function jadwal_tkk()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		
		$data = array(
				'title' => 'Jadwal TKK',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'sssubunit' => $this->session->userdata('sssubunit_setting_tkk'),
				'jnsjab' => $this->session->userdata('jnsjab_setting_pns'),
				'nama' => $this->session->userdata('nama_setting_tkk'),
				'nik' => $this->session->userdata('nik_setting_tkk'),
		);
		$this->load->view('jadwal_tkk',$data);
		input_log($this->user_id,'lihat jadwal tkk');
	}
	
	public function get_jadwal_tkk()
	{
		//print_r($_REQUEST);
		$order = $_REQUEST['order'][0];
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/setting/jadwal_tkk');
		//$aColumns = array( 'nip', 'nip','nama', 'nip','nip');
		$sssubunit = $this->input->post('sssubunit');
		$this->session->set_userdata('sssubunit_setting_tkk',$sssubunit);
		$nama = $this->input->post('nama');
		$this->session->set_userdata('nama_setting_tkk',$nama);
		$nik = $this->input->post('nik');
		$this->session->set_userdata('nik_setting_tkk',$nik);
		
		$sSearch = '';
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		if(isset($select) && $select != '')
		{
			if(!empty($s_biro)) 
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
			}
			else
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			}
		}
		
		if(isset($nama) && $nama != '')
		{
			$sSearch .= " and (nama like '%".$nama."%') ";
		}
		if(isset($nik) && $nik != '')
		{
			$sSearch .= " and (a.nik = '".$nik."') ";
		}
			
		//limit
		if ( isset( $_REQUEST['start'] ) && $_REQUEST['length'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_REQUEST['start'] ).", ".
				intval( $_REQUEST['length'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "`".$aColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]."` ".
						($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		//$sOrder = " order by ".$aColumns[$order['column']]." ".$order['dir']." ";
		$s0 = "select count(a.nik) as jml from d_tkk a
				LEFT JOIN d_pegawai_tipe_jadwal_tkk b on(a.nik = b.nik)
				LEFT JOIN ref_tipe_jadwal c on(b.tipe_jadwal = c.id)
				where 1 = 1
				";
		$s0 .= $sSearch;
		
		$sheet_total = $this->db->query($s0)->row();
		$sheet_total = $sheet_total->jml;
	
		$s1 = "SELECT DISTINCT a.*,
									b.*,
									concat(kuntp,kunkom,kununit,rincunit1,rincunit2,rincunit3) as kunker,
									c.shift,
									if(b.tipe_jadwal != 0,(select keterangan from ref_tipe_jadwal where id = b.tipe_jadwal),'-') as tipe_jadwal_keterangan
			FROM d_tkk a
			LEFT JOIN d_pegawai_tipe_jadwal_tkk b on(a.nik = b.nik)
			LEFT JOIN ref_tipe_jadwal c on(b.tipe_jadwal = c.id)
			WHERE 1=1";
		$s1 .= $sSearch;
		$s1 .= " order by a.nik asc ";
		
		$s1 .= $sLimit;
	//	echo $s1;
		$sheet1 = $this->db->query($s1);
		$result = array();
		$i=0;
		$no=1;
		foreach($sheet1->result() as $sheet)
		{
			
			$result[$i]['0'] = $no;
			$result[$i]['1'] = $sheet->nik;
			$result[$i]['2'] = $sheet->nama;
			$result[$i]['3'] = $this->get_skpd($sheet->kunker);
			$result[$i]['4'] = $sheet->tipe_jadwal_keterangan;
			if($sheet->shift == 1)
			{
				$result[$i]['5'] = '<a target="_blank" href="/setting/atur_jadwal_tkk/'.$sheet->id.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="View">
									<i class="glyphicon glyphicon-search"></i>
								</a>';
			}
			else
			{
				$result[$i]['5'] = '';
			}
			if($hak_akses['up'] == 1)
			{
				$result[$i]['6'] = '<a href="/setting/edit_jadwal_tkk/'.$sheet->id.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Edit">
									<i class="glyphicon glyphicon-edit"></i>
								</a>';
			}
			else
			{
				$result[$i]['6'] = '';
			}
			/*
			if($hak_akses['de'] == 1)
			{
				$result[$i]['6'] = '<a id="del_'.$no.'" href="/setting/hapus_jadwal_pns/'.$sheet->id.'" class="btn btn-danger del" data-toggle="tooltip" data-placement="top" title="Hapus">
								<i class="glyphicon glyphicon-trash"></i>
								</a>
								<script>
								$(document).ready(function () {
									$("#del_'.$no.'").click(function(){
										if (!confirm("Do you want to delete")){
										  return false;
										}
									});
								});
								</script>
								';
			}
			else
			{
				$result[$i]['6'] = '';
			}
			*/
			$no++;
			$i++;
		}
		
		$data = $result;
		$results = array(
			"iTotalRecords" => ($sheet_total),
			"iTotalDisplayRecords" => ($sheet_total),
			"aaData"=>$data);
			
		echo json_encode($results);
	}
	
	public function edit_jadwal_tkk()
	{
		$id = $this->uri->segment(3);
		$s = "select a.*
				from d_pegawai_tipe_jadwal_tkk a
				where id = '$id' ";
				//echo $s;
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			$this->load->library('form_validation');
			//$this->form_validation->set_rules('nip', 'NIP', 'trim|required');
			//$this->form_validation->set_rules('tanggal_mulai', 'Tanggal Mulai', 'trim|required');
			$this->form_validation->set_rules('tipe_jadwal', 'Tipe Jadwal', 'trim|required');
			
			if ($this->form_validation->run() === TRUE)
			{
				$posts = $this->input->post();
				print_r($posts);
				echo '</br>';
				//$nip = $this->db->escape_str($posts['nip']);
				$tipe_jadwal = $this->db->escape_str($posts['tipe_jadwal']);
				$update_date = date('Y-m-d H:i:s');
				$update_by = $this->userid;
				
				$sql = 	"update d_pegawai_tipe_jadwal_tkk 
							SET tipe_jadwal = '$tipe_jadwal',
								update_date = '$update_date',
								update_by = '$update_by'
							where id = '$id'
								";
				echo $sql.'</br>';
				$this->db->query($sql);	
				input_log($this->user_id,'edit jadwal tkk dengan id:'.$id);
				redirect('/setting/jadwal_tkk');
			}
			else
			{
				$this->data['nik'] = array(
						'name'  => 'nik',
						'id'    => 'nik',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('nik',(($r->nik))),
						'class'  => 'form-control',
						'disabled'=>'disabled'
					);
				
					$this->data['tipe_jadwal'] = array(
						'name'  => 'tipe_jadwal',
						'id'    => 'tipe_jadwal',
						'class'  => 'form-control',
					);
					$s = "select * from ref_tipe_jadwal where 1 = 1";
					$q = $this->db->query($s);
					$options_tipe_jadwal[''] = '----------';
					foreach($q->result_array() as $rq)
					{
						$options_tipe_jadwal[($rq['id'])] = $rq['keterangan'];
					}
					
					$this->data['options_tipe_jadwal'] = $options_tipe_jadwal;
					$this->data['selected_tipe_jadwal'] = $this->form_validation->set_value('tipe_jadwal',$r->tipe_jadwal);
														
					$this->data['title'] = 'Data Tipe Jadwal TKK';
					$this->data['subtitle'] = 'Form Edit Data Tipe Jadwal TKK';
					$this->data['error'] = '';
					$this->data['r'] = $r;
					$this->load->view('form_jadwal_tkk',$this->data);
			}
		}
		else
		{
			redirect('/');
		}
			
	}
	
	public function atur_jadwal_tkk()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/setting/jadwal_tkk');
		$bulan = $this->session->userdata('bulan');
		$bulan = ((isset($bulan) && $bulan != '')?$bulan:date('n'));
		$tahun = $this->session->userdata('tahun');		
		$tahun = ((isset($tahun) && $tahun != '')?$tahun:date('Y'));
		$jml_hari = date('t',strtotime('1-'.$bulan.'-'.$tahun));
		$id = $this->uri->segment(3);
		$s = "select a.*,
					b.nama as nama,
					CASE KUNKOM
								   WHEN 10 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' 	)
								   WHEN 11 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' 	) 
								   ELSE (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,4) = concat(kuntp,kunkom) and mid(kunker,5,8) = '00000000' 	)
					END as skpd
				from d_pegawai_tipe_jadwal_tkk a 
				left join d_tkk b on(a.nik = b.nik)
				where a.id = '$id' ";
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			$nik = $q->row()->nik;
			for($i=1;$i<=$jml_hari;$i++)
			{
				$sc = "select id 
					from d_jadwal_lembur_tkk
					where nik = '$nik' 
					and day(tanggal) = '$i'
					and month(tanggal) = '$bulan'
					and year(tanggal) = '$tahun'
				";
				$qc = $this->db->query($sc);
				if($qc->num_rows() == 0)
				{
					$tanggal = date('Y-m-d',strtotime($i.'-'.$bulan.'-'.$tahun));
					$data = array(
							'nik' => $nik,
							'tanggal' => $tanggal,
							'added_date' => date('Y-m-d H:i:s'),
							'added_by' => $this->user_id
					);
					$this->db->insert('d_jadwal_lembur_tkk',$data);
				}
			}
			$data = array(
				'title' => 'Atur Jadwal TKK',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'nik' => $q->row()->nik,
				'nama' => $q->row()->nama,
				'skpd' => $q->row()->skpd,
				'bulan' => $bulan,
				'tahun' => $tahun,
				'id_tipe_jadwal' => $id,
			);
			$this->load->view('atur_jadwal_tkk',$data);
			input_log($this->user_id,'atur jadwal tkk dengan nik:'.$q->row()->nik);
		}
		else
		{
			redirect('/');
		}
		
	}
	
	public function get_jadwal_piket_tkk()
	{
		//print_r($_REQUEST);
		$order = $_REQUEST['order'][0];
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/setting/atur_jadwal_tkk');
		$nik = $this->input->post('nik');
		$bulan = $this->input->post('bulan');
		$this->session->set_userdata('bulan',$bulan);
		$tahun = $this->input->post('tahun');
		$this->session->set_userdata('tahun',$tahun);
		$id_tipe_jadwal = $this->input->post('id_tipe_jadwal');
		$jml_hari = date('t',strtotime('1-'.$bulan.'-'.$tahun));
		//input jadwal piket
		for($i=1;$i<=$jml_hari;$i++)
			{
				$sc = "select id 
					from d_jadwal_lembur_tkk 
					where nik = '$nik' 
					and day(tanggal) = '$i'
					and month(tanggal) = '$bulan'
					and year(tanggal) = '$tahun'
				";
				$qc = $this->db->query($sc);
				if($qc->num_rows() == 0)
				{
					$tanggal = date('Y-m-d',strtotime($i.'-'.$bulan.'-'.$tahun));
					$data = array(
							'nik' => $nik,
							'tanggal' => $tanggal,
							'added_date' => date('Y-m-d H:i:s'),
							'added_by' => '1'
					);
					$this->db->insert('d_jadwal_lembur_tkk',$data);
				}
			}
			
		$sSearch = '';
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		if(isset($nik) && $nik != '')
		{
			$sSearch .= " and (a.nik = '".$nik."') ";
		}
		if(isset($bulan) && $bulan != '0')
		{
			$sSearch .= " and (month(tanggal) = '".$bulan."') ";
		}
		if(isset($tahun) && $tahun != '0')
		{
			$sSearch .= " and (year(tanggal) = '".$tahun."') ";
		}
		
		//limit
		if ( isset( $_REQUEST['start'] ) && $_REQUEST['length'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_REQUEST['start'] ).", ".
				intval( $_REQUEST['length'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "`".$aColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]."` ".
						($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		//$sOrder = " order by ".$aColumns[$order['column']]." ".$order['dir']." ";
		$s0 = "select count(a.id) as jml from d_jadwal_lembur_tkk a			
				where 1 = 1
				";
		$s0 .= $sSearch;
		
		$sheet_total = $this->db->query($s0)->row();
		$sheet_total = $sheet_total->jml;
	
		$s1 = "SELECT a.* 
			FROM d_jadwal_lembur_tkk a
			WHERE 1=1";
		$s1 .= $sSearch;
		$s1 .= " order by day(tanggal) asc, month(tanggal) asc, year(tanggal) asc ";
		
		$s1 .= $sLimit;
	//	echo $s1;
		$sheet1 = $this->db->query($s1);
		$result = array();
		$i=0;
		$no=1;
		foreach($sheet1->result() as $sheet)
		{
			
			$result[$i]['0'] = $no;
			$result[$i]['1'] = date('D',strtotime($sheet->tanggal));
			$result[$i]['2'] = date('d-M-Y',strtotime($sheet->tanggal));
			$result[$i]['3'] = ((isset($sheet->jadwal_masuk) && $sheet->jadwal_masuk != '' && $sheet->jadwal_masuk != '0000-00-00 00:00:00')?date('d-M-Y H:i:s',strtotime($sheet->jadwal_masuk)):'-');
			$result[$i]['4'] = ((isset($sheet->jadwal_keluar) && $sheet->jadwal_keluar != '' && $sheet->jadwal_keluar != '0000-00-00 00:00:00')?date('d-M-Y H:i:s',strtotime($sheet->jadwal_keluar)):'-');
			$result[$i]['5'] = (($sheet->is_libur==1)?'Ya':'-');
			$result[$i]['6'] = $sheet->keterangan;
			$result[$i]['7'] = '<a href="/setting/edit_jadwal_piket_tkk/'.$id_tipe_jadwal.'/'.$sheet->id.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Edit">
									<i class="glyphicon glyphicon-edit"></i>
								</a>';
			$no++;
			$i++;
		}
		
		$data = $result;
		$results = array(
			"iTotalRecords" => ($sheet_total),
			"iTotalDisplayRecords" => ($sheet_total),
			"aaData"=>$data);
			
		echo json_encode($results);
	}
	
	public function edit_jadwal_piket_tkk()
	{
		$id_tipe_jadwal = $this->uri->segment(3);
		$id = $this->uri->segment(4);
		$s = "select a.*
				from d_jadwal_lembur_tkk a
				where a.id = '$id' ";
				//echo $s;
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			$this->load->library('form_validation');
			$this->form_validation->set_rules('nik', 'NIK', 'trim|required');
			$this->form_validation->set_rules('tanggal', 'tanggal', 'trim|required');
			
			if ($this->form_validation->run() === TRUE)
			{
				$posts = $this->input->post();
				print_r($posts);
				echo '</br>';
				$jadwal_masuk = $this->db->escape_str($posts['jadwal_masuk']);
				$jadwal_masuk = ((isset($jadwal_masuk) && $jadwal_masuk != '')?date('Y-m-d H:i:s',strtotime($jadwal_masuk)):'');
				$jadwal_keluar = $this->db->escape_str($posts['jadwal_keluar']);
				$jadwal_keluar = ((isset($jadwal_keluar) && $jadwal_keluar != '')?date('Y-m-d H:i:s',strtotime($jadwal_keluar)):'');
				$is_libur = $this->db->escape_str($posts['is_libur']);
				$keterangan = $this->db->escape_str($posts['keterangan']);	
				$update_date = date('Y-m-d H:i:s');
				$update_by = $this->userid;
				
				$sql = 	"update d_jadwal_lembur_tkk 
							SET jadwal_masuk = '$jadwal_masuk',
								jadwal_keluar = '$jadwal_keluar',
								is_libur = '$is_libur',
								keterangan = '$keterangan',
								update_date = '$update_date',
								update_by = '$update_by'
							where id = '$id'
								";
				echo $sql.'</br>';
				$this->db->query($sql);	
				input_log($this->user_id,'edit jadwal tkk dengan id:'.$id);
				redirect('/setting/atur_jadwal_tkk/'.$id_tipe_jadwal);
			}
			else
			{
				$this->data['jadwal_masuk'] = array(
						'name'  => 'jadwal_masuk',
						'id'    => 'jadwal_masuk',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('jadwal_masuk',((isset($r->jadwal_masuk) && $r->jadwal_masuk != '' && $r->jadwal_masuk != '0000-00-00 00:00:00')?date('d-m-Y H:i:s',strtotime($r->jadwal_masuk)):'')),
						'class'  => 'form-control datepicker',
					);
				$this->data['jadwal_keluar'] = array(
						'name'  => 'jadwal_keluar',
						'id'    => 'jadwal_keluar',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('jadwal_keluar',((isset($r->jadwal_keluar) && $r->jadwal_keluar != '' && $r->jadwal_keluar != '0000-00-00 00:00:00')?date('d-m-Y H:i:s',strtotime($r->jadwal_keluar)):'')),
						'class'  => 'form-control datepicker',
					);
					$this->data['keterangan'] = array(
						'name'  => 'keterangan',
						'id'    => 'keterangan',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('keterangan',$r->keterangan),
						'class'  => 'form-control',
					);
					$this->data['is_libur'] = array(
					'name'  => 'is_libur',
					'id'    => 'is_libur',
					'class'  => 'form-control',
					);
					$options_libur['0'] = 'Tidak Libur';
					$options_libur['1'] = 'Libur';
					
					$this->data['options_libur'] = $options_libur;
					$this->data['selected_libur'] = $this->form_validation->set_value('is_libur',$r->is_libur);			
					$this->data['title'] = 'Data Jadwal Piket TKK';
					$this->data['subtitle'] = 'Form Update Data Piket TKK';
					$this->data['error'] = '';
					$this->data['r'] = $r;
					$this->load->view('form_edit_piket_tkk',$this->data);
			}
		}
		else
		{
			redirect('/');
		}
			
	}
	
	function get_skpd($kunker)
	{
		$kuntp = substr($kunker,0,2);
		$kunkom = substr($kunker,2,2);
		$kununit = substr($kunker,4,2);
		switch($kunkom)
		{
			case '10':
			case '11':
				$s = "select nunker from referensi_unit_kerja where substr(kunker,1,2) = '$kuntp' 
																and substr(kunker,3,2) = '$kunkom' 
																and substr(kunker,5,8) = '00000000'";
				$q = $this->db->query($s);
				if($q->num_rows() > 0)
				{
					$nunker = $q->row()->nunker;
				}
				else
				{
					$nunker = '';
				}
			break;
			
			default:
				$s = "select nunker from referensi_unit_kerja where substr(kunker,1,2) = '$kuntp' 
																and substr(kunker,3,2) = '$kunkom' 
																and substr(kunker,5,2) = '$kununit' 
																and substr(kunker,7,6) = '000000'
																";
				$q = $this->db->query($s);
				if($q->num_rows() > 0)
				{
					$nunker = $q->row()->nunker;
				}
				else
				{
					$nunker = '';
				}
			break;
		}
		return $nunker;
	}
	
	public function alamat_server()
	{
		if(isset($_REQUEST['save']) && $_REQUEST['save'] == 1)
		{
			$su = "update t_setting set value = '".$this->input->post('alamat')."' where id = 1";
			$this->db->query($su);
		}
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		$alamat = $this->db->query('select value as alamat from t_setting where id = 1')->row()->alamat;
		$data = array(
				'title' => 'Setting Alamat Server',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'alamat' => $alamat
		);
		$this->load->view('alamat_server',$data);
		input_log($this->user_id,'lihat setting alamat_server');
	}
	
	public function akun_proximy()
	{
		if(isset($_REQUEST['save']) && $_REQUEST['save'] == 1)
		{
			$su = "update t_setting set value = '".$this->input->post('akun')."' where id = 2";
			$this->db->query($su);
			$su = "update t_setting set value = '".$this->input->post('password')."' where id = 3";
			$this->db->query($su);
		}
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		$akun = $this->db->query('select value as akun from t_setting where id = 2')->row()->akun;
		$password = $this->db->query('select value as password from t_setting where id = 3')->row()->password;
		$data = array(
				'title' => 'Setting Akun Proximy',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'akun' => $akun,
				'password' => $password
		);
		$this->load->view('akun_proximy',$data);
		input_log($this->user_id,'lihat setting akun proximy');
	}
	
	public function interval_refresh_dashboard()
	{
		if(isset($_REQUEST['save']) && $_REQUEST['save'] == 1)
		{
			$su = "update t_setting set value = '".$this->input->post('interval_refresh')."' where id = 4";
			$this->db->query($su);
		}
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		$interval_refresh = $this->db->query('select value as interval_refresh from t_setting where id = 4')->row()->interval_refresh;
		$data = array(
				'title' => 'Setting interval refresh dashboard',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'interval_refresh' => $interval_refresh
		);
		$this->load->view('interval_refresh_dashboard',$data);
		input_log($this->user_id,'lihat setting interval_refresh_dashboard');
	}
	
	public function denah()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		$id_place = $this->session->userdata('id_place_denah');
		$id_floor = $this->session->userdata('id_floor_denah');
		$id_place=((isset($id_place) && $id_place!= '')?$id_place:($this->db->query('select id_place from d_project where status = 0 and updated_at = (select max(updated_at) from d_project where status = 0) ')->row()->id_place));
		$scf = 'select id_floor from d_site where id_place = "'.$id_place.'" and updated_at = (select max(updated_at) from d_site where id_place = "'.$id_place.'" ) ';
	//	echo $scf;
		$qcf = $this->db->query($scf);
		if($qcf->num_rows() > 0)
		{
			$nf = $qcf->row()->id_floor;
		}
		else
		{
			$nf = 0;
		}
		$id_floor=((isset($id_floor) && $id_floor!= '')?$id_floor:($nf));
		$data = array(
				'title' => 'Denah Proyek',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'id_place' => $id_place,
				'id_floor' => $id_floor,
		);
		$this->load->view('denah',$data);
		input_log($this->user_id,'lihat Denah');
	}
	
	public function get_denah()
	{
		//print_r($_REQUEST);
		$order = $_REQUEST['order'][0];
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/setting/denah');
		//$aColumns = array( 'nip', 'nip','nama', 'nip','nip');
		$id_place = $this->input->post('id_place');
		$this->session->set_userdata('id_place_denah',$id_place);
		$id_floor = $this->input->post('id_floor');
		$this->session->set_userdata('id_floor_denah',$id_floor);
		
		$sSearch = '';
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		if(isset($id_place) && $id_place != '')
		{
			$sSearch .= " and (c.id_place = '".$id_place."') ";
		}
		if(isset($id_floor) && $id_floor != '')
		{
			$sSearch .= " and (b.id_floor = '".$id_floor."') ";
		}
			
		//limit
		if ( isset( $_REQUEST['start'] ) && $_REQUEST['length'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_REQUEST['start'] ).", ".
				intval( $_REQUEST['length'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "`".$aColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]."` ".
						($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		//$sOrder = " order by ".$aColumns[$order['column']]." ".$order['dir']." ";
		$s0 = "select count(a.id) as jml 
				from d_denah a
				LEFT JOIN d_site b on(a.id_floor = b.id_floor)
				LEFT JOIN d_project c on(b.id_place = c.id_place)
				where 1 = 1
				";
		$s0 .= $sSearch;
		
		$sheet_total = $this->db->query($s0)->row();
		$sheet_total = $sheet_total->jml;
	
		$s1 = "SELECT a.*,
						b.nama as nama_floor,
						c.nama as nama_project
			from d_denah a
				LEFT JOIN d_site b on(a.id_floor = b.id_floor)
				LEFT JOIN d_project c on(b.id_place = c.id_place)
				where 1 = 1
				";
		$s1 .= $sSearch;
		$s1 .= " order by a.id asc ";
		
		$s1 .= $sLimit;
	//	echo $s1;
		$sheet1 = $this->db->query($s1);
		$result = array();
		$i=0;
		$no=1;
		foreach($sheet1->result() as $sheet)
		{
			
			$result[$i]['0'] = $no;
			$result[$i]['1'] = $sheet->nama_project;
			$result[$i]['2'] = $sheet->nama_floor;
			$result[$i]['3'] = $sheet->denah;
			$result[$i]['4'] = '<a target="_blank" href="/denah/'.$sheet->id_floor.'/'.$sheet->file_geojson.'"><i class="glyphicon glyphicon-search"></i></a>';
			$result[$i]['5'] = '<a target="_blank" href="/denah/'.$sheet->id_floor.'/'.$sheet->file_gambar.'"><i class="glyphicon glyphicon-search"></i></a>';
			if($hak_akses['up'] == 1)
			{
				$result[$i]['6'] = '<a href="/setting/edit_denah/'.$sheet->id.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Edit">
									<i class="glyphicon glyphicon-edit"></i>
								</a>';
			}
			else
			{
				$result[$i]['6'] = '';
			}
			
			if($hak_akses['de'] == 1)
			{
				$result[$i]['7'] = '<a id="del_'.$no.'" href="/setting/hapus_denah/'.$sheet->id.'" class="btn btn-danger del" data-toggle="tooltip" data-placement="top" title="Hapus">
								<i class="glyphicon glyphicon-trash"></i>
								</a>
								<script>
								$(document).ready(function () {
									$("#del_'.$no.'").click(function(){
										if (!confirm("Do you want to delete")){
										  return false;
										}
									});
								});
								</script>
								';
			}
			else
			{
				$result[$i]['7'] = '';
			}
			
			$no++;
			$i++;
		}
		
		$data = $result;
		$results = array(
			"iTotalRecords" => ($sheet_total),
			"iTotalDisplayRecords" => ($sheet_total),
			"aaData"=>$data);
			
		echo json_encode($results);
	}
	
	public function tambah_denah()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('denah', 'Nama', 'trim|required');
			
			if ($this->form_validation->run() === TRUE)
			{
				$posts = $this->input->post();
				//print_r($posts);
				//	echo '</br>';
				$id_place = $this->db->escape_str($posts['id_place']);
				$id_floor = $this->db->escape_str($posts['id_floor']);
				$denah = $this->db->escape_str($posts['denah']);
				
				$added_date = date('Y-m-d H:i:s');
				$added_by = $this->userid;
				$path = 'denah/';
				$config['upload_path'] = $path;
				$config['allowed_types'] = 'kmz';
				$this->load->library('upload', $config);
				$field_name = 'file_kmz';
				if ($this->upload->do_upload($field_name))
				{
					$data = array('upload_data' => $this->upload->data());
					/* $file_1 = $path.$id.$data['upload_data']['file_ext'];
					if (file_exists($file_1)) {unlink($file_1);}       
					$org_1 = $path.$data['upload_data']['file_name'];
					rename($org_1, $file_1);	
					$file_gambar = $id.$data['upload_data']['file_ext']; 
					$file_gambar = $data['upload_data']['file_name'];
					*/
					if (!file_exists($path.$id_floor)) 
					{
						mkdir($path.$id_floor,0777);
					}
					$zip = new ZipArchive;
					//echo $path.$data['upload_data']['file_name'];
					
					$dz = zip_open($path.$data['upload_data']['file_name']);
					$array_file = array();
					while ($zip_entry = zip_read($dz))
					{
						$array_file[] = zip_entry_name($zip_entry);
					}
					$res = $zip->open($path.$data['upload_data']['file_name']);
					if ($res === TRUE) 
					{
					  $zip->extractTo($path.$id_floor);
					  $zip->close();
					  //echo 'Success!';
					} 
					else 
					{
					  //echo 'errors';
					}
					//print_r($array_file);
					foreach($array_file as $f)
					{
						//echo $path.$id_floor.'/'.$f;
						if(is_file($path.$id_floor.'/'.$f))
						{
							if(substr($f,-3) == 'png')
							{
								rename($path.$id_floor.'/'.$f,$path.$id_floor.'/denah.png');
							}
							if(substr($f,-3) == 'kml')
							{
								$file=fopen($path.$id_floor.'/'.$f,"r") or exit("Unable to open file!");
								while (!feof($file))
								{
									$line = fgets($file);
									$line = trim($line);
									if(substr($line,0,7) == '<north>')
									{
										$north = substr($line,7);
										$north = substr($north,0,-8);
										echo $north;
									}
									if(substr($line,0,7) == '<south>')
									{
										$south = substr($line,7);
										$south = substr($south,0,-8);
										echo $south;
									}
									if(substr($line,0,6) == '<east>')
									{
										$east = substr($line,6);
										$east = substr($east,0,-7);
										echo $east;
									}
									if(substr($line,0,6) == '<west>')
									{
										$west = substr($line,6);
										$west = substr($west,0,-7);
										echo $west;
									}
								}
								fclose($file);
								
								$kml = fopen($path.$id_floor."/denah.kml", "w");
								$txt = '<?xml version="1.0" encoding="UTF-8"?>
										<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom">
										<GroundOverlay>
										<name>'.$denah.'</name>
										<Icon>';
								fwrite($kml, $txt);
								$txt ='<href>'.alamataplikasi.'denah/'.$id_floor.'/denah.png</href>';
								fwrite($kml, $txt);
								$txt = '</Icon>
										<LatLonBox>
											<north>'.$north.'</north>
											<south>'.$south.'</south>
											<east>'.$east.'</east>
											<west>'.$west.'</west>
										</LatLonBox>
									</GroundOverlay>
										</kml>';
								fwrite($kml, $txt);
								unlink($path.$id_floor.'/'.$f);
							}
						}
					}
				}
				else
				{
					$this->session->set_flashdata('message', $this->upload->display_errors());	
					$file_gambar = '';
					//echo $this->upload->display_errors();
				}	
				
				$sql = 	"insert into d_denah 
							SET id_place = '$id_place', 
								id_floor = '$id_floor',
								denah = '$denah',
								file_geojson = 'denah.kml',
								file_gambar = 'denah.png',
								added_date = '$added_date',
								added_by = '$added_by'
								";
			///	echo $sql.'</br>';
				$this->db->query($sql);	
				$id = $this->db->insert_id();
				
				input_log($this->userid,'Tambah Data denah dengan id:'.$id);				
				redirect('/setting/denah');
			}
			else
			{
				$this->data['id_place'] = array(
						'name'  => 'id_place',
						'id'    => 'id_place',
						'class'  => 'form-control',
					);
				$s = "select * from d_project where status = 0 ";
				$q = $this->db->query($s);
				$options_place[''] = '----------';
				foreach($q->result_array() as $rq)
				{
					$options_place[($rq['id_place'])] = $rq['nama'];
				}
					
				$this->data['options_place'] = $options_place;
				$this->data['selected_place'] = $this->form_validation->set_value('id_place');
				
				$this->data['id_floor'] = array(
						'name'  => 'id_floor',
						'id'    => 'id_floor',
						'class'  => 'form-control',
					);
				$this->data['options_floor'] = '';
				$this->data['selected_floor'] = $this->form_validation->set_value('id_floor');
				
				$this->data['denah'] = array(
						'name'  => 'denah',
						'id'    => 'denah',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('denah'),
						'class'  => 'form-control',
					);
					
				$this->data['title'] = 'Data Denah';
				$this->data['subtitle'] = 'Form Penambahan Data Denah';
				$this->data['error'] = '';
				$this->load->view('form_denah',$this->data);
			}
			
	}
	
	public function edit_denah()
	{
		$id = $this->uri->segment(3);
		$s = "SELECT a.*
			FROM d_denah a
			WHERE 1=1
			and id = '$id' 
				";
		//echo $s;
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			$this->load->library('form_validation');
			$this->form_validation->set_rules('denah', 'Nama', 'trim|required');
			
			if ($this->form_validation->run() === TRUE)
			{
				$posts = $this->input->post();
			//	print_r($posts);
			//	echo '</br>';
				$id_place = $this->db->escape_str($posts['id_place']);
				$id_floor = $this->db->escape_str($posts['id_floor']);
				$denah = $this->db->escape_str($posts['denah']);
				
				$update_date = date('Y-m-d H:i:s');
				$update_by = $this->userid;
				$path = 'denah/';
				$config['upload_path'] = $path;
				$config['allowed_types'] = 'kmz';
				$this->load->library('upload', $config);
				$field_name = 'file_gambar';
				$field_name = 'file_kmz';
				if ($this->upload->do_upload($field_name))
				{
					$data = array('upload_data' => $this->upload->data());
					if (!file_exists($path.$id_floor)) 
					{
						mkdir($path.$id_floor,0777);
					}
					$zip = new ZipArchive;
					$dz = zip_open($path.$data['upload_data']['file_name']);
					$array_file = array();
					while ($zip_entry = zip_read($dz))
					{
						$array_file[] = zip_entry_name($zip_entry);
					}
					$res = $zip->open($path.$data['upload_data']['file_name']);
					if ($res === TRUE) 
					{
					  $zip->extractTo($path.$id_floor);
					  $zip->close();
					} 
					else 
					{
					  //echo 'errors';
					}
					//print_r($array_file);
					foreach($array_file as $f)
					{
						//echo $path.$id_floor.'/'.$f;
						if(is_file($path.$id_floor.'/'.$f))
						{
							if(substr($f,-3) == 'png')
							{
								rename($path.$id_floor.'/'.$f,$path.$id_floor.'/denah.png');
							}
							if(substr($f,-3) == 'kml')
							{
								unlink($path.$id_floor.'/denah.kml');
								$file=fopen($path.$id_floor.'/'.$f,"r") or exit("Unable to open file!");
								while (!feof($file))
								{
									$line = fgets($file);
									$line = trim($line);
									if(substr($line,0,7) == '<north>')
									{
										$north = substr($line,7);
										$north = substr($north,0,-8);
										echo $north;
									}
									if(substr($line,0,7) == '<south>')
									{
										$south = substr($line,7);
										$south = substr($south,0,-8);
										echo $south;
									}
									if(substr($line,0,6) == '<east>')
									{
										$east = substr($line,6);
										$east = substr($east,0,-7);
										echo $east;
									}
									if(substr($line,0,6) == '<west>')
									{
										$west = substr($line,6);
										$west = substr($west,0,-7);
										echo $west;
									}
								}
								fclose($file);
								
								$kml = fopen($path.$id_floor."/denah.kml", "w");
								$txt = '<?xml version="1.0" encoding="UTF-8"?>
										<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom">
										<GroundOverlay>
										<name>'.$denah.'</name>
										<Icon>';
								fwrite($kml, $txt);
								$txt ='<href>'.alamataplikasi.'denah/'.$id_floor.'/denah.png</href>';
								fwrite($kml, $txt);
								$txt = '</Icon>
										<LatLonBox>
											<north>'.$north.'</north>
											<south>'.$south.'</south>
											<east>'.$east.'</east>
											<west>'.$west.'</west>
										</LatLonBox>
									</GroundOverlay>
										</kml>';
								fwrite($kml, $txt);
								unlink($path.$id_floor.'/'.$f);
							}
						}
					}
				}
				else
				{
					$this->session->set_flashdata('message', $this->upload->display_errors());	
				}	
					
				
				$sql = 	"update d_denah 
							SET id_place = '$id_place', 
								id_floor = '$id_floor',
								denah = '$denah',
								update_date = '$update_date',
								update_by = '$update_by'
							where id = '$id' 
								";
			///	echo $sql.'</br>';
				$this->db->query($sql);	
				input_log($this->userid,'Edit Data denah dengan id:'.$id);
				redirect('/setting/denah');
			}
			else
			{
				$r = $q->row();
				$this->data['id_place'] = array(
						'name'  => 'id_place',
						'id'    => 'id_place',
						'class'  => 'form-control',
				);
				$s = "select * from d_project where status = 0 ";
				$q = $this->db->query($s);
				$options_place[''] = '----------';
				foreach($q->result_array() as $rq)
				{
					$options_place[($rq['id_place'])] = $rq['nama'];
				}
				$this->data['options_place'] = $options_place;
				$this->data['selected_place'] = $this->form_validation->set_value('id_place',$r->id_place);
				
				$this->data['id_floor'] = array(
						'name'  => 'id_floor',
						'id'    => 'id_floor',
						'class'  => 'form-control',
				);
				$s = "select * from d_site where id_place = '$r->id_place' ";
				$q = $this->db->query($s);
				foreach($q->result_array() as $rq)
				{
					$options_floor[($rq['id_floor'])] = $rq['nama'];
				}
				$this->data['options_floor'] = $options_floor;
				$this->data['selected_floor'] = $this->form_validation->set_value('id_floor',$r->id_floor);
					
				$this->data['denah'] = array(
						'name'  => 'denah',
						'id'    => 'denah',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('denah',$r->denah),
						'class'  => 'form-control',
				);
					
				$this->data['title'] = 'Data Denah';
				$this->data['subtitle'] = '';
				$this->data['page_title'] = 'Form Pemutakhiran Data Denah';
				$this->data['error'] = '';
				$this->data['r'] = $r;
				//print_r($this->data);
				$this->load->view('form_denah',$this->data);
			}
		
		}
		else
		{
			redirect('/');
		}
		
	}
	
	public function hapus_denah()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/setting/denah');
		if($hak_akses['de'] == 1)
		{
			$id = $this->uri->segment(3);
			$sc = "select * from d_denah where id = '$id' ";
			$qc = $this->db->query($sc);
			if($qc->num_rows() > 0)
			{
				$rc = $qc->row();
				unlink('denah/'.$rc->file_gambar);
				$sd = "delete from d_denah where id = '$id' ";
				$this->db->query($sd);
				
				redirect('/setting/denah/');
			}
			else
			{
				redirect('/');
			}
		}
		else
		{
			redirect('/');
		}
		
	}
	
	public function combo_floor()
	{
		$array = array();
		if ($_GET['_name'] == 'id_place') 
		{
			$id_place = $_GET['_value'];
			
				$sql = "SELECT * 
						FROM d_site 
						WHERE id_place = '$id_place'					
						";
				//echo $sql;
				$query = $this->db->query($sql);
				foreach($query->result_array() as $row)
				{
					$array[] = array($row['id_floor'] => $row['nama']);
				}
			
						
		}
		
		echo json_encode( $array );
	}
	
	public function token_akun()
	{
		if(isset($_REQUEST['save']) && $_REQUEST['save'] == 1)
		{
			$su = "update t_setting set value = '".$this->input->post('token_akun')."' where id = 5";
			$this->db->query($su);
		}
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		$token_akun = $this->db->query('select value as token_akun from t_setting where id = 5')->row()->token_akun;
		$data = array(
				'title' => 'Setting Token Akun Proximy',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'token_akun' => $token_akun,
		);
		$this->load->view('token_akun',$data);
		input_log($this->user_id,'lihat setting token akun proximy');
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */