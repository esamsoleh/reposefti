<?php
$array_bulan = array(
	'1' => 'Januari',
	'2' => 'Februari',
	'3' => 'Maret',
	'4' => 'April',
	'5' => 'Mei',
	'6' => 'Juni',
	'7' => 'Juli',
	'8' => 'Agustus',
	'9' => 'September',
	'10' => 'Oktober',
	'11' => 'Nopember',
	'12' => 'Desember',
);
?>
<html>
<head>
<style>
table.tableu {
  border-collapse: collapse;
}
table.tableu td , th  {
  border: 1px solid #999;
  padding: 0.5rem;
  text-align: left;
  vertical-align:top;
}

table.table1 {
  border-collapse: collapse;
}
table.table1 td , th  {
  padding: 0.5rem;
  vertical-align:top;
}
</style>
</head>
<body>
<h3>Rekapitulasi Aktivitas Edit Data Absen Bulan <?=$array_bulan[$bulan];?> Tahun <?=$tahun;?></h3>
<table class="tableu" id="tableu">
	<tr>
		<th>No</th>
		<th>Satuan Kerja</th>
		<th>Jumlah Pengeditan PNS</th>
		<th>Jumlah Pengeditan TKK</th>
	</tr>
	<?php
	$no=1;
	foreach($q->result() as $sheet)
	{
		echo '<tr>';
		echo '<td>'.$no.'</td>';
		echo '<td>'.$sheet->NUNKER.'</td>';
		echo '<td style="text-align:right">'.getRekapAktivitasEditPns($sheet->KUNKER,$bulan,$tahun).'</td>';
		echo '<td style="text-align:right">'.getRekapAktivitasEditTkk($sheet->KUNKER,$bulan,$tahun).'</td>';
		echo '</tr>';
		$no++;
	}
	?>
</table>
</body>
</html> 
<?php

function getRekapAktivitasEditPns($kunker,$bulan,$tahun)
	{
		$CI =& get_instance();
		$s = "select count(a.id) as jumlah
				from log_edit_absensi_pns a
				left join d_pegawai b on(a.nip_pegawai = b.nip)
				where 1=1
				AND month(a.tgl_data) = '$bulan'
				AND year(a.tgl_data) = '$tahun'
				AND (IF((b.kunkom)=10 || (b.kunkom)=11,
							concat(kuntp,kunkom,kununit) =  '".substr($kunker,0,6)."' ,
							concat(kuntp,kunkom) =   '".substr($kunker,0,4)."'  
						)
					)
		";
		//echo $s;
		$jml = $CI->db->query($s)->row()->jumlah;
		return $jml;
	}
	
	function getRekapAktivitasEditTkk($kunker,$bulan,$tahun)
	{
		$CI =& get_instance();
		$s = "select count(a.id) as jumlah
				from log_edit_absensi_tkk a
				left join d_tkk b on(a.nik = b.nik)
				where 1=1
				AND month(a.tgl_data) = '$bulan'
				AND year(a.tgl_data) = '$tahun'
				AND (IF((b.kunkom)=10 || (b.kunkom)=11,
							concat(kuntp,kunkom,kununit) =  '".substr($kunker,0,6)."' ,
							concat(kuntp,kunkom) =   '".substr($kunker,0,4)."'  
						)
					)
		";
		$jml = $CI->db->query($s)->row()->jumlah;
		return $jml;
	}
	
	?>