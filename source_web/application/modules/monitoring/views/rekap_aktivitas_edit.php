<?php $this->load->view('header');?>
<?php
$array_bulan = array(
	'1' => 'Januari',
	'2' => 'Februari',
	'3' => 'Maret',
	'4' => 'April',
	'5' => 'Mei',
	'6' => 'Juni',
	'7' => 'Juli',
	'8' => 'Agustus',
	'9' => 'September',
	'10' => 'Oktober',
	'11' => 'Nopember',
	'12' => 'Desember'
);
?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('top');?>
  <!-- Left side column. contains the logo and sidebar -->
  
  <?php $this->load->view('sidebar');?>
 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title;?>
        <small><?=$subtitle;?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-white">
					<div class="panel-body">
													
						<div class="row">
							<form name="formPegawai" id="formPegawai" method="post" action="/log/harian" class="form-horizontal">
							<input type="hidden" name="cari" value="1">
							
							<div class="row">
									<label  class="col-sm-2 control-label">Bulan</label>
								<div class="col-xs-4">
									<select name="bulan" id="bulan" class="form-control">
										<option value="1" <?=(($bulan == 1)?'selected':'');?>>Januari</option>
										<option value="2" <?=(($bulan == 2)?'selected':'');?>>Februari</option>
										<option value="3" <?=(($bulan == 3)?'selected':'');?>>Maret</option>
										<option value="4" <?=(($bulan == 4)?'selected':'');?>>April</option>
										<option value="5" <?=(($bulan == 5)?'selected':'');?>>Mei</option>
										<option value="6" <?=(($bulan == 6)?'selected':'');?>>Juni</option>
										<option value="7" <?=(($bulan == 7)?'selected':'');?>>Juli</option>
										<option value="8" <?=(($bulan == 8)?'selected':'');?>>Agustus</option>
										<option value="9" <?=(($bulan == 9)?'selected':'');?>>September</option>
										<option value="10" <?=(($bulan == 10)?'selected':'');?>>Oktober</option>
										<option value="11" <?=(($bulan == 11)?'selected':'');?>>Nopember</option>
										<option value="12" <?=(($bulan == 12)?'selected':'');?>>Desember</option>
									</select>
								</div>
							</div>
							<div class="row">
									<label  class="col-sm-2 control-label">Tahun</label>
								<div class="col-xs-4">
									<input class="form-control" type="text" name="tahun" id="tahun" value="<?=$tahun;?>">
								</div>
							</div>
							
							</br>
							</br>
							</br>
														
							</form>					
						</div>										
					</div>
				</div>
			</div>
			<div class="col-xs-1">
			</div>
		</div>
		<div class="row">
		
			<div class="col-xs-12">
				<div class="panel panel-success">
                    <div class="panel-heading">
                        Rekap Log Pengeditan
                    </div>
					<div class="panel-body">
						<button id="eksport_pdf" type="button" class="btn btn-success">
						  Export PDF
						</button>
						<hr>
						<h3 id="judul" style="text-align:center">Rekapitulasi Aktivitas Edit Data Absen Bulan <?=$array_bulan[$bulan];?> Tahun <?=$tahun;?></h3>
                        <div class="table-responsive">
							<table class="table table-bordered" id="tabel">
							<thead>
							<tr>
								<th>No</th>
								<th>Satuan Kerja</th>
								<th>Jumlah Pengeditan PNS</th>
								<th>Jumlah Pengeditan TKK</th>
							</tr>
							</thead>
							<tbody>
							</tbody>
							</table>
						</div>
					
					</div>
				</div>
			</div>
		
		</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
 
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<form id="frm_ekspor_pdf" name="frm_ekspor_pdf" method="post" action="/monitoring/ekspor_rekap_aktivitas_edit_pdf" target="_blank">
		<input type="hidden" name="bulan_pdf" id="bulan_pdf" value="<?=$bulan;?>">
		<input type="hidden" name="tahun_pdf" id="tahun_pdf" value="<?=$tahun;?>">
</form>
<style>
tr.terlambat {
        font-weight: bold;
        color: pink;
		background-color: red;
    }
</style>
<?php $this->load->view('javascript');?>
<script src="/assets/js/jquery.chainedSelects.js"></script>
<script src="/assets/js/dataTables/jquery.dataTables.js"></script>
<script src="/assets/js/dataTables/dataTables.bootstrap.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
	
		var oTable = $("#tabel").dataTable({
			"oLanguage" : {
				"sSearch" : "Search",
				"oPaginate" : {
				"sFirst":"First",
				"sPrevious" : "Previous",
				"sNext" : "Next",
				"sLast":"Last"
			}
			}, 
			"ordering": false,
			 "paging": false,
			//"iDisplayLength" : 10,
			"bFilter" : false,               
			"bLengthChange": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": "/monitoring/get_rekap_aktivitas_edit",
				"type": "POST",
				"data": {
							'bulan': function(){return $("#bulan").val(); }, 
							'tahun': function(){return $("#tahun").val(); }, 
						}
			},
			"createdRow": function ( row, data, index ) {
            if ( (data[5] >= 1) ) {
			   $(row).addClass('terlambat');
            }
			}
		});  
		
		$('#tahun').on( 'keyup', function () {
			oTable.fnDraw();
			var tahun = $('#tahun').val();
			$("h3").text("Rekapitulasi Aktivitas Edit Data Absen Bulan <?=$array_bulan[$bulan];?> Tahun "+tahun)
		} );
		
		$('#bulan').on( 'change', function () {
			oTable.fnDraw();
			var bulan = ($('#bulan').val()-1);
			var nama_bulan = ["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","Nopember","Desember"];
			$("h3").text("Rekapitulasi Aktivitas Edit Data Absen Bulan "+nama_bulan[bulan]+" Tahun <?=$tahun;?>")
		} );
		
		$('#eksport_xls').click(function (e) {
				$('#frm_ekspor_xls').submit();
		});
		$('#eksport_pdf').click(function (e) {
				$('#frm_ekspor_pdf').submit();
		});

		$('#bulan').on( 'change', function () {
		//	frm_ekspor_xls.bulan_xls.value = $('#bulan').val();
			frm_ekspor_pdf.bulan_pdf.value = $('#bulan').val();
		} );
		$('#tahun').on( 'change', function () {
		//	frm_ekspor_xls.tahun_xls.value = $('#tahun').val();
			frm_ekspor_pdf.tahun_pdf.value = $('#tahun').val();
		} );
		
		
		});
		</script>
</body>
</html>
