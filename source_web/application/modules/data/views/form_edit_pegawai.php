<?php $this->load->view('header');?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('top');?>
  <!-- Left side column. contains the logo and sidebar -->
  
  <?php $this->load->view('sidebar');?>
 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title;?>
        <small><?=$subtitle;?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
		<div class="col-xs-12">
					<div class="panel panel-success">
					<div class="panel-heading">
							<h3 class="panel-title"><?=$page_title;?></h3>
						</div>
						<div class="panel-body">
							<?php echo validation_errors(); ?>
							<form name="frm_pegawai" id="frm_pegawai" method="post" action="/<?=$this->uri->uri_string();?>">
							<div class="row">
								<div class="col-xs-3"><label>NIP</label></div>
								<div class="col-xs-9">
									<?=$r->nip;?>
								</div>
							</div>							
							<div class="row">
								<div class="col-xs-3"><label>Nama</label></div>
								<div class="col-xs-9">
									<?=$r->nama;?>
								</div>
							</div>					
							<div class="row">
								<div class="col-xs-3"><label>Pangkat</label></div>
								<div class="col-xs-9">
									<?=$r->pangkat;?>
								</div>
							</div>					
							<div class="row">
								<div class="col-xs-3"><label>Jenis Jabatan</label></div>
								<div class="col-xs-9">
									<?=$r->jenis_jabatan;?>
								</div>
							</div>				
							<div class="row">
								<div class="col-xs-3"><label>Jabatan</label></div>
								<div class="col-xs-9">
									<?=$r->jabatan;?>
								</div>
							</div>					
							<div class="row">
								<div class="col-xs-3"><label>SKPD</label></div>
								<div class="col-xs-9">
									<?=$r->skpd;?>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-3"><label>Status Keaktifan</label></div>
								<div class="col-xs-9">
									<?php
										echo form_dropdown($non_aktif,$options_non_aktif,$selected_non_aktif);
									?>
								</div>
							</div>
							
							</br>
							<div class="row">
								<div class="col-xs-6"><button id="simpan_form" class="btn btn-block btn-success" type="button">Simpan</button></div>
								<div class="col-xs-6"><button id="batal" class="btn btn-block btn-success" type="button">Batal</button></div>
							</div>
						</div>
					</div>
				
		</div>
		</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
 
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('javascript');?>
<script src="/assets/js/jquery.chainedSelects.js"></script>
<script src="/assets/js/dataTables/jquery.dataTables.js"></script>
<script src="/assets/js/dataTables/dataTables.bootstrap.js"></script>
<script type="text/javascript">
$(document).ready(function () {
	required = ["NIP"];
	errornotice = $("#error");
	emptyerror = "Mohon field ini diisi.";
	$('#simpan_form').click(function(){
		//Validate required fields
		for (i=0;i<required.length;i++) {
			var input = $('#'+required[i]);
			if ((input.val() == "") || (input.val() == emptyerror)) {
				input.addClass("needsfilled");
				input.val(emptyerror);
				errornotice.fadeIn(750);
			} else {
				input.removeClass("needsfilled");
			}
		}
			
		//if any inputs on the page have the class 'needsfilled' the form will not submit
		if ($(":input").hasClass("needsfilled")) {
			return false;
		} else {
			errornotice.hide();
			$('#frm_pegawai').submit();
		}
										  
	});
									
	$(":input").focus(function(){		
	   if ($(this).hasClass("needsfilled") ) {
			$(this).val("");
			$(this).removeClass("needsfilled");
		}
	});
	
	$('#batal').click(function(){
        parent.history.back();
        return false;
    });
	
	$('#unit').chainSelect('#subunit','/data/combo_unit',
		{ 
			before:function (target) //before request hide the target combobox and display the loading message
			{ 
				$("#loading").css("display","block");
				$(target).css("display","none");
			},
			after:function (target) //after request show the target combobox and hide the loading message
			{ 
				$("#loading").css("display","none");
				$(target).css("display","inline");
			}
		});
		$('#subunit').chainSelect('#ssubunit','/data/combo_unit',
		{ 
			before:function (target) //before request hide the target combobox and display the loading message
			{ 
				$("#loading").css("display","block");
				$(target).css("display","none");
			},
			after:function (target) //after request show the target combobox and hide the loading message
			{ 
				$("#loading").css("display","none");
				$(target).css("display","inline");
			}
		});
		$('#ssubunit').chainSelect('#sssubunit','/data/combo_unit',
		{ 
			before:function (target) //before request hide the target combobox and display the loading message
			{ 
				$("#loading").css("display","block");
				$(target).css("display","none");
			},
			after:function (target) //after request show the target combobox and hide the loading message
			{ 
				$("#loading").css("display","none");
				$(target).css("display","inline");
			}
		});
	
	
});
</script>
</body>
</html>
