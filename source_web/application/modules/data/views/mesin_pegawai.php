<?php $this->load->view('header');?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('top');?>
  <!-- Left side column. contains the logo and sidebar -->
  
  <?php $this->load->view('sidebar');?>
 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title;?>
        <small><?=$subtitle;?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-white">
					<div class="panel-body">
						<div class="row">							
							<form name="formPegawai" id="formPegawai" method="post" action="/<?=$this->uri->uri_string();?>" class="form-horizontal">
							<input type="hidden" name="cari" value="1">
							
							<div class="row">
									<label class="col-sm-2 control-label">Mesin - Project</label>
								<div class="col-xs-10">
									<select class="form-control" name="id_mesin" id="id_mesin">
										<option value="0">----------</option>
										<?php
										$sql = "SELECT a.id,
													a.serial_number,
													(select nama from d_project where id = a.id_project ) as nama_project
												FROM d_mesin_absensi a
												where 1=1
												ORDER BY id";
										//echo $sql;
										$query = $this->db->query($sql);
										foreach($query->result_array() as $row)
										{
											echo '<option value ="'.$row['id'].'" '.((isset($id_mesin) && $id_mesin == $row['id'])?"selected":"").'>'.($row['serial_number'].'-'.$row['nama_project']).'</option>';
										}
										?>
									</select>
								</div>
							</div>
							
							</br>
							</br>
							</br>
							<div class="row">
								<div class="col-sm-2">
								</div>
								<div class="col-sm-2">
									<button id="tambah" class="btn btn-block btn-success" type="button" onclick="buka_form_tambah_data()">Tambah</button>
								</div>
								
							</div>
							
							</form>					
						</div>										
					</div>
				</div>
			</div>
			<div class="col-xs-1">
			</div>
		</div>
		<div class="row">
		
			<div class="col-xs-12">
				<div class="panel panel-success">
                    <div class="panel-heading">
                        Daftar Mesin Pegawai
                    </div>
					<div class="panel-body">
					<hr>
                        <div class="table-responsive">
							<table id="tabel" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
							<thead>
							<tr>
								<th>No</th>
								<th>Serial Number</th>
								<th>Project</th>
								<th>NIP</th>
								<th>Nama Pegawai</th>
								<th>Hapus</th>
							</tr>
							</thead>
							<tbody>
							</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			
		</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
 
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('javascript');?>
<!-- DataTables -->
        <link href="/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/datatables/fixedHeader.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/datatables/scroller.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/datatables/dataTables.colVis.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/datatables/fixedColumns.dataTables.min.css" rel="stylesheet" type="text/css"/>
		
		<script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="/assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>

        <script src="/assets/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="/assets/plugins/datatables/buttons.bootstrap4.min.js"></script>

        <script src="/assets/plugins/datatables/jszip.min.js"></script>
        <script src="/assets/plugins/datatables/pdfmake.min.js"></script>
        <script src="/assets/plugins/datatables/vfs_fonts.js"></script>

        <script src="/assets/plugins/datatables/buttons.html5.min.js"></script>
        <script src="/assets/plugins/datatables/buttons.print.min.js"></script>

        <script src="/assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
        <script src="/assets/plugins/datatables/dataTables.keyTable.min.js"></script>
        <script src="/assets/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="/assets/plugins/datatables/responsive.bootstrap4.min.js"></script>
        <script src="/assets/plugins/datatables/dataTables.scroller.min.js"></script>
        <script src="/assets/plugins/datatables/dataTables.colVis.js"></script>
        <script src="/assets/plugins/datatables/dataTables.fixedColumns.min.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$('#lihat').click(function (e) {
				$('#formPegawai').submit();
		});
		$('#sinkronisasi').click(function (e) {
				$('#formSinkronisasi').submit();
		});
		
		var oTable = $("#tabel").dataTable({
			"oLanguage" : {
				"sSearch" : "Search",
				"oPaginate" : {
				"sFirst":"First",
				"sPrevious" : "Previous",
				"sNext" : "Next",
				"sLast":"Last"
			}
			}, 
			"ordering": false,
			"iDisplayLength" : 10,
			"bFilter" : false,               
			"bLengthChange": true,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": "/data/get_mesin_pegawai",
				"type": "POST",
				"data": {
							'id_mesin':function(){return $("#id_mesin").val(); },
							'sssubunit':function(){return $("#sssubunit").val(); },
						}
			}
		});  
		
		$('#id_mesin').on( 'change', function () {
			oTable.fnDraw();
		} );
		
		});
		function buka_form_tambah_data()
		{
			window.location.assign('/data/tambah_mesin_pegawai');
		}
		</script>
</body>
</html>
