<?php $this->load->view('header');?>
  <style type="text/css">
    #map {
      width: 100%;
      height: 480px;
    }
  </style>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('top');?>
  <!-- Left side column. contains the logo and sidebar -->
  
  <?php $this->load->view('sidebar');?>
 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title;?>
        <small><?=$subtitle;?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-white">
					<div class="panel-body">
						<?php echo validation_errors(); ?>

							<form name="frm" id="frm" method="post" action="/<?=$this->uri->uri_string();?>" enctype="multipart/form-data">	
								<input type="hidden" name="update" value="1">
								<table class="table">
								<?php
								$array_kual = array();
								foreach($qkual->result() as $kual)
								{
									array_push($array_kual,$kual->kualifikasi);
								}
								$i=1;
								foreach($qck->result() as $rck)
								{
									if (in_array($rck->id, $array_kual))
									{
										$status = 'checked';
									}
									else
									{
										$status = '';
									}
									echo '<tr>';
									echo '<td>'.$i.'</td>';
									echo '<td><input name="id['.$rck->id.']" type="checkbox" value="'.$rck->id.'" '.$status.' ></td>';
									echo '<td>'.$rck->nama.'</td>';
									echo '</tr>';
									$i++;
								}
								?>					
								</table>
							</br>
							<div class="form-group row">
								<div class="col-md-6"><button id="simpan_form" class="btn btn-block btn-success" type="button">Simpan</button></div>
									<div class="col-md-6"><button id="batal" class="btn btn-block btn-success" type="button">Batal</button></div>
								</div>
							</form>
					</div>
				</div>
					
			</div>
		</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
 
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('javascript');?>
<script src="/assets/js/jquery.chainedSelects.js"></script>
<script src="/assets/js/dataTables/jquery.dataTables.js"></script>
<script src="/assets/js/dataTables/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC5Jrp9PtHe0WapppUzxbIpMDWMAcV3qE4"></script>
<script src="https://unpkg.com/location-picker/dist/location-picker.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
		
	$('#simpan_form').click(function(){
				
		$('#frm').submit();
												  
	});
	
	$('#batal').click(function(){
				parent.history.back();
				return false;
			});
											
});
</script>
</body>
</html>
