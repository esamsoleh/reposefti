<?php $this->load->view('header');?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('top');?>
  <!-- Left side column. contains the logo and sidebar -->
  
  <?php $this->load->view('sidebar');?>
 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title;?>
        <small><?=$subtitle;?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-white">
									<div class="panel-body">
									<?php echo validation_errors(); ?>

												<form name="frm" id="frm" method="post" action="/<?=$this->uri->uri_string();?>" enctype="multipart/form-data">													
													<div class="form-group row">
														<div class="col-md-4"><label>Tingkat Bangunan</label></div>
														<div class="col-md-8">
															<?php echo form_input($tingkat);?>
														</div>
													</div>
													<div class="form-group row">
														<div class="col-md-4"><label>Tinggi Tingkat Bangunan (M)</label></div>
														<div class="col-md-8">
															<?php echo form_input($tinggi);?>
														</div>
													</div>
													</br>
													<div class="form-group row">
														<div class="col-md-6"><button id="simpan_form" class="btn btn-block btn-success" type="button">Simpan</button></div>
														<div class="col-md-6"><button id="batal" class="btn btn-block btn-success" type="button">Batal</button></div>
													</div>
												</form>
									</div>
				</div>
					
			</div>
		</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
 
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('javascript');?>
<script src="/assets/js/jquery.chainedSelects.js"></script>
<script src="/assets/js/dataTables/jquery.dataTables.js"></script>
<script src="/assets/js/dataTables/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC5Jrp9PtHe0WapppUzxbIpMDWMAcV3qE4"></script>
<script src="https://unpkg.com/location-picker/dist/location-picker.min.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
		required = ["nama","lokasi_x","lokasi_y"];
			errornotice = $("#error");
			emptyerror = "Mohon field ini diisi.";
			$('#simpan_form').click(function(){
				//Validate required fields
				for (i=0;i<required.length;i++) {
					var input = $('#'+required[i]);
					if ((input.val() == "") || (input.val() == emptyerror)) {
						input.addClass("needsfilled");
						input.val(emptyerror);
						errornotice.fadeIn(750);
					} else {
						input.removeClass("needsfilled");
					}
				}
					
				//if any inputs on the page have the class 'needsfilled' the form will not submit
				if ($(":input").hasClass("needsfilled")) {
					return false;
				} else {
					errornotice.hide();
					$('#frm').submit();
				}
												  
			});
											
			$(":input").focus(function(){		
			   if ($(this).hasClass("needsfilled") ) {
					$(this).val("");
					$(this).removeClass("needsfilled");
				}
			});
			
			$('#batal').click(function(){
				parent.history.back();
				return false;
			});
			
			  var lokasi_x = document.getElementById('lokasi_x');
			  var lokasi_y = document.getElementById('lokasi_y');
			  var map = document.getElementById('map');
				var init_lat = <?php echo ((isset($r->lokasi_x) && $r->lokasi_x != '')?$r->lokasi_x:-6.175469329632223);?>;
				var init_long = <?php echo ((isset($r->lokasi_y) && $r->lokasi_y != '')?$r->lokasi_y:106.82729225921626);?>;
			  // Initialize LocationPicker plugin
			  var lp = new locationPicker(map, {
				setCurrentPosition: true, // You can omit this, defaults to true
				lat: init_lat,
				lng: init_long,
			  }, {
				zoom: 15 // You can set any google map options here, zoom defaults to 15
			  });

			  // Listen to button onclick event
			  $('#confirmPosition').click(function(){
				// Get current location and show it in HTML
				var location = lp.getMarkerPosition();
				//onClickPositionView.innerHTML = 'The chosen location is ' + location.lat + ',' + location.lng;
				lokasi_x.value = location.lat;
				lokasi_y.value = location.lng;
			  });
	
});
		</script>
		<script>
  // Get element references
  

</script>
</body>
</html>
