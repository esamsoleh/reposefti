<?php $this->load->view('header');?>
<?php 
if(isset($row['serial_number']))
{
	$serial_number = $row['serial_number'];
}
elseif(isset($_POST['serial_number']))
{
	$serial_number = $this->db->escape_str($_POST['serial_number']);
}
else
{
	$serial_number = '';
}
if(isset($row['ip_mesin']))
{
	$ip_mesin = $row['ip_mesin'];
}
elseif(isset($_POST['ip_mesin']))
{
	$ip_mesin = $this->db->escape_str($_POST['ip_mesin']);
}
else
{
	$ip_mesin = '';
}
if(isset($row['lokasi']))
{
	$sssubunit = $row['lokasi'];
}
elseif(isset($_POST['sssubunit']))
{
	$sssubunit = $this->db->escape_str($_POST['sssubunit']);
}
else
{
	$sssubunit = '';
}

?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('top');?>
  <!-- Left side column. contains the logo and sidebar -->
  
  <?php $this->load->view('sidebar');?>
 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title;?>
        <small><?=$subtitle;?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
		<div class="col-xs-12">
					<div class="panel panel-success">
					<div class="panel-heading">
							<h3 class="panel-title"><?=$page_title;?></h3>
						</div>
						<div class="panel-body">
							<?php echo validation_errors(); ?>
							<form name="frm_pegawai" id="frm_pegawai" method="post" action="/<?=$this->uri->uri_string();?>">
							<div class="row">
								<div class="col-xs-6"><label>Serial Number</label></div>
								<div class="col-xs-6">
									<?=$serial_number;?>
								</div>
							</div>							
							<div class="row">
								<div class="col-xs-6"><label>IP</label></div>
								<div class="col-xs-6">
									<?=$ip_mesin;?>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6"><label>Lokasi</label></div>
								<div class="col-xs-6">
									<select class="form-control" name="unit" id="unit">
									<?php
									$unit = ((substr($sssubunit,2,2) == '10' or substr($sssubunit,2,2) == '11')?substr($sssubunit,0,6).'000000':substr($sssubunit,0,4).'00000000');
									$s_biro = $this->session->userdata('s_biro');
									//echo 'ssssssssssssssssbiro: '.$s_biro.'</br>';
									if (!empty($s_biro) && $s_biro != '100000000000') 
									{
										$sql = "SELECT * 
												FROM referensi_unit_kerja 
												WHERE (IF(MID('$s_biro',3,2)=10 || MID('$s_biro',3,2)=11,
													mid(kunker,1,6) = mid('$s_biro',1,6)  ,
													mid(kunker,1,4) = mid('$s_biro',1,4)  )
												)
												AND (IF(MID('$s_biro',3,2)=10 || MID('$s_biro',3,2)=11,
													mid(kunker,7,6) = '000000' ,
													mid(kunker,5,8) = '00000000' )
												)
												
												ORDER BY KUNKER";
										//echo $sql.'</br>';
										$query = $this->db->query($sql);
										foreach ($query->result_array() as $row)
										{ 
											echo '<option value ="'.$row['KUNKER'].'" '.((isset($unit) && $unit == $row['KUNKER'])?"selected":"").'>'.($row['NUNKER']).'</option>';
										}
										
									} 											
									else 
									{
										$sql = "SELECT * FROM referensi_unit_kerja 
												WHERE KUNKER LIKE '10__00000000' 
												
												ORDER BY NUNKER";
										$sql = "SELECT * FROM referensi_unit_kerja 
												WHERE (IF(MID(kunker,3,2)=10 || MID(kunker,3,2)=11,
													mid(kunker,7,6) = '000000' ,
													mid(kunker,5,8) = '00000000' )
												)
												
												ORDER BY NUNKER";
										$query = $this->db->query($sql);
										echo '<option value="100000000000" '.((isset($unit) && ($unit == '100000000000'))?"selected":"").'>Semua</option>';			
										foreach ($query->result_array() as $row)
										{ 
											echo '<option value ="'.$row['KUNKER'].'" '.((isset($unit) && $unit == $row['KUNKER'])?"selected":"").'>'.($row['NUNKER']).'</option>';
										} 								
									}
									?>
										
									</select>
								</div>
							</div>
							<div class="row">
									<div class="col-xs-6"><label></label></div>
								<div class="col-xs-6">
									<select class="form-control" name="subunit" id="subunit">
									<?php 
									$subunit = ((substr($sssubunit,2,2) == '10' or substr($sssubunit,2,2) == '11')?substr($sssubunit,0,8).'0000':substr($sssubunit,0,6).'000000');
									if (!empty($s_biro) && $s_biro != '100000000000') 
									{
										//$subunit = $s_biro;
										if(isset($subunit) && $subunit != NULL)
										{
											$sql = "SELECT * 
												FROM referensi_unit_kerja 
												WHERE (IF(MID('$subunit',3,2)=10 || MID('$subunit',3,2)=11,
													mid(kunker,1,6) = mid('$subunit',1,6)  ,
													mid(kunker,1,4) = mid('$subunit',1,4)  )
												)
												AND (IF(MID('$subunit',3,2)=10 || MID('$subunit',3,2)=11,
													mid(kunker,9,4) = '0000' ,
													mid(kunker,7,6) = '000000' )
												)
												
												ORDER BY KUNKER";
											$query = $this->db->query($sql);
											//echo $sql;
											foreach ($query->result_array() as $row)
											{ 
												echo '<option value ="'.$row['KUNKER'].'" '.((isset($subunit) && $subunit == $row['KUNKER'])?"selected":"").'>'.($row['NUNKER']).'</option>';
											}
										}
										else
										{
											$sql = "SELECT * 
													FROM referensi_unit_kerja 
													WHERE MID(KUNKER,1,4) = '$s_biro' 
													AND MID(KUNKER,7,6) = '000000'
													
													ORDER BY KUNKER";
											$sql = "SELECT * 
												FROM referensi_unit_kerja 
												WHERE (IF(MID('$s_biro',3,2)=10 || MID('$s_biro',3,2)=11,
													mid(kunker,1,6) = mid('$s_biro',1,6)  ,
													mid(kunker,1,4) = mid('$s_biro',1,4)  )
												)
												AND (IF(MID('$s_biro',3,2)=10 || MID('$s_biro',3,2)=11,
													mid(kunker,9,4) = '0000' ,
													mid(kunker,7,6) = '000000' )
												)
												
												ORDER BY KUNKER";
													//echo $sql;
											$query = $this->db->query($sql);
											foreach ($query->result_array() as $row)
											{ 
												echo '<option value ="'.$row['KUNKER'].'" '.((isset($subunit) && $subunit == $row['KUNKER'])?"selected":"").'>'.($row['NUNKER']).'</option>';
											}
										}
									}
									else
									{
										//$subunit = $s_biro;
										if(isset($subunit) && $subunit != NULL)
													{
														$sql = "SELECT * 
															FROM referensi_unit_kerja 
															WHERE MID(KUNKER,1,4) = '".substr($subunit,0,4)."' 										
															AND MID(KUNKER,7,6) = '000000'
															
															ORDER BY KUNKER";
														//echo $sql;
														$query = $this->db->query($sql);
														if($query->num_rows() > 0)
														{
															foreach ($query->result_array() as $row)
															{ 
																echo '<option value ="'.$row['KUNKER'].'" '.((isset($subunit) && $subunit == $row['KUNKER'])?"selected":"").'>'.($row['NUNKER']).'</option>';
															}
														}
														else
														{
															echo '<option value="100000000000" '.((isset($subunit) && ($subunit == '100000000000'))?"selected":"").'>Semua</option>';			
														}
													}
													else
													{
														echo '<option value="100000000000">Semua</option>';
													}
									}
									?>	
									</select>
								</div>
							</div>
							<div class="row">
									<div class="col-xs-6"><label></label></div>
								<div class="col-xs-6">
									<select class="form-control" name="ssubunit" id="ssubunit">
										<?php 
										$ssubunit = ((substr($sssubunit,2,2) == '10' or substr($sssubunit,2,2) == '11')?substr($sssubunit,0,10).'00':substr($sssubunit,0,8).'0000');
									if (!empty($s_biro) && $s_biro != '100000000000') 
									{
									//	$ssubunit = $s_biro;
										if(isset($ssubunit) && $ssubunit != NULL)
										{
											$sql = "SELECT * 
												FROM referensi_unit_kerja 
												WHERE (IF(MID('$ssubunit',3,2)=10 || MID('$ssubunit',3,2)=11,
													mid(kunker,1,8) = mid('$ssubunit',1,8)  ,
													mid(kunker,1,6) = mid('$ssubunit',1,6)  )
												)
												AND (IF(MID('$ssubunit',3,2)=10 || MID('$ssubunit',3,2)=11,
													mid(kunker,11,2) = '00' ,
													mid(kunker,9,4) = '0000' )
												)
												
												ORDER BY NUNKER";
											$query = $this->db->query($sql);
											foreach ($query->result_array() as $row)
											{ 
												echo '<option value ="'.$row['KUNKER'].'" '.((isset($ssubunit) && $ssubunit == $row['KUNKER'])?"selected":"").'>'.($row['NUNKER']).'</option>';
											}
										}
										else
										{
											$sql = "SELECT * 
													FROM referensi_unit_kerja 
													WHERE MID(KUNKER,1,4) = '$s_biro' 
													AND MID(KUNKER,5,8) = '00000000'
												
													ORDER BY KUNKER";
											$sql = "SELECT * 
												FROM referensi_unit_kerja 
												WHERE (IF(MID('$s_biro',3,2)=10 || MID('$s_biro',3,2)=11,
													mid(kunker,1,6) = mid('$s_biro',1,6)  ,
													mid(kunker,1,4) = mid('$s_biro',1,4)  )
												)
												AND (IF(MID('$s_biro',3,2)=10 || MID('$s_biro',3,2)=11,
													mid(kunker,7,6) = '000000' ,
													mid(kunker,5,8) = '00000000' )
												)
												
												ORDER BY NUNKER";
													//echo $sql;
											$query = $this->db->query($sql);
											foreach ($query->result_array() as $row)
											{ 
												echo '<option value ="'.$row['KUNKER'].'" '.((isset($ssubunit) && $ssubunit == $row['KUNKER'])?"selected":"").'>'.($row['NUNKER']).'</option>';
											}
										}
										
									}
									else
									{
										if(isset($ssubunit) && $ssubunit != NULL)
													{
														$sql = "SELECT * 
															FROM referensi_unit_kerja 
															WHERE MID(KUNKER,1,6) = '".substr($ssubunit,0,6)."' 
															AND MID(KUNKER,9,4) = '0000'
												
															ORDER BY KUNKER";
														//echo $sql;
														$query = $this->db->query($sql);
														if($query->num_rows() > 0)
														{
															foreach ($query->result_array() as $row)
															{ 
																echo '<option value ="'.$row['KUNKER'].'" '.((isset($ssubunit) && $ssubunit == $row['KUNKER'])?"selected":"").'>'.($row['NUNKER']).'</option>';
															}
														}
														else
														{
															echo '<option value="100000000000" '.((isset($ssubunit) && ($ssubunit == '100000000000'))?"selected":"").'>Semua</option>';
														}
													}
													else
													{
														echo '<option value="100000000000">Semua</option>';
													}
									}
									?>	
									</select>
								</div>
							</div>
							<div class="row">
									<div class="col-xs-6"><label></label></div>
								<div class="col-xs-6">
									<select class="form-control" name="sssubunit" id="sssubunit">
										<?php 
									if (!empty($s_biro) && $s_biro != '100000000000') 
									{
									//	$sssubunit = $s_biro;
										if(isset($sssubunit) && $sssubunit != NULL)
										{
											$sql = "SELECT * 
												FROM referensi_unit_kerja 
												WHERE (IF(MID('$sssubunit',3,2)=10 || MID('$sssubunit',3,2)=11,
													mid(kunker,1,10) = mid('$sssubunit',1,10)  ,
													mid(kunker,1,8) = mid('$sssubunit',1,8)  )
												)
												AND (IF(MID('$sssubunit',3,2)=10 || MID('$sssubunit',3,2)=11,
													1=1,
													mid(kunker,11,2) = '00' )
												)
												
												ORDER BY NUNKER";
											echo $sql;
											$query = $this->db->query($sql);
											foreach ($query->result_array() as $row)
											{ 
												echo '<option value ="'.$row['KUNKER'].'" '.((isset($sssubunit) && $sssubunit == $row['KUNKER'])?"selected":"").'>'.($row['NUNKER']).'</option>';
											}
										}
										else
										{
											$sql = "SELECT * 
													FROM referensi_unit_kerja 
													WHERE MID(KUNKER,1,4) = '$s_biro' 
													AND MID(KUNKER,5,8) = '00000000'
												
													ORDER BY KUNKER";
											$sql = "SELECT * 
												FROM referensi_unit_kerja 
												WHERE (IF(MID('$s_biro',3,2)=10 || MID('$s_biro',3,2)=11,
													mid(kunker,1,6) = mid('$s_biro',1,6)  ,
													mid(kunker,1,4) = mid('$s_biro',1,4)  )
												)
												AND (IF(MID('$s_biro',3,2)=10 || MID('$s_biro',3,2)=11,
													mid(kunker,7,6) = '000000' ,
													mid(kunker,5,8) = '00000000' )
												)
												
												ORDER BY NUNKER";
													echo $sql;
											$query = $this->db->query($sql);
											foreach ($query->result_array() as $row)
											{ 
												echo '<option value ="'.$row['KUNKER'].'" '.((isset($ssubunit) && $ssubunit == $row['KUNKER'])?"selected":"").'>'.($row['NUNKER']).'</option>';
											}
										}
										
									}
									else
									{
										if(isset($sssubunit) && $sssubunit != NULL)
													{
														$sql = "SELECT * 
															FROM referensi_unit_kerja 
															WHERE MID(KUNKER,1,8) = '".substr($sssubunit,0,8)."' 
												
															ORDER BY KUNKER";
														$query = $this->db->query($sql);
														if($query->num_rows() > 0)
														{
															foreach ($query->result_array() as $row)
															{ 
																echo '<option value ="'.$row['KUNKER'].'" '.((isset($sssubunit) && $sssubunit == $row['KUNKER'])?"selected":"").'>'.($row['NUNKER']).'</option>';
															}
														}
														else
														{
															echo '<option value="100000000000" '.((isset($sssubunit) && ($sssubunit == '100000000000'))?"selected":"").'>Semua</option>';
														}
													}
													else
													{
														echo '<option value="100000000000">Semua</option>';
													}
									}
									?>	
									</select>
								</div>
							</div>
								
							</br>
							<div class="row">
								<div class="col-xs-6"><button id="simpan_form" class="btn btn-block btn-success" type="button">Simpan</button></div>
								<div class="col-xs-6"><button id="batal" class="btn btn-block btn-success" type="button">Batal</button></div>
							</div>
						</div>
					</div>
				
		</div>
		</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
 
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('javascript');?>
<script src="/assets/js/jquery.chainedSelects.js"></script>
<script src="/assets/js/dataTables/jquery.dataTables.js"></script>
<script src="/assets/js/dataTables/dataTables.bootstrap.js"></script>
<script type="text/javascript">
$(document).ready(function () {
	required = ["NIP"];
	errornotice = $("#error");
	emptyerror = "Mohon field ini diisi.";
	$('#simpan_form').click(function(){
		//Validate required fields
		for (i=0;i<required.length;i++) {
			var input = $('#'+required[i]);
			if ((input.val() == "") || (input.val() == emptyerror)) {
				input.addClass("needsfilled");
				input.val(emptyerror);
				errornotice.fadeIn(750);
			} else {
				input.removeClass("needsfilled");
			}
		}
			
		//if any inputs on the page have the class 'needsfilled' the form will not submit
		if ($(":input").hasClass("needsfilled")) {
			return false;
		} else {
			errornotice.hide();
			$('#frm_pegawai').submit();
		}
										  
	});
									
	$(":input").focus(function(){		
	   if ($(this).hasClass("needsfilled") ) {
			$(this).val("");
			$(this).removeClass("needsfilled");
		}
	});
	
	$('#batal').click(function(){
        parent.history.back();
        return false;
    });
	
	$('#unit').chainSelect('#subunit','/data/combo_unit',
		{ 
			before:function (target) //before request hide the target combobox and display the loading message
			{ 
				$("#loading").css("display","block");
				$(target).css("display","none");
			},
			after:function (target) //after request show the target combobox and hide the loading message
			{ 
				$("#loading").css("display","none");
				$(target).css("display","inline");
			}
		});
		$('#subunit').chainSelect('#ssubunit','/data/combo_unit',
		{ 
			before:function (target) //before request hide the target combobox and display the loading message
			{ 
				$("#loading").css("display","block");
				$(target).css("display","none");
			},
			after:function (target) //after request show the target combobox and hide the loading message
			{ 
				$("#loading").css("display","none");
				$(target).css("display","inline");
			}
		});
		$('#ssubunit').chainSelect('#sssubunit','/data/combo_unit',
		{ 
			before:function (target) //before request hide the target combobox and display the loading message
			{ 
				$("#loading").css("display","block");
				$(target).css("display","none");
			},
			after:function (target) //after request show the target combobox and hide the loading message
			{ 
				$("#loading").css("display","none");
				$(target).css("display","inline");
			}
		});
	
	
});
</script>
</body>
</html>
