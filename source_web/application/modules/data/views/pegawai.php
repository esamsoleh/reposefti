<?php $this->load->view('header');?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('top');?>
  <!-- Left side column. contains the logo and sidebar -->
  
  <?php $this->load->view('sidebar');?>
 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title;?>
        <small><?=$subtitle;?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-white">
					<div class="panel-body">
					
							<form name="formPegawai" id="formPegawai" method="post" action="/data/pegawai" class="form-horizontal">
							<input type="hidden" name="cari" value="1">
							
							<div class="form-group row">
									<label  class="col-md-2 control-label">Nama</label>
								<div class="col-md-4">
									<input class="form-control" type="text" name="nama" id="nama" value="<?=$nama;?>">
								</div>
									<label  class="col-md-2 control-label">NIP</label>
								<div class="col-md-4">
									<input class="form-control" type="text" name="reqnip" id="reqnip" value="<?=$reqnip;?>">
								</div>
							</div>
							
							<div class="form-group row">
								<div class="col-md-2">
								</div>
								<?php 
									$s_access = $this->session->userdata('s_access');
									if($s_access == 1)
									{
									?>
										<div class="col-md-2">
											<button id="add" class="btn btn-block btn-success" type="button">Tambah Data</button>
										</div>
									<?php
									}
									?>
												
							</div>
							
							</form>	
							
					</div>
				</div>
			</div>
			<div class="col-md-1">
			</div>
		</div>
		<div class="row">
		
			<div class="col-md-12">
				<div class="panel panel-success">
                    <div class="panel-heading">
                        Daftar Pegawai
                    </div>
					<div class="panel-body">
					<hr>
                        <div class="table-responsive">
							<table id="tabel" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
									
							<thead>
							<tr>
							<th>No</th>
							<th>NIP</th>
							<th>Nama</th>
							<th>Jabatan</th>
							<th>Kualifikasi</th>
							<th>Foto</th>
							<th>Edit</th>
							<th>Hapus</th>
							</tr>
							</thead>
							<tbody>
							
							</tbody>
							</table>
						</div>
					
					</div>
				</div>
			</div>
			
		</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
 
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('javascript');?>
<script src="/assets/js/jquery.chainedSelects.js"></script>
<!-- DataTables -->
        <link href="/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/datatables/fixedHeader.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/datatables/scroller.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/datatables/dataTables.colVis.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/datatables/fixedColumns.dataTables.min.css" rel="stylesheet" type="text/css"/>
		<script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="/assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>

        <script src="/assets/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="/assets/plugins/datatables/buttons.bootstrap4.min.js"></script>

        <script src="/assets/plugins/datatables/jszip.min.js"></script>
        <script src="/assets/plugins/datatables/pdfmake.min.js"></script>
        <script src="/assets/plugins/datatables/vfs_fonts.js"></script>

        <script src="/assets/plugins/datatables/buttons.html5.min.js"></script>
        <script src="/assets/plugins/datatables/buttons.print.min.js"></script>

        <script src="/assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
        <script src="/assets/plugins/datatables/dataTables.keyTable.min.js"></script>
        <script src="/assets/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="/assets/plugins/datatables/responsive.bootstrap4.min.js"></script>
        <script src="/assets/plugins/datatables/dataTables.scroller.min.js"></script>
        <script src="/assets/plugins/datatables/dataTables.colVis.js"></script>
        <script src="/assets/plugins/datatables/dataTables.fixedColumns.min.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
				
		var oTable = $("#tabel").dataTable({
			"oLanguage" : {
				"sSearch" : "Search",
				"oPaginate" : {
				"sFirst":"First",
				"sPrevious" : "Previous",
				"sNext" : "Next",
				"sLast":"Last"
			}
			}, 
			"ordering": false,
			"iDisplayLength" : 10,
			"bFilter" : false,               
			"bLengthChange": true,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": "/data/get_pegawai",
				"type": "POST",
				"data": {
							'nama': function(){return $("#nama").val(); }, 
							'reqnip': function(){return $("#reqnip").val(); }, 
						}
			}
		});  
		
		$('#nama,#reqnip').on( 'keyup', function () {
			oTable.fnDraw();
		} );
		
		$('#add').click(function(){
			window.location.assign("/data/tambah_pegawai/")
		});	
		
});
		</script>
</body>
</html>
