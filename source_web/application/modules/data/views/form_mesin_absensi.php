<?php $this->load->view('header');?>
<?php 
if(isset($row['id_project']))
{
	$id_project = $row['id_project'];
}
elseif(isset($_POST['id_project']))
{
	$id_project = $this->db->escape_str($_POST['id_project']);
}
else
{
	$id_project = '';
}
if(isset($row['serial_number']))
{
	$serial_number = $row['serial_number'];
}
elseif(isset($_POST['serial_number']))
{
	$serial_number = $this->db->escape_str($_POST['serial_number']);
}
else
{
	$serial_number = '';
}
if(isset($row['ip']))
{
	$ip = $row['ip'];
}
elseif(isset($_POST['ip']))
{
	$ip = $this->db->escape_str($_POST['ip']);
}
else
{
	$ip = '';
}

?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('top');?>
  <!-- Left side column. contains the logo and sidebar -->
  
  <?php $this->load->view('sidebar');?>
 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title;?>
        <small><?=$subtitle;?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
		<div class="col-xs-12">
					<div class="panel panel-success">
					<div class="panel-heading">
							<h3 class="panel-title"><?=$page_title;?></h3>
						</div>
						<div class="panel-body">
							<?php echo validation_errors(); ?>
							<form name="frm_pegawai" id="frm_pegawai" method="post" action="/<?=$this->uri->uri_string();?>">
							<div class="row">
								<div class="col-xs-6"><label>Project</label></div>
								<div class="col-xs-6">
									<select name="id_project" id="id_project" class="selecter_1 form-control" >
										
									</select>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6"><label>Serial Number</label></div>
								<div class="col-xs-6">
									<input type="text" class="form-control" id="serial_number" name="serial_number" value="<?=set_value('serial_number',($serial_number));?>">
								</div>
							</div>							
							<div class="row">
								<div class="col-xs-6"><label>Alamat IP</label></div>
								<div class="col-xs-6">
									<input type="text" class="form-control" id="ip" name="ip" value="<?=set_value('ip',($ip));?>">
								</div>
							</div>
							
							</br>
							<div class="row">
								<div class="col-xs-6"><button id="simpan_form" class="btn btn-block btn-success" type="button">Simpan</button></div>
								<div class="col-xs-6"><button id="batal" class="btn btn-block btn-success" type="button">Batal</button></div>
							</div>
						</div>
					</div>
				
		</div>
		</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
 
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('javascript');?>
<script src="/assets/js/jquery.chainedSelects.js"></script>
<script src="/assets/js/dataTables/jquery.dataTables.js"></script>
<script src="/assets/js/dataTables/dataTables.bootstrap.js"></script>
<link rel="stylesheet" href="/assets/plugins/select2-4.0.3/dist/css/select2.min.css">
<script src="/assets/plugins/select2-4.0.3/dist/js/select2.full.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
	required = ["id_project,ip,serial_number"];
	errornotice = $("#error");
	emptyerror = "Mohon field ini diisi.";
	$('#simpan_form').click(function(){
		//Validate required fields
		for (i=0;i<required.length;i++) {
			var input = $('#'+required[i]);
			if ((input.val() == "") || (input.val() == emptyerror)) {
				input.addClass("needsfilled");
				input.val(emptyerror);
				errornotice.fadeIn(750);
			} else {
				input.removeClass("needsfilled");
			}
		}
			
		//if any inputs on the page have the class 'needsfilled' the form will not submit
		if ($(":input").hasClass("needsfilled")) {
			return false;
		} else {
			errornotice.hide();
			$('#frm_pegawai').submit();
		}
										  
	});
									
	$(":input").focus(function(){		
	   if ($(this).hasClass("needsfilled") ) {
			$(this).val("");
			$(this).removeClass("needsfilled");
		}
	});
	
	$('#batal').click(function(){
        parent.history.back();
        return false;
    });
	
	$( "#id_project" ).select2({   
		placeholder: "Ketik Nama Project",
		allowClear: false,
		ajax: {
			url: "/data/cari_project",
			dataType: 'json',
			delay: 250,
			data: function (params) {
				return {
					q: params.term // search term
				};
			},
			processResults: function (data) {
				return {
					//results: data
					 results: $.map(data, function (item) {
                    return {
                        text: item.nama,
                        id: item.id
                    }
					 })
				};
			},
			cache: true
		},
		minimumInputLength: 2
	});
	
		
});
</script>
</body>
</html>
