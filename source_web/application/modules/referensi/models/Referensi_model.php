<?php
class Referensi_model extends CI_Model {
	
	private $tablename = array(
	
		'ref_kualifikasi',
		'referensi_persentase_keterlambatan',
		'referensi_persentase_ketidakhadiran',
		'referensi_persentase_pulang_cepat',
		'referensi_proporsi_tunjangan',
		'referensi_tipe_jadwal',
		'referensi_pph_tkk',
	);
	
	
	public function __construct()
    {
            parent::__construct();
	}
	
	public function get_referensi($referensi_id,$fields_selected,$sorting,$row_id,$page_row,$namacari)
	{	
		
			$sql = "select ";
			for ($i=0;$i<count($fields_selected);$i++)
			{
				if($i==(count($fields_selected)-1))
				{
					$sql.=$fields_selected[$i];
				}
				else
				{
					$sql.=$fields_selected[$i].",";
				}
			}
			$sql .= " from ".$this->tablename[$referensi_id-1]." WHERE 1=1 ";
			
			if(isset($namacari) && $namacari != '')
			{
				$sql.='AND ( ';
				for ($i=0;$i<count($fields_selected);$i++)
				{
					if($i==(count($fields_selected)-1))
					{
						$sql.= $fields_selected[$i]." like '%".$namacari."%' ";
					}
					else
					{
						$sql.= $fields_selected[$i]." like '%".$namacari."%' OR ";
					}
				}
				$sql.=' ) ';
			}
			if($sorting != NULL)
			{
				$sql .= " order by ".$sorting." ";
			}
			if(($page_row != 0))
			{
				$sql.=" limit $row_id,$page_row";
			}
		
		//print_r($sql);
		$query = $this->db->query($sql);
		return $query;
	}
	
	function getFields($referensi_id){
	
		return $this->db->field_data($this->tablename[$referensi_id-1]);
	
	}
	
	public function prosessData($fields,$posts,$referensi_id){
		
			$this->db->where($fields[0],$posts[$fields[0]]);
			$query = $this->db->get($this->tablename[$referensi_id-1]);
			if($query->num_rows()==0){
				$sql = "insert into ".$this->tablename[$referensi_id-1]." set ";
				for ($i=0;$i<count($fields);$i++){
					if($i==(count($fields)-1)){
						$sql.= "".$fields[$i]." = '".$posts[$fields[$i]]."'";
					}else{
					$sql.="".$fields[$i]." = '".$posts[$fields[$i]]."',";
					}
				}
				//$sql.=")";
			}else{
				$sql = "update ".$this->tablename[$referensi_id-1]." set ";
				for ($i=0;$i<count($fields);$i++){
					if($i==(count($fields)-1)){
						$sql.="".$fields[$i]." = '".$posts[$fields[$i]]."' ";
					}else{
						$sql.= "".$fields[$i]." = '".$posts[$fields[$i]]."', ";
					}
				}
				$sql.="where ".$fields[0]." = '".$posts[$fields[0]]."'";
			}
		
		print_r($sql);
		$this->db->query($sql);
	}
			
	public function deleteData($fields,$deletes,$referensi_id){		
		print_r($deletes['Delete']);
			for ($i=0;$i<count($deletes['Delete']);$i++){
			
				$sql = "delete from ".$this->tablename[$referensi_id-1]." where ".$fields[0]->name." = '".$deletes['Delete'][$i]."'";
			
		print_r($sql);
			$this->db->query($sql);
		}
		}
		
	public function cari_all($cari,$fields,$referensi_id)
	{	
		if($referensi_id == 3)
		{
		//	$sql = "SELECT * FROM ".$this->tablename[$referensi_id-1]." WHERE ".$fields[1]->name." LIKE '%".$cari."%' limit $row_id, $page_row";
			$sql = "select a.kunker,
							a.nunker,
							a.njab,
							a.unkerjagrade,
							b.NESELON as keselon
					from unkerja a
					left join eselon b
					on (a.keselon = b.KESELON)
					where nunker like '%".$cari."%' 
					";
		}
		if($referensi_id == 6)
		{
		//	$sql = "SELECT * FROM ".$this->tablename[$referensi_id-1]." WHERE ".$fields[1]->name." LIKE '%".$cari."%' limit $row_id, $page_row";
		/* 	$sql = "select a.kunker,
							a.nunker,
							a.njab,
							a.unkerjagrade,
							b.NESELON as keselon
					from unkerja a
					left join eselon b
					on (a.keselon = b.KESELON)
					where nunker like '%".$cari."%' 
					"; */
			$sql = 	"SELECT uu.unit_urusan_id,
							u1.kurusan, 
							u1.nurusan, 
							if(u1.wajib='W','Wajib',if(u1.wajib='P','Pilihan','------')) as wajib, 
							u2.kunker, 
							u2.nunker
					FROM uniturusan uu
				    JOIN urusan u1 ON uu.kurusan = u1.kurusan
					JOIN unkerja u2 ON uu.kunker = u2.kunker
					WHERE (u2.kunker LIKE '10__00000000' OR u2.kunker LIKE '1001____0000')
					AND u1.nurusan like '%".$cari."%'
					ORDER BY u1.kurusan, u2.nunker";
			
		}
		else
		{
			$sql = "SELECT * FROM ".$this->tablename[$referensi_id-1]." WHERE ".$fields[1]." LIKE '%".$cari."%' ";
		}
		//$sql = "SELECT * FROM ".$this->tablename[$referensi_id-1]." WHERE ".$fields[1]->name." LIKE '%".$cari."%'";
		//echo $sql;
		return $query = $this->db->query($sql);
	}
	
	public function cari($cari,$fields_selected,$referensi_id,$page_row,$row_id)
	{
		//print_r($fields_selected);
		if($referensi_id == 3)
		{
		//	$sql = "SELECT * FROM ".$this->tablename[$referensi_id-1]." WHERE ".$fields[1]->name." LIKE '%".$cari."%' limit $row_id, $page_row";
			$sql = "select a.kunker,
							a.nunker,
							a.njab,
							a.unkerjagrade,
							b.NESELON as keselon
					from unkerja a
					left join eselon b
					on (a.keselon = b.KESELON)
					where nunker like '%".$cari."%' 
					order by ".$fields_selected[0]." asc limit $row_id,$page_row 
					";
		}
		if($referensi_id == 6)
		{
		//	$sql = "SELECT * FROM ".$this->tablename[$referensi_id-1]." WHERE ".$fields[1]->name." LIKE '%".$cari."%' limit $row_id, $page_row";
			$sql = "select a.kunker,
							a.nunker,
							a.njab,
							a.unkerjagrade,
							b.NESELON as keselon
					from unkerja a
					left join eselon b
					on (a.keselon = b.KESELON)
					where nunker like '%".$cari."%' 
					order by ".$fields_selected[0]." asc limit $row_id,$page_row 
					";
			$sql = 	"SELECT uu.unit_urusan_id,
							u1.kurusan, 
							u1.nurusan, 
							if(u1.wajib='W','Wajib',if(u1.wajib='P','Pilihan','------')) as wajib, 
							u2.kunker, 
							u2.nunker
					FROM uniturusan uu
				    JOIN urusan u1 ON uu.kurusan = u1.kurusan
					JOIN unkerja u2 ON uu.kunker = u2.kunker
					WHERE (u2.kunker LIKE '10__00000000' OR u2.kunker LIKE '1001____0000')
					AND u1.nurusan like '%".$cari."%'
					ORDER BY u1.kurusan, u2.nunker";
			if(($page_row != 0))
			{
				$sql.=" limit $row_id,$page_row";
			}
		}
		else
		{
			$sql = "SELECT * FROM ".$this->tablename[$referensi_id-1]." WHERE ".$fields_selected[1]." LIKE '%".$cari."%' limit $row_id, $page_row";
		}
	//	echo $sql;
		return $query = $this->db->query($sql);
	}
	
	public function get_gapoks($tahun)
	{	
		
		$sql = "select A.*,
						B.NGOLRU
				from gapok A
				left join golruang B
				on (A.KGOLRU = B.KGOLRU)
				WHERE A.TAHUN = $tahun";
				
		$query = $this->db->query($sql);
		return $query;
	
	}
	
	public function get_gapok($page_row,$row_id,$tahun)
	{	
		
		$sql = "select A.*,
						concat(B.NGOLRU,' (',A.KGOLRU,')') as NGOLRU
				from gapok A
				left join golruang B
				on (A.KGOLRU = B.KGOLRU)
				WHERE A.TAHUN = $tahun
				ORDER BY A.KGOLRU, MKG ASC
				limit $row_id,$page_row";
				
		$query = $this->db->query($sql);
		return $query;
	
	}
	
	public function prosess_data_gapok($posts){
			$this->db->where('KGOLRU',$posts['KGOLRU']);
			$this->db->where('MKG',$posts['MKG']);
			$this->db->where('TAHUN',$posts['TAHUN']);
			$query = $this->db->get($this->tablename[13]);
			if($query->num_rows()==0){
				$sql = "insert into ".$this->tablename[13]." values(";
				$sql.=$posts['KGOLRU'].",";
				$sql.=$posts['MKG'].",";
				$sql.=$posts['TAHUN'].",";
				$sql.=$posts['GPOK'].",";
				$sql.=$posts['TPPCPNS'].",";
				$sql.=$posts['TPPPNS'];
				$sql.=")";
			}else{
				$sql = "update ".$this->tablename[13]." set ";
				$sql.= "GPOK = ".$posts['GPOK'].",";
				$sql.= "TPPCPNS = ".$posts['TPPCPNS'].",";
				$sql.= "TPPPNS = ".$posts['TPPPNS']." ";
				$sql.="where KGOLRU = '".$posts['KGOLRU']."' ";
				$sql.="AND MKG = '".$posts['MKG']."' ";
				$sql.="AND TAHUN = '".$posts['TAHUN']."' ";
			}
		print_r($sql);
		$this->db->query($sql);
			}
	
	public function delete_data_gapok($deletes){		
			for ($i=0;$i<count($deletes['Delete']);$i++){
			$filter = explode("/",$deletes['Delete'][$i]);
			
			$sql = "delete from gapok where KGOLRU = '".$filter[0]."'
											AND MKG = '".$filter[1]."'
											AND TAHUN = '".$deletes['TAHUN']."' ";
		//print_r($sql);	
			$this->db->query($sql);
		}
		}
	
	public function get_keselon($neselon){
		$sql = "select KESELON from eselon where NESELON = '$neselon'";
		
		$query = $this->db->query($sql);
		$row = $query->row();
	
		return $row->KESELON;
	}
	
	public function get_uniturusan($page_row,$row_id)
	{	
		
		$sql = 	"SELECT uu.unit_urusan_id,
							u1.kurusan, 
							u1.nurusan, 
							if(u1.wajib='W','Wajib',if(u1.wajib='P','Pilihan','------')) as wajib, 
							u2.kunker, 
							u2.nunker
					FROM uniturusan uu
				    JOIN urusan u1 ON uu.kurusan = u1.kurusan
					JOIN unkerja u2 ON uu.kunker = u2.kunker
					WHERE u2.kunker LIKE '10__00000000' OR u2.kunker LIKE '1001____0000'
					ORDER BY u1.kurusan, u2.nunker";
			if(($page_row != 0)){
			$sql.=" limit $row_id,$page_row";
				}
		
		//echo $page_row;
	//	print_r($sql);		
		$query = $this->db->query($sql);
		return $query;
	
	}

	public function prosess_data_mapping($posts){
			if($posts['unit_urusan_id']==0){
			$sql_check = "SELECT * FROM uniturusan WHERE kurusan = '".$posts['kurusan']."' AND kunker = '".$posts['kunker']."'";
			$query = $this->db->query($sql_check);
				if($query->num_rows()==0){
					if ($posts['sub']==1) {
						while (substr($posts['kunker'],-2) == '00')
							$posts['kunker'] = substr($posts['kunker'],0,-2);
						$sql = 	"INSERT INTO uniturusan (kunker, kurusan) ".
								"SELECT kunker, '".$posts['kurusan']."' FROM unkerja WHERE kunker LIKE '".$posts['kunker']."%'";
					} else {
						$sql = 	"INSERT INTO uniturusan (kunker, kurusan) VALUES ".
								"('".$posts['kunker']."', '".$posts['kurusan']."')";  
					}
				}else{
					$row = $query->row_array();
					$sql = 	"UPDATE uniturusan 
							SET kurusan = '".$posts['kurusan']."',
								kunker = '".$posts['kunker']."'
							WHERE 	unit_urusan_id = '".$row['unit_urusan_id']."'					
													";
													
					}
			//	print_r($sql);
				$this->db->query($sql);
			}else{
				
				if ($posts['sub']==1) {
					while (substr($posts['kunker'],-2) == '00')
						$posts['kunker'] = substr($posts['kunker'],0,-2);
						
						$sql_select_unker = "select * from uniturusan where kunker like '".$posts['kunker']."%'";
						$query_unker = $this->db->query($sql_select_unker);
					
						foreach ($query_unker->result_array() as $row_unker){
							$sql_update = "update uniturusan 
											set kurusan = '".$posts['kurusan']."'
											where kunker = '".$row_unker['kunker']."' 
											and unit_urusan_id = '".$row_unker['unit_urusan_id']."'";
							
						//	print_r($sql_update);
							$this->db->query($sql_update);
						
						}
				}else{
				
					$sql_update = 	"UPDATE uniturusan 
						SET kurusan = '".$posts['kurusan']."',
							kunker = '".$posts['kunker']."'
						WHERE 	unit_urusan_id = '".$posts['unit_urusan_id']."'					
													";
				//	print_r($sql_update);
					$this->db->query($sql_update);
			}
			
		
			
		}
		}
	
	public function getTable($referensi_id)
	{
		return $this->tablename[$referensi_id-1];
	}

	public function get_provinsi()
	{
		$sql = "select * from referensi_wilayah where TWIL =  1";
		$query = $this->db->query($sql);
		return $query;
	}
	
	public function get_kabkot()
	{
		$sql = "select * from referensi_wilayah where TWIL =  2";
		$query = $this->db->query($sql);
		return $query;
	}
	
}
