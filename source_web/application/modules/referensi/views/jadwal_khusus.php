<?php $this->load->view('header');?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('top');?>
  <!-- Left side column. contains the logo and sidebar -->
  
  <?php $this->load->view('sidebar');?>
 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title;?>
        <small><?=$subtitle;?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
							<div class="col-md-12">
								<div class="panel panel-white">
								<div class="panel-body">
								
									<div id="ReferensiContainer"></div>
								</div>
								</div>
							</div>
         
						</div>
						

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
 
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('javascript');?>
<link href="/assets/js/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
<script src="/assets/js/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<link href="/assets/js/jtable/themes/metro/green/jtable.min.css" rel="stylesheet" type="text/css" />
<script src="/assets/js/jtable/jquery.jtable.min.js" type="text/javascript"></script>

<script type="text/javascript">
         $(document).ready(function () {
			$('#ReferensiContainer').jtable({
                    title: "Daftar Jadwal Khusus",
                    paging: true, //Enable paging
                    pageSize: 10, //Set page size (default: 10)
                    sorting: true, //Enable sorting
                    defaultSorting: 'id ASC', //Set default sorting
                    actions: {
                        listAction: '/referensi/list_jadwal_khusus',
                        createAction: '/referensi/add_jadwal_khusus',
                        updateAction: '/referensi/update_jadwal_khusus',
                        deleteAction: '/referensi/del_jadwal_khusus',
					
                    },
                    fields: {
					id:{
						key:true,
						create:false,
						edit:false,
						list:true,	
						title:'ID',				
					},
					tipe_jadwal:{		
						title:'Tipe Jadwal',	
						create:true,
						edit:true,
						list:true,	
						options:'<?=site_url()?>/referensi/tipe_jadwal_option'				
					},
					hari:{		
						title:'Hari',	
						create:true,
						edit:true,
						list:true,	
						options: { '0': 'Minggu', 
									'1': 'Senin', 
									'2': 'Selasa', 
									'3': 'Rabu', 
									'4': 'Kamis', 
									'5': 'Jumat', 
									'6': 'Sabtu'									
									}						
					},
					jadwal_jam_masuk:{
						create:true,
						edit:true,
						list:true,	
						title:'Jadwal Jam Masuk',				
					},
					jadwal_menit_masuk:{
						create:true,
						edit:true,
						list:true,	
						title:'Jadwal Menit Masuk',				
					},
					jadwal_jam_keluar:{
						create:true,
						edit:true,
						list:true,	
						title:'Jadwal Jam Keluar',				
					},
					jadwal_menit_keluar:{
						create:true,
						edit:true,
						list:true,	
						title:'Jadwal Menit Keluar',				
					},
					aktif:{		
						title:'Aktif',	
						create:true,
						edit:true,
						list:true,	
						options: { '0': 'Tidak Aktif', 
									'1': 'Aktif'									
									}
						
					},
					keterangan:{
						create:true,
						edit:true,
						list:true,	
						title:'Keterangan',				
					},
							
				}
			});
			
			$('#load_cari_data').click(function (e) {
				e.preventDefault();
				$('#ReferensiContainer').jtable('load', {
					namacari: $('#namacari').val()
				});
			});
			//Load list from server
			$('#ReferensiContainer').jtable('load');
				
        });
    </script>
</html>
