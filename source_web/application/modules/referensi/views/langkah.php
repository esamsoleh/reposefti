<!DOCTYPE html>
<!-- Template Name: Rapido - Responsive Admin Template build with Twitter Bootstrap 3.x Version: 1.2 Author: ClipTheme -->
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<?php $this->load->view('header');?> 

    <!-- start: BODY -->
	<body>
		<?php $this->load->view('sliding-bar');?>
		<div class="main-wrapper">
			<?php $this->load->view('topbar');?>
			<?php $this->load->view('vertikal-menu');?>
			<?php $this->load->view('page-slide-right');?>
			<!-- start: MAIN CONTAINER -->
			<div class="main-container inner">
				<!-- start: PAGE -->
				<div class="main-content">					
					<!-- end: SPANEL CONFIGURATION MODAL FORM -->
					<div class="container">
						<!-- start: PAGE HEADER -->
						<!-- start: TOOLBAR -->
						<div class="toolbar row">
							<div class="col-sm-6 hidden-xs">
								<div class="page-header">
									<h1><?=$title;?> <small><?=$subtitle;?></small></h1>
								</div>
							</div>
							
						</div>
						<!-- end: TOOLBAR -->
						<!-- end: PAGE HEADER -->
						<!-- start: BREADCRUMB -->
						<div class="row">
							<div class="col-md-12">
								<ol class="breadcrumb">
									<li class="active">
										<a href="/dashboard">
											Dashboard
										</a>
									</li>
								</ol>
							</div>
						</div>
						<!-- end: BREADCRUMB -->
						<!-- start: PAGE CONTENT -->
						
						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-white">
								<div class="panel-body">
									<form>
									<div class="col-lg-4">
										<input class="form-control" type="text" name="namacari" id="namacari" placeholder="Kata Kunci">
									</div>
									<div class="col-lg-4">
										<button class="btn btn-success" type="submit" id="load_cari_data">Cari</button>
									</div>
									</form>
									</br>
									</br>
									<div id="ReferensiContainer"></div>
								</div>
								</div>
							</div>
         
						</div>
					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>
				</div>
				<!-- end: PAGE -->
			</div>
			<!-- end: MAIN CONTAINER -->
			<?php $this->load->view('footer');?>
		</div>
		
		
	</body>
	<!-- end: BODY -->
	
	<?php $this->load->view('javascript')?>
<?php $this->load->view('footer');?>
<link href="/assets/js/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
<script src="/assets/js/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<link href="/assets/js/jtable/themes/metro/green/jtable.min.css" rel="stylesheet" type="text/css" />
<script src="/assets/js/jtable/jquery.jtable.min.js" type="text/javascript"></script>

<script type="text/javascript">
         $(document).ready(function () {
			$('#ReferensiContainer').jtable({
                    title: "Daftar Syarat Layanan",
                    paging: true, //Enable paging
                    pageSize: 10, //Set page size (default: 10)
                    sorting: true, //Enable sorting
                    defaultSorting: 'id DESC', //Set default sorting
                    actions: {
                        listAction: '<?=site_url();?>/referensi/list_langkah',
                        createAction: '<?=site_url();?>/referensi/add_langkah',
                        updateAction: '<?=site_url();?>/referensi/update_langkah',
                        deleteAction: '<?=site_url()?>/referensi/del_langkah',
					
                    },
                    fields: {
						id:{
						key: true,
						create:false,
						edit:false,
						list:true,		
						title:'ID',			
						},
					id_jenis_layanan:{		
						title:'Jenis Layanan',	
						options:'<?=site_url()?>/referensi/jenis_layanan_options'
						
					},
					langkah:{
						create:true,
						edit:true,
						list:true,	
						title:'Langkah',				
					},
					nama_langkah:{
						create:true,
						edit:true,
						list:true,	
						title:'Nama Langkah',				
					},
					pelaksana:{
						create:true,
						edit:true,
						list:true,	
						title:'Pelaksana',		
						options:'<?=site_url()?>/referensi/jenis_pelaksana'						
					},
					
				}
			});
			
			$('#load_cari_data').click(function (e) {
				e.preventDefault();
				$('#ReferensiContainer').jtable('load', {
					namacari: $('#namacari').val()
				});
			});
			//Load list from server
			$('#ReferensiContainer').jtable('load');
				
        });
    </script>
</html>
