<?php $this->load->view('header');?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('top');?>
  <!-- Left side column. contains the logo and sidebar -->
  
  <?php $this->load->view('sidebar');?>
 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title;?>
        <small><?=$subtitle;?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
							<div class="col-md-12">
								<div class="panel panel-white">
								<div class="panel-body">
									<form>
									<div class="col-lg-4">
										<input class="form-control" type="text" name="namacari" id="namacari" placeholder="Kata Kunci">
									</div>
									<div class="col-lg-4">
										<button class="btn btn-success" type="submit" id="load_cari_data">Cari</button>
									</div>
									</form>
									</br>
									</br>
									<div id="ReferensiContainer"></div>
								</div>
								</div>
							</div>
         
						</div>
						

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
 
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('javascript');?>

<script type="text/javascript">
         $(document).ready(function () {
			$('#ReferensiContainer').jtable({
                    title: "Daftar <?=$type_referensi;?>",
                    paging: true, //Enable paging
                    pageSize: 10, //Set page size (default: 10)
                    sorting: true, //Enable sorting
                    defaultSorting: '<?=$fields[0]->name;?> ASC', //Set default sorting
                    actions: {
                        listAction: '<?=site_url();?>/referensi/list_referensi/<?=$referensi_id;?>',
                        createAction: '<?=site_url();?>/referensi/add_referensi/<?=$referensi_id;?>',
                        updateAction: '<?=site_url();?>/referensi/update_referensi/<?=$referensi_id;?>',
                        deleteAction: '<?=site_url()?>/referensi/del_referensi/<?=$referensi_id;?>',
					
                    },
                    fields: {
					<?php for($i=0;$i<count($field_selected);$i++){?>
					<?=$field_selected[$i];?>:{
					<?php if($i==0){?>
					key: true,
					width:'10%',
					create:false,
					edit:false,
					<? }else{
						?>
						create:true,
						edit:true,
						<?php
					} ?>
					title:'<?=$fields_alias[$i];?>',
					list:true,
					
					},
			<?php } ?>
					}
			});
			
			$('#load_cari_data').click(function (e) {
				e.preventDefault();
				$('#ReferensiContainer').jtable('load', {
					namacari: $('#namacari').val()
				});
			});
			//Load list from server
			$('#ReferensiContainer').jtable('load');
				
        });
    </script>
</body>
</html>