<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Log extends CI_Controller {
	public $apps;
	var $nama_user;
	var $file_foto;
	var $uri_string;
	var $s_biro;
	var $userid;
	var $user_id;
	var $groupid;
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
		if (!$this->ion_auth->logged_in())
			{
				redirect('/auth/login', 'refresh');
			}
		$this->apps = 'Kehadiran';
		$this->load->model('log_model');
		$this->nama_user = $this->ion_auth->user()->row()->first_name.' '.$this->ion_auth->user()->row()->last_name;
		$this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		$this->userid = $this->session->userdata('s_username');
		$this->uri_string = '/'.$this->uri->uri_string();
		$this->s_biro = $this->session->userdata('s_biro');
		$this->user_id = $this->ion_auth->user()->row()->id;
		$this->groupid = $this->ion_auth->get_users_groups($this->user_id)->row()->id;
	}
	 
	public function absensi()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		
		$data = array(
				'title' => 'Data Log Absensi',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'sssubunit' => $this->session->userdata('sssubunit_log_absensi'),
				'jnsjab' => $this->session->userdata('jnsjab_log_absensi'),
				'keselon' => $this->session->userdata('keselon_log_absensi'),
				'nama' => $this->session->userdata('nama_log_absensi'),
				'reqnip' => $this->session->userdata('reqnip_log_absensi'),
				'tanggal' => $this->session->userdata('tanggal_log_absensi'),
		);
		$this->load->view('absensi',$data);
		input_log($this->user_id,'Lihat Data Log Absensi');
	}
	
	public function get_absensi()
	{
		set_time_limit(0);
		//print_r($_REQUEST);
		//$order = $_REQUEST['order'][0];
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/log/absensi');
		//$aColumns = array( 'nip', 'nip','nama', 'nip','nip');
		$nama = $this->input->post('nama');
		$this->session->set_userdata('nama_log_absensi',$nama);
		$reqnip = $this->input->post('reqnip');
		$this->session->set_userdata('reqnip_log_absensi',$reqnip);
		$tanggal = $this->input->post('tanggal');
		$this->session->set_userdata('tanggal_log_absensi',$tanggal);
		
		$sSearch = '';
		
		if(isset($nama) && $nama != '')
		{
			$sSearch .= " and (nama like '%".$nama."%') ";
		}
		if(isset($reqnip) && $reqnip != '')
		{
			$sSearch .= " and (a.nip = '".$reqnip."') ";
		}
		/* if(isset($tanggal) && $tanggal != '')
		{
			$sSearch .= " and (date(b.tanggal) = '".date('Y-m-d',strtotime($tanggal))."') ";
		} */
	
		//limit
		if ( isset( $_REQUEST['start'] ) && $_REQUEST['length'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_REQUEST['start'] ).", ".
				intval( $_REQUEST['length'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "`".$aColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]."` ".
						($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		//$sOrder = " order by ".$aColumns[$order['column']]." ".$order['dir']." ";
		$s0 = "select count(nip) as jml
				from d_pegawai b 
				where 1 = 1
				";
		$s0 .= $sSearch;
		
		$sheet_total = $this->db->query($s0)->row();
		$sheet_total = $sheet_total->jml;
		$s1 = "set @rownum  = 0;";
	//	$this->db->query($s1);
		$s1 = "SELECT a.*,
						a.nip,
						c.nama as nama_jabatan
			FROM d_pegawai a
			left join ref_jabatan c on(a.id_jabatan = c.id)
			WHERE 1=1
			";
		$s1 .= $sSearch;
		$s1 .= " order by a.nip asc ";
		
		$s1 .= $sLimit;
		//echo $s1;
		$sheet1 = $this->db->query($s1);
		$result = array();
		$i=0;
		$no=1;
		foreach($sheet1->result() as $sheet)
		{
			$data_pegawai = data_pegawai($sheet->nip);
			$result[$i]['0'] = $no;
			$result[$i]['1'] = $sheet->nip;
			$result[$i]['2'] = $data_pegawai['nama'];
			$result[$i]['3'] = $data_pegawai['nama_jabatan'];
			//$result[$i]['4'] = date('d-M-Y H:i:s', strtotime($sheet->tanggal));		
			$result[$i]['4'] = $this->get_waktu_tap($sheet->nip,$tanggal);	
			$no++;
			$i++;
		}
		
		$data = $result;
		$results = array(
			"iTotalRecords" => ($sheet_total),
			"iTotalDisplayRecords" => ($sheet_total),
			"aaData"=>$data);
			
		echo json_encode($results);
	}
	
	function get_waktu_tap($nip,$tanggal)
	{
		$tanggal = date('Y-m-d',strtotime($tanggal));
		$result = '';
		$s = "select tanggal from absensi_log where date(tanggal) = '$tanggal' and nip = '$nip' ";
	//	echo $s;
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			foreach($q->result() as $r)
			{
				$result .= date('d-M-Y H:i:s',strtotime($r->tanggal)).'</br>';
			}
		}
		else
		{
			$result .='';
		}
		return $result;
	}
	
	public function absensi_tkk()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		
		$data = array(
				'title' => 'Data Log Absensi TKK',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'sssubunit' => $this->session->userdata('sssubunit_log_absensi_tkk'),
				'nama' => $this->session->userdata('nama_log_absensi_tkk'),
				'nik' => $this->session->userdata('nik_log_absensi_tkk'),
				'tanggal' => $this->session->userdata('tanggal_log_absensi_tkk'),
		);
		$this->load->view('absensi_tkk',$data);
		input_log($this->user_id,'Lihat Data Log Absensi TKK');
	}
	
	public function get_absensi_tkk()
	{
		//print_r($_REQUEST);
		$order = $_REQUEST['order'][0];
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/log/absensi_tkk');
		//$aColumns = array( 'nip', 'nip','nama', 'nip','nip');
		$sssubunit = $this->input->post('sssubunit');
		$this->session->set_userdata('sssubunit_log_absensi_tkk',$sssubunit);
		$nama = $this->input->post('nama');
		$this->session->set_userdata('nama_log_absensi_tkk',$nama);
		$nik = $this->input->post('nik');
		$this->session->set_userdata('nik_log_absensi_tkk',$nik);
		$tanggal = $this->input->post('tanggal');
		$this->session->set_userdata('tanggal_log_absensi_tkk',$tanggal);
		
		$sSearch = '';
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		if(isset($select) && $select != '')
		{
			if(!empty($s_biro)) 
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
			}
			else
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			}
		}
		
		if(isset($nama) && $nama != '')
		{
			$sSearch .= " and (nama like '%".$nama."%') ";
		}
		if(isset($nik) && $nik != '')
		{
			$sSearch .= " and (a.nik = '".$nik."') ";
		}
		if(isset($tanggal) && $tanggal != '')
		{
			$sSearch .= " and (date(b.tanggal) = '".date('Y-m-d',strtotime($tanggal))."') ";
		}
	
		//limit
		if ( isset( $_REQUEST['start'] ) && $_REQUEST['length'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_REQUEST['start'] ).", ".
				intval( $_REQUEST['length'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "`".$aColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]."` ".
						($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		//$sOrder = " order by ".$aColumns[$order['column']]." ".$order['dir']." ";
		$s0 = "select count(b.id_log) as jml
				from absensi_log b 
				left join d_tkk a on(a.nik = b.nip)
				where 1 = 1
				and b.nip != ''
				and a.nik is not null
				";
		$s0 .= $sSearch;
		
		$sheet_total = $this->db->query($s0)->row();
		$sheet_total = $sheet_total->jml;
		$s1 = "set @rownum  = 0;";
		$this->db->query($s1);
		$s1 = "SELECT b.*,
					a.nik
			FROM absensi_log b
			left join d_tkk a on(a.nik = b.nip)
			WHERE 1=1
			and b.nip != ''
			and a.nik is not null
			";
		$s1 .= $sSearch;
		$s1 .= " order by b.tanggal desc ";
		
		$s1 .= $sLimit;
	//	echo $s1;
		$sheet1 = $this->db->query($s1);
		$result = array();
		$i=0;
		$no=1;
		foreach($sheet1->result() as $sheet)
		{
			$data_tkk = data_pegawai($sheet->nik);
			$result[$i]['0'] = $sheet->id_log;
			$result[$i]['1'] = $sheet->nik;
			$result[$i]['2'] = $data_tkk['nama'];
			$result[$i]['3'] = $data_tkk['unit_kerja'];
			$result[$i]['4'] = date('d-M-Y H:i:s', strtotime($sheet->tanggal));		
			$no++;
			$i++;
		}
		
		$data = $result;
		$results = array(
			"iTotalRecords" => ($sheet_total),
			"iTotalDisplayRecords" => ($sheet_total),
			"aaData"=>$data);
			
		echo json_encode($results);
	}
	
	public function harian()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		
		$data = array(
				'title' => 'Data Absensi Harian',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'nama' => $this->session->userdata('nama_harian'),
				'reqnip' => $this->session->userdata('reqnip_harian'),
				'tanggal' => $this->session->userdata('tanggal_harian'),
		);
		$this->load->view('harian',$data);
		input_log($this->user_id,'Lihat Data Absensi harian pegawai');
	}
	
	public function get_harian()
	{
		
		//print_r($_REQUEST);
		//$order = $_REQUEST['order'][0];
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/log/harian');
		//$aColumns = array( 'nip', 'nip','nama', 'nip','nip');
		$nama = $this->input->post('nama');
		$this->session->set_userdata('nama_harian',$nama);
		$reqnip = $this->input->post('reqnip');
		$this->session->set_userdata('reqnip_harian',$reqnip);
		$tanggal = $this->input->post('tanggal');
		$this->session->set_userdata('tanggal_harian',$tanggal);
		$tgl = date('j',strtotime($tanggal));
		$bln = date('n',strtotime($tanggal));
		$thn = date('Y',strtotime($tanggal));
		$hak_edit = 1;
		//cek utk pencegatan
		/* $tgl_max_lapor = $this->db->query('select tanggal from ref_waktu_lapor')->row()->tanggal;
		$tgl_sekarang = date('j');
		if($tgl_sekarang < $tgl_max_lapor)
		{
			$bln_compare = date('n')-1;
			if($bln >= $bln_compare)
			{
				$hak_edit = 1;
			}
			elseif($bln < $bln_compare)
			{
				$hak_edit = 0;
			}
		}
		else
		{
			$bln_compare = date('n');
			if($bln == $bln_compare)
			{
				$hak_edit = 1;
			}
			elseif($bln < $bln_compare)
			{
				$hak_edit = 0;
			}
		} */
		
		$sSearch = '';
		
		if(isset($nama) && $nama != '')
		{
			$sSearch .= " and (nama like '%".$nama."%') ";
		}
		if(isset($reqnip) && $reqnip != '')
		{
			$sSearch .= " and (a.nip = '".$reqnip."') ";
		}
		/* if(isset($tgl) && $tgl != '')
		{
			$sSearch .= " and (b.tanggal = '".$tgl."') ";
		}
		if(isset($bln) && $bln != '')
		{
			$sSearch .= " and (b.bulan = '".$bln."') ";
		}
		if(isset($thn) && $thn != '')
		{
			$sSearch .= " and (b.tahun = '".$thn."') ";
		}
	 */
		//limit
		if ( isset( $_REQUEST['start'] ) && $_REQUEST['length'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_REQUEST['start'] ).", ".
				intval( $_REQUEST['length'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "`".$aColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]."` ".
						($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		//$sOrder = " order by ".$aColumns[$order['column']]." ".$order['dir']." ";
		$s0 = "select count(a.nip) as jml 
				from d_pegawai a
				LEFT JOIN d_absen_harian b on(a.nip = b.nip)
				where 1 = 1
				and a.non_aktif = 0
				";
		$s0 = "select count(a.nip) as jml 
				from d_pegawai a
				where 1 = 1
				and a.non_aktif = 0
				";
		$s0 .= $sSearch;
		
		$sheet_total = $this->db->query($s0)->row();
		$sheet_total = $sheet_total->jml;
	
		$s1 = "SELECT DISTINCT a.nama,
									(select nama from ref_jabatan where id = a.id_jabatan) as jabatan,
									b.*
			FROM d_pegawai a
			LEFT JOIN d_absen_harian b on(a.nip = b.nip)
			WHERE 1=1
			and a.non_aktif = 0";
		$s1 = "SELECT DISTINCT a.nip,a.nama,
									(select nama from ref_jabatan where id = a.id_jabatan) as jabatan
			FROM d_pegawai a
			WHERE 1=1
			and a.non_aktif = 0";
		$s1 .= $sSearch;
		$s1 .= " order by nip asc ";		
		$s1 .= $sLimit;
	//	echo $s1;
		$sheet1 = $this->db->query($s1);
		$result = array();
		$i=0;
		$no=1;
		foreach($sheet1->result() as $sheet)
		{
			$absen = $this->data_absen($sheet->nip,$tanggal);
			$result[$i]['0'] = $no;
			$result[$i]['1'] = $sheet->nip.' </br> '.$sheet->nama;
			$result[$i]['2'] = $sheet->jabatan;
			$result[$i]['3'] = (((isset($absen->libur) && $absen->libur == 1)||(isset($absen->jenis_tidak_masuk) && $absen->jenis_tidak_masuk != 0))?'-':((isset($absen->jadwal_masuk) && $absen->jadwal_masuk != '' && $absen->jadwal_masuk != '0000-00-00 00:00:00' && $absen->jadwal_masuk != NULL)?date('d-m-Y H:i',strtotime($absen->jadwal_masuk)):'shift'));
			$result[$i]['4'] = ((isset($absen->realisasi_masuk) && $absen->realisasi_masuk != '' && $absen->realisasi_masuk != NULL)?date('d-m-Y H:i',strtotime($absen->realisasi_masuk)):'');
			$result[$i]['5'] = 	$absen->keterlambatan_masuk;	
			$result[$i]['6'] = (((isset($absen->libur) && $absen->libur == 1)||(isset($absen->jenis_tidak_masuk) && $absen->jenis_tidak_masuk != 0))?'-':((isset($absen->jadwal_keluar) && $absen->jadwal_keluar != '' && $absen->jadwal_keluar != '0000-00-00 00:00:00' && $absen->jadwal_keluar != NULL)?date('d-m-Y H:i',strtotime($absen->jadwal_keluar)):'shift'));
			$result[$i]['7'] = ((isset($absen->realisasi_keluar) && $absen->realisasi_keluar != '' && $absen->realisasi_keluar != NULL)?date('d-m-Y H:i',strtotime($absen->realisasi_keluar)):'');
			$result[$i]['8'] = 	$absen->kecepatan_keluar;	
			
			$no++;
			$i++;
		}
		
		$data = $result;
		$results = array(
			"iTotalRecords" => ($sheet_total),
			"iTotalDisplayRecords" => ($sheet_total),
			"aaData"=>$data);
			
		echo json_encode($results);
	}
	
	function data_absen($nip,$tanggal_buat)
	{
		$added_date = date('Y-m-d H:i:s');
		$added_by = $this->user_id;
		$tanggal = date('j',strtotime($tanggal_buat));
		$bulan = date('n',strtotime($tanggal_buat));
		$tahun = date('Y',strtotime($tanggal_buat));
		$sc = "select * from d_absen_harian where tanggal = '$tanggal' and bulan = '$bulan' and tahun = '$tahun' and nip = '$nip' ";
				//	echo $sc.'</br>';
				$qcabsen = $this->db->query($sc);
				if($qcabsen->num_rows() == 0)
				{
						$st = "select a.*,
										b.*
								from d_pegawai_tipe_jadwal a
								left join ref_tipe_jadwal b on(a.tipe_jadwal = b.id)
								where nip = '$nip' 
								
								";
						$rst = $this->db->query($st)->row_array();
						if($rst['shift'] == 0)
						{			
							$cek_libur = $this->cek_libur($tanggal_buat,$rst['sabtu_masuk']);
							if($cek_libur == 1)
							{
								$libur = 1;
								$persentase_potongan = 0;
								$jadwal_jam_masuk = $rst['jadwal_jam_masuk'];
								$jadwal_menit_masuk = $rst['jadwal_menit_masuk'];
								$jadwal_masuk = date('Y-m-d H:i',strtotime($tanggal.'-'.$bulan.'-'.$tahun.' '.$jadwal_jam_masuk.':'.$jadwal_menit_masuk));
								$jadwal_jam_keluar = $rst['jadwal_jam_keluar'];
								$jadwal_menit_keluar = $rst['jadwal_menit_keluar'];
								$jadwal_keluar = date('Y-m-d H:i',strtotime($tanggal.'-'.$bulan.'-'.$tahun.' '.$jadwal_jam_keluar.':'.$jadwal_menit_keluar));
								$jadwal_masuk = NULL;
								$jadwal_keluar = NULL;
								$si = "insert into d_absen_harian
										set nip = '$nip',
											tanggal = '$tanggal',
											bulan = '$bulan',
											tahun = '$tahun',
											libur = '$libur',
											added_date = '$added_date',
											added_by = '$added_by'
								";
							//	echo $si.'</br>';
								$qi = $this->db->query($si);
							}
							else
							{
								$libur = 0;
								
								//cek jika jadwal khusus
								$hari_ini = date('w',strtotime($tanggal_buat));
								$sc = "select * from ref_jadwal_khusus where hari = '$hari_ini' and aktif = 1 and tipe_jadwal = '$rst[tipe_jadwal]' ";
								$qc = $this->db->query($sc);
								if($qc->num_rows() > 0)
								{
									$rjk = $qc->row_array();
									$jadwal_jam_masuk = $rjk['jadwal_jam_masuk'];
									$jadwal_menit_masuk = $rjk['jadwal_menit_masuk'];
									$jadwal_masuk = date('Y-m-d H:i',strtotime($tanggal.'-'.$bulan.'-'.$tahun.' '.$jadwal_jam_masuk.':'.$jadwal_menit_masuk));
									$jadwal_jam_keluar = $rjk['jadwal_jam_keluar'];
									$jadwal_menit_keluar = $rjk['jadwal_menit_keluar'];
									$jadwal_keluar = date('Y-m-d H:i',strtotime($tanggal.'-'.$bulan.'-'.$tahun.' '.$jadwal_jam_keluar.':'.$jadwal_menit_keluar));	
								}
								else
								{
									$jadwal_jam_masuk = $rst['jadwal_jam_masuk'];
									$jadwal_menit_masuk = $rst['jadwal_menit_masuk'];
									$jadwal_masuk = date('Y-m-d H:i',strtotime($tanggal.'-'.$bulan.'-'.$tahun.' '.$jadwal_jam_masuk.':'.$jadwal_menit_masuk));
									$jadwal_jam_keluar = $rst['jadwal_jam_keluar'];
									$jadwal_menit_keluar = $rst['jadwal_menit_keluar'];
									$jadwal_keluar = date('Y-m-d H:i',strtotime($tanggal.'-'.$bulan.'-'.$tahun.' '.$jadwal_jam_keluar.':'.$jadwal_menit_keluar));	
								}
								
								$si = "insert into d_absen_harian
										set nip = '$nip',
											tanggal = '$tanggal',
											bulan = '$bulan',
											tahun = '$tahun',
											jadwal_masuk = '$jadwal_masuk',
											jadwal_keluar = '$jadwal_keluar',
											libur = '$libur',
											added_date = '$added_date',
											added_by = '$added_by'
								";
							//	echo $si.'</br>';
								$qi = $this->db->query($si);
							}
							
						}
						else
						{
							$tgl = date('Y-m-d',strtotime($tanggal_buat));
							$sc = "select a.* 
								from d_jadwal_lembur a
								where nip = '$nip'
								and tanggal = '$tgl'
								";
							$qc = $this->db->query($sc);
							if($qc->num_rows() > 0)
							{
								$rc = $qc->row();
								if($rc->is_libur == 1)
								{
									$libur = $rc->libur;
									$persentase_potongan = 0;
									$jadwal_masuk = NULL;
									$jadwal_keluar = NULL;
									$si = "insert into d_absen_harian
										set nip = '$nip',
											tanggal = '$tanggal',
											bulan = '$bulan',
											tahun = '$tahun',
											libur = '$libur',
											added_date = '$added_date',
											added_by = 'system'
									";
								//	echo $si.'</br>';
									$qi = $this->db->query($si);
								}
								else
								{
									$jadwal_masuk = $rc->jadwal_masuk;
									$jadwal_keluar = $rc->jadwal_keluar;
									$libur = 0;
									$si = "insert into d_absen_harian
										set nip = '$nip',
											tanggal = '$tanggal',
											bulan = '$bulan',
											tahun = '$tahun',
											jadwal_masuk = '$jadwal_masuk',
											jadwal_keluar = '$jadwal_keluar',
											libur = '$libur',									
											added_date = '$added_date',
											added_by = 'system'
									";
								//	echo $si.'</br>';
									$qi = $this->db->query($si);
								}						
								
							}
							else
							{
								$libur = 1;
								$persentase_potongan = 0;
								$jadwal_masuk = NULL;
								$jadwal_keluar = NULL;
								$si = "insert into d_absen_harian
										set nip = '$nip',
											tanggal = '$tanggal',
											bulan = '$bulan',
											tahun = '$tahun',
											libur = '$libur',
											added_date = '$added_date',
											added_by = 'system'
								";
							//	echo $si.'</br>';
								$qi = $this->db->query($si);
							}
							
						}
						
				}
				
				
		$sa = "select * from absensi_log where nip = '$nip' and day(tanggal) = '$tanggal' and month(tanggal) = '$bulan' and year(tanggal) = '$tahun' order by tanggal asc";
		$qa = $this->db->query($sa);
		if($qa->num_rows() > 0)
		{
			foreach($qa->result() as $ra)
			{
				$waktu = $ra->tanggal;
				$scabsen = "select * from d_absen_harian where tanggal = '$tanggal' and bulan = '$bulan' and tahun = '$tahun' and nip = '$nip' ";
				$rabsen = $this->db->query($scabsen)->row();
				$jadwal_masuk = $rabsen->jadwal_masuk;
				$realisasi_masuk = $rabsen->realisasi_masuk;
				$jadwal_keluar = $rabsen->jadwal_keluar;
				$realisasi_keluar = $rabsen->realisasi_keluar;
				
				if($realisasi_masuk == NULL OR $realisasi_masuk == '0000-00-00')
				{
					if((strtotime($waktu)) > (strtotime($jadwal_masuk)))
					{
						$keterlambatan_masuk = (((strtotime($waktu)) - (strtotime($jadwal_masuk)))/60);
					}
					else
					{
						$keterlambatan_masuk=0;
					}
					$array_update = array(
						'realisasi_masuk' => $waktu,
						'keterlambatan_masuk' => $keterlambatan_masuk,
					);
					$this->db->where('nip', $nip);
					$this->db->where('tanggal', $tanggal);
					$this->db->where('bulan', $bulan);
					$this->db->where('tahun', $tahun);
					$this->db->update('d_absen_harian',$array_update);
				}
				else
				{
					if((strtotime($waktu)) < (strtotime($jadwal_keluar)))
					{
						$kecepatan_keluar = (((strtotime($jadwal_keluar)) - (strtotime($waktu)))/60);
					}
					else
					{
						$kecepatan_keluar=0;
					}
					$array_update = array(
						'realisasi_keluar' => $waktu,
						'kecepatan_keluar' => $kecepatan_keluar,
					);
					$this->db->where('nip', $nip);
					$this->db->where('tanggal', $tanggal);
					$this->db->where('bulan', $bulan);
					$this->db->where('tahun', $tahun);
					$this->db->update('d_absen_harian',$array_update);
				}
			}
		}
		
		$sh = "select * from d_absen_harian where nip = '$nip' and tanggal = '$tanggal' and bulan = '$bulan' and tahun = '$tahun' ";
		$qh = $this->db->query($sh);
		return $qh->row();
	}
	
	public function edit_harian()
	{
		$id = $this->uri->segment(3);
		$s = "select a.*,
					if(b.kunkom = 10 || b.kunkom = 11,concat(b.kuntp,b.kunkom,b.kununit,'000000'),concat(b.kuntp,b.kunkom,'00000000')) as biro,
					b.nama as nama_lengkap,
					(select ngolru from referensi_golongan_ruang where kgolru = b.kgolru) as golongan,
					b.jabatan,
					CASE KUNKOM
								   WHEN 10 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' 	)
								   WHEN 11 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' 	) 
								   ELSE (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,4) = concat(kuntp,kunkom) and mid(kunker,5,8) = '00000000' 	)
					END as skpd
				from d_absen_harian a 
				left join d_pegawai b on(a.nip = b.nip)
			where id = '$id'
			and b.non_aktif = 0
				";
		//echo $s;
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			$biro = $r->biro;
			if($this->s_biro != '100000000000' && $this->s_biro != '' && $this->s_biro != $biro)
			{
				redirect('/');
			}
			
			$this->load->library('form_validation');
			$this->form_validation->set_rules('jadwal_masuk', 'Jadwal Masuk', 'trim|required');
			$this->form_validation->set_rules('realisasi_masuk', 'Realisasi Masuk', 'trim|required');
			$this->form_validation->set_rules('jadwal_keluar', 'Jadwal Masuk', 'trim|required');
			$this->form_validation->set_rules('realisasi_keluar', 'Realisasi Keluar', 'trim|required');
			
			if ($this->form_validation->run() === TRUE)
			{
				$posts = $this->input->post();
			//	print_r($posts);
			//	echo '</br>';
				$jadwal_masuk = $this->db->escape_str($posts['jadwal_masuk']);				
				$realisasi_masuk = $this->db->escape_str($posts['realisasi_masuk']);
				$realisasi_masuk = date('Y-m-d H:i',strtotime($realisasi_masuk));
				$keterlambatan_masuk = (strtotime($realisasi_masuk) - strtotime($jadwal_masuk))/60;
				if($keterlambatan_masuk > 0)
				{
					$keterlambatan_masuk = $keterlambatan_masuk;
					$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
				}
				else
				{
					$keterlambatan_masuk = 0;
					$potongan_terlambat_masuk=0;
				}
				
				$jadwal_keluar = $this->db->escape_str($posts['jadwal_keluar']);	
				$realisasi_keluar = $this->db->escape_str($posts['realisasi_keluar']);
				$realisasi_keluar = date('Y-m-d H:i',strtotime($realisasi_keluar));
				$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($realisasi_keluar))/60;
				if($kecepatan_keluar > 0)
				{
					$kecepatan_keluar = $kecepatan_keluar;
					$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
				}
				else
				{
					$kecepatan_keluar = 0;
					$potongan_pulang_cepat=0;
				}
				$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat;
											
				$update_date = date('Y-m-d H:i:s');
				$update_by = $this->userid;
						
				$jadwal_masuk = date('Y-m-d H:i:s',strtotime($jadwal_masuk));	
				$jadwal_keluar = date('Y-m-d H:i:s',strtotime($jadwal_keluar));	
				
				$keterangan = $this->db->escape_str($posts['keterangan']);	
				$sql = 	"update d_absen_harian 
							SET jadwal_masuk = '$jadwal_masuk',
								realisasi_masuk = '$realisasi_masuk',
								keterlambatan_masuk = '$keterlambatan_masuk',
								potongan_terlambat_masuk = '$potongan_terlambat_masuk',
								jadwal_keluar = '$jadwal_keluar',
								realisasi_keluar = '$realisasi_keluar',
								kecepatan_keluar = '$kecepatan_keluar',
								potongan_pulang_cepat = '$potongan_pulang_cepat',
								persentase_potongan = '$persentase_potongan',
								keterangan = '$keterangan',
								update_date = '$update_date',
								jenis_tidak_masuk = '0',
								potongan_tidak_masuk = '0',
								update_by = '$update_by',
								edited = '1'
							where id = '$id'
								";
				//echo $sql.'</br>';
				$this->db->query($sql);	
				$this->update_data_rekap_keuangan($r->nip,$jadwal_masuk,$persentase_potongan);		
				$this->input_log_edit_absensi($id,$this->user_id,$r,$update_date);
				input_log($this->user_id,'Edit Data Absensi dengan id:'.$id);
				redirect('/log/harian');
			}
			else
			{
				$this->data['jadwal_masuk'] = array(
						'name'  => 'jadwal_masuk',
						'id'    => 'jadwal_masuk',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('jadwal_masuk',(isset($r->jadwal_masuk)?date('d-m-Y H:i',strtotime($r->jadwal_masuk)):'')),
						'class'  => 'form-control datepicker',
					);
				$this->data['realisasi_masuk'] = array(
						'name'  => 'realisasi_masuk',
						'id'    => 'realisasi_masuk',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('realisasi_masuk',(isset($r->realisasi_masuk)?date('d-m-Y H:i',strtotime($r->realisasi_masuk)):'')),
						'class'  => 'form-control datepicker',
					);	
				$this->data['jadwal_keluar'] = array(
						'name'  => 'jadwal_keluar',
						'id'    => 'jadwal_keluar',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('jadwal_keluar',(isset($r->jadwal_keluar)?date('d-m-Y H:i',strtotime($r->jadwal_keluar)):'')),
						'class'  => 'form-control datepicker',
					);	
				$this->data['realisasi_keluar'] = array(
						'name'  => 'realisasi_keluar',
						'id'    => 'realisasi_keluar',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('realisasi_keluar',(isset($r->realisasi_keluar)?date('d-m-Y H:i',strtotime($r->realisasi_keluar)):'')),
						'class'  => 'form-control datepicker',
					);	
				$this->data['keterangan'] = array(
						'name'  => 'keterangan',
						'id'    => 'keterangan',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('keterangan',(($r->keterangan))),
						'class'  => 'form-control',
				);	
				$this->data['title'] = 'Data Kehadiran Harian Pegawai';
				$this->data['subtitle'] = 'Form Pemutakhiran Data Kehadiran Harian';
				$this->data['error'] = '';
				$this->data['r'] = $r;
				$this->load->view('form_edit_harian',$this->data);
			}
		
		}
		else
		{
			redirect('/');
		}
		
	}
	
	function input_log_edit_absensi($id,$userid,$r,$update_date)
	{
		$data = array(
			'userid' => $userid,
			'ip' => $_SERVER['REMOTE_ADDR'],
			'nip_pegawai' => $r->nip,
			'id_data' => $id,
			'tgl_data' => date('Y-m-d',strtotime($r->tanggal.'-'.$r->bulan.'-'.$r->tahun)),
			'data_lama' => serialize($r),
			'update_date' => $update_date,
		);
		$this->db->insert('log_edit_absensi_pns',$data);
		//echo $this->db->last_query();
	}
	
	public function ekspor_absen_harian_pns_pdf()
	{
		set_time_limit(0);
		$this->load->library('pdf');
		$pdf = $this->pdf->load('format => A-2 L');
	//	print_r($_REQUEST);
		$sssubunit = $this->input->post('sssubunit_pdf');
		$jnsjab = $this->input->post('jnsjab_pdf');
		$keselon = $this->input->post('keselon_pdf');
		$nama = $this->input->post('nama_pdf');
		$reqnip = $this->input->post('reqnip_pdf');
		$tanggal = $this->input->post('tanggal_pdf');
		$tgl = date('j',strtotime($tanggal));
		$bln = date('n',strtotime($tanggal));
		$thn = date('Y',strtotime($tanggal));
		$sSearch = '';
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		if(isset($select) && $select != '')
		{
			if(!empty($s_biro)) 
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
			}
			else
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			}
		}
		
		if(isset($nama) && $nama != '')
		{
			$sSearch .= " and (nama like '%".$nama."%') ";
		}
		if(isset($reqnip) && $reqnip != '')
		{
			$sSearch .= " and (a.nip = '".$reqnip."') ";
		}
		if(isset($jnsjab) && $jnsjab != '0' && $jnsjab != '')
		{
			$sSearch .= " and (jnsjab = '".$jnsjab."') ";
		}
		if(isset($keselon) && $keselon != '0' && $keselon != '')
		{
			$sSearch .= " and (keselon = '".$keselon."') ";
		}
		if(isset($tgl) && $tgl != '')
		{
			$sSearch .= " and (b.tanggal = '".$tgl."') ";
		}
		if(isset($bln) && $bln != '')
		{
			$sSearch .= " and (b.bulan = '".$bln."') ";
		}
		if(isset($thn) && $thn != '')
		{
			$sSearch .= " and (b.tahun = '".$thn."') ";
		}
	
		$s1 = "SELECT DISTINCT a.nama,
									a.keselon,
									a.kgolru,
									a.kuntp,
									a.kunkom,
									a.kununit,
									a.tanggal_lahir,
									a.jabatan,
									(select neselon from referensi_eselon where keselon = a.keselon) as eselon,
									(select ngolru from referensi_golongan_ruang where KGOLRU = a.KGOLRU) as golongan,
									(select pangkat from referensi_golongan_ruang where KGOLRU = a.KGOLRU) as pangkat,
									b.*,
									if(b.jenis_tidak_masuk != 0,(select jenis_ketidakhadiran from referensi_persentase_ketidakhadiran where id = b.jenis_tidak_masuk),'-') as ketidakhadiran
			FROM d_pegawai a
			LEFT JOIN d_absen_harian b on(a.nip = b.nip)
			WHERE 1=1
			and a.non_aktif = 0";
		$s1 .= $sSearch;
		$s1 .= " order by keselon asc, kgolru desc, nip asc ";
		//echo $s1;
		$q= $this->db->query($s1);
		$data['q'] = $q;
		$data['tanggal'] = $tanggal;
		$data['unit_kerja'] = $this->get_unit_kerja($sssubunit);
		$html = $this->load->view('cetak_absen_harian_pns',$data,true);
		input_log($this->user_id,'Ekspor absen harian pns ke dalam bentuk PDF');
		$pdf->WriteHTML($html);
		$pdf->Output();
			
	}
	
	function get_unit_kerja($sssubunit)
	{
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		if(isset($select) && $select != '')
		{
			if(!empty($s_biro)) 
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
			}
			else
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			}
		}
		
		$s = "select nunker from referensi_unit_kerja where 1=1 and kunker = '$sssubunit' ";
	//	$s .= $sSearch;
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			$row = $q->row()->nunker;
		}
		else
		{
			$row = '';
		}
		return $row;
	}
	
	function get_potongan_terlambat_masuk($keterlambatan_masuk)
	{
		$sc = "select * from referensi_persentase_keterlambatan where 1=1";
		
		$qc = $this->db->query($sc);
		$persentase_pemotongan=0;
		foreach($qc->result() as $row)
		{
			$awal = $row->awal;
			$akhir = $row->akhir;
			$presentase = $row->presentase;
			if($keterlambatan_masuk >= $awal && $keterlambatan_masuk <= $akhir)
			{
				$persentase_pemotongan = $presentase;
			}
		}
		
		$persentase_pemotongan = ((isset($persentase_pemotongan) && $persentase_pemotongan > 0)?$persentase_pemotongan:0);
		return $persentase_pemotongan;
	}
	
	function get_potongan_pulang_cepat($kecepatan_keluar)
	{
		$sc = "select * from referensi_persentase_pulang_cepat where 1=1";
		$qc = $this->db->query($sc);
		foreach($qc->result() as $row)
		{
			$awal = $row->awal;
			$akhir = $row->akhir;
			$presentase = $row->presentase;
			if($kecepatan_keluar >= $awal && $kecepatan_keluar <= $akhir)
			{
				$persentase_pemotongan = $presentase;
			}
		}
		$persentase_pemotongan = ((isset($persentase_pemotongan) && $persentase_pemotongan > 0)?$persentase_pemotongan:0);
		return $persentase_pemotongan;
	}
	
	function update_data_rekap_keuangan($nip,$tanggal,$pemotongan_per_hari)
	{
		$tanggal = date('Y-m-d',strtotime($tanggal));
		$update_date = date('Y-m-d H:i:s');
		$s = "select id from d_rekap_keuangan_harian where tanggal = '$tanggal' and nip = '$nip' ";
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			$id = $q->row()->id;
			$tpp = $this->get_tpp($nip);
			$si = "update d_rekap_keuangan_harian 
					set tanggal = '$tanggal',
						nip = '$nip',
						total_tpp = '$tpp[total_tpp]',
						tpp_statis = '$tpp[tpp_statis]',
						tpp_dinamis = '$tpp[tpp_dinamis]',
						pemotongan_per_hari = '$pemotongan_per_hari',
						update_date = '$update_date',	
						update_by = 'system'
					where id = '$id'
			";
			$this->db->query($si);
		}
		else
		{
			$tpp = $this->get_tpp($nip);
			$si = "insert into d_rekap_keuangan_harian 
					set tanggal = '$tanggal',
						nip = '$nip',
						total_tpp = '$tpp[total_tpp]',
						tpp_statis = '$tpp[tpp_statis]',
						tpp_dinamis = '$tpp[tpp_dinamis]',
						pemotongan_per_hari = '$pemotongan_per_hari',
						update_date = '$update_date',	
						update_by = 'system'					
			";
			$this->db->query($si);
		}
		$this->db->insert('coba_coba',array('keterangan'=>$si,'waktu'=>date('Y-m-d H:i:s')));
	}
	
	function get_tpp($nip)
	{
		$sp = "select * from referensi_proporsi_tunjangan";
		$qp = $this->db->query($sp);
		$proporsi = array();
		foreach($qp->result() as $rp)
		{
			$proporsi[$rp->status] = $rp->proporsi;
		}
		$s = "select jnsjab,kstatus from d_pegawai where nip = '$nip' ";
		
		$jnsjab = $this->db->query($s)->row()->jnsjab;
		$kstatus = $this->db->query($s)->row()->kstatus;
		switch($jnsjab)
		{
			case 1:
				$s1 = "select b.tunjangan 
						from d_pegawai a 
						left join referensi_unit_kerja b on(a.kunker = b.kunker)
					where a.nip = '$nip'
						";
				
				$q1 = $this->db->query($s1);
				if($q1->num_rows() > 0)
				{
					$tunjangan = $q1->row()->tunjangan;
					$result['total_tpp'] = $tunjangan;
					$result['tpp_statis'] = $tunjangan*($proporsi['statis']/100);
					$result['tpp_dinamis'] = $tunjangan*($proporsi['dinamis']/100);					
				}
				else
				{
					$result['total_tpp'] = 0;
					$result['tpp_statis'] = 0;
					$result['tpp_dinamis'] = 0;	
				}
			break;
			
			case 2:
				$s1 = "select b.tunjangan 
						from d_pegawai a 
						left join referensi_jabatan_fungsional_tertentu b on(a.kjab = b.KJAB)
					where a.nip = '$nip'
						";
				$q1 = $this->db->query($s1);
				if($q1->num_rows() > 0)
				{
					$tunjangan = $q1->row()->tunjangan;
					$result['total_tpp'] = $tunjangan;
					$result['tpp_statis'] = $tunjangan*($proporsi['statis']/100);
					$result['tpp_dinamis'] = $tunjangan*($proporsi['dinamis']/100);					
				}
				else
				{
					$result['total_tpp'] = 0;
					$result['tpp_statis'] = 0;
					$result['tpp_dinamis'] = 0;	
				}
			break;
			
			case 3:
			
			break;
			
			case 4:
				$s1 = "select a.tunjangan 
						from referensi_tunjangan_jfu a 
					where a.status = '$kstatus'
						";
				$q1 = $this->db->query($s1);
				if($q1->num_rows() > 0)
				{
					$tunjangan = $q1->row()->tunjangan;
					$result['total_tpp'] = $tunjangan;
					$result['tpp_statis'] = $tunjangan*($proporsi['statis']/100);
					$result['tpp_dinamis'] = $tunjangan*($proporsi['dinamis']/100);					
				}
				else
				{
					$result['total_tpp'] = 0;
					$result['tpp_statis'] = 0;
					$result['tpp_dinamis'] = 0;	
				}
			break;
		}
		return $result;
	}

	public function harian_tkk()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		
		$data = array(
				'title' => 'Data Absensi Harian TKK',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'sssubunit' => $this->session->userdata('sssubunit_harian_tkk'),
				'nama' => $this->session->userdata('nama_harian_tkk'),
				'nik' => $this->session->userdata('nik_harian_tkk'),
				'tanggal' => $this->session->userdata('tanggal_harian_tkk'),
		);
		$this->load->view('harian_tkk',$data);
		input_log($this->user_id,'Lihat Data absens harian tkk');
	}
	
	public function get_harian_tkk()
	{
		//print_r($_REQUEST);
		$order = $_REQUEST['order'][0];
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/log/harian');
		//$aColumns = array( 'nip', 'nip','nama', 'nip','nip');
		$sssubunit = $this->input->post('sssubunit');
		$this->session->set_userdata('sssubunit_harian_tkk',$sssubunit);
		$nama = $this->input->post('nama');
		$this->session->set_userdata('nama_harian_tkk',$nama);
		$nik = $this->input->post('nik');
		$this->session->set_userdata('nik_harian_tkk',$nik);
		$tanggal = $this->input->post('tanggal');
		$this->session->set_userdata('tanggal_harian_tkk',$tanggal);
		$tgl = date('j',strtotime($tanggal));
		$bln = date('n',strtotime($tanggal));
		$thn = date('Y',strtotime($tanggal));
		$hak_edit = 1;
		//cek utk pencegatan
		$tgl_max_lapor = $this->db->query('select tanggal from ref_waktu_lapor')->row()->tanggal;
		$tgl_sekarang = date('j');
		if($tgl_sekarang < $tgl_max_lapor)
		{
			$bln_compare = date('n')-1;
			if($bln >= $bln_compare)
			{
				$hak_edit = 1;
			}
			elseif($bln < $bln_compare)
			{
				$hak_edit = 0;
			}
		}
		else
		{
			$bln_compare = date('n');
			if($bln == $bln_compare)
			{
				$hak_edit = 1;
			}
			elseif($bln < $bln_compare)
			{
				$hak_edit = 0;
			}
		}
		
		$sSearch = '';
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		if(isset($select) && $select != '')
		{
			if(!empty($s_biro)) 
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
			}
			else
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			}
		}
		
		if(isset($nama) && $nama != '')
		{
			$sSearch .= " and (nama like '%".$nama."%') ";
		}
		if(isset($nik) && $nik != '')
		{
			$sSearch .= " and (a.nik = '".$nik."') ";
		}
		
		if(isset($tgl) && $tgl != '')
		{
			$sSearch .= " and (b.tanggal = '".$tgl."') ";
		}
		if(isset($bln) && $bln != '')
		{
			$sSearch .= " and (b.bulan = '".$bln."') ";
		}
		if(isset($thn) && $thn != '')
		{
			$sSearch .= " and (b.tahun = '".$thn."') ";
		}
	
		//limit
		if ( isset( $_REQUEST['start'] ) && $_REQUEST['length'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_REQUEST['start'] ).", ".
				intval( $_REQUEST['length'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "`".$aColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]."` ".
						($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		//$sOrder = " order by ".$aColumns[$order['column']]." ".$order['dir']." ";
		$s0 = "select count(a.nik) as jml from d_tkk a
		LEFT JOIN d_absen_harian_tkk b on(a.nik = b.nik)
				where 1 = 1
				and a.non_aktif = 0
				";
		$s0 .= $sSearch;
		
		$sheet_total = $this->db->query($s0)->row();
		$sheet_total = $sheet_total->jml;
	
		$s1 = "SELECT DISTINCT a.*,
								concat(kuntp,kunkom,kununit,rincunit1,rincunit2,rincunit3) as kunker,
								b.*,									
								if(b.jenis_tidak_masuk != 0,(select jenis_ketidakhadiran from referensi_persentase_ketidakhadiran where id = b.jenis_tidak_masuk),'-') as ketidakhadiran
			FROM d_tkk a
			LEFT JOIN d_absen_harian_tkk b on(a.nik = b.nik)
			WHERE 1=1
			and a.non_aktif = 0";
		$s1 .= $sSearch;
		$s1 .= " order by a.nik desc ";
		
		$s1 .= $sLimit;
	//	echo $s1;
		$sheet1 = $this->db->query($s1);
		$result = array();
		$i=0;
		$no=1;
		foreach($sheet1->result() as $sheet)
		{
			
			$result[$i]['0'] = $no;
			$result[$i]['1'] = $sheet->nik.' </br> '.$sheet->nama;
			$result[$i]['2'] = $this->get_skpd($sheet->kunker);
			$result[$i]['3'] = (((isset($sheet->libur) && $sheet->libur == 1)||(isset($sheet->jenis_tidak_masuk) && $sheet->jenis_tidak_masuk != 0))?'-':((isset($sheet->jadwal_masuk) && $sheet->jadwal_masuk != '' && $sheet->jadwal_masuk != '0000-00-00 00:00:00' && $sheet->jadwal_masuk != NULL)?date('d-m-Y H:i',strtotime($sheet->jadwal_masuk)):'shift'));
			$result[$i]['4'] = ((isset($sheet->realisasi_masuk) && $sheet->realisasi_masuk != '' && $sheet->realisasi_masuk != NULL)?date('d-m-Y H:i',strtotime($sheet->realisasi_masuk)):'');
			$result[$i]['5'] = 	$sheet->keterlambatan_masuk;	
			$result[$i]['6'] = (((isset($sheet->libur) && $sheet->libur == 1)||(isset($sheet->jenis_tidak_masuk) && $sheet->jenis_tidak_masuk != 0))?'-':((isset($sheet->jadwal_keluar) && $sheet->jadwal_keluar != '' && $sheet->jadwal_keluar != '0000-00-00 00:00:00' && $sheet->jadwal_keluar != NULL)?date('d-m-Y H:i',strtotime($sheet->jadwal_keluar)):'shift'));
			$result[$i]['7'] = ((isset($sheet->realisasi_keluar) && $sheet->realisasi_keluar != '' && $sheet->realisasi_keluar != NULL)?date('d-m-Y H:i',strtotime($sheet->realisasi_keluar)):'');
			$result[$i]['8'] = 	$sheet->kecepatan_keluar;	
			$result[$i]['9'] = 	$sheet->ketidakhadiran;	
			$result[$i]['10'] = 	$sheet->persentase_potongan;	
			$result[$i]['11'] = 	$sheet->keterangan;	
			if($hak_edit == 1)
			{
			$result[$i]['12'] = '<a href="/log/edit_harian_tkk/'.$sheet->id.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Edit">
									<i class="glyphicon glyphicon-edit"></i>
								</a>';
			}
			else
			{
				$result[$i]['12'] = '';
			}
			$result[$i]['13'] = 	$sheet->edited;	
			$no++;
			$i++;
		}
		
		$data = $result;
		$results = array(
			"iTotalRecords" => ($sheet_total),
			"iTotalDisplayRecords" => ($sheet_total),
			"aaData"=>$data);
			
		echo json_encode($results);
	}
	
	public function edit_harian_tkk()
	{
		$id = $this->uri->segment(3);
		$s = "select a.*,
					if(b.kunkom = 10 || b.kunkom = 11,concat(b.kuntp,b.kunkom,b.kununit,'000000'),concat(b.kuntp,b.kunkom,'00000000')) as biro,
					b.nama as nama_lengkap,
					CASE KUNKOM
								   WHEN 10 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' 	)
								   WHEN 11 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' 	) 
								   ELSE (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,4) = concat(kuntp,kunkom) and mid(kunker,5,8) = '00000000' 	)
					END as skpd
				from d_absen_harian_tkk a 
				left join d_tkk b on(a.nik = b.nik)
			where a.id = '$id'
			and b.non_aktif = 0
				";
		//echo $s;
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			$biro = $r->biro;
			if($this->s_biro != '100000000000' && $this->s_biro != '' && $this->s_biro != $biro)
			{
				redirect('/');
			}
			
			$this->load->library('form_validation');
			$this->form_validation->set_rules('jadwal_masuk', 'Jadwal Masuk', 'trim|required');
			$this->form_validation->set_rules('realisasi_masuk', 'Realisasi Masuk', 'trim|required');
			$this->form_validation->set_rules('jadwal_keluar', 'Jadwal Masuk', 'trim|required');
			$this->form_validation->set_rules('realisasi_keluar', 'Realisasi Keluar', 'trim|required');
			if ($this->form_validation->run() === TRUE)
			{
				$posts = $this->input->post();
			//	print_r($posts);
			//	echo '</br>';
				$jadwal_masuk = $this->db->escape_str($posts['jadwal_masuk']);		
				$realisasi_masuk = $this->db->escape_str($posts['realisasi_masuk']);
				$realisasi_masuk = date('Y-m-d H:i',strtotime($realisasi_masuk));
				$keterlambatan_masuk = (strtotime($realisasi_masuk) - strtotime($jadwal_masuk))/60;
				if($keterlambatan_masuk > 0)
				{
					$keterlambatan_masuk = $keterlambatan_masuk;
					$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
				}
				else
				{
					$keterlambatan_masuk = 0;
					$potongan_terlambat_masuk=0;
				}
				
				$jadwal_keluar = $this->db->escape_str($posts['jadwal_keluar']);					
				$realisasi_keluar = $this->db->escape_str($posts['realisasi_keluar']);
				$realisasi_keluar = date('Y-m-d H:i',strtotime($realisasi_keluar));
				$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($realisasi_keluar))/60;
				if($kecepatan_keluar > 0)
				{
					$kecepatan_keluar = $kecepatan_keluar;
					$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
				}
				else
				{
					$kecepatan_keluar = 0;
					$potongan_pulang_cepat=0;
				}
				$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat;
											
				$update_date = date('Y-m-d H:i:s');
				$update_by = $this->userid;
				
				$jadwal_masuk = date('Y-m-d H:i:s',strtotime($jadwal_masuk));	
				$jadwal_keluar = date('Y-m-d H:i:s',strtotime($jadwal_keluar));	
				
				$keterangan = $this->db->escape_str($posts['keterangan']);	
				$sql = 	"update d_absen_harian_tkk 
							SET jadwal_masuk = '$jadwal_masuk',
								realisasi_masuk = '$realisasi_masuk',
								keterlambatan_masuk = '$keterlambatan_masuk',
								potongan_terlambat_masuk = '$potongan_terlambat_masuk',
								jadwal_keluar = '$jadwal_keluar',
								realisasi_keluar = '$realisasi_keluar',
								kecepatan_keluar = '$kecepatan_keluar',
								potongan_pulang_cepat = '$potongan_pulang_cepat',
								persentase_potongan = '$persentase_potongan',
								keterangan = '$keterangan',
								update_date = '$update_date',
								update_by = '$update_by',
								edited = '1'
							where id = '$id'
								";
			//	echo $sql.'</br>';
				$this->db->query($sql);	
				$this->update_data_rekap_keuangan_tkk($r->nik,$jadwal_masuk,$persentase_potongan);	
				$this->input_log_edit_absensi_tkk($id,$this->user_id,$r,$update_date);				
				input_log($this->user_id,'edit data absensi harian tkk dengan id:'.$id);
				redirect('/log/harian_tkk');
			}
			else
			{
				$this->data['jadwal_masuk'] = array(
						'name'  => 'jadwal_masuk',
						'id'    => 'jadwal_masuk',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('jadwal_masuk',(isset($r->jadwal_masuk)?date('d-m-Y H:i',strtotime($r->jadwal_masuk)):'')),
						'class'  => 'form-control datepicker',
					);
				$this->data['realisasi_masuk'] = array(
						'name'  => 'realisasi_masuk',
						'id'    => 'realisasi_masuk',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('realisasi_masuk',(isset($r->realisasi_masuk)?date('d-m-Y H:i',strtotime($r->realisasi_masuk)):'')),
						'class'  => 'form-control datepicker',
					);	
				$this->data['jadwal_keluar'] = array(
						'name'  => 'jadwal_keluar',
						'id'    => 'jadwal_keluar',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('jadwal_keluar',(isset($r->jadwal_keluar)?date('d-m-Y H:i',strtotime($r->jadwal_keluar)):'')),
						'class'  => 'form-control datepicker',
					);	
				$this->data['realisasi_keluar'] = array(
						'name'  => 'realisasi_keluar',
						'id'    => 'realisasi_keluar',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('realisasi_keluar',(isset($r->realisasi_keluar)?date('d-m-Y H:i',strtotime($r->realisasi_keluar)):'')),
						'class'  => 'form-control datepicker',
					);	
				$this->data['keterangan'] = array(
						'name'  => 'keterangan',
						'id'    => 'keterangan',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('keterangan',(($r->keterangan))),
						'class'  => 'form-control',
				);	
				$this->data['title'] = 'Data Kehadiran Harian Pegawai';
				$this->data['subtitle'] = 'Form Pemutakhiran Data Kehadiran Harian';
				$this->data['error'] = '';
				$this->data['r'] = $r;
				$this->load->view('form_edit_harian_tkk',$this->data);
			}
		
		}
		else
		{
			redirect('/');
		}
		
	}
	
	function input_log_edit_absensi_tkk($id,$userid,$r,$update_date)
	{
		$data = array(
			'userid' => $userid,
			'ip' => $_SERVER['REMOTE_ADDR'],
			'nik' => $r->nik,
			'id_data' => $id,
			'tgl_data' => date('Y-m-d',strtotime($r->tanggal.'-'.$r->bulan.'-'.$r->tahun)),
			'data_lama' => serialize($r),
			'update_date' => $update_date,
		);
		$this->db->insert('log_edit_absensi_tkk',$data);
		//echo $this->db->last_query();
	}
	
	public function ekspor_absen_harian_tkk_pdf()
	{
		set_time_limit(0);
		$this->load->library('pdf');
		$pdf = $this->pdf->load('format => A-2 L');
	//	print_r($_REQUEST);
		$sssubunit = $this->input->post('sssubunit_pdf');
		$nama = $this->input->post('nama_pdf');
		$nik = $this->input->post('nik_pdf');
		$tanggal = $this->input->post('tanggal_pdf');
		$tgl = date('j',strtotime($tanggal));
		$bln = date('n',strtotime($tanggal));
		$thn = date('Y',strtotime($tanggal));
		$sSearch = '';
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		if(isset($select) && $select != '')
		{
			if(!empty($s_biro)) 
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
			}
			else
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			}
		}
		
		if(isset($nama) && $nama != '')
		{
			$sSearch .= " and (nama like '%".$nama."%') ";
		}
		if(isset($nik) && $nik != '')
		{
			$sSearch .= " and (a.nik = '".$nik."') ";
		}
		
		if(isset($tgl) && $tgl != '')
		{
			$sSearch .= " and (b.tanggal = '".$tgl."') ";
		}
		if(isset($bln) && $bln != '')
		{
			$sSearch .= " and (b.bulan = '".$bln."') ";
		}
		if(isset($thn) && $thn != '')
		{
			$sSearch .= " and (b.tahun = '".$thn."') ";
		}
	
		$s1 = "SELECT DISTINCT a.*,
								b.*,
									CASE KUNKOM
								   WHEN 10 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' 	)
								   WHEN 11 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' 	) 
								   ELSE (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,4) = concat(kuntp,kunkom) and mid(kunker,5,8) = '00000000' 	)
								END as skpd,
								if(b.jenis_tidak_masuk != 0,(select jenis_ketidakhadiran from referensi_persentase_ketidakhadiran where id = b.jenis_tidak_masuk),'-') as ketidakhadiran
			FROM d_tkk a
			LEFT JOIN d_absen_harian_tkk b on(a.nik = b.nik)
			WHERE 1=1
			and a.non_aktif = 0";
		$s1 .= $sSearch;
		$s1 .= " order by a.nik desc ";
	//	echo $s1;
		$q= $this->db->query($s1);
		$data['q'] = $q;
		$data['tanggal'] = $tanggal;
		$data['unit_kerja'] = $this->get_unit_kerja($sssubunit);
		$html = $this->load->view('cetak_absen_harian_tkk',$data,true);
		input_log($this->user_id,'ekspor absensi harian tkk ke dalam bentuk pdf');
		$pdf->WriteHTML($html);
		$pdf->Output();
			
	}
	
	function update_data_rekap_keuangan_tkk($nik,$tanggal,$pemotongan_per_hari)
	{
		$this->db->insert('coba_coba',array('keterangan'=>'update data keuangan','waktu'=>date('Y-m-d H:i:s')));
		$tanggal = date('Y-m-d',strtotime($tanggal));
		$update_date = date('Y-m-d H:i:s');
		$s = "select id from d_rekap_keuangan_harian_tkk where tanggal = '$tanggal' and nik = '$nik' ";
		$this->db->insert('coba_coba',array('keterangan'=>$s,'waktu'=>date('Y-m-d H:i:s')));
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			$id = $q->row()->id;
			$tpp = $this->get_tpp_tkk($nik);
			$si = "update d_rekap_keuangan_harian_tkk 
					set tanggal = '$tanggal',
						nik = '$nik',
						total_tpp = '$tpp[total_tpp]',
						tpp_statis = '$tpp[tpp_statis]',
						tpp_dinamis = '$tpp[tpp_dinamis]',
						pemotongan_per_hari = '$pemotongan_per_hari',
						update_date = '$update_date',	
						update_by = 'system'
					where id = '$id'
			";
			$this->db->query($si);
		}
		else
		{
			$tpp = $this->get_tpp_tkk($nik);
			$this->db->insert('coba_coba',array('keterangan'=>$tpp['total_tpp'],'waktu'=>date('Y-m-d H:i:s')));
			$si = "insert into d_rekap_keuangan_harian_tkk 
					set tanggal = '$tanggal',
						nik = '$nik',
						total_tpp = '$tpp[total_tpp]',
						tpp_statis = '$tpp[tpp_statis]',
						tpp_dinamis = '$tpp[tpp_dinamis]',
						pemotongan_per_hari = '$pemotongan_per_hari',
						update_date = '$update_date',	
						update_by = 'system'					
			";
			$this->db->query($si);
		}
		$this->db->insert('coba_coba',array('keterangan'=>$si,'waktu'=>date('Y-m-d H:i:s')));
	}
	
	function get_tpp_tkk($nik)
	{
		$sp = "select * from referensi_proporsi_tunjangan";
		$qp = $this->db->query($sp);
		$proporsi = array();
		foreach($qp->result() as $rp)
		{
			$proporsi[$rp->status] = $rp->proporsi;
		}
		$s1 = "select a.tunjangan 
						from referensi_tunjangan_jfu a 
					where a.status = '4'
						";
		$q1 = $this->db->query($s1);
		if($q1->num_rows() > 0)
		{
			$tunjangan = $q1->row()->tunjangan;
			$result['total_tpp'] = $tunjangan;
			$result['tpp_statis'] = $tunjangan*($proporsi['statis']/100);
			$result['tpp_dinamis'] = $tunjangan*($proporsi['dinamis']/100);					
		}
		else
		{
			$result['total_tpp'] = 0;
			$result['tpp_statis'] = 0;
			$result['tpp_dinamis'] = 0;	
		}
		
		return $result;
	}
	
	public function bulanan()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		$bulan = $this->session->userdata('bulan_bulanan');
		$data = array(
				'title' => 'Data Absensi Bulanan',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'sssubunit' => $this->session->userdata('sssubunit_bulanan'),
				'jnsjab' => $this->session->userdata('jnsjab_bulanan'),
				'keselon' => $this->session->userdata('keselon_bulanan'),
				'nama' => $this->session->userdata('nama_bulanan'),
				'reqnip' => $this->session->userdata('reqnip_bulanan'),
				'bulan' => ((isset($bulan) && $bulan != '')?$bulan:date('n')),
				'tahun' => $this->session->userdata('tahun_bulanan'),
		);
		$this->load->view('bulanan',$data);
		input_log($this->user_id,'Lihat Data bulanan PNS');
	}
	
	public function get_bulanan()
	{
		//print_r($_REQUEST);
		//$order = $_REQUEST['order'][0];
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/log/bulanan');
		//$aColumns = array( 'nip', 'nip','nama', 'nip','nip');
		$nama = $this->input->post('nama');
		$this->session->set_userdata('nama_bulanan',$nama);
		$reqnip = $this->input->post('reqnip');
		$this->session->set_userdata('reqnip_bulanan',$reqnip);
		$bulan = $this->input->post('bulan');
		$this->session->set_userdata('bulan_bulanan',$bulan);
		$tahun = $this->input->post('tahun');
		$this->session->set_userdata('tahun_bulanan',$tahun);
		
		$sSearch = '';
	
		if(isset($nama) && $nama != '')
		{
			$sSearch .= " and (nama like '%".$nama."%') ";
		}
		if(isset($reqnip) && $reqnip != '')
		{
			$sSearch .= " and (a.nip = '".$reqnip."') ";
		}
		
		//limit
		if ( isset( $_REQUEST['start'] ) && $_REQUEST['length'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_REQUEST['start'] ).", ".
				intval( $_REQUEST['length'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "`".$aColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]."` ".
						($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		//$sOrder = " order by ".$aColumns[$order['column']]." ".$order['dir']." ";
		$s0 = "select count(a.nip) as jml 
				from d_pegawai a
				where 1 = 1
				and a.non_aktif = 0
				";
		$s0 .= $sSearch;
		//echo $s0;
		$sheet_total = $this->db->query($s0)->row();
		$sheet_total = $sheet_total->jml;
	
		$s1 = "SELECT DISTINCT a.nip,a.nama,c.nama as nama_jabatan
				FROM d_pegawai a
				left join ref_jabatan c on(a.id_jabatan = c.id)
				WHERE 1=1
				and a.non_aktif = 0";
		$s1 .= $sSearch;
		$s1 .= " order by nip asc ";
		
		$s1 .= $sLimit;
	//	echo $s1;
		$sheet1 = $this->db->query($s1);
		$result = array();
		$i=0;
		$no=1;
		foreach($sheet1->result() as $sheet)
		{
			
			$result[$i]['0'] = $no;
			$result[$i]['1'] = $sheet->nip;
			$result[$i]['2'] = $sheet->nama;
			$result[$i]['3'] = $sheet->nama_jabatan;
			$result[$i]['4'] = 	'<a target="_blank" href="/log/view_bulanan/'.$sheet->nip.'/'.$bulan.'/'.$tahun.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="View">
									<i class="glyphicon glyphicon-search"></i>
									</a>';	
			$result[$i]['5'] = 	'<a target="_blank" href="/log/pdf_bulanan/'.$sheet->nip.'/'.$bulan.'/'.$tahun.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="View">
									<i class="glyphicon glyphicon-print"></i>
									</a>';	
			$result[$i]['6'] = 	'<a href="/log/refresh_bulanan/'.str_replace(' ','',$sheet->nip).'/'.$bulan.'/'.$tahun.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Refresh">
									<i class="glyphicon glyphicon-refresh"></i>
									</a>';	
			$no++;
			$i++;
		}
		
		$data = $result;
		$results = array(
			"iTotalRecords" => ($sheet_total),
			"iTotalDisplayRecords" => ($sheet_total),
			"aaData"=>$data);
			
		echo json_encode($results);
	}
	
	public function bulanan_tkk()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		$bulan = $this->session->userdata('bulan_bulanan_tkk');
		$data = array(
				'title' => 'Data Absensi Bulanan TKK',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'sssubunit' => $this->session->userdata('sssubunit_bulanan_tkk'),
				'nama' => $this->session->userdata('nama_bulanan_tkk'),
				'nik' => $this->session->userdata('nik_bulanan_tkk'),
				'bulan' => ((isset($bulan) && $bulan != '')?$bulan:date('n')),
				'tahun' => $this->session->userdata('tahun_bulanan_tkk'),
		);
		$this->load->view('bulanan_tkk',$data);
		input_log($this->user_id,'Lihat Data bulanan tkk');
	}
	
	public function get_bulanan_tkk()
	{
		//print_r($_REQUEST);
		$order = $_REQUEST['order'][0];
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/log/bulanan');
		//$aColumns = array( 'nip', 'nip','nama', 'nip','nip');
		$sssubunit = $this->input->post('sssubunit');
		$this->session->set_userdata('sssubunit_bulanan_tkk',$sssubunit);
		$nama = $this->input->post('nama');
		$this->session->set_userdata('nama_bulanan_tkk',$nama);
		$nik = $this->input->post('nik');
		$this->session->set_userdata('nik_bulanan_tkk',$nik);
		$bulan = $this->input->post('bulan');
		$this->session->set_userdata('bulan_bulanan_tkk',$bulan);
		$tahun = $this->input->post('tahun');
		$this->session->set_userdata('tahun_bulanan_tkk',$tahun);
		
		$sSearch = '';
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		if(isset($select) && $select != '')
		{
			if(!empty($s_biro)) 
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
			}
			else
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			}
		}
		
		if(isset($nama) && $nama != '')
		{
			$sSearch .= " and (nama like '%".$nama."%') ";
		}
		if(isset($nik) && $nik != '')
		{
			$sSearch .= " and (a.nik = '".$nik."') ";
		}
			
		//limit
		if ( isset( $_REQUEST['start'] ) && $_REQUEST['length'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_REQUEST['start'] ).", ".
				intval( $_REQUEST['length'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "`".$aColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]."` ".
						($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		//$sOrder = " order by ".$aColumns[$order['column']]." ".$order['dir']." ";
		$s0 = "select count(a.nik) as jml 
				from d_tkk a
				where 1 = 1
				and a.non_aktif = 0
				";
		$s0 .= $sSearch;
		
		$sheet_total = $this->db->query($s0)->row();
		$sheet_total = $sheet_total->jml;
	
		$s1 = "SELECT DISTINCT a.nik,a.nama,
								concat(kuntp,kunkom,kununit,rincunit1,rincunit2,rincunit3) as kunker
				FROM d_tkk a
				WHERE 1=1
				and a.non_aktif = 0";
		$s1 .= $sSearch;
		$s1 .= " order by nik desc ";
		
		$s1 .= $sLimit;
	//	echo $s1;
		$sheet1 = $this->db->query($s1);
		$result = array();
		$i=0;
		$no=1;
		foreach($sheet1->result() as $sheet)
		{
			
			$result[$i]['0'] = $no;
			$result[$i]['1'] = $sheet->nik;
			$result[$i]['2'] = $sheet->nama;
			$result[$i]['3'] = $this->get_skpd($sheet->kunker);
			$result[$i]['4'] = 	'<a target="_blank" href="/log/view_bulanan_tkk/'.str_replace(' ','',$sheet->nik).'/'.$bulan.'/'.$tahun.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="View">
									<i class="glyphicon glyphicon-search"></i>
									</a>';	
			$result[$i]['5'] = 	'<a target="_blank" href="/log/pdf_bulanan_tkk/'.str_replace(' ','',$sheet->nik).'/'.$bulan.'/'.$tahun.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="View">
									<i class="glyphicon glyphicon-print"></i>
									</a>';	
			$result[$i]['6'] = 	'<a href="/log/refresh_bulanan_tkk/'.str_replace(' ','',$sheet->nik).'/'.$bulan.'/'.$tahun.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="View">
									<i class="glyphicon glyphicon-refresh"></i>
									</a>';	
			$no++;
			$i++;
		}
		
		$data = $result;
		$results = array(
			"iTotalRecords" => ($sheet_total),
			"iTotalDisplayRecords" => ($sheet_total),
			"aaData"=>$data);
			
		echo json_encode($results);
	}
	
	public function cari_nip()
	{
		$where = "";
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		$q = $this->db->escape_str($_REQUEST['q']);
		$s = "select a.NIP,a.NAMA,
					CONCAT(a.nip,
							'-',
							a.nama,
							'-',
							a.jabatan
							)  AS NIPNAMA
				FROM d_pegawai a
				where 1=1
				";
		if(!empty($s_biro)) 
		{
			$select = $s_biro;
			if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$where = " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$where = " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$where = " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$where = " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$where = " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
		}
		$s .= $where;
		$s .=" and (a.nama like '%".$q."%' OR a.nip like '%".$q."%' )";
		//echo $s;
		$q = $this->db->query($s);
		$res = array();
		foreach($q->result_array() as $r)
		{
			$res[] = $r;
		}
		echo json_encode($res);
	}
	
	public function view_log_absensi()
	{
		$nip = $_REQUEST['nip'];
		$bulan = $_REQUEST['bulan'];
		$tahun = $_REQUEST['tahun'];
		$jml_hari = cal_days_in_month(CAL_GREGORIAN,$bulan,$tahun);
		$res = '';
		$res .='<div class="row">';
		$res .='<div class="col-xs-12">
				<div class="panel panel-success">
                    <div class="panel-heading">
                        Daftar Absensi Pegawai Dengan NIP: '.$nip.'
                    </div>
					<div class="panel-body">';
		$res .='<table class="table">';
		$res .='<tr><td>No</td><td>Tanggal</td><td>Masuk</td><td>Pulang</td></tr>';
		for($i=1;$i<=$jml_hari;$i++)
		{
			$tgl = date('d-M-Y',strtotime($tahun.'-'.$bulan.'-'.$i));
			$tanggal = date('d-m-Y',strtotime($tahun.'-'.$bulan.'-'.$i));
			$res .='<tr><td>'.$i.'</td><td>'.$tgl.'</td><td>'.get_waktu_masuk($nip,$tanggal).'</td><td>'.get_waktu_pulang($nip,$tanggal).'</td></tr>';
		}
		$res .='</table>';
		$res .='</div>
				</div>
				</div>
				</div>';
		echo $res;
	}
	
	public function view_bulanan()
	{
		$added_date = date('Y-m-d H:i:s');
		$added_by = $this->user_id;
		$nip = $this->uri->segment(3);
		$bulan = $this->uri->segment(4);
		$tahun = $this->uri->segment(5);
		$jumlah_hari = date('t',strtotime('1-'.$bulan.'-'.$tahun));
		
		for($i=1;$i<=$jumlah_hari;$i++)
		{
			$tanggal = $i;
			$tanggal_buat = $tahun.'-'.$bulan.'-'.$i;
			$sc = "select * from d_absen_harian where tanggal = '$tanggal' and bulan = '$bulan' and tahun = '$tahun' and nip = '$nip' ";
				//	echo $sc.'</br>';
				$qcabsen = $this->db->query($sc);
				if($qcabsen->num_rows() == 0)
				{
						$st = "select a.*,
										b.*
								from d_pegawai_tipe_jadwal a
								left join ref_tipe_jadwal b on(a.tipe_jadwal = b.id)
								where nip = '$nip' 
								
								";
						$rst = $this->db->query($st)->row_array();
						if($rst['shift'] == 0)
						{			
							$cek_libur = $this->cek_libur($tanggal_buat,$rst['sabtu_masuk']);
							if($cek_libur == 1)
							{
								$libur = 1;
								$persentase_potongan = 0;
								$jadwal_jam_masuk = $rst['jadwal_jam_masuk'];
								$jadwal_menit_masuk = $rst['jadwal_menit_masuk'];
								$jadwal_masuk = date('Y-m-d H:i',strtotime($tanggal.'-'.$bulan.'-'.$tahun.' '.$jadwal_jam_masuk.':'.$jadwal_menit_masuk));
								$jadwal_jam_keluar = $rst['jadwal_jam_keluar'];
								$jadwal_menit_keluar = $rst['jadwal_menit_keluar'];
								$jadwal_keluar = date('Y-m-d H:i',strtotime($tanggal.'-'.$bulan.'-'.$tahun.' '.$jadwal_jam_keluar.':'.$jadwal_menit_keluar));
								$jadwal_masuk = NULL;
								$jadwal_keluar = NULL;
								$si = "insert into d_absen_harian
										set nip = '$nip',
											tanggal = '$tanggal',
											bulan = '$bulan',
											tahun = '$tahun',
											libur = '$libur',
											added_date = '$added_date',
											added_by = '$added_by'
								";
							//	echo $si.'</br>';
								$qi = $this->db->query($si);
							}
							else
							{
								$libur = 0;
								
								//cek jika jadwal khusus
								$hari_ini = date('w',strtotime($tanggal_buat));
								$sc = "select * from ref_jadwal_khusus where hari = '$hari_ini' and aktif = 1 and tipe_jadwal = '$rst[tipe_jadwal]' ";
								$qc = $this->db->query($sc);
								if($qc->num_rows() > 0)
								{
									$rjk = $qc->row_array();
									$jadwal_jam_masuk = $rjk['jadwal_jam_masuk'];
									$jadwal_menit_masuk = $rjk['jadwal_menit_masuk'];
									$jadwal_masuk = date('Y-m-d H:i',strtotime($tanggal.'-'.$bulan.'-'.$tahun.' '.$jadwal_jam_masuk.':'.$jadwal_menit_masuk));
									$jadwal_jam_keluar = $rjk['jadwal_jam_keluar'];
									$jadwal_menit_keluar = $rjk['jadwal_menit_keluar'];
									$jadwal_keluar = date('Y-m-d H:i',strtotime($tanggal.'-'.$bulan.'-'.$tahun.' '.$jadwal_jam_keluar.':'.$jadwal_menit_keluar));	
								}
								else
								{
									$jadwal_jam_masuk = $rst['jadwal_jam_masuk'];
									$jadwal_menit_masuk = $rst['jadwal_menit_masuk'];
									$jadwal_masuk = date('Y-m-d H:i',strtotime($tanggal.'-'.$bulan.'-'.$tahun.' '.$jadwal_jam_masuk.':'.$jadwal_menit_masuk));
									$jadwal_jam_keluar = $rst['jadwal_jam_keluar'];
									$jadwal_menit_keluar = $rst['jadwal_menit_keluar'];
									$jadwal_keluar = date('Y-m-d H:i',strtotime($tanggal.'-'.$bulan.'-'.$tahun.' '.$jadwal_jam_keluar.':'.$jadwal_menit_keluar));	
								}
								
								$si = "insert into d_absen_harian
										set nip = '$nip',
											tanggal = '$tanggal',
											bulan = '$bulan',
											tahun = '$tahun',
											jadwal_masuk = '$jadwal_masuk',
											jadwal_keluar = '$jadwal_keluar',
											libur = '$libur',
											added_date = '$added_date',
											added_by = '$added_by'
								";
							//	echo $si.'</br>';
								$qi = $this->db->query($si);
							}
							
						}
						else
						{
							$tgl = date('Y-m-d',strtotime($tanggal_buat));
							$sc = "select a.* 
								from d_jadwal_lembur a
								where nip = '$nip'
								and tanggal = '$tgl'
								";
							$qc = $this->db->query($sc);
							if($qc->num_rows() > 0)
							{
								$rc = $qc->row();
								if($rc->is_libur == 1)
								{
									$libur = $rc->libur;
									$persentase_potongan = 0;
									$jadwal_masuk = NULL;
									$jadwal_keluar = NULL;
									$si = "insert into d_absen_harian
										set nip = '$nip',
											tanggal = '$tanggal',
											bulan = '$bulan',
											tahun = '$tahun',
											libur = '$libur',
											added_date = '$added_date',
											added_by = 'system'
									";
								//	echo $si.'</br>';
									$qi = $this->db->query($si);
								}
								else
								{
									$jadwal_masuk = $rc->jadwal_masuk;
									$jadwal_keluar = $rc->jadwal_keluar;
									$libur = 0;
									$si = "insert into d_absen_harian
										set nip = '$nip',
											tanggal = '$tanggal',
											bulan = '$bulan',
											tahun = '$tahun',
											jadwal_masuk = '$jadwal_masuk',
											jadwal_keluar = '$jadwal_keluar',
											libur = '$libur',									
											added_date = '$added_date',
											added_by = 'system'
									";
								//	echo $si.'</br>';
									$qi = $this->db->query($si);
								}						
								
							}
							else
							{
								$libur = 1;
								$persentase_potongan = 0;
								$jadwal_masuk = NULL;
								$jadwal_keluar = NULL;
								$si = "insert into d_absen_harian
										set nip = '$nip',
											tanggal = '$tanggal',
											bulan = '$bulan',
											tahun = '$tahun',
											libur = '$libur',
											added_date = '$added_date',
											added_by = 'system'
								";
							//	echo $si.'</br>';
								$qi = $this->db->query($si);
							}
							
						}
						
				}
				
				
			$sa = "select * from absensi_log where nip = '$nip' and day(tanggal) = '$tanggal' and month(tanggal) = '$bulan' and year(tanggal) = '$tahun' order by tanggal asc";
			$qa = $this->db->query($sa);
			if($qa->num_rows() > 0)
			{
				foreach($qa->result() as $ra)
				{
					$waktu = $ra->tanggal;
					$scabsen = "select * from d_absen_harian where tanggal = '$tanggal' and bulan = '$bulan' and tahun = '$tahun' and nip = '$nip' ";
					$rabsen = $this->db->query($scabsen)->row();
					$jadwal_masuk = $rabsen->jadwal_masuk;
					$realisasi_masuk = $rabsen->realisasi_masuk;
					$jadwal_keluar = $rabsen->jadwal_keluar;
					$realisasi_keluar = $rabsen->realisasi_keluar;
					
					if($realisasi_masuk == NULL OR $realisasi_masuk == '0000-00-00')
					{
						if((strtotime($waktu)) > (strtotime($jadwal_masuk)))
						{
							$keterlambatan_masuk = (((strtotime($waktu)) - (strtotime($jadwal_masuk)))/60);
						}
						else
						{
							$keterlambatan_masuk=0;
						}
						$array_update = array(
							'realisasi_masuk' => $waktu,
							'keterlambatan_masuk' => $keterlambatan_masuk,
						);
						$this->db->where('nip', $nip);
						$this->db->where('tanggal', $tanggal);
						$this->db->where('bulan', $bulan);
						$this->db->where('tahun', $tahun);
						$this->db->update('d_absen_harian',$array_update);
					}
					else
					{
						if((strtotime($waktu)) < (strtotime($jadwal_keluar)))
						{
							$kecepatan_keluar = (((strtotime($jadwal_keluar)) - (strtotime($waktu)))/60);
						}
						else
						{
							$kecepatan_keluar=0;
						}
						$array_update = array(
							'realisasi_keluar' => $waktu,
							'kecepatan_keluar' => $kecepatan_keluar,
						);
						$this->db->where('nip', $nip);
						$this->db->where('tanggal', $tanggal);
						$this->db->where('bulan', $bulan);
						$this->db->where('tahun', $tahun);
						$this->db->update('d_absen_harian',$array_update);
					}
				}
			}
			
		}
		$sc = "select a.*
				from d_absen_harian a
				where nip = '$nip' 
				and bulan = '$bulan' 
				and tahun = '$tahun' 
				order by a.tahun asc, bulan asc, tanggal asc
				";
		//echo $sc;
		$qc = $this->db->query($sc);
		if($qc->num_rows() > 0)
		{
			$sql = "SELECT DISTINCT a.nama,
									a.keselon,
									a.kgolru,
									a.kuntp,
									a.kunkom,
									a.kununit,
									a.tanggal_lahir,
									a.jabatan,
									(select neselon from referensi_eselon where keselon = a.keselon) as eselon,
									(select ngolru from referensi_golongan_ruang where KGOLRU = a.KGOLRU) as golongan,
									(select pangkat from referensi_golongan_ruang where KGOLRU = a.KGOLRU) as pangkat
			FROM d_pegawai a
			where nip = '$nip' ";
			$sql = "SELECT DISTINCT a.nip,a.nama,c.nama as nama_jabatan
				FROM d_pegawai a
				left join ref_jabatan c on(a.id_jabatan = c.id)
				where nip = '$nip' ";
			$data = $this->db->query($sql)->row_array();
			$nama_bulan = array(
				'1' => 'Januari',
				'2' => 'Februari',
				'3' => 'Maret',
				'4' => 'April',
				'5' => 'Mei',
				'6' => 'Juni',
				'7' => 'Juli',
				'8' => 'Agustus',
				'9' => 'September',
				'10' => 'Oktober',
				'11' => 'November',
				'12' => 'Desember',
			);
			$data = array(
				'title' => 'Daftar Hadir Bulanan',
				'subtitle' => '',
				'rows' => $qc,
				'nip' => $nip,
				'bulan' => $bulan,
				'tahun' => $tahun,
				'data' => $data,
				'nama_bulan' => $nama_bulan
			);
			$this->load->view('view_bulanan',$data);
			input_log($this->user_id,'Lihat Data Absensi Bulanan dengan nip:'.$nip.' bulan:'.$bulan.' tahun:'.$tahun);
		}
	}
	
	public function pdf_bulanan()
	{
		error_reporting(0);
		$this->load->library('pdf');
		$pdf = $this->pdf->load('utf-8', 'F4');
		$nip = $this->uri->segment(3);
		$bulan = $this->uri->segment(4);
		$tahun = $this->uri->segment(5);
		$sc = "select a.*
				from d_absen_harian a
				where nip = '$nip' 
				and bulan = '$bulan' 
				and tahun = '$tahun'
				order by a.tahun asc, bulan asc, tanggal asc
				";
		//echo $sc;
		$qc = $this->db->query($sc);
		if($qc->num_rows() > 0)
		{
			$sql = "SELECT DISTINCT a.nip,a.nama,c.nama as nama_jabatan
				FROM d_pegawai a
				left join ref_jabatan c on(a.id_jabatan = c.id)
				where nip = '$nip' ";
			$datas = $this->db->query($sql)->row_array();
			$nama_bulan = array(
				'1' => 'Januari',
				'2' => 'Februari',
				'3' => 'Maret',
				'4' => 'April',
				'5' => 'Mei',
				'6' => 'Juni',
				'7' => 'Juli',
				'8' => 'Agustus',
				'9' => 'September',
				'10' => 'Oktober',
				'11' => 'November',
				'12' => 'Desember',
			);
			$data = array(
				'title' => 'Daftar Hadir Bulanan',
				'subtitle' => '',
				'rows' => $qc,
				'nip' => $nip,
				'bulan' => $bulan,
				'tahun' => $tahun,
				'data' => $datas,
				'nama_bulan' => $nama_bulan
			);
			$html = $this->load->view('pdf_bulanan',$data,true);
			input_log($this->user_id,'Cetak Data Absensi Bulanan ke dalam bentuk pdf dengan nip:'.$nip.' bulan:'.$bulan.' tahun:'.$tahun);
			$pdf->WriteHTML($html);
			$pdf->Output();
		}
	}
	
	public function refresh_bulanan()
	{
		$nip = $this->uri->segment(3);
		$bulan = $this->uri->segment(4);
		$tahun = $this->uri->segment(5);
		$bulan_now = date('n');
		$update_date = date('Y-m-d H:i:s');
		if($bulan == $bulan_now)
		{
			$jml_tanggal = date('j');
		}
		else
		{
			$har = '1-'.$bulan.'-'.$tahun;
			$jml_tanggal = date('t',strtotime($har));
		}
		
		for($i=1;$i<=$jml_tanggal;$i++)
		{
			$tanggal=$i;
			$tgl = date('Y-m-d',strtotime($tanggal.'-'.$bulan.'-'.$tahun));
			$s = "select * from absensi_log 
					where nip = '$nip' 
					and month(tanggal) = '$bulan' 
					and year(tanggal) = '$tahun' 
					and day(tanggal) = '$tanggal'
					order by tanggal asc";
			//echo $s.'</br>';
			$q = $this->db->query($s);
			if($q->num_rows() > 0)
			{
				foreach($q->result() as $row)
				{
					$this->refresh_kehadiran($nip,$row->tanggal);
				}
			}
			else
			{
				//update data absen harian
				$sca = "select a.id from d_absen_harian a
						where 1=1
						and nip = '$nip'
						and tanggal = '$tanggal'
						and bulan = '$bulan'
						and tahun = '$tahun' ";
				$qca = $this->db->query($sca);
				if($qca->num_rows() == 0)
				{
					$st = "select a.*,
									b.*
							from d_pegawai_tipe_jadwal a
							left join ref_tipe_jadwal b on(a.tipe_jadwal = b.id)
							where nip = '$nip' 
							
							";
					$rst = $this->db->query($st)->row_array();
					//if($rst['tipe_jadwal'] == 1)
					if($rst['shift'] == 0)
					{			
						//$cek_libur = $this->cek_libur($tgl);
						$cek_libur = $this->cek_libur($tgl,$rst['sabtu_masuk']);
					//	echo 'hari libur'."\n";
						if($cek_libur == 1)
						{
							$libur = 1;
							$persentase_potongan = 0;
							$jadwal_jam_masuk = $rst['jadwal_jam_masuk'];
							$jadwal_menit_masuk = $rst['jadwal_menit_masuk'];
							$jadwal_masuk = date('Y-m-d H:i',strtotime($tanggal.'-'.$bulan.'-'.$tahun.' '.$jadwal_jam_masuk.':'.$jadwal_menit_masuk));
							$jadwal_jam_keluar = $rst['jadwal_jam_keluar'];
							$jadwal_menit_keluar = $rst['jadwal_menit_keluar'];
							$jadwal_keluar = date('Y-m-d H:i',strtotime($tanggal.'-'.$bulan.'-'.$tahun.' '.$jadwal_jam_keluar.':'.$jadwal_menit_keluar));	
							$si = "insert into d_absen_harian
									set nip = '$nip',
										tanggal = '$tanggal',
										bulan = '$bulan',
										tahun = '$tahun',
										libur = '$libur',
										persentase_potongan = '$persentase_potongan',
										update_date = '$update_date',
										update_by = 'system_refresh'
							";
						//	echo $si."\n";
							$qi = $this->db->query($si);
						}
						else
						{
							$libur = 0;
							$persentase_potongan = $this->get_tidak_masuk();
							//cek jika jadwal khusus
							$hari_ini = date('w');
							$sc = "select * from referensi_jadwal_khusus where hari = '$hari_ini' and aktif = 1 ";
							$qc = $this->db->query($sc);
							if($qc->num_rows() > 0)
							{
								$rjk = $qc->row_array();
								$jadwal_jam_masuk = $rjk['jadwal_jam_masuk'];
								$jadwal_menit_masuk = $rjk['jadwal_menit_masuk'];
								$jadwal_masuk = date('Y-m-d H:i',strtotime($tanggal.'-'.$bulan.'-'.$tahun.' '.$jadwal_jam_masuk.':'.$jadwal_menit_masuk));
								$jadwal_jam_keluar = $rjk['jadwal_jam_keluar'];
								$jadwal_menit_keluar = $rjk['jadwal_menit_keluar'];
								$jadwal_keluar = date('Y-m-d H:i',strtotime($tanggal.'-'.$bulan.'-'.$tahun.' '.$jadwal_jam_keluar.':'.$jadwal_menit_keluar));	
							}
							else
							{
								$jadwal_jam_masuk = $rst['jadwal_jam_masuk'];
								$jadwal_menit_masuk = $rst['jadwal_menit_masuk'];
								$jadwal_masuk = date('Y-m-d H:i',strtotime($tanggal.'-'.$bulan.'-'.$tahun.' '.$jadwal_jam_masuk.':'.$jadwal_menit_masuk));
								$jadwal_jam_keluar = $rst['jadwal_jam_keluar'];
								$jadwal_menit_keluar = $rst['jadwal_menit_keluar'];
								$jadwal_keluar = date('Y-m-d H:i',strtotime($tanggal.'-'.$bulan.'-'.$tahun.' '.$jadwal_jam_keluar.':'.$jadwal_menit_keluar));	
							}
							
							$si = "insert into d_absen_harian
									set nip = '$nip',
										tanggal = '$tanggal',
										bulan = '$bulan',
										tahun = '$tahun',
										jadwal_masuk = '$jadwal_masuk',
										jadwal_keluar = '$jadwal_keluar',
										libur = '$libur',
										persentase_potongan = '$persentase_potongan',
										update_date = '$update_date',
										update_by = 'system_refresh'
							";
						//	echo $si."\n";
							$qi = $this->db->query($si);
						}
						
					}
					else
					{					
						$sc = "select a.* 
							from d_jadwal_lembur a
							where nip = '$nip'
							and tanggal = '$tgl'
							";
						$qc = $this->db->query($sc);
						if($qc->num_rows() > 0)
						{
							$rc = $qc->row();
							if($rc->is_libur == 1)
							{
								$libur = $rc->libur;
								$persentase_potongan = 0;
								$si = "insert into d_absen_harian
									set nip = '$nip',
										tanggal = '$tanggal',
										bulan = '$bulan',
										tahun = '$tahun',
										libur = '$libur',
										persentase_potongan = '$persentase_potongan',
										update_date = '$update_date',
										update_by = 'system_refresh'
								";
							//	echo $si."\n";
								$qi = $this->db->query($si);
							}
							else
							{
								$jadwal_masuk = $rc->jadwal_masuk;
								$jadwal_keluar = $rc->jadwal_keluar;
								$libur = $rc->libur;
								$persentase_potongan = $this->get_tidak_masuk();
								$si = "insert into d_absen_harian
									set nip = '$nip',
										tanggal = '$tanggal',
										bulan = '$bulan',
										tahun = '$tahun',
										jadwal_masuk = '$jadwal_masuk',
										jadwal_keluar = '$jadwal_keluar',
										libur = '$libur',
										persentase_potongan = '$persentase_potongan',
										update_date = '$update_date',
										update_by = 'system_refresh'
								";
							//	echo $si."\n";
								$qi = $this->db->query($si);
							}						
							
						}
						else
						{
							$libur = 1;
							$persentase_potongan = 0;
							$si = "insert into d_absen_harian
									set nip = '$nip',
										tanggal = '$tanggal',
										bulan = '$bulan',
										tahun = '$tahun',
										libur = '$libur',
										persentase_potongan = '$persentase_potongan',
										update_date = '$update_date',
										update_by = 'system_refresh'
							";
					//		echo $si."\n";
							$qi = $this->db->query($si);
						}
						
					}
					
					//perhitungan tpp
					$tpp = $this->get_tpp($nip);				
					$st = "select a.*,
									b.*
							from d_pegawai_tipe_jadwal a
							left join ref_tipe_jadwal b on(a.tipe_jadwal = b.id)
							where nip = '$nip' 						
							";
					$rst = $this->db->query($st)->row_array();
					//if($rst['tipe_jadwal'] == 1)
					if($rst['shift'] == 0)
					{			
					//	$cek_libur = $this->cek_libur($tgl);
						$cek_libur = $this->cek_libur($tgl,$rst['sabtu_masuk']);
						if($cek_libur == 1)
						{
							$pemotongan_per_hari=0;
							$si = "insert into d_rekap_keuangan_harian
								set tanggal = '$tgl',
									nip = '$nip',
									total_tpp = '$tpp[total_tpp]',
									tpp_statis = '$tpp[tpp_statis]',
									tpp_dinamis = '$tpp[tpp_dinamis]',
									pemotongan_per_hari = '$pemotongan_per_hari',
									update_date = '$update_date',	
									update_by = 'system_refresh'		
							";
						//	echo $si."\n";
							$qi = $this->db->query($si);
						}
						else
						{
							$pemotongan_per_hari = $this->get_tidak_masuk();
							$si = "insert into d_rekap_keuangan_harian
								set tanggal = '$tgl',
									nip = '$nip',
									total_tpp = '$tpp[total_tpp]',
									tpp_statis = '$tpp[tpp_statis]',
									tpp_dinamis = '$tpp[tpp_dinamis]',
									pemotongan_per_hari = '$pemotongan_per_hari',
									update_date = '$update_date',	
									update_by = 'system_refresh'		
							";
						//	echo $si."\n";
							$qi = $this->db->query($si);
						}
						
					}
					else
					{
						$sc = "select a.* 
							from d_jadwal_lembur_tkk a
							where nip = '$nip'
							and tanggal = '$tgl'
							";
						$qc = $this->db->query($sc);
						if($qc->num_rows() > 0)
						{
							$rc = $qc->row();
							if($rc->is_libur == 1)
							{
								$pemotongan_per_hari=0;
								$si = "insert into d_rekap_keuangan_harian
								set tanggal = '$tgl',
									nip = '$nip',
									total_tpp = '$tpp[total_tpp]',
									tpp_statis = '$tpp[tpp_statis]',
									tpp_dinamis = '$tpp[tpp_dinamis]',
									pemotongan_per_hari = '$pemotongan_per_hari',
										update_date = '$update_date',
										update_by = 'system_refresh'
								";
						//		echo $si."\n";
								$qi = $this->db->query($si);
							}
							else
							{
								$pemotongan_per_hari = $this->get_tidak_masuk();
								$si = "insert into d_rekap_keuangan_harian
								set tanggal = '$tgl',
									nip = '$nip',
									total_tpp = '$tpp[total_tpp]',
									tpp_statis = '$tpp[tpp_statis]',
									tpp_dinamis = '$tpp[tpp_dinamis]',
									pemotongan_per_hari = '$pemotongan_per_hari',
										update_date = '$update_date',
										update_by = 'system_refresh'
								";
						//		echo $si."\n";
								$qi = $this->db->query($si);
							}						
							
						}
						else
						{
							$pemotongan_per_hari = $this->get_tidak_masuk();
							$si = "insert into d_rekap_keuangan_harian
								set tanggal = '$tgl',
									nip = '$nip',
									total_tpp = '$tpp[total_tpp]',
									tpp_statis = '$tpp[tpp_statis]',
									tpp_dinamis = '$tpp[tpp_dinamis]',
									pemotongan_per_hari = '$pemotongan_per_hari',
										update_date = '$update_date',
										update_by = 'system_refresh'
								";
						//		echo $si."\n";
								$qi = $this->db->query($si);
						}
						
					}
				
				}
				else
				{
					$tpp = $this->get_tpp($nip);				
					$st = "select a.*,
									b.*
							from d_pegawai_tipe_jadwal a
							left join ref_tipe_jadwal b on(a.tipe_jadwal = b.id)
							where nip = '$nip' 						
							";
					$rst = $this->db->query($st)->row_array();
				//	if($rst['tipe_jadwal'] == 1)
					if($rst['shift'] == 0)
					{			
						//$cek_libur = $this->cek_libur($tgl);
						$cek_libur = $this->cek_libur($tgl,$rst['sabtu_masuk']);
						if($cek_libur == 1)
						{
							$pemotongan_per_hari=0;
						}
						else
						{
							$pemotongan_per_hari = $this->get_tidak_masuk();
						}
						
					}
					else
					{
						$sc = "select a.* 
							from d_jadwal_lembur a
							where nip = '$nip'
							and tanggal = '$tgl'
							";
						$qc = $this->db->query($sc);
						if($qc->num_rows() > 0)
						{
							$rc = $qc->row();
							if($rc->is_libur == 1)
							{
								$pemotongan_per_hari=0;
							}
							else
							{
								$pemotongan_per_hari = $this->get_tidak_masuk();
							}						
							
						}
						else
						{
							$pemotongan_per_hari = $this->get_tidak_masuk();
						}
						
					}
					
					$skeu = "select id from d_rekap_keuangan_harian where tanggal = '$tgl' and nip = '$nip' ";
					$qkeu = $this->db->query($skeu);
					if($qkeu->num_rows() == 0)
					{
							$si = "insert into d_rekap_keuangan_harian 
									set tanggal = '$tgl',
										nip = '$nip',
										total_tpp = '$tpp[total_tpp]',
										tpp_statis = '$tpp[tpp_statis]',
										tpp_dinamis = '$tpp[tpp_dinamis]',
										pemotongan_per_hari = '$pemotongan_per_hari',
										update_date = '$update_date',	
										update_by = 'system_refresh'					
							";
							$this->db->query($si);
					}
				
				}
				
				
			}
			
		}
		
		redirect('/log/bulanan');
	}
	
	public function view_bulanan_tkk()
	{
		$nik = $this->uri->segment(3);
		$bulan = $this->uri->segment(4);
		$tahun = $this->uri->segment(5);
		$sc = "select a.*,
					if(a.jenis_tidak_masuk != 0,(select jenis_ketidakhadiran from referensi_persentase_ketidakhadiran where id = a.jenis_tidak_masuk),'-') as ketidakhadiran
				from d_absen_harian_tkk a
				where nik = '$nik' 
				and bulan = '$bulan' 
				and tahun = '$tahun' 
				order by a.tahun asc, bulan asc, tanggal asc
				";
	//	echo $sc;
		$qc = $this->db->query($sc);
		if($qc->num_rows() > 0)
		{
			$sql = "SELECT DISTINCT a.*,(select nunker from referensi_unit_kerja where kunker = concat(kuntp,kunkom,kununit,rincunit1,rincunit2,rincunit3)) as skpd
			FROM d_tkk a
			where nik = '$nik' ";
			$data = $this->db->query($sql)->row_array();
			$nama_bulan = array(
				'1' => 'Januari',
				'2' => 'Februari',
				'3' => 'Maret',
				'4' => 'April',
				'5' => 'Mei',
				'6' => 'Juni',
				'7' => 'Juli',
				'8' => 'Agustus',
				'9' => 'September',
				'10' => 'Oktober',
				'11' => 'November',
				'12' => 'Desember',
			);
			$data = array(
				'title' => 'Daftar Hadir Bulanan TKK',
				'subtitle' => '',
				'rows' => $qc,
				'nik' => $nik,
				'bulan' => $bulan,
				'tahun' => $tahun,
				'data' => $data,
				'nama_bulan' => $nama_bulan
			);
			$this->load->view('view_bulanan_tkk',$data);
			input_log($this->user_id,'Lihat Data Absensi Bulanan TKK dengan nik:'.$nik.' bulan:'.$bulan.' tahun:'.$tahun);
		}
	}
	
	public function pdf_bulanan_tkk()
	{
		ini_set('display_errors', 0);
		$this->load->library('pdf');
		$pdf = $this->pdf->load();
		$nik = $this->uri->segment(3);
		$bulan = $this->uri->segment(4);
		$tahun = $this->uri->segment(5);
		$sc = "select a.*,
					if(a.jenis_tidak_masuk != 0,(select jenis_ketidakhadiran from referensi_persentase_ketidakhadiran where id = a.jenis_tidak_masuk),'-') as ketidakhadiran
				from d_absen_harian_tkk a
				where nik = '$nik' 
				and bulan = '$bulan' 
				and tahun = '$tahun'
				order by a.tahun asc, bulan asc, tanggal asc
				";
		$qc = $this->db->query($sc);
		if($qc->num_rows() > 0)
		{
			$sql = "SELECT DISTINCT a.*,(select nunker from referensi_unit_kerja where kunker = concat(kuntp,kunkom,kununit,rincunit1,rincunit2,rincunit3)) as skpd
			FROM d_tkk a
			where nik = '$nik'  ";
			$data = $this->db->query($sql)->row_array();
			$nama_bulan = array(
				'1' => 'Januari',
				'2' => 'Februari',
				'3' => 'Maret',
				'4' => 'April',
				'5' => 'Mei',
				'6' => 'Juni',
				'7' => 'Juli',
				'8' => 'Agustus',
				'9' => 'September',
				'10' => 'Oktober',
				'11' => 'November',
				'12' => 'Desember',
			);
			$data = array(
				'title' => 'Daftar Hadir Bulanan',
				'subtitle' => '',
				'rows' => $qc,
				'nik' => $nik,
				'bulan' => $bulan,
				'tahun' => $tahun,
				'data' => $data,
				'nama_bulan' => $nama_bulan
			);
			$html = $this->load->view('pdf_bulanan_tkk',$data,true);
			input_log($this->user_id,'Cetak Data Absensi Bulanan tkk ke dalam bentuk pdf dengan nik:'.$nik.' bulan:'.$bulan.' tahun:'.$tahun);
			$pdf->WriteHTML($html);
			$pdf->Output();
		}
	}
	
	public function refresh_bulanan_tkk()
	{
		$nip = $this->uri->segment(3);
		$nik=$nip;
		$bulan = $this->uri->segment(4);
		$tahun = $this->uri->segment(5);
		$bulan_now = date('n');
		$update_date = date('Y-m-d H:i:s');
		if($bulan == $bulan_now)
		{
			$jml_tanggal = date('j');
		}
		else
		{
			$har = '1-'.$bulan.'-'.$tahun;
			$jml_tanggal = date('t',strtotime($har));
		}
		
		for($i=1;$i<=$jml_tanggal;$i++)
		{
			$tanggal=$i;
			$tgl = date('Y-m-d',strtotime($tanggal.'-'.$bulan.'-'.$tahun));
			$s = "select * from absensi_log 
					where nip = '$nik' 
					and month(tanggal) = '$bulan' 
					and year(tanggal) = '$tahun' 
					and day(tanggal) = '$tanggal'
					order by tanggal asc";
			//echo $s.'</br>';
			$q = $this->db->query($s);
			if($q->num_rows() > 0)
			{
				foreach($q->result() as $row)
				{
					$this->refresh_kehadiran($nip,$row->tanggal);
				}
			}
			else
			{
				//update data absen harian
				$sca = "select a.id from d_absen_harian_tkk a
						where 1=1
						and nik = '$nip'
						and tanggal = '$tanggal'
						and bulan = '$bulan'
						and tahun = '$tahun' ";
				$qca = $this->db->query($sca);
				if($qca->num_rows() == 0)
				{
					$st = "select a.*,
									b.*
							from d_pegawai_tipe_jadwal_tkk a
							left join ref_tipe_jadwal b on(a.tipe_jadwal = b.id)
							where nik = '$nip' 
							
							";
					$rst = $this->db->query($st)->row_array();
					//if($rst['tipe_jadwal'] == 1)
					if($rst['shift'] == 0)
					{			
						//$cek_libur = $this->cek_libur($tgl);
						$cek_libur = $this->cek_libur($tgl,$rst['sabtu_masuk']);
					//	echo 'hari libur'."\n";
						if($cek_libur == 1)
						{
							$libur = 1;
							$persentase_potongan = 0;
							$jadwal_jam_masuk = $rst['jadwal_jam_masuk'];
							$jadwal_menit_masuk = $rst['jadwal_menit_masuk'];
							$jadwal_masuk = date('Y-m-d H:i',strtotime($tanggal.'-'.$bulan.'-'.$tahun.' '.$jadwal_jam_masuk.':'.$jadwal_menit_masuk));
							$jadwal_jam_keluar = $rst['jadwal_jam_keluar'];
							$jadwal_menit_keluar = $rst['jadwal_menit_keluar'];
							$jadwal_keluar = date('Y-m-d H:i',strtotime($tanggal.'-'.$bulan.'-'.$tahun.' '.$jadwal_jam_keluar.':'.$jadwal_menit_keluar));	
							$si = "insert into d_absen_harian_tkk
									set nik = '$nik',
										tanggal = '$tanggal',
										bulan = '$bulan',
										tahun = '$tahun',
										libur = '$libur',
										persentase_potongan = '$persentase_potongan',
										update_date = '$update_date',
										update_by = 'system_refresh'
							";
						//	echo $si."\n";
							$qi = $this->db->query($si);
						}
						else
						{
							$libur = 0;
							$persentase_potongan = $this->get_tidak_masuk();
							//cek jika jadwal khusus
							$hari_ini = date('w');
							$sc = "select * from referensi_jadwal_khusus where hari = '$hari_ini' and aktif = 1 ";
							$qc = $this->db->query($sc);
							if($qc->num_rows() > 0)
							{
								$rjk = $qc->row_array();
								$jadwal_jam_masuk = $rjk['jadwal_jam_masuk'];
								$jadwal_menit_masuk = $rjk['jadwal_menit_masuk'];
								$jadwal_masuk = date('Y-m-d H:i',strtotime($tanggal.'-'.$bulan.'-'.$tahun.' '.$jadwal_jam_masuk.':'.$jadwal_menit_masuk));
								$jadwal_jam_keluar = $rjk['jadwal_jam_keluar'];
								$jadwal_menit_keluar = $rjk['jadwal_menit_keluar'];
								$jadwal_keluar = date('Y-m-d H:i',strtotime($tanggal.'-'.$bulan.'-'.$tahun.' '.$jadwal_jam_keluar.':'.$jadwal_menit_keluar));	
							}
							else
							{
								$jadwal_jam_masuk = $rst['jadwal_jam_masuk'];
								$jadwal_menit_masuk = $rst['jadwal_menit_masuk'];
								$jadwal_masuk = date('Y-m-d H:i',strtotime($tanggal.'-'.$bulan.'-'.$tahun.' '.$jadwal_jam_masuk.':'.$jadwal_menit_masuk));
								$jadwal_jam_keluar = $rst['jadwal_jam_keluar'];
								$jadwal_menit_keluar = $rst['jadwal_menit_keluar'];
								$jadwal_keluar = date('Y-m-d H:i',strtotime($tanggal.'-'.$bulan.'-'.$tahun.' '.$jadwal_jam_keluar.':'.$jadwal_menit_keluar));	
							}
							
							$si = "insert into d_absen_harian_tkk
									set nik = '$nik',
										tanggal = '$tanggal',
										bulan = '$bulan',
										tahun = '$tahun',
										jadwal_masuk = '$jadwal_masuk',
										jadwal_keluar = '$jadwal_keluar',
										libur = '$libur',
										persentase_potongan = '$persentase_potongan',
										update_date = '$update_date',
										update_by = 'system_refresh'
							";
						//	echo $si."\n";
							$qi = $this->db->query($si);
						}
						
					}
					else
					{					
						$sc = "select a.* 
							from d_jadwal_lembur_tkk a
							where nik = '$nik'
							and tanggal = '$tgl'
							";
						$qc = $this->db->query($sc);
						if($qc->num_rows() > 0)
						{
							$rc = $qc->row();
							if($rc->is_libur == 1)
							{
								$libur = $rc->libur;
								$persentase_potongan = 0;
								$si = "insert into d_absen_harian_tkk
									set nik = '$nik',
										tanggal = '$tanggal',
										bulan = '$bulan',
										tahun = '$tahun',
										libur = '$libur',
										persentase_potongan = '$persentase_potongan',
										update_date = '$update_date',
										update_by = 'system_refresh'
								";
							//	echo $si."\n";
								$qi = $this->db->query($si);
							}
							else
							{
								$jadwal_masuk = $rc->jadwal_masuk;
								$jadwal_keluar = $rc->jadwal_keluar;
								$libur = $rc->libur;
								$persentase_potongan = $this->get_tidak_masuk();
								$si = "insert into d_absen_harian_tkk
									set nik = '$nik',
										tanggal = '$tanggal',
										bulan = '$bulan',
										tahun = '$tahun',
										jadwal_masuk = '$jadwal_masuk',
										jadwal_keluar = '$jadwal_keluar',
										libur = '$libur',
										persentase_potongan = '$persentase_potongan',
										update_date = '$update_date',
										update_by = 'system_refresh'
								";
							//	echo $si."\n";
								$qi = $this->db->query($si);
							}						
							
						}
						else
						{
							$libur = 1;
							$persentase_potongan = 0;
							$si = "insert into d_absen_harian_tkk
									set nik = '$nik',
										tanggal = '$tanggal',
										bulan = '$bulan',
										tahun = '$tahun',
										libur = '$libur',
										persentase_potongan = '$persentase_potongan',
										update_date = '$update_date',
										update_by = 'system_refresh'
							";
					//		echo $si."\n";
							$qi = $this->db->query($si);
						}
						
					}
					
					//perhitungan tpp
					$tpp = $this->get_tpp_tkk($nik);				
					$st = "select a.*,
									b.*
							from d_pegawai_tipe_jadwal_tkk a
							left join ref_tipe_jadwal b on(a.tipe_jadwal = b.id)
							where nik = '$nik' 						
							";
					$rst = $this->db->query($st)->row_array();
					//if($rst['tipe_jadwal'] == 1)
					if($rst['shift'] == 0)
					{			
						//$cek_libur = $this->cek_libur($tgl);
						$cek_libur = $this->cek_libur($tgl,$rst['sabtu_masuk']);
						if($cek_libur == 1)
						{
							$pemotongan_per_hari=0;
							$si = "insert into d_rekap_keuangan_harian_tkk
								set tanggal = '$tgl',
									nik = '$nik',
									total_tpp = '$tpp[total_tpp]',
									tpp_statis = '$tpp[tpp_statis]',
									tpp_dinamis = '$tpp[tpp_dinamis]',
									pemotongan_per_hari = '$pemotongan_per_hari',
									update_date = '$update_date',	
									update_by = 'system_refresh'		
							";
						//	echo $si."\n";
							$qi = $this->db->query($si);
						}
						else
						{
							$pemotongan_per_hari = $this->get_tidak_masuk();
							$si = "insert into d_rekap_keuangan_harian_tkk
								set tanggal = '$tgl',
									nik = '$nik',
									total_tpp = '$tpp[total_tpp]',
									tpp_statis = '$tpp[tpp_statis]',
									tpp_dinamis = '$tpp[tpp_dinamis]',
									pemotongan_per_hari = '$pemotongan_per_hari',
									update_date = '$update_date',	
									update_by = 'system_refresh'		
							";
						//	echo $si."\n";
							$qi = $this->db->query($si);
						}
						
					}
					else
					{
						$sc = "select a.* 
							from d_jadwal_lembur_tkk a
							where nik = '$nik'
							and tanggal = '$tgl'
							";
						$qc = $this->db->query($sc);
						if($qc->num_rows() > 0)
						{
							$rc = $qc->row();
							if($rc->is_libur == 1)
							{
								$pemotongan_per_hari=0;
								$si = "insert into d_rekap_keuangan_harian_tkk
								set tanggal = '$tgl',
									nik = '$nik',
									total_tpp = '$tpp[total_tpp]',
									tpp_statis = '$tpp[tpp_statis]',
									tpp_dinamis = '$tpp[tpp_dinamis]',
									pemotongan_per_hari = '$pemotongan_per_hari',
										update_date = '$update_date',
										update_by = 'system_refresh'
								";
						//		echo $si."\n";
								$qi = $this->db->query($si);
							}
							else
							{
								$pemotongan_per_hari = $this->get_tidak_masuk();
								$si = "insert into d_rekap_keuangan_harian_tkk
								set tanggal = '$tgl',
									nik = '$nik',
									total_tpp = '$tpp[total_tpp]',
									tpp_statis = '$tpp[tpp_statis]',
									tpp_dinamis = '$tpp[tpp_dinamis]',
									pemotongan_per_hari = '$pemotongan_per_hari',
										update_date = '$update_date',
										update_by = 'system_refresh'
								";
						//		echo $si."\n";
								$qi = $this->db->query($si);
							}						
							
						}
						else
						{
							$pemotongan_per_hari = $this->get_tidak_masuk();
							$si = "insert into d_rekap_keuangan_harian_tkk
								set tanggal = '$tgl',
									nik = '$nik',
									total_tpp = '$tpp[total_tpp]',
									tpp_statis = '$tpp[tpp_statis]',
									tpp_dinamis = '$tpp[tpp_dinamis]',
									pemotongan_per_hari = '$pemotongan_per_hari',
										update_date = '$update_date',
										update_by = 'system_refresh'
								";
						//		echo $si."\n";
								$qi = $this->db->query($si);
						}
						
					}
					
				}
				else
				{
					//perhitungan tpp
					$tpp = $this->get_tpp_tkk($nik);				
					$st = "select a.*,
									b.*
							from d_pegawai_tipe_jadwal_tkk a
							left join ref_tipe_jadwal b on(a.tipe_jadwal = b.id)
							where nik = '$nik' 						
							";
					$rst = $this->db->query($st)->row_array();
					//if($rst['tipe_jadwal'] == 1)
					if($rst['shift'] == 0)
					{			
						//$cek_libur = $this->cek_libur($tgl);
						$cek_libur = $this->cek_libur($tgl,$rst['sabtu_masuk']);
						if($cek_libur == 1)
						{
							$pemotongan_per_hari=0;
						}
						else
						{
							$pemotongan_per_hari = $this->get_tidak_masuk();
						}
						
					}
					else
					{
						$sc = "select a.* 
							from d_jadwal_lembur_tkk a
							where nik = '$nik'
							and tanggal = '$tgl'
							";
						$qc = $this->db->query($sc);
						if($qc->num_rows() > 0)
						{
							$rc = $qc->row();
							if($rc->is_libur == 1)
							{
								$pemotongan_per_hari=0;
							}
							else
							{
								$pemotongan_per_hari = $this->get_tidak_masuk();
							}						
							
						}
						else
						{
							$pemotongan_per_hari = $this->get_tidak_masuk();
						}
						
					}
					$skeu = "select id from d_rekap_keuangan_harian_tkk where tanggal = '$tgl' and nik = '$nik' ";
					$qkeu = $this->db->query($skeu);
					if($qkeu->num_rows() == 0)
					{
						$si = "insert into d_rekap_keuangan_harian_tkk
								set tanggal = '$tgl',
									nik = '$nik',
									total_tpp = '$tpp[total_tpp]',
									tpp_statis = '$tpp[tpp_statis]',
									tpp_dinamis = '$tpp[tpp_dinamis]',
									pemotongan_per_hari = '$pemotongan_per_hari',
										update_date = '$update_date',
										update_by = 'system_refresh'
								";
						//		echo $si."\n";
								$qi = $this->db->query($si);
					}
				}
			}
			
		}
		
		redirect('/log/bulanan_tkk');
	}
	
	public function manual()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		
		$data = array(
				'title' => 'Data Absensi Manual',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'sssubunit' => $this->session->userdata('sssubunit_manual'),
				'jnsjab' => $this->session->userdata('jnsjab_manual'),
				'keselon' => $this->session->userdata('keselon_manual'),
				'nama' => $this->session->userdata('nama_manual'),
				'reqnip' => $this->session->userdata('reqnip_manual'),
		);
		$this->load->view('manual',$data);
		input_log($this->user_id,'Lihat konfirmasi manual pns');
	}
	
	public function get_manual()
	{
		//print_r($_REQUEST);
		$order = $_REQUEST['order'][0];
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/log/manual');
		//$aColumns = array( 'nip', 'nip','nama', 'nip','nip');
		$sssubunit = $this->input->post('sssubunit');
		$this->session->set_userdata('sssubunit_manual',$sssubunit);
		$jnsjab = $this->input->post('jnsjab');
		$this->session->set_userdata('jnsjab_manual',$jnsjab);
		$keselon = $this->input->post('keselon');
		$this->session->set_userdata('keselon_manual',$keselon);
		$nama = $this->input->post('nama');
		$this->session->set_userdata('nama_manual',$nama);
		$reqnip = $this->input->post('reqnip');
		$this->session->set_userdata('reqnip_manual',$reqnip);
		
		$sSearch = '';
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		if(isset($select) && $select != '')
		{
			if(!empty($s_biro)) 
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
			}
			else
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			}
		}
		
		if(isset($nama) && $nama != '')
		{
			$sSearch .= " and (nama like '%".$nama."%') ";
		}
		if(isset($reqnip) && $reqnip != '')
		{
			$sSearch .= " and (b.nip = '".$reqnip."') ";
		}
		if(isset($jnsjab) && $jnsjab != '0')
		{
			$sSearch .= " and (jnsjab = '".$jnsjab."') ";
		}
		if(isset($keselon) && $keselon != '0')
		{
			$sSearch .= " and (keselon = '".$keselon."') ";
		}
			
		//limit
		if ( isset( $_REQUEST['start'] ) && $_REQUEST['length'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_REQUEST['start'] ).", ".
				intval( $_REQUEST['length'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "`".$aColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]."` ".
						($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		//$sOrder = " order by ".$aColumns[$order['column']]." ".$order['dir']." ";
		$s0 = "select count(a.nip) as jml from d_ketidakhadiran b
		LEFT JOIN d_pegawai a on(a.nip = b.nip)
				where 1 = 1
				";
		$s0 .= $sSearch;
		
		$sheet_total = $this->db->query($s0)->row();
		$sheet_total = $sheet_total->jml;
	
		$s1 = "SELECT DISTINCT a.nama,
									a.keselon,
									a.kgolru,
									a.kuntp,
									a.kunkom,
									a.kununit,
									a.tanggal_lahir,
									a.jabatan,
									(select neselon from referensi_eselon where keselon = a.keselon) as eselon,
									(select ngolru from referensi_golongan_ruang where KGOLRU = a.KGOLRU) as golongan,
									(select pangkat from referensi_golongan_ruang where KGOLRU = a.KGOLRU) as pangkat,
									b.*,
									(select username from t_users where id = b.added_by) as penginput,
									(select username from t_users where id = b.update_by) as pengupdate,
									(select jenis_ketidakhadiran from referensi_persentase_ketidakhadiran where id = b.jenis_ketidakhadiran) as ketidakhadiran
			FROM d_ketidakhadiran b
			LEFT JOIN d_pegawai a on(b.nip = a.nip)
			WHERE 1=1";
		$s1 .= $sSearch;
		$s1 .= " order by id desc ";
		
		$s1 .= $sLimit;
	//	echo $s1;
		$sheet1 = $this->db->query($s1);
		$result = array();
		$i=0;
		$no=1;
		foreach($sheet1->result() as $sheet)
		{
			
			$result[$i]['0'] = $no;
			$result[$i]['1'] = $sheet->nip.' </br> '.$sheet->nama.' </br> '.$sheet->golongan;
			$result[$i]['2'] = $sheet->jabatan.' </br> '.$sheet->eselon;
			$result[$i]['3'] = $sheet->ketidakhadiran;
			$result[$i]['4'] = date('d-M-Y',strtotime($sheet->tanggal_mulai));
			$result[$i]['5'] = date('d-M-Y',strtotime($sheet->tanggal_selesai));
			$result[$i]['6'] = 	$sheet->keterangan;	
			$result[$i]['7'] = 	$sheet->penginput.'</br>'.date('d-m-Y H:i:s',strtotime($sheet->added_date));	
			$result[$i]['8'] = 	$sheet->pengupdate.'</br>'.(($sheet->update_date != NULL && $sheet->update_date != '0000-00-00 00:00:00')?date('d-m-Y H:i:s',strtotime($sheet->update_date)):'');	
			$result[$i]['9'] = '<a target="_blank" href="/log/view_bukti/'.$sheet->id.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="View">
									<i class="glyphicon glyphicon-search"></i>
								</a>';
			if($hak_akses['up'] == 1)
			{
				$result[$i]['10'] = '<a href="/log/edit_manual/'.$sheet->id.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Edit">
									<i class="glyphicon glyphicon-edit"></i>
								</a>';
			}
			else
			{
				$result[$i]['10'] = '';
			}
			if($hak_akses['de'] == 1)
			{
				$result[$i]['11'] = '<a id="del_'.$no.'" href="/log/hapus_manual/'.$sheet->id.'" class="btn btn-danger del" data-toggle="tooltip" data-placement="top" title="Hapus">
								<i class="glyphicon glyphicon-trash"></i>
								</a>
								<script>
								$(document).ready(function () {
									$("#del_'.$no.'").click(function(){
										if (!confirm("Do you want to delete")){
										  return false;
										}
									});
								});
								</script>
								';
			}
			else
			{
				$result[$i]['11'] = '';
			}
			
			$no++;
			$i++;
		}
		
		$data = $result;
		$results = array(
			"iTotalRecords" => ($sheet_total),
			"iTotalDisplayRecords" => ($sheet_total),
			"aaData"=>$data);
			
		echo json_encode($results);
	}
	
	public function tambah_manual()
	{
		$this->load->library('form_validation');
			$this->form_validation->set_rules('nip', 'NIP', 'trim|required');
			$this->form_validation->set_rules('tanggal_mulai', 'Tanggal Mulai', 'trim|required');
			$this->form_validation->set_rules('tanggal_selesai', 'Tanggal Selesai', 'trim|required');
			
			if ($this->form_validation->run() === TRUE)
			{
				$posts = $this->input->post();
				//print_r($posts);
			//	echo '</br>';
				$nip = $this->db->escape_str($posts['nip']);
				$jenis_ketidakhadiran = $this->db->escape_str($posts['jenis_ketidakhadiran']);
				$tanggal_mulai = $this->db->escape_str($posts['tanggal_mulai']);
				$tanggal_mulai = date('Y-m-d',strtotime($tanggal_mulai));
				$tanggal_selesai = $this->db->escape_str($posts['tanggal_selesai']);
				$tanggal_selesai = date('Y-m-d',strtotime($tanggal_selesai));
				$keterangan = $this->db->escape_str($posts['keterangan']);	
				$added_date = date('Y-m-d H:i:s');
				$added_by = $this->userid;
				$jml_file = $posts['jml_file'];
				$sql = 	"insert into d_ketidakhadiran 
							SET nip = '$nip', 
								jenis_ketidakhadiran = '$jenis_ketidakhadiran',
								tanggal_mulai = '$tanggal_mulai',
								tanggal_selesai = '$tanggal_selesai',
								keterangan = '$keterangan',
								added_date = '$added_date',
								added_by = '$added_by'
								";
			///	echo $sql.'</br>';
				$this->db->query($sql);	
				$id = $this->db->insert_id();
				//upload foto
				//$config['upload_path'] = 'assets/foto/';
				$path = 'assets/bukti/';
				$config['upload_path'] = $path;
				$config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
				$this->load->library('upload', $config);
				
				if($jml_file > 0)
				{
					for($i=1;$i<=$jml_file;$i++)
					{
						$field_name = 'input_'.$i;
						if ($this->upload->do_upload($field_name))
						{
							$data = array('upload_data' => $this->upload->data());
							$file_1 = $path.$id.'-'.$i.time().$data['upload_data']['file_ext'];
							if (file_exists($file_1)) {unlink($file_1);}       
							$org_1 = $path.$data['upload_data']['file_name'];
							rename($org_1, $file_1);	
							$nama_file = $id.'-'.$i.time().$data['upload_data']['file_ext'];
						}
						else
						{
							$this->session->set_flashdata('message', $this->upload->display_errors());	
							$err = $this->upload->display_errors();
							print_r($err);
							$nama_file = '';
						}
						$si = " insert into d_ketidakhadiran_bukti
								set id_ketidakhadiran = '$id',
									nama_file = '$nama_file'
							";
						echo $si.'</br>';
						$this->db->query($si);
					}
					
				}
				
				$this->update_absen($id);
				input_log($this->user_id,'Tambah konfirmasi manual pns dengan id:'.$id);				
				redirect('/log/manual');
			}
			else
			{
				$this->data['nip'] = array(
						'name'  => 'nip',
						'id'    => 'nip',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('nip'),
						'class'  => 'form-control',
					);
				$this->data['tanggal_mulai'] = array(
						'name'  => 'tanggal_mulai',
						'id'    => 'tanggal_mulai',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('tanggal_mulai'),
						'class'  => 'form-control datepicker',
					);
				$this->data['tanggal_selesai'] = array(
						'name'  => 'tanggal_selesai',
						'id'    => 'tanggal_selesai',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('tanggal_selesai'),
						'class'  => 'form-control datepicker',
					);
					$this->data['keterangan'] = array(
						'name'  => 'keterangan',
						'id'    => 'keterangan',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('keterangan'),
						'class'  => 'form-control',
					);
								
					
					$this->data['jenis_ketidakhadiran'] = array(
					'name'  => 'jenis_ketidakhadiran',
					'id'    => 'jenis_ketidakhadiran',
					'class'  => 'form-control',
					);
					$s = "select * from referensi_persentase_ketidakhadiran where 1 = 1";
					$q = $this->db->query($s);
					$options_jenis_ketidakhadiran[''] = '----------';
					foreach($q->result_array() as $rq)
					{
						$options_jenis_ketidakhadiran[($rq['id'])] = $rq['jenis_ketidakhadiran'];
					}
					
					$this->data['options_jenis_ketidakhadiran'] = $options_jenis_ketidakhadiran;
					$this->data['selected_ketidakhadiran'] = $this->form_validation->set_value('jenis_ketidakhadiran');
														
					$this->data['title'] = 'Data Kehadiran Manual';
					$this->data['subtitle'] = 'Form Penambahan Data Kehadiran Manual';
					$this->data['error'] = '';
					$this->load->view('form_manual',$this->data);
			}
			
	}
	
	public function edit_manual()
	{
		$id = $this->uri->segment(3);
		if($this->groupid != 1)
		{
			$sc = "select id from d_ketidakhadiran where id='$id' and added_by = '$this->user_id' ";
			//echo $sc;
			$qc = $this->db->query($sc);
			if($qc->num_rows() == 0)
			{
				redirect('/');
			}
		}
		$s = "select a.*,
					b.nama as nama_lengkap,
					(select ngolru from referensi_golongan_ruang where kgolru = b.kgolru) as golongan,
					b.jabatan,
					CASE KUNKOM
								   WHEN 10 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' 	)
								   WHEN 11 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' 	) 
								   ELSE (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,4) = concat(kuntp,kunkom) and mid(kunker,5,8) = '00000000' 	)
					END as skpd
				from d_ketidakhadiran a
				left join d_pegawai b on(a.nip = b.nip)
				where a.id = '$id' ";
				//echo $s;
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			$this->load->library('form_validation');
			//$this->form_validation->set_rules('nip', 'NIP', 'trim|required');
			$this->form_validation->set_rules('tanggal_mulai', 'Tanggal Mulai', 'trim|required');
			$this->form_validation->set_rules('tanggal_selesai', 'Tanggal Selesai', 'trim|required');
			
			if ($this->form_validation->run() === TRUE)
			{
				//hapus absen dan rekap
				$r = $this->db->query($s)->row();
				$nip = $r->nip;
				$tanggal_mulai = $r->tanggal_mulai;
				$tanggal_selesai = $r->tanggal_selesai;
				$selisih = (strtotime($tanggal_selesai) - strtotime($tanggal_mulai))/(60*60*24);
				for($i=0;$i<=$selisih;$i++)
					{
						$penambah = $i*60*60*24;
						$tanggal_time = strtotime($tanggal_mulai) + $penambah;
						$tanggal = date('Y-m-d',$tanggal_time);
						$tgl = date('j',$tanggal_time);
						$bln = date('n',$tanggal_time);
						$thn = date('Y',$tanggal_time);
						$update_date = date('Y-m-d H:i:s');
						//$libur = $this->cek_libur($tanggal);
						$now = date('Y-m-d');
						$now_time = strtotime($now);
						if($tanggal_time > $now_time)
						{
							$sd = "delete from d_absen_harian where nip = '$nip' and tanggal = '$tgl' and bulan = '$bln' and tahun = '$thn' ";
						//	echo $sd.'</br>';
							$qc = $this->db->query($sd);						
							$sdr = "delete from d_rekap_keuangan_harian where nip = '$nip' and tanggal = '$tanggal' ";
							$qc = $this->db->query($sdr);	
						}
					}
				//update data ketidakhadiran
				$posts = $this->input->post();
			//	print_r($posts);
			//	echo '</br>';
				//$nip = $this->db->escape_str($posts['nip']);
				$jenis_ketidakhadiran = $this->db->escape_str($posts['jenis_ketidakhadiran']);
				$tanggal_mulai = $this->db->escape_str($posts['tanggal_mulai']);
				$tanggal_mulai = date('Y-m-d',strtotime($tanggal_mulai));
				$tanggal_selesai = $this->db->escape_str($posts['tanggal_selesai']);
				$tanggal_selesai = date('Y-m-d',strtotime($tanggal_selesai));
				$keterangan = $this->db->escape_str($posts['keterangan']);	
				$update_date = date('Y-m-d H:i:s');
				$update_by = $this->userid;
				$jml_file = $posts['jml_file'];
				$sql = 	"update d_ketidakhadiran 
							SET jenis_ketidakhadiran = '$jenis_ketidakhadiran',
								tanggal_mulai = '$tanggal_mulai',
								tanggal_selesai = '$tanggal_selesai',
								keterangan = '$keterangan',
								update_date = '$update_date',
								update_by = '$update_by'
							where id = '$id'
								";
			//	echo $sql.'</br>';
				$this->db->query($sql);	
				//upload foto
				//$config['upload_path'] = 'assets/foto/';
				$path = 'assets/bukti/';
				$config['upload_path'] = $path;
				$config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
				$this->load->library('upload', $config);
				if($jml_file > 0)
				{
					for($i=1;$i<=$jml_file;$i++)
					{
						$field_name = 'input_'.$i;
						if ($this->upload->do_upload($field_name))
						{
							$data = array('upload_data' => $this->upload->data());
							$file_1 = $path.$id.'-'.$i.time().$data['upload_data']['file_ext'];
							if (file_exists($file_1)) {unlink($file_1);}       
							$org_1 = $path.$data['upload_data']['file_name'];
							rename($org_1, $file_1);	
							$nama_file = $id.'-'.$i.time().$data['upload_data']['file_ext'];
						}
						else
						{
							$this->session->set_flashdata('message', $this->upload->display_errors());	
							$err = $this->upload->display_errors();
							print_r($err);
							$nama_file = '';
						}
						$si = " insert into d_ketidakhadiran_bukti
								set id_ketidakhadiran = '$id',
									nama_file = '$nama_file'
							";
						echo $si.'</br>';
						$this->db->query($si);
					}
					
				}
				
				$this->update_absen($id);	
				input_log($this->user_id,'Edit konfirmasi manual pns dengan id:'.$id);
				redirect('/log/manual');
			}
			else
			{
				$this->data['tanggal_mulai'] = array(
						'name'  => 'tanggal_mulai',
						'id'    => 'tanggal_mulai',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('tanggal_mulai',(isset($r->tanggal_mulai)?date('d-m-Y',strtotime($r->tanggal_mulai)):'')),
						'class'  => 'form-control datepicker',
					);
				$this->data['tanggal_selesai'] = array(
						'name'  => 'tanggal_selesai',
						'id'    => 'tanggal_selesai',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('tanggal_selesai',(isset($r->tanggal_selesai)?date('d-m-Y',strtotime($r->tanggal_selesai)):'')),
						'class'  => 'form-control datepicker',
					);
					$this->data['keterangan'] = array(
						'name'  => 'keterangan',
						'id'    => 'keterangan',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('keterangan',$r->keterangan),
						'class'  => 'form-control',
					);
								
					
					$this->data['jenis_ketidakhadiran'] = array(
					'name'  => 'jenis_ketidakhadiran',
					'id'    => 'jenis_ketidakhadiran',
					'class'  => 'form-control',
					);
					$s = "select * from referensi_persentase_ketidakhadiran where 1 = 1";
					$q = $this->db->query($s);
					$options_jenis_ketidakhadiran[''] = '----------';
					foreach($q->result_array() as $rq)
					{
						$options_jenis_ketidakhadiran[($rq['id'])] = $rq['jenis_ketidakhadiran'];
					}
					
					$this->data['options_jenis_ketidakhadiran'] = $options_jenis_ketidakhadiran;
					$this->data['selected_ketidakhadiran'] = $this->form_validation->set_value('jenis_ketidakhadiran',$r->jenis_ketidakhadiran);
														
					$this->data['title'] = 'Data Kehadiran Manual';
					$this->data['subtitle'] = 'Form Penambahan Data Kehadiran Manual';
					$this->data['error'] = '';
					$this->data['r'] = $r;
					$this->load->view('form_edit_manual',$this->data);
			}
		}
		else
		{
			redirect('/');
		}
			
	}
	
	public function hapus_manual()
	{
		$id = $this->uri->segment(3);
		if($this->groupid != 1)
		{
			$sc = "select id from d_ketidakhadiran where id='$id' and added_by = '$this->user_id' ";
			//echo $sc;
			$qc = $this->db->query($sc);
			if($qc->num_rows() == 0)
			{
				redirect('/');
			}
		}
		$sct = "select nip,tanggal_mulai,tanggal_selesai from d_ketidakhadiran where id = '$id' ";
		$qct = $this->db->query($sct);
		if($qct->num_rows() > 0)
		{
			$r = $qct->row();
				$nip = $r->nip;
				$tanggal_mulai = $r->tanggal_mulai;
			//	echo $tanggal_mulai.'</br>';
				$tanggal_selesai = $r->tanggal_selesai;
				$selisih = (strtotime($tanggal_selesai) - strtotime($tanggal_mulai))/(60*60*24);
				for($i=0;$i<=$selisih;$i++)
				{
					$penambah = $i*60*60*24;
					$tanggal_time = strtotime($tanggal_mulai) + $penambah;
					$tanggal = date('Y-m-d',$tanggal_time);
					$tgl = date('j',$tanggal_time);
					$bln = date('n',$tanggal_time);
					$thn = date('Y',$tanggal_time);
					$now = date('Y-m-d');
					$now_time = strtotime($now);
					if($tanggal_time > $now_time)
					{
						$sd = "delete from d_absen_harian where nip = '$nip' and tanggal = '$tgl' and bulan = '$bln' and tahun = '$thn' ";
						//echo $sd;
						$this->db->query($sd);
						$sdr = "delete from d_rekap_keuangan_harian where nip = '$nip' and tanggal = '$tanggal' ";
						//echo $sdr;
						$this->db->query($sdr);
					}
									
				}
			
			$sc = "select nama_file from d_ketidakhadiran_bukti where id_ketidakhadiran = '$id' ";
			$q = $this->db->query($sc);
			foreach($q->result_array() as $ri)
			{
				unlink('assets/bukti/'.$ri['nama_file']);	
			}
			$d1 = "delete from d_ketidakhadiran_bukti where id_ketidakhadiran = '$id' ";
			$this->db->query($d1);
			
			//hapus data ketidakhadiran
			$s = "delete from d_ketidakhadiran where id = '$id' ";
		//	echo $s;
			$this->db->query($s);
			input_log($this->user_id,'Hapus konfirmasi manual pns dengan id:'.$id);
			redirect('/log/manual');
		}
		
	}
	
	public function view_bukti()
	{
		$id = $this->uri->segment(3);
		$s = "select * from d_ketidakhadiran_bukti where id_ketidakhadiran = '$id' ";
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			foreach($q->result() as $row)
			{				
				echo '&nbsp; <img src="/assets/bukti/'.$row->nama_file.'"></br>';
				echo '<hr>';
			}
			
		}
		else
		{
			echo '<img src="/assets/bukti/noimage.png">';
		}
	}
	
	public function hapus_bukti()
	{
		$id = $this->uri->segment(3);
		if($this->groupid != 1)
		{
			$sc = "select id from d_ketidakhadiran where id='$id' and added_by = '$this->user_id' ";
			//echo $sc;
			$qc = $this->db->query($sc);
			if($qc->num_rows() == 0)
			{
				redirect('/');
			}
		}
		$s = "select * from d_ketidakhadiran_bukti where id = '$id' ";
		$ri = $this->db->query($s)->row_array();
		unlink('assets/bukti/'.$ri['nama_file']);	
		$d = "delete from d_ketidakhadiran_bukti where id = '$id' ";
		$this->db->query($d);
		redirect('/log/edit_manual/'.$ri['id_ketidakhadiran']);
		
	}
	
	public function manual_tkk()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		
		$data = array(
				'title' => 'Data Absensi Manual TKK',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'sssubunit' => $this->session->userdata('sssubunit_manual_tkk'),
				'jnsjab' => $this->session->userdata('jnsjab_manual_tkk'),
				'keselon' => $this->session->userdata('keselon_manual_tkk'),
				'nama' => $this->session->userdata('nama_manual_tkk'),
				'nik' => $this->session->userdata('nik_manual_tkk'),
		);
		$this->load->view('manual_tkk',$data);
		input_log($this->user_id,'Lihat konfirmasi manual tkk');
	}
	
	public function get_manual_tkk()
	{
		//print_r($_REQUEST);
		$order = $_REQUEST['order'][0];
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/log/manual_tkk');
		//$aColumns = array( 'nip', 'nip','nama', 'nip','nip');
		$sssubunit = $this->input->post('sssubunit');
		$this->session->set_userdata('sssubunit_manual_tkk',$sssubunit);
		$nama = $this->input->post('nama');
		$this->session->set_userdata('nama_manual_tkk',$nama);
		$nik = $this->input->post('nik');
		$this->session->set_userdata('nik_manual_tkk',$nik);
		
		$sSearch = '';
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		if(isset($select) && $select != '')
		{
			if(!empty($s_biro)) 
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
			}
			else
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			}
		}
		
		if(isset($nama) && $nama != '')
		{
			$sSearch .= " and (nama like '%".$nama."%') ";
		}
		if(isset($nik) && $nik != '')
		{
			$sSearch .= " and (b.nik = '".$nik."') ";
		}
			
		//limit
		if ( isset( $_REQUEST['start'] ) && $_REQUEST['length'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_REQUEST['start'] ).", ".
				intval( $_REQUEST['length'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "`".$aColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]."` ".
						($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		//$sOrder = " order by ".$aColumns[$order['column']]." ".$order['dir']." ";
		$s0 = "select count(a.nik) as jml from d_ketidakhadiran_tkk b
		LEFT JOIN d_tkk a on(a.nik = b.nik)
				where 1 = 1
				";
		$s0 .= $sSearch;
		
		$sheet_total = $this->db->query($s0)->row();
		$sheet_total = $sheet_total->jml;
	
		$s1 = "SELECT DISTINCT a.nik,a.nama,									
									b.*,
									(select username from t_users where id = b.added_by) as penginput,
									(select username from t_users where id = b.update_by) as pengupdate,
									concat(kuntp,kunkom,kununit,rincunit1,rincunit2,rincunit3) as kunker,
									(select jenis_ketidakhadiran from referensi_persentase_ketidakhadiran where id = b.jenis_ketidakhadiran) as ketidakhadiran
			FROM d_ketidakhadiran_tkk b
			LEFT JOIN d_tkk a on(b.nik = a.nik)
			WHERE 1=1";
		$s1 .= $sSearch;
		$s1 .= " order by b.id desc ";
		
		$s1 .= $sLimit;
	//	echo $s1;
		$sheet1 = $this->db->query($s1);
		$result = array();
		$i=0;
		$no=1;
		foreach($sheet1->result() as $sheet)
		{
			
			$result[$i]['0'] = $no;
			$result[$i]['1'] = $sheet->nik.' </br> '.$sheet->nama;
			$result[$i]['2'] = $this->get_skpd($sheet->kunker);
			$result[$i]['3'] = $sheet->ketidakhadiran;
			$result[$i]['4'] = date('d-M-Y',strtotime($sheet->tanggal_mulai));
			$result[$i]['5'] = date('d-M-Y',strtotime($sheet->tanggal_selesai));
			$result[$i]['6'] = 	$sheet->keterangan;	
			$result[$i]['7'] = 	$sheet->penginput.'</br>'.date('d-m-Y H:i:s',strtotime($sheet->added_date));	
			$result[$i]['8'] = 	$sheet->pengupdate.'</br>'.(($sheet->update_date != NULL && $sheet->update_date != '0000-00-00 00:00:00')?date('d-m-Y H:i:s',strtotime($sheet->update_date)):'');	
			$result[$i]['9'] = '<a target="_blank" href="/log/view_bukti_tkk/'.$sheet->id.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="View">
									<i class="glyphicon glyphicon-search"></i>
								</a>';
			if($hak_akses['up'] == 1)
			{
				$result[$i]['10'] = '<a href="/log/edit_manual_tkk/'.$sheet->id.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Edit">
									<i class="glyphicon glyphicon-edit"></i>
								</a>';
			}
			else
			{
				$result[$i]['10'] = '';
			}
			if($hak_akses['de'] == 1)
			{
				$result[$i]['11'] = '<a id="del_'.$no.'" href="/log/hapus_manual_tkk/'.$sheet->id.'" class="btn btn-danger del" data-toggle="tooltip" data-placement="top" title="Hapus">
								<i class="glyphicon glyphicon-trash"></i>
								</a>
								<script>
								$(document).ready(function () {
									$("#del_'.$no.'").click(function(){
										if (!confirm("Do you want to delete")){
										  return false;
										}
									});
								});
								</script>
								';
			}
			else
			{
				$result[$i]['11'] = '';
			}
			
			$no++;
			$i++;
		}
		
		$data = $result;
		$results = array(
			"iTotalRecords" => ($sheet_total),
			"iTotalDisplayRecords" => ($sheet_total),
			"aaData"=>$data);
			
		echo json_encode($results);
	}
	
	public function tambah_manual_tkk()
	{
		$this->load->library('form_validation');
			$this->form_validation->set_rules('nik', 'NIK', 'trim|required');
			$this->form_validation->set_rules('tanggal_mulai', 'Tanggal Mulai', 'trim|required');
			$this->form_validation->set_rules('tanggal_selesai', 'Tanggal Selesai', 'trim|required');
			
			if ($this->form_validation->run() === TRUE)
			{
				$posts = $this->input->post();
			//	print_r($posts);
			//	echo '</br>';
				$nik = $this->db->escape_str($posts['nik']);
				$jenis_ketidakhadiran = $this->db->escape_str($posts['jenis_ketidakhadiran']);
				$tanggal_mulai = $this->db->escape_str($posts['tanggal_mulai']);
				$tanggal_mulai = date('Y-m-d',strtotime($tanggal_mulai));
				$tanggal_selesai = $this->db->escape_str($posts['tanggal_selesai']);
				$tanggal_selesai = date('Y-m-d',strtotime($tanggal_selesai));
				$keterangan = $this->db->escape_str($posts['keterangan']);	
				$added_date = date('Y-m-d H:i:s');
				$added_by = $this->userid;
				$jml_file = $posts['jml_file'];
				$sql = 	"insert into d_ketidakhadiran_tkk 
							SET nik = '$nik', 
								jenis_ketidakhadiran = '$jenis_ketidakhadiran',
								tanggal_mulai = '$tanggal_mulai',
								tanggal_selesai = '$tanggal_selesai',
								keterangan = '$keterangan',
								added_date = '$added_date',
								added_by = '$added_by'
								";
			//	echo $sql.'</br>';
				$this->db->query($sql);	
				$id = $this->db->insert_id();
				//upload foto
				$path = 'assets/bukti_tkk/';
				$config['upload_path'] = $path;
				$config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
				$this->load->library('upload', $config);
				
				if($jml_file > 0)
				{
					for($i=1;$i<=$jml_file;$i++)
					{
						$field_name = 'input_'.$i;
						if ($this->upload->do_upload($field_name))
						{
							$data = array('upload_data' => $this->upload->data());
							$file_1 = $path.$id.'-'.$i.time().$data['upload_data']['file_ext'];
							if (file_exists($file_1)) {unlink($file_1);}       
							$org_1 = $path.$data['upload_data']['file_name'];
							rename($org_1, $file_1);	
							$nama_file = $id.'-'.$i.time().$data['upload_data']['file_ext'];
						}
						else
						{
							$this->session->set_flashdata('message', $this->upload->display_errors());	
							$err = $this->upload->display_errors();
							print_r($err);
							$nama_file = '';
						}
						$si = " insert into d_ketidakhadiran_bukti_tkk
								set id_ketidakhadiran_tkk = '$id',
									nama_file = '$nama_file'
							";
						echo $si.'</br>';
						$this->db->query($si);
					}
					
				}
								
				$this->update_absen_tkk($id);	
				input_log($this->user_id,'tambah konfirmasi manual tkk dengan id:'.$id);
				redirect('/log/manual_tkk');
			}
			else
			{
				$this->data['nik'] = array(
						'name'  => 'nik',
						'id'    => 'nik',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('nik'),
						'class'  => 'form-control',
					);
				$this->data['tanggal_mulai'] = array(
						'name'  => 'tanggal_mulai',
						'id'    => 'tanggal_mulai',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('tanggal_mulai'),
						'class'  => 'form-control datepicker',
					);
				$this->data['tanggal_selesai'] = array(
						'name'  => 'tanggal_selesai',
						'id'    => 'tanggal_selesai',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('tanggal_selesai'),
						'class'  => 'form-control datepicker',
					);
					$this->data['keterangan'] = array(
						'name'  => 'keterangan',
						'id'    => 'keterangan',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('keterangan'),
						'class'  => 'form-control',
					);
								
					
					$this->data['jenis_ketidakhadiran'] = array(
					'name'  => 'jenis_ketidakhadiran',
					'id'    => 'jenis_ketidakhadiran',
					'class'  => 'form-control',
					);
					$s = "select * from referensi_persentase_ketidakhadiran where 1 = 1";
					$q = $this->db->query($s);
					$options_jenis_ketidakhadiran[''] = '----------';
					foreach($q->result_array() as $rq)
					{
						$options_jenis_ketidakhadiran[($rq['id'])] = $rq['jenis_ketidakhadiran'];
					}
					
					$this->data['options_jenis_ketidakhadiran'] = $options_jenis_ketidakhadiran;
					$this->data['selected_ketidakhadiran'] = $this->form_validation->set_value('jenis_ketidakhadiran');
														
					$this->data['title'] = 'Data Kehadiran Manual TKK';
					$this->data['subtitle'] = 'Form Penambahan Data Kehadiran Manual TKK';
					$this->data['error'] = '';
					$this->load->view('form_manual_tkk',$this->data);
			}
			
	}
	
	public function edit_manual_tkk()
	{
		$id = $this->uri->segment(3);
		if($this->groupid != 1)
		{
			$sc = "select id from d_ketidakhadiran_tkk where id='$id' and added_by = '$this->user_id' ";
			//echo $sc;
			$qc = $this->db->query($sc);
			if($qc->num_rows() == 0)
			{
				redirect('/');
			}
		}
		$s = "select a.*,
					b.nama as nama_lengkap,
					CASE KUNKOM
								   WHEN 10 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' 	)
								   WHEN 11 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' 	) 
								   ELSE (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,4) = concat(kuntp,kunkom) and mid(kunker,5,8) = '00000000' 	)
					END as skpd
				from d_ketidakhadiran_tkk a
				left join d_tkk b on(a.nik = b.nik)
				where a.id = '$id' ";
				//echo $s;
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			$this->load->library('form_validation');
			//$this->form_validation->set_rules('nip', 'NIP', 'trim|required');
			$this->form_validation->set_rules('tanggal_mulai', 'Tanggal Mulai', 'trim|required');
			$this->form_validation->set_rules('tanggal_selesai', 'Tanggal Selesai', 'trim|required');
			
			if ($this->form_validation->run() === TRUE)
			{
				//hapus absen dan rekap
				$r = $this->db->query($s)->row();
				$nik = $r->nik;
				$tanggal_mulai = $r->tanggal_mulai;
				$tanggal_selesai = $r->tanggal_selesai;
				$selisih = (strtotime($tanggal_selesai) - strtotime($tanggal_mulai))/(60*60*24);
				for($i=0;$i<=$selisih;$i++)
					{
						$penambah = $i*60*60*24;
						$tanggal_time = strtotime($tanggal_mulai) + $penambah;
						$tanggal = date('Y-m-d',$tanggal_time);
						$tgl = date('j',$tanggal_time);
						$bln = date('n',$tanggal_time);
						$thn = date('Y',$tanggal_time);
						$update_date = date('Y-m-d H:i:s');
					//	$libur = $this->cek_libur($tanggal);
						$now = date('Y-m-d');
						$now_time = strtotime($now);
						if($tanggal_time > $now_time)
						{
								$sd = "delete from d_absen_harian_tkk where nik = '$nik' and tanggal = '$tgl' and bulan = '$bln' and tahun = '$thn' ";
							//	echo $sd.'</br>';
								$qc = $this->db->query($sd);						
								$sdr = "delete from d_rekap_keuangan_harian_tkk where nik = '$nik' and tanggal = '$tanggal' ";
								$qc = $this->db->query($sdr);	
						}
					}
				//update data ketidakhadiran
				$posts = $this->input->post();
			//	print_r($posts);
			//	echo '</br>';
				//$nip = $this->db->escape_str($posts['nip']);
				$jenis_ketidakhadiran = $this->db->escape_str($posts['jenis_ketidakhadiran']);
				$tanggal_mulai = $this->db->escape_str($posts['tanggal_mulai']);
				$tanggal_mulai = date('Y-m-d',strtotime($tanggal_mulai));
				$tanggal_selesai = $this->db->escape_str($posts['tanggal_selesai']);
				$tanggal_selesai = date('Y-m-d',strtotime($tanggal_selesai));
				$keterangan = $this->db->escape_str($posts['keterangan']);	
				$update_date = date('Y-m-d H:i:s');
				$update_by = $this->userid;
				$jml_file = $posts['jml_file'];
				$sql = 	"update d_ketidakhadiran_tkk 
							SET jenis_ketidakhadiran = '$jenis_ketidakhadiran',
								tanggal_mulai = '$tanggal_mulai',
								tanggal_selesai = '$tanggal_selesai',
								keterangan = '$keterangan',
								update_date = '$update_date',
								update_by = '$update_by'
							where id = '$id'
								";
			//	echo $sql.'</br>';
				$this->db->query($sql);	
				//upload foto
				//$config['upload_path'] = 'assets/foto/';
				$path = 'assets/bukti_tkk/';
				$config['upload_path'] = $path;
				$config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
				$this->load->library('upload', $config);
				if($jml_file > 0)
				{
					for($i=1;$i<=$jml_file;$i++)
					{
						$field_name = 'input_'.$i;
						if ($this->upload->do_upload($field_name))
						{
							$data = array('upload_data' => $this->upload->data());
							$file_1 = $path.$id.'-'.$i.time().$data['upload_data']['file_ext'];
							if (file_exists($file_1)) {unlink($file_1);}       
							$org_1 = $path.$data['upload_data']['file_name'];
							rename($org_1, $file_1);	
							$nama_file = $id.'-'.$i.time().$data['upload_data']['file_ext'];
						}
						else
						{
							$this->session->set_flashdata('message', $this->upload->display_errors());	
							$err = $this->upload->display_errors();
							print_r($err);
							$nama_file = '';
						}
						$si = " insert into d_ketidakhadiran_bukti_tkk
								set id_ketidakhadiran_tkk = '$id',
									nama_file = '$nama_file'
							";
						//echo $si.'</br>';
						$this->db->query($si);
					}
					
				}
								
				$this->update_absen_tkk($id);	
				input_log($this->user_id,'edit konfirmasi manual tkk dengan id:'.$id);
				redirect('/log/manual_tkk');
			}
			else
			{
				$this->data['tanggal_mulai'] = array(
						'name'  => 'tanggal_mulai',
						'id'    => 'tanggal_mulai',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('tanggal_mulai',(isset($r->tanggal_mulai)?date('d-m-Y',strtotime($r->tanggal_mulai)):'')),
						'class'  => 'form-control datepicker',
					);
				$this->data['tanggal_selesai'] = array(
						'name'  => 'tanggal_selesai',
						'id'    => 'tanggal_selesai',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('tanggal_selesai',(isset($r->tanggal_selesai)?date('d-m-Y',strtotime($r->tanggal_selesai)):'')),
						'class'  => 'form-control datepicker',
					);
					$this->data['keterangan'] = array(
						'name'  => 'keterangan',
						'id'    => 'keterangan',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('keterangan',$r->keterangan),
						'class'  => 'form-control',
					);
								
					
					$this->data['jenis_ketidakhadiran'] = array(
					'name'  => 'jenis_ketidakhadiran',
					'id'    => 'jenis_ketidakhadiran',
					'class'  => 'form-control',
					);
					$s = "select * from referensi_persentase_ketidakhadiran where 1 = 1";
					$q = $this->db->query($s);
					$options_jenis_ketidakhadiran[''] = '----------';
					foreach($q->result_array() as $rq)
					{
						$options_jenis_ketidakhadiran[($rq['id'])] = $rq['jenis_ketidakhadiran'];
					}
					
					$this->data['options_jenis_ketidakhadiran'] = $options_jenis_ketidakhadiran;
					$this->data['selected_ketidakhadiran'] = $this->form_validation->set_value('jenis_ketidakhadiran',$r->jenis_ketidakhadiran);
														
					$this->data['title'] = 'Data Kehadiran Manual';
					$this->data['subtitle'] = 'Form Penambahan Data Kehadiran Manual';
					$this->data['error'] = '';
					$this->data['r'] = $r;
					$this->load->view('form_edit_manual_tkk',$this->data);
			}
		}
		else
		{
			redirect('/');
		}
			
	}
	
	public function hapus_manual_tkk()
	{
		$id = $this->uri->segment(3);
		if($this->groupid != 1)
		{
			$sc = "select id from d_ketidakhadiran_tkk where id='$id' and added_by = '$this->user_id' ";
			//echo $sc;
			$qc = $this->db->query($sc);
			if($qc->num_rows() == 0)
			{
				redirect('/');
			}
		}
		$sct = "select nik,tanggal_mulai,tanggal_selesai from d_ketidakhadiran_tkk where id = '$id' ";
		$qct = $this->db->query($sct);
		if($qct->num_rows() > 0)
		{
			$r = $qct->row();
			$nik = $r->nik;
			$tanggal_mulai = $r->tanggal_mulai;
		//	echo $tanggal_mulai.'</br>';
			$tanggal_selesai = $r->tanggal_selesai;
			$selisih = (strtotime($tanggal_selesai) - strtotime($tanggal_mulai))/(60*60*24);
			for($i=0;$i<=$selisih;$i++)
			{
				$penambah = $i*60*60*24;
				$tanggal_time = strtotime($tanggal_mulai) + $penambah;
				$tanggal = date('Y-m-d',$tanggal_time);
				$tgl = date('j',$tanggal_time);
				$bln = date('n',$tanggal_time);
				$thn = date('Y',$tanggal_time);
				$now = date('Y-m-d');
				$now_time = strtotime($now);
				if($tanggal_time > $now_time)
				{
					$sd = "delete from d_absen_harian_tkk where nik = '$nik' and tanggal = '$tgl' and bulan = '$bln' and tahun = '$thn' ";
				//	echo $sd;
					$this->db->query($sd);
					$sdr = "delete from d_rekap_keuangan_harian_tkk where nik = '$nik' and tanggal = '$tanggal' ";
				//	echo $sdr;
					$this->db->query($sdr);
				}
								
			}
		
			$sc = "select nama_file from d_ketidakhadiran_bukti_tkk where id_ketidakhadiran_tkk = '$id' ";
			$q = $this->db->query($sc);
			foreach($q->result_array() as $ri)
			{
				unlink('assets/bukti_tkk/'.$ri['nama_file']);	
			}
			$d1 = "delete from d_ketidakhadiran_bukti_tkk where id_ketidakhadiran_tkk = '$id' ";
			$this->db->query($d1);
			
			//hapus data ketidakhadiran	
			$s = "delete from d_ketidakhadiran_tkk where id = '$id' ";
			$this->db->query($s);
			input_log($this->user_id,'hapus konfirmasi manual tkk dengan id:'.$id);
			redirect('/log/manual_tkk');
		}
	}
	
	public function view_bukti_tkk()
	{
		$id = $this->uri->segment(3);
		$s = "select * from d_ketidakhadiran_bukti_tkk where id_ketidakhadiran_tkk = '$id' ";
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			foreach($q->result() as $row)
			{				
				echo '&nbsp; <img src="/assets/bukti_tkk/'.$row->nama_file.'"></br>';
				echo '<hr>';
			}
			
		}
		else
		{
			echo '<img src="/assets/bukti_tkk/noimage.png">';
		}
	}
	
	public function hapus_bukti_tkk()
	{
		$id = $this->uri->segment(3);
		if($this->groupid != 1)
		{
			$sc = "select id from d_ketidakhadiran_tkk where id='$id' and added_by = '$this->user_id' ";
			//echo $sc;
			$qc = $this->db->query($sc);
			if($qc->num_rows() == 0)
			{
				redirect('/');
			}
		}
		$s = "select * from d_ketidakhadiran_bukti_tkk where id = '$id' ";
		$ri = $this->db->query($s)->row_array();
		unlink('assets/bukti_tkk/'.$ri['nama_file']);	
		$d = "delete from d_ketidakhadiran_bukti_tkk where id = '$id' ";
		$this->db->query($d);
		redirect('/log/edit_manual_tkk/'.$ri['id_ketidakhadiran_tkk']);
		
	}
	
	public function cari_nik()
	{
		$where = "";
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		$q = $this->db->escape_str($_REQUEST['q']);
		$s = "select a.nik,a.nama,
					CONCAT(a.nik,
							'-',
							a.nama,
							'-',
							CASE KUNKOM
								   WHEN 10 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' 	)
								   WHEN 11 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' 	) 
								   ELSE (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,4) = concat(kuntp,kunkom) and mid(kunker,5,8) = '00000000' 	)
							END
							)  AS niknama
				FROM d_tkk a
				where 1=1
				";
		if(!empty($s_biro)) 
		{
			$select = $s_biro;
			if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$where = " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$where = " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$where = " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$where = " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$where = " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
		}
		$s .= $where;
		$s .=" and (a.nama like '%".$q."%' OR a.nik like '%".$q."%' )";
		//echo $s;
		$q = $this->db->query($s);
		$res = array();
		foreach($q->result_array() as $r)
		{
			$res[] = $r;
		}
		echo json_encode($res);
	}
	
	public function view_identitas()
	{
		$biro = substr($this->s_biro,0,4);
		$NIP = $_REQUEST['NIP'];
		$sql_id = "select a.nip,
							a.nama,
							(select ngolru from referensi_golongan_ruang where kgolru = a.kgolru) as golongan,
							a.jabatan,
								CASE KUNKOM
								   WHEN 10 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' 	)
								   WHEN 11 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' 	) 
								   ELSE (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,4) = concat(kuntp,kunkom) and mid(kunker,5,8) = '00000000' 	)
								END as skpd
							from d_pegawai a 
							WHERE a.NIP = '$NIP'
							
							";
		if(isset($biro) && $biro!='')
		{
			$sql_id .=" and concat(kuntp,kunkom) = '$biro' ";
		}
		//echo $sql_id.'</br>';
		$query_id = $this->db->query($sql_id);
		$result = '';
		if($query_id->num_rows() > 0)
		{
			$row = $query_id->row_array();
			$result .='<div class="row">
								<div class="col-xs-4"><label>Nama</label></div>
								<div class="col-xs-8">
									<a>'.$row['nama'].'</a>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-4"><label>Golongan</label></div>
								<div class="col-xs-8">
									<a>'.$row['golongan'].'</a>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-4"><label>Jabatan</label></div>
								<div class="col-xs-8">
									<a>'.$row['jabatan'].'</a>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-4"><label>SKPD</label></div>
								<div class="col-xs-8">
									<a>'.$row['skpd'].'</a>
								</div>
							</div>
							
							';
							
		}
		else
		{
			$result .='<div class="row">
								<div class="col-xs-6"><label>Nama Lengkap</label></div>
								<div class="col-xs-6">
									
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6"><label>SKPD</label></div>
								<div class="col-xs-6">
									
								</div>
							</div>
							';
		}
		echo $result;
	}
	
	public function view_identitas_tkk()
	{
		$biro = substr($this->s_biro,0,4);
		$nik = $_REQUEST['nik'];
		$sql_id = "select a.nik,
							a.nama,
								CASE KUNKOM
								   WHEN 10 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' 	)
								   WHEN 11 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' 	) 
								   ELSE (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,4) = concat(kuntp,kunkom) and mid(kunker,5,8) = '00000000' 	)
								END as skpd
							from d_tkk a 
							WHERE a.nik = '$nik'
							
							";
		if(isset($biro) && $biro!='')
		{
			$sql_id .=" and concat(kuntp,kunkom) = '$biro' ";
		}
		//echo $sql_id.'</br>';
		$query_id = $this->db->query($sql_id);
		$result = '';
		if($query_id->num_rows() > 0)
		{
			$row = $query_id->row_array();
			$result .='<div class="row">
								<div class="col-xs-4"><label>Nama</label></div>
								<div class="col-xs-8">
									<a>'.$row['nama'].'</a>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-4"><label>SKPD</label></div>
								<div class="col-xs-8">
									<a>'.$row['skpd'].'</a>
								</div>
							</div>
							
							';
							
		}
		else
		{
			$result .='<div class="row">
								<div class="col-xs-6"><label>Nama Lengkap</label></div>
								<div class="col-xs-6">
									
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6"><label>SKPD</label></div>
								<div class="col-xs-6">
									
								</div>
							</div>
							';
		}
		echo $result;
	}
	
	function update_absen($id)
	{
		$update_by = $this->ion_auth->user()->row()->id;
		$s = "select a.*,
					(select persentase from referensi_persentase_ketidakhadiran where id = a.jenis_ketidakhadiran) as persentase
				from d_ketidakhadiran a
				where a.id = '$id' ";
		$r = $this->db->query($s)->row();
		$nip = $r->nip;
		$tanggal_mulai = $r->tanggal_mulai;
	//	echo $tanggal_mulai.'</br>';
		$tanggal_selesai = $r->tanggal_selesai;
	//	echo $tanggal_selesai.'</br>';
		$jenis_ketidakhadiran = $r->jenis_ketidakhadiran;
		$persentase = $r->persentase;
		$selisih = (strtotime($tanggal_selesai) - strtotime($tanggal_mulai))/(60*60*24);
		if($selisih == 0)
		{
			$cl = 0;
			if($jenis_ketidakhadiran == 6)
			{
						if($cl<2)
						{
							$persentase = 0;
						}
			}
			
			$tgl = date('j',strtotime($tanggal_mulai));
			$bln = date('n',strtotime($tanggal_mulai));
			$thn = date('Y',strtotime($tanggal_mulai));
			$update_date = date('Y-m-d H:i:s');
			$update_by = $this->userid;
			$st = "select a.*,
								b.*
						from d_pegawai_tipe_jadwal a
						left join ref_tipe_jadwal b on(a.tipe_jadwal = b.id)
						where nip = '$nip' 
						
						";
			$rst = $this->db->query($st)->row_array();
			if($rst['shift'] == 0)
			{
				//$libur = $this->cek_libur($tanggal_mulai);
				$libur = $this->cek_libur($tanggal_mulai,$rst['sabtu_masuk']);
				if($libur == 0)
				{
					$sc = "select id from d_absen_harian where nip = '$nip' and tanggal = '$tgl' and bulan = '$bln' and tahun = '$thn' ";
					$qc = $this->db->query($sc);
					if($qc->num_rows() > 0)
					{
						
						$id_absen = $qc->row()->id;
						$su = "update d_absen_harian 
								set jenis_tidak_masuk = '$jenis_ketidakhadiran',
									potongan_tidak_masuk = '$persentase',
									persentase_potongan = '$persentase',
									update_date = '$update_date',
									update_by = '$update_by',
									libur = '0',
									edited = '1'
								where id = '$id_absen'
								";
						//echo $su.'</br>';
						$this->db->query($su);
					}
					else
					{
						$si = "insert into d_absen_harian
												set nip = '$nip',
													tanggal = '$tgl',
													bulan = '$bln',
													tahun = '$thn',
													jenis_tidak_masuk = '$jenis_ketidakhadiran',
													potongan_tidak_masuk = '$persentase',
													persentase_potongan = '$persentase',
													added_date = '$update_date',
													added_by = '$update_by',
													libur = '0',
													edited = '1'
												";
						//echo $si.'</br>';
						$qi = $this->db->query($si);
					}
					
					$sc = "select id from d_rekap_keuangan_harian where nip = '$nip' and tanggal = '$tanggal_mulai' ";
					$qc = $this->db->query($sc);
					if($qc->num_rows() > 0)
					{
						$id_uang = $qc->row()->id;
						$tpp = $this->get_tpp($nip);
						$si = "update d_rekap_keuangan_harian 
								set total_tpp = '$tpp[total_tpp]',
									tpp_statis = '$tpp[tpp_statis]',
									tpp_dinamis = '$tpp[tpp_dinamis]',
									pemotongan_per_hari = '$persentase',
									update_date = '$update_date',	
									update_by = '$update_by'
								where id = '$id_uang'
						";
						$this->db->query($si);
					}
					else
					{
						$tpp = $this->get_tpp($nip);
						$si = "insert into d_rekap_keuangan_harian 
								set tanggal = '$tanggal_mulai',
									nip = '$nip',
									total_tpp = '$tpp[total_tpp]',
									tpp_statis = '$tpp[tpp_statis]',
									tpp_dinamis = '$tpp[tpp_dinamis]',
									pemotongan_per_hari = '$persentase',
									update_date = '$update_date',	
									update_by = '$update_by'				
						";
						$this->db->query($si);
					}
					
				}
				elseif($libur == 1)
				{
					$sc = "select id from d_absen_harian where nip = '$nip' and tanggal = '$tgl' and bulan = '$bln' and tahun = '$thn' ";
					$qc = $this->db->query($sc);
					if($qc->num_rows() > 0)
					{
						$id_absen = $qc->row()->id;
						$su = "update d_absen_harian 
								set libur = '1',
									update_date = '$update_date',
									update_by = '$update_by',
									edited = '1'
								where id = '$id_absen'
								";
						$this->db->query($su);
					}
					else
					{
						
						$si = "insert into d_absen_harian
												set nip = '$nip',
													tanggal = '$tgl',
													bulan = '$bln',
													tahun = '$thn',
													libur = '1',
													update_date = '$update_date',
													update_by = '$update_by'
												";
						//echo $si.'</br>';
						$qi = $this->db->query($si);
					}
					
					$sc = "select id from d_rekap_keuangan_harian where nip = '$nip' and tanggal = '$tanggal_mulai' ";
					$qc = $this->db->query($sc);
					if($qc->num_rows() > 0)
					{
						$id_uang = $qc->row()->id;
						$tpp = $this->get_tpp($nip);
						$si = "update d_rekap_keuangan_harian 
								set total_tpp = '$tpp[total_tpp]',
									tpp_statis = '$tpp[tpp_statis]',
									tpp_dinamis = '$tpp[tpp_dinamis]',
									pemotongan_per_hari = '0',
									update_date = '$update_date',	
									update_by = 'system'
								where id = '$id_uang'
						";
						$this->db->query($si);
					}
					else
					{
						$tpp = $this->get_tpp($nip);
						$si = "insert into d_rekap_keuangan_harian 
								set tanggal = '$tanggal_mulai',
									nip = '$nip',
									total_tpp = '$tpp[total_tpp]',
									tpp_statis = '$tpp[tpp_statis]',
									tpp_dinamis = '$tpp[tpp_dinamis]',
									pemotongan_per_hari = '0',
									update_date = '$update_date',	
									update_by = 'system'					
						";
						$this->db->query($si);
					}
					
				}
				
			}
			else
			{
					$sc = "select a.* 
						from d_jadwal_lembur a
						where nip = '$nip'
						and tanggal = '$tanggal_mulai'
						";
					$qc = $this->db->query($sc);
					if($qc->num_rows() > 0)
					{
						$rc = $qc->row();
						if($rc->is_libur == 1)
						{						
							$sch = "select id from d_absen_harian where nip = '$nip' and tanggal = '$tgl' and bulan = '$bln' and tahun = '$thn' ";
							$qch = $this->db->query($sch);
							if($qch->num_rows() > 0)
							{
								$id_absen = $qch->row()->id;
								$su = "update d_absen_harian 
										set jenis_tidak_masuk = '$jenis_ketidakhadiran',
											potongan_tidak_masuk = '0',
											persentase_potongan = '0',
											update_date = '$update_date',
											update_by = '$update_by',
											libur = '0',
											edited = '1'
										where id = '$id_absen'
										";
								//echo $su.'</br>';
								$this->db->query($su);
							}
							else
							{
								$si = "insert into d_absen_harian
														set nip = '$nip',
															tanggal = '$tgl',
															bulan = '$bln',
															tahun = '$thn',
															jenis_tidak_masuk = '$jenis_ketidakhadiran',
															potongan_tidak_masuk = '0',
															persentase_potongan = '0',
															added_date = '$update_date',
															added_by = '$update_by',
															libur = '0',
															edited = '1'
														";
								//echo $si.'</br>';
								$qi = $this->db->query($si);
							}
							
							$sck = "select id from d_rekap_keuangan_harian where nip = '$nip' and tanggal = '$tanggal_mulai' ";
							$qck = $this->db->query($sck);
							if($qck->num_rows() > 0)
							{
								$id_uang = $qck->row()->id;
								$tpp = $this->get_tpp($nip);
								$si = "update d_rekap_keuangan_harian 
										set total_tpp = '$tpp[total_tpp]',
											tpp_statis = '$tpp[tpp_statis]',
											tpp_dinamis = '$tpp[tpp_dinamis]',
											pemotongan_per_hari = '0',
											update_date = '$update_date',	
											update_by = '$update_by'
										where id = '$id_uang'
								";
								$this->db->query($si);
							}
							else
							{
								$tpp = $this->get_tpp($nip);
								$si = "insert into d_rekap_keuangan_harian 
										set tanggal = '$tanggal_mulai',
											nip = '$nip',
											total_tpp = '$tpp[total_tpp]',
											tpp_statis = '$tpp[tpp_statis]',
											tpp_dinamis = '$tpp[tpp_dinamis]',
											pemotongan_per_hari = '0',
											update_date = '$update_date',	
											update_by = '$update_by'				
								";
								$this->db->query($si);
							}
							
						}
						else
						{
							$sch = "select id from d_absen_harian where nip = '$nip' and tanggal = '$tgl' and bulan = '$bln' and tahun = '$thn' ";
							$qch = $this->db->query($sch);
							if($qch->num_rows() > 0)
							{
								$id_absen = $qch->row()->id;
								$su = "update d_absen_harian 
										set jenis_tidak_masuk = '$jenis_ketidakhadiran',
											potongan_tidak_masuk = '$persentase',
											persentase_potongan = '$persentase',
											update_date = '$update_date',
											update_by = '$update_by',
											libur = '0',
											edited = '1'
										where id = '$id_absen'
										";
								//echo $su.'</br>';
								$this->db->query($su);
							}
							else
							{
								$si = "insert into d_absen_harian
														set nip = '$nip',
															tanggal = '$tgl',
															bulan = '$bln',
															tahun = '$thn',
															jenis_tidak_masuk = '$jenis_ketidakhadiran',
															potongan_tidak_masuk = '$persentase',
															persentase_potongan = '$persentase',
															added_date = '$update_date',
															added_by = '$update_by',
															libur = '0',
															edited = '1'
														";
								//echo $si.'</br>';
								$qi = $this->db->query($si);
							}
							
							$sck = "select id from d_rekap_keuangan_harian where nip = '$nip' and tanggal = '$tanggal_mulai' ";
							$qck = $this->db->query($sck);
							if($qck->num_rows() > 0)
							{
								$id_uang = $qck->row()->id;
								$tpp = $this->get_tpp($nip);
								$si = "update d_rekap_keuangan_harian 
										set total_tpp = '$tpp[total_tpp]',
											tpp_statis = '$tpp[tpp_statis]',
											tpp_dinamis = '$tpp[tpp_dinamis]',
											pemotongan_per_hari = '$persentase',
											update_date = '$update_date',	
											update_by = '$update_by'
										where id = '$id_uang'
								";
								$this->db->query($si);
							}
							else
							{
								$tpp = $this->get_tpp($nip);
								$si = "insert into d_rekap_keuangan_harian 
										set tanggal = '$tanggal_mulai',
											nip = '$nip',
											total_tpp = '$tpp[total_tpp]',
											tpp_statis = '$tpp[tpp_statis]',
											tpp_dinamis = '$tpp[tpp_dinamis]',
											pemotongan_per_hari = '$persentase',
											update_date = '$update_date',	
											update_by = '$update_by'				
								";
								$this->db->query($si);
							}
							
						}						
						
					}
					else
					{
						$sch = "select id from d_absen_harian where nip = '$nip' and tanggal = '$tgl' and bulan = '$bln' and tahun = '$thn' ";
							$qch = $this->db->query($sch);
							if($qch->num_rows() > 0)
							{
								$id_absen = $qch->row()->id;
								$su = "update d_absen_harian 
										set jenis_tidak_masuk = '$jenis_ketidakhadiran',
											potongan_tidak_masuk = '$persentase',
											persentase_potongan = '$persentase',
											update_date = '$update_date',
											update_by = '$update_by',
											libur = '0',
											edited = '1'
										where id = '$id_absen'
										";
								//echo $su.'</br>';
								$this->db->query($su);
							}
							else
							{
								$si = "insert into d_absen_harian
														set nip = '$nip',
															tanggal = '$tgl',
															bulan = '$bln',
															tahun = '$thn',
															jenis_tidak_masuk = '$jenis_ketidakhadiran',
															potongan_tidak_masuk = '$persentase',
															persentase_potongan = '$persentase',
															added_date = '$update_date',
															added_by = '$update_by',
															libur = '0',
															edited = '1'
														";
								//echo $si.'</br>';
								$qi = $this->db->query($si);
							}
							
							$sck = "select id from d_rekap_keuangan_harian where nip = '$nip' and tanggal = '$tanggal_mulai' ";
							$qck = $this->db->query($sck);
							if($qck->num_rows() > 0)
							{
								$id_uang = $qck->row()->id;
								$tpp = $this->get_tpp($nip);
								$si = "update d_rekap_keuangan_harian 
										set total_tpp = '$tpp[total_tpp]',
											tpp_statis = '$tpp[tpp_statis]',
											tpp_dinamis = '$tpp[tpp_dinamis]',
											pemotongan_per_hari = '$persentase',
											update_date = '$update_date',	
											update_by = '$update_by'
										where id = '$id_uang'
								";
								$this->db->query($si);
							}
							else
							{
								$tpp = $this->get_tpp($nip);
								$si = "insert into d_rekap_keuangan_harian 
										set tanggal = '$tanggal_mulai',
											nip = '$nip',
											total_tpp = '$tpp[total_tpp]',
											tpp_statis = '$tpp[tpp_statis]',
											tpp_dinamis = '$tpp[tpp_dinamis]',
											pemotongan_per_hari = '$persentase',
											update_date = '$update_date',	
											update_by = '$update_by'				
								";
								$this->db->query($si);
							}
							
					}
					
			}
		}
		else
		{
			$cl = 0;
			for($i=0;$i<=$selisih;$i++)
			{
				
				$persentase = $r->persentase;
				if($jenis_ketidakhadiran == 6)
					{
						if($cl<2)
						{
							$persentase = 0;
						}
					}
				$penambah = $i*60*60*24;
				$tanggal_time = strtotime($tanggal_mulai) + $penambah;
				$tanggal = date('Y-m-d',$tanggal_time);
				$tgl = date('j',$tanggal_time);
				$bln = date('n',$tanggal_time);
				$thn = date('Y',$tanggal_time);
				$update_date = date('Y-m-d H:i:s');
				$st = "select a.*,
								b.*
						from d_pegawai_tipe_jadwal a
						left join ref_tipe_jadwal b on(a.tipe_jadwal = b.id)
						where nip = '$nip' 
						";
				$rst = $this->db->query($st)->row_array();
				if($rst['shift'] == 0)
				{
					$libur = $this->cek_libur($tanggal,$rst['sabtu_masuk']);
					if($libur == 0)
					{
						$sc = "select id from d_absen_harian where nip = '$nip' and tanggal = '$tgl' and bulan = '$bln' and tahun = '$thn' ";
					//	echo $sc.'</br>';
						$qc = $this->db->query($sc);
						if($qc->num_rows() > 0)
						{
							
							$id_absen = $qc->row()->id;
							$su = "update d_absen_harian 
									set jenis_tidak_masuk = '$jenis_ketidakhadiran',
										potongan_tidak_masuk = '$persentase',
										persentase_potongan = '$persentase',
										update_date = '$update_date',
										update_by = 'system'
									where id = '$id_absen'
									";
							//echo $su.'</br>';
							$this->db->query($su);
						}
						else
						{
							$si = "insert into d_absen_harian
													set nip = '$nip',
														tanggal = '$tgl',
														bulan = '$bln',
														tahun = '$thn',
														jenis_tidak_masuk = '$jenis_ketidakhadiran',
														potongan_tidak_masuk = '$persentase',
														persentase_potongan = '$persentase',
														added_date = '$update_date',
														added_by = 'system'
													";
						//	echo $si.'</br>';
							$qi = $this->db->query($si);
						}
						
						$sc = "select id from d_rekap_keuangan_harian where nip = '$nip' and tanggal = '$tanggal' ";
						$qc = $this->db->query($sc);
						if($qc->num_rows() > 0)
						{
							$id_uang = $qc->row()->id;
							$tpp = $this->get_tpp($nip);
							$si = "update d_rekap_keuangan_harian 
									set total_tpp = '$tpp[total_tpp]',
										tpp_statis = '$tpp[tpp_statis]',
										tpp_dinamis = '$tpp[tpp_dinamis]',
										pemotongan_per_hari = '$persentase',
										update_date = '$update_date',	
										update_by = 'system'
									where id = '$id_uang'
							";
							//echo $si.'</br>';
							$this->db->query($si);
						}
						else
						{
							$tpp = $this->get_tpp($nip);
							$si = "insert into d_rekap_keuangan_harian 
									set tanggal = '$tanggal',
										nip = '$nip',
										total_tpp = '$tpp[total_tpp]',
										tpp_statis = '$tpp[tpp_statis]',
										tpp_dinamis = '$tpp[tpp_dinamis]',
										pemotongan_per_hari = '$persentase',
										update_date = '$update_date',	
										update_by = 'system'					
							";
							$this->db->query($si);
						}
						
						$cl++;
					}
					elseif($libur == 1)
					{
						$sc = "select id from d_absen_harian where nip = '$nip' and tanggal = '$tgl' and bulan = '$bln' and tahun = '$thn' ";
					//	echo $sc.'</br>';
						$qc = $this->db->query($sc);
						if($qc->num_rows() > 0)
						{
							$id_absen = $qc->row()->id;
							$su = "update d_absen_harian 
									set libur = '1',
										update_date = '$update_date',
										update_by = 'system'
									where id = '$id_absen'
									";
							//echo $su.'</br>';
							$this->db->query($su);
						}
						else
						{
							$si = "insert into d_absen_harian
													set nip = '$nip',
														tanggal = '$tgl',
														bulan = '$bln',
														tahun = '$thn',
														libur = '1',
														added_date = '$update_date',
														added_by = 'system'
													";
					//		echo $si.'</br>';
							$qi = $this->db->query($si);
						}
						
						$sc = "select id from d_rekap_keuangan_harian where nip = '$nip' and tanggal = '$tanggal' ";
						$qc = $this->db->query($sc);
						if($qc->num_rows() > 0)
						{
							$id_uang = $qc->row()->id;
							$tpp = $this->get_tpp($nip);
							$si = "update d_rekap_keuangan_harian 
									set total_tpp = '$tpp[total_tpp]',
										tpp_statis = '$tpp[tpp_statis]',
										tpp_dinamis = '$tpp[tpp_dinamis]',
										pemotongan_per_hari = '0',
										update_date = '$update_date',	
										update_by = 'system'
									where id = '$id_uang'
							";
							//echo $si.'</br>';
							$this->db->query($si);
						}
						else
						{
							$tpp = $this->get_tpp($nip);
							$si = "insert into d_rekap_keuangan_harian 
									set tanggal = '$tanggal',
										nip = '$nip',
										total_tpp = '$tpp[total_tpp]',
										tpp_statis = '$tpp[tpp_statis]',
										tpp_dinamis = '$tpp[tpp_dinamis]',
										pemotongan_per_hari = '0',
										update_date = '$update_date',	
										update_by = 'system'					
							";
							$this->db->query($si);
						}
					
					}
				}
				else
				{
					$sc = "select a.* 
						from d_jadwal_lembur a
						where nip = '$nip'
						and tanggal = '$tanggal'
						";
					$qc = $this->db->query($sc);
					if($qc->num_rows() > 0)
					{
						$rc = $qc->row();
						if($rc->is_libur == 1)
						{						
							$sch = "select id from d_absen_harian where nip = '$nip' and tanggal = '$tgl' and bulan = '$bln' and tahun = '$thn' ";
							$qch = $this->db->query($sch);
							if($qch->num_rows() > 0)
							{
								$id_absen = $qch->row()->id;
								$su = "update d_absen_harian 
										set jenis_tidak_masuk = '$jenis_ketidakhadiran',
											potongan_tidak_masuk = '0',
											persentase_potongan = '0',
											update_date = '$update_date',
											update_by = '$update_by',
											libur = '0',
											edited = '1'
										where id = '$id_absen'
										";
								//echo $su.'</br>';
								$this->db->query($su);
							}
							else
							{
								$si = "insert into d_absen_harian
														set nip = '$nip',
															tanggal = '$tgl',
															bulan = '$bln',
															tahun = '$thn',
															jenis_tidak_masuk = '$jenis_ketidakhadiran',
															potongan_tidak_masuk = '0',
															persentase_potongan = '0',
															added_date = '$update_date',
															added_by = '$update_by',
															libur = '0',
															edited = '1'
														";
								//echo $si.'</br>';
								$qi = $this->db->query($si);
							}
							
							$sck = "select id from d_rekap_keuangan_harian where nip = '$nip' and tanggal = '$tanggal' ";
							$qck = $this->db->query($sck);
							if($qck->num_rows() > 0)
							{
								$id_uang = $qck->row()->id;
								$tpp = $this->get_tpp($nip);
								$si = "update d_rekap_keuangan_harian 
										set total_tpp = '$tpp[total_tpp]',
											tpp_statis = '$tpp[tpp_statis]',
											tpp_dinamis = '$tpp[tpp_dinamis]',
											pemotongan_per_hari = '0',
											update_date = '$update_date',	
											update_by = '$update_by'
										where id = '$id_uang'
								";
								$this->db->query($si);
							}
							else
							{
								$tpp = $this->get_tpp($nip);
								$si = "insert into d_rekap_keuangan_harian 
										set tanggal = '$tanggal',
											nip = '$nip',
											total_tpp = '$tpp[total_tpp]',
											tpp_statis = '$tpp[tpp_statis]',
											tpp_dinamis = '$tpp[tpp_dinamis]',
											pemotongan_per_hari = '0',
											update_date = '$update_date',	
											update_by = '$update_by'				
								";
								$this->db->query($si);
							}
							
						}
						else
						{
							$sch = "select id from d_absen_harian where nip = '$nip' and tanggal = '$tgl' and bulan = '$bln' and tahun = '$thn' ";
							$qch = $this->db->query($sch);
							if($qch->num_rows() > 0)
							{
								$id_absen = $qch->row()->id;
								$su = "update d_absen_harian 
										set jenis_tidak_masuk = '$jenis_ketidakhadiran',
											potongan_tidak_masuk = '$persentase',
											persentase_potongan = '$persentase',
											update_date = '$update_date',
											update_by = '$update_by',
											libur = '0',
											edited = '1'
										where id = '$id_absen'
										";
								//echo $su.'</br>';
								$this->db->query($su);
							}
							else
							{
								$si = "insert into d_absen_harian
														set nip = '$nip',
															tanggal = '$tgl',
															bulan = '$bln',
															tahun = '$thn',
															jenis_tidak_masuk = '$jenis_ketidakhadiran',
															potongan_tidak_masuk = '$persentase',
															persentase_potongan = '$persentase',
															added_date = '$update_date',
															added_by = '$update_by',
															libur = '0',
															edited = '1'
														";
								//echo $si.'</br>';
								$qi = $this->db->query($si);
							}
							
							$sck = "select id from d_rekap_keuangan_harian where nip = '$nip' and tanggal = '$tanggal' ";
							$qck = $this->db->query($sck);
							if($qck->num_rows() > 0)
							{
								$id_uang = $qck->row()->id;
								$tpp = $this->get_tpp($nip);
								$si = "update d_rekap_keuangan_harian 
										set total_tpp = '$tpp[total_tpp]',
											tpp_statis = '$tpp[tpp_statis]',
											tpp_dinamis = '$tpp[tpp_dinamis]',
											pemotongan_per_hari = '$persentase',
											update_date = '$update_date',	
											update_by = '$update_by'
										where id = '$id_uang'
								";
								$this->db->query($si);
							}
							else
							{
								$tpp = $this->get_tpp($nip);
								$si = "insert into d_rekap_keuangan_harian 
										set tanggal = '$tanggal',
											nip = '$nip',
											total_tpp = '$tpp[total_tpp]',
											tpp_statis = '$tpp[tpp_statis]',
											tpp_dinamis = '$tpp[tpp_dinamis]',
											pemotongan_per_hari = '$persentase',
											update_date = '$update_date',	
											update_by = '$update_by'				
								";
								$this->db->query($si);
							}
							
							$cl++;
						}						
						
					}
					else
					{
						$sch = "select id from d_absen_harian where nip = '$nip' and tanggal = '$tgl' and bulan = '$bln' and tahun = '$thn' ";
							$qch = $this->db->query($sch);
							if($qch->num_rows() > 0)
							{
								$id_absen = $qch->row()->id;
								$su = "update d_absen_harian 
										set jenis_tidak_masuk = '$jenis_ketidakhadiran',
											potongan_tidak_masuk = '$persentase',
											persentase_potongan = '$persentase',
											update_date = '$update_date',
											update_by = '$update_by',
											libur = '0',
											edited = '1'
										where id = '$id_absen'
										";
								//echo $su.'</br>';
								$this->db->query($su);
							}
							else
							{
								$si = "insert into d_absen_harian
														set nip = '$nip',
															tanggal = '$tgl',
															bulan = '$bln',
															tahun = '$thn',
															jenis_tidak_masuk = '$jenis_ketidakhadiran',
															potongan_tidak_masuk = '$persentase',
															persentase_potongan = '$persentase',
															added_date = '$update_date',
															added_by = '$update_by',
															libur = '0',
															edited = '1'
														";
								//echo $si.'</br>';
								$qi = $this->db->query($si);
							}
							
							$sck = "select id from d_rekap_keuangan_harian where nip = '$nip' and tanggal = '$tanggal' ";
							$qck = $this->db->query($sck);
							if($qck->num_rows() > 0)
							{
								$id_uang = $qck->row()->id;
								$tpp = $this->get_tpp($nip);
								$si = "update d_rekap_keuangan_harian 
										set total_tpp = '$tpp[total_tpp]',
											tpp_statis = '$tpp[tpp_statis]',
											tpp_dinamis = '$tpp[tpp_dinamis]',
											pemotongan_per_hari = '$persentase',
											update_date = '$update_date',	
											update_by = '$update_by'
										where id = '$id_uang'
								";
								$this->db->query($si);
							}
							else
							{
								$tpp = $this->get_tpp($nip);
								$si = "insert into d_rekap_keuangan_harian 
										set tanggal = '$tanggal',
											nip = '$nip',
											total_tpp = '$tpp[total_tpp]',
											tpp_statis = '$tpp[tpp_statis]',
											tpp_dinamis = '$tpp[tpp_dinamis]',
											pemotongan_per_hari = '$persentase',
											update_date = '$update_date',	
											update_by = '$update_by'				
								";
								$this->db->query($si);
							}
						
						$cl++;
					}
					
				}
			}
		}
	}
	
	function update_absen_tkk($id)
	{
		$update_by = $this->userid;
		$s = "select a.*,
					(select persentase from referensi_persentase_ketidakhadiran where id = a.jenis_ketidakhadiran) as persentase
				from d_ketidakhadiran_tkk a
				where a.id = '$id' ";
		$r = $this->db->query($s)->row();
		$nik = $r->nik;
		$tanggal_mulai = $r->tanggal_mulai;
	//	echo $tanggal_mulai.'</br>';
		$tanggal_selesai = $r->tanggal_selesai;
	//	echo $tanggal_selesai.'</br>';
		$jenis_ketidakhadiran = $r->jenis_ketidakhadiran;
		$persentase = $r->persentase;
		$selisih = (strtotime($tanggal_selesai) - strtotime($tanggal_mulai))/(60*60*24);
		if($selisih == 0)
		{
			$cl=0;
			if($jenis_ketidakhadiran == 26)
			{
						if($cl<2)
						{
							$persentase = 0;
						}
			}
			$tgl = date('j',strtotime($tanggal_mulai));
			$bln = date('n',strtotime($tanggal_mulai));
			$thn = date('Y',strtotime($tanggal_mulai));
			$update_date = date('Y-m-d H:i:s');
			$update_by = $this->userid;
			$st = "select a.*,
								b.*
						from d_pegawai_tipe_jadwal_tkk a
						left join ref_tipe_jadwal b on(a.tipe_jadwal = b.id)
						where nik = '$nik' 
						
						";
			$rst = $this->db->query($st)->row_array();
			if($rst['shift'] == 0)
			{
				//$libur = $this->cek_libur($tanggal_mulai);
				$libur = $this->cek_libur($tanggal_mulai,$rst['sabtu_masuk']);
				if($libur == 0)
				{
					$sc = "select id from d_absen_harian_tkk where nik = '$nik' and tanggal = '$tgl' and bulan = '$bln' and tahun = '$thn' ";
					$qc = $this->db->query($sc);
					if($qc->num_rows() > 0)
					{
						$id_absen = $qc->row()->id;
						$su = "update d_absen_harian_tkk 
								set jenis_tidak_masuk = '$jenis_ketidakhadiran',
									potongan_tidak_masuk = '$persentase',
									persentase_potongan = '$persentase',
									update_date = '$update_date',
									update_by = '$update_by',
									edited = '1'
								where id = '$id_absen'
								";
						$this->db->query($su);
					}
					else
					{
						$si = "insert into d_absen_harian_tkk
												set nik = '$nik',
													tanggal = '$tgl',
													bulan = '$bln',
													tahun = '$thn',
													jenis_tidak_masuk = '$jenis_ketidakhadiran',
													potongan_tidak_masuk = '$persentase',
													persentase_potongan = '$persentase',
													update_date = '$update_date',
													update_by = '$update_by',
													edited = '1'
												";
						//echo $si.'</br>';
						$qi = $this->db->query($si);
					}
					
					$sc = "select id from d_rekap_keuangan_harian_tkk where nik = '$nik' and tanggal = '$tanggal_mulai' ";
					$qc = $this->db->query($sc);
					if($qc->num_rows() > 0)
					{
						$id_uang = $qc->row()->id;
						$tpp = $this->get_tpp_tkk($nik);
						$si = "update d_rekap_keuangan_harian 
								set total_tpp = '$tpp[total_tpp]',
									tpp_statis = '$tpp[tpp_statis]',
									tpp_dinamis = '$tpp[tpp_dinamis]',
									pemotongan_per_hari = '$persentase',
									update_date = '$update_date',	
									update_by = 'system'
								where id = '$id_uang'
						";
						$this->db->query($si);
					}
					else
					{
						$tpp = $this->get_tpp_tkk($nik);
						$si = "insert into d_rekap_keuangan_harian_tkk 
								set tanggal = '$tanggal_mulai',
									nik = '$nik',
									total_tpp = '$tpp[total_tpp]',
									tpp_statis = '$tpp[tpp_statis]',
									tpp_dinamis = '$tpp[tpp_dinamis]',
									pemotongan_per_hari = '$persentase',
									update_date = '$update_date',	
									update_by = 'system'					
						";
						$this->db->query($si);
					}
					
					$cl++;
				}
				elseif($libur == 1)
				{
					$persentase = 0;
					$sc = "select id from d_absen_harian_tkk where nik = '$nik' and tanggal = '$tgl' and bulan = '$bln' and tahun = '$thn' ";
					$qc = $this->db->query($sc);
					if($qc->num_rows() > 0)
					{
						$id_absen = $qc->row()->id;
						$su = "update d_absen_harian_tkk 
								set libur = '1',
									potongan_tidak_masuk = '$persentase',
									persentase_potongan = '$persentase',
									update_date = '$update_date',
									update_by = '$update_by',
									edited = '1'
								where id = '$id_absen'
								";
						$this->db->query($su);
					}
					else
					{
						$si = "insert into d_absen_harian_tkk
												set nik = '$nik',
													tanggal = '$tgl',
													bulan = '$bln',
													tahun = '$thn',
													libur = '1',
													update_date = '$update_date',
													update_by = '$update_by',
													edited = '1'
												";
						//echo $si.'</br>';
						$qi = $this->db->query($si);
					}
					
					$sc = "select id from d_rekap_keuangan_harian_tkk where nik = '$nik' and tanggal = '$tanggal_mulai' ";
					$qc = $this->db->query($sc);
					if($qc->num_rows() > 0)
					{
						$id_uang = $qc->row()->id;
						$tpp = $this->get_tpp_tkk($nik);
						$si = "update d_rekap_keuangan_harian 
								set total_tpp = '$tpp[total_tpp]',
									tpp_statis = '$tpp[tpp_statis]',
									tpp_dinamis = '$tpp[tpp_dinamis]',
									pemotongan_per_hari = '0',
									update_date = '$update_date',	
									update_by = 'system'
								where id = '$id_uang'
						";
						$this->db->query($si);
					}
					else
					{
						$tpp = $this->get_tpp_tkk($nik);
						$si = "insert into d_rekap_keuangan_harian_tkk 
								set tanggal = '$tanggal_mulai',
									nik = '$nik',
									total_tpp = '$tpp[total_tpp]',
									tpp_statis = '$tpp[tpp_statis]',
									tpp_dinamis = '$tpp[tpp_dinamis]',
									pemotongan_per_hari = '0',
									update_date = '$update_date',	
									update_by = 'system'					
						";
						$this->db->query($si);
					}
					
				}
				
			}
			else
			{
				$sc = "select a.* 
						from d_jadwal_lembur_tkk a
						where nik = '$nik'
						and tanggal = '$tanggal_mulai'
						";
					$qc = $this->db->query($sc);
					if($qc->num_rows() > 0)
					{
						$rc = $qc->row();
						if($rc->is_libur == 1)
						{						
							$sch = "select id from d_absen_harian_tkk where nik = '$nik' and tanggal = '$tgl' and bulan = '$bln' and tahun = '$thn' ";
							$qch = $this->db->query($sch);
							if($qch->num_rows() > 0)
							{
								$id_absen = $qch->row()->id;
								$su = "update d_absen_harian_tkk 
										set jenis_tidak_masuk = '$jenis_ketidakhadiran',
											potongan_tidak_masuk = '0',
											persentase_potongan = '0',
											update_date = '$update_date',
											update_by = '$update_by',
											libur = '0',
											edited = '1'
										where id = '$id_absen'
										";
								//echo $su.'</br>';
								$this->db->query($su);
							}
							else
							{
								$si = "insert into d_absen_harian_tkk
														set nik = '$nik',
															tanggal = '$tgl',
															bulan = '$bln',
															tahun = '$thn',
															jenis_tidak_masuk = '$jenis_ketidakhadiran',
															potongan_tidak_masuk = '0',
															persentase_potongan = '0',
															added_date = '$update_date',
															added_by = '$update_by',
															libur = '0',
															edited = '1'
														";
								//echo $si.'</br>';
								$qi = $this->db->query($si);
							}
							
							$sck = "select id from d_rekap_keuangan_harian_tkk where nip = '$nip' and tanggal = '$tanggal_mulai' ";
							$qck = $this->db->query($sck);
							if($qck->num_rows() > 0)
							{
								$id_uang = $qck->row()->id;
								$tpp = $this->get_tpp_tkk($nik);
								$si = "update d_rekap_keuangan_harian_tkk 
										set total_tpp = '$tpp[total_tpp]',
											tpp_statis = '$tpp[tpp_statis]',
											tpp_dinamis = '$tpp[tpp_dinamis]',
											pemotongan_per_hari = '0',
											update_date = '$update_date',	
											update_by = '$update_by'
										where id = '$id_uang'
								";
								$this->db->query($si);
							}
							else
							{
								$tpp = $this->get_tpp_tkk($nik);
								$si = "insert into d_rekap_keuangan_harian_tkk 
										set tanggal = '$tanggal_mulai',
											nik = '$nik',
											total_tpp = '$tpp[total_tpp]',
											tpp_statis = '$tpp[tpp_statis]',
											tpp_dinamis = '$tpp[tpp_dinamis]',
											pemotongan_per_hari = '0',
											update_date = '$update_date',	
											update_by = '$update_by'				
								";
								$this->db->query($si);
							}
							
						}
						else
						{
							$sch = "select id from d_absen_harian_tkk where nik = '$nik' and tanggal = '$tgl' and bulan = '$bln' and tahun = '$thn' ";
							$qch = $this->db->query($sch);
							if($qch->num_rows() > 0)
							{
								$id_absen = $qch->row()->id;
								$su = "update d_absen_harian_tkk 
										set jenis_tidak_masuk = '$jenis_ketidakhadiran',
											potongan_tidak_masuk = '$persentase',
											persentase_potongan = '$persentase',
											update_date = '$update_date',
											update_by = '$update_by',
											libur = '0',
											edited = '1'
										where id = '$id_absen'
										";
								//echo $su.'</br>';
								$this->db->query($su);
							}
							else
							{
								$si = "insert into d_absen_harian_tkk
														set nik = '$nik',
															tanggal = '$tgl',
															bulan = '$bln',
															tahun = '$thn',
															jenis_tidak_masuk = '$jenis_ketidakhadiran',
															potongan_tidak_masuk = '$persentase',
															persentase_potongan = '$persentase',
															added_date = '$update_date',
															added_by = '$update_by',
															libur = '0',
															edited = '1'
														";
								//echo $si.'</br>';
								$qi = $this->db->query($si);
							}
							
							$sck = "select id from d_rekap_keuangan_harian_tkk where nik = '$nik' and tanggal = '$tanggal_mulai' ";
							$qck = $this->db->query($sck);
							if($qck->num_rows() > 0)
							{
								$id_uang = $qck->row()->id;
								$tpp = $this->get_tpp_tkk($nik);
								$si = "update d_rekap_keuangan_harian_tkk 
										set total_tpp = '$tpp[total_tpp]',
											tpp_statis = '$tpp[tpp_statis]',
											tpp_dinamis = '$tpp[tpp_dinamis]',
											pemotongan_per_hari = '$persentase',
											update_date = '$update_date',	
											update_by = '$update_by'
										where id = '$id_uang'
								";
								$this->db->query($si);
							}
							else
							{
								$tpp = $this->get_tpp_tkk($nik);
								$si = "insert into d_rekap_keuangan_harian_tkk 
										set tanggal = '$tanggal_mulai',
											nik = '$nik',
											total_tpp = '$tpp[total_tpp]',
											tpp_statis = '$tpp[tpp_statis]',
											tpp_dinamis = '$tpp[tpp_dinamis]',
											pemotongan_per_hari = '$persentase',
											update_date = '$update_date',	
											update_by = '$update_by'				
								";
								$this->db->query($si);
							}
						
							$cl++;
						}						
						
					}
					else
					{
						$sch = "select id from d_absen_harian_tkk where nik = '$nik' and tanggal = '$tgl' and bulan = '$bln' and tahun = '$thn' ";
							$qch = $this->db->query($sch);
							if($qch->num_rows() > 0)
							{
								$id_absen = $qch->row()->id;
								$su = "update d_absen_harian_tkk 
										set jenis_tidak_masuk = '$jenis_ketidakhadiran',
											potongan_tidak_masuk = '$persentase',
											persentase_potongan = '$persentase',
											update_date = '$update_date',
											update_by = '$update_by',
											libur = '0',
											edited = '1'
										where id = '$id_absen'
										";
								//echo $su.'</br>';
								$this->db->query($su);
							}
							else
							{
								$si = "insert into d_absen_harian_tkk
														set nik = '$nik',
															tanggal = '$tgl',
															bulan = '$bln',
															tahun = '$thn',
															jenis_tidak_masuk = '$jenis_ketidakhadiran',
															potongan_tidak_masuk = '$persentase',
															persentase_potongan = '$persentase',
															added_date = '$update_date',
															added_by = '$update_by',
															libur = '0',
															edited = '1'
														";
								//echo $si.'</br>';
								$qi = $this->db->query($si);
							}
							
							$sck = "select id from d_rekap_keuangan_harian_tkk where nik = '$nik' and tanggal = '$tanggal_mulai' ";
							$qck = $this->db->query($sck);
							if($qck->num_rows() > 0)
							{
								$id_uang = $qck->row()->id;
								$tpp = $this->get_tpp_tkk($nik);
								$si = "update d_rekap_keuangan_harian_tkk 
										set total_tpp = '$tpp[total_tpp]',
											tpp_statis = '$tpp[tpp_statis]',
											tpp_dinamis = '$tpp[tpp_dinamis]',
											pemotongan_per_hari = '$persentase',
											update_date = '$update_date',	
											update_by = '$update_by'
										where id = '$id_uang'
								";
								$this->db->query($si);
							}
							else
							{
								$tpp = $this->get_tpp_tkk($nik);
								$si = "insert into d_rekap_keuangan_harian_tkk 
										set tanggal = '$tanggal_mulai',
											nik = '$nik',
											total_tpp = '$tpp[total_tpp]',
											tpp_statis = '$tpp[tpp_statis]',
											tpp_dinamis = '$tpp[tpp_dinamis]',
											pemotongan_per_hari = '$persentase',
											update_date = '$update_date',	
											update_by = '$update_by'				
								";
								$this->db->query($si);
							}
						
						$cl++;						
					}
			}
		}
		else
		{
			$cl=0;
			for($i=0;$i<=$selisih;$i++)
			{
				$persentase = $r->persentase;
				if($jenis_ketidakhadiran == 26)
				{
						if($cl<2)
						{
							$persentase = 0;
						}
				}
				//echo 'persen'.$persentase.'</br>';
				$penambah = $i*60*60*24;
				$tanggal_time = strtotime($tanggal_mulai) + $penambah;
				$tanggal = date('Y-m-d',$tanggal_time);
				$tgl = date('j',$tanggal_time);
				$bln = date('n',$tanggal_time);
				$thn = date('Y',$tanggal_time);
				$update_date = date('Y-m-d H:i:s');
				$st = "select a.*,
								b.*
						from d_pegawai_tipe_jadwal_tkk a
						left join ref_tipe_jadwal b on(a.tipe_jadwal = b.id)
						where nik = '$nik' 
						
						";
				$rst = $this->db->query($st)->row_array();
				if($rst['shift'] == 0)
				{
					//$libur = $this->cek_libur($tanggal);
					$libur = $this->cek_libur($tanggal,$rst['sabtu_masuk']);
					
					if($libur == 0)
					{
						$sc = "select id from d_absen_harian_tkk where nik = '$nik' and tanggal = '$tgl' and bulan = '$bln' and tahun = '$thn' ";
					//	echo $sc.'</br>';
						$qc = $this->db->query($sc);
						if($qc->num_rows() > 0)
						{
							$id_absen = $qc->row()->id;
							$su = "update d_absen_harian_tkk 
									set jenis_tidak_masuk = '$jenis_ketidakhadiran',
										potongan_tidak_masuk = '$persentase',
										persentase_potongan = '$persentase',
										update_date = '$update_date',
										update_by = '$update_by'
									where id = '$id_absen'
									";
							//echo $su.'</br>';
							$this->db->query($su);
						}
						else
						{
							$si = "insert into d_absen_harian_tkk
													set nik = '$nik',
														tanggal = '$tgl',
														bulan = '$bln',
														tahun = '$thn',
														jenis_tidak_masuk = '$jenis_ketidakhadiran',
														potongan_tidak_masuk = '$persentase',
														persentase_potongan = '$persentase',
														update_date = '$update_date',
														update_by = '$update_by'
													";
						//	echo $si.'</br>';
							$qi = $this->db->query($si);
						}
						
						$sc = "select id from d_rekap_keuangan_harian_tkk where nik = '$nik' and tanggal = '$tanggal' ";
						$qc = $this->db->query($sc);
						if($qc->num_rows() > 0)
						{
							$id_uang = $qc->row()->id;
							$tpp = $this->get_tpp_tkk($nik);
							$si = "update d_rekap_keuangan_harian_tkk 
									set total_tpp = '$tpp[total_tpp]',
										tpp_statis = '$tpp[tpp_statis]',
										tpp_dinamis = '$tpp[tpp_dinamis]',
										pemotongan_per_hari = '$persentase',
										update_date = '$update_date',	
										update_by = 'system'
									where id = '$id_uang'
							";
							//echo $si.'</br>';
							$this->db->query($si);
						}
						else
						{
							$tpp = $this->get_tpp_tkk($nik);
							$si = "insert into d_rekap_keuangan_harian_tkk 
									set tanggal = '$tanggal',
										nik = '$nik',
										total_tpp = '$tpp[total_tpp]',
										tpp_statis = '$tpp[tpp_statis]',
										tpp_dinamis = '$tpp[tpp_dinamis]',
										pemotongan_per_hari = '$persentase',
										update_date = '$update_date',	
										update_by = 'system'					
							";
							$this->db->query($si);
						}
						
						$cl++;
						echo $cl;
					}
					elseif($libur == 1)
					{
						$sc = "select id from d_absen_harian_tkk where nik = '$nik' and tanggal = '$tgl' and bulan = '$bln' and tahun = '$thn' ";
					//	echo $sc.'</br>';
						$qc = $this->db->query($sc);
						if($qc->num_rows() > 0)
						{
							$id_absen = $qc->row()->id;
							$su = "update d_absen_harian_tkk 
									set libur = '1',
										update_date = '$update_date',
										update_by = '$update_by',
										edited = '1'
									where id = '$id_absen'
									";
							//echo $su.'</br>';
							$this->db->query($su);
						}
						else
						{
							$si = "insert into d_absen_harian_tkk
													set nik = '$nik',
														tanggal = '$tgl',
														bulan = '$bln',
														tahun = '$thn',
														libur = '1',
														update_date = '$update_date',
														update_by = '$update_by',
														edited = '1'
													";
						//	echo $si.'</br>';
							$qi = $this->db->query($si);
						}
						
						$sc = "select id from d_rekap_keuangan_harian_tkk where nik = '$nik' and tanggal = '$tanggal' ";
						$qc = $this->db->query($sc);
						if($qc->num_rows() > 0)
						{
							$id_uang = $qc->row()->id;
							$tpp = $this->get_tpp_tkk($nik);
							$si = "update d_rekap_keuangan_harian_tkk 
									set total_tpp = '$tpp[total_tpp]',
										tpp_statis = '$tpp[tpp_statis]',
										tpp_dinamis = '$tpp[tpp_dinamis]',
										pemotongan_per_hari = '0',
										update_date = '$update_date',	
										update_by = 'system'
									where id = '$id_uang'
							";
							//echo $si.'</br>';
							$this->db->query($si);
						}
						else
						{
							$tpp = $this->get_tpp_tkk($nik);
							$si = "insert into d_rekap_keuangan_harian_tkk 
									set tanggal = '$tanggal',
										nik = '$nik',
										total_tpp = '$tpp[total_tpp]',
										tpp_statis = '$tpp[tpp_statis]',
										tpp_dinamis = '$tpp[tpp_dinamis]',
										pemotongan_per_hari = '0',
										update_date = '$update_date',	
										update_by = 'system'					
							";
							$this->db->query($si);
						}
					}
				}
				else
				{
					$sc = "select a.* 
						from d_jadwal_lembur_tkk a
						where nik = '$nik'
						and tanggal = '$tanggal'
						";
					$qc = $this->db->query($sc);
					if($qc->num_rows() > 0)
					{
						$rc = $qc->row();
						if($rc->is_libur == 1)
						{						
							$sch = "select id from d_absen_harian_tkk where nik = '$nik' and tanggal = '$tgl' and bulan = '$bln' and tahun = '$thn' ";
							$qch = $this->db->query($sch);
							if($qch->num_rows() > 0)
							{
								$id_absen = $qch->row()->id;
								$su = "update d_absen_harian_tkk 
										set jenis_tidak_masuk = '$jenis_ketidakhadiran',
											potongan_tidak_masuk = '0',
											persentase_potongan = '0',
											update_date = '$update_date',
											update_by = '$update_by',
											libur = '0',
											edited = '1'
										where id = '$id_absen'
										";
								//echo $su.'</br>';
								$this->db->query($su);
							}
							else
							{
								$si = "insert into d_absen_harian_tkk
														set nik = '$nik',
															tanggal = '$tgl',
															bulan = '$bln',
															tahun = '$thn',
															jenis_tidak_masuk = '$jenis_ketidakhadiran',
															potongan_tidak_masuk = '0',
															persentase_potongan = '0',
															added_date = '$update_date',
															added_by = '$update_by',
															libur = '0',
															edited = '1'
														";
								//echo $si.'</br>';
								$qi = $this->db->query($si);
							}
							
							$sck = "select id from d_rekap_keuangan_harian_tkk where nip = '$nip' and tanggal = '$tanggal' ";
							$qck = $this->db->query($sck);
							if($qck->num_rows() > 0)
							{
								$id_uang = $qck->row()->id;
								$tpp = $this->get_tpp_tkk($nik);
								$si = "update d_rekap_keuangan_harian_tkk 
										set total_tpp = '$tpp[total_tpp]',
											tpp_statis = '$tpp[tpp_statis]',
											tpp_dinamis = '$tpp[tpp_dinamis]',
											pemotongan_per_hari = '0',
											update_date = '$update_date',	
											update_by = '$update_by'
										where id = '$id_uang'
								";
								$this->db->query($si);
							}
							else
							{
								$tpp = $this->get_tpp_tkk($nik);
								$si = "insert into d_rekap_keuangan_harian_tkk 
										set tanggal = '$tanggal',
											nik = '$nik',
											total_tpp = '$tpp[total_tpp]',
											tpp_statis = '$tpp[tpp_statis]',
											tpp_dinamis = '$tpp[tpp_dinamis]',
											pemotongan_per_hari = '0',
											update_date = '$update_date',	
											update_by = '$update_by'				
								";
								$this->db->query($si);
							}
							
						}
						else
						{
							$sch = "select id from d_absen_harian_tkk where nik = '$nik' and tanggal = '$tgl' and bulan = '$bln' and tahun = '$thn' ";
							$qch = $this->db->query($sch);
							if($qch->num_rows() > 0)
							{
								$id_absen = $qch->row()->id;
								$su = "update d_absen_harian_tkk 
										set jenis_tidak_masuk = '$jenis_ketidakhadiran',
											potongan_tidak_masuk = '$persentase',
											persentase_potongan = '$persentase',
											update_date = '$update_date',
											update_by = '$update_by',
											libur = '0',
											edited = '1'
										where id = '$id_absen'
										";
								//echo $su.'</br>';
								$this->db->query($su);
							}
							else
							{
								$si = "insert into d_absen_harian_tkk
														set nik = '$nik',
															tanggal = '$tgl',
															bulan = '$bln',
															tahun = '$thn',
															jenis_tidak_masuk = '$jenis_ketidakhadiran',
															potongan_tidak_masuk = '$persentase',
															persentase_potongan = '$persentase',
															added_date = '$update_date',
															added_by = '$update_by',
															libur = '0',
															edited = '1'
														";
								//echo $si.'</br>';
								$qi = $this->db->query($si);
							}
							
							$sck = "select id from d_rekap_keuangan_harian_tkk where nik = '$nik' and tanggal = '$tanggal' ";
							$qck = $this->db->query($sck);
							if($qck->num_rows() > 0)
							{
								$id_uang = $qck->row()->id;
								$tpp = $this->get_tpp_tkk($nik);
								$si = "update d_rekap_keuangan_harian_tkk 
										set total_tpp = '$tpp[total_tpp]',
											tpp_statis = '$tpp[tpp_statis]',
											tpp_dinamis = '$tpp[tpp_dinamis]',
											pemotongan_per_hari = '$persentase',
											update_date = '$update_date',	
											update_by = '$update_by'
										where id = '$id_uang'
								";
								$this->db->query($si);
							}
							else
							{
								$tpp = $this->get_tpp_tkk($nik);
								$si = "insert into d_rekap_keuangan_harian_tkk 
										set tanggal = '$tanggal',
											nik = '$nik',
											total_tpp = '$tpp[total_tpp]',
											tpp_statis = '$tpp[tpp_statis]',
											tpp_dinamis = '$tpp[tpp_dinamis]',
											pemotongan_per_hari = '$persentase',
											update_date = '$update_date',	
											update_by = '$update_by'				
								";
								$this->db->query($si);
							}
							
							$cl++;
						}						
						
					}
					else
					{
						$sch = "select id from d_absen_harian_tkk where nik = '$nik' and tanggal = '$tgl' and bulan = '$bln' and tahun = '$thn' ";
							$qch = $this->db->query($sch);
							if($qch->num_rows() > 0)
							{
								$id_absen = $qch->row()->id;
								$su = "update d_absen_harian_tkk 
										set jenis_tidak_masuk = '$jenis_ketidakhadiran',
											potongan_tidak_masuk = '$persentase',
											persentase_potongan = '$persentase',
											update_date = '$update_date',
											update_by = '$update_by',
											libur = '0',
											edited = '1'
										where id = '$id_absen'
										";
								//echo $su.'</br>';
								$this->db->query($su);
							}
							else
							{
								$si = "insert into d_absen_harian_tkk
														set nik = '$nik',
															tanggal = '$tgl',
															bulan = '$bln',
															tahun = '$thn',
															jenis_tidak_masuk = '$jenis_ketidakhadiran',
															potongan_tidak_masuk = '$persentase',
															persentase_potongan = '$persentase',
															added_date = '$update_date',
															added_by = '$update_by',
															libur = '0',
															edited = '1'
														";
								//echo $si.'</br>';
								$qi = $this->db->query($si);
							}
							
							$sck = "select id from d_rekap_keuangan_harian_tkk where nik = '$nik' and tanggal = '$tanggal' ";
							$qck = $this->db->query($sck);
							if($qck->num_rows() > 0)
							{
								$id_uang = $qck->row()->id;
								$tpp = $this->get_tpp_tkk($nik);
								$si = "update d_rekap_keuangan_harian_tkk 
										set total_tpp = '$tpp[total_tpp]',
											tpp_statis = '$tpp[tpp_statis]',
											tpp_dinamis = '$tpp[tpp_dinamis]',
											pemotongan_per_hari = '$persentase',
											update_date = '$update_date',	
											update_by = '$update_by'
										where id = '$id_uang'
								";
								$this->db->query($si);
							}
							else
							{
								$tpp = $this->get_tpp_tkk($nik);
								$si = "insert into d_rekap_keuangan_harian_tkk 
										set tanggal = '$tanggal',
											nik = '$nik',
											total_tpp = '$tpp[total_tpp]',
											tpp_statis = '$tpp[tpp_statis]',
											tpp_dinamis = '$tpp[tpp_dinamis]',
											pemotongan_per_hari = '$persentase',
											update_date = '$update_date',	
											update_by = '$update_by'				
								";
								$this->db->query($si);
							}
						
						$cl++;						
					}
			
				}
			}
		}
	}
	
	public function rekap_kehadiran_bulanan()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		
		$data = array(
				'title' => 'Data Rekap Kehadiran Bulanan',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'sssubunit' => $this->session->userdata('sssubunit_rekap'),
				'jnsjab' => $this->session->userdata('jnsjab_rekap'),
				'keselon' => $this->session->userdata('keselon_rekap'),
				'nama' => $this->session->userdata('nama_rekap'),
				'reqnip' => $this->session->userdata('reqnip_rekap'),
				'bulan' => $this->session->userdata('bulan_rekap'),
				'tahun' => $this->session->userdata('tahun_rekap'),
		);
		$this->load->view('rekap_kehadiran_bulanan',$data);
		input_log($this->user_id,'Lihat kehadiran bulanan pns');
	}
	
	public function get_rekap_kehadiran_bulanan()
	{
		//print_r($_REQUEST);
		$order = $_REQUEST['order'][0];
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/log/rekap_kehadiran_bulanan');
		//$aColumns = array( 'nip', 'nip','nama', 'nip','nip');
		$sssubunit = $this->input->post('sssubunit');
		$this->session->set_userdata('sssubunit_rekap',$sssubunit);
		$jnsjab = $this->input->post('jnsjab');
		$this->session->set_userdata('jnsjab_rekap',$jnsjab);
		$keselon = $this->input->post('keselon');
		$this->session->set_userdata('keselon_rekap',$keselon);
		$nama = $this->input->post('nama');
		$this->session->set_userdata('nama_rekap',$nama);
		$reqnip = $this->input->post('reqnip');
		$this->session->set_userdata('reqnip_rekap',$reqnip);
		$bulan = $this->input->post('bulan');
		$this->session->set_userdata('bulan_rekap',$bulan);
		$tahun = $this->input->post('tahun');
		$this->session->set_userdata('tahun_rekap',$tahun);
		
		$sSearch = '';
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		if(isset($select) && $select != '')
		{
			if(!empty($s_biro)) 
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
			}
			else
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			}
		}
		
		if(isset($nama) && $nama != '')
		{
			$sSearch .= " and (nama like '%".$nama."%') ";
		}
		if(isset($reqnip) && $reqnip != '')
		{
			$sSearch .= " and (a.nip = '".$reqnip."') ";
		}
		if(isset($jnsjab) && $jnsjab != '0')
		{
			$sSearch .= " and (jnsjab = '".$jnsjab."') ";
		}
		if(isset($keselon) && $keselon != '0')
		{
			$sSearch .= " and (keselon = '".$keselon."') ";
		}
		/* if(isset($bulan) && $bulan != '')
		{
			$sSearch .= " and (b.bulan = '".$bulan."') ";
		}
		if(isset($tahun) && $tahun != '')
		{
			$sSearch .= " and (b.tahun = '".$tahun."') ";
		} */
	
		//limit
		if ( isset( $_REQUEST['start'] ) && $_REQUEST['length'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_REQUEST['start'] ).", ".
				intval( $_REQUEST['length'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "`".$aColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]."` ".
						($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		//$sOrder = " order by ".$aColumns[$order['column']]." ".$order['dir']." ";
		$s0 = "select count(a.nip) as jml 
				from d_pegawai a
				where 1 = 1
				and a.non_aktif = 0
				";
		$s0 .= $sSearch;
		
		$sheet_total = $this->db->query($s0)->row();
		$sheet_total = $sheet_total->jml;
	
		$s1 = "SELECT DISTINCT a.nip,a.nama,
									a.keselon,
									a.kgolru,
									a.kuntp,
									a.kunkom,
									a.kununit,
									a.tanggal_lahir,
									a.jabatan,
									a.kunker,
									(select neselon from referensi_eselon where keselon = a.keselon) as eselon,
									(select ngolru from referensi_golongan_ruang where KGOLRU = a.KGOLRU) as golongan,
									(select pangkat from referensi_golongan_ruang where KGOLRU = a.KGOLRU) as pangkat,									
									(select sum(keterlambatan_masuk) from d_absen_harian where nip = a.nip and bulan = '$bulan' and tahun = '$tahun' ) as jumlah_telat_masuk,
									(select sum(kecepatan_keluar) from d_absen_harian where nip = a.nip and bulan = '$bulan' and tahun = '$tahun' ) as jumlah_pulang_cepat,
									(select count(jenis_tidak_masuk) from d_absen_harian where nip = a.nip and bulan = '$bulan' and tahun = '$tahun' and jenis_tidak_masuk != 0) as jumlah_tidak_masuk,
									(select sum(persentase_potongan) from d_absen_harian where nip = a.nip and bulan = '$bulan' and tahun = '$tahun' ) as jumlah_potongan
				FROM d_pegawai a
				WHERE 1=1
				and a.non_aktif = 0";
		$s1 .= $sSearch;
		$s1 .= " order by keselon asc, kgolru desc, nip asc ";
		
		$s1 .= $sLimit;
	//	echo $s1;
		$sheet1 = $this->db->query($s1);
		$result = array();
		$i=0;
		$no=1;
		foreach($sheet1->result() as $sheet)
		{
			$result[$i]['0'] = $no;
			$result[$i]['1'] = $sheet->nip.' </br> '.$sheet->nama;
			$result[$i]['2'] = $sheet->pangkat.' </br> '.$sheet->golongan;
			$result[$i]['3'] = $sheet->jabatan.' </br> '.$sheet->eselon;
			$result[$i]['4'] = $this->get_skpd($sheet->kunker);	
			$result[$i]['5'] =	number_format($sheet->jumlah_telat_masuk,2,',','.');	
			$result[$i]['6'] =	number_format($sheet->jumlah_pulang_cepat,2,',','.');	
			$result[$i]['7'] =	$sheet->jumlah_tidak_masuk;	
			$result[$i]['8'] = $sheet->jumlah_potongan;	
			$no++;
			$i++;
		}
		
		$data = $result;
		$results = array(
			"iTotalRecords" => ($sheet_total),
			"iTotalDisplayRecords" => ($sheet_total),
			"aaData"=>$data);
			
		echo json_encode($results);
	}
	
	public function ekspor_rekap_kehadiran_bulanan_pns_pdf()
	{
		set_time_limit(0);
		$this->load->library('pdf');
		$pdf = $this->pdf->load('format => A-2 L');
		//print_r($_REQUEST);
		$sssubunit = $this->input->post('sssubunit_pdf');
		$jnsjab = $this->input->post('jnsjab_pdf');
		$keselon = $this->input->post('keselon_pdf');
		$nama = $this->input->post('nama_pdf');
		$reqnip = $this->input->post('reqnip_pdf');
		$bulan = $this->input->post('bulan_pdf');
		$tahun = $this->input->post('tahun_pdf');
		$sSearch = '';
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		if(isset($select) && $select != '')
		{
			if(!empty($s_biro)) 
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
			}
			else
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			}
		}
		
		if(isset($nama) && $nama != '')
		{
			$sSearch .= " and (nama like '%".$nama."%') ";
		}
		if(isset($reqnip) && $reqnip != '')
		{
			$sSearch .= " and (a.nip = '".$reqnip."') ";
		}
		if(isset($jnsjab) && $jnsjab != '0' && $jnsjab != '')
		{
			$sSearch .= " and (jnsjab = '".$jnsjab."') ";
		}
		if(isset($keselon) && $keselon != '0' && $keselon != '')
		{
			$sSearch .= " and (keselon = '".$keselon."') ";
		}
		
		$s1 = "SELECT DISTINCT a.nip,a.nama,
									a.keselon,
									a.kgolru,
									a.kuntp,
									a.kunkom,
									a.kununit,
									a.tanggal_lahir,
									a.jabatan,
									(select neselon from referensi_eselon where keselon = a.keselon) as eselon,
									(select ngolru from referensi_golongan_ruang where KGOLRU = a.KGOLRU) as golongan,
									(select pangkat from referensi_golongan_ruang where KGOLRU = a.KGOLRU) as pangkat,
									CASE KUNKOM
									   WHEN 01 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,4) = concat(kuntp,kunkom) and mid(kunker,3,8) = '00000000' )
									   WHEN 10 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' )
									   WHEN 11 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' ) 
									   ELSE (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,4) = concat(kuntp,kunkom) and mid(kunker,5,8) = '00000000' )
									END as skpd,
									(select sum(keterlambatan_masuk) from d_absen_harian where nip = a.nip and bulan = '$bulan' and tahun = '$tahun' ) as jumlah_telat_masuk,
									(select sum(kecepatan_keluar) from d_absen_harian where nip = a.nip and bulan = '$bulan' and tahun = '$tahun' ) as jumlah_pulang_cepat,
									(select count(jenis_tidak_masuk) from d_absen_harian where nip = a.nip and bulan = '$bulan' and tahun = '$tahun' and jenis_tidak_masuk != 0) as jumlah_tidak_masuk,
									(select sum(persentase_potongan) from d_absen_harian where nip = a.nip and bulan = '$bulan' and tahun = '$tahun' ) as jumlah_potongan
				FROM d_pegawai a
				WHERE 1=1
				and a.non_aktif = 0";
		$s1 .= $sSearch;
		$s1 .= " order by keselon asc, kgolru desc, nip asc ";
	//	echo $s1;
		$q= $this->db->query($s1);
		$data['q'] = $q;
		$data['unit_kerja'] = $this->get_unit_kerja($sssubunit);
		$data['bulan'] = $bulan;
		$data['tahun'] = $tahun;
		$html = $this->load->view('cetak_rekap_kehadiran_bulanan_pns',$data,true);
		input_log($this->user_id,'cetak kehadiran bulanan pns');
		$pdf->WriteHTML($html);
		$pdf->Output();
			
	}
	
	public function rekap_kehadiran_bulanan_tkk()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		
		$data = array(
				'title' => 'Data Rekap Kehadiran Bulanan TKK',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'sssubunit' => $this->session->userdata('sssubunit_rekap'),
				'nama' => $this->session->userdata('nama_rekap'),
				'nik' => $this->session->userdata('nik'),
				'bulan' => $this->session->userdata('bulan_rekap'),
				'tahun' => $this->session->userdata('tahun_rekap'),
		);
		$this->load->view('rekap_kehadiran_bulanan_tkk',$data);
		input_log($this->user_id,'Lihat rekap kehadiran bulanan tkk');
	}
	
	public function get_rekap_kehadiran_bulanan_tkk()
	{
		//print_r($_REQUEST);
		$order = $_REQUEST['order'][0];
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/log/rekap_kehadiran_bulanan_tkk');
		//$aColumns = array( 'nip', 'nip','nama', 'nip','nip');
		$sssubunit = $this->input->post('sssubunit');
		$this->session->set_userdata('sssubunit_rekap',$sssubunit);
		$nama = $this->input->post('nama');
		$this->session->set_userdata('nama_rekap',$nama);
		$nik = $this->input->post('nik');
		$this->session->set_userdata('nik_rekap',$nik);
		$bulan = $this->input->post('bulan');
		$this->session->set_userdata('bulan_rekap',$bulan);
		$tahun = $this->input->post('tahun');
		$this->session->set_userdata('tahun_rekap',$tahun);
		
		$sSearch = '';
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		if(isset($select) && $select != '')
		{
			if(!empty($s_biro)) 
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
			}
			else
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			}
		}
		
		if(isset($nama) && $nama != '')
		{
			$sSearch .= " and (nama like '%".$nama."%') ";
		}
		if(isset($nik) && $nik != '')
		{
			$sSearch .= " and (a.nik = '".$nik."') ";
		}
			
		//limit
		if ( isset( $_REQUEST['start'] ) && $_REQUEST['length'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_REQUEST['start'] ).", ".
				intval( $_REQUEST['length'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "`".$aColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]."` ".
						($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		//$sOrder = " order by ".$aColumns[$order['column']]." ".$order['dir']." ";
		$s0 = "select count(a.nik) as jml 
				from d_tkk a
				LEFT JOIN d_absen_harian_tkk b on(a.nik = b.nik)
				where 1 = 1
				and a.non_aktif = 0
				";
		$s0 .= $sSearch;
		
		$sheet_total = $this->db->query($s0)->row();
		$sheet_total = $sheet_total->jml;
	
		$s1 = "SELECT DISTINCT a.nik,a.nama,
								concat(kuntp,kunkom,kununit,rincunit1,rincunit2,rincunit3) as kunker,
								(select sum(keterlambatan_masuk) from d_absen_harian_tkk where nik = a.nik and bulan = '$bulan' and tahun = '$tahun' ) as jumlah_telat_masuk,
								(select sum(kecepatan_keluar) from d_absen_harian_tkk where nik = a.nik and bulan = '$bulan' and tahun = '$tahun' ) as jumlah_pulang_cepat,
								(select count(jenis_tidak_masuk) from d_absen_harian_tkk where nik = a.nik and bulan = '$bulan' and tahun = '$tahun' and jenis_tidak_masuk != 0) as jumlah_tidak_masuk,
								(select sum(persentase_potongan) from d_absen_harian_tkk where nik = a.nik and bulan = '$bulan' and tahun = '$tahun' ) as jumlah_potongan
				FROM d_tkk a
				WHERE 1=1
				and a.non_aktif = 0";
		$s1 .= $sSearch;
		$s1 .= " order by nik desc ";
		
		$s1 .= $sLimit;
	//	echo $s1;
		$sheet1 = $this->db->query($s1);
		$result = array();
		$i=0;
		$no=1;
		foreach($sheet1->result() as $sheet)
		{
			
			$result[$i]['0'] = $no;
			$result[$i]['1'] = $sheet->nik.' </br>'.$sheet->nama;
			$result[$i]['2'] = $this->get_skpd($sheet->kunker);
			$result[$i]['3'] = number_format($sheet->jumlah_telat_masuk,2,',','.');
			$result[$i]['4'] = number_format($sheet->jumlah_pulang_cepat,2,',','.');
			$result[$i]['5'] = $sheet->jumlah_tidak_masuk;
			$result[$i]['6'] = $sheet->jumlah_potongan;
			$no++;
			$i++;
		}
		
		$data = $result;
		$results = array(
			"iTotalRecords" => ($sheet_total),
			"iTotalDisplayRecords" => ($sheet_total),
			"aaData"=>$data);
			
		echo json_encode($results);
	}
	
	public function ekspor_rekap_kehadiran_bulanan_tkk_pdf()
	{
		set_time_limit(0);
		$this->load->library('pdf');
		$pdf = $this->pdf->load('format => A-2 L');
		//print_r($_REQUEST);
		$sssubunit = $this->input->post('sssubunit_pdf');
		$nama = $this->input->post('nama_pdf');
		$nik = $this->input->post('nik_pdf');
		$bulan = $this->input->post('bulan_pdf');
		$tahun = $this->input->post('tahun_pdf');
		$sSearch = '';
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		if(isset($select) && $select != '')
		{
			if(!empty($s_biro)) 
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
			}
			else
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			}
		}
		
		if(isset($nama) && $nama != '')
		{
			$sSearch .= " and (nama like '%".$nama."%') ";
		}
		if(isset($nik) && $nik != '')
		{
			$sSearch .= " and (a.nik = '".$nik."') ";
		}
		
		$s1 = "SELECT DISTINCT a.nik,a.nama,
								CASE KUNKOM
								   WHEN 10 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' 	)
								   WHEN 11 then (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,6) = concat(kuntp,kunkom,kununit) and mid(kunker,7,6) = '000000' 	) 
								   ELSE (SELECT NUNKER FROM referensi_unit_kerja WHERE mid(kunker,1,4) = concat(kuntp,kunkom) and mid(kunker,5,8) = '00000000' 	)
								END as skpd,
								(select sum(keterlambatan_masuk) from d_absen_harian_tkk where nik = a.nik and bulan = '$bulan' and tahun = '$tahun' ) as jumlah_telat_masuk,
								(select sum(kecepatan_keluar) from d_absen_harian_tkk where nik = a.nik and bulan = '$bulan' and tahun = '$tahun' ) as jumlah_pulang_cepat,
								(select count(jenis_tidak_masuk) from d_absen_harian_tkk where nik = a.nik and bulan = '$bulan' and tahun = '$tahun' and jenis_tidak_masuk != 0) as jumlah_tidak_masuk,
								(select sum(persentase_potongan) from d_absen_harian_tkk where nik = a.nik and bulan = '$bulan' and tahun = '$tahun' ) as jumlah_potongan
				FROM d_tkk a
				WHERE 1=1
				and a.non_aktif = 0";
		$s1 .= $sSearch;
		$s1 .= " order by nik desc ";
	//	echo $s1;
		$q= $this->db->query($s1);
		$data['q'] = $q;
		$data['unit_kerja'] = $this->get_unit_kerja($sssubunit);
		$data['bulan'] = $bulan;
		$data['tahun'] = $tahun;
		$html = $this->load->view('cetak_rekap_kehadiran_bulanan_tkk',$data,true);
		input_log($this->user_id,'cetak rekap kehadiran bulanan tkk');
		$pdf->WriteHTML($html);
		$pdf->Output();
			
	}
	
	function get_skpd($kunker)
	{
		$kuntp = substr($kunker,0,2);
		$kunkom = substr($kunker,2,2);
		$kununit = substr($kunker,4,2);
		switch($kunkom)
		{
			case '10':
			case '11':
				$s = "select nunker from referensi_unit_kerja where substr(kunker,1,2) = '$kuntp' 
																and substr(kunker,3,2) = '$kunkom' 
																and substr(kunker,5,8) = '00000000'";
				$q = $this->db->query($s);
				if($q->num_rows() > 0)
				{
					$nunker = $q->row()->nunker;
				}
				else
				{
					$nunker = '';
				}
			break;
			
			default:
				$s = "select nunker from referensi_unit_kerja where substr(kunker,1,2) = '$kuntp' 
																and substr(kunker,3,2) = '$kunkom' 
																and substr(kunker,5,2) = '$kununit' 
																and substr(kunker,7,6) = '000000'
																";
				$q = $this->db->query($s);
				if($q->num_rows() > 0)
				{
					$nunker = $q->row()->nunker;
				}
				else
				{
					$nunker = '';
				}
			break;
		}
		return $nunker;
	}
	
	/* function cek_libur($hari)
	{
		$tgl = date('Y-m-d',strtotime($hari));
		$urutan_hari = date('w',strtotime($hari));
	//	echo 'urutan hari:'.$urutan_hari;
		$sc = "select * from d_hari_libur where tanggal = '$tgl' ";
	//	echo $sc;
		$qc = $this->db->query($sc);
		if($qc->num_rows() > 0)
		{
			$tanggal_merah = 1;
		}
		if((isset($tanggal_merah) && $tanggal_merah == 1) or ($urutan_hari == 0 || $urutan_hari == 6))
		{
			$libur = 1;
		}
		else
		{
			$libur = 0;
		}
	//	echo 'libur: '.$libur;
		return $libur;
	}
	 */
	 
	function cek_libur($tanggal_buat,$sabtu_masuk)
	{
		$tgl = date('Y-m-d',strtotime($tanggal_buat));
		$urutan_hari = date('w',strtotime($tanggal_buat));
		if($sabtu_masuk == 1)
		{
			if($urutan_hari == 0)
			{
				$akhir_pekan = 1;
			}
			else
			{
				$akhir_pekan = 0;
			}
		}
		else
		{
			if($urutan_hari == 0 || $urutan_hari == 6)
			{
				$akhir_pekan = 1;
			}
			else
			{
				$akhir_pekan = 0;
			}
		}
		$sc = "select * from d_hari_libur where tanggal = '$tgl' ";
		$qc = $this->db->query($sc);
		if($qc->num_rows() > 0)
		{
			$tanggal_merah = 1;
		}
		if((isset($tanggal_merah) && $tanggal_merah == 1) or ($akhir_pekan == 1 ))
		{
			$libur = 1;
		}
		else
		{
			$libur = 0;
		}
		return $libur;
	}
	
	function migrasi_bukti()
	{
		$s = "select * from d_ketidakhadiran where bukti != ''";
		$q = $this->db->query($s);
		foreach($q->result() as $r)
		{
			$si = "insert into d_ketidakhadiran_bukti
					set id_ketidakhadiran = '$r->id',
						nama_file = '$r->bukti'
						";
			echo $si.'</br>';
			$this->db->query($si);
		}
		
		$s = "select * from d_ketidakhadiran_tkk where bukti != ''";
		$q = $this->db->query($s);
		foreach($q->result() as $r)
		{
			$si = "insert into d_ketidakhadiran_bukti_tkk
					set id_ketidakhadiran_tkk = '$r->id',
						nama_file = '$r->bukti'
						";
			echo $si.'</br>';
			$this->db->query($si);
		}
	}
	
	//upload log
	public function upload()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		
		$data = array(
			'title' => 'Data Log Absensi Yang Diinput Manual',
			'subtitle' => '',
			'hak_akses' => $hak_akses,
			'sssubunit' => $this->session->userdata('sssubunit_log_upload'),
			'bulan' => $this->session->userdata('bulan_log_upload'),
			'tahun' => $this->session->userdata('tahun_log_upload'),
			'id_mesin' => $this->session->userdata('id_mesin_log_upload'),
			);
		$this->load->view('upload',$data);
		input_log($this->user_id,'Lihat Data Log Absensi Yang Diinput Manual');
	}
	
	public function get_upload()
	{
		//print_r($_REQUEST);
		$order = $_REQUEST['order'][0];
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/log/upload');
		$sssubunit = $this->input->post('sssubunit');
		$this->session->set_userdata('sssubunit_log_upload',$sssubunit);
		$bulan = $this->input->post('bulan');
		$this->session->set_userdata('bulan_log_upload',$bulan);
		$tahun = $this->input->post('tahun');
		$this->session->set_userdata('tahun_log_upload',$tahun);
		$id_mesin = $this->input->post('id_mesin');
		$this->session->set_userdata('id_mesin_log_upload',$id_mesin);
		
		$sSearch = '';
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		/* if(isset($select) && $select != '')
		{
			if(!empty($s_biro)) 
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
			}
			else
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			}
		}
		 */
		if(isset($id_mesin) && $id_mesin != '')
		{
			$sSearch .= " and (id_mesin = '".$id_mesin."') ";
		}
		if(isset($bulan) && $bulan != '')
		{
			$sSearch .= " and (month(upload_time) = '".$bulan."') ";
		}
		if(isset($tahun) && $tahun != '')
		{
			$sSearch .= " and (year(upload_time) = '".$tahun."') ";
		}
	
		//limit
		if ( isset( $_REQUEST['start'] ) && $_REQUEST['length'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_REQUEST['start'] ).", ".
				intval( $_REQUEST['length'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "`".$aColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]."` ".
						($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		//$sOrder = " order by ".$aColumns[$order['column']]." ".$order['dir']." ";
		$s0 = "select count(a.id) as jml
				from d_upload_log a
				where 1 = 1
				";
		$s0 .= $sSearch;
		
		$sheet_total = $this->db->query($s0)->row();
		$sheet_total = $sheet_total->jml;
		$s1 = "set @rownum  = 0;";
		$this->db->query($s1);
		$s1 = "SELECT b.*,(select serial_number from mesin_absensi where id = b.id_mesin) as serial_number,
				(select username from t_users where id = b.upload_by) as username
			FROM d_upload_log b
			WHERE 1=1
			";
		$s1 .= $sSearch;
		$s1 .= " order by b.upload_time desc ";
		
		$s1 .= $sLimit;
	//	echo $s1;
		$sheet1 = $this->db->query($s1);
		$result = array();
		$i=0;
		$no=1;
		foreach($sheet1->result() as $sheet)
		{
			$result[$i]['0'] = $no;
			$result[$i]['1'] = $sheet->serial_number;
			$result[$i]['2'] = ((isset($sheet->file_upload) && $sheet->file_upload != '')?'<a target="_blank" href="/assets/log/'.$sheet->file_upload.'">'.$sheet->file_upload.'</a>':'-');
			$result[$i]['3'] = $sheet->keterangan;
			$result[$i]['4'] = date('d-M-Y H:i:s', strtotime($sheet->upload_time));		
			$result[$i]['5'] = $sheet->username;
			$result[$i]['6'] = '<a target="_blank" href="/log/view_upload/'.$sheet->id.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="View">
									<i class="glyphicon glyphicon-search"></i>
								</a>';
			
			if($hak_akses['de'] == 1)
			{
				$result[$i]['7'] = '<a id="del_'.$no.'" href="/log/hapus_upload/'.$sheet->id.'" class="btn btn-danger del" data-toggle="tooltip" data-placement="top" title="Hapus">
								<i class="glyphicon glyphicon-trash"></i>
								</a>
								<script>
								$(document).ready(function () {
									$("#del_'.$no.'").click(function(){
										if (!confirm("Do you want to delete")){
										  return false;
										}
									});
								});
								</script>
								';
			}
			else
			{
				$result[$i]['7'] = '';
			}
			
			$no++;
			$i++;
		}
		
		$data = $result;
		$results = array(
			"iTotalRecords" => ($sheet_total),
			"iTotalDisplayRecords" => ($sheet_total),
			"aaData"=>$data);
			
		echo json_encode($results);
	}
	
	public function tambah_upload()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('id_mesin', 'Mesin', 'trim|required');
		if (empty($_FILES['userfile']['name']))
		{
			$this->form_validation->set_rules('userfile', 'Document', 'required');
		}
			
		if ($this->form_validation->run() === TRUE)
		{
				$posts = $this->input->post();
				$id_mesin = $this->db->escape_str($posts['id_mesin']);
				$keterangan = $this->db->escape_str($posts['keterangan']);
				$upload_time = date('Y-m-d H:i:s');
				$upload_by = $this->userid;
				$upload_date = date('Y-m-d-H-i-s');
				$path = 'assets/log/';
				$config['upload_path'] = $path;
				$config['allowed_types'] = 'dat';
				$this->load->library('upload', $config);
				$field_name = 'userfile';
				if ($this->upload->do_upload($field_name))
				{
					$data = array('upload_data' => $this->upload->data());
					$file_1 = $path.$id_mesin.'-'.$upload_date.$data['upload_data']['file_ext'];
					if (file_exists($file_1)) {unlink($file_1);}       
					$org_1 = $path.$data['upload_data']['file_name'];
					rename($org_1, $file_1);	
					$file_upload = $id_mesin.'-'.$upload_date.$data['upload_data']['file_ext'];
					$sql = 	"insert into d_upload_log 
							SET id_mesin = '$id_mesin', 
								upload_time = '$upload_time',
								upload_by = '$upload_by',
								keterangan = '$keterangan',
								file_upload = '$file_upload'
								";
				//	echo $sql.'</br>';
					$this->db->query($sql);	
					$id = $this->db->insert_id();
					///input data log
					$this->update_data($id);
					input_log($this->user_id,'Tambah data file upload dengan id:'.$id);				
					redirect('/log/upload');
				}
				else
				{
					$this->session->set_flashdata('message', $this->upload->display_errors());	
					$file_upload = '';
					$this->data['keterangan'] = array(
						'name'  => 'keterangan',
						'id'    => 'keterangan',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('keterangan'),
						'class'  => 'form-control',
					);
									
					$this->data['title'] = 'Data Form Upload Log';
					$this->data['subtitle'] = 'Form Penambahan Data File Upload Log';
					$this->data['error'] = '';
					$this->load->view('form_upload_log',$this->data);
				}	
				
		}
		else
		{
			$this->data['keterangan'] = array(
						'name'  => 'keterangan',
						'id'    => 'keterangan',
						'type'  => 'text',
						'value' => $this->form_validation->set_value('keterangan'),
						'class'  => 'form-control',
			);
									
			$this->data['title'] = 'Data Form Upload Log';
			$this->data['subtitle'] = 'Form Penambahan Data File Upload Log';
			$this->data['error'] = '';
			$this->load->view('form_upload_log',$this->data);
		}
			
	}
	
	function update_data($id_upload)
	{
		$s = "select a.file_upload,
					b.serial_number
				from d_upload_log a 
				left join mesin_absensi b on(a.id_mesin = b.id)
				where a.id = '$id_upload' ";
		$q = $this->db->query($s)->row();
		$file_upload = $q->file_upload;
		$serial_number = $q->serial_number;
		$id_upload = $id_upload;
		$myfile = fopen('assets/log/'.$file_upload, "r") or die("Unable to open file!");
		$waktu_masuk = date('Y-m-d H:i:s');
		$masuk_by = $this->userid;
		// Output one line until end-of-file
		while(!feof($myfile)) 
		{
		  $line = fgets($myfile);
		  $absen = preg_split('/\s+/', $line);
		  if(isset($absen[1]))
		  {
				$pin = $absen[0];
				$nip = $absen[1];
			//	echo 'nip:'.$nip.'</br>';
				$tanggal = $absen[2];
				$jam = $absen[3];
				$tanggal = $tanggal.' '.$jam;
				$tanggal_masuk = date('Y-m-d H:i:s',strtotime($tanggal));
				$si = "insert into absensi_log_manual
							set serial_number = '$serial_number',
								pin = '$pin',
								nip = '$nip',
								tanggal = '$tanggal_masuk',
								waktu_masuk = '$waktu_masuk',
								masuk_by = '$masuk_by',
								id_upload = '$id_upload'
								
				";
			//	echo $si.'<br>';
				$this->db->query($si);
				$cek_status = $this->cek_status($nip);
			//	echo 'stats;'.$cek_status;
							if($cek_status == 1)
							{
								$tgl = date('j',strtotime($tanggal));
								$bln = date('n',strtotime($tanggal));
								$thn = date('Y',strtotime($tanggal));
								$jam = date('G',strtotime($tanggal));
								$menit = date('i',strtotime($tanggal));
								$scj = "select *
										from d_absen_harian
										where nip = '$nip'
										and	tanggal = '$tgl'
										and	bulan = '$bln'
										and	tahun = '$thn'
								";
							//	echo $scj;
								//$this->db->insert('coba_coba',array('keterangan'=>$scj,'waktu'=>date('Y-m-d H:i:s')));
								$qcj = $this->db->query($scj);
								if($qcj->num_rows() > 0)
								{
									//$cek_libur = $this->cek_libur;
									//$cek_libur = $this->cek_libur($tanggal);
									$st = "select a.*,
													b.*
											from d_pegawai_tipe_jadwal a
											left join ref_tipe_jadwal b on(a.tipe_jadwal = b.id)
											where nip = '$nip' 
											
											";
									$rst = $this->db->query($st)->row_array();
									if($rst['shift'] == 0)
									{			
										//$cek_libur = $this->cek_libur($tgl);
										$cek_libur = $this->cek_libur($tanggal,$rst['sabtu_masuk']);
										if($cek_libur == 0)
										{
											$rcj = $qcj->row();
											$id_absen_harian = $rcj->id;
											$jadwal_masuk = $rcj->jadwal_masuk;
											$realisasi_masuk = $rcj->realisasi_masuk;
											$jadwal_keluar = $rcj->jadwal_keluar;
											$realisasi_keluar = $rcj->realisasi_keluar;
											$potongan_terlambat_masuk = $rcj->potongan_terlambat_masuk;
											$potongan_pulang_cepat = $rcj->potongan_pulang_cepat;
											$potongan_tidak_masuk = $rcj->potongan_tidak_masuk;
											
											if($jadwal_masuk == '0000-00-00 00:00:00' || $jadwal_keluar == '0000-00-00 00:00:00')
											{
												if($jadwal_masuk == '0000-00-00 00:00:00')
												{
													$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($tanggal))/60;
													if($kecepatan_keluar > 0)
													{
														$kecepatan_keluar = $kecepatan_keluar;
														$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
													}
													else
													{
														$kecepatan_keluar = 0;
														$potongan_pulang_cepat=0;
													}
													$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
													$su = "update d_absen_harian
																	set realisasi_keluar = '$tanggal',
																		kecepatan_keluar = '$kecepatan_keluar',
																		potongan_pulang_cepat = '$potongan_pulang_cepat',
																		persentase_potongan = '$persentase_potongan',     
																		update_date = '$tanggal',
																		update_by = 'system'
																	where id = '$id_absen_harian'
													";
													$this->db->query($su);
													$this->db->insert('coba_coba',array('keterangan'=>$su,'waktu'=>date('Y-m-d H:i:s'),'nip'=>$nip));
												}
												elseif($jadwal_keluar == '0000-00-00 00:00:00')
												{
													if($realisasi_masuk == NULL)
													{
														$keterlambatan_masuk = (strtotime($tanggal) - strtotime($jadwal_masuk))/60;
														if($keterlambatan_masuk > 0)
														{
															$keterlambatan_masuk = $keterlambatan_masuk;
															$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
														}
														else
														{
															$keterlambatan_masuk = 0;
															$potongan_terlambat_masuk=0;
														}
														$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
														$su = "update d_absen_harian
																		set realisasi_masuk = '$tanggal',
																			keterlambatan_masuk = '$keterlambatan_masuk',
																			potongan_terlambat_masuk = '$potongan_terlambat_masuk',
																			persentase_potongan = '$persentase_potongan',     
																			update_date = '$tanggal',
																			update_by = 'system'
																		where id = '$id_absen_harian'
														";
														$this->db->query($su);
														$this->db->insert('coba_coba',array('keterangan'=>$su,'waktu'=>date('Y-m-d H:i:s'),'nip'=>$nip));
													}
												}
											}
											else
											{
												$jam_bagi_dua = (strtotime($jadwal_keluar) - strtotime($jadwal_masuk))/2;
												$istirahat = strtotime($jadwal_masuk)+$jam_bagi_dua;
												if(strtotime($tanggal)<$istirahat)
												{
													if($realisasi_masuk == NULL)
													{
														$keterlambatan_masuk = (strtotime($tanggal) - strtotime($jadwal_masuk))/60;
														if($keterlambatan_masuk > 0)
														{
															$keterlambatan_masuk = $keterlambatan_masuk;
															$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
														}
														else
														{
															$keterlambatan_masuk = 0;
															$potongan_terlambat_masuk=0;
														}
														
														$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($tanggal))/60;
														if($kecepatan_keluar > 0)
														{
															$kecepatan_keluar = $kecepatan_keluar;
															$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
														}
														else
														{
															$kecepatan_keluar = 0;
															$potongan_pulang_cepat=0;
														}
														$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
														$su = "update d_absen_harian
																	set realisasi_masuk = '$tanggal',
																		keterlambatan_masuk = '$keterlambatan_masuk',
																		potongan_terlambat_masuk = '$potongan_terlambat_masuk',
																		realisasi_keluar = '$tanggal',
																		kecepatan_keluar = '$kecepatan_keluar',
																		potongan_pulang_cepat = '$potongan_pulang_cepat',
																		persentase_potongan = '$persentase_potongan',     
																		update_date = '$tanggal',
																		update_by = 'system'
																	where id = '$id_absen_harian'
														";
														$this->db->query($su);
														$this->db->insert('coba_coba',array('keterangan'=>$su,'waktu'=>date('Y-m-d H:i:s'),'nip'=>$nip));
													}
													else
													{
														
														$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($tanggal))/60;
														if($kecepatan_keluar > 0)
														{
															$kecepatan_keluar = $kecepatan_keluar;
															$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
														}
														else
														{
															$kecepatan_keluar = 0;
															$potongan_pulang_cepat=0;
														}
														$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
														$su = "update d_absen_harian
																	set realisasi_keluar = '$tanggal',
																		kecepatan_keluar = '$kecepatan_keluar',
																		potongan_pulang_cepat = '$potongan_pulang_cepat',
																		persentase_potongan = '$persentase_potongan',     
																		update_date = '$tanggal',
																		update_by = 'system'
																	where id = '$id_absen_harian'
														";
														$this->db->query($su);
														$this->db->insert('coba_coba',array('keterangan'=>$su,'waktu'=>date('Y-m-d H:i:s'),'nip'=>$nip));
													}
												}
												else
												{
													if($realisasi_masuk == NULL)
													{
														$realisasi_masuk = $tanggal;	
														$keterlambatan_masuk = (strtotime($realisasi_masuk) - strtotime($jadwal_masuk))/60;
														//$this->db->insert('coba_coba',array('keterangan'=>'keterlambatan_masuk: '.$keterlambatan_masuk,'waktu'=>date('Y-m-d H:i:s')));	
														$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
														//$this->db->insert('coba_coba',array('keterangan'=>'POTONGAN TELAH: '.$potongan_terlambat_masuk,'waktu'=>date('Y-m-d H:i:s')));	
														$realisasi_keluar = $tanggal;							
														$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($realisasi_keluar))/60;
														if($kecepatan_keluar > 0)
														{
															$kecepatan_keluar = $kecepatan_keluar;
															$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
														}
														else
														{
															$kecepatan_keluar = 0;
															$potongan_pulang_cepat=0;
														}
														$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
														$su = "update d_absen_harian
																	set jadwal_masuk = '$jadwal_masuk',
																		realisasi_masuk = '$realisasi_masuk',
																		keterlambatan_masuk = '$keterlambatan_masuk',
																		potongan_terlambat_masuk = '$potongan_terlambat_masuk',
																		jadwal_keluar = '$jadwal_keluar', 
																		realisasi_keluar = '$realisasi_keluar', 
																		kecepatan_keluar = '$kecepatan_keluar', 
																		potongan_pulang_cepat = '$potongan_pulang_cepat', 
																		persentase_potongan = '$persentase_potongan', 
																		update_date = '$tanggal',
																		update_by = 'system'
																	where id = '$id_absen_harian'
														";
														$this->db->query($su);
														$this->db->insert('coba_coba',array('keterangan'=>$su,'waktu'=>date('Y-m-d H:i:s'),'nip'=>$nip));
													}
													else
													{
														$realisasi_keluar = $tanggal;							
														$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($realisasi_keluar))/60;
														if($kecepatan_keluar > 0)
														{
															$kecepatan_keluar = $kecepatan_keluar;
															$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
														}
														else
														{
															$kecepatan_keluar = 0;
															$potongan_pulang_cepat=0;
														}
														$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
														$su = "update d_absen_harian
																	set realisasi_keluar = '$realisasi_keluar', 
																		kecepatan_keluar = '$kecepatan_keluar', 
																		potongan_pulang_cepat = '$potongan_pulang_cepat', 
																		persentase_potongan = '$persentase_potongan', 
																		update_date = '$tanggal',
																		update_by = 'system'
																	where id = '$id_absen_harian'
														";
														$this->db->query($su);
														$this->db->insert('coba_coba',array('keterangan'=>$su,'waktu'=>date('Y-m-d H:i:s'),'nip'=>$nip));
													}
												}
												
											}
										
										}
									}
									else
									{
										$sl = "select * from d_jadwal_lembur where nip = '$nip' and tanggal = '$tanggal' ";
										$ql = $this->db->query($sl);
										if($ql->num_rows() > 0)
										{
											$jl = $ql->row_array();
											if($jl['is_libur'] == 0)
											{
												$rcj = $qcj->row();
												$id_absen_harian = $rcj->id;
												$jadwal_masuk = $rcj->jadwal_masuk;
												$realisasi_masuk = $rcj->realisasi_masuk;
												$jadwal_keluar = $rcj->jadwal_keluar;
												$realisasi_keluar = $rcj->realisasi_keluar;
												$potongan_terlambat_masuk = $rcj->potongan_terlambat_masuk;
												$potongan_pulang_cepat = $rcj->potongan_pulang_cepat;
												$potongan_tidak_masuk = $rcj->potongan_tidak_masuk;
												
												if($jadwal_masuk == '0000-00-00 00:00:00' || $jadwal_keluar == '0000-00-00 00:00:00')
												{
													if($jadwal_masuk == '0000-00-00 00:00:00')
													{
														$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($tanggal))/60;
														if($kecepatan_keluar > 0)
														{
															$kecepatan_keluar = $kecepatan_keluar;
															$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
														}
														else
														{
															$kecepatan_keluar = 0;
															$potongan_pulang_cepat=0;
														}
														$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
														$su = "update d_absen_harian
																		set realisasi_keluar = '$tanggal',
																			kecepatan_keluar = '$kecepatan_keluar',
																			potongan_pulang_cepat = '$potongan_pulang_cepat',
																			persentase_potongan = '$persentase_potongan',     
																			update_date = '$tanggal',
																			update_by = 'system'
																		where id = '$id_absen_harian'
														";
														$this->db->query($su);
														$this->db->insert('coba_coba',array('keterangan'=>$su,'waktu'=>date('Y-m-d H:i:s'),'nip'=>$nip));
													}
													elseif($jadwal_keluar == '0000-00-00 00:00:00')
													{
														if($realisasi_masuk == NULL)
														{
															$keterlambatan_masuk = (strtotime($tanggal) - strtotime($jadwal_masuk))/60;
															if($keterlambatan_masuk > 0)
															{
																$keterlambatan_masuk = $keterlambatan_masuk;
																$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
															}
															else
															{
																$keterlambatan_masuk = 0;
																$potongan_terlambat_masuk=0;
															}
															$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
															$su = "update d_absen_harian
																			set realisasi_masuk = '$tanggal',
																				keterlambatan_masuk = '$keterlambatan_masuk',
																				potongan_terlambat_masuk = '$potongan_terlambat_masuk',
																				persentase_potongan = '$persentase_potongan',     
																				update_date = '$tanggal',
																				update_by = 'system'
																			where id = '$id_absen_harian'
															";
															$this->db->query($su);
															$this->db->insert('coba_coba',array('keterangan'=>$su,'waktu'=>date('Y-m-d H:i:s'),'nip'=>$nip));
														}
													}
												}
												else
												{
													$jam_bagi_dua = (strtotime($jadwal_keluar) - strtotime($jadwal_masuk))/2;
													$istirahat = strtotime($jadwal_masuk)+$jam_bagi_dua;
													if(strtotime($tanggal)<$istirahat)
													{
														if($realisasi_masuk == NULL)
														{
															$keterlambatan_masuk = (strtotime($tanggal) - strtotime($jadwal_masuk))/60;
															if($keterlambatan_masuk > 0)
															{
																$keterlambatan_masuk = $keterlambatan_masuk;
																$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
															}
															else
															{
																$keterlambatan_masuk = 0;
																$potongan_terlambat_masuk=0;
															}
															
															$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($tanggal))/60;
															if($kecepatan_keluar > 0)
															{
																$kecepatan_keluar = $kecepatan_keluar;
																$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
															}
															else
															{
																$kecepatan_keluar = 0;
																$potongan_pulang_cepat=0;
															}
															$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
															$su = "update d_absen_harian
																		set realisasi_masuk = '$tanggal',
																			keterlambatan_masuk = '$keterlambatan_masuk',
																			potongan_terlambat_masuk = '$potongan_terlambat_masuk',
																			realisasi_keluar = '$tanggal',
																			kecepatan_keluar = '$kecepatan_keluar',
																			potongan_pulang_cepat = '$potongan_pulang_cepat',
																			persentase_potongan = '$persentase_potongan',     
																			update_date = '$tanggal',
																			update_by = 'system'
																		where id = '$id_absen_harian'
															";
															$this->db->query($su);
															$this->db->insert('coba_coba',array('keterangan'=>$su,'waktu'=>date('Y-m-d H:i:s'),'nip'=>$nip));
														}
														else
														{
															
															$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($tanggal))/60;
															if($kecepatan_keluar > 0)
															{
																$kecepatan_keluar = $kecepatan_keluar;
																$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
															}
															else
															{
																$kecepatan_keluar = 0;
																$potongan_pulang_cepat=0;
															}
															$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
															$su = "update d_absen_harian
																		set realisasi_keluar = '$tanggal',
																			kecepatan_keluar = '$kecepatan_keluar',
																			potongan_pulang_cepat = '$potongan_pulang_cepat',
																			persentase_potongan = '$persentase_potongan',     
																			update_date = '$tanggal',
																			update_by = 'system'
																		where id = '$id_absen_harian'
															";
															$this->db->query($su);
															$this->db->insert('coba_coba',array('keterangan'=>$su,'waktu'=>date('Y-m-d H:i:s'),'nip'=>$nip));
														}
													}
													else
													{
														if($realisasi_masuk == NULL)
														{
															$realisasi_masuk = $tanggal;	
															$keterlambatan_masuk = (strtotime($realisasi_masuk) - strtotime($jadwal_masuk))/60;
															//$this->db->insert('coba_coba',array('keterangan'=>'keterlambatan_masuk: '.$keterlambatan_masuk,'waktu'=>date('Y-m-d H:i:s')));	
															$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
															//$this->db->insert('coba_coba',array('keterangan'=>'POTONGAN TELAH: '.$potongan_terlambat_masuk,'waktu'=>date('Y-m-d H:i:s')));	
															$realisasi_keluar = $tanggal;							
															$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($realisasi_keluar))/60;
															if($kecepatan_keluar > 0)
															{
																$kecepatan_keluar = $kecepatan_keluar;
																$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
															}
															else
															{
																$kecepatan_keluar = 0;
																$potongan_pulang_cepat=0;
															}
															$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
															$su = "update d_absen_harian
																		set jadwal_masuk = '$jadwal_masuk',
																			realisasi_masuk = '$realisasi_masuk',
																			keterlambatan_masuk = '$keterlambatan_masuk',
																			potongan_terlambat_masuk = '$potongan_terlambat_masuk',
																			jadwal_keluar = '$jadwal_keluar', 
																			realisasi_keluar = '$realisasi_keluar', 
																			kecepatan_keluar = '$kecepatan_keluar', 
																			potongan_pulang_cepat = '$potongan_pulang_cepat', 
																			persentase_potongan = '$persentase_potongan', 
																			update_date = '$tanggal',
																			update_by = 'system'
																		where id = '$id_absen_harian'
															";
															$this->db->query($su);
															$this->db->insert('coba_coba',array('keterangan'=>$su,'waktu'=>date('Y-m-d H:i:s'),'nip'=>$nip));
														}
														else
														{
															$realisasi_keluar = $tanggal;							
															$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($realisasi_keluar))/60;
															if($kecepatan_keluar > 0)
															{
																$kecepatan_keluar = $kecepatan_keluar;
																$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
															}
															else
															{
																$kecepatan_keluar = 0;
																$potongan_pulang_cepat=0;
															}
															$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
															$su = "update d_absen_harian
																		set realisasi_keluar = '$realisasi_keluar', 
																			kecepatan_keluar = '$kecepatan_keluar', 
																			potongan_pulang_cepat = '$potongan_pulang_cepat', 
																			persentase_potongan = '$persentase_potongan', 
																			update_date = '$tanggal',
																			update_by = 'system'
																		where id = '$id_absen_harian'
															";
															$this->db->query($su);
															$this->db->insert('coba_coba',array('keterangan'=>$su,'waktu'=>date('Y-m-d H:i:s'),'nip'=>$nip));
														}
													}
													
												}
											
											}
											else
											{
												
											}
										}
									}
								}
								else
								{
									$st = "select a.*,
											b.*
											from d_pegawai_tipe_jadwal a
											left join ref_tipe_jadwal b on(a.tipe_jadwal = b.id)
											where nip = '$nip' 									
									";
									$rst = $this->db->query($st)->row_array();
									//if($rst['tipe_jadwal']=='1')
									if($rst['shift'] == 0)
									{
									//	$cek_libur = $this->cek_libur;
									//	$cek_libur = $this->cek_libur($tanggal);
										$cek_libur = $this->cek_libur($tanggal,$rst['sabtu_masuk']);
										if($cek_libur == 0)
										{
											//cek jika jadwal khusus
											$hari_ini = date('w',strtotime($thn.'-'.$bln.'-'.$tgl));
											$sc = "select * from referensi_jadwal_khusus where hari = '$hari_ini' and aktif = 1 ";
											$qc = $this->db->query($sc);
											if($qc->num_rows() > 0)
											{
												$rjk = $qc->row_array();
												$jadwal_jam_masuk = $rjk['jadwal_jam_masuk'];
												$jadwal_menit_masuk = $rjk['jadwal_menit_masuk'];
												$jadwal_masuk = date('Y-m-d H:i',strtotime($tgl.'-'.$bln.'-'.$thn.' '.$jadwal_jam_masuk.':'.$jadwal_menit_masuk));
												$jadwal_jam_keluar = $rjk['jadwal_jam_keluar'];
												$jadwal_menit_keluar = $rjk['jadwal_menit_keluar'];
												$jadwal_keluar = date('Y-m-d H:i',strtotime($tgl.'-'.$bln.'-'.$thn.' '.$jadwal_jam_keluar.':'.$jadwal_menit_keluar));	
											}
											else
											{
												$jadwal_jam_masuk = $rst['jadwal_jam_masuk'];
												$jadwal_menit_masuk = $rst['jadwal_menit_masuk'];
												$jadwal_masuk = date('Y-m-d H:i',strtotime($tgl.'-'.$bln.'-'.$thn.' '.$jadwal_jam_masuk.':'.$jadwal_menit_masuk));
												$jadwal_jam_keluar = $rst['jadwal_jam_keluar'];
												$jadwal_menit_keluar = $rst['jadwal_menit_keluar'];
												$jadwal_keluar = date('Y-m-d H:i',strtotime($tgl.'-'.$bln.'-'.$thn.' '.$jadwal_jam_keluar.':'.$jadwal_menit_keluar));	
											}
											
											
											$jam_bagi_dua = (strtotime($jadwal_keluar) - strtotime($jadwal_masuk))/2;
											$istirahat = strtotime($jadwal_masuk)+$jam_bagi_dua;										
											$libur = 0;
											if(strtotime($tanggal)<$istirahat)
											{
													$realisasi_masuk = $tanggal;
													$keterlambatan_masuk = (strtotime($realisasi_masuk) - strtotime($jadwal_masuk))/60;
													if($keterlambatan_masuk > 0)
													{
														$keterlambatan_masuk = $keterlambatan_masuk;
														$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
													}
													else
													{
														$keterlambatan_masuk = 0;
														$potongan_terlambat_masuk=0;
													}
													$persentase_potongan = $potongan_terlambat_masuk;
													$si = "insert into d_absen_harian
														set nip = '$nip',
															tanggal = '$tgl',
															bulan = '$bln',
															tahun = '$thn',
															jadwal_masuk = '$jadwal_masuk',
															realisasi_masuk = '$realisasi_masuk',
															keterlambatan_masuk = '$keterlambatan_masuk',
															potongan_terlambat_masuk = '$potongan_terlambat_masuk',
															jadwal_keluar = '$jadwal_keluar', 
															libur = '$libur',
															persentase_potongan = '$persentase_potongan',
															update_date = '$tanggal',
															update_by = 'system'
														";
											//		echo $si.'</br>';
													$qi = $this->db->query($si);
												
											}
											else
											{
												$realisasi_masuk = $tanggal;	
												$keterlambatan_masuk = (strtotime($realisasi_masuk) - strtotime($jadwal_masuk))/60;
												//$this->db->insert('coba_coba',array('keterangan'=>'keterlambatan_masuk: '.$keterlambatan_masuk,'waktu'=>date('Y-m-d H:i:s')));	
												$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
												//$this->db->insert('coba_coba',array('keterangan'=>'POTONGAN TELAH: '.$potongan_terlambat_masuk,'waktu'=>date('Y-m-d H:i:s')));	
												$realisasi_keluar = $tanggal;
												$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($realisasi_keluar))/60;
												if($kecepatan_keluar > 0)
												{
													$kecepatan_keluar = $kecepatan_keluar;
													$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
												}
												else
												{
													$kecepatan_keluar = 0;
													$potongan_pulang_cepat=0;
												}
												$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat;
												$si = "insert into d_absen_harian
														set nip = '$nip',
															tanggal = '$tgl',
															bulan = '$bln',
															tahun = '$thn',
															jadwal_masuk = '$jadwal_masuk',
															realisasi_masuk = '$realisasi_masuk',
															keterlambatan_masuk = '$keterlambatan_masuk',
															potongan_terlambat_masuk = '$potongan_terlambat_masuk',
															jadwal_keluar = '$jadwal_keluar', 
															realisasi_keluar = '$realisasi_keluar', 
															kecepatan_keluar = '$kecepatan_keluar', 
															potongan_pulang_cepat = '$potongan_pulang_cepat', 
															persentase_potongan = '$persentase_potongan', 
															libur = '$libur',
															update_date = '$tanggal',
															update_by = 'system'
														";
													//echo $si.'</br>';
													$qi = $this->db->query($si);
																					
											}
										
										}
										else
										{
											$libur = 1;
											$si = "insert into d_absen_harian
														set nip = '$nip',
															tanggal = '$tgl',
															bulan = '$bln',
															tahun = '$thn',
															libur = '$libur',
															update_date = '$tanggal',
															update_by = 'system'
														";
												///	echo $si.'</br>';
													$qi = $this->db->query($si);
										}
										
									}
									else
									{
										$sl = "select * from d_jadwal_lembur where nip = '$nip' and tanggal = '$tanggal' ";
										$ql = $this->db->query($sl);
										if($ql->num_rows() > 0)
										{
											$jl = $ql->row_array();
											if($jl['is_libur'] == 0)
											{
												$jadwal_masuk = $jl['jadwal_masuk'];
												$jadwal_keluar = $jl['jadwal_keluar'];	
												if($jadwal_masuk == '0000-00-00 00:00:00' || $jadwal_keluar == '0000-00-00 00:00:00')
												{
													$libur = 0;
													if($jadwal_masuk == '0000-00-00 00:00:00')
														{
															$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($tanggal))/60;
															if($kecepatan_keluar > 0)
															{
																$kecepatan_keluar = $kecepatan_keluar;
																$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
															}
															else
															{
																$kecepatan_keluar = 0;
																$potongan_pulang_cepat=0;
															}
															$persentase_potongan = $potongan_pulang_cepat;
															$su = "insert into d_absen_harian
																set nip = '$nip',
																	tanggal = '$tgl',
																	bulan = '$bln',
																	tahun = '$thn',
																	jadwal_masuk = '$jadwal_masuk',
																	jadwal_keluar = '$jadwal_keluar', 
																	realisasi_keluar = '$tanggal', 
																	kecepatan_keluar = '$kecepatan_keluar', 
																	potongan_pulang_cepat = '$potongan_pulang_cepat', 
																	persentase_potongan = '$persentase_potongan', 
																	libur = '$libur',
																	update_date = '$tanggal',
																	update_by = 'system'
															";
															$this->db->query($su);
															$this->db->insert('coba_coba',array('keterangan'=>$su,'waktu'=>date('Y-m-d H:i:s'),'nip'=>$nip));
														}
														elseif($jadwal_keluar == '0000-00-00 00:00:00')
														{
															
																$keterlambatan_masuk = (strtotime($tanggal) - strtotime($jadwal_masuk))/60;
																if($keterlambatan_masuk > 0)
																{
																	$keterlambatan_masuk = $keterlambatan_masuk;
																	$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
																}
																else
																{
																	$keterlambatan_masuk = 0;
																	$potongan_terlambat_masuk=0;
																}
																$persentase_potongan = $potongan_terlambat_masuk;
																$su = "insert into d_absen_harian
																set nip = '$nip',
																	tanggal = '$tgl',
																	bulan = '$bln',
																	tahun = '$thn',
																	jadwal_masuk = '$jadwal_masuk',
																	realisasi_masuk = '$tanggal',
																	keterlambatan_masuk = '$keterlambatan_masuk',
																	potongan_terlambat_masuk = '$potongan_terlambat_masuk',
																	jadwal_keluar = '$jadwal_keluar', 
																	persentase_potongan = '$persentase_potongan', 
																	libur = '$libur',
																	update_date = '$tanggal',
																	update_by = 'system'
																";
																$this->db->query($su);
																$this->db->insert('coba_coba',array('keterangan'=>$su,'waktu'=>date('Y-m-d H:i:s'),'nip'=>$nip));
															
														}
												}
												else
												{
													$hari_ini = date('w',strtotime($thn.'-'.$bln.'-'.$tgl));
													$jam_bagi_dua = (strtotime($jadwal_keluar) - strtotime($jadwal_masuk))/2;
													$istirahat = strtotime($jadwal_masuk)+$jam_bagi_dua;										
													$libur = 0;
													if(strtotime($tanggal)<$istirahat)
													{
														$this->db->insert('coba_coba',array('keterangan'=>'masuk','waktu'=>date('Y-m-d H:i:s'),'nip'=>$nip));
														
															$realisasi_masuk = $tanggal;
															$keterlambatan_masuk = (strtotime($realisasi_masuk) - strtotime($jadwal_masuk))/60;
															if($keterlambatan_masuk > 0)
															{
																$keterlambatan_masuk = $keterlambatan_masuk;
																$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
															}
															else
															{
																$keterlambatan_masuk = 0;
																$potongan_terlambat_masuk=0;
															}
															$persentase_potongan = $potongan_terlambat_masuk;
															$si = "insert into d_absen_harian
																set nip = '$nip',
																	tanggal = '$tgl',
																	bulan = '$bln',
																	tahun = '$thn',
																	jadwal_masuk = '$jadwal_masuk',
																	realisasi_masuk = '$realisasi_masuk',
																	keterlambatan_masuk = '$keterlambatan_masuk',
																	potongan_terlambat_masuk = '$potongan_terlambat_masuk',
																	jadwal_keluar = '$jadwal_keluar', 
																	libur = '$libur',
																	persentase_potongan = '$persentase_potongan',
																	update_date = '$tanggal',
																	update_by = 'system'
																";
														//	echo $si.'</br>';
															$qi = $this->db->query($si);
															$this->db->insert('coba_coba',array('keterangan'=>$si,'waktu'=>date('Y-m-d H:i:s'),'nip'=>$nip));
														
													}
													else
													{
														$realisasi_masuk = $tanggal;	
														$keterlambatan_masuk = (strtotime($realisasi_masuk) - strtotime($jadwal_masuk))/60;
														//$this->db->insert('coba_coba',array('keterangan'=>'keterlambatan_masuk: '.$keterlambatan_masuk,'waktu'=>date('Y-m-d H:i:s')));	
														$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
														//$this->db->insert('coba_coba',array('keterangan'=>'POTONGAN TELAH: '.$potongan_terlambat_masuk,'waktu'=>date('Y-m-d H:i:s')));	
														$realisasi_keluar = $tanggal;
														$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($realisasi_keluar))/60;
														if($kecepatan_keluar > 0)
														{
															$kecepatan_keluar = $kecepatan_keluar;
															$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
														}
														else
														{
															$kecepatan_keluar = 0;
															$potongan_pulang_cepat=0;
														}
														$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat;
														$si = "insert into d_absen_harian
																set nip = '$nip',
																	tanggal = '$tgl',
																	bulan = '$bln',
																	tahun = '$thn',
																	jadwal_masuk = '$jadwal_masuk',
																	realisasi_masuk = '$realisasi_masuk',
																	keterlambatan_masuk = '$keterlambatan_masuk',
																	potongan_terlambat_masuk = '$potongan_terlambat_masuk',
																	jadwal_keluar = '$jadwal_keluar', 
																	realisasi_keluar = '$realisasi_keluar', 
																	kecepatan_keluar = '$kecepatan_keluar', 
																	potongan_pulang_cepat = '$potongan_pulang_cepat', 
																	persentase_potongan = '$persentase_potongan', 
																	libur = '$libur',
																	update_date = '$tanggal',
																	update_by = 'system'
																";
														//	echo $si.'</br>';
															$this->db->insert('coba_coba',array('keterangan'=>$si,'waktu'=>date('Y-m-d H:i:s'),'nip'=>$nip));
															$qi = $this->db->query($si);
																							
													}
										
												}
												
											}
											else
											{
												$libur = 1;
												$si = "insert into d_absen_harian
														set nip = '$nip',
															tanggal = '$tgl',
															bulan = '$bln',
															tahun = '$thn',
															libur = '$libur',
															update_date = '$tanggal',
															update_by = 'system'
														";
											//	echo $si.'</br>';
												$this->db->insert('coba_coba',array('keterangan'=>$si,'waktu'=>date('Y-m-d H:i:s'),'nip'=>$nip));
												$qi = $this->db->query($si);
											}
										}
									}
									
								}
								
								$this->update_data_rekap_keuangan($nip,$tanggal,$persentase_potongan);
							}
							elseif($cek_status == 2)
							{
								$nik = $nip;
								$tgl = date('j',strtotime($tanggal));
								$bln = date('n',strtotime($tanggal));
								$thn = date('Y',strtotime($tanggal));
								$jam = date('G',strtotime($tanggal));
								$menit = date('i',strtotime($tanggal));
								$scj = "select *
										from d_absen_harian_tkk
										where nik = '$nik'
										and	tanggal = '$tgl'
										and	bulan = '$bln'
										and	tahun = '$thn'
								";
								//$this->db->insert('coba_coba',array('keterangan'=>$scj,'waktu'=>date('Y-m-d H:i:s')));
								$qcj = $this->db->query($scj);
								if($qcj->num_rows() > 0)
								{
									$st = "select a.*,
												b.*
											from d_pegawai_tipe_jadwal_tkk a
											left join ref_tipe_jadwal b on(a.tipe_jadwal = b.id)
											where nik = '$nik' 									
									";
									$rst = $this->db->query($st)->row_array();
									if($rst['shift'] == 0)
									{
										//$cek_libur = $this->cek_libur($tanggal);
										$cek_libur = $this->cek_libur($tanggal,$rst['sabtu_masuk']);
										if($cek_libur == 0)
										{
											$rcj = $qcj->row();
											$id_absen_harian = $rcj->id;
											$jadwal_masuk = $rcj->jadwal_masuk;
											$realisasi_masuk = $rcj->realisasi_masuk;
											$jadwal_keluar = $rcj->jadwal_keluar;
											$realisasi_keluar = $rcj->realisasi_keluar;
											$potongan_terlambat_masuk = $rcj->potongan_terlambat_masuk;
											$potongan_pulang_cepat = $rcj->potongan_pulang_cepat;
											$potongan_tidak_masuk = $rcj->potongan_tidak_masuk;
											if($jadwal_masuk == '0000-00-00 00:00:00' || $jadwal_keluar == '0000-00-00 00:00:00')
											{
												if($jadwal_masuk == '0000-00-00 00:00:00')
												{
													$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($tanggal))/60;
													if($kecepatan_keluar > 0)
													{
														$kecepatan_keluar = $kecepatan_keluar;
														$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
													}
													else
													{
														$kecepatan_keluar = 0;
														$potongan_pulang_cepat=0;
													}
													$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
													$su = "update d_absen_harian_tkk
																	set realisasi_keluar = '$tanggal',
																		kecepatan_keluar = '$kecepatan_keluar',
																		potongan_pulang_cepat = '$potongan_pulang_cepat',
																		persentase_potongan = '$persentase_potongan',     
																		update_date = '$tanggal',
																		update_by = 'system'
																	where id = '$id_absen_harian'
													";
													$this->db->query($su);
													$this->db->insert('coba_coba',array('keterangan'=>$su,'waktu'=>date('Y-m-d H:i:s'),'nip'=>$nip));
												}
												elseif($jadwal_keluar == '0000-00-00 00:00:00')
												{
													if($realisasi_masuk == NULL)
													{
														$keterlambatan_masuk = (strtotime($tanggal) - strtotime($jadwal_masuk))/60;
														if($keterlambatan_masuk > 0)
														{
															$keterlambatan_masuk = $keterlambatan_masuk;
															$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
														}
														else
														{
															$keterlambatan_masuk = 0;
															$potongan_terlambat_masuk=0;
														}
														$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
														$su = "update d_absen_harian_tkk
																		set realisasi_masuk = '$tanggal',
																			keterlambatan_masuk = '$keterlambatan_masuk',
																			potongan_terlambat_masuk = '$potongan_terlambat_masuk',
																			persentase_potongan = '$persentase_potongan',     
																			update_date = '$tanggal',
																			update_by = 'system'
																		where id = '$id_absen_harian'
														";
														$this->db->query($su);
														$this->db->insert('coba_coba',array('keterangan'=>$su,'waktu'=>date('Y-m-d H:i:s'),'nip'=>$nip));
													}
												}
											}
											else
											{
												$jam_bagi_dua = (strtotime($jadwal_keluar) - strtotime($jadwal_masuk))/2;
												$istirahat = strtotime($jadwal_masuk)+$jam_bagi_dua;
												if(strtotime($tanggal)<$istirahat)
												{
													if($realisasi_masuk == NULL)
													{
														$keterlambatan_masuk = (strtotime($tanggal) - strtotime($jadwal_masuk))/60;
														if($keterlambatan_masuk > 0)
														{
															$keterlambatan_masuk = $keterlambatan_masuk;
															$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
														}
														else
														{
															$keterlambatan_masuk = 0;
															$potongan_terlambat_masuk=0;
														}
														
														$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($tanggal))/60;
														if($kecepatan_keluar > 0)
														{
															$kecepatan_keluar = $kecepatan_keluar;
															$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
														}
														else
														{
															$kecepatan_keluar = 0;
															$potongan_pulang_cepat=0;
														}
														$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
														$su = "update d_absen_harian_tkk
																	set realisasi_masuk = '$tanggal',
																		keterlambatan_masuk = '$keterlambatan_masuk',
																		potongan_terlambat_masuk = '$potongan_terlambat_masuk',
																		realisasi_keluar = '$tanggal',
																		kecepatan_keluar = '$kecepatan_keluar',
																		potongan_pulang_cepat = '$potongan_pulang_cepat',
																		persentase_potongan = '$persentase_potongan',     
																		update_date = '$tanggal',
																		update_by = 'system'
																	where id = '$id_absen_harian'
														";
														$this->db->query($su);
														$this->db->insert('coba_coba',array('keterangan'=>$su,'waktu'=>date('Y-m-d H:i:s'),'nip'=>$nip));
													}
													else
													{
														
														$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($tanggal))/60;
														if($kecepatan_keluar > 0)
														{
															$kecepatan_keluar = $kecepatan_keluar;
															$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
														}
														else
														{
															$kecepatan_keluar = 0;
															$potongan_pulang_cepat=0;
														}
														$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
														$su = "update d_absen_harian_tkk
																	set realisasi_keluar = '$tanggal',
																		kecepatan_keluar = '$kecepatan_keluar',
																		potongan_pulang_cepat = '$potongan_pulang_cepat',
																		persentase_potongan = '$persentase_potongan',     
																		update_date = '$tanggal',
																		update_by = 'system'
																	where id = '$id_absen_harian'
														";
														$this->db->query($su);
														$this->db->insert('coba_coba',array('keterangan'=>$su,'waktu'=>date('Y-m-d H:i:s'),'nip'=>$nip));
													}
												}
												else
												{
													if($realisasi_masuk == NULL)
													{
														$realisasi_masuk = $tanggal;	
														$keterlambatan_masuk = (strtotime($realisasi_masuk) - strtotime($jadwal_masuk))/60;
														//$this->db->insert('coba_coba',array('keterangan'=>'keterlambatan_masuk: '.$keterlambatan_masuk,'waktu'=>date('Y-m-d H:i:s')));	
														$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
														//$this->db->insert('coba_coba',array('keterangan'=>'POTONGAN TELAH: '.$potongan_terlambat_masuk,'waktu'=>date('Y-m-d H:i:s')));	
														$realisasi_keluar = $tanggal;							
														$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($realisasi_keluar))/60;
														if($kecepatan_keluar > 0)
														{
															$kecepatan_keluar = $kecepatan_keluar;
															$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
														}
														else
														{
															$kecepatan_keluar = 0;
															$potongan_pulang_cepat=0;
														}
														$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
														$su = "update d_absen_harian_tkk
																	set jadwal_masuk = '$jadwal_masuk',
																		realisasi_masuk = '$realisasi_masuk',
																		keterlambatan_masuk = '$keterlambatan_masuk',
																		potongan_terlambat_masuk = '$potongan_terlambat_masuk',
																		jadwal_keluar = '$jadwal_keluar', 
																		realisasi_keluar = '$realisasi_keluar', 
																		kecepatan_keluar = '$kecepatan_keluar', 
																		potongan_pulang_cepat = '$potongan_pulang_cepat', 
																		persentase_potongan = '$persentase_potongan', 
																		update_date = '$tanggal',
																		update_by = 'system'
																	where id = '$id_absen_harian'
														";
														$this->db->query($su);
														$this->db->insert('coba_coba',array('keterangan'=>$su,'waktu'=>date('Y-m-d H:i:s'),'nip'=>$nip));
													}
													else
													{
														$realisasi_keluar = $tanggal;							
														$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($realisasi_keluar))/60;
														if($kecepatan_keluar > 0)
														{
															$kecepatan_keluar = $kecepatan_keluar;
															$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
														}
														else
														{
															$kecepatan_keluar = 0;
															$potongan_pulang_cepat=0;
														}
														$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
														$su = "update d_absen_harian_tkk
																	set realisasi_keluar = '$realisasi_keluar', 
																		kecepatan_keluar = '$kecepatan_keluar', 
																		potongan_pulang_cepat = '$potongan_pulang_cepat', 
																		persentase_potongan = '$persentase_potongan', 
																		update_date = '$tanggal',
																		update_by = 'system'
																	where id = '$id_absen_harian'
														";
														$this->db->query($su);
														$this->db->insert('coba_coba',array('keterangan'=>$su,'waktu'=>date('Y-m-d H:i:s'),'nip'=>$nip));
													}
												}
											}
										}
									}
									else
									{
										$sl = "select * from d_jadwal_lembur_tkk where nik = '$nik' and tanggal = '$tanggal' ";
										$ql = $this->db->query($sl);
										if($ql->num_rows() > 0)
										{
											$jl = $ql->row_array();
											if($jl['is_libur'] == 0)
											{
												$rcj = $qcj->row();
												$id_absen_harian = $rcj->id;
												$jadwal_masuk = $rcj->jadwal_masuk;
												$realisasi_masuk = $rcj->realisasi_masuk;
												$jadwal_keluar = $rcj->jadwal_keluar;
												$realisasi_keluar = $rcj->realisasi_keluar;
												$potongan_terlambat_masuk = $rcj->potongan_terlambat_masuk;
												$potongan_pulang_cepat = $rcj->potongan_pulang_cepat;
												$potongan_tidak_masuk = $rcj->potongan_tidak_masuk;
												if($jadwal_masuk == '0000-00-00 00:00:00' || $jadwal_keluar == '0000-00-00 00:00:00')
												{
													if($jadwal_masuk == '0000-00-00 00:00:00')
													{
														$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($tanggal))/60;
														if($kecepatan_keluar > 0)
														{
															$kecepatan_keluar = $kecepatan_keluar;
															$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
														}
														else
														{
															$kecepatan_keluar = 0;
															$potongan_pulang_cepat=0;
														}
														$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
														$su = "update d_absen_harian_tkk
																		set realisasi_keluar = '$tanggal',
																			kecepatan_keluar = '$kecepatan_keluar',
																			potongan_pulang_cepat = '$potongan_pulang_cepat',
																			persentase_potongan = '$persentase_potongan',     
																			update_date = '$tanggal',
																			update_by = 'system'
																		where id = '$id_absen_harian'
														";
														$this->db->query($su);
														$this->db->insert('coba_coba',array('keterangan'=>$su,'waktu'=>date('Y-m-d H:i:s'),'nip'=>$nip));
													}
													elseif($jadwal_keluar == '0000-00-00 00:00:00')
													{
														if($realisasi_masuk == NULL)
														{
															$keterlambatan_masuk = (strtotime($tanggal) - strtotime($jadwal_masuk))/60;
															if($keterlambatan_masuk > 0)
															{
																$keterlambatan_masuk = $keterlambatan_masuk;
																$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
															}
															else
															{
																$keterlambatan_masuk = 0;
																$potongan_terlambat_masuk=0;
															}
															$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
															$su = "update d_absen_harian_tkk
																			set realisasi_masuk = '$tanggal',
																				keterlambatan_masuk = '$keterlambatan_masuk',
																				potongan_terlambat_masuk = '$potongan_terlambat_masuk',
																				persentase_potongan = '$persentase_potongan',     
																				update_date = '$tanggal',
																				update_by = 'system'
																			where id = '$id_absen_harian'
															";
															$this->db->query($su);
															$this->db->insert('coba_coba',array('keterangan'=>$su,'waktu'=>date('Y-m-d H:i:s'),'nip'=>$nip));
														}
													}
												}
												else
												{
													$jam_bagi_dua = (strtotime($jadwal_keluar) - strtotime($jadwal_masuk))/2;
													$istirahat = strtotime($jadwal_masuk)+$jam_bagi_dua;
													if(strtotime($tanggal)<$istirahat)
													{
														if($realisasi_masuk == NULL)
														{
															$keterlambatan_masuk = (strtotime($tanggal) - strtotime($jadwal_masuk))/60;
															if($keterlambatan_masuk > 0)
															{
																$keterlambatan_masuk = $keterlambatan_masuk;
																$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
															}
															else
															{
																$keterlambatan_masuk = 0;
																$potongan_terlambat_masuk=0;
															}
															
															$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($tanggal))/60;
															if($kecepatan_keluar > 0)
															{
																$kecepatan_keluar = $kecepatan_keluar;
																$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
															}
															else
															{
																$kecepatan_keluar = 0;
																$potongan_pulang_cepat=0;
															}
															$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
															$su = "update d_absen_harian_tkk
																		set realisasi_masuk = '$tanggal',
																			keterlambatan_masuk = '$keterlambatan_masuk',
																			potongan_terlambat_masuk = '$potongan_terlambat_masuk',
																			realisasi_keluar = '$tanggal',
																			kecepatan_keluar = '$kecepatan_keluar',
																			potongan_pulang_cepat = '$potongan_pulang_cepat',
																			persentase_potongan = '$persentase_potongan',     
																			update_date = '$tanggal',
																			update_by = 'system'
																		where id = '$id_absen_harian'
															";
															$this->db->query($su);
															$this->db->insert('coba_coba',array('keterangan'=>$su,'waktu'=>date('Y-m-d H:i:s'),'nip'=>$nip));
														}
														else
														{
															
															$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($tanggal))/60;
															if($kecepatan_keluar > 0)
															{
																$kecepatan_keluar = $kecepatan_keluar;
																$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
															}
															else
															{
																$kecepatan_keluar = 0;
																$potongan_pulang_cepat=0;
															}
															$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
															$su = "update d_absen_harian_tkk
																		set realisasi_keluar = '$tanggal',
																			kecepatan_keluar = '$kecepatan_keluar',
																			potongan_pulang_cepat = '$potongan_pulang_cepat',
																			persentase_potongan = '$persentase_potongan',     
																			update_date = '$tanggal',
																			update_by = 'system'
																		where id = '$id_absen_harian'
															";
															$this->db->query($su);
															$this->db->insert('coba_coba',array('keterangan'=>$su,'waktu'=>date('Y-m-d H:i:s'),'nip'=>$nip));
														}
													}
													else
													{
														if($realisasi_masuk == NULL)
														{
															$realisasi_masuk = $tanggal;	
															$keterlambatan_masuk = (strtotime($realisasi_masuk) - strtotime($jadwal_masuk))/60;
															//$this->db->insert('coba_coba',array('keterangan'=>'keterlambatan_masuk: '.$keterlambatan_masuk,'waktu'=>date('Y-m-d H:i:s')));	
															$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
															//$this->db->insert('coba_coba',array('keterangan'=>'POTONGAN TELAH: '.$potongan_terlambat_masuk,'waktu'=>date('Y-m-d H:i:s')));	
															$realisasi_keluar = $tanggal;							
															$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($realisasi_keluar))/60;
															if($kecepatan_keluar > 0)
															{
																$kecepatan_keluar = $kecepatan_keluar;
																$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
															}
															else
															{
																$kecepatan_keluar = 0;
																$potongan_pulang_cepat=0;
															}
															$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
															$su = "update d_absen_harian_tkk
																		set jadwal_masuk = '$jadwal_masuk',
																			realisasi_masuk = '$realisasi_masuk',
																			keterlambatan_masuk = '$keterlambatan_masuk',
																			potongan_terlambat_masuk = '$potongan_terlambat_masuk',
																			jadwal_keluar = '$jadwal_keluar', 
																			realisasi_keluar = '$realisasi_keluar', 
																			kecepatan_keluar = '$kecepatan_keluar', 
																			potongan_pulang_cepat = '$potongan_pulang_cepat', 
																			persentase_potongan = '$persentase_potongan', 
																			update_date = '$tanggal',
																			update_by = 'system'
																		where id = '$id_absen_harian'
															";
															$this->db->query($su);
															$this->db->insert('coba_coba',array('keterangan'=>$su,'waktu'=>date('Y-m-d H:i:s'),'nip'=>$nip));
														}
														else
														{
															$realisasi_keluar = $tanggal;							
															$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($realisasi_keluar))/60;
															if($kecepatan_keluar > 0)
															{
																$kecepatan_keluar = $kecepatan_keluar;
																$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
															}
															else
															{
																$kecepatan_keluar = 0;
																$potongan_pulang_cepat=0;
															}
															$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
															$su = "update d_absen_harian_tkk
																		set realisasi_keluar = '$realisasi_keluar', 
																			kecepatan_keluar = '$kecepatan_keluar', 
																			potongan_pulang_cepat = '$potongan_pulang_cepat', 
																			persentase_potongan = '$persentase_potongan', 
																			update_date = '$tanggal',
																			update_by = 'system'
																		where id = '$id_absen_harian'
															";
															$this->db->query($su);
															$this->db->insert('coba_coba',array('keterangan'=>$su,'waktu'=>date('Y-m-d H:i:s'),'nip'=>$nip));
														}
													}
												}
											}
											else
											{
												
											}
										}
									}
									
								}
								else
								{
									$st = "select a.*,
												b.*
											from d_pegawai_tipe_jadwal_tkk a
											left join ref_tipe_jadwal b on(a.tipe_jadwal = b.id)
											where nik = '$nik' 									
									";
								//	$this->db->insert('coba_coba',array('keterangan'=>$st,'waktu'=>date('Y-m-d H:i:s')));
									$rst = $this->db->query($st)->row_array();
									//if($rst['tipe_jadwal']=='1')
									if($rst['shift'] == 0)
									{
										//$cek_libur = $this->cek_libur;
										//$cek_libur = $this->cek_libur($tanggal);
										$cek_libur = $this->cek_libur($tanggal,$rst['sabtu_masuk']);
										if($cek_libur == 0)
										{
											//cek jika jadwal khusus
											$hari_ini = date('w',strtotime($thn.'-'.$bln.'-'.$tgl));
											$sc = "select * from referensi_jadwal_khusus where hari = '$hari_ini' and aktif = 1 ";
											$qc = $this->db->query($sc);
											if($qc->num_rows() > 0)
											{
												$rjk = $qc->row_array();
												$jadwal_jam_masuk = $rjk['jadwal_jam_masuk'];
												$jadwal_menit_masuk = $rjk['jadwal_menit_masuk'];
												$jadwal_masuk = date('Y-m-d H:i',strtotime($tgl.'-'.$bln.'-'.$thn.' '.$jadwal_jam_masuk.':'.$jadwal_menit_masuk));
												$jadwal_jam_keluar = $rjk['jadwal_jam_keluar'];
												$jadwal_menit_keluar = $rjk['jadwal_menit_keluar'];
												$jadwal_keluar = date('Y-m-d H:i',strtotime($tgl.'-'.$bln.'-'.$thn.' '.$jadwal_jam_keluar.':'.$jadwal_menit_keluar));	
											}
											else
											{
												$jadwal_jam_masuk = $rst['jadwal_jam_masuk'];
												$jadwal_menit_masuk = $rst['jadwal_menit_masuk'];
												$jadwal_masuk = date('Y-m-d H:i',strtotime($tgl.'-'.$bln.'-'.$thn.' '.$jadwal_jam_masuk.':'.$jadwal_menit_masuk));
												$jadwal_jam_keluar = $rst['jadwal_jam_keluar'];
												$jadwal_menit_keluar = $rst['jadwal_menit_keluar'];
												$jadwal_keluar = date('Y-m-d H:i',strtotime($tgl.'-'.$bln.'-'.$thn.' '.$jadwal_jam_keluar.':'.$jadwal_menit_keluar));	
											}
											
											$jam_bagi_dua = (strtotime($jadwal_keluar) - strtotime($jadwal_masuk))/2;
											$istirahat = strtotime($jadwal_masuk)+$jam_bagi_dua;										
											$libur = 0;
											if(strtotime($tanggal)<$istirahat)
											{
												$this->db->insert('coba_coba',array('keterangan'=>'masuk','waktu'=>date('Y-m-d H:i:s'),'nip'=>$nip));
												
													$realisasi_masuk = $tanggal;
													$keterlambatan_masuk = (strtotime($realisasi_masuk) - strtotime($jadwal_masuk))/60;
													if($keterlambatan_masuk > 0)
													{
														$keterlambatan_masuk = $keterlambatan_masuk;
														$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
													}
													else
													{
														$keterlambatan_masuk = 0;
														$potongan_terlambat_masuk=0;
													}
													$persentase_potongan = $potongan_terlambat_masuk;
													$si = "insert into d_absen_harian_tkk
														set nik = '$nik',
															tanggal = '$tgl',
															bulan = '$bln',
															tahun = '$thn',
															jadwal_masuk = '$jadwal_masuk',
															realisasi_masuk = '$realisasi_masuk',
															keterlambatan_masuk = '$keterlambatan_masuk',
															potongan_terlambat_masuk = '$potongan_terlambat_masuk',
															jadwal_keluar = '$jadwal_keluar', 
															libur = '$libur',
															persentase_potongan = '$persentase_potongan',
															update_date = '$tanggal',
															update_by = 'system'
														";
													//echo $si.'</br>';
													$qi = $this->db->query($si);
													$this->db->insert('coba_coba',array('keterangan'=>$si,'waktu'=>date('Y-m-d H:i:s'),'nip'=>$nip));
												
											}
											else
											{
												$realisasi_masuk = $tanggal;	
												$keterlambatan_masuk = (strtotime($realisasi_masuk) - strtotime($jadwal_masuk))/60;
												//$this->db->insert('coba_coba',array('keterangan'=>'keterlambatan_masuk: '.$keterlambatan_masuk,'waktu'=>date('Y-m-d H:i:s')));	
												$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
												//$this->db->insert('coba_coba',array('keterangan'=>'POTONGAN TELAH: '.$potongan_terlambat_masuk,'waktu'=>date('Y-m-d H:i:s')));	
												$realisasi_keluar = $tanggal;
												$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($realisasi_keluar))/60;
												if($kecepatan_keluar > 0)
												{
													$kecepatan_keluar = $kecepatan_keluar;
													$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
												}
												else
												{
													$kecepatan_keluar = 0;
													$potongan_pulang_cepat=0;
												}
												$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat;
												$si = "insert into d_absen_harian_tkk
														set nik = '$nik',
															tanggal = '$tgl',
															bulan = '$bln',
															tahun = '$thn',
															jadwal_masuk = '$jadwal_masuk',
															realisasi_masuk = '$realisasi_masuk',
															keterlambatan_masuk = '$keterlambatan_masuk',
															potongan_terlambat_masuk = '$potongan_terlambat_masuk',
															jadwal_keluar = '$jadwal_keluar', 
															realisasi_keluar = '$realisasi_keluar', 
															kecepatan_keluar = '$kecepatan_keluar', 
															potongan_pulang_cepat = '$potongan_pulang_cepat', 
															persentase_potongan = '$persentase_potongan', 
															libur = '$libur',
															update_date = '$tanggal',
															update_by = 'system'
														";
													//echo $si.'</br>';
													$this->db->insert('coba_coba',array('keterangan'=>$si,'waktu'=>date('Y-m-d H:i:s'),'nip'=>$nip));
													$qi = $this->db->query($si);
																					
											}
										
										}
										else
										{
											$libur = 1;
											$si = "insert into d_absen_harian_tkk
														set nik = '$nik',
															tanggal = '$tgl',
															bulan = '$bln',
															tahun = '$thn',
															libur = '$libur',
															update_date = '$tanggal',
															update_by = 'system'
														";
												//	echo $si.'</br>';
													$this->db->insert('coba_coba',array('keterangan'=>$si,'waktu'=>date('Y-m-d H:i:s'),'nip'=>$nip));
													$qi = $this->db->query($si);
										}
										
									}
									else
									{
										$sl = "select * from d_jadwal_lembur_tkk where nik = '$nik' and tanggal = '$tanggal' ";
										$ql = $this->db->query($sl);
										if($ql->num_rows() > 0)
										{
											$jl = $ql->row_array();
											if($jl['is_libur'] == 0)
											{
												$jadwal_masuk = $jl['jadwal_masuk'];
												$jadwal_keluar = $jl['jadwal_keluar'];		
												if($jadwal_masuk == '0000-00-00 00:00:00' || $jadwal_keluar == '0000-00-00 00:00:00')
												{
													$libur = 0;
													if($jadwal_masuk == '0000-00-00 00:00:00')
														{
															$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($tanggal))/60;
															if($kecepatan_keluar > 0)
															{
																$kecepatan_keluar = $kecepatan_keluar;
																$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
															}
															else
															{
																$kecepatan_keluar = 0;
																$potongan_pulang_cepat=0;
															}
															$persentase_potongan = $potongan_pulang_cepat;
															$su = "insert into d_absen_harian_tkk
																set nik = '$nik',
																	tanggal = '$tgl',
																	bulan = '$bln',
																	tahun = '$thn',
																	jadwal_masuk = '$jadwal_masuk',
																	jadwal_keluar = '$jadwal_keluar', 
																	realisasi_keluar = '$tanggal', 
																	kecepatan_keluar = '$kecepatan_keluar', 
																	potongan_pulang_cepat = '$potongan_pulang_cepat', 
																	persentase_potongan = '$persentase_potongan', 
																	libur = '$libur',
																	update_date = '$tanggal',
																	update_by = 'system'
															";
															$this->db->query($su);
															$this->db->insert('coba_coba',array('keterangan'=>$su,'waktu'=>date('Y-m-d H:i:s'),'nip'=>$nip));
														}
														elseif($jadwal_keluar == '0000-00-00 00:00:00')
														{
															
																$keterlambatan_masuk = (strtotime($tanggal) - strtotime($jadwal_masuk))/60;
																if($keterlambatan_masuk > 0)
																{
																	$keterlambatan_masuk = $keterlambatan_masuk;
																	$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
																}
																else
																{
																	$keterlambatan_masuk = 0;
																	$potongan_terlambat_masuk=0;
																}
																$persentase_potongan = $potongan_terlambat_masuk;
																$su = "insert into d_absen_harian_tkk
																set nik = '$nik',
																	tanggal = '$tgl',
																	bulan = '$bln',
																	tahun = '$thn',
																	jadwal_masuk = '$jadwal_masuk',
																	realisasi_masuk = '$tanggal',
																	keterlambatan_masuk = '$keterlambatan_masuk',
																	potongan_terlambat_masuk = '$potongan_terlambat_masuk',
																	jadwal_keluar = '$jadwal_keluar', 
																	persentase_potongan = '$persentase_potongan', 
																	libur = '$libur',
																	update_date = '$tanggal',
																	update_by = 'system'
																";
																$this->db->query($su);
																$this->db->insert('coba_coba',array('keterangan'=>$su,'waktu'=>date('Y-m-d H:i:s'),'nip'=>$nip));
															
														}
												}
												else
												{
													$hari_ini = date('w',strtotime($thn.'-'.$bln.'-'.$tgl));
													$jam_bagi_dua = (strtotime($jadwal_keluar) - strtotime($jadwal_masuk))/2;
													$istirahat = strtotime($jadwal_masuk)+$jam_bagi_dua;										
													$libur = 0;
													if(strtotime($tanggal)<$istirahat)
													{
														$this->db->insert('coba_coba',array('keterangan'=>'masuk','waktu'=>date('Y-m-d H:i:s')));
														
															$realisasi_masuk = $tanggal;
															$keterlambatan_masuk = (strtotime($realisasi_masuk) - strtotime($jadwal_masuk))/60;
															if($keterlambatan_masuk > 0)
															{
																$keterlambatan_masuk = $keterlambatan_masuk;
																$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
															}
															else
															{
																$keterlambatan_masuk = 0;
																$potongan_terlambat_masuk=0;
															}
															$persentase_potongan = $potongan_terlambat_masuk;
															$si = "insert into d_absen_harian_tkk
																set nik = '$nik',
																	tanggal = '$tgl',
																	bulan = '$bln',
																	tahun = '$thn',
																	jadwal_masuk = '$jadwal_masuk',
																	realisasi_masuk = '$realisasi_masuk',
																	keterlambatan_masuk = '$keterlambatan_masuk',
																	potongan_terlambat_masuk = '$potongan_terlambat_masuk',
																	jadwal_keluar = '$jadwal_keluar', 
																	libur = '$libur',
																	persentase_potongan = '$persentase_potongan',
																	update_date = '$tanggal',
																	update_by = 'system'
																";
															//echo $si.'</br>';
															$qi = $this->db->query($si);
															$this->db->insert('coba_coba',array('keterangan'=>$si,'waktu'=>date('Y-m-d H:i:s'),'nip'=>$nip));
														
													}
													else
													{
														$realisasi_masuk = $tanggal;	
														$keterlambatan_masuk = (strtotime($realisasi_masuk) - strtotime($jadwal_masuk))/60;
														//$this->db->insert('coba_coba',array('keterangan'=>'keterlambatan_masuk: '.$keterlambatan_masuk,'waktu'=>date('Y-m-d H:i:s')));	
														$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
														//$this->db->insert('coba_coba',array('keterangan'=>'POTONGAN TELAH: '.$potongan_terlambat_masuk,'waktu'=>date('Y-m-d H:i:s')));	
														$realisasi_keluar = $tanggal;
														$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($realisasi_keluar))/60;
														if($kecepatan_keluar > 0)
														{
															$kecepatan_keluar = $kecepatan_keluar;
															$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
														}
														else
														{
															$kecepatan_keluar = 0;
															$potongan_pulang_cepat=0;
														}
														$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat;
														$si = "insert into d_absen_harian_tkk
																set nik = '$nik',
																	tanggal = '$tgl',
																	bulan = '$bln',
																	tahun = '$thn',
																	jadwal_masuk = '$jadwal_masuk',
																	realisasi_masuk = '$realisasi_masuk',
																	keterlambatan_masuk = '$keterlambatan_masuk',
																	potongan_terlambat_masuk = '$potongan_terlambat_masuk',
																	jadwal_keluar = '$jadwal_keluar', 
																	realisasi_keluar = '$realisasi_keluar', 
																	kecepatan_keluar = '$kecepatan_keluar', 
																	potongan_pulang_cepat = '$potongan_pulang_cepat', 
																	persentase_potongan = '$persentase_potongan', 
																	libur = '$libur',
																	update_date = '$tanggal',
																	update_by = 'system'
																";
														//	echo $si.'</br>';
															$this->db->insert('coba_coba',array('keterangan'=>$si,'waktu'=>date('Y-m-d H:i:s'),'nip'=>$nip));
															$qi = $this->db->query($si);
																							
													}
													
												}
											}
											else
											{
												$libur = 1;
												$si = "insert into d_absen_harian_tkk
														set nik = '$nik',
															tanggal = '$tgl',
															bulan = '$bln',
															tahun = '$thn',
															libur = '$libur',
															update_date = '$tanggal',
															update_by = 'system'
														";
											//	echo $si.'</br>';
												$this->db->insert('coba_coba',array('keterangan'=>$si,'waktu'=>date('Y-m-d H:i:s'),'nip'=>$nip));
												$qi = $this->db->query($si);
											}
										}
									}
									
								}
								
								$this->update_data_rekap_keuangan_tkk($nik,$tanggal,$persentase_potongan);
							}
				
		  }
		  
		}
		fclose($myfile);
		
	}
	
	public function hapus_upload()
	{
		$id = $this->uri->segment(3);
		$sct = "select * from d_upload_log where id = '$id' ";
		$qct = $this->db->query($sct);
		if($qct->num_rows() > 0)
		{
			$rct = $qct->row();
			$sd = "delete from absensi_log_manual where id_upload = '$id'";
			$qd = $this->db->query($sd);
			if($rct->file_upload != '')
			{
				unlink('assets/log/'.$rct->file_upload);	
			}
			$d1 = "delete from d_upload_log where id = '$id' ";
			$this->db->query($d1);
		}
		redirect('/log/upload');
	}
	
	public function view_upload()
	{
		$id = $this->uri->segment(3);
		$s = "select * from absensi_log_manual where id_upload = '$id' ";
		$qs = $this->db->query($s);
		$data = array(
			'title' => 'Data Log Absensi Yang Diinput Manual',
			'subtitle' => '',
			'rows' => $qs,
			);
		$this->load->view('view_upload',$data);
	}
	
	public function combo_unit()
	{
		$array = array();
		if ($_GET['_name'] == 'unit') 
		{
			$unit = $_GET['_value'];
			if($unit != '100000000000')
			{
				$sql = "select * 
					from referensi_unit_kerja
					where mid(kunker,1,4) = '".substr($unit,0,4)."'
					and mid(kunker,7,6) = '000000'
					
					order by KUNKER asc
				";
				$sql = "SELECT * 
											FROM referensi_unit_kerja 
											WHERE (IF(MID('$unit',3,2)=10 || MID('$unit',3,2)=11,
												mid(kunker,1,6) = mid('$unit',1,6)  ,
												mid(kunker,1,4) = mid('$unit',1,4)  )
											)
											AND (IF(MID('$unit',3,2)=10 || MID('$unit',3,2)=11,
												mid(kunker,9,4) = '0000' ,
												mid(kunker,7,6) = '000000' )
											)
											
											ORDER BY KUNKER";
				//echo $sql;
				$query = $this->db->query($sql);
				foreach($query->result_array() as $row)
				{
					$array[] = array($row['KUNKER'] => $row['NUNKER']);
				}
			}
			else
			{
				$array[] = array('100000000000' => 'Semua');
			}
			
		}
		elseif ($_GET['_name'] == 'subunit') 
		{
			$subunit = $_GET['_value'];
			if($subunit != '100000000000')
			{
				$sql = "select * 
					from referensi_unit_kerja
					where mid(kunker,1,6) = '".substr($subunit,0,6)."'
					and mid(kunker,9,4) = '0000'
					
					order by KUNKER asc
				";
				$sql = "SELECT * 
											FROM referensi_unit_kerja 
											WHERE (IF(MID('$subunit',3,2)=10 || MID('$subunit',3,2)=11,
												mid(kunker,1,8) = mid('$subunit',1,8)  ,
												mid(kunker,1,6) = mid('$subunit',1,6)  )
											)
											AND (IF(MID('$subunit',3,2)=10 || MID('$subunit',3,2)=11,
												mid(kunker,11,2) = '00' ,
												mid(kunker,9,4) = '0000' )
											)
											
											ORDER BY KUNKER";
				//echo $sql;
				$query = $this->db->query($sql);
				foreach($query->result_array() as $row)
				{
					$array[] = array($row['KUNKER'] => $row['NUNKER']);
				}
			}
			else
			{
				$array[] = array('100000000000' => 'Semua');
			}
			
		}
		elseif ($_GET['_name'] == 'ssubunit') 
		{
			$ssubunit = $_GET['_value'];
			if($ssubunit != '100000000000')
			{
				$sql = "select * 
					from referensi_unit_kerja
					where mid(kunker,1,8) = '".substr($ssubunit,0,8)."'
					and mid(kunker,11,2) = '00'
					
					order by KUNKER asc
				";
				$sql = "SELECT * 
											FROM referensi_unit_kerja 
											WHERE (IF(MID('$ssubunit',3,2)=10 || MID('$ssubunit',3,2)=11,
												mid(kunker,1,10) = mid('$ssubunit',1,10)  ,
												mid(kunker,1,8) = mid('$ssubunit',1,8)  )
											)
											AND (IF(MID('$ssubunit',3,2)=10 || MID('$ssubunit',3,2)=11,
												1=1,
												mid(kunker,11,2) = '00' )
											)
											
											ORDER BY KUNKER";
				//echo $sql;
				$query = $this->db->query($sql);
				foreach($query->result_array() as $row)
				{
					$array[] = array($row['KUNKER'] => $row['NUNKER']);
				}
			}
			else
			{
				$array[] = array('100000000000' => 'Semua');
			}
			
		}
		elseif ($_GET['_name'] == 'sssubunit') 
		{
			$sssubunit = $_GET['_value'];
			if($sssubunit != '100000000000')
			{
				$sql = "SELECT a.id,
							a.serial_number,
							(select nunker from referensi_unit_kerja where kunker = a.lokasi ) as lokasi_mesin
						FROM mesin_absensi a
						where 1=1
						and a.lokasi = '$sssubunit'
						ORDER BY id";
				//echo $sql;
				$query = $this->db->query($sql);
				foreach($query->result_array() as $row)
				{
					$array[] = array($row['id'] => $row['serial_number'].'-'.$row['lokasi_mesin']);
				}
			}
			
		}
		
		echo json_encode( $array );
	}
	
	// source code update data
				
	function cek_status($nip)
	{
		$sc = "select nip from d_pegawai where nip = '$nip' ";
		$qc = $this->db->query($sc);
		if($qc->num_rows() > 0)
		{
			$status_pegawai = 1;
		}
		
		$sc1 = "select nik from d_tkk where nik = '$nip' ";
		$qc1 = $this->db->query($sc1);
		if($qc1->num_rows() > 0)
		{
			$status_tkk = 1;
		}
		
		if(isset($status_pegawai) && $status_pegawai == 1)
		{
			$status = 1;
		}
		elseif(isset($status_tkk) && $status_tkk == 1)
		{
			$status = 2;
		}
		else
		{
			$status = '';
		}
		return $status;
	}
	
	/* function cek_libur()
	{
		$tgl = date('Y-m-d');
		$urutan_hari = date('w');
		$sc = "select * from d_hari_libur where tanggal = '$tgl' ";
		$qc = $this->db->query($sc);
		if($qc->num_rows() > 0)
		{
			$tanggal_merah = 1;
		}
		if((isset($tanggal_merah) && $tanggal_merah == 1) or ($urutan_hari == 0 || $urutan_hari == 6))
		{
			$libur = 1;
		}
		else
		{
			$libur = 0;
		}
		return $libur;
	} */
	
	function refresh_kehadiran($nip,$tanggal)
	{
		$cek_status = $this->cek_status($nip);
			//	echo 'stats;'.$cek_status;
							if($cek_status == 1)
							{
								$tgl = date('j',strtotime($tanggal));
								$bln = date('n',strtotime($tanggal));
								$thn = date('Y',strtotime($tanggal));
								$jam = date('G',strtotime($tanggal));
								$menit = date('i',strtotime($tanggal));
								$scj = "select *
										from d_absen_harian
										where nip = '$nip'
										and	tanggal = '$tgl'
										and	bulan = '$bln'
										and	tahun = '$thn'
								";
							//	echo $scj;
								$qcj = $this->db->query($scj);
								if($qcj->num_rows() > 0)
								{
									$st = "select a.*,
											b.*
											from d_pegawai_tipe_jadwal a
											left join ref_tipe_jadwal b on(a.tipe_jadwal = b.id)
											where nip = '$nip' 									
									";
									$rst = $this->db->query($st)->row_array();
									//if($rst['tipe_jadwal']=='1')
									if($rst['shift'] == 0)
									{
										//$cek_libur = $this->cek_libur;
										//$cek_libur = $this->cek_libur($tanggal);
										$cek_libur = $this->cek_libur($tanggal,$rst['sabtu_masuk']);
										if($cek_libur == 0)
										{
											$rcj = $qcj->row();
											$id_absen_harian = $rcj->id;
											$jadwal_masuk = $rcj->jadwal_masuk;
											$realisasi_masuk = $rcj->realisasi_masuk;
											$jadwal_keluar = $rcj->jadwal_keluar;
											$realisasi_keluar = $rcj->realisasi_keluar;
											$potongan_terlambat_masuk = $rcj->potongan_terlambat_masuk;
											$potongan_pulang_cepat = $rcj->potongan_pulang_cepat;
											$potongan_tidak_masuk = $rcj->potongan_tidak_masuk;
											
											if($jadwal_masuk == '0000-00-00 00:00:00' || $jadwal_keluar == '0000-00-00 00:00:00')
											{
												if($jadwal_masuk == '0000-00-00 00:00:00')
												{
													$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($tanggal))/60;
													if($kecepatan_keluar > 0)
													{
														$kecepatan_keluar = $kecepatan_keluar;
														$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
													}
													else
													{
														$kecepatan_keluar = 0;
														$potongan_pulang_cepat=0;
													}
													$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
													$su = "update d_absen_harian
																	set realisasi_keluar = '$tanggal',
																		kecepatan_keluar = '$kecepatan_keluar',
																		potongan_pulang_cepat = '$potongan_pulang_cepat',
																		persentase_potongan = '$persentase_potongan',     
																		update_date = '$tanggal',
																		update_by = 'system'
																	where id = '$id_absen_harian'
													";
													$this->db->query($su);
												//	echo $su.'</br>';
												}
												elseif($jadwal_keluar == '0000-00-00 00:00:00')
												{
													if($realisasi_masuk == NULL)
													{
														$keterlambatan_masuk = (strtotime($tanggal) - strtotime($jadwal_masuk))/60;
														if($keterlambatan_masuk > 0)
														{
															$keterlambatan_masuk = $keterlambatan_masuk;
															$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
														}
														else
														{
															$keterlambatan_masuk = 0;
															$potongan_terlambat_masuk=0;
														}
														$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
														$su = "update d_absen_harian
																		set realisasi_masuk = '$tanggal',
																			keterlambatan_masuk = '$keterlambatan_masuk',
																			potongan_terlambat_masuk = '$potongan_terlambat_masuk',
																			persentase_potongan = '$persentase_potongan',     
																			update_date = '$tanggal',
																			update_by = 'system'
																		where id = '$id_absen_harian'
														";
														$this->db->query($su);
													//	echo $su.'</br>';
													}
												}
											}
											else
											{
												$jam_bagi_dua = (strtotime($jadwal_keluar) - strtotime($jadwal_masuk))/2;
												$istirahat = strtotime($jadwal_masuk)+$jam_bagi_dua;
												if(strtotime($tanggal)<$istirahat)
												{
													if($realisasi_masuk == NULL)
													{
														$keterlambatan_masuk = (strtotime($tanggal) - strtotime($jadwal_masuk))/60;
														if($keterlambatan_masuk > 0)
														{
															$keterlambatan_masuk = $keterlambatan_masuk;
															$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
														}
														else
														{
															$keterlambatan_masuk = 0;
															$potongan_terlambat_masuk=0;
														}
														
														$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($tanggal))/60;
														if($kecepatan_keluar > 0)
														{
															$kecepatan_keluar = $kecepatan_keluar;
															$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
														}
														else
														{
															$kecepatan_keluar = 0;
															$potongan_pulang_cepat=0;
														}
														$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
														$su = "update d_absen_harian
																	set realisasi_masuk = '$tanggal',
																		keterlambatan_masuk = '$keterlambatan_masuk',
																		potongan_terlambat_masuk = '$potongan_terlambat_masuk',
																		realisasi_keluar = '$tanggal',
																		kecepatan_keluar = '$kecepatan_keluar',
																		potongan_pulang_cepat = '$potongan_pulang_cepat',
																		persentase_potongan = '$persentase_potongan',     
																		update_date = '$tanggal',
																		update_by = 'system'
																	where id = '$id_absen_harian'
														";
														$this->db->query($su);
													//	echo $su.'</br>';
													}
													else
													{
														
														$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($tanggal))/60;
														if($kecepatan_keluar > 0)
														{
															$kecepatan_keluar = $kecepatan_keluar;
															$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
														}
														else
														{
															$kecepatan_keluar = 0;
															$potongan_pulang_cepat=0;
														}
														$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
														$su = "update d_absen_harian
																	set realisasi_keluar = '$tanggal',
																		kecepatan_keluar = '$kecepatan_keluar',
																		potongan_pulang_cepat = '$potongan_pulang_cepat',
																		persentase_potongan = '$persentase_potongan',     
																		update_date = '$tanggal',
																		update_by = 'system'
																	where id = '$id_absen_harian'
														";
														$this->db->query($su);
													//	echo $su.'</br>';
													}
												}
												else
												{
													if($realisasi_masuk == NULL)
													{
														$realisasi_masuk = $tanggal;	
														$keterlambatan_masuk = (strtotime($realisasi_masuk) - strtotime($jadwal_masuk))/60;
														//$this->db->insert('coba_coba',array('keterangan'=>'keterlambatan_masuk: '.$keterlambatan_masuk,'waktu'=>date('Y-m-d H:i:s')));	
														$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
														//$this->db->insert('coba_coba',array('keterangan'=>'POTONGAN TELAH: '.$potongan_terlambat_masuk,'waktu'=>date('Y-m-d H:i:s')));	
														$realisasi_keluar = $tanggal;							
														$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($realisasi_keluar))/60;
														if($kecepatan_keluar > 0)
														{
															$kecepatan_keluar = $kecepatan_keluar;
															$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
														}
														else
														{
															$kecepatan_keluar = 0;
															$potongan_pulang_cepat=0;
														}
														$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
														$su = "update d_absen_harian
																	set jadwal_masuk = '$jadwal_masuk',
																		realisasi_masuk = '$realisasi_masuk',
																		keterlambatan_masuk = '$keterlambatan_masuk',
																		potongan_terlambat_masuk = '$potongan_terlambat_masuk',
																		jadwal_keluar = '$jadwal_keluar', 
																		realisasi_keluar = '$realisasi_keluar', 
																		kecepatan_keluar = '$kecepatan_keluar', 
																		potongan_pulang_cepat = '$potongan_pulang_cepat', 
																		persentase_potongan = '$persentase_potongan', 
																		update_date = '$tanggal',
																		update_by = 'system'
																	where id = '$id_absen_harian'
														";
														$this->db->query($su);
													//	echo $su.'</br>';
													}
													else
													{
														$realisasi_keluar = $tanggal;							
														$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($realisasi_keluar))/60;
														if($kecepatan_keluar > 0)
														{
															$kecepatan_keluar = $kecepatan_keluar;
															$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
														}
														else
														{
															$kecepatan_keluar = 0;
															$potongan_pulang_cepat=0;
														}
														$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
														$su = "update d_absen_harian
																	set realisasi_keluar = '$realisasi_keluar', 
																		kecepatan_keluar = '$kecepatan_keluar', 
																		potongan_pulang_cepat = '$potongan_pulang_cepat', 
																		persentase_potongan = '$persentase_potongan', 
																		update_date = '$tanggal',
																		update_by = 'system'
																	where id = '$id_absen_harian'
														";
														$this->db->query($su);
													//	echo $su.'</br>';
													}
												}
												
											}
										}
										else
										{
											$persentase_potongan = 0;
										}
									}
									else
									{
										$sl = "select * from d_jadwal_lembur where nip = '$nip' and tanggal = '$tanggal' ";
										$ql = $this->db->query($sl);
										if($ql->num_rows() > 0)
										{
											$jl = $ql->row_array();
											if($jl['is_libur'] == 0)
											{
												//$jadwal_masuk = $jl['jadwal_masuk'];
												//$jadwal_keluar = $jl['jadwal_keluar'];	
												$rcj = $qcj->row();
												$id_absen_harian = $rcj->id;
												$jadwal_masuk = $rcj->jadwal_masuk;
												$realisasi_masuk = $rcj->realisasi_masuk;
												$jadwal_keluar = $rcj->jadwal_keluar;
												$realisasi_keluar = $rcj->realisasi_keluar;
												$potongan_terlambat_masuk = $rcj->potongan_terlambat_masuk;
												$potongan_pulang_cepat = $rcj->potongan_pulang_cepat;
												$potongan_tidak_masuk = $rcj->potongan_tidak_masuk;
												
												if($jadwal_masuk == '0000-00-00 00:00:00' || $jadwal_keluar == '0000-00-00 00:00:00')
												{
													if($jadwal_masuk == '0000-00-00 00:00:00')
													{
														$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($tanggal))/60;
														if($kecepatan_keluar > 0)
														{
															$kecepatan_keluar = $kecepatan_keluar;
															$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
														}
														else
														{
															$kecepatan_keluar = 0;
															$potongan_pulang_cepat=0;
														}
														$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
														$su = "update d_absen_harian
																		set realisasi_keluar = '$tanggal',
																			kecepatan_keluar = '$kecepatan_keluar',
																			potongan_pulang_cepat = '$potongan_pulang_cepat',
																			persentase_potongan = '$persentase_potongan',     
																			update_date = '$tanggal',
																			update_by = 'system'
																		where id = '$id_absen_harian'
														";
														$this->db->query($su);
													//	echo $su.'</br>';
													}
													elseif($jadwal_keluar == '0000-00-00 00:00:00')
													{
														if($realisasi_masuk == NULL)
														{
															$keterlambatan_masuk = (strtotime($tanggal) - strtotime($jadwal_masuk))/60;
															if($keterlambatan_masuk > 0)
															{
																$keterlambatan_masuk = $keterlambatan_masuk;
																$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
															}
															else
															{
																$keterlambatan_masuk = 0;
																$potongan_terlambat_masuk=0;
															}
															$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
															$su = "update d_absen_harian
																			set realisasi_masuk = '$tanggal',
																				keterlambatan_masuk = '$keterlambatan_masuk',
																				potongan_terlambat_masuk = '$potongan_terlambat_masuk',
																				persentase_potongan = '$persentase_potongan',     
																				update_date = '$tanggal',
																				update_by = 'system'
																			where id = '$id_absen_harian'
															";
															$this->db->query($su);
														//	echo $su.'</br>';
														}
													}
												}
												else
												{
													$jam_bagi_dua = (strtotime($jadwal_keluar) - strtotime($jadwal_masuk))/2;
													$istirahat = strtotime($jadwal_masuk)+$jam_bagi_dua;
													if(strtotime($tanggal)<$istirahat)
													{
														if($realisasi_masuk == NULL)
														{
															$keterlambatan_masuk = (strtotime($tanggal) - strtotime($jadwal_masuk))/60;
															if($keterlambatan_masuk > 0)
															{
																$keterlambatan_masuk = $keterlambatan_masuk;
																$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
															}
															else
															{
																$keterlambatan_masuk = 0;
																$potongan_terlambat_masuk=0;
															}
															
															$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($tanggal))/60;
															if($kecepatan_keluar > 0)
															{
																$kecepatan_keluar = $kecepatan_keluar;
																$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
															}
															else
															{
																$kecepatan_keluar = 0;
																$potongan_pulang_cepat=0;
															}
															$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
															$su = "update d_absen_harian
																		set realisasi_masuk = '$tanggal',
																			keterlambatan_masuk = '$keterlambatan_masuk',
																			potongan_terlambat_masuk = '$potongan_terlambat_masuk',
																			realisasi_keluar = '$tanggal',
																			kecepatan_keluar = '$kecepatan_keluar',
																			potongan_pulang_cepat = '$potongan_pulang_cepat',
																			persentase_potongan = '$persentase_potongan',     
																			update_date = '$tanggal',
																			update_by = 'system'
																		where id = '$id_absen_harian'
															";
															$this->db->query($su);
														//	echo $su.'</br>';
														}
														else
														{
															
															$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($tanggal))/60;
															if($kecepatan_keluar > 0)
															{
																$kecepatan_keluar = $kecepatan_keluar;
																$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
															}
															else
															{
																$kecepatan_keluar = 0;
																$potongan_pulang_cepat=0;
															}
															$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
															$su = "update d_absen_harian
																		set realisasi_keluar = '$tanggal',
																			kecepatan_keluar = '$kecepatan_keluar',
																			potongan_pulang_cepat = '$potongan_pulang_cepat',
																			persentase_potongan = '$persentase_potongan',     
																			update_date = '$tanggal',
																			update_by = 'system'
																		where id = '$id_absen_harian'
															";
															$this->db->query($su);
														//	echo $su.'</br>';
														}
													}
													else
													{
														if($realisasi_masuk == NULL)
														{
															$realisasi_masuk = $tanggal;	
															$keterlambatan_masuk = (strtotime($realisasi_masuk) - strtotime($jadwal_masuk))/60;
															//$this->db->insert('coba_coba',array('keterangan'=>'keterlambatan_masuk: '.$keterlambatan_masuk,'waktu'=>date('Y-m-d H:i:s')));	
															$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
															//$this->db->insert('coba_coba',array('keterangan'=>'POTONGAN TELAH: '.$potongan_terlambat_masuk,'waktu'=>date('Y-m-d H:i:s')));	
															$realisasi_keluar = $tanggal;							
															$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($realisasi_keluar))/60;
															if($kecepatan_keluar > 0)
															{
																$kecepatan_keluar = $kecepatan_keluar;
																$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
															}
															else
															{
																$kecepatan_keluar = 0;
																$potongan_pulang_cepat=0;
															}
															$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
															$su = "update d_absen_harian
																		set jadwal_masuk = '$jadwal_masuk',
																			realisasi_masuk = '$realisasi_masuk',
																			keterlambatan_masuk = '$keterlambatan_masuk',
																			potongan_terlambat_masuk = '$potongan_terlambat_masuk',
																			jadwal_keluar = '$jadwal_keluar', 
																			realisasi_keluar = '$realisasi_keluar', 
																			kecepatan_keluar = '$kecepatan_keluar', 
																			potongan_pulang_cepat = '$potongan_pulang_cepat', 
																			persentase_potongan = '$persentase_potongan', 
																			update_date = '$tanggal',
																			update_by = 'system'
																		where id = '$id_absen_harian'
															";
															$this->db->query($su);
														//	echo $su.'</br>';
														}
														else
														{
															$realisasi_keluar = $tanggal;							
															$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($realisasi_keluar))/60;
															if($kecepatan_keluar > 0)
															{
																$kecepatan_keluar = $kecepatan_keluar;
																$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
															}
															else
															{
																$kecepatan_keluar = 0;
																$potongan_pulang_cepat=0;
															}
															$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
															$su = "update d_absen_harian
																		set realisasi_keluar = '$realisasi_keluar', 
																			kecepatan_keluar = '$kecepatan_keluar', 
																			potongan_pulang_cepat = '$potongan_pulang_cepat', 
																			persentase_potongan = '$persentase_potongan', 
																			update_date = '$tanggal',
																			update_by = 'system'
																		where id = '$id_absen_harian'
															";
															$this->db->query($su);
														//	echo $su.'</br>';
														}
													}
													
												}
											}
											else
											{
												$persentase_potongan = 0;
											}
										}
									}
								}
								else
								{
									$st = "select a.*,
											b.*
											from d_pegawai_tipe_jadwal a
											left join ref_tipe_jadwal b on(a.tipe_jadwal = b.id)
											where nip = '$nip' 									
									";
									$rst = $this->db->query($st)->row_array();
									//if($rst['tipe_jadwal']=='1')
									if($rst['shift'] == 0)
									{
									//	$cek_libur = $this->cek_libur;
									//	$cek_libur = $this->cek_libur($tanggal);
										$cek_libur = $this->cek_libur($tanggal,$rst['sabtu_masuk']);
									//	echo 'ceklibr:'.$cek_libur;
										if($cek_libur == 0)
										{
											//cek jika jadwal khusus
											$hari_ini = date('w',strtotime($thn.'-'.$bln.'-'.$tgl));
											$sc = "select * from referensi_jadwal_khusus where hari = '$hari_ini' and aktif = 1 ";
											$qc = $this->db->query($sc);
											if($qc->num_rows() > 0)
											{
												$rjk = $qc->row_array();
												$jadwal_jam_masuk = $rjk['jadwal_jam_masuk'];
												$jadwal_menit_masuk = $rjk['jadwal_menit_masuk'];
												$jadwal_masuk = date('Y-m-d H:i',strtotime($tgl.'-'.$bln.'-'.$thn.' '.$jadwal_jam_masuk.':'.$jadwal_menit_masuk));
												$jadwal_jam_keluar = $rjk['jadwal_jam_keluar'];
												$jadwal_menit_keluar = $rjk['jadwal_menit_keluar'];
												$jadwal_keluar = date('Y-m-d H:i',strtotime($tgl.'-'.$bln.'-'.$thn.' '.$jadwal_jam_keluar.':'.$jadwal_menit_keluar));	
											}
											else
											{
												$jadwal_jam_masuk = $rst['jadwal_jam_masuk'];
												$jadwal_menit_masuk = $rst['jadwal_menit_masuk'];
												$jadwal_masuk = date('Y-m-d H:i',strtotime($tgl.'-'.$bln.'-'.$thn.' '.$jadwal_jam_masuk.':'.$jadwal_menit_masuk));
												$jadwal_jam_keluar = $rst['jadwal_jam_keluar'];
												$jadwal_menit_keluar = $rst['jadwal_menit_keluar'];
												$jadwal_keluar = date('Y-m-d H:i',strtotime($tgl.'-'.$bln.'-'.$thn.' '.$jadwal_jam_keluar.':'.$jadwal_menit_keluar));	
											}
											
											
											$jam_bagi_dua = (strtotime($jadwal_keluar) - strtotime($jadwal_masuk))/2;
											$istirahat = strtotime($jadwal_masuk)+$jam_bagi_dua;										
											$libur = 0;
											if(strtotime($tanggal)<$istirahat)
											{
													$realisasi_masuk = $tanggal;
													$keterlambatan_masuk = (strtotime($realisasi_masuk) - strtotime($jadwal_masuk))/60;
													if($keterlambatan_masuk > 0)
													{
														$keterlambatan_masuk = $keterlambatan_masuk;
														$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
													}
													else
													{
														$keterlambatan_masuk = 0;
														$potongan_terlambat_masuk=0;
													}
													$persentase_potongan = $potongan_terlambat_masuk;
													$si = "insert into d_absen_harian
														set nip = '$nip',
															tanggal = '$tgl',
															bulan = '$bln',
															tahun = '$thn',
															jadwal_masuk = '$jadwal_masuk',
															realisasi_masuk = '$realisasi_masuk',
															keterlambatan_masuk = '$keterlambatan_masuk',
															potongan_terlambat_masuk = '$potongan_terlambat_masuk',
															jadwal_keluar = '$jadwal_keluar', 
															libur = '$libur',
															persentase_potongan = '$persentase_potongan',
															update_date = '$tanggal',
															update_by = 'system'
														";
												//	echo $si.'</br>';
													$qi = $this->db->query($si);
												
											}
											else
											{
												$realisasi_masuk = $tanggal;	
												$keterlambatan_masuk = (strtotime($realisasi_masuk) - strtotime($jadwal_masuk))/60;
												//$this->db->insert('coba_coba',array('keterangan'=>'keterlambatan_masuk: '.$keterlambatan_masuk,'waktu'=>date('Y-m-d H:i:s')));	
												$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
												//$this->db->insert('coba_coba',array('keterangan'=>'POTONGAN TELAH: '.$potongan_terlambat_masuk,'waktu'=>date('Y-m-d H:i:s')));	
												$realisasi_keluar = $tanggal;
												$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($realisasi_keluar))/60;
												if($kecepatan_keluar > 0)
												{
													$kecepatan_keluar = $kecepatan_keluar;
													$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
												}
												else
												{
													$kecepatan_keluar = 0;
													$potongan_pulang_cepat=0;
												}
												$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat;
												$si = "insert into d_absen_harian
														set nip = '$nip',
															tanggal = '$tgl',
															bulan = '$bln',
															tahun = '$thn',
															jadwal_masuk = '$jadwal_masuk',
															realisasi_masuk = '$realisasi_masuk',
															keterlambatan_masuk = '$keterlambatan_masuk',
															potongan_terlambat_masuk = '$potongan_terlambat_masuk',
															jadwal_keluar = '$jadwal_keluar', 
															realisasi_keluar = '$realisasi_keluar', 
															kecepatan_keluar = '$kecepatan_keluar', 
															potongan_pulang_cepat = '$potongan_pulang_cepat', 
															persentase_potongan = '$persentase_potongan', 
															libur = '$libur',
															update_date = '$tanggal',
															update_by = 'system'
														";
												//	echo $si.'</br>';
													$qi = $this->db->query($si);
																					
											}
										
										}
										else
										{
											$libur = 1;
											$si = "insert into d_absen_harian
														set nip = '$nip',
															tanggal = '$tgl',
															bulan = '$bln',
															tahun = '$thn',
															libur = '$libur',
															update_date = '$tanggal',
															update_by = 'system'
														";
												//	echo $si.'</br>';
													$qi = $this->db->query($si);
										}
										
									}
									else
									{
										$sl = "select * from d_jadwal_lembur where nip = '$nip' and tanggal = '$tanggal' ";
										$ql = $this->db->query($sl);
										if($ql->num_rows() > 0)
										{
											$jl = $ql->row_array();
											if($jl['is_libur'] == 0)
											{
												$jadwal_masuk = $jl['jadwal_masuk'];
												$jadwal_keluar = $jl['jadwal_keluar'];	
												if($jadwal_masuk == '0000-00-00 00:00:00' || $jadwal_keluar == '0000-00-00 00:00:00')
												{
													$libur = 0;
													if($jadwal_masuk == '0000-00-00 00:00:00')
														{
															$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($tanggal))/60;
															if($kecepatan_keluar > 0)
															{
																$kecepatan_keluar = $kecepatan_keluar;
																$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
															}
															else
															{
																$kecepatan_keluar = 0;
																$potongan_pulang_cepat=0;
															}
															$persentase_potongan = $potongan_pulang_cepat;
															$su = "insert into d_absen_harian
																set nip = '$nip',
																	tanggal = '$tgl',
																	bulan = '$bln',
																	tahun = '$thn',
																	jadwal_masuk = '$jadwal_masuk',
																	jadwal_keluar = '$jadwal_keluar', 
																	realisasi_keluar = '$tanggal', 
																	kecepatan_keluar = '$kecepatan_keluar', 
																	potongan_pulang_cepat = '$potongan_pulang_cepat', 
																	persentase_potongan = '$persentase_potongan', 
																	libur = '$libur',
																	update_date = '$tanggal',
																	update_by = 'system'
															";
															$this->db->query($su);
														//	echo $su.'</br>';
														}
														elseif($jadwal_keluar == '0000-00-00 00:00:00')
														{
															
																$keterlambatan_masuk = (strtotime($tanggal) - strtotime($jadwal_masuk))/60;
																if($keterlambatan_masuk > 0)
																{
																	$keterlambatan_masuk = $keterlambatan_masuk;
																	$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
																}
																else
																{
																	$keterlambatan_masuk = 0;
																	$potongan_terlambat_masuk=0;
																}
																$persentase_potongan = $potongan_terlambat_masuk;
																$su = "insert into d_absen_harian
																set nip = '$nip',
																	tanggal = '$tgl',
																	bulan = '$bln',
																	tahun = '$thn',
																	jadwal_masuk = '$jadwal_masuk',
																	realisasi_masuk = '$tanggal',
																	keterlambatan_masuk = '$keterlambatan_masuk',
																	potongan_terlambat_masuk = '$potongan_terlambat_masuk',
																	jadwal_keluar = '$jadwal_keluar', 
																	persentase_potongan = '$persentase_potongan', 
																	libur = '$libur',
																	update_date = '$tanggal',
																	update_by = 'system'
																";
																$this->db->query($su);
														//		echo $su.'</br>';
															
														}
												}
												else
												{
													$hari_ini = date('w',strtotime($thn.'-'.$bln.'-'.$tgl));
													$jam_bagi_dua = (strtotime($jadwal_keluar) - strtotime($jadwal_masuk))/2;
													$istirahat = strtotime($jadwal_masuk)+$jam_bagi_dua;										
													$libur = 0;
													if(strtotime($tanggal)<$istirahat)
													{
															$realisasi_masuk = $tanggal;
															$keterlambatan_masuk = (strtotime($realisasi_masuk) - strtotime($jadwal_masuk))/60;
															if($keterlambatan_masuk > 0)
															{
																$keterlambatan_masuk = $keterlambatan_masuk;
																$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
															}
															else
															{
																$keterlambatan_masuk = 0;
																$potongan_terlambat_masuk=0;
															}
															$persentase_potongan = $potongan_terlambat_masuk;
															$si = "insert into d_absen_harian
																set nip = '$nip',
																	tanggal = '$tgl',
																	bulan = '$bln',
																	tahun = '$thn',
																	jadwal_masuk = '$jadwal_masuk',
																	realisasi_masuk = '$realisasi_masuk',
																	keterlambatan_masuk = '$keterlambatan_masuk',
																	potongan_terlambat_masuk = '$potongan_terlambat_masuk',
																	jadwal_keluar = '$jadwal_keluar', 
																	libur = '$libur',
																	persentase_potongan = '$persentase_potongan',
																	update_date = '$tanggal',
																	update_by = 'system'
																";
														//	echo $si.'</br>';
															$qi = $this->db->query($si);
														
													}
													else
													{
														$realisasi_masuk = $tanggal;	
														$keterlambatan_masuk = (strtotime($realisasi_masuk) - strtotime($jadwal_masuk))/60;
														//$this->db->insert('coba_coba',array('keterangan'=>'keterlambatan_masuk: '.$keterlambatan_masuk,'waktu'=>date('Y-m-d H:i:s')));	
														$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
														//$this->db->insert('coba_coba',array('keterangan'=>'POTONGAN TELAH: '.$potongan_terlambat_masuk,'waktu'=>date('Y-m-d H:i:s')));	
														$realisasi_keluar = $tanggal;
														$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($realisasi_keluar))/60;
														if($kecepatan_keluar > 0)
														{
															$kecepatan_keluar = $kecepatan_keluar;
															$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
														}
														else
														{
															$kecepatan_keluar = 0;
															$potongan_pulang_cepat=0;
														}
														$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat;
														$si = "insert into d_absen_harian
																set nip = '$nip',
																	tanggal = '$tgl',
																	bulan = '$bln',
																	tahun = '$thn',
																	jadwal_masuk = '$jadwal_masuk',
																	realisasi_masuk = '$realisasi_masuk',
																	keterlambatan_masuk = '$keterlambatan_masuk',
																	potongan_terlambat_masuk = '$potongan_terlambat_masuk',
																	jadwal_keluar = '$jadwal_keluar', 
																	realisasi_keluar = '$realisasi_keluar', 
																	kecepatan_keluar = '$kecepatan_keluar', 
																	potongan_pulang_cepat = '$potongan_pulang_cepat', 
																	persentase_potongan = '$persentase_potongan', 
																	libur = '$libur',
																	update_date = '$tanggal',
																	update_by = 'system'
																";
														//	echo $si.'</br>';
															$qi = $this->db->query($si);
																							
													}
										
												}
												
											}
											else
											{
												$libur = 1;
												$si = "insert into d_absen_harian
														set nip = '$nip',
															tanggal = '$tgl',
															bulan = '$bln',
															tahun = '$thn',
															libur = '$libur',
															update_date = '$tanggal',
															update_by = 'system'
														";
												//echo $si.'</br>';
												$qi = $this->db->query($si);
											}
										}
									}
									
								}
								
								$this->update_data_rekap_keuangan($nip,$tanggal,$persentase_potongan);
							}
							elseif($cek_status == 2)
							{
								$nik = $nip;
								$tgl = date('j',strtotime($tanggal));
								$bln = date('n',strtotime($tanggal));
								$thn = date('Y',strtotime($tanggal));
								$jam = date('G',strtotime($tanggal));
								$menit = date('i',strtotime($tanggal));
								$scj = "select *
										from d_absen_harian_tkk
										where nik = '$nik'
										and	tanggal = '$tgl'
										and	bulan = '$bln'
										and	tahun = '$thn'
								";
							//	echo $scj.'</br>';
								$qcj = $this->db->query($scj);
								if($qcj->num_rows() > 0)
								{
									$st = "select a.*,
														b.*
												from d_pegawai_tipe_jadwal_tkk a
												left join ref_tipe_jadwal b on(a.tipe_jadwal = b.id)
												where nik = '$nip' 
												
												";
										$rst = $this->db->query($st)->row_array();
										//if($rst['tipe_jadwal'] == 1)
										if($rst['shift'] == 0)
										{
											//$cek_libur = $this->cek_libur($tanggal);
											$cek_libur = $this->cek_libur($tgl,$rst['sabtu_masuk']);
											if($cek_libur == 0)
											{
												$rcj = $qcj->row();
												$id_absen_harian = $rcj->id;
												$jadwal_masuk = $rcj->jadwal_masuk;
												$realisasi_masuk = $rcj->realisasi_masuk;
												$jadwal_keluar = $rcj->jadwal_keluar;
												$realisasi_keluar = $rcj->realisasi_keluar;
												$potongan_terlambat_masuk = $rcj->potongan_terlambat_masuk;
												$potongan_pulang_cepat = $rcj->potongan_pulang_cepat;
												$potongan_tidak_masuk = $rcj->potongan_tidak_masuk;
												if($jadwal_masuk == '0000-00-00 00:00:00' || $jadwal_keluar == '0000-00-00 00:00:00')
												{
													if($jadwal_masuk == '0000-00-00 00:00:00')
													{
														$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($tanggal))/60;
														if($kecepatan_keluar > 0)
														{
															$kecepatan_keluar = $kecepatan_keluar;
															$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
														}
														else
														{
															$kecepatan_keluar = 0;
															$potongan_pulang_cepat=0;
														}
														$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
														$su = "update d_absen_harian_tkk
																		set realisasi_keluar = '$tanggal',
																			kecepatan_keluar = '$kecepatan_keluar',
																			potongan_pulang_cepat = '$potongan_pulang_cepat',
																			persentase_potongan = '$persentase_potongan',     
																			update_date = '$tanggal',
																			update_by = 'system'
																		where id = '$id_absen_harian'
														";
														$this->db->query($su);
													//	echo $su.'</br>';
													}
													elseif($jadwal_keluar == '0000-00-00 00:00:00')
													{
														if($realisasi_masuk == NULL)
														{
															$keterlambatan_masuk = (strtotime($tanggal) - strtotime($jadwal_masuk))/60;
															if($keterlambatan_masuk > 0)
															{
																$keterlambatan_masuk = $keterlambatan_masuk;
																$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
															}
															else
															{
																$keterlambatan_masuk = 0;
																$potongan_terlambat_masuk=0;
															}
															$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
															$su = "update d_absen_harian_tkk
																			set realisasi_masuk = '$tanggal',
																				keterlambatan_masuk = '$keterlambatan_masuk',
																				potongan_terlambat_masuk = '$potongan_terlambat_masuk',
																				persentase_potongan = '$persentase_potongan',     
																				update_date = '$tanggal',
																				update_by = 'system'
																			where id = '$id_absen_harian'
															";
															$this->db->query($su);
														//	echo $su.'</br>';
														}
													}
												}
												else
												{
													$jam_bagi_dua = (strtotime($jadwal_keluar) - strtotime($jadwal_masuk))/2;
													$istirahat = strtotime($jadwal_masuk)+$jam_bagi_dua;
													if(strtotime($tanggal)<$istirahat)
													{
														if($realisasi_masuk == NULL)
														{
															$keterlambatan_masuk = (strtotime($tanggal) - strtotime($jadwal_masuk))/60;
															if($keterlambatan_masuk > 0)
															{
																$keterlambatan_masuk = $keterlambatan_masuk;
																$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
															}
															else
															{
																$keterlambatan_masuk = 0;
																$potongan_terlambat_masuk=0;
															}
															
															$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($tanggal))/60;
															if($kecepatan_keluar > 0)
															{
																$kecepatan_keluar = $kecepatan_keluar;
																$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
															}
															else
															{
																$kecepatan_keluar = 0;
																$potongan_pulang_cepat=0;
															}
															$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
															$su = "update d_absen_harian_tkk
																		set realisasi_masuk = '$tanggal',
																			keterlambatan_masuk = '$keterlambatan_masuk',
																			potongan_terlambat_masuk = '$potongan_terlambat_masuk',
																			realisasi_keluar = '$tanggal',
																			kecepatan_keluar = '$kecepatan_keluar',
																			potongan_pulang_cepat = '$potongan_pulang_cepat',
																			persentase_potongan = '$persentase_potongan',     
																			update_date = '$tanggal',
																			update_by = 'system'
																		where id = '$id_absen_harian'
															";
															$this->db->query($su);
														//	echo $su.'</br>';
														}
														else
														{
															
															$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($tanggal))/60;
															if($kecepatan_keluar > 0)
															{
																$kecepatan_keluar = $kecepatan_keluar;
																$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
															}
															else
															{
																$kecepatan_keluar = 0;
																$potongan_pulang_cepat=0;
															}
															$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
															$su = "update d_absen_harian_tkk
																		set realisasi_keluar = '$tanggal',
																			kecepatan_keluar = '$kecepatan_keluar',
																			potongan_pulang_cepat = '$potongan_pulang_cepat',
																			persentase_potongan = '$persentase_potongan',     
																			update_date = '$tanggal',
																			update_by = 'system'
																		where id = '$id_absen_harian'
															";
															$this->db->query($su);
														//	echo $su.'</br>';
														}
													}
													else
													{
														if($realisasi_masuk == NULL)
														{
															$realisasi_masuk = $tanggal;	
															$keterlambatan_masuk = (strtotime($realisasi_masuk) - strtotime($jadwal_masuk))/60;
															//$this->db->insert('coba_coba',array('keterangan'=>'keterlambatan_masuk: '.$keterlambatan_masuk,'waktu'=>date('Y-m-d H:i:s')));	
															$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
															//$this->db->insert('coba_coba',array('keterangan'=>'POTONGAN TELAH: '.$potongan_terlambat_masuk,'waktu'=>date('Y-m-d H:i:s')));	
															$realisasi_keluar = $tanggal;							
															$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($realisasi_keluar))/60;
															if($kecepatan_keluar > 0)
															{
																$kecepatan_keluar = $kecepatan_keluar;
																$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
															}
															else
															{
																$kecepatan_keluar = 0;
																$potongan_pulang_cepat=0;
															}
															$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
															$su = "update d_absen_harian_tkk
																		set jadwal_masuk = '$jadwal_masuk',
																			realisasi_masuk = '$realisasi_masuk',
																			keterlambatan_masuk = '$keterlambatan_masuk',
																			potongan_terlambat_masuk = '$potongan_terlambat_masuk',
																			jadwal_keluar = '$jadwal_keluar', 
																			realisasi_keluar = '$realisasi_keluar', 
																			kecepatan_keluar = '$kecepatan_keluar', 
																			potongan_pulang_cepat = '$potongan_pulang_cepat', 
																			persentase_potongan = '$persentase_potongan', 
																			update_date = '$tanggal',
																			update_by = 'system'
																		where id = '$id_absen_harian'
															";
															$this->db->query($su);
														//	echo $su.'</br>';
														}
														else
														{
															$realisasi_keluar = $tanggal;							
															$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($realisasi_keluar))/60;
															if($kecepatan_keluar > 0)
															{
																$kecepatan_keluar = $kecepatan_keluar;
																$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
															}
															else
															{
																$kecepatan_keluar = 0;
																$potongan_pulang_cepat=0;
															}
															$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
															$su = "update d_absen_harian_tkk
																		set realisasi_keluar = '$realisasi_keluar', 
																			kecepatan_keluar = '$kecepatan_keluar', 
																			potongan_pulang_cepat = '$potongan_pulang_cepat', 
																			persentase_potongan = '$persentase_potongan', 
																			update_date = '$tanggal',
																			update_by = 'system'
																		where id = '$id_absen_harian'
															";
															$this->db->query($su);
														//	echo $su.'</br>';
														}
													}
												}
											}
											else
											{
												$persentase_potongan=0;
											}
										}
										else
										{
											$sl = "select * from d_jadwal_lembur_tkk where nik = '$nik' and tanggal = '$tanggal' ";
											$ql = $this->db->query($sl);
											if($ql->num_rows() > 0)
											{
												$jl = $ql->row_array();
												if($jl['is_libur'] == 0)
												{
													//$jadwal_masuk = $jl['jadwal_masuk'];
													//$jadwal_keluar = $jl['jadwal_keluar'];		
													$rcj = $qcj->row();
													$id_absen_harian = $rcj->id;
													$jadwal_masuk = $rcj->jadwal_masuk;
													$realisasi_masuk = $rcj->realisasi_masuk;
													$jadwal_keluar = $rcj->jadwal_keluar;
													$realisasi_keluar = $rcj->realisasi_keluar;
													$potongan_terlambat_masuk = $rcj->potongan_terlambat_masuk;
													$potongan_pulang_cepat = $rcj->potongan_pulang_cepat;
													$potongan_tidak_masuk = $rcj->potongan_tidak_masuk;
													if($jadwal_masuk == '0000-00-00 00:00:00' || $jadwal_keluar == '0000-00-00 00:00:00')
													{
														if($jadwal_masuk == '0000-00-00 00:00:00')
														{
															$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($tanggal))/60;
															if($kecepatan_keluar > 0)
															{
																$kecepatan_keluar = $kecepatan_keluar;
																$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
															}
															else
															{
																$kecepatan_keluar = 0;
																$potongan_pulang_cepat=0;
															}
															$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
															$su = "update d_absen_harian_tkk
																			set realisasi_keluar = '$tanggal',
																				kecepatan_keluar = '$kecepatan_keluar',
																				potongan_pulang_cepat = '$potongan_pulang_cepat',
																				persentase_potongan = '$persentase_potongan',     
																				update_date = '$tanggal',
																				update_by = 'system'
																			where id = '$id_absen_harian'
															";
															$this->db->query($su);
														//	echo $su.'</br>';
														}
														elseif($jadwal_keluar == '0000-00-00 00:00:00')
														{
															if($realisasi_masuk == NULL)
															{
																$keterlambatan_masuk = (strtotime($tanggal) - strtotime($jadwal_masuk))/60;
																if($keterlambatan_masuk > 0)
																{
																	$keterlambatan_masuk = $keterlambatan_masuk;
																	$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
																}
																else
																{
																	$keterlambatan_masuk = 0;
																	$potongan_terlambat_masuk=0;
																}
																$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
																$su = "update d_absen_harian_tkk
																				set realisasi_masuk = '$tanggal',
																					keterlambatan_masuk = '$keterlambatan_masuk',
																					potongan_terlambat_masuk = '$potongan_terlambat_masuk',
																					persentase_potongan = '$persentase_potongan',     
																					update_date = '$tanggal',
																					update_by = 'system'
																				where id = '$id_absen_harian'
																";
																$this->db->query($su);
															//	echo $su.'</br>';
															}
														}
													}
													else
													{
														$jam_bagi_dua = (strtotime($jadwal_keluar) - strtotime($jadwal_masuk))/2;
														$istirahat = strtotime($jadwal_masuk)+$jam_bagi_dua;
														if(strtotime($tanggal)<$istirahat)
														{
															if($realisasi_masuk == NULL)
															{
																$keterlambatan_masuk = (strtotime($tanggal) - strtotime($jadwal_masuk))/60;
																if($keterlambatan_masuk > 0)
																{
																	$keterlambatan_masuk = $keterlambatan_masuk;
																	$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
																}
																else
																{
																	$keterlambatan_masuk = 0;
																	$potongan_terlambat_masuk=0;
																}
																
																$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($tanggal))/60;
																if($kecepatan_keluar > 0)
																{
																	$kecepatan_keluar = $kecepatan_keluar;
																	$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
																}
																else
																{
																	$kecepatan_keluar = 0;
																	$potongan_pulang_cepat=0;
																}
																$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
																$su = "update d_absen_harian_tkk
																			set realisasi_masuk = '$tanggal',
																				keterlambatan_masuk = '$keterlambatan_masuk',
																				potongan_terlambat_masuk = '$potongan_terlambat_masuk',
																				realisasi_keluar = '$tanggal',
																				kecepatan_keluar = '$kecepatan_keluar',
																				potongan_pulang_cepat = '$potongan_pulang_cepat',
																				persentase_potongan = '$persentase_potongan',     
																				update_date = '$tanggal',
																				update_by = 'system'
																			where id = '$id_absen_harian'
																";
																$this->db->query($su);
															//	echo $su.'</br>';
															}
															else
															{
																
																$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($tanggal))/60;
																if($kecepatan_keluar > 0)
																{
																	$kecepatan_keluar = $kecepatan_keluar;
																	$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
																}
																else
																{
																	$kecepatan_keluar = 0;
																	$potongan_pulang_cepat=0;
																}
																$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
																$su = "update d_absen_harian_tkk
																			set realisasi_keluar = '$tanggal',
																				kecepatan_keluar = '$kecepatan_keluar',
																				potongan_pulang_cepat = '$potongan_pulang_cepat',
																				persentase_potongan = '$persentase_potongan',     
																				update_date = '$tanggal',
																				update_by = 'system'
																			where id = '$id_absen_harian'
																";
																$this->db->query($su);
															//	echo $su.'</br>';
															}
														}
														else
														{
															if($realisasi_masuk == NULL)
															{
																$realisasi_masuk = $tanggal;	
																$keterlambatan_masuk = (strtotime($realisasi_masuk) - strtotime($jadwal_masuk))/60;
																//$this->db->insert('coba_coba',array('keterangan'=>'keterlambatan_masuk: '.$keterlambatan_masuk,'waktu'=>date('Y-m-d H:i:s')));	
																$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
																//$this->db->insert('coba_coba',array('keterangan'=>'POTONGAN TELAH: '.$potongan_terlambat_masuk,'waktu'=>date('Y-m-d H:i:s')));	
																$realisasi_keluar = $tanggal;							
																$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($realisasi_keluar))/60;
																if($kecepatan_keluar > 0)
																{
																	$kecepatan_keluar = $kecepatan_keluar;
																	$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
																}
																else
																{
																	$kecepatan_keluar = 0;
																	$potongan_pulang_cepat=0;
																}
																$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
																$su = "update d_absen_harian_tkk
																			set jadwal_masuk = '$jadwal_masuk',
																				realisasi_masuk = '$realisasi_masuk',
																				keterlambatan_masuk = '$keterlambatan_masuk',
																				potongan_terlambat_masuk = '$potongan_terlambat_masuk',
																				jadwal_keluar = '$jadwal_keluar', 
																				realisasi_keluar = '$realisasi_keluar', 
																				kecepatan_keluar = '$kecepatan_keluar', 
																				potongan_pulang_cepat = '$potongan_pulang_cepat', 
																				persentase_potongan = '$persentase_potongan', 
																				update_date = '$tanggal',
																				update_by = 'system'
																			where id = '$id_absen_harian'
																";
																$this->db->query($su);
															//	echo $su.'</br>';
															}
															else
															{
																$realisasi_keluar = $tanggal;							
																$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($realisasi_keluar))/60;
																if($kecepatan_keluar > 0)
																{
																	$kecepatan_keluar = $kecepatan_keluar;
																	$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
																}
																else
																{
																	$kecepatan_keluar = 0;
																	$potongan_pulang_cepat=0;
																}
																$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat + $potongan_tidak_masuk;
																$su = "update d_absen_harian_tkk
																			set realisasi_keluar = '$realisasi_keluar', 
																				kecepatan_keluar = '$kecepatan_keluar', 
																				potongan_pulang_cepat = '$potongan_pulang_cepat', 
																				persentase_potongan = '$persentase_potongan', 
																				update_date = '$tanggal',
																				update_by = 'system'
																			where id = '$id_absen_harian'
																";
																$this->db->query($su);
															//	echo $su.'</br>';
															}
														}
													}
												}
												else
												{
													$persentase_potongan=0;
												}
											}
										}
									
								}
								else
								{
									$st = "select a.*,
												b.*
											from d_pegawai_tipe_jadwal_tkk a
											left join ref_tipe_jadwal b on(a.tipe_jadwal = b.id)
											where nik = '$nik' 									
									";
									$rst = $this->db->query($st)->row_array();
								//	if($rst['tipe_jadwal']=='1')
									if($rst['shift'] == 0)
									{
										//$cek_libur = $this->cek_libur;
										//$cek_libur = $this->cek_libur($tanggal);
										$cek_libur = $this->cek_libur($tgl,$rst['sabtu_masuk']);
										if($cek_libur == 0)
										{
											//cek jika jadwal khusus
											$hari_ini = date('w',strtotime($thn.'-'.$bln.'-'.$tgl));
											$sc = "select * from referensi_jadwal_khusus where hari = '$hari_ini' and aktif = 1 ";
											$qc = $this->db->query($sc);
											if($qc->num_rows() > 0)
											{
												$rjk = $qc->row_array();
												$jadwal_jam_masuk = $rjk['jadwal_jam_masuk'];
												$jadwal_menit_masuk = $rjk['jadwal_menit_masuk'];
												$jadwal_masuk = date('Y-m-d H:i',strtotime($tgl.'-'.$bln.'-'.$thn.' '.$jadwal_jam_masuk.':'.$jadwal_menit_masuk));
												$jadwal_jam_keluar = $rjk['jadwal_jam_keluar'];
												$jadwal_menit_keluar = $rjk['jadwal_menit_keluar'];
												$jadwal_keluar = date('Y-m-d H:i',strtotime($tgl.'-'.$bln.'-'.$thn.' '.$jadwal_jam_keluar.':'.$jadwal_menit_keluar));	
											}
											else
											{
												$jadwal_jam_masuk = $rst['jadwal_jam_masuk'];
												$jadwal_menit_masuk = $rst['jadwal_menit_masuk'];
												$jadwal_masuk = date('Y-m-d H:i',strtotime($tgl.'-'.$bln.'-'.$thn.' '.$jadwal_jam_masuk.':'.$jadwal_menit_masuk));
												$jadwal_jam_keluar = $rst['jadwal_jam_keluar'];
												$jadwal_menit_keluar = $rst['jadwal_menit_keluar'];
												$jadwal_keluar = date('Y-m-d H:i',strtotime($tgl.'-'.$bln.'-'.$thn.' '.$jadwal_jam_keluar.':'.$jadwal_menit_keluar));	
											}
											
											$jam_bagi_dua = (strtotime($jadwal_keluar) - strtotime($jadwal_masuk))/2;
											$istirahat = strtotime($jadwal_masuk)+$jam_bagi_dua;										
											$libur = 0;
											if(strtotime($tanggal)<$istirahat)
											{
												$this->db->insert('coba_coba',array('keterangan'=>'masuk','waktu'=>date('Y-m-d H:i:s'),'nip'=>$nip));
												
													$realisasi_masuk = $tanggal;
													$keterlambatan_masuk = (strtotime($realisasi_masuk) - strtotime($jadwal_masuk))/60;
													if($keterlambatan_masuk > 0)
													{
														$keterlambatan_masuk = $keterlambatan_masuk;
														$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
													}
													else
													{
														$keterlambatan_masuk = 0;
														$potongan_terlambat_masuk=0;
													}
													$persentase_potongan = $potongan_terlambat_masuk;
													$si = "insert into d_absen_harian_tkk
														set nik = '$nik',
															tanggal = '$tgl',
															bulan = '$bln',
															tahun = '$thn',
															jadwal_masuk = '$jadwal_masuk',
															realisasi_masuk = '$realisasi_masuk',
															keterlambatan_masuk = '$keterlambatan_masuk',
															potongan_terlambat_masuk = '$potongan_terlambat_masuk',
															jadwal_keluar = '$jadwal_keluar', 
															libur = '$libur',
															persentase_potongan = '$persentase_potongan',
															update_date = '$tanggal',
															update_by = 'system'
														";
													//echo $si.'</br>';
													$qi = $this->db->query($si);
												
											}
											else
											{
												$realisasi_masuk = $tanggal;	
												$keterlambatan_masuk = (strtotime($realisasi_masuk) - strtotime($jadwal_masuk))/60;
												//$this->db->insert('coba_coba',array('keterangan'=>'keterlambatan_masuk: '.$keterlambatan_masuk,'waktu'=>date('Y-m-d H:i:s')));	
												$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
												//$this->db->insert('coba_coba',array('keterangan'=>'POTONGAN TELAH: '.$potongan_terlambat_masuk,'waktu'=>date('Y-m-d H:i:s')));	
												$realisasi_keluar = $tanggal;
												$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($realisasi_keluar))/60;
												if($kecepatan_keluar > 0)
												{
													$kecepatan_keluar = $kecepatan_keluar;
													$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
												}
												else
												{
													$kecepatan_keluar = 0;
													$potongan_pulang_cepat=0;
												}
												$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat;
												$si = "insert into d_absen_harian_tkk
														set nik = '$nik',
															tanggal = '$tgl',
															bulan = '$bln',
															tahun = '$thn',
															jadwal_masuk = '$jadwal_masuk',
															realisasi_masuk = '$realisasi_masuk',
															keterlambatan_masuk = '$keterlambatan_masuk',
															potongan_terlambat_masuk = '$potongan_terlambat_masuk',
															jadwal_keluar = '$jadwal_keluar', 
															realisasi_keluar = '$realisasi_keluar', 
															kecepatan_keluar = '$kecepatan_keluar', 
															potongan_pulang_cepat = '$potongan_pulang_cepat', 
															persentase_potongan = '$persentase_potongan', 
															libur = '$libur',
															update_date = '$tanggal',
															update_by = 'system'
														";
												//	echo $si.'</br>';
													$qi = $this->db->query($si);
																					
											}
										
										}
										else
										{
											$libur = 1;
											$si = "insert into d_absen_harian_tkk
														set nik = '$nik',
															tanggal = '$tgl',
															bulan = '$bln',
															tahun = '$thn',
															libur = '$libur',
															update_date = '$tanggal',
															update_by = 'system'
														";
												//	echo $si.'</br>';
													$qi = $this->db->query($si);
										}
										
									}
									else
									{
										$sl = "select * from d_jadwal_lembur_tkk where nik = '$nik' and tanggal = '$tanggal' ";
										$ql = $this->db->query($sl);
										if($ql->num_rows() > 0)
										{
											$jl = $ql->row_array();
											if($jl['is_libur'] == 0)
											{
												$jadwal_masuk = $jl['jadwal_masuk'];
												$jadwal_keluar = $jl['jadwal_keluar'];		
												if($jadwal_masuk == '0000-00-00 00:00:00' || $jadwal_keluar == '0000-00-00 00:00:00')
												{
													$libur = 0;
													if($jadwal_masuk == '0000-00-00 00:00:00')
														{
															$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($tanggal))/60;
															if($kecepatan_keluar > 0)
															{
																$kecepatan_keluar = $kecepatan_keluar;
																$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
															}
															else
															{
																$kecepatan_keluar = 0;
																$potongan_pulang_cepat=0;
															}
															$persentase_potongan = $potongan_pulang_cepat;
															$su = "insert into d_absen_harian_tkk
																set nik = '$nik',
																	tanggal = '$tgl',
																	bulan = '$bln',
																	tahun = '$thn',
																	jadwal_masuk = '$jadwal_masuk',
																	jadwal_keluar = '$jadwal_keluar', 
																	realisasi_keluar = '$tanggal', 
																	kecepatan_keluar = '$kecepatan_keluar', 
																	potongan_pulang_cepat = '$potongan_pulang_cepat', 
																	persentase_potongan = '$persentase_potongan', 
																	libur = '$libur',
																	update_date = '$tanggal',
																	update_by = 'system'
															";
															$this->db->query($su);
													//		echo $su.'</br>';
														}
														elseif($jadwal_keluar == '0000-00-00 00:00:00')
														{
															
																$keterlambatan_masuk = (strtotime($tanggal) - strtotime($jadwal_masuk))/60;
																if($keterlambatan_masuk > 0)
																{
																	$keterlambatan_masuk = $keterlambatan_masuk;
																	$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
																}
																else
																{
																	$keterlambatan_masuk = 0;
																	$potongan_terlambat_masuk=0;
																}
																$persentase_potongan = $potongan_terlambat_masuk;
																$su = "insert into d_absen_harian_tkk
																set nik = '$nik',
																	tanggal = '$tgl',
																	bulan = '$bln',
																	tahun = '$thn',
																	jadwal_masuk = '$jadwal_masuk',
																	realisasi_masuk = '$tanggal',
																	keterlambatan_masuk = '$keterlambatan_masuk',
																	potongan_terlambat_masuk = '$potongan_terlambat_masuk',
																	jadwal_keluar = '$jadwal_keluar', 
																	persentase_potongan = '$persentase_potongan', 
																	libur = '$libur',
																	update_date = '$tanggal',
																	update_by = 'system'
																";
																$this->db->query($su);
													//			echo $su.'</br>';
															
														}
												}
												else
												{
													$hari_ini = date('w',strtotime($thn.'-'.$bln.'-'.$tgl));
													$jam_bagi_dua = (strtotime($jadwal_keluar) - strtotime($jadwal_masuk))/2;
													$istirahat = strtotime($jadwal_masuk)+$jam_bagi_dua;										
													$libur = 0;
													if(strtotime($tanggal)<$istirahat)
													{
														
															$realisasi_masuk = $tanggal;
															$keterlambatan_masuk = (strtotime($realisasi_masuk) - strtotime($jadwal_masuk))/60;
															if($keterlambatan_masuk > 0)
															{
																$keterlambatan_masuk = $keterlambatan_masuk;
																$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
															}
															else
															{
																$keterlambatan_masuk = 0;
																$potongan_terlambat_masuk=0;
															}
															$persentase_potongan = $potongan_terlambat_masuk;
															$si = "insert into d_absen_harian_tkk
																set nik = '$nik',
																	tanggal = '$tgl',
																	bulan = '$bln',
																	tahun = '$thn',
																	jadwal_masuk = '$jadwal_masuk',
																	realisasi_masuk = '$realisasi_masuk',
																	keterlambatan_masuk = '$keterlambatan_masuk',
																	potongan_terlambat_masuk = '$potongan_terlambat_masuk',
																	jadwal_keluar = '$jadwal_keluar', 
																	libur = '$libur',
																	persentase_potongan = '$persentase_potongan',
																	update_date = '$tanggal',
																	update_by = 'system'
																";
														//	echo $si.'</br>';
															$qi = $this->db->query($si);
														
													}
													else
													{
														$realisasi_masuk = $tanggal;	
														$keterlambatan_masuk = (strtotime($realisasi_masuk) - strtotime($jadwal_masuk))/60;
														//$this->db->insert('coba_coba',array('keterangan'=>'keterlambatan_masuk: '.$keterlambatan_masuk,'waktu'=>date('Y-m-d H:i:s')));	
														$potongan_terlambat_masuk = $this->get_potongan_terlambat_masuk($keterlambatan_masuk);
														//$this->db->insert('coba_coba',array('keterangan'=>'POTONGAN TELAH: '.$potongan_terlambat_masuk,'waktu'=>date('Y-m-d H:i:s')));	
														$realisasi_keluar = $tanggal;
														$kecepatan_keluar = (strtotime($jadwal_keluar) - strtotime($realisasi_keluar))/60;
														if($kecepatan_keluar > 0)
														{
															$kecepatan_keluar = $kecepatan_keluar;
															$potongan_pulang_cepat = $this->get_potongan_pulang_cepat($kecepatan_keluar);
														}
														else
														{
															$kecepatan_keluar = 0;
															$potongan_pulang_cepat=0;
														}
														$persentase_potongan = $potongan_terlambat_masuk + $potongan_pulang_cepat;
														$si = "insert into d_absen_harian_tkk
																set nik = '$nik',
																	tanggal = '$tgl',
																	bulan = '$bln',
																	tahun = '$thn',
																	jadwal_masuk = '$jadwal_masuk',
																	realisasi_masuk = '$realisasi_masuk',
																	keterlambatan_masuk = '$keterlambatan_masuk',
																	potongan_terlambat_masuk = '$potongan_terlambat_masuk',
																	jadwal_keluar = '$jadwal_keluar', 
																	realisasi_keluar = '$realisasi_keluar', 
																	kecepatan_keluar = '$kecepatan_keluar', 
																	potongan_pulang_cepat = '$potongan_pulang_cepat', 
																	persentase_potongan = '$persentase_potongan', 
																	libur = '$libur',
																	update_date = '$tanggal',
																	update_by = 'system'
																";
														//	echo $si.'</br>';
															$qi = $this->db->query($si);
																							
													}
													
												}
											}
											else
											{
												$libur = 1;
												$si = "insert into d_absen_harian_tkk
														set nik = '$nik',
															tanggal = '$tgl',
															bulan = '$bln',
															tahun = '$thn',
															libur = '$libur',
															update_date = '$tanggal',
															update_by = 'system'
														";
												//echo $si.'</br>';
												$qi = $this->db->query($si);
											}
										}
									}
									
								}
								
								$this->update_data_rekap_keuangan_tkk($nik,$tanggal,$persentase_potongan);
							}
				
	}
	
	function get_tidak_masuk()
	{
		$s = "select persentase from referensi_persentase_ketidakhadiran where id = '1' ";
		$result = $this->db->query($s)->row()->persentase;
		return $result;
	}
	
	public function mingguan()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		$tahun = $this->session->userdata('tahun_mingguan');
		$tahun = ((isset($tahun) && $tahun != '')?$tahun:date('Y'));
		$bulan = $this->session->userdata('bulan_mingguan');
		$bulan = ((isset($bulan) && $bulan != '')?$bulan:date('n').'/'.$tahun);
		$minggu = $this->session->userdata('minggu_mingguan');
		if(isset($minggu) && $minggu != '')
		{
			$minggu = $minggu;
		}
		else
		{
			$bulane = explode('/',$bulan);
			$bulane = $bulane[0];
			$hari_ini = date('j');
			$hari_dalam_minggu = date('N');
			$tanggal_basis_minggu = $hari_ini - $hari_dalam_minggu;
			if($tanggal_basis_minggu >0)
			{
				$t = ($tanggal_basis_minggu + 1).'-'.$bulane.'-'.$tahun;
				$tanggal_awal_minggu = date('j-n-Y',strtotime(($tanggal_basis_minggu + 1).'-'.$bulane.'-'.$tahun));
				$tanggal_akhir_minggu = date('j-n-Y',strtotime(($tanggal_basis_minggu + 7).'-'.$bulane.'-'.$tahun));
				$minggu = $tanggal_awal_minggu.'/'.$tanggal_akhir_minggu;
			}
			else
			{
				$time_now = strtotime($hari_ini.'-'.$bulane.'-'.$tahun);
				$time_awal = $time_now - ($hari_dalam_minggu-1)*60*60*24;
				$time_akhir =  $time_now + (7-$hari_dalam_minggu)*60*60*24;;
				$tanggal_awal_minggu = date('j-n-Y',($time_awal));
				$tanggal_akhir_minggu = date('j-n-Y',($time_akhir));
				$minggu = '';
				$minggu = $tanggal_awal_minggu.'/'.$tanggal_akhir_minggu;
			}
			
		}
		
		$data = array(
				'title' => 'Data Absensi Mingguan',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'sssubunit' => $this->session->userdata('sssubunit_mingguan'),
				'jnsjab' => $this->session->userdata('jnsjab_mingguan'),
				'keselon' => $this->session->userdata('keselon_mingguan'),
				'nama' => $this->session->userdata('nama_mingguan'),
				'reqnip' => $this->session->userdata('reqnip_mingguan'),
				'bulan' => $bulan,
				'tahun' => $tahun,
				'minggu' => $minggu,
		);
		$this->load->view('mingguan',$data);
		input_log($this->user_id,'Lihat Data Mingguan PNS');
	}
	
	public function get_mingguan()
	{
		//print_r($_REQUEST);
		$order = $_REQUEST['order'][0];
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/log/bulanan');
		//$aColumns = array( 'nip', 'nip','nama', 'nip','nip');
		$sssubunit = $this->input->post('sssubunit');
		$this->session->set_userdata('sssubunit_mingguan',$sssubunit);
		$jnsjab = $this->input->post('jnsjab');
		$this->session->set_userdata('jnsjab_mingguan',$jnsjab);
		$keselon = $this->input->post('keselon');
		$this->session->set_userdata('keselon_mingguan',$keselon);
		$nama = $this->input->post('nama');
		$this->session->set_userdata('nama_mingguan',$nama);
		$reqnip = $this->input->post('reqnip');
		$this->session->set_userdata('reqnip_mingguan',$reqnip);
		$bulan = $this->input->post('bulan');
		$this->session->set_userdata('bulan_mingguan',$bulan);
		$tahun = $this->input->post('tahun');
		$this->session->set_userdata('tahun_mingguan',$tahun);
		$minggu = $this->input->post('minggu');
		$this->session->set_userdata('minggu_mingguan',$minggu);
		
		$sSearch = '';
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		if(isset($select) && $select != '')
		{
			if(!empty($s_biro)) 
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
			}
			else
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			}
		}
		
		if(isset($nama) && $nama != '')
		{
			$sSearch .= " and (nama like '%".$nama."%') ";
		}
		if(isset($reqnip) && $reqnip != '')
		{
			$sSearch .= " and (a.nip = '".$reqnip."') ";
		}
		if(isset($jnsjab) && $jnsjab != '0')
		{
			$sSearch .= " and (jnsjab = '".$jnsjab."') ";
		}
		if(isset($keselon) && $keselon != '0')
		{
			$sSearch .= " and (keselon = '".$keselon."') ";
		}
	
		//limit
		if ( isset( $_REQUEST['start'] ) && $_REQUEST['length'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_REQUEST['start'] ).", ".
				intval( $_REQUEST['length'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "`".$aColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]."` ".
						($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		//$sOrder = " order by ".$aColumns[$order['column']]." ".$order['dir']." ";
		$s0 = "select count(a.nip) as jml 
				from d_pegawai a
				where 1 = 1
				and a.non_aktif = 0
				";
		$s0 .= $sSearch;
		//echo $s0;
		$sheet_total = $this->db->query($s0)->row();
		$sheet_total = $sheet_total->jml;
	
		$s1 = "SELECT DISTINCT a.nip,a.nama,
									a.keselon,
									a.kgolru,
									a.kuntp,
									a.kunkom,
									a.kununit,
									a.tanggal_lahir,
									a.jabatan,
									(select neselon from referensi_eselon where keselon = a.keselon) as eselon,
									(select ngolru from referensi_golongan_ruang where KGOLRU = a.KGOLRU) as golongan,
									(select pangkat from referensi_golongan_ruang where KGOLRU = a.KGOLRU) as pangkat
				FROM d_pegawai a
				WHERE 1=1
				and a.non_aktif = 0";
		$s1 .= $sSearch;
		$s1 .= " order by keselon asc, kgolru desc, nip asc ";
		
		$s1 .= $sLimit;
	//	echo $s1;
		$sheet1 = $this->db->query($s1);
		$result = array();
		$i=0;
		$no=1;
		foreach($sheet1->result() as $sheet)
		{
			
			$result[$i]['0'] = $no;
			$result[$i]['1'] = $sheet->nip;
			$result[$i]['2'] = $sheet->nama;
			$result[$i]['3'] = $sheet->golongan;
			$result[$i]['4'] = $sheet->jabatan;
			$result[$i]['5'] = 	$sheet->eselon;	
			$result[$i]['6'] = 	'<a target="_blank" href="/log/view_mingguan/'.$sheet->nip.'/'.$bulan.'/'.$minggu.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="View">
									<i class="glyphicon glyphicon-search"></i>
									</a>';	
			$result[$i]['7'] = 	'<a target="_blank" href="/log/pdf_mingguan/'.$sheet->nip.'/'.$bulan.'/'.$minggu.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="View">
									<i class="glyphicon glyphicon-print"></i>
									</a>';	
			$no++;
			$i++;
		}
		
		$data = $result;
		$results = array(
			"iTotalRecords" => ($sheet_total),
			"iTotalDisplayRecords" => ($sheet_total),
			"aaData"=>$data);
			
		echo json_encode($results);
	}
	
	public function view_mingguan()
	{
		$nip = $this->uri->segment(3);
		$bulan = $this->uri->segment(4);
		$tahun = $this->uri->segment(5);
		$tanggal_awal = $this->uri->segment(6);
	//	echo $tanggal_awal.'</br>';
		$awal = date('Y-m-d',strtotime($tanggal_awal));
		$tgl_awal = date('d',strtotime($tanggal_awal));
		$bln_awal = date('n',strtotime($tanggal_awal));
		$thn_awal = date('Y',strtotime($tanggal_awal));
		$tanggal_akhir = $this->uri->segment(7);
	//	echo $tanggal_akhir.'</br>';
		$akhir = date('Y-m-d',strtotime($tanggal_akhir));
		$tgl_akhir = date('d',strtotime($tanggal_akhir));
		$bln_akhir = date('n',strtotime($tanggal_akhir));
		$thn_akhir = date('Y',strtotime($tanggal_akhir));
		$sc = "select a.*,
					if(a.jenis_tidak_masuk != 0,(select jenis_ketidakhadiran from referensi_persentase_ketidakhadiran where id = a.jenis_tidak_masuk),'-') as ketidakhadiran
				from d_absen_harian a
				where nip = '$nip' 
				and (date(a.jadwal_masuk) between '$awal' and '$akhir')
				order by a.tahun asc, bulan asc, tanggal asc
				";
	//	echo $sc;
		$qc = $this->db->query($sc);
		if($qc->num_rows() > 0)
		{
			$sql = "SELECT DISTINCT a.nama,
									a.keselon,
									a.kgolru,
									a.kuntp,
									a.kunkom,
									a.kununit,
									a.tanggal_lahir,
									a.jabatan,
									(select neselon from referensi_eselon where keselon = a.keselon) as eselon,
									(select ngolru from referensi_golongan_ruang where KGOLRU = a.KGOLRU) as golongan,
									(select pangkat from referensi_golongan_ruang where KGOLRU = a.KGOLRU) as pangkat
			FROM d_pegawai a
			where nip = '$nip' ";
			$data_peg = $this->db->query($sql)->row_array();
			$nama_bulan = array(
				'1' => 'Januari',
				'2' => 'Februari',
				'3' => 'Maret',
				'4' => 'April',
				'5' => 'Mei',
				'6' => 'Juni',
				'7' => 'Juli',
				'8' => 'Agustus',
				'9' => 'September',
				'10' => 'Oktober',
				'11' => 'November',
				'12' => 'Desember',
			);
			$data = array(
				'title' => 'Daftar Hadir Bulanan',
				'subtitle' => '',
				'rows' => $qc,
				'nip' => $nip,
				'bulan' => $bulan,
				'tahun' => $tahun,
				'data' => $data_peg,
				'nama_bulan' => $nama_bulan,
				'tanggal_awal' => $tanggal_awal,
				'tanggal_akhir' => $tanggal_akhir
			);
			$this->load->view('view_mingguan',$data);
			input_log($this->user_id,'Lihat Data Absensi Mingguan dengan nip:'.$nip.' bulan:'.$bulan.' tahun:'.$tahun);
		}
	}
	
	public function pdf_mingguan()
	{
		error_reporting(0);
		$this->load->library('pdf');
		$pdf = $this->pdf->load('utf-8', 'F4');
		$nip = $this->uri->segment(3);
		$bulan = $this->uri->segment(4);
		$tahun = $this->uri->segment(5);
		$tanggal_awal = $this->uri->segment(6);
	//	echo $tanggal_awal.'</br>';
		$awal = date('Y-m-d',strtotime($tanggal_awal));
		$tgl_awal = date('d',strtotime($tanggal_awal));
		$bln_awal = date('n',strtotime($tanggal_awal));
		$thn_awal = date('Y',strtotime($tanggal_awal));
		$tanggal_akhir = $this->uri->segment(7);
	//	echo $tanggal_akhir.'</br>';
		$akhir = date('Y-m-d',strtotime($tanggal_akhir));
		$tgl_akhir = date('d',strtotime($tanggal_akhir));
		$bln_akhir = date('n',strtotime($tanggal_akhir));
		$thn_akhir = date('Y',strtotime($tanggal_akhir));
		$sc = "select a.*,
					if(a.jenis_tidak_masuk != 0,(select jenis_ketidakhadiran from referensi_persentase_ketidakhadiran where id = a.jenis_tidak_masuk),'-') as ketidakhadiran
				from d_absen_harian a
				where nip = '$nip' 
				and (date(a.jadwal_masuk) between '$awal' and '$akhir')
				order by a.tahun asc, bulan asc, tanggal asc
				";
	//	echo $sc;
		$qc = $this->db->query($sc);
		if($qc->num_rows() > 0)
		{
			$sql = "SELECT DISTINCT a.nama,
									a.keselon,
									a.kgolru,
									a.kuntp,
									a.kunkom,
									a.kununit,
									a.tanggal_lahir,
									a.jabatan,
									(select neselon from referensi_eselon where keselon = a.keselon) as eselon,
									(select ngolru from referensi_golongan_ruang where KGOLRU = a.KGOLRU) as golongan,
									(select pangkat from referensi_golongan_ruang where KGOLRU = a.KGOLRU) as pangkat
			FROM d_pegawai a
			where nip = '$nip' ";
			$data = $this->db->query($sql)->row_array();
			$nama_bulan = array(
				'1' => 'Januari',
				'2' => 'Februari',
				'3' => 'Maret',
				'4' => 'April',
				'5' => 'Mei',
				'6' => 'Juni',
				'7' => 'Juli',
				'8' => 'Agustus',
				'9' => 'September',
				'10' => 'Oktober',
				'11' => 'November',
				'12' => 'Desember',
			);
			$data = array(
				'title' => 'Daftar Hadir Bulanan',
				'subtitle' => '',
				'rows' => $qc,
				'nip' => $nip,
				'bulan' => $bulan,
				'tahun' => $tahun,
				'data' => $data,
				'nama_bulan' => $nama_bulan,
				'tanggal_awal' => $tanggal_awal,
				'tanggal_akhir' => $tanggal_akhir
			);
			$html = $this->load->view('pdf_mingguan',$data,true);
			input_log($this->user_id,'Cetak Data Absensi Mingguan ke dalam bentuk pdf dengan nip:'.$nip.' bulan:'.$bulan.' tahun:'.$tahun);
			$pdf->WriteHTML($html);
			$pdf->Output();
		}
	}
	
	public function pdf_mingguan_skpd()
	{
		error_reporting(0);
		$this->load->library('pdf');
		$pdf = $this->pdf->load('utf-8', 'F4');
		$sssubunit = $this->input->post('sssubunit');
		$bulans = $this->input->post('sssubunit');
		$bulans = explode('/',$bulans);
		$bulan = $bulans[0];
		$tahun = $bulans[1];
		$minggus = $this->input->post('minggu');
		$minggu = explode('/',$minggus);
		$tanggal_awal = $minggu[0];
		$awal = date('Y-m-d',strtotime($tanggal_awal));
		$tgl_awal = date('d',strtotime($tanggal_awal));
		$bln_awal = date('n',strtotime($tanggal_awal));
		$thn_awal = date('Y',strtotime($tanggal_awal));
		$tanggal_akhir = $minggu[1];
		$akhir = date('Y-m-d',strtotime($tanggal_akhir));
		$tgl_akhir = date('d',strtotime($tanggal_akhir));
		$bln_akhir = date('n',strtotime($tanggal_akhir));
		$thn_akhir = date('Y',strtotime($tanggal_akhir));
		
		$reqnip = $this->input->post('reqnip');
		$jnsjab = $this->input->post('jnsjab');
		$keselon = $this->input->post('keselon');
		$nama = $this->input->post('nama');
		
		$sSearch = '';
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		if(isset($select) && $select != '')
		{
			if(!empty($s_biro)) 
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
			}
			else
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			}
		}
		
		if(isset($nama) && $nama != '')
		{
			$sSearch .= " and (nama like '%".$nama."%') ";
		}
		if(isset($reqnip) && $reqnip != '')
		{
			$sSearch .= " and (a.nip = '".$reqnip."') ";
		}
		if(isset($jnsjab) && $jnsjab != '0' && $jnsjab != '')
		{
			$sSearch .= " and (jnsjab = '".$jnsjab."') ";
		}
		if(isset($keselon) && $keselon != '0' && $keselon != '')
		{
			$sSearch .= " and (keselon = '".$keselon."') ";
		}
	
		$s1 = "SELECT DISTINCT a.nip,a.nama,
									a.keselon,
									a.kgolru,
									a.kuntp,
									a.kunkom,
									a.kununit,
									a.tanggal_lahir,
									a.jabatan,
									(select neselon from referensi_eselon where keselon = a.keselon) as eselon,
									(select ngolru from referensi_golongan_ruang where KGOLRU = a.KGOLRU) as golongan,
									(select pangkat from referensi_golongan_ruang where KGOLRU = a.KGOLRU) as pangkat
				FROM d_pegawai a
				WHERE 1=1
				and a.non_aktif = 0";
		$s1 .= $sSearch;
		$s1 .= " order by keselon asc, kgolru desc, nip asc ";
	//	echo $s1;
		$qc = $this->db->query($s1);
		$nama_bulan = array(
				'1' => 'Januari',
				'2' => 'Februari',
				'3' => 'Maret',
				'4' => 'April',
				'5' => 'Mei',
				'6' => 'Juni',
				'7' => 'Juli',
				'8' => 'Agustus',
				'9' => 'September',
				'10' => 'Oktober',
				'11' => 'November',
				'12' => 'Desember',
			);
			$data = array(
				'title' => 'Daftar Hadir Mingguan',
				'subtitle' => '',
				'rows' => $qc,
				'bulan' => $bulan,
				'tahun' => $tahun,
				'nama_bulan' => $nama_bulan,
				'tanggal_awal' => $tanggal_awal,
				'tanggal_akhir' => $tanggal_akhir
			);
		$html = $this->load->view('pdf_mingguan_skpd',$data,true);
		input_log($this->user_id,'Cetak Data Absensi Mingguan ke dalam bentuk pdf dengan unit kerja '.$unit_kerja.' bulan:'.$bulan.' tahun:'.$tahun);
		$pdf->WriteHTML($html);
		$pdf->Output();
		
	}
	
	public function combo_minggu()
	{
		$array = array();
		if ($_GET['_name'] == 'tahun') 
		{
			$tahun = $_GET['_value'];
			$array[] = array('1/'.$tahun => 'Januari');
			$array[] = array('2/'.$tahun => 'Februari');
			$array[] = array('3/'.$tahun => 'Maret');
			$array[] = array('4/'.$tahun => 'April');
			$array[] = array('5/'.$tahun => 'Mei');
			$array[] = array('6/'.$tahun => 'Juni');
			$array[] = array('7/'.$tahun => 'Juli');
			$array[] = array('8/'.$tahun => 'Agustus');
			$array[] = array('9/'.$tahun => 'September');
			$array[] = array('10/'.$tahun => 'Oktober');
			$array[] = array('11/'.$tahun => 'November');
			$array[] = array('12/'.$tahun => 'Desember');
		}
		elseif ($_GET['_name'] == 'bulan') 
		{
			$bulan = $_GET['_value'];
			$bulan = explode('/',$bulan);
			$bln = $bulan[0];
			$thn = $bulan[1];
		//	$ming = '';
			$array_minggu = array();
			$jml_hari = date('t',strtotime('1-'.$bln.'-'.$thn));
			for($i=1;$i<=$jml_hari;$i++)
			{
				$hari_dalam_minggu = date('N',strtotime($i.'-'.$bln.'-'.$thn));
				if(($i-$hari_dalam_minggu) < 0)
				{
					if(($hari_dalam_minggu % 7) == 0)
					{
						$tanggal_akhir_minggu = date('j-n-Y',strtotime($i.'-'.$bln.'-'.$thn));
						$time_tanggal_akhir_minggu = strtotime($i.'-'.$bln.'-'.$thn);
						$time_tanggal_awal_minggu = $time_tanggal_akhir_minggu - (6*60*60*24);
						$tanggal_awal_minggu = date('j-n-Y',$time_tanggal_awal_minggu);
						$ming = $tanggal_awal_minggu.'/'.$tanggal_akhir_minggu;
					}
				}
				elseif(($i-$hari_dalam_minggu) >= 0)
				{
					if(($hari_dalam_minggu % 7) == 1)
					{
						$tanggal_awal_minggu = date('j-n-Y',strtotime($i.'-'.$bln.'-'.$thn));
						$time_tanggal_awal_minggu = strtotime($i.'-'.$bln.'-'.$thn);
						$time_tanggal_akhir_minggu = $time_tanggal_awal_minggu + (6*60*60*24);
						$tanggal_akhir_minggu = date('j-n-Y',$time_tanggal_akhir_minggu);
						$ming = $tanggal_awal_minggu.'/'.$tanggal_akhir_minggu;
					}
				}
				if(isset($ming) && $ming != '')
				{
					$array_minggu[$ming] = $ming;
				}
				
			}
			foreach($array_minggu as $arm)
			{
				$array[] = array($arm => $arm);
			}
		}
		
		echo json_encode( $array );
	}
	
	public function mingguan_tkk()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		$tahun = $this->session->userdata('tahun_mingguan_tkk');
		$tahun = ((isset($tahun) && $tahun != '')?$tahun:date('Y'));
		$bulan = $this->session->userdata('bulan_mingguan_tkk');
		$bulan = ((isset($bulan) && $bulan != '')?$bulan:date('n').'/'.$tahun);
		$minggu = $this->session->userdata('minggu_mingguan_tkk');
		if(isset($minggu) && $minggu != '')
		{
			$minggu = $minggu;
		}
		else
		{
			$bulane = explode('/',$bulan);
			$bulane = $bulane[0];
			$hari_ini = date('j');
			$hari_dalam_minggu = date('N');
			$tanggal_basis_minggu = $hari_ini - $hari_dalam_minggu;
			if($tanggal_basis_minggu >0)
			{
				$t = ($tanggal_basis_minggu + 1).'-'.$bulane.'-'.$tahun;
				$tanggal_awal_minggu = date('j-n-Y',strtotime(($tanggal_basis_minggu + 1).'-'.$bulane.'-'.$tahun));
				$tanggal_akhir_minggu = date('j-n-Y',strtotime(($tanggal_basis_minggu + 7).'-'.$bulane.'-'.$tahun));
				$minggu = $tanggal_awal_minggu.'/'.$tanggal_akhir_minggu;
			}
			else
			{
				$time_now = strtotime($hari_ini.'-'.$bulane.'-'.$tahun);
				$time_awal = $time_now - ($hari_dalam_minggu)*60*60*24;
				$time_akhir =  $time_now + (7-$hari_dalam_minggu)*60*60*24;;
				$tanggal_awal_minggu = date('j-n-Y',($time_awal));
				$tanggal_akhir_minggu = date('j-n-Y',($time_akhir));
				$minggu = '';
			}
			
		}
		
		$data = array(
				'title' => 'Data Absensi Mingguan TKK',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'sssubunit' => $this->session->userdata('sssubunit_mingguan_tkk'),
				'nama' => $this->session->userdata('nama_mingguan_tkk'),
				'nik' => $this->session->userdata('nik_mingguan_tkk'),
				'bulan' => $bulan,
				'tahun' => $tahun,
				'minggu' => $minggu,
		);
		$this->load->view('mingguan_tkk',$data);
		input_log($this->user_id,'Lihat Data Mingguan TKK');
	}
	
	public function get_mingguan_tkk()
	{
		//print_r($_REQUEST);
		$order = $_REQUEST['order'][0];
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,'/log/bulanan');
		//$aColumns = array( 'nip', 'nip','nama', 'nip','nip');
		$sssubunit = $this->input->post('sssubunit');
		$this->session->set_userdata('sssubunit_mingguan_tkk',$sssubunit);
		$nama = $this->input->post('nama');
		$this->session->set_userdata('nama_mingguan_tkk',$nama);
		$nik = $this->input->post('nik');
		$this->session->set_userdata('nik_mingguan_tkk',$nik);
		$bulan = $this->input->post('bulan');
		$this->session->set_userdata('bulan_mingguan_tkk',$bulan);
		$tahun = $this->input->post('tahun');
		$this->session->set_userdata('tahun_mingguan_tkk',$tahun);
		$minggu = $this->input->post('minggu');
		$this->session->set_userdata('minggu_mingguan_tkk',$minggu);
		
		$sSearch = '';
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		if(isset($select) && $select != '')
		{
			if(!empty($s_biro)) 
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
			}
			else
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			}
		}
		
		if(isset($nama) && $nama != '')
		{
			$sSearch .= " and (nama like '%".$nama."%') ";
		}
		if(isset($nik) && $nik != '')
		{
			$sSearch .= " and (a.nik = '".$nik."') ";
		}
		//limit
		if ( isset( $_REQUEST['start'] ) && $_REQUEST['length'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_REQUEST['start'] ).", ".
				intval( $_REQUEST['length'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "`".$aColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]."` ".
						($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		//$sOrder = " order by ".$aColumns[$order['column']]." ".$order['dir']." ";
		$s0 = "select count(a.nik) as jml 
				from d_tkk a
				where 1 = 1
				and a.non_aktif = 0
				";
		$s0 .= $sSearch;
		//echo $s0;
		$sheet_total = $this->db->query($s0)->row();
		$sheet_total = $sheet_total->jml;
	
		$s1 = "SELECT DISTINCT a.nik,a.nama,a.tanggal_lahir,
								concat(kuntp,kunkom,kununit,rincunit1,rincunit2,rincunit3) as kunker
				FROM d_tkk a
				WHERE 1=1
			and a.non_aktif = 0";
		$s1 .= $sSearch;
		$s1 .= " order by tanggal_lahir desc, nik asc ";
		
		$s1 .= $sLimit;
	//	echo $s1;
		$sheet1 = $this->db->query($s1);
		$result = array();
		$i=0;
		$no=1;
		foreach($sheet1->result() as $sheet)
		{
			
			$result[$i]['0'] = $no;
			$result[$i]['1'] = $sheet->nik;
			$result[$i]['2'] = $sheet->nama;
			$result[$i]['3'] = $this->get_skpd($sheet->kunker);
			$result[$i]['4'] = 	'<a target="_blank" href="/log/view_mingguan_tkk/'.str_replace(' ','',$sheet->nik).'/'.$bulan.'/'.$minggu.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="View">
									<i class="glyphicon glyphicon-search"></i>
									</a>';	
			$result[$i]['5'] = 	'<a target="_blank" href="/log/pdf_mingguan_tkk/'.str_replace(' ','',$sheet->nik).'/'.$bulan.'/'.$minggu.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="View">
									<i class="glyphicon glyphicon-print"></i>
									</a>';	
			/* $result[$i]['6'] = 	'<a href="/log/refresh_bulanan_tkk/'.str_replace(' ','',$sheet->nik).'/'.$bulan.'/'.$tahun.'" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="View">
									<i class="glyphicon glyphicon-refresh"></i>
									</a>';	 */
			$no++;
			$i++;
		}
		
		$data = $result;
		$results = array(
			"iTotalRecords" => ($sheet_total),
			"iTotalDisplayRecords" => ($sheet_total),
			"aaData"=>$data);
			
		echo json_encode($results);
	}
	
	public function view_mingguan_tkk()
	{
		$nik = $this->uri->segment(3);
		$bulan = $this->uri->segment(4);
		$tahun = $this->uri->segment(5);
		$tanggal_awal = $this->uri->segment(6);
	//	echo $tanggal_awal.'</br>';
		$awal = date('Y-m-d',strtotime($tanggal_awal));
		$tgl_awal = date('d',strtotime($tanggal_awal));
		$bln_awal = date('n',strtotime($tanggal_awal));
		$thn_awal = date('Y',strtotime($tanggal_awal));
		$tanggal_akhir = $this->uri->segment(7);
	//	echo $tanggal_akhir.'</br>';
		$akhir = date('Y-m-d',strtotime($tanggal_akhir));
		$tgl_akhir = date('d',strtotime($tanggal_akhir));
		$bln_akhir = date('n',strtotime($tanggal_akhir));
		$thn_akhir = date('Y',strtotime($tanggal_akhir));
		$sc = "select a.*,
					if(a.jenis_tidak_masuk != 0,(select jenis_ketidakhadiran from referensi_persentase_ketidakhadiran where id = a.jenis_tidak_masuk),'-') as ketidakhadiran
				from d_absen_harian_tkk a
				where nik = '$nik' 
				and (date(a.jadwal_masuk) between '$awal' and '$akhir')
				order by a.tahun asc, bulan asc, tanggal asc
				";
	//	echo $sc;
		$qc = $this->db->query($sc);
		if($qc->num_rows() > 0)
		{
			$sql = "SELECT DISTINCT a.*,(select nunker from referensi_unit_kerja where kunker = concat(kuntp,kunkom,kununit,rincunit1,rincunit2,rincunit3)) as skpd
			FROM d_tkk a
			where nik = '$nik' ";
			$data_peg = $this->db->query($sql)->row_array();
			$nama_bulan = array(
				'1' => 'Januari',
				'2' => 'Februari',
				'3' => 'Maret',
				'4' => 'April',
				'5' => 'Mei',
				'6' => 'Juni',
				'7' => 'Juli',
				'8' => 'Agustus',
				'9' => 'September',
				'10' => 'Oktober',
				'11' => 'November',
				'12' => 'Desember',
			);
			$data = array(
				'title' => 'Daftar Hadir Mingguan',
				'subtitle' => '',
				'rows' => $qc,
				'nik' => $nik,
				'bulan' => $bulan,
				'tahun' => $tahun,
				'data' => $data_peg,
				'nama_bulan' => $nama_bulan,
				'tanggal_awal' => $tanggal_awal,
				'tanggal_akhir' => $tanggal_akhir
			);
			$this->load->view('view_mingguan_tkk',$data);
			input_log($this->user_id,'Lihat Data Absensi Mingguan TKK dengan nip:'.$nik.' bulan:'.$bulan.' tahun:'.$tahun);
		}
	}
	
	public function pdf_mingguan_tkk()
	{
		error_reporting(0);
		$this->load->library('pdf');
		$pdf = $this->pdf->load('utf-8', 'F4');
		$nik = $this->uri->segment(3);
		$bulan = $this->uri->segment(4);
		$tahun = $this->uri->segment(5);
		$tanggal_awal = $this->uri->segment(6);
	//	echo $tanggal_awal.'</br>';
		$awal = date('Y-m-d',strtotime($tanggal_awal));
		$tgl_awal = date('d',strtotime($tanggal_awal));
		$bln_awal = date('n',strtotime($tanggal_awal));
		$thn_awal = date('Y',strtotime($tanggal_awal));
		$tanggal_akhir = $this->uri->segment(7);
	//	echo $tanggal_akhir.'</br>';
		$akhir = date('Y-m-d',strtotime($tanggal_akhir));
		$tgl_akhir = date('d',strtotime($tanggal_akhir));
		$bln_akhir = date('n',strtotime($tanggal_akhir));
		$thn_akhir = date('Y',strtotime($tanggal_akhir));
		$sc = "select a.*,
					if(a.jenis_tidak_masuk != 0,(select jenis_ketidakhadiran from referensi_persentase_ketidakhadiran where id = a.jenis_tidak_masuk),'-') as ketidakhadiran
				from d_absen_harian_tkk a
				where nik = '$nik' 
				and (date(a.jadwal_masuk) between '$awal' and '$akhir')
				order by a.tahun asc, bulan asc, tanggal asc
				";
	//	echo $sc;
		$qc = $this->db->query($sc);
		if($qc->num_rows() > 0)
		{
			$sql = "SELECT DISTINCT a.*,(select nunker from referensi_unit_kerja where kunker = concat(kuntp,kunkom,kununit,rincunit1,rincunit2,rincunit3)) as skpd
			FROM d_tkk a
			where nik = '$nik' ";
			$data_peg = $this->db->query($sql)->row_array();
			$nama_bulan = array(
				'1' => 'Januari',
				'2' => 'Februari',
				'3' => 'Maret',
				'4' => 'April',
				'5' => 'Mei',
				'6' => 'Juni',
				'7' => 'Juli',
				'8' => 'Agustus',
				'9' => 'September',
				'10' => 'Oktober',
				'11' => 'November',
				'12' => 'Desember',
			);
			$data = array(
				'title' => 'Daftar Hadir Mingguan TKK',
				'subtitle' => '',
				'rows' => $qc,
				'nik' => $nik,
				'bulan' => $bulan,
				'tahun' => $tahun,
				'data' => $data_peg,
				'nama_bulan' => $nama_bulan,
				'tanggal_awal' => $tanggal_awal,
				'tanggal_akhir' => $tanggal_akhir
			);
			$html = $this->load->view('pdf_mingguan_tkk',$data,true);
			input_log($this->user_id,'Cetak Data Absensi Mingguan TKK ke dalam bentuk pdf dengan nip:'.$nik.' bulan:'.$bulan.' tahun:'.$tahun);
			$pdf->WriteHTML($html);
			$pdf->Output();
		}
	}

	public function pdf_mingguan_tkk_skpd()
	{
		//print_r($_REQUEST);
		error_reporting(0);
		$this->load->library('pdf');
		$pdf = $this->pdf->load('utf-8', 'F4');
		$sssubunit = $this->input->post('sssubunit');
		$bulans = $this->input->post('sssubunit');
		$bulans = explode('/',$bulans);
		$bulan = $bulans[0];
		$tahun = $bulans[1];
		$minggus = $this->input->post('minggu');
		$minggu = explode('/',$minggus);
		$tanggal_awal = $minggu[0];
		$awal = date('Y-m-d',strtotime($tanggal_awal));
		$tgl_awal = date('d',strtotime($tanggal_awal));
		$bln_awal = date('n',strtotime($tanggal_awal));
		$thn_awal = date('Y',strtotime($tanggal_awal));
		$tanggal_akhir = $minggu[1];
		$akhir = date('Y-m-d',strtotime($tanggal_akhir));
		$tgl_akhir = date('d',strtotime($tanggal_akhir));
		$bln_akhir = date('n',strtotime($tanggal_akhir));
		$thn_akhir = date('Y',strtotime($tanggal_akhir));
		
		$nik = $this->input->post('nik');
		$nama = $this->input->post('nama');
		
		$sSearch = '';
		if($this->session->userdata('s_biro') != '')
		{
			$s_biro = $this->session->userdata('s_biro');
		}
		
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		if(isset($select) && $select != '')
		{
			if(!empty($s_biro)) 
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
			}
			else
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			}
		}
		
		if(isset($nama) && $nama != '')
		{
			$sSearch .= " and (nama like '%".$nama."%') ";
		}
		if(isset($nik) && $nik != '')
		{
			$sSearch .= " and (a.nik = '".$nik."') ";
		}
	
		$s1 = "SELECT DISTINCT a.nik,a.nama,a.tanggal_lahir,
								concat(kuntp,kunkom,kununit,rincunit1,rincunit2,rincunit3) as kunker
				FROM d_tkk a
				WHERE 1=1";
		$s1 .= $sSearch;
		$s1 .= " order by tanggal_lahir desc, nik asc ";
	//	echo $s1;
		$qc = $this->db->query($s1);
		$nama_bulan = array(
				'1' => 'Januari',
				'2' => 'Februari',
				'3' => 'Maret',
				'4' => 'April',
				'5' => 'Mei',
				'6' => 'Juni',
				'7' => 'Juli',
				'8' => 'Agustus',
				'9' => 'September',
				'10' => 'Oktober',
				'11' => 'November',
				'12' => 'Desember',
			);
			$data = array(
				'title' => 'Daftar Hadir Mingguan',
				'subtitle' => '',
				'rows' => $qc,
				'bulan' => $bulan,
				'tahun' => $tahun,
				'nama_bulan' => $nama_bulan,
				'tanggal_awal' => $tanggal_awal,
				'tanggal_akhir' => $tanggal_akhir
			);
		//print_r($data);
		$html = $this->load->view('pdf_mingguan_tkk_skpd',$data,true);
		input_log($this->user_id,'Cetak Data Absensi Mingguan ke dalam bentuk pdf dengan unit kerja '.$unit_kerja.' bulan:'.$bulan.' tahun:'.$tahun);
		$pdf->WriteHTML($html);
		$pdf->Output();
		
	}
	
	//rincian bulanan
	public function rincian_bulanan_pns()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		$tahun = $this->session->userdata('tahun_rincian_bulanan');
		$tahun = ((isset($tahun) && $tahun != '')?$tahun:date('Y'));
		$bulan = $this->session->userdata('bulan_rincian_bulanan');
		$bulan = ((isset($bulan) && $bulan != '')?$bulan:date('n'));
		
		$data = array(
				'title' => 'Data Rincian Bulanan PNS',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'sssubunit' => $this->session->userdata('sssubunit_rincian_bulanan'),
				'bulan' => $bulan,
				'tahun' => $tahun,
		);
		$this->load->view('rincian_bulanan_pns',$data);
		input_log($this->user_id,'Lihat Data Rincian Bulanan PNS');
	}
	
	public function view_rincian_bulanan_pns()
	{
		$bulan = $this->input->post('bulan');
		$tahun = $this->input->post('tahun');
		$sssubunit = $this->input->post('sssubunit');
		
		$result = '';
		$sSearch = '';
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		if(isset($select) && $select != '')
		{
			if(!empty($s_biro)) 
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
			}
			else
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			}
		}
		$s1 = "SELECT DISTINCT a.nip,a.nama,
									a.keselon,
									a.kgolru,
									a.kuntp,
									a.kunkom,
									a.kununit,
									a.tanggal_lahir,
									a.jabatan,
									(select neselon from referensi_eselon where keselon = a.keselon) as eselon,
									(select ngolru from referensi_golongan_ruang where KGOLRU = a.KGOLRU) as golongan,
									(select pangkat from referensi_golongan_ruang where KGOLRU = a.KGOLRU) as pangkat
				FROM d_pegawai a
				WHERE 1=1
				and a.non_aktif = 0";
		$s1 .= $sSearch;
		$s1 .= " order by keselon asc, kgolru desc, nip asc ";
		$q1 = $this->db->query($s1);
		if($q1->num_rows() > 0)
		{
			$jml_hari = date('t',strtotime('1-'.$bulan.'-'.$tahun));
			$result .='<div class="row">
						<div class="col-md-12">
							<div class="panel panel-white">
								<div class="panel-body " style="max-height: 10;overflow-y: scroll;">';
								$result .= '<table class="table table-bordered">';
								$result .= '<tr><td rowspan="2" style="text-align:center">No</td><td rowspan="2"  style="text-align:center">NIP / Nama</td><td colspan="'.$jml_hari.'" style="text-align:center">Tanggal</td></tr>';
								$result .='<tr>';
								for($i=1;$i<=$jml_hari;$i++)
								{
									$result .='<td style="text-align:center">'.$i.'</td>';
								}
								$result .='</tr>';
								$no=1;
								foreach($q1->result() as $row)
								{
									$result .='<tr>';
									$result .='<td>'.$no.'</td>';
									$result .='<td>'.$row->nip.'<br>'.$row->nama.'</td>';
									for($i=1;$i<=$jml_hari;$i++)
									{
										$result .='<td>'.$this->get_absen($row->nip,$i,$bulan,$tahun).'</td>';
									}
									$result .='</tr>';
									$no++;
								}
								$result .= '</table>';
			$result .='</div></div></div></div>';
		}
		else
		{
			$result .= '<div class="row"><div class="panel-body">';
			$result .= '<div class="row">';
			$result .= '<div class="col-md-12">';
			$result .='<div class="alert alert-info alert-dismissable">
							<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
							<strong>Data yang dicari tidak ada</strong>
						</div>';
			$result .='</div></div></div></div>';		
		}
		echo $result;
	}
	
	function get_absen($nip,$tgl,$bulan,$tahun)
	{
		$s = "select a.realisasi_masuk,
					a.realisasi_keluar,
					a.libur,
					a.jenis_tidak_masuk,
					(select jenis_ketidakhadiran from referensi_persentase_ketidakhadiran where id = a.jenis_tidak_masuk) as ketidakhadiran
				from d_absen_harian a
				where nip  = '$nip'
				and tanggal = '$tgl'
				and bulan = '$bulan'
				and tahun = '$tahun'
				";
	//	echo $s.'</br>';
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			if($r->libur == 0)
			{
				if($r->jenis_tidak_masuk == 0)
				{
					$res = 'I:'.date('H:i:s',strtotime($r->realisasi_masuk)).'<br>';
					$res .= 'O:'.date('H:i:s',strtotime($r->realisasi_keluar));
				}
				else
				{
					$res = '<span style="background-color: yellow;" >'.$r->ketidakhadiran.'</span>';
				}
				
			}
			else
			{
				$res = '<span style="background-color: red;" >Libur</span>';
			}
			
		}
		else
		{
			$res = '-';
		}
		//echo $res;
		return $res;
				
	}
	
	public function cetak_rincian_bulanan_pns()
	{
		error_reporting(0);
		set_time_limit(0);
		ini_set('memory_limit','2000M');
		$this->load->library('pdf');
		$pdf = $this->pdf->load('utf-8', 'A3-L');
		$bulan = $this->input->post('bulan');
		$tahun = $this->input->post('tahun');
		$sssubunit = $this->input->post('sssubunit');
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		if(isset($select) && $select != '')
		{
			if(!empty($s_biro)) 
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
			}
			else
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			}
		}
		$s1 = "SELECT DISTINCT a.nip,a.nama,
									a.keselon,
									a.kgolru,
									a.kuntp,
									a.kunkom,
									a.kununit,
									a.tanggal_lahir,
									a.jabatan,
									(select neselon from referensi_eselon where keselon = a.keselon) as eselon,
									(select ngolru from referensi_golongan_ruang where KGOLRU = a.KGOLRU) as golongan,
									(select pangkat from referensi_golongan_ruang where KGOLRU = a.KGOLRU) as pangkat
				FROM d_pegawai a
				WHERE 1=1
				and a.non_aktif = 0";
		$s1 .= $sSearch;
		$s1 .= " order by keselon asc, kgolru desc, nip asc ";
		$q1 = $this->db->query($s1);
		if($q1->num_rows() > 0)
		{
			$jml_hari = date('t',strtotime('1-'.$bulan.'-'.$tahun));
			$unit_kerja = $this->get_unit_kerja($sssubunit);
			$nama_bulan = array(
				'1' => 'Januari',
				'2' => 'Februari',
				'3' => 'Maret',
				'4' => 'April',
				'5' => 'Mei',
				'6' => 'Juni',
				'7' => 'Juli',
				'8' => 'Agustus',
				'9' => 'September',
				'10' => 'Oktober',
				'11' => 'November',
				'12' => 'Desember',
			);
			$data = array(
				'title' => 'Rekap Daftar Hadir Bulanan',
				'subtitle' => '',
				'rows' => $q1,
				'bulan' => $bulan,
				'tahun' => $tahun,
				'nama_bulan' => $nama_bulan,
				'jml_hari' => $jml_hari,
				'unit_kerja' => $unit_kerja,
				'controller' => $this
			);
			$html = $this->load->view('cetak_bulanan_rincian_pns',$data,true);
		//	$this->load->view('cetak_bulanan_rincian_pns',$data);
			input_log($this->user_id,'Cetak Data Absensi rincian bulanan ke dalam bentuk pdf dengan unit kerja '.$unit_kerja.' bulan:'.$bulan.' tahun:'.$tahun);
			$pdf->WriteHTML($html);
			$pdf->Output();
		}
	}
	
	public function rincian_bulanan_tkk()
	{
		$user_groups = $this->ion_auth->get_users_groups()->result_array();
		$hak_akses = get_akses($user_groups,$this->uri_string);
		$tahun = $this->session->userdata('tahun_rincian_bulanan_tkk');
		$tahun = ((isset($tahun) && $tahun != '')?$tahun:date('Y'));
		$bulan = $this->session->userdata('bulan_rincian_bulanan_tkk');
		$bulan = ((isset($bulan) && $bulan != '')?$bulan:date('n'));
		
		$data = array(
				'title' => 'Data Rincian Bulanan TKK',
				'subtitle' => '',
				'hak_akses' => $hak_akses,
				'sssubunit' => $this->session->userdata('sssubunit_rincian_bulanan_tkk'),
				'bulan' => $bulan,
				'tahun' => $tahun,
		);
		$this->load->view('rincian_bulanan_tkk',$data);
		input_log($this->user_id,'Lihat Data Rincian Bulanan TKK');
	}
	
	public function view_rincian_bulanan_tkk()
	{
		$bulan = $this->input->post('bulan');
		$tahun = $this->input->post('tahun');
		$sssubunit = $this->input->post('sssubunit');
		
		$result = '';
		$sSearch = '';
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		if(isset($select) && $select != '')
		{
			if(!empty($s_biro)) 
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
			}
			else
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			}
		}
		$s1 = "SELECT DISTINCT a.nik,a.nama,
									a.kuntp,
									a.kunkom,
									a.kununit,
									a.tanggal_lahir
				FROM d_tkk a
				WHERE 1=1
				and a.non_aktif = 0";
		$s1 .= $sSearch;
		$s1 .= " order by nik asc ";
		$q1 = $this->db->query($s1);
		if($q1->num_rows() > 0)
		{
			$jml_hari = date('t',strtotime('1-'.$bulan.'-'.$tahun));
			$result .='<div class="row">
						<div class="col-md-12">
							<div class="panel panel-white">
								<div class="panel-body " style="max-height: 10;overflow-y: scroll;">';
								$result .= '<table class="table table-bordered">';
								$result .= '<tr><td rowspan="2" style="text-align:center">No</td><td rowspan="2"  style="text-align:center">NIK / Nama</td><td colspan="'.$jml_hari.'" style="text-align:center">Tanggal</td></tr>';
								$result .='<tr>';
								for($i=1;$i<=$jml_hari;$i++)
								{
									$result .='<td style="text-align:center">'.$i.'</td>';
								}
								$result .='</tr>';
								$no=1;
								foreach($q1->result() as $row)
								{
									$result .='<tr>';
									$result .='<td>'.$no.'</td>';
									$result .='<td>'.$row->nik.'<br>'.$row->nama.'</td>';
									for($i=1;$i<=$jml_hari;$i++)
									{
										$result .='<td>'.$this->get_absen_tkk($row->nik,$i,$bulan,$tahun).'</td>';
									}
									$result .='</tr>';
									$no++;
								}
								$result .= '</table>';
			$result .='</div></div></div></div>';
		}
		else
		{
			$result .= '<div class="row"><div class="panel-body">';
			$result .= '<div class="row">';
			$result .= '<div class="col-md-12">';
			$result .='<div class="alert alert-info alert-dismissable">
							<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
							<strong>Data yang dicari tidak ada</strong>
						</div>';
			$result .='</div></div></div></div>';		
		}
		echo $result;
	}
	
	function get_absen_tkk($nik,$tgl,$bulan,$tahun)
	{
		$s = "select a.realisasi_masuk,
					a.realisasi_keluar,
					a.libur,
					a.jenis_tidak_masuk,
					(select jenis_ketidakhadiran from referensi_persentase_ketidakhadiran where id = a.jenis_tidak_masuk) as ketidakhadiran
				from d_absen_harian_tkk a
				where nik  = '$nik'
				and tanggal = '$tgl'
				and bulan = '$bulan'
				and tahun = '$tahun'
				";
	//	echo $s.'</br>';
		$q = $this->db->query($s);
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			if($r->libur == 0)
			{
				if($r->jenis_tidak_masuk == 0)
				{
					$res = 'I:'.date('H:i:s',strtotime($r->realisasi_masuk)).'<br>';
					$res .= 'O:'.date('H:i:s',strtotime($r->realisasi_keluar));
				}
				else
				{
					$res = '<span style="background-color: yellow;" >'.$r->ketidakhadiran.'</span>';
				}
				
			}
			else
			{
				$res = '<span style="background-color: red;" >Libur</span>';
			}
			
		}
		else
		{
			$res = '-';
		}
		//echo $res;
		return $res;
				
	}
	
	public function cetak_rincian_bulanan_tkk()
	{
		error_reporting(0);
		set_time_limit(0);
		ini_set('memory_limit','2000M');
		$this->load->library('pdf');
		$pdf = $this->pdf->load('utf-8', 'A3-L');
		$bulan = $this->input->post('bulan');
		$tahun = $this->input->post('tahun');
		$sssubunit = $this->input->post('sssubunit');
		(isset($sssubunit))?$select = $sssubunit:$select = NULL;
		if(isset($select) && $select != '')
		{
			if(!empty($s_biro)) 
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					$biro = substr($select,0,4);
					if ($biro <> '1000') 
					{
						$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
					} 
					else 
					{
				//		$where = " AND kuntp = '10'";
					}
				}
			}
			else
			{
				if (substr($select,8,2) != "00") 
				{
					$biro = substr($select,0,10);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1,rincunit2) = '$biro'";
				} 
				elseif (substr($select,6,2) != "00") 
				{
					$biro = substr($select,0,8);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit,rincunit1) = '$biro'";
				} 
				else if ((substr($select,4,2) != "00") && (substr($select,4,2) != "__")) 
				{
					$biro = substr($select,0,6);
					$sSearch .= " AND CONCAT(kuntp,kunkom,kununit) = '$biro'";
				} 
				else if ((substr($select,2,2) != "00") && (substr($select,2,2) != "__")) 
				{
					$biro = substr($select,0,4);
					$sSearch .= " AND CONCAT(kuntp,kunkom) = '$biro'";
				} 
				else 
				{
					
					//	$where = " AND kuntp = '10'";
				
				}
			}
		}
		$s1 = "SELECT DISTINCT a.nik,a.nama,
									a.kuntp,
									a.kunkom,
									a.kununit,
									a.tanggal_lahir
				FROM d_tkk a
				WHERE 1=1
			and a.non_aktif = 0";
		$s1 .= $sSearch;
		$s1 .= " order by nik asc ";
		$q1 = $this->db->query($s1);
		if($q1->num_rows() > 0)
		{
			$jml_hari = date('t',strtotime('1-'.$bulan.'-'.$tahun));
			$unit_kerja = $this->get_unit_kerja($sssubunit);
			$nama_bulan = array(
				'1' => 'Januari',
				'2' => 'Februari',
				'3' => 'Maret',
				'4' => 'April',
				'5' => 'Mei',
				'6' => 'Juni',
				'7' => 'Juli',
				'8' => 'Agustus',
				'9' => 'September',
				'10' => 'Oktober',
				'11' => 'November',
				'12' => 'Desember',
			);
			$data = array(
				'title' => 'Rekap Daftar Hadir Bulanan TKK',
				'subtitle' => '',
				'rows' => $q1,
				'bulan' => $bulan,
				'tahun' => $tahun,
				'nama_bulan' => $nama_bulan,
				'jml_hari' => $jml_hari,
				'unit_kerja' => $unit_kerja,
				'controller' => $this
			);
			$html = $this->load->view('cetak_bulanan_rincian_tkk',$data,true);
		//	$this->load->view('cetak_bulanan_rincian_pns',$data);
			input_log($this->user_id,'Cetak Data Absensi rincian bulanan TKK ke dalam bentuk pdf dengan unit kerja '.$unit_kerja.' bulan:'.$bulan.' tahun:'.$tahun);
			$pdf->WriteHTML($html);
			$pdf->Output();
		}
	}
	
	/* function cek_libur($tanggal_buat,$sabtu_masuk)
	{
		$tgl = date('Y-m-d',strtotime($tanggal_buat));
		$urutan_hari = date('w',strtotime($tanggal_buat));
		if($sabtu_masuk == 1)
		{
			if($urutan_hari == 0)
			{
				$akhir_pekan = 1;
			}
			else
			{
				$akhir_pekan = 0;
			}
		}
		else
		{
			if($urutan_hari == 0 || $urutan_hari == 6)
			{
				$akhir_pekan = 1;
			}
			else
			{
				$akhir_pekan = 0;
			}
		}
		$sc = "select * from d_hari_libur where tanggal = '$tgl' ";
		$qc = $this->db->query($sc);
		if($qc->num_rows() > 0)
		{
			$tanggal_merah = 1;
		}
		if((isset($tanggal_merah) && $tanggal_merah == 1) or ($akhir_pekan == 1 ))
		{
			$libur = 1;
		}
		else
		{
			$libur = 0;
		}
		return $libur;
	}
	 */
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */