<?php $this->load->view('header');?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('top');?>
  <!-- Left side column. contains the logo and sidebar -->
  
  <?php $this->load->view('sidebar');?>
 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title;?>
        <small><?=$subtitle;?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-white">
					<div class="panel-body">
						<div class="row">
							<form name="formPegawai" id="formPegawai" method="post" action="/log/harian" class="form-horizontal">
							<div class="row">
									<label class="col-sm-2 control-label">Pilih Pegawai</label>
								<div class="col-xs-10">
									<select id="nip" class="form-control select2" name="nip">
										<option value="">&nbsp;</option>
									</select>
								</div>
							</div>	
							<div class="row">
									<label class="col-sm-2 control-label">Periode</label>
								<div class="col-xs-2">
									<select id="bulan" class="form-control" name="bulan">
										<option value="1">Januari</option>
										<option value="2">Februari</option>
										<option value="3">Maret</option>
										<option value="4">April</option>
										<option value="5">Mei</option>
										<option value="6">Juni</option>
										<option value="7">Juli</option>
										<option value="8">Agustus</option>
										<option value="9">September</option>
										<option value="10">Oktober</option>
										<option value="11">November</option>
										<option value="12">Desember</option>
									</select>
								</div>
								<div class="col-xs-2">
									<select id="tahun" class="form-control select2" name="tahun">
										<?php
										$s = "select year(tanggal) as tahun from absensi_log group by tahun";
										$q = $this->db->query($s);
										foreach($q->result_array() as $row)
										{
											echo '<option value="'.$row['tahun'].'">'.$row['tahun'].'</option>';
										}
										
										?>
									</select>
								</div>
							</div>		
							<div class="row">
								<div class="col-sm-2">
								</div>
								<div class="col-sm-2">
										<button id="cari" class="btn btn-block btn-success" type="button">Cari</button>
								</div>						
							</div>
														
							</form>					
						</div>										
					</div>
				</div>
			</div>
			<div class="col-xs-1">
			</div>
		</div>
		<div id="result"></div>
		
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
 
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('javascript');?>
<script src="/assets/js/jquery.chainedSelects.js"></script>
<script src="/assets/js/dataTables/jquery.dataTables.js"></script>
<script src="/assets/js/dataTables/dataTables.bootstrap.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
	
	$( "#nip" ).select2({   
		placeholder: "Ketik NIP/NAMA",
		allowClear: false,
		ajax: {
			url: "/log/cari_nip",
			dataType: 'json',
			delay: 250,
			data: function (params) {
				return {
					q: params.term // search term
				};
			},
			processResults: function (data) {
				return {
					//results: data
					 results: $.map(data, function (item) {
                    return {
                        text: item.NIPNAMA,
                        id: item.NIP
                    }
					 })
				};
			},
			cache: true
		},
		minimumInputLength: 2
	});
	
	$('#cari').click(function(){
			$.post('/log/view_log_absensi',
			{
				'nip': formPegawai.nip.value,
				'bulan': formPegawai.bulan.value,
				'tahun': formPegawai.tahun.value,
			},
			function(data) {
				$('#result').html(data);
				}
			);
	});	

});
</script>
</body>
</html>
