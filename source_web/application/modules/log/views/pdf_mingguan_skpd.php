<?php
foreach($rows->result() as $r)
{
	$nip = $r->nip;
	$sql = "SELECT DISTINCT a.nama,
									a.keselon,
									a.kgolru,
									a.kuntp,
									a.kunkom,
									a.kununit,
									a.tanggal_lahir,
									a.jabatan,
									(select neselon from referensi_eselon where keselon = a.keselon) as eselon,
									(select ngolru from referensi_golongan_ruang where KGOLRU = a.KGOLRU) as golongan,
									(select pangkat from referensi_golongan_ruang where KGOLRU = a.KGOLRU) as pangkat
			FROM d_pegawai a
			where nip = '$nip' ";
			$data = $this->db->query($sql)->row_array();
			
?>
							<table>
								<tr>
									<td>NIP</td><td>:</td><td><?=$nip;?></td>
								</tr>
								<tr>
									<td>Nama</td><td>:</td><td><?=$data['nama'];?></td>
								</tr>
								<tr>
									<td>Golongan</td><td>:</td><td><?=$data['golongan'];?></td>
								</tr>
								<tr>
									<td>Jabatan</td><td>:</td><td><?=$data['jabatan'];?></td>
								</tr>
								<tr>
									<td>Eselon</td><td>:</td><td><?=$data['eselon'];?></td>
								</tr>
								<tr>
									<td>Periode</td><td>:</td><td><?=$tanggal_awal;?> s/d <?=$tanggal_akhir;?></td>
								</tr>
							</table>
<?php
$awal = date('Y-m-d',strtotime($tanggal_awal));
$akhir = date('Y-m-d',strtotime($tanggal_akhir));
$sc = "select a.*,
					if(a.jenis_tidak_masuk != 0,(select jenis_ketidakhadiran from referensi_persentase_ketidakhadiran where id = a.jenis_tidak_masuk),'-') as ketidakhadiran
				from d_absen_harian a
				where nip = '$nip' 
				and (date(a.jadwal_masuk) between '$awal' and '$akhir')
				order by a.tahun asc, bulan asc, tanggal asc
				";
	//	echo $sc;
		$qc = $this->db->query($sc);
?>
							<table border="1" cellpadding="2" cellspacing="0" style="border:1px #000000 solid; font-family:Arial, Helvetica, sans-serif; font-size:11px;">
							<thead>
							<tr>
								<th>No</th>
								<th>Tanggal</th>
								<th>Jadwal Masuk</th>
								<th>Waktu Masuk</th>
								<th>Keterlambatan (Menit)</th>
								<th>Jadwal Pulang</th>
								<th>Waktu Pulang</th>
								<th>Pulang Cepat (Menit)</th>
								<th>Tidak Masuk</th>
								<th>Persentase Pemotongan (%)</th>
							</tr>
							</thead>
							<tbody>
							<?php
							$i= 1;
							foreach($qc->result_array() as $row)
							{
								if($row['libur'] == 1)
								{
									$ket='style="background:red"';
								}
								else
								{
									$ket = '';
								}
								
								echo '<tr '.$ket.'>';
								echo '<td>'.$i.'</td>';
								echo '<td>'.date('d-M-Y',strtotime($row['tahun'].'-'.$row['bulan'].'-'.$row['tanggal'])).'</td>';
								echo '<td style="text-align:right">'.((isset($row['jadwal_masuk']) && $row['jadwal_masuk'] != NULL && $row['jadwal_masuk'] != '' && $row['jadwal_masuk'] != '0000-00-00')?date('d-M-Y H:i:s',strtotime($row['jadwal_masuk'])):'-').'</td>';
								echo '<td style="text-align:right">'.((isset($row['realisasi_masuk']) && $row['realisasi_masuk'] != NULL && $row['realisasi_masuk'] != '' && $row['realisasi_masuk'] != '0000-00-00')?date('d-M-Y H:i:s',strtotime($row['realisasi_masuk'])):'-').'</td>';
								echo '<td style="text-align:right">'.$row['keterlambatan_masuk'].'</td>';
								echo '<td style="text-align:right">'.((isset($row['jadwal_keluar']) && $row['jadwal_keluar'] != NULL && $row['jadwal_keluar'] != '' && $row['jadwal_keluar'] != '0000-00-00')?date('d-M-Y H:i:s',strtotime($row['jadwal_keluar'])):'-').'</td>';
								echo '<td style="text-align:right">'.((isset($row['realisasi_keluar']) && $row['realisasi_keluar'] != NULL && $row['realisasi_keluar'] != '' && $row['realisasi_keluar'] != '0000-00-00')?date('d-M-Y H:i:s',strtotime($row['realisasi_keluar'])):'-').'</td>';
								echo '<td style="text-align:right">'.$row['kecepatan_keluar'].'</td>';
								echo '<td>'.$row['ketidakhadiran'].'</td>';
								echo '<td style="text-align:right">'.$row['persentase_potongan'].'</td>';
								echo '</tr>';
								$i++;
							}
							?>
							</tbody>
							</table>
							<pagebreak>

<?php
}
?>