<?php $this->load->view('header');?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('top');?>
  <!-- Left side column. contains the logo and sidebar -->
  
  <?php $this->load->view('sidebar');?>
 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title;?>
        <small><?=$subtitle;?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">		
		<div class="row">
		<?php
		if(isset($rows))
		{	
			if($rows->num_rows() > 0)
			{
			?>
			<div class="col-xs-12">
				<div class="panel panel-success">
                    <div class="panel-heading">
                        Data Absensi Pegawai 
                    </div>
					<div class="panel-body">
						
                        <div class="table-responsive">
							<table class="table table-striped table-bordered">
							<thead>
							<tr>
								<th>No</th>
								<th>Serial Number</th>
								<th>PIN</th>
								<th>NIP</th>
								<th>Tanggal</th>
							</tr>
							</thead>
							<tbody>
							<?php
							$i= 1;
							$total_pemotongan = 0;
							foreach($rows->result_array() as $row)
							{
								echo '<tr>';
								echo '<td>'.$i.'</td>';
								echo '<td>'.$row['serial_number'].'</td>';
								echo '<td style="text-align:left">'.($row['pin']).'</td>';
								echo '<td style="text-align:left">'.($row['nip']).'</td>';
								echo '<td style="text-align:left">'.((isset($row['tanggal']) && $row['tanggal'] != NULL && $row['tanggal'] != '' && $row['tanggal'] != '0000-00-00')?date('d-M-Y H:i:s',strtotime($row['tanggal'])):'-').'</td>';
								echo '</tr>';
								$i++;
							}
							?>
							</tbody>
							</table>
						</div>
					
					</div>
				</div>
			</div>
			<?php
			}
			else
			{
				?>
				<div class="col-xs-12">
				<div class="alert alert-success alert-dismissable">
					<strong>Data yang dicari tidak ada!</strong>				
				</div>
				</div>
				<?php
			}
		}
		?>
		</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
 
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('javascript');?>
<script src="/assets/js/jquery.chainedSelects.js"></script>
<script src="/assets/js/dataTables/jquery.dataTables.js"></script>
<script src="/assets/js/dataTables/dataTables.bootstrap.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
		
		$('#tabel').dataTable({
					"bFilter": false, 
					"bInfo": true,
					"bLengthChange": false,
		});
		
		});
		</script>
</body>
</html>
