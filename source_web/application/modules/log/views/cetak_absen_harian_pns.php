<?php
setlocale(LC_TIME, 'id_ID.utf8');
$hari = strftime( "%A", strtotime($tanggal));
?>
<html>
<head>
</head>
<body>
<h3>Data Kehadiran Harian Pegawai PNS di <?=$unit_kerja;?>, pada hari <?=$hari;?> tanggal <?=date('d-M-Y',strtotime($tanggal));?></h3>
<table border="1" cellpadding="2" cellspacing="0" style="border:1px #000000 solid; font-family:Arial, Helvetica, sans-serif; font-size:11px;">
	<tr><td>No</td>
		<td>NIP / Nama / Golongan</td>
		<td>Jabatan / Eselon</td>
		<td>Jadwal Masuk</td>
		<td>Waktu Masuk</td>
		<td>Keterlambatan (Menit)</td>
		<td>Jadwal Pulang</td>
		<td>Waktu Pulang</td>
		<td>Pulang Cepat (Menit)</td>
		<td>Tidak Masuk</td>
		<td>Persentase Pemotongan (%)</td>	
	</tr>
	<?php
	$no=1;
	foreach($q->result() as $sheet)
	{
		echo '<tr>';
		echo '<td>'.$no.'</td>';
		echo '<td>'.$sheet->nip.'<br>'.$sheet->nama.'<br>'.$sheet->golongan.'</td>';
		echo '<td>'.$sheet->jabatan.'<br>'.$sheet->eselon.'</td>';
		echo '<td>'.((isset($sheet->jadwal_masuk) && $sheet->jadwal_masuk != '' && $sheet->jadwal_masuk != NULL)?date('d-m-Y H:i',strtotime($sheet->jadwal_masuk)):'').'</td>';
		echo '<td>'.((isset($sheet->realisasi_masuk) && $sheet->realisasi_masuk != '' && $sheet->realisasi_masuk != NULL)?date('d-m-Y H:i',strtotime($sheet->realisasi_masuk)):'').'</td>';
		echo '<td style="text-align:right">'.number_format($sheet->keterlambatan_masuk,1,',','.').'</td>';
		echo '<td>'.((isset($sheet->jadwal_keluar) && $sheet->jadwal_keluar != '' && $sheet->jadwal_keluar != NULL)?date('d-m-Y H:i',strtotime($sheet->jadwal_keluar)):'').'</td>';
		echo '<td>'.((isset($sheet->realisasi_keluar) && $sheet->realisasi_keluar != '' && $sheet->realisasi_keluar != NULL)?date('d-m-Y H:i',strtotime($sheet->realisasi_keluar)):'').'</td>';
		echo '<td style="text-align:right">'.number_format($sheet->kecepatan_keluar,1,',','.').'</td>';
		echo '<td>'.$sheet->ketidakhadiran.'</td>';
		echo '<td style="text-align:right">'.number_format($sheet->persentase_potongan,1,',','.').'</td>';
		echo '</tr>';
		$no++;
	}
	?>
</table>
</body>
</html> 
