<?php $this->load->view('header');?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('top');?>
  <!-- Left side column. contains the logo and sidebar -->
  
  <?php $this->load->view('sidebar');?>
 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title;?>
        <small><?=$subtitle;?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">		
		<div class="row">
		<?php
		if(isset($rows))
		{	
			if($rows->num_rows() > 0)
			{
			?>
			<div class="col-xs-12">
				<div class="panel panel-success">
                    <div class="panel-heading">
                        Data Absensi Pegawai 
                    </div>
					<div class="panel-body">
						<div class="row">
								<label  class="col-sm-2 control-label">NIK</label>
								<div class="col-xs-4">
									<?=$nik;?>
								</div>
						</div>
						<div class="row">
								<label  class="col-sm-2 control-label">Nama</label>
								<div class="col-xs-4">
									<?=$data['nama'];?>
								</div>
						</div>
						<div class="row">
								<label  class="col-sm-2 control-label">SKPD</label>
								<div class="col-xs-4">
									<?=$data['skpd'];?>
								</div>
						</div>
						<div class="row">
								<label  class="col-sm-2 control-label">Periode</label>
								<div class="col-xs-4">
									<?=$tanggal_awal;?> s/d <?=$tanggal_akhir;?>
								</div>
						</div>
						
                        <div class="table-responsive">
							<table class="table table-striped table-bordered">
							<thead>
							<tr>
								<th>No</th>
								<th>Tanggal Absen</th>
								<th>Jadwal Masuk</th>
								<th>Waktu Masuk</th>
								<th>Keterlambatan (Menit)</th>
								<th>Jadwal Pulang</th>
								<th>Waktu Pulang</th>
								<th>Pulang Cepat (Menit)</th>
								<th>Tidak Masuk</th>
								<th>Persentase Pemotongan (%)</th>
							</tr>
							</thead>
							<tbody>
							<?php
							$i= 1;
							$total_pemotongan = 0;
							foreach($rows->result_array() as $row)
							{
								if($row['libur'] == 1)
								{
									$ket='style="background:red"';
								}
								elseif($row['keterlambatan_masuk'] > 0)
								{
									$ket='style="background:pink"';
								}
								else
								{
									$ket = '';
								}
								
								
								echo '<tr '.$ket.'>';
								echo '<td>'.$i.'</td>';
								echo '<td>'.date('d-M-Y',strtotime($row['tahun'].'-'.$row['bulan'].'-'.$row['tanggal'])).'</td>';
								echo '<td style="text-align:right">'.((isset($row['jadwal_masuk']) && $row['jadwal_masuk'] != NULL && $row['jadwal_masuk'] != '' && $row['jadwal_masuk'] != '0000-00-00')?date('d-M-Y H:i:s',strtotime($row['jadwal_masuk'])):'-').'</td>';
								echo '<td style="text-align:right">'.((isset($row['realisasi_masuk']) && $row['realisasi_masuk'] != NULL && $row['realisasi_masuk'] != '' && $row['realisasi_masuk'] != '0000-00-00')?date('d-M-Y H:i:s',strtotime($row['realisasi_masuk'])):'-').'</td>';
								echo '<td style="text-align:right">'.$row['keterlambatan_masuk'].'</td>';
								echo '<td style="text-align:right">'.((isset($row['jadwal_keluar']) && $row['jadwal_keluar'] != NULL && $row['jadwal_keluar'] != '' && $row['jadwal_keluar'] != '0000-00-00')?date('d-M-Y H:i:s',strtotime($row['jadwal_keluar'])):'-').'</td>';
								echo '<td style="text-align:right">'.((isset($row['realisasi_keluar']) && $row['realisasi_keluar'] != NULL && $row['realisasi_keluar'] != '' && $row['realisasi_keluar'] != '0000-00-00')?date('d-M-Y H:i:s',strtotime($row['realisasi_keluar'])):'-').'</td>';
								echo '<td style="text-align:right">'.$row['kecepatan_keluar'].'</td>';
								echo '<td>'.$row['ketidakhadiran'].'</td>';
								echo '<td style="text-align:right">'.$row['persentase_potongan'].'</td>';
								echo '</tr>';
								$i++;
								$total_pemotongan = $total_pemotongan + $row['persentase_potongan'];
							}
							?>
							<tr>
								<td colspan="9"  style="text-align:right">Total</td>
								<td  style="text-align:right"><?=$total_pemotongan;?></td>
							</tr>
							</tbody>
							</table>
						</div>
					
					</div>
				</div>
			</div>
			<?php
			}
			else
			{
				?>
				<div class="col-xs-12">
				<div class="alert alert-success alert-dismissable">
					<strong>Data yang dicari tidak ada!</strong>				
				</div>
				</div>
				<?php
			}
		}
		?>
		</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
 
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('javascript');?>
<script src="/assets/js/jquery.chainedSelects.js"></script>
<script src="/assets/js/dataTables/jquery.dataTables.js"></script>
<script src="/assets/js/dataTables/dataTables.bootstrap.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
		
		$('#tabel').dataTable({
					"bFilter": false, 
					"bInfo": true,
					"bLengthChange": false,
		});
		
		});
		</script>
</body>
</html>
