							<table>
								<tr>
									<td>NIP</td><td>:</td><td><?=$nip;?></td>
								</tr>
								<tr>
									<td>Nama</td><td>:</td><td><?=$data['nama'];?></td>
								</tr>
								<tr>
									<td>Golongan</td><td>:</td><td><?=$data['golongan'];?></td>
								</tr>
								<tr>
									<td>Jabatan</td><td>:</td><td><?=$data['jabatan'];?></td>
								</tr>
								<tr>
									<td>Eselon</td><td>:</td><td><?=$data['eselon'];?></td>
								</tr>
								<tr>
									<td>Periode</td><td>:</td><td><?=$nama_bulan[$bulan];?>-<?=$tahun;?></td>
								</tr>
							</table>
							<table border="1" cellpadding="2" cellspacing="0" style="border:1px #000000 solid; font-family:Arial, Helvetica, sans-serif; font-size:11px;">
							<thead>
							<tr>
								<th>No</th>
								<th>Tanggal</th>
								<th>Besaran Tunjangan (Rp.)</th>
								<th>Proporsi Kinerja (Rp.)</th>
								<th>Proporsi Kedisiplinan (Absensi) (Rp.)</th>
								<th>Persentase Pemotongan (%)</th>
								<th>Total Pemotongan  (Rp.)</th>
							</tr>
							</thead>
							<tbody>
							<?php
							$i= 1;
							$total_pemotongan = 0;
							$total_pemotongan_tpp =0;
							foreach($rows->result_array() as $row)
							{
								echo '<tr>';
								echo '<td>'.$i.'</td>';
								echo '<td>'.date('d-M-Y',strtotime($row['tanggal'])).'</td>';
								echo '<td style="text-align:right">'.number_format($row['total_tpp'],2,',','.').'</td>';
								echo '<td style="text-align:right">'.number_format($row['tpp_statis'],2,',','.').'</td>';
								echo '<td style="text-align:right">'.number_format($row['tpp_dinamis'],2,',','.').'</td>';
								echo '<td style="text-align:right">'.$row['pemotongan_per_hari'].'</td>';
								echo '<td style="text-align:right">'.number_format($row['tpp_dinamis']*($row['pemotongan_per_hari']/100),2,',','.').'</td>';
								echo '</tr>';
								$i++;
								$total_pemotongan = $total_pemotongan + $row['pemotongan_per_hari'];
								$total_pemotongan_tpp = $total_pemotongan_tpp + ($row['tpp_dinamis']*($row['pemotongan_per_hari']/100));
								
							}
							?>
							<tr>
								<td colspan="5"  style="text-align:right">Total Persentase Pemotongan (%)</td>
								<td  style="text-align:right"><?=$total_pemotongan;?></td>
							</tr>
							<tr>
								<td colspan="6"  style="text-align:right">Total Pemotongan (Rp)</td>
								<td  style="text-align:right"><?=number_format($total_pemotongan_tpp,2,',','.');?></td>
							</tr>
							<tr>
								<td colspan="6"  style="text-align:right">Total Tunjangan Diterima Sebelum Pajak</td>
								<td  style="text-align:right"><?=number_format(($row['tpp_statis']+$row['tpp_dinamis']-$total_pemotongan_tpp),2,',','.');?></td>
							</tr>
							</tbody>
							</table>