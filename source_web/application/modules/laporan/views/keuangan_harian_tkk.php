<?php $this->load->view('header');?>
<?php
if($sssubunit == '')
{
	$sssubunit = $this->s_biro;
}
if(substr($sssubunit,2,2) == '10' || substr($sssubunit,2,2) == '11')
{
	$unit = substr($sssubunit,0,6).'000000';
	$subunit = substr($sssubunit,0,8).'0000';
	$ssubunit = substr($sssubunit,0,10).'00';
}	
else
{
	$unit = substr($sssubunit,0,4).'00000000';
	$subunit = substr($sssubunit,0,6).'000000';
	$ssubunit = substr($sssubunit,0,8).'0000';
}

?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('top');?>
  <!-- Left side column. contains the logo and sidebar -->
  
  <?php $this->load->view('sidebar');?>
 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title;?>
        <small><?=$subtitle;?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-white">
					<div class="panel-body">
						<div class="row">
							<form name="formPegawai" id="formPegawai" method="post" action="/log/harian" class="form-horizontal">
							<input type="hidden" name="cari" value="1">
							<div class="row">
									<label class="col-sm-2 control-label">Unit Kerja</label>
								<div class="col-xs-10">
									<select class="form-control" name="unit" id="unit">
									<?php
									$s_biro = $this->session->userdata('s_biro');
									//echo 'ssssssssssssssssbiro: '.$s_biro.'</br>';
									if (!empty($s_biro) && $s_biro != '100000000000') 
									{
										$sql = "SELECT * 
												FROM referensi_unit_kerja 
												WHERE (IF(MID('$s_biro',3,2)=10 || MID('$s_biro',3,2)=11,
													mid(kunker,1,6) = mid('$s_biro',1,6)  ,
													mid(kunker,1,4) = mid('$s_biro',1,4)  )
												)
												AND (IF(MID('$s_biro',3,2)=10 || MID('$s_biro',3,2)=11,
													mid(kunker,7,6) = '000000' ,
													mid(kunker,5,8) = '00000000' )
												)												
												ORDER BY KUNKER";
										//echo $sql.'</br>';
										$query = $this->db->query($sql);
										foreach ($query->result_array() as $row)
										{ 
											echo '<option value ="'.$row['KUNKER'].'" '.((isset($unit) && $unit == $row['KUNKER'])?"selected":"").'>'.($row['NUNKER']).'</option>';
										}
										
									} 											
									else 
									{
										$sql = "SELECT * FROM referensi_unit_kerja 
												WHERE (IF(MID(kunker,3,2)=10 || MID(kunker,3,2)=11,
													mid(kunker,7,6) = '000000' ,
													mid(kunker,5,8) = '00000000' )
												)
												
												ORDER BY NUNKER";
										$query = $this->db->query($sql);
										echo '<option value="100000000000" '.((isset($unit) && ($unit == '100000000000'))?"selected":"").'>Semua</option>';			
										foreach ($query->result_array() as $row)
										{ 
											echo '<option value ="'.$row['KUNKER'].'" '.((isset($unit) && $unit == $row['KUNKER'])?"selected":"").'>'.($row['NUNKER']).'</option>';
										} 								
									}
									?>
									
									</select>
								</div>
							</div>
							<div class="row">
									<label class="col-sm-2 control-label">Sub Unit Kerja</label>
								<div class="col-xs-10">
									<select class="form-control" name="subunit" id="subunit">
									<?php 
									if (!empty($s_biro) && $s_biro != '100000000000') 
									{
										//$subunit = $s_biro;
										$rincunit1 = substr($s_biro,6,2);
										if(isset($subunit) && $subunit != NULL && $subunit != '')
										{
											if($rincunit1 != '00')
											{
												$sql = "SELECT * 
													FROM referensi_unit_kerja 
													WHERE (IF(MID('$subunit',3,2)=10 || MID('$subunit',3,2)=11,
														mid(kunker,1,8) = mid('$subunit',1,8),
														mid(kunker,1,6) = mid('$subunit',1,6)  )
													)
													AND (IF(MID('$subunit',3,2)=10 || MID('$subunit',3,2)=11,
														mid(kunker,9,4) = '0000' ,
														mid(kunker,7,6) = '000000' )
													)
													ORDER BY KUNKER";
											}
											else
											{
												$sql = "SELECT * 
													FROM referensi_unit_kerja 
													WHERE (IF(MID('$subunit',3,2)=10 || MID('$subunit',3,2)=11,
														mid(kunker,1,6) = mid('$subunit',1,6),
														mid(kunker,1,4) = mid('$subunit',1,4)  )
													)
													AND (IF(MID('$subunit',3,2)=10 || MID('$subunit',3,2)=11,
														mid(kunker,9,4) = '0000' ,
														mid(kunker,7,6) = '000000' )
													)
													ORDER BY KUNKER";
											}
											
											$query = $this->db->query($sql);
											echo $sql;
											foreach ($query->result_array() as $row)
											{ 
												echo '<option value ="'.$row['KUNKER'].'" '.((isset($subunit) && $subunit == $row['KUNKER'])?"selected":"").'>'.($row['NUNKER']).'</option>';
											}
										}
										else
										{
											if($rincunit1 != '00')
											{
												$sql = "SELECT * 
													FROM referensi_unit_kerja 
													WHERE (IF(MID('$s_biro',3,2)=10 || MID('$s_biro',3,2)=11,
														mid(kunker,1,8) = mid('$s_biro',1,8),
														mid(kunker,1,6) = mid('$s_biro',1,6)  )
													)
													AND (IF(MID('$s_biro',3,2)=10 || MID('$s_biro',3,2)=11,
														mid(kunker,9,4) = '0000' ,
														mid(kunker,7,6) = '000000' )
													)
													ORDER BY KUNKER";
											}
											else
											{
												$sql = "SELECT * 
													FROM referensi_unit_kerja 
													WHERE (IF(MID('$s_biro',3,2)=10 || MID('$s_biro',3,2)=11,
														mid(kunker,1,6) = mid('$s_biro',1,6),
														mid(kunker,1,4) = mid('$s_biro',1,4)  )
													)
													AND (IF(MID('$s_biro',3,2)=10 || MID('$s_biro',3,2)=11,
														mid(kunker,9,4) = '0000' ,
														mid(kunker,7,6) = '000000' )
													)
													ORDER BY KUNKER";
											}
											
													//echo $sql;
											$query = $this->db->query($sql);
											foreach ($query->result_array() as $row)
											{ 
												echo '<option value ="'.$row['KUNKER'].'" '.((isset($subunit) && $subunit == $row['KUNKER'])?"selected":"").'>'.($row['NUNKER']).'</option>';
											}
										}
									}
									else
									{
										//$subunit = $s_biro;
										if(isset($subunit) && $subunit != NULL)
										{
														$sql = "SELECT * 
															FROM referensi_unit_kerja 
															WHERE MID(KUNKER,1,4) = '".substr($subunit,0,4)."' 										
															AND MID(KUNKER,7,6) = '000000'
															
															ORDER BY KUNKER";
											$sql = "SELECT * 
												FROM referensi_unit_kerja 
												WHERE (IF(MID('$subunit',3,2)=10 || MID('$subunit',3,2)=11,
													mid(kunker,1,6) = mid('$subunit',1,6)  ,
													mid(kunker,1,4) = mid('$subunit',1,4)  )
												)
												AND (IF(MID('$subunit',3,2)=10 || MID('$subunit',3,2)=11,
													mid(kunker,9,4) = '0000' ,
													mid(kunker,7,6) = '000000' )
												)
												ORDER BY KUNKER";
														//echo $sql;
														$query = $this->db->query($sql);
														if($query->num_rows() > 0)
														{
															foreach ($query->result_array() as $row)
															{ 
																echo '<option value ="'.$row['KUNKER'].'" '.((isset($subunit) && $subunit == $row['KUNKER'])?"selected":"").'>'.($row['NUNKER']).'</option>';
															}
														}
														else
														{
															echo '<option value="100000000000" '.((isset($subunit) && ($subunit == '100000000000'))?"selected":"").'>Semua</option>';			
														}
										}
										else
										{
											echo '<option value="100000000000">Semua</option>';
										}
									}
									?>	
									</select>
								</div>
							</div>
							<div class="row">
									<label  class="col-sm-2 control-label">Sub Sub Unit Kerja</label>
								<div class="col-xs-10">
									<select class="form-control" name="ssubunit" id="ssubunit">
										<?php 
									if (!empty($s_biro) && $s_biro != '100000000000') 
									{
									//	$ssubunit = $s_biro;
										$rincunit2 = substr($s_biro,8,2);
										if(isset($ssubunit) && $ssubunit != NULL && $ssubunit != '')
										{
											if($rincunit1 != '00')
											{
												$sql = "SELECT * 
												FROM referensi_unit_kerja 
												WHERE (IF(MID('$ssubunit',3,2)=10 || MID('$ssubunit',3,2)=11,
													mid(kunker,1,10) = mid('$ssubunit',1,10)  ,
													mid(kunker,1,8) = mid('$ssubunit',1,8)  )
												)
												AND (IF(MID('$ssubunit',3,2)=10 || MID('$ssubunit',3,2)=11,
													mid(kunker,11,2) = '00' ,
													mid(kunker,9,4) = '0000' )
												)												
												ORDER BY NUNKER";
											}
											else
											{
												$sql = "SELECT * 
												FROM referensi_unit_kerja 
												WHERE (IF(MID('$ssubunit',3,2)=10 || MID('$ssubunit',3,2)=11,
													mid(kunker,1,8) = mid('$ssubunit',1,8)  ,
													mid(kunker,1,6) = mid('$ssubunit',1,6)  )
												)
												AND (IF(MID('$ssubunit',3,2)=10 || MID('$ssubunit',3,2)=11,
													mid(kunker,11,2) = '00' ,
													mid(kunker,9,4) = '0000' )
												)												
												ORDER BY NUNKER";
											}
											
											
											$query = $this->db->query($sql);
											foreach ($query->result_array() as $row)
											{ 
												echo '<option value ="'.$row['KUNKER'].'" '.((isset($ssubunit) && $ssubunit == $row['KUNKER'])?"selected":"").'>'.($row['NUNKER']).'</option>';
											}
										}
										else
										{
											if($rincunit1 != '00')
											{
												$sql = "SELECT * 
												FROM referensi_unit_kerja 
												WHERE (IF(MID('$s_biro',3,2)=10 || MID('$s_biro',3,2)=11,
													mid(kunker,1,10) = mid('$s_biro',1,10)  ,
													mid(kunker,1,8) = mid('$s_biro',1,8)  )
												)
												AND (IF(MID('$s_biro',3,2)=10 || MID('$s_biro',3,2)=11,
													mid(kunker,11,2) = '00' ,
													mid(kunker,9,4) = '0000'  )
												)
												
												ORDER BY NUNKER";
											}
											else
											{
												$sql = "SELECT * 
												FROM referensi_unit_kerja 
												WHERE (IF(MID('$s_biro',3,2)=10 || MID('$s_biro',3,2)=11,
													mid(kunker,1,8) = mid('$s_biro',1,8)  ,
													mid(kunker,1,6) = mid('$s_biro',1,6)  )
												)
												AND (IF(MID('$s_biro',3,2)=10 || MID('$s_biro',3,2)=11,
													mid(kunker,11,2) = '00' ,
													mid(kunker,9,4) = '0000'  )
												)
												
												ORDER BY NUNKER";
											}
											
													//echo $sql;
											$query = $this->db->query($sql);
											foreach ($query->result_array() as $row)
											{ 
												echo '<option value ="'.$row['KUNKER'].'" '.((isset($ssubunit) && $ssubunit == $row['KUNKER'])?"selected":"").'>'.($row['NUNKER']).'</option>';
											}
										}
										
									}
									else
									{
										if(isset($ssubunit) && $ssubunit != NULL)
													{
														$sql = "SELECT * 
															FROM referensi_unit_kerja 
															WHERE (IF(MID('$ssubunit',3,2)=10 || MID('$ssubunit',3,2)=11,
																mid(kunker,1,8) = mid('$ssubunit',1,8)  ,
																mid(kunker,1,6) = mid('$ssubunit',1,6)  )
															)
															AND (IF(MID('$ssubunit',3,2)=10 || MID('$ssubunit',3,2)=11,
																mid(kunker,11,2) = '00' ,
																mid(kunker,9,4) = '0000' )
															)
															
															ORDER BY NUNKER";
														//echo $sql;
														$query = $this->db->query($sql);
														if($query->num_rows() > 0)
														{
															foreach ($query->result_array() as $row)
															{ 
																echo '<option value ="'.$row['KUNKER'].'" '.((isset($ssubunit) && $ssubunit == $row['KUNKER'])?"selected":"").'>'.($row['NUNKER']).'</option>';
															}
														}
														else
														{
															echo '<option value="100000000000" '.((isset($ssubunit) && ($ssubunit == '100000000000'))?"selected":"").'>Semua</option>';
														}
													}
													else
													{
														echo '<option value="100000000000">Semua</option>';
													}
									}
									?>	
									</select>
								</div>
							</div>
							<div class="row">
									<label class="col-sm-2 control-label">Sub Sub Sub Unit Kerja</label>
								<div class="col-xs-10">
									<select class="form-control" name="sssubunit" id="sssubunit">
										<?php 
									if (!empty($s_biro) && $s_biro != '100000000000') 
									{
									//	$sssubunit = $s_biro;
										$rincunit3 = substr($s_biro,10,2);
										if(isset($sssubunit) && $sssubunit != NULL)
										{
											if($rincunit2 != '00')
											{
												$sql = "SELECT * 
												FROM referensi_unit_kerja 
												WHERE (IF(MID('$sssubunit',3,2)=10 || MID('$sssubunit',3,2)=11,
													mid(kunker,1,12) = mid('$sssubunit',1,12)  ,
													mid(kunker,1,10) = mid('$sssubunit',1,10)  )
												)
												AND (IF(MID('$sssubunit',3,2)=10 || MID('$sssubunit',3,2)=11,
													1=1,
													mid(kunker,11,2) = '00' )
												)
												
												ORDER BY NUNKER";
											}
											else
											{
												$sql = "SELECT * 
												FROM referensi_unit_kerja 
												WHERE (IF(MID('$sssubunit',3,2)=10 || MID('$sssubunit',3,2)=11,
													mid(kunker,1,10) = mid('$sssubunit',1,10)  ,
													mid(kunker,1,8) = mid('$sssubunit',1,8)  )
												)
												AND (IF(MID('$sssubunit',3,2)=10 || MID('$sssubunit',3,2)=11,
													1=1,
													mid(kunker,11,2) = '00' )
												)
												
												ORDER BY NUNKER";
											}
											
											//echo $sql;
											$query = $this->db->query($sql);
											foreach ($query->result_array() as $row)
											{ 
												echo '<option value ="'.$row['KUNKER'].'" '.((isset($sssubunit) && $sssubunit == $row['KUNKER'])?"selected":"").'>'.($row['NUNKER']).'</option>';
											}
										}
										else
										{
											if($rincunit1 != '00')
											{
												$sql = "SELECT * 
												FROM referensi_unit_kerja 
												WHERE (IF(MID('$s_biro',3,2)=10 || MID('$s_biro',3,2)=11,
													mid(kunker,1,6) = mid('$s_biro',1,6)  ,
													mid(kunker,1,4) = mid('$s_biro',1,4)  )
												)
												AND (IF(MID('$s_biro',3,2)=10 || MID('$s_biro',3,2)=11,
													1=1,
													mid(kunker,11,2) = '00'  )
												)
												
												ORDER BY NUNKER";
											}
											else
											{
												$sql = "SELECT * 
												FROM referensi_unit_kerja 
												WHERE (IF(MID('$s_biro',3,2)=10 || MID('$s_biro',3,2)=11,
													mid(kunker,1,10) = mid('$s_biro',1,10)  ,
													mid(kunker,1,8) = mid('$s_biro',1,8)  )
												)
												AND (IF(MID('$s_biro',3,2)=10 || MID('$s_biro',3,2)=11,
													1=1,
													mid(kunker,11,2) = '00'  )
												)
												
												ORDER BY NUNKER";
											}
											
											//		echo $sql;
											$query = $this->db->query($sql);
											foreach ($query->result_array() as $row)
											{ 
												echo '<option value ="'.$row['KUNKER'].'" '.((isset($ssubunit) && $ssubunit == $row['KUNKER'])?"selected":"").'>'.($row['NUNKER']).'</option>';
											}
										}
										
									}
									else
									{
										if(isset($sssubunit) && $sssubunit != NULL)
													{
														$sql = "SELECT * 
												FROM referensi_unit_kerja 
												WHERE (IF(MID('$sssubunit',3,2)=10 || MID('$sssubunit',3,2)=11,
													mid(kunker,1,10) = mid('$sssubunit',1,10)  ,
													mid(kunker,1,8) = mid('$sssubunit',1,8)  )
												)
												AND (IF(MID('$sssubunit',3,2)=10 || MID('$sssubunit',3,2)=11,
													1=1,
													mid(kunker,11,2) = '00' )
												)
												
												ORDER BY NUNKER";
														$query = $this->db->query($sql);
														if($query->num_rows() > 0)
														{
															foreach ($query->result_array() as $row)
															{ 
																echo '<option value ="'.$row['KUNKER'].'" '.((isset($sssubunit) && $sssubunit == $row['KUNKER'])?"selected":"").'>'.($row['NUNKER']).'</option>';
															}
														}
														else
														{
															echo '<option value="100000000000" '.((isset($sssubunit) && ($sssubunit == '100000000000'))?"selected":"").'>Semua</option>';
														}
													}
													else
													{
														echo '<option value="100000000000">Semua</option>';
													}
									}
									?>	
									</select>
								</div>
							</div>
							<div class="row">
									<label class="col-sm-2 control-label">Jenis Jabatan</label>
								<div class="col-xs-4">
									<select class="form-control" name="jnsjab" id="jnsjab">
									<?php
										$sql = "SELECT * FROM referensi_jenis_jabatan ORDER BY JNSJAB";	
										$query = $this->db->query($sql);
										echo '<option value="0" '.((isset($jnsjab) && ($jnsjab == '0'))?"selected":"").'>Semua</option>';			
										foreach ($query->result_array() as $row)
										{ 
											echo '<option value ="'.$row['JNSJAB'].'" '.((isset($jnsjab) && $jnsjab == $row['JNSJAB'])?"selected":"").'>'.($row['NJNSJAB']).'</option>';
										} 								
									
									?>
									</select>
								</div>
									<label  class="col-sm-2 control-label">Eselon</label>
								<div class="col-xs-4">
									<select class="form-control" name="keselon" id="keselon">
										<?php
										$sql = "SELECT * FROM referensi_eselon ORDER BY KESELON";	
										$query = $this->db->query($sql);
										echo '<option value="0" '.((isset($keselon) && ($keselon == '0'))?"selected":"").'>Semua</option>';			
										foreach ($query->result_array() as $row)
										{ 
											echo '<option value ="'.$row['KESELON'].'" '.((isset($keselon) && $keselon == $row['KESELON'])?"selected":"").'>'.($row['NESELON']).'</option>';
										} 								
									
									?>
									</select>
								</div>
							</div>	
							
							<div class="row">
									<label  class="col-sm-2 control-label">Nama</label>
								<div class="col-xs-4">
									<input class="form-control" type="text" name="nama" id="nama" value="<?=$nama;?>">
								</div>
									<label  class="col-sm-2 control-label">NIK</label>
								<div class="col-xs-4">
									<input class="form-control" type="text" name="nik" id="nik" value="<?=$nik;?>">
								</div>
							</div>
							<div class="row">
									<label  class="col-sm-2 control-label">Tanggal</label>
								<div class="col-xs-4">
									<input class="form-control pull-right datepicker" id="tanggal" type="text" name="tanggal" value="<?=date('d-m-Y');?>">
								</div>
							</div>
							</br>
							</br>
							</br>
														
							</form>					
						</div>										
					</div>
				</div>
			</div>
			<div class="col-xs-1">
			</div>
		</div>
		<div class="row">
		
			<div class="col-xs-12">
				<div class="panel panel-primary">
                    <div class="panel-heading">
                        Daftar Pegawai
                    </div>
					<div class="panel-body">
						<div class="row">
							<div class="col-xs-12">
								<button id="eksport_xls" type="button" class="btn btn-success">
									Export XLSX
								</button>
								<!--
								<button id="eksport_pdf" type="button" class="btn btn-success">
									Export PDF
								</button>
								-->
							</div>
						</div>
					<hr>
                        <div class="table-responsive">
							<table class="table table-striped table-bordered table-hover display" id="tabel">
							<thead>
							<tr>
								<th>No</th>
								<th>NIP / Nama </th>
								<th>SKPD</th>
								<th>Tanggal</th>
								<th>Besaran Tunjangan Awal</th>
								<th>Proporsi Kinerja </th>
								<th>Proporsi Kedisiplinan (Absensi)</th>
								<th>Persentase Pemotongan (%)</th>
								<th>Potongan Per Hari</th>
							</tr>
							</thead>
							<tbody>
							
							</tbody>
							</table>
						</div>
					
					</div>
				</div>
			</div>
		
		</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
 
  <div class="control-sidebar-bg"></div>
</div>
<form id="frm_ekspor_xls" name="frm_ekspor_xls" method="post" action="/laporan/ekspor_keuangan_harian_tkk_xls">
		<input type="hidden" name="sssubunit_xls" id="sssubunit_xls" value="0">
		<input type="hidden" name="jnsjab_xls" id="jnsjab_xls" value="0">
		<input type="hidden" name="keselon_xls" id="keselon_xls" value="0">
		<input type="hidden" name="nama_xls" id="nama_xls" value="">
		<input type="hidden" name="nik_xls" id="nik_xls" value="">
		<input type="hidden" name="tanggal_xls" id="tanggal_xls" value="<?=date('d-m-Y');?>">
</form>
<form id="frm_ekspor_pdf" name="frm_ekspor_pdf" method="post" action="/data/ekspor_tanggal_pdf" target="_blank">
		<input type="hidden" name="sssubunit_pdf" id="sssubunit_pdf" value="0">
		<input type="hidden" name="jnsjab_pdf" id="jnsjab_pdf" value="0">
		<input type="hidden" name="keselon_pdf" id="keselon_pdf" value="0">
		<input type="hidden" name="nama_pdf" id="nama_pdf" value="">
		<input type="hidden" name="nik_pdf" id="nik_pdf" value="">
		<input type="hidden" name="tanggal_pdf" id="tanggal_pdf" value="<?=date('d-m-Y');?>">
</form>
<!-- ./wrapper -->
<?php $this->load->view('javascript');?>
<link rel="stylesheet" href="/assets/css/buttons.dataTables.min.css">
<script src="/assets/js/jquery.chainedSelects.js"></script>
<script src="/assets/js/dataTables/jquery.dataTables.min.js"></script>
<script src="/assets/js/dataTables/dataTables.bootstrap.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
	
		$('#unit').chainSelect('#subunit','/data/combo_unit',
		{ 
			before:function (target) //before request hide the target combobox and display the loading message
			{ 
				$("#loading").css("display","block");
				$(target).css("display","none");
			},
			after:function (target) //after request show the target combobox and hide the loading message
			{ 
				$("#loading").css("display","none");
				$(target).css("display","inline");
			}
		});
		$('#subunit').chainSelect('#ssubunit','/data/combo_unit',
		{ 
			before:function (target) //before request hide the target combobox and display the loading message
			{ 
				$("#loading").css("display","block");
				$(target).css("display","none");
			},
			after:function (target) //after request show the target combobox and hide the loading message
			{ 
				$("#loading").css("display","none");
				$(target).css("display","inline");
			}
		});
		$('#ssubunit').chainSelect('#sssubunit','/data/combo_unit',
		{ 
			before:function (target) //before request hide the target combobox and display the loading message
			{ 
				$("#loading").css("display","block");
				$(target).css("display","none");
			},
			after:function (target) //after request show the target combobox and hide the loading message
			{ 
				$("#loading").css("display","none");
				$(target).css("display","inline");
			}
		});
	
		 //Date picker
		$('.datepicker').datepicker({
		  autoclose: true,
		  format:'dd-mm-yyyy'
		})
		
		var oTable = $("#tabel").dataTable({
			"oLanguage" : {
				"sSearch" : "Search",
				"oPaginate" : {
				"sFirst":"First",
				"sPrevious" : "Previous",
				"sNext" : "Next",
				"sLast":"Last"
			}
			}, 
			"ordering": false,
			"iDisplayLength" : 10,
			"bFilter" : false,               
			"bLengthChange": true,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": "/laporan/get_keuangan_harian_tkk",
				"type": "POST",
				"data": {
							'sssubunit':function(){return $("#sssubunit").val(); },
							'jnsjab': function(){return $("#jnsjab").val(); } ,
							'keselon':function(){return $("#keselon").val(); },
							'nama': function(){return $("#nama").val(); }, 
							'nik': function(){return $("#nik").val(); }, 
							'tanggal': function(){return $("#tanggal").val(); }, 
						}
			}
		});  
		
		$('#nama,#nik,#tanggal').on( 'keyup', function () {
			oTable.fnDraw();
		} );
		
		$('#sssubunit,#jnsjab,#keselon,#tanggal').on( 'change', function () {
			oTable.fnDraw();
		} );
		
		$('#eksport_xls').click(function (e) {
				$('#frm_ekspor_xls').submit();
		});
		$('#eksport_pdf').click(function (e) {
				$('#frm_ekspor_pdf').submit();
		});
		$('#sssubunit').on( 'change', function () {
			frm_ekspor_xls.sssubunit_xls.value = $('#sssubunit').val();
			frm_ekspor_pdf.sssubunit_pdf.value = $('#sssubunit').val();
		} );
		$('#jnsjab').on( 'change', function () {
			frm_ekspor_xls.jnsjab_xls.value = $('#jnsjab').val();
			frm_ekspor_pdf.jnsjab_pdf.value = $('#jnsjab').val();
		} );
		$('#keselon').on( 'change', function () {
			frm_ekspor_xls.keselon_xls.value = $('#keselon').val();
			frm_ekspor_pdf.keselon_pdf.value = $('#keselon').val();
		} );
		$('#nama').on( 'change', function () {
			frm_ekspor_xls.nama_xls.value = $('#nama').val();
			frm_ekspor_pdf.nama_pdf.value = $('#nama').val();
		} );
		$('#nik').on( 'change', function () {
			frm_ekspor_xls.nik_xls.value = $('#nik').val();
			frm_ekspor_pdf.nik_pdf.value = $('#nik').val();
		} );
		$('#tanggal').on( 'change', function () {
			frm_ekspor_xls.tanggal_xls.value = $('#tanggal').val();
			frm_ekspor_pdf.tanggal_pdf.value = $('#tanggal').val();
		} );
		
		});
		</script>
</body>
</html>
