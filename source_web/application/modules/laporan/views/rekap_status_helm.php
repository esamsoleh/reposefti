<?php $this->load->view('header');?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('top');?>
  <!-- Left side column. contains the logo and sidebar -->
  
  <?php $this->load->view('sidebar');?>
 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title;?>
        <small><?=$subtitle;?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-white">
					<div class="panel-body">
						<div class="row">
							<form name="formPegawai" id="formPegawai" method="post" action="/log/absensi" class="form-horizontal">
							<input type="hidden" name="cari" value="1">
							
							<div class="row">
									<label  class="col-md-2 control-label">Nama</label>
								<div class="col-md-4">
									<input class="form-control" type="text" name="nama" id="nama">
								</div>
									<label  class="col-md-2 control-label">NIP</label>
								<div class="col-md-4">
									<input class="form-control" type="text" name="reqnip" id="reqnip" >
								</div>
							</div>
							<div class="row">
									<label  class="col-md-2 control-label">Bulan</label>
								<div class="col-md-4">
									<select class="form-control" name="bulan" id="bulan">
												<option value="1" <?=($bulan==1?'selected':'');?>>Januari</option>
												<option value="2" <?=($bulan==2?'selected':'');?>>Februari</option>
												<option value="3" <?=($bulan==3?'selected':'');?>>Maret</option>
												<option value="4" <?=($bulan==4?'selected':'');?>>April</option>
												<option value="5" <?=($bulan==5?'selected':'');?>>Mei</option>
												<option value="6" <?=($bulan==6?'selected':'');?>>Juni</option>
												<option value="7" <?=($bulan==7?'selected':'');?>>Juli</option>
												<option value="8" <?=($bulan==8?'selected':'');?>>Agustus</option>
												<option value="9" <?=($bulan==9?'selected':'');?>>September</option>
												<option value="10" <?=($bulan==10?'selected':'');?>>Oktober</option>
												<option value="11" <?=($bulan==11?'selected':'');?>>Nopember</option>
												<option value="12" <?=($bulan==12?'selected':'');?>>Desember</option>
											</select>
								</div>
									<label  class="col-md-2 control-label">Tahun</label>
								<div class="col-md-4">
									<input class="form-control pull-right" id="tahun" type="text" name="tahun" value="<?=date('Y');?>">
								</div>
							</div>
							</br>
							</br>
							</form>					
						</div>										
					</div>
				</div>
			</div>
			<div class="col-xs-1">
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="panel panel-success">
                    <div class="panel-heading">
                        Daftar Pegawai
                    </div>
					<div class="panel-body">
					
                        <div class="table-responsive">
							<table id="tabel" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
							<thead>
							<tr>
							<th>No</th>
							<th>NIP</th>
							<th>Nama</th>
							<th>Jabatan</th>
							<th>Jumlah Tidak Pakai Helm</th>
							</tr>
							</thead>
							<tbody>
							
							</tbody>
							</table>
						</div>
					
					</div>
				</div>
			</div>
			
		</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
 
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('javascript');?>
<!-- DataTables -->
        <link href="/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/datatables/fixedHeader.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/datatables/scroller.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/datatables/dataTables.colVis.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/datatables/fixedColumns.dataTables.min.css" rel="stylesheet" type="text/css"/>
		<script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="/assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>

        <script src="/assets/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="/assets/plugins/datatables/buttons.bootstrap4.min.js"></script>

        <script src="/assets/plugins/datatables/jszip.min.js"></script>
        <script src="/assets/plugins/datatables/pdfmake.min.js"></script>
        <script src="/assets/plugins/datatables/vfs_fonts.js"></script>

        <script src="/assets/plugins/datatables/buttons.html5.min.js"></script>
        <script src="/assets/plugins/datatables/buttons.print.min.js"></script>

        <script src="/assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
        <script src="/assets/plugins/datatables/dataTables.keyTable.min.js"></script>
        <script src="/assets/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="/assets/plugins/datatables/responsive.bootstrap4.min.js"></script>
        <script src="/assets/plugins/datatables/dataTables.scroller.min.js"></script>
        <script src="/assets/plugins/datatables/dataTables.colVis.js"></script>
        <script src="/assets/plugins/datatables/dataTables.fixedColumns.min.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
		
		 //Date picker
		$('.datepicker').datepicker({
		  autoclose: true,
		  format:'dd-mm-yyyy'
		})
		
		var oTable = $("#tabel").dataTable({
			"oLanguage" : {
				"sSearch" : "Search",
				"oPaginate" : {
				"sFirst":"First",
				"sPrevious" : "Previous",
				"sNext" : "Next",
				"sLast":"Last"
			}
			}, 
			"ordering": false,
			"iDisplayLength" : 10,
			"bFilter" : false,               
			"bLengthChange": true,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": "/laporan/get_rekap_status_helm",
				"type": "POST",
				"data": {
							'nama': function(){return $("#nama").val(); }, 
							'reqnip': function(){return $("#reqnip").val(); }, 
							'bulan': function(){return $("#bulan").val(); }, 
							'tahun': function(){return $("#tahun").val(); }, 
						}
			}
		});  
		
		$('#nama,#reqnip,#tahun').on( 'keyup', function () {
			oTable.fnDraw();
		} );
		
		$('#bulan').on( 'change', function () {
			oTable.fnDraw();
		} );
		
		
		
		});
		</script>
</body>
</html>
