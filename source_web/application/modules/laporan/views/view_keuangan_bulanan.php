<?php $this->load->view('header');?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('top');?>
  <!-- Left side column. contains the logo and sidebar -->
  
  <?php $this->load->view('sidebar');?>
 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title;?>
        <small><?=$subtitle;?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">		
		<div class="row">
		<?php
		if(isset($rows))
		{	
			if($rows->num_rows() > 0)
			{
			?>
			<div class="col-xs-12">
				<div class="panel panel-success">
                    <div class="panel-heading">
                        Data Absensi Pegawai 
                    </div>
					<div class="panel-body">
						<div class="row">
								<label  class="col-sm-2 control-label">NIP</label>
								<div class="col-xs-4">
									<?=$nip;?>
								</div>
						</div>
						<div class="row">
								<label  class="col-sm-2 control-label">Nama</label>
								<div class="col-xs-4">
									<?=$data['nama'];?>
								</div>
						</div>
						<div class="row">
								<label  class="col-sm-2 control-label">Golongan</label>
								<div class="col-xs-4">
									<?=$data['golongan'];?>
								</div>
						</div>
						<div class="row">
								<label  class="col-sm-2 control-label">Jabatan</label>
								<div class="col-xs-10">
									<?=$data['jabatan'];?>
								</div>
						</div>
						<div class="row">
								<label  class="col-sm-2 control-label">Eselon</label>
								<div class="col-xs-4">
									<?=$data['eselon'];?>
								</div>
						</div>
						<div class="row">
								<label  class="col-sm-2 control-label">Periode</label>
								<div class="col-xs-4">
									<?=$nama_bulan[$bulan];?>-<?=$tahun;?>
								</div>
						</div>
						
                        <div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
							<thead>
							<tr>
								<th>No</th>
								<th>Tanggal</th>
								<th>Besaran Tunjangan (Rp.)</th>
								<th>Proporsi Kinerja (Rp.)</th>
								<th>Proporsi Kedisiplinan (Absensi) (Rp.)</th>
								<th>Persentase Pemotongan (%)</th>
								<th>Total Pemotongan  (Rp.)</th>
							</tr>
							</thead>
							<tbody>
							<?php
							$i= 1;
							$total_pemotongan = 0;
							$total_pemotongan_tpp = 0;
							foreach($rows->result_array() as $row)
							{
								echo '<tr>';
								echo '<td>'.$i.'</td>';
								echo '<td>'.date('d-M-Y',strtotime($row['tanggal'])).'</td>';
								echo '<td style="text-align:right">'.number_format($row['total_tpp'],2,',','.').'</td>';
								echo '<td style="text-align:right">'.number_format($row['tpp_statis'],2,',','.').'</td>';
								echo '<td style="text-align:right">'.number_format($row['tpp_dinamis'],2,',','.').'</td>';
								echo '<td style="text-align:right">'.$row['pemotongan_per_hari'].'</td>';
								echo '<td style="text-align:right">'.number_format($row['tpp_dinamis']*($row['pemotongan_per_hari']/100),2,',','.').'</td>';
								echo '</tr>';
								$i++;
								$total_pemotongan = $total_pemotongan + $row['pemotongan_per_hari'];
								$total_pemotongan_tpp = $total_pemotongan_tpp + ($row['tpp_dinamis']*($row['pemotongan_per_hari']/100));
							}
							?>
							<tr>
								<td colspan="5"  style="text-align:right">Total Persentase Pemotongan (%)</td>
								<td  style="text-align:right"><?=$total_pemotongan;?></td>
							</tr>
							<tr>
								<td colspan="6"  style="text-align:right">Total Pemotongan (Rp)</td>
								<td  style="text-align:right"><?=number_format($total_pemotongan_tpp,2,',','.');?></td>
							</tr>
							<tr>
								<td colspan="6"  style="text-align:right">Total Tunjangan Diterima Sebelum Pajak</td>
								<td  style="text-align:right"><?=number_format(($row['tpp_statis']+$row['tpp_dinamis']-$total_pemotongan_tpp),2,',','.');?></td>
							</tr>
							</tbody>
							</table>
						</div>
					
					</div>
				</div>
			</div>
			<?php
			}
			else
			{
				?>
				<div class="col-xs-12">
				<div class="alert alert-success alert-dismissable">
					<strong>Data yang dicari tidak ada!</strong>				
				</div>
				</div>
				<?php
			}
		}
		?>
		</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
 
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('javascript');?>
<script src="/assets/js/jquery.chainedSelects.js"></script>
<script src="/assets/js/dataTables/jquery.dataTables.js"></script>
<script src="/assets/js/dataTables/dataTables.bootstrap.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
		
		$('#tabel').dataTable({
					"bFilter": false, 
					"bInfo": true,
					"bLengthChange": false,
		});
		
		});
		</script>
</body>
</html>
