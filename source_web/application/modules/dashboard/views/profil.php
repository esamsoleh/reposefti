<?php $this->load->view('header');?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('top');?>
  <!-- Left side column. contains the logo and sidebar -->
  
  <?php $this->load->view('sidebar');?>
 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title;?>
        <small><?=$subtitle;?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
							<div class="col-md-12">
								<div class="panel panel-white">
								
									<div class="panel-body">
										<?php echo validation_errors(); ?>
										<?php //echo $error; ?>
										<form name="frm" id="frm" method="post" action="<?=site_url().'/'.$this->uri->uri_string();?>" enctype="multipart/form-data">
										
										<span id="edit_form" class="glyphicon glyphicon-edit btn btn-warning" aria-hidden="true">EDIT</span>
										<span id="batal_edit" class="glyphicon glyphicon-edit btn btn-info" aria-hidden="true"> BATAL</span>
										<span id="simpan_form" class="glyphicon glyphicon-floppy-disk btn btn-success" aria-hidden="true"> SIMPAN</span>
										<hr>
										<div class="row">
											<div class="form-group">
												<div class="col-xs-4"></div>
												<div class="col-xs-4">
													<label>
														Foto
													</label>
													<div class="fileupload fileupload-new" data-provides="fileupload">
														<?php
														if(isset($foto) && $foto != '')
														{
															$src_img = "/assets/foto/".$foto;
														}
														else
														{
															$src_img = "/assets/foto/no-image.png";
														}
														?>
														<div class="fileupload-new thumbnail"><img src="<?=$src_img;?>" alt=""/>
														</div>
														<div class="fileupload-preview fileupload-exists thumbnail"></div>
														<div>
															<span class="btn btn-light-grey btn-file">
															<span class="fileupload-new"><i class="fa fa-picture-o"></i> Select image</span>
															<span class="fileupload-exists"><i class="fa fa-picture-o"></i> Change</span>
															<input type="file" name="userfile" id="userfile" disabled>
															</span>
															<a href="#" class="btn fileupload-exists btn-light-grey" data-dismiss="fileupload">
																<i class="fa fa-times"></i> Remove
															</a>
														</div>
													</div>
												</div>
												<div class="col-xs-4"></div>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-4"><label>Username</label></div>
											<div class="col-xs-8">
											<?php
												echo form_input($username);
											?>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-4"><label>Nama Depan</label></div>
											<div class="col-xs-8">
												<?php echo form_input($first_name);?>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-4"><label>Nama Belakang</label></div>
											<div class="col-xs-8">
												 <?php echo form_input($last_name);?>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-4"><label>Email</label></div>
											<div class="col-xs-8">
											<?php
												echo form_input($email);
											?>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-4"><label>No Telepon</label></div>
											<div class="col-xs-8">
											<?php 
												echo form_input($phone);
											?>
											</div>
										</div>
										
										
									</div>
								</div>
							</div>
						<div class="col-xs-1">
						</div>
						</div>
						

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
 
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('javascript');?>
<script src="<?=base_url();?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<script type="text/javascript">
		$(document).ready(function () {
			required = ["first_name"];
			errornotice = $("#error");
			emptyerror = "Mohon field ini diisi.";
			$('#simpan_form').click(function(){
				//Validate required fields
				for (i=0;i<required.length;i++) {
					var input = $('#'+required[i]);
					if ((input.val() == "") || (input.val() == emptyerror)) {
						input.addClass("needsfilled");
						input.val(emptyerror);
						errornotice.fadeIn(750);
					} else {
						input.removeClass("needsfilled");
					}
				}
					
				//if any inputs on the page have the class 'needsfilled' the form will not submit
				if ($(":input").hasClass("needsfilled")) {
					return false;
				} else {
					errornotice.hide();
					$('#frm').submit();
				}
												  
			});
											
			$(":input").focus(function(){		
			   if ($(this).hasClass("needsfilled") ) {
					$(this).val("");
					$(this).removeClass("needsfilled");
				}
			});
			
			$('#batal').click(function(){
				parent.history.back();
				return false;
			});
			
			$("#batal_edit").hide();
			$("#simpan_form").hide();
			$('#edit_form').click(function (e) {
				$("#edit_form").hide();
				$("#batal_edit").show();
				$("#simpan_form").show();
				$("#first_name").attr("disabled", false);
				$("#last_name").attr("disabled", false);
				$("#email").attr("disabled", false);
				$("#phone").attr("disabled", false);
				$("#userfile").attr("disabled", false);
			});
			
			$('#batal_edit').click(function (e) {
				document.frm.reset();
				$("#edit_form").show();
				$("#batal_edit").hide();
				$("#simpan_form").hide();
				$("#first_name").attr("disabled", true);
				$("#last_name").attr("disabled", true);
				$("#email").attr("disabled", true);
				$("#phone").attr("disabled", true);
				$("#userfile").attr("disabled", true);
				});
	
		});
		</script>
</body>
</html>
