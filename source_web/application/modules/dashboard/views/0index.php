<?php $this->load->view('header');?>
 
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('top');?>
  <!-- Left side column. contains the logo and sidebar -->
  
  <?php $this->load->view('sidebar');?>
 
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

     <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
		<div class="col-md-6">
          <!-- BAR CHART -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <i class="fa fa-bar-chart-o"></i>

              <h3 class="box-title">Project <?=$proyek[$n]['nama'];?></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
							<div class="row">
									<label  class="col-sm-2 control-label">Proyek</label>
								<div class="col-xs-10">
									<select class="form-control" name="id_place" id="id_place" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
										<?php
										foreach($proyek as $proy)
										{
											echo '<option value="/dashboard/index/'.$proy['id_place'].'" '.(($n==$proy['id_place'])?'selected':'').'>'.$proy['nama'].'</option>';
										}
										?>
									</select>
								</div>
							</div>
							</br>
               <div id="peta"></div>
            </div>
            <!-- /.box-body-->
          </div>
          <!-- /.box -->
        </div>
		
		<div class="col-md-6">
          <!-- BAR CHART -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <i class="fa fa-bar-chart-o"></i>

              <h3 class="box-title">Project <?=$proyek[$n]['nama'];?></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
							<div class="row">
									<label  class="col-sm-2 control-label">Lantai</label>
								<div class="col-xs-10">
									<select class="form-control" name="id_place" id="id_place" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
										<?php
										foreach($floor as $flo)
										{
											echo '<option value="/dashboard/index/'.$proy['id_place'].'/'.$flo['id_floor'].'" '.(($nf==$flo['id_floor'])?'selected':'').'>'.$flo['nama'].'</option>';
										}
										?>
									</select>
								</div>
							</div>
							</br>
               <div id="peta_floor"></div>
            </div>
            <!-- /.box-body-->
          </div>
          <!-- /.box -->
        </div>
		</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
 
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('javascript');?>

<script type="text/javascript">
	$(document).ready(function () {
		
		
		});
		</script>
<style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #peta {
        height: 400px;
      }
	  #peta_floor {
        height: 400px;
      }
    </style>
<script>
      var map;
	  var mapFloor;
      function initMap() {
        map = new google.maps.Map(document.getElementById('peta'), {
          center: {lat: <?=$proyek[$n]['lat'];?>, lng: <?=$proyek[$n]['lng'];?>},
          zoom: 20,
		  mapTypeId:google.maps.MapTypeId.ROADMAP
        });
		
		var marker = new google.maps.Marker({
          position: {lat: <?=$proyek[$n]['lat'];?>, lng: <?=$proyek[$n]['lng'];?>},
          map: map,
          title: '<?=$proyek[$n]['nama'];?>'
        });
	
		
	  
	   mapFloor = new google.maps.Map(document.getElementById('peta_floor'), {
          center: {lat: <?=$proyek[$n]['lat'];?>, lng: <?=$proyek[$n]['lng'];?>},
          zoom: 30,
		  mapTypeId:google.maps.MapTypeId.ROADMAP
        });
		
		var marker = new google.maps.Marker({
          position: {lat: <?=$proyek[$n]['lat'];?>, lng: <?=$proyek[$n]['lng'];?>},
          map: mapFloor,
          title: '<?=$proyek[$n]['nama'];?>'
        });
		
		<?php
		
		if(isset($floor[$nf]['anchors']))
		{
			$floorCoordinate = json_decode($floor[$nf]['anchors'],TRUE);
			?>
			 var floorPlanCoordinates = [
			<?php
			foreach($floorCoordinate as $floorco)
			{
			?>
			  {lat: <?=$floorco['lat'];?>, lng: <?=$floorco['lng'];?>},
			<?php
			}
			?>
			];
			
			var floorPath = new google.maps.Polygon({
			  path: floorPlanCoordinates,
			  geodesic: true,
			  strokeColor: '#FF0000',
			  strokeOpacity: 1.0,
			  strokeWeight: 2
			});

			floorPath.setMap(mapFloor);
		<?php
		}
		?>
		
		<?php
		if(isset($lokasi_input))
		{
			foreach($lokasi_input as $data_lok)
			{
			?>
			  var marker = new google.maps.Marker({
				  position: {lat: <?=$data_lok['lat'];?>, lng: <?=$data_lok['lng'];?>},
				  map: mapFloor,
				  title: '<?=$data_lok['name'];?>',				  
				  icon: {
						  url: "http://maps.google.com/mapfiles/ms/icons/blue-dot.png"
					},
				});
			<?php
			}
			
		}
		?>
		
		<?php
		if(isset($posisi_orang) && $posisi_orang != '' && $posisi_orang != NULL )
		{
			foreach($posisi_orang as $pos)
			{
				?>
				var marker = new google.maps.Marker({
				  position: {lat: <?=$pos['lat'];?>, lng: <?=$pos['lng'];?>},
				  map: mapFloor,
				  title: '<?=$pos['id_aktivitas_visitor'];?>'
				});
				<?php
			}
		}
		?>
	  }
    </script>
    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDbrhUR7OJhCFsG2AXCQdzkP8gnR0E9ZvM&callback=initMap"></script>

</body>
</html>
