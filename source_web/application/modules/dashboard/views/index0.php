<?php $this->load->view('header');?>
 
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('top');?>
  <!-- Left side column. contains the logo and sidebar -->
  
  <?php $this->load->view('sidebar');?>
 
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

     <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
		<div class="col-md-12">
          <!-- BAR CHART -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <i class="fa fa-bar-chart-o"></i>

              <h3 class="box-title">Project <?=$proyek[$n]['nama'];?></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
							<div class="row">
									<label  class="col-sm-2 control-label">Proyek</label>
								<div class="col-xs-10">
									<select class="form-control" name="id_place" id="id_place" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
										<?php
										foreach($proyek as $proy)
										{
											echo '<option value="/dashboard/index/'.$proy['id_place'].'" '.(($n==$proy['id_place'])?'selected':'').'>'.$proy['nama'].'</option>';
										}
										?>
									</select>
								</div>
							</div>
							</br>
               <div id="peta"></div>
            </div>
            <!-- /.box-body-->
          </div>
          <!-- /.box -->
        </div>
		
		<div class="col-md-12">
          <!-- BAR CHART -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <i class="fa fa-bar-chart-o"></i>

              <h3 class="box-title">Project <?=$proyek[$n]['nama'];?></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
							<div class="row">
									<label  class="col-sm-2 control-label">Lantai</label>
								<div class="col-xs-10">
									<select class="form-control" name="id_place" id="id_place" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
										<?php
										foreach($floor as $flo)
										{
											echo '<option value="/dashboard/index/'.$proy['id_place'].'/'.$flo['id_floor'].'" '.(($nf==$flo['id_floor'])?'selected':'').'>'.$flo['nama'].'</option>';
										}
										?>
									</select>
								</div>
							</div>
							</br>
               <div id="peta_floor"></div>
            </div>
            <!-- /.box-body-->
          </div>
          <!-- /.box -->
        </div>
		</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
 
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('javascript');?>
<script type="text/javascript">
	$(document).ready(function () {
		
		setInterval( function () {
			location.reload();
		}, 3000 );
		
		});
		</script>
<style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #peta {
        height: 400px;
      }
	  #peta_floor {
        height: 400px;
      }
    </style>
	<script>
	//map pertama
	var mymap = L.map('peta').setView([<?=$proyek[$n]['lat'];?>, <?=$proyek[$n]['lng'];?>], 18);
	L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
		attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		maxZoom: 18,
		id: 'mapbox.streets',
		accessToken: 'pk.eyJ1Ijoic2Ftc29sZWgiLCJhIjoiY2p2a2V1cTU2MG5jdzN5bWw2MDBrODByaSJ9.eLmd2Zo6T_9pt3QJoFDXdw'
	}).addTo(mymap);
	var marker = L.marker([<?=$proyek[$n]['lat'];?>, <?=$proyek[$n]['lng'];?>]).addTo(mymap);
	
	//map kedua
	var floorMap = L.map('peta_floor').setView([<?=$proyek[$n]['lat'];?>, <?=$proyek[$n]['lng'];?>], 18);
	L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
		attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		maxZoom: 30,
		id: 'mapbox.streets',
		accessToken: 'pk.eyJ1Ijoic2Ftc29sZWgiLCJhIjoiY2p2a2V1cTU2MG5jdzN5bWw2MDBrODByaSJ9.eLmd2Zo6T_9pt3QJoFDXdw'
	}).addTo(floorMap);
		
	var polygon = L.polygon([
	<?php		
		if(isset($floor[$nf]['anchors']))
		{
			$floorCoordinate = json_decode($floor[$nf]['anchors'],TRUE);
			
			foreach($floorCoordinate as $floorco)
			{
			?>
			  [<?=$floorco['lat'];?>, <?=$floorco['lng'];?>],
			<?php
			}
		}
			?>
	]).addTo(floorMap);
	
	<?php
		if(isset($lokasi_input))
		{
			foreach($lokasi_input as $data_lok)
			{
			?>
			var marker = L.marker([<?=$data_lok['lat'];?>, <?=$data_lok['lng'];?>]).addTo(floorMap);
			<?php
			}
		}
	?>
	<?php
		if(isset($posisi_orang) && $posisi_orang != '' && $posisi_orang != NULL )
		{
			foreach($posisi_orang as $pos)
			{
				?>
				var myIcon = L.icon({
					iconUrl: 'my-icon.png',
					iconSize: [38, 95],
					iconAnchor: [22, 94],
					popupAnchor: [-3, -76],
					shadowUrl: 'my-icon-shadow.png',
					shadowSize: [68, 95],
					shadowAnchor: [22, 94]
				});
				var marker = L.marker([<?=$pos['lat'];?>, <?=$pos['lng'];?>],{title: <?=$pos['nip'];?>}).addTo(floorMap);
				<?php
			}
		}
		?>
	</script>

</body>
</html>
