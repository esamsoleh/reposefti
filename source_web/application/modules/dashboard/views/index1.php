<?php $this->load->view('header');?>
 
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('top');?>
  <!-- Left side column. contains the logo and sidebar -->
  
  <?php $this->load->view('sidebar');?>
 
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

     <!-- Main content -->
    <section class="content" id="content-section">
      <!-- Small boxes (Stat box) -->
      <div class="row">
		<div class="col-md-12">
          <!-- BAR CHART -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <i class="fa fa-bar-chart-o"></i>

              <h3 class="box-title">Project <?=$proyek[$n]['nama'];?></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
							<div class="row">
									<label  class="col-sm-2 control-label">Proyek</label>
								<div class="col-xs-4">
									<select class="form-control" name="id_place" id="id_place" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
										<?php
										foreach($proyek as $proy)
										{
											echo '<option value="/dashboard/index/'.$proy['id_place'].'" '.(($n==$proy['id_place'])?'selected':'').'>'.$proy['nama'].'</option>';
										}
										?>
									</select>
								</div>
									<label  class="col-sm-2 control-label">Lantai</label>
								<div class="col-xs-4">
									<select class="form-control" name="id_place" id="id_place" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
										<?php
										foreach($floor as $flo)
										{
											echo '<option value="/dashboard/index/'.$proy['id_place'].'/'.$flo['id_floor'].'" '.(($nf==$flo['id_floor'])?'selected':'').'>'.$flo['nama'].'</option>';
										}
										?>
									</select>
								</div>
							</div>
							</br>
				<div id="peta-section">
					<div id="peta"></div>
				</div>
            </div>
            <!-- /.box-body-->
          </div>
          <!-- /.box -->
        </div>
		
		
		</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
 
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('javascript');?>
<script type="text/javascript">
	$(document).ready(function () {
		
		setInterval(loadContent, <?=$interval_refresh_dashboard;?>000);
			function loadContent() {
			//	$("#content-section").empty();
				$("#peta-section").empty();
					$.ajax({
						url:'/dashboard/section',
						method:'post',
						data: {
							'id_place': '<?=$n;?>', 
							'id_floor': '<?=$nf;?>', 
						}
						}).done(function(html) {
						//	$("#content-section").append(html);
							$("#peta-section").append(html);
					});
									
			}
								
		});
		</script>
<style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #peta {
        height: 400px;
      }
	  #peta_floor {
        height: 400px;
      }
    </style>
	<?php		
		if(isset($floor[$nf]['anchors']))
		{
			$lat = 0;
			$lng = 0;
			$i=0;
			$floorCoordinate = json_decode($floor[$nf]['anchors'],TRUE);
			
			foreach($floorCoordinate as $floorco)
			{
				$lat = $lat+$floorco['lat'];
				$lng = $lng+$floorco['lng'];
				$i++;
			}
			$lat_baru = $lat/$i;
			$lng_baru = $lng/$i;
		}
	?>
	<script>
	//map pertama
	var mymap = L.map('peta').setView([<?=$lat_baru;?>, <?=$lng_baru;?>], 20);
	L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
		attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		maxZoom: 30,
		id: 'mapbox.streets',
		accessToken: 'pk.eyJ1Ijoic2Ftc29sZWgiLCJhIjoiY2p2a2V1cTU2MG5jdzN5bWw2MDBrODByaSJ9.eLmd2Zo6T_9pt3QJoFDXdw'
	}).addTo(mymap);	
	
	//var polygon = L.polygon([
	<?php		
	/* if(isset($floor[$nf]['anchors']))
		{
			$floorCoordinate = json_decode($floor[$nf]['anchors'],TRUE);
			$i=1;
			foreach($floorCoordinate as $floorco)
			{
			?>
			  [<?=$floorco['lat'];?>, <?=$floorco['lng'];?>],
			<?php
				$koorlat[$i] = $floorco['lat'];
				$koorlng[$i] = $floorco['lng'];
				$i++;
			}
			
		} */
	?>
	//]).addTo(mymap);
	
	<?php
		if(isset($lokasi_input))
		{
			foreach($lokasi_input as $data_lok)
			{
			?>
				var marker = L.marker([<?=$data_lok['lat'];?>, <?=$data_lok['lng'];?>]).addTo(mymap);
			<?php
			}
		}
	?>
	<?php
		if(isset($posisi_orang) && $posisi_orang != '' && $posisi_orang != NULL )
		{
			$npos = 1;
			foreach($posisi_orang as $pos)
			{
				?>
				var myIcon_<?=$npos;?> = L.icon({
					iconUrl: '<?=base_url();?>/assets/ikon/<?=$pos['ikon'];?>',
				//	shadowUrl: 'my-icon-shadow.png',
					iconSize: [20, 20],
					shadowSize: [68, 95],
					iconAnchor: [22, 94],
					shadowAnchor: [22, 94],
					popupAnchor: [-3, -76],
				});
				var marker_<?=$npos;?> = L.marker([<?=$pos['lat'];?>, <?=$pos['lng'];?>],{icon: myIcon_<?=$npos;?>}).addTo(mymap).bindPopup("<?=$pos['nip'];?> - <?=$pos['nama'];?>");
				<?php
				$npos++;
			}
		}
		?>
		
		<?php		
		if(isset($geofence[$n]['area']))
		{
			$areaCoordinate = json_decode($geofence[$n]['area'],TRUE);
			?>
			var circle = L.circle([<?=$areaCoordinate['lat'];?>, <?=$areaCoordinate['lng'];?>], {
				color: 'red',
				fillColor: '#f03',
				fillOpacity: 0.5,
				radius: <?=$geofence[$n]['radius'];?>
			}).addTo(mymap);
			<?php
		}
		?>
	<?php
		$denah = base_url().'/denah/'.$floor[$nf]['file_gambar'];
	?>
	/* var	bounds = new L.LatLngBounds(
		new L.LatLng(<?=$koorlat[1];?>,<?=$koorlng[1];?>),
		new L.LatLng(<?=$koorlat[$i-1];?>,<?=$koorlng[$i-1];?>),
	); */
//	mymap.fitBounds(bounds);
	//var overlay = new L.ImageOverlay("https://upload.wikimedia.org/wikipedia/commons/thumb/d/d7/Rectangle_Geometry_Vector.svg/1200px-Rectangle_Geometry_Vector.svg.png", bounds, {
	/* var overlay = new L.ImageOverlay("<?=$denah;?>", bounds, {
			opacity: 0.5,
			interactive: true,
			attribution: '&copy; A.B.B Corp.'
	});
	mymap.addLayer(overlay); */
	
	
	var geojsonLayer = new L.GeoJSON.AJAX("<?=$denah;?>");       
	geojsonLayer.addTo(mymap);
	</script>	
		

</body>
</html>
