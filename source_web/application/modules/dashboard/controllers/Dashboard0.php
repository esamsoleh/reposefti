<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public $apps;
	var $nama_user;
	var $file_foto;
	var $s_biro;
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
		if (!$this->ion_auth->logged_in())
			{
				redirect('/auth/login', 'refresh');
			}
		$this->apps = 'dashboard';
		$this->s_biro = $this->session->userdata('s_biro');
		$this->nama_user = $this->ion_auth->user()->row()->first_name.' '.$this->ion_auth->user()->row()->last_name;
		$this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	}
	
	public function index()
	{
		$j=$this->uri->segment(3);
		$fields = array(
			'email' => 'dena05meidina@gmail.com',
			'password' => 'awaspanas',
		);
		$fields_string = '';
		foreach($fields as $key=>$value) 
		{ 
			$fields_string .= $key.'='.$value.'&'; 
		}
		rtrim($fields_string, '&');
		
		//login
		$ch = curl_init();
		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL, 'https://api.proximi.fi/core_auth/login');
		curl_setopt($ch,CURLOPT_POST, count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, TRUE);

		//execute post
		$result = curl_exec($ch);
		curl_close($ch);
		
		$result = json_decode($result,TRUE);
		$token = $result['token'];
		
		//cek place
		$ch = curl_init();
		$authorization = "Authorization: Bearer ".$token;
		curl_setopt($ch,CURLOPT_URL, 'https://api.proximi.fi/core/places');
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, TRUE);  // Prepare the authorisation token
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization )); // Inject the token into the header
       
		$result = curl_exec($ch);
		$result = json_decode($result,TRUE);
		$n=((isset($j) && $j!= '')?$j:0);
		$i = 0;
		foreach($result as $res)
		{
			$proyek[$i]['i'] = $i;
			$proyek[$i]['id'] = $res['id'];
			$proyek[$i]['name'] = $res['name'];
			$proyek[$i]['location'] = $res['location'];
			$i++;
		}
		//print_r($proyek);
		//close connection
		curl_close($ch);
		
		//cek input
		$ch = curl_init();
		$authorization = "Authorization: Bearer ".$token;
		curl_setopt($ch,CURLOPT_URL, 'https://api.proximi.fi/core/inputs');
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, TRUE);  // Prepare the authorisation token
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization )); // Inject the token into the header
       
		$result = curl_exec($ch);
		$result = json_decode($result,TRUE);
		$ip = 0;
		foreach($result as $res)
		{
			$lokasi_input[$res['place_id']][$ip]['marker'] = $res['data']['marker'];
			$ip++;
		}
		//print_r($lokasi_input);
		
		//close connection
		curl_close($ch);
		//print_r($lok_input);
		
		//cek visitor
		$ch = curl_init();
		$authorization = "Authorization: Bearer ".$token;
		curl_setopt($ch,CURLOPT_URL, 'https://api.proximi.fi/core/visitors');
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, TRUE);  // Prepare the authorisation token
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization )); // Inject the token into the header
       
		$result = curl_exec($ch);
		$result = json_decode($result,TRUE);
		
		foreach($result as $res)
		{
			if($res['id'] != '')
			{
				$visitors[] = $res['id'];
			}
		}
		//close connection
		curl_close($ch);
		
		$posisi_orang = array();
		$np = 0;
		foreach($visitors as $vis)
		{
			//cek visitor
			$ch = curl_init();
			$authorization = "Authorization: Bearer ".$token;
			curl_setopt($ch,CURLOPT_URL, 'https://api.proximi.fi/analytics/visitor/'.$vis);
			curl_setopt($ch,CURLOPT_RETURNTRANSFER, TRUE);  // Prepare the authorisation token
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization )); // Inject the token into the header
		   
			$result = curl_exec($ch);
			$result = json_decode($result,TRUE);
			if(isset($result['lastPosition']) && $result['lastPosition'] != '' && $result['lastPosition'] != NULL)
			{
				$posisi_orang[$np] = $result['lastPosition'];
			}
			
			//close connection
			curl_close($ch);
			$np++;
		}
		
		$data = array(
			'title' => 'Dashboard',
			'proyek' => $proyek,
			'n' => $n,
			'lokasi_input' => $lokasi_input,
			'posisi_orang' => $posisi_orang,
		);
		$this->load->view('index',$data);
	}
	
	public function profil()
	{
		$r = $this->ion_auth->user()->row();
		$this->load->library('form_validation');
		$this->form_validation->set_rules('first_name', 'Nama Depan', 'trim|required');
		$this->form_validation->set_rules('last_name', 'Nama Belakang', 'trim|required');
		$this->form_validation->set_rules('phone', 'Telepon', 'trim');
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		
		if ($this->form_validation->run() === TRUE)
		{
			$posts = $this->input->post();
			print_r($posts);
			echo '</br>';
			$first_name = $this->db->escape_str($posts['first_name']);
			$last_name = $this->db->escape_str($posts['last_name']);
			$phone = $this->db->escape_str($posts['phone']);
			$email = $this->db->escape_str($posts['email']);
			
			//upload foto
			$config['upload_path'] = 'assets/foto/';
			$config['allowed_types'] = 'gif|jpg|png';
			$this->load->library('upload', $config);
			$field_name = 'userfile';
			if ($this->upload->do_upload($field_name))
			{
				$data = $this->upload->data();
				$foto = $data['file_name'];
			}
			else
			{
				$foto = $r->foto;
				$this->session->set_flashdata('message', $this->upload->display_errors());	
			}	
				$sql = 	"update t_users 
						SET first_name = '$first_name', 
							last_name = '$last_name',
							phone = '$phone',
							email = '$email',
							foto = '$foto'
						WHERE id = '$r->id'
						";
				echo $sql.'</br>';
				$this->db->query($sql);	
				
			redirect('/dashboard/profil');
		}
		else
		{
			$this->data['first_name'] = array(
                'name'  => 'first_name',
                'id'    => 'first_name',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('first_name',$r->first_name),
                'class'  => 'form-control',
				'disabled' => '',
            );
            $this->data['last_name'] = array(
                'name'  => 'last_name',
                'id'    => 'last_name',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('last_name',$r->last_name),
                'class'  => 'form-control',
				'disabled' => '',
            );
			
            $this->data['email'] = array(
                'name'  => 'email',
                'id'    => 'email',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('email',$r->email),
                'class'  => 'form-control',
				'disabled' => '',
            );
			
			 $this->data['username'] = array(
                'name'  => 'username',
                'id'    => 'username',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('username',$r->username),
                'class'  => 'form-control',
				'disabled' => '',
            );
			
            $this->data['phone'] = array(
                'name'  => 'phone',
                'id'    => 'phone',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('phone',$r->phone),
                'class'  => 'form-control',
				'disabled' => '',
            );
										
			$this->data['title'] = 'Profil';
			$this->data['subtitle'] = '';
			$this->data['foto'] = $r->foto;
			
			$this->load->view('profil',$this->data);
		}
		
	}
	
	public function ganti_password()
	{
		$user = $this->ion_auth->user()->row();
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
		$this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');
		
		if ($this->form_validation->run() === TRUE)
		{
			$posts = $this->input->post();
			$password = $this->db->escape_str($posts['password']);
			$data = array(
						'password' => $password,
						 );
			$this->ion_auth->update($user->id, $data);
			
			$this->session->set_flashdata('message', $this->ion_auth->messages() );			
			redirect('/dashboard/ganti_password');
		}
		else
		{
			 $this->data['email'] = array(
                'name'  => 'email',
                'id'    => 'email',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('email',$user->email),
                'class'  => 'form-control',
				'disabled' => '',
            );
			
			$this->data['password'] = array(
			'name' => 'password',
			'id'   => 'password',
			'type' => 'password',
			'class'  => 'form-control',
			);
			$this->data['password_confirm'] = array(
				'name' => 'password_confirm',
				'id'   => 'password_confirm',
				'type' => 'password',
				'class'  => 'form-control',
			);
							
			$this->data['title'] = 'Form Ganti Password';
			$this->data['subtitle'] = '';
			$this->data['message']='';
			
			$this->load->view('ganti_password',$this->data);
		}
		
	}
	
	public function combo_wilayah()
	{
		$array = array();
		if ($_GET['_name'] == 'kode_provinsi' OR $_GET['_name'] == 'KINSPROP' OR $_GET['_name'] == 'KPROP') 
		{
			$kode_provinsi = substr($_GET['_value'],0,2);
			
				$sql = "SELECT KWIL, 
							NWIL
					FROM ref_wilayah 
					WHERE MID(KWIL,1,2) = '$kode_provinsi' 
					AND		TWIL = '2'
					ORDER BY KWIL";
				$query = $this->db->query($sql);
				//$array[] = array('0' => '----------');
				foreach($query->result_array() as $row)
				{
					$array[] = array($row['KWIL'] => $row['NWIL']);
				}
		}
		elseif ($_GET['_name'] == 'kode_kabupaten' OR $_GET['_name'] == 'KINSKAB' OR $_GET['_name'] == 'KKAB') 
		{
			$kode_kabupaten = substr($_GET['_value'],0,4);
			$sql = "SELECT KWIL, 
							NWIL
					FROM ref_wilayah 
					WHERE MID(KWIL,1,4) = '$kode_kabupaten' 
					AND		TWIL = '3'
					ORDER BY KWIL";
				$query = $this->db->query($sql);
				//$array[] = array('0' => '----------');
				foreach($query->result_array() as $row)
				{
					$array[] = array($row['KWIL'] => $row['NWIL']);
				}
		} 
		elseif ($_GET['_name'] == 'kode_kecamatan' OR $_GET['_name'] == 'KINSKEC') 
		{
			$kode_kecamatan = substr($_GET['_value'],0,6);
			$sql = "SELECT KWIL, 
							NWIL
					FROM ref_wilayah 
					WHERE MID(KWIL,1,6) = '$kode_kecamatan' 
					AND		TWIL = '4'
					ORDER BY KWIL";
				$query = $this->db->query($sql);
			//	$array[] = array('0' => '----------');
				foreach($query->result_array() as $row)
				{
					$array[] = array($row['KWIL'] => $row['NWIL']);
				}
		} 
		
		echo json_encode( $array );
	}
	
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */