<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public $apps;
	var $nama_user;
	var $file_foto;
	var $s_biro;
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
		if (!$this->ion_auth->logged_in())
			{
				redirect('/auth/login', 'refresh');
			}
		$this->apps = 'dashboard';
		$this->s_biro = $this->session->userdata('s_biro');
		$this->nama_user = $this->ion_auth->user()->row()->first_name.' '.$this->ion_auth->user()->row()->last_name;
		$this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	}
	
	public function index()
	{
		$j=$this->uri->segment(3);	
		$f=$this->uri->segment(4);			
		$n=((isset($j) && $j!= '')?$j:($this->db->query('select id_place from d_project where updated_at = (select max(updated_at) from d_project) ')->row()->id_place));
		$scf = 'select id_floor from d_site where id_place = "'.$n.'" and updated_at = (select max(updated_at) from d_site where id_place = "'.$n.'" ) ';
	//	echo $scf;
		$qcf = $this->db->query($scf);
		if($qcf->num_rows() > 0)
		{
			$nf = $qcf->row()->id_floor;
		}
		else
		{
			$nf = 0;
		}
		$nf=((isset($f) && $f!= '')?$f:($nf));
		$qp = $this->db->get('d_project');
		foreach($qp->result() as $rp)
		{
			$proyek[$rp->id_place]['id_place']=$rp->id_place;
			$proyek[$rp->id_place]['nama']=$rp->nama;
			$proyek[$rp->id_place]['lat']=$rp->lat;
			$proyek[$rp->id_place]['lng']=$rp->lng;
		}
		
		$si = "select * from d_input where id_place = '$n' ";
		$qi = $this->db->query($si);
		$ip = 0;
		$lokasi_input = array();
		foreach($qi->result_array() as $res)
		{
			$lokasi_input[$ip]['lat'] = $res['lat'];
			$lokasi_input[$ip]['lng'] = $res['lng'];
			$lokasi_input[$ip]['name'] = $res['name'];
			$ip++;
		}
		$hari = date('d');
		$bulan = date('n');
		$tahun = date('Y');
		$si = "select a.update_date,
						a.nip,
						a.latitude,
						a.longitude,
						b.nama
			from d_status_helm a
			left join d_pegawai b on(a.nip = b.nip)
			where day(a.update_date) = '$hari' 
			and month(a.update_date) = '$bulan'
			and year(a.update_date) = '$tahun'
			";
		$qi = $this->db->query($si);
		$ip = 0;
		$posisi_orang = array();
		foreach($qi->result_array() as $res)
		{
			$posisi_orang[$ip]['lat'] = $res['latitude'];
			$posisi_orang[$ip]['lng'] = $res['longitude'];
			$posisi_orang[$ip]['nip'] = $res['nip'];
			$posisi_orang[$ip]['nama'] = $res['nama'];
			$ip++;
		}
		
		//floor
		$sf = "select * from d_site where id_place = '$n' ";
		$qf = $this->db->query($sf);
		$floor = array();
		foreach($qf->result() as $rf)
		{
			$floor[$rf->id_floor]['id_floor']=$rf->id_floor;
			$floor[$rf->id_floor]['nama']=$rf->nama;
			$floor[$rf->id_floor]['anchors']=$rf->anchors;
		}
		
		//geofence
		$sf = "select * from d_geofence where place_id = '$n' ";
		$qf = $this->db->query($sf);
		$geofence = array();
		foreach($qf->result() as $rf)
		{
			$geofence[$rf->place_id]['id_geofence']=$rf->id_geofence;
			$geofence[$rf->place_id]['name']=$rf->name;
			$geofence[$rf->place_id]['area']=$rf->area;
			$geofence[$rf->place_id]['radius']=$rf->radius;
		}
		$data = array(
			'title' => 'Dashboard',
			'proyek' => $proyek,
			'n' => $n,
			'nf' => $nf,
			'lokasi_input' => $lokasi_input,
			'posisi_orang' => $posisi_orang,
			'floor' => $floor,
			'geofence' => $geofence,
		);
		$this->load->view('index',$data);
	}
	
	public function profil()
	{
		$r = $this->ion_auth->user()->row();
		$this->load->library('form_validation');
		$this->form_validation->set_rules('first_name', 'Nama Depan', 'trim|required');
		$this->form_validation->set_rules('last_name', 'Nama Belakang', 'trim|required');
		$this->form_validation->set_rules('phone', 'Telepon', 'trim');
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		
		if ($this->form_validation->run() === TRUE)
		{
			$posts = $this->input->post();
			print_r($posts);
			echo '</br>';
			$first_name = $this->db->escape_str($posts['first_name']);
			$last_name = $this->db->escape_str($posts['last_name']);
			$phone = $this->db->escape_str($posts['phone']);
			$email = $this->db->escape_str($posts['email']);
			
			//upload foto
			$config['upload_path'] = 'assets/foto/';
			$config['allowed_types'] = 'gif|jpg|png';
			$this->load->library('upload', $config);
			$field_name = 'userfile';
			if ($this->upload->do_upload($field_name))
			{
				$data = $this->upload->data();
				$foto = $data['file_name'];
			}
			else
			{
				$foto = $r->foto;
				$this->session->set_flashdata('message', $this->upload->display_errors());	
			}	
				$sql = 	"update t_users 
						SET first_name = '$first_name', 
							last_name = '$last_name',
							phone = '$phone',
							email = '$email',
							foto = '$foto'
						WHERE id = '$r->id'
						";
				echo $sql.'</br>';
				$this->db->query($sql);	
				
			redirect('/dashboard/profil');
		}
		else
		{
			$this->data['first_name'] = array(
                'name'  => 'first_name',
                'id'    => 'first_name',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('first_name',$r->first_name),
                'class'  => 'form-control',
				'disabled' => '',
            );
            $this->data['last_name'] = array(
                'name'  => 'last_name',
                'id'    => 'last_name',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('last_name',$r->last_name),
                'class'  => 'form-control',
				'disabled' => '',
            );
			
            $this->data['email'] = array(
                'name'  => 'email',
                'id'    => 'email',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('email',$r->email),
                'class'  => 'form-control',
				'disabled' => '',
            );
			
			 $this->data['username'] = array(
                'name'  => 'username',
                'id'    => 'username',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('username',$r->username),
                'class'  => 'form-control',
				'disabled' => '',
            );
			
            $this->data['phone'] = array(
                'name'  => 'phone',
                'id'    => 'phone',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('phone',$r->phone),
                'class'  => 'form-control',
				'disabled' => '',
            );
										
			$this->data['title'] = 'Profil';
			$this->data['subtitle'] = '';
			$this->data['foto'] = $r->foto;
			
			$this->load->view('profil',$this->data);
		}
		
	}
	
	public function ganti_password()
	{
		$user = $this->ion_auth->user()->row();
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
		$this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');
		
		if ($this->form_validation->run() === TRUE)
		{
			$posts = $this->input->post();
			$password = $this->db->escape_str($posts['password']);
			$data = array(
						'password' => $password,
						 );
			$this->ion_auth->update($user->id, $data);
			
			$this->session->set_flashdata('message', $this->ion_auth->messages() );			
			redirect('/dashboard/ganti_password');
		}
		else
		{
			 $this->data['email'] = array(
                'name'  => 'email',
                'id'    => 'email',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('email',$user->email),
                'class'  => 'form-control',
				'disabled' => '',
            );
			
			$this->data['password'] = array(
			'name' => 'password',
			'id'   => 'password',
			'type' => 'password',
			'class'  => 'form-control',
			);
			$this->data['password_confirm'] = array(
				'name' => 'password_confirm',
				'id'   => 'password_confirm',
				'type' => 'password',
				'class'  => 'form-control',
			);
							
			$this->data['title'] = 'Form Ganti Password';
			$this->data['subtitle'] = '';
			$this->data['message']='';
			
			$this->load->view('ganti_password',$this->data);
		}
		
	}
	
	public function combo_wilayah()
	{
		$array = array();
		if ($_GET['_name'] == 'kode_provinsi' OR $_GET['_name'] == 'KINSPROP' OR $_GET['_name'] == 'KPROP') 
		{
			$kode_provinsi = substr($_GET['_value'],0,2);
			
				$sql = "SELECT KWIL, 
							NWIL
					FROM ref_wilayah 
					WHERE MID(KWIL,1,2) = '$kode_provinsi' 
					AND		TWIL = '2'
					ORDER BY KWIL";
				$query = $this->db->query($sql);
				//$array[] = array('0' => '----------');
				foreach($query->result_array() as $row)
				{
					$array[] = array($row['KWIL'] => $row['NWIL']);
				}
		}
		elseif ($_GET['_name'] == 'kode_kabupaten' OR $_GET['_name'] == 'KINSKAB' OR $_GET['_name'] == 'KKAB') 
		{
			$kode_kabupaten = substr($_GET['_value'],0,4);
			$sql = "SELECT KWIL, 
							NWIL
					FROM ref_wilayah 
					WHERE MID(KWIL,1,4) = '$kode_kabupaten' 
					AND		TWIL = '3'
					ORDER BY KWIL";
				$query = $this->db->query($sql);
				//$array[] = array('0' => '----------');
				foreach($query->result_array() as $row)
				{
					$array[] = array($row['KWIL'] => $row['NWIL']);
				}
		} 
		elseif ($_GET['_name'] == 'kode_kecamatan' OR $_GET['_name'] == 'KINSKEC') 
		{
			$kode_kecamatan = substr($_GET['_value'],0,6);
			$sql = "SELECT KWIL, 
							NWIL
					FROM ref_wilayah 
					WHERE MID(KWIL,1,6) = '$kode_kecamatan' 
					AND		TWIL = '4'
					ORDER BY KWIL";
				$query = $this->db->query($sql);
			//	$array[] = array('0' => '----------');
				foreach($query->result_array() as $row)
				{
					$array[] = array($row['KWIL'] => $row['NWIL']);
				}
		} 
		
		echo json_encode( $array );
	}
	
	function section()
	{
		$n = $this->input->post('id_place');
		$nf = $this->input->post('id_floor');
		$qp = $this->db->get('d_project');
		foreach($qp->result() as $rp)
		{
			$proyek[$rp->id_place]['id_place']=$rp->id_place;
			$proyek[$rp->id_place]['nama']=$rp->nama;
			$proyek[$rp->id_place]['lat']=$rp->lat;
			$proyek[$rp->id_place]['lng']=$rp->lng;
		}
		
		$si = "select * from d_input where id_place = '$n' ";
		$qi = $this->db->query($si);
		$ip = 0;
		$lokasi_input = array();
		foreach($qi->result_array() as $res)
		{
			$lokasi_input[$ip]['lat'] = $res['lat'];
			$lokasi_input[$ip]['lng'] = $res['lng'];
			$lokasi_input[$ip]['name'] = $res['name'];
			$ip++;
		}
		$hari = date('d');
		$bulan = date('n');
		$tahun = date('Y');
		$si = "select a.update_date,
						a.nip,
						a.latitude,
						a.longitude,
						b.nama
			from d_status_helm a
			left join d_pegawai b on(a.nip = b.nip)
			where day(a.update_date) = '$hari' 
			and month(a.update_date) = '$bulan'
			and year(a.update_date) = '$tahun'
			";
		$qi = $this->db->query($si);
		$ip = 0;
		$posisi_orang = array();
		foreach($qi->result_array() as $res)
		{
			$posisi_orang[$ip]['lat'] = $res['latitude'];
			$posisi_orang[$ip]['lng'] = $res['longitude'];
			$posisi_orang[$ip]['nip'] = $res['nip'];
			$posisi_orang[$ip]['nama'] = $res['nama'];
			$ip++;
		}
		
		//floor
		$sf = "select * from d_site where id_place = '$n' ";
		$qf = $this->db->query($sf);
		$floor = array();
		foreach($qf->result() as $rf)
		{
			$floor[$rf->id_floor]['id_floor']=$rf->id_floor;
			$floor[$rf->id_floor]['nama']=$rf->nama;
			$floor[$rf->id_floor]['anchors']=$rf->anchors;
		}
		
		//geofence
		$sf = "select * from d_geofence where place_id = '$n' ";
		$qf = $this->db->query($sf);
		$geofence = array();
		foreach($qf->result() as $rf)
		{
			$geofence[$rf->place_id]['id_geofence']=$rf->id_geofence;
			$geofence[$rf->place_id]['name']=$rf->name;
			$geofence[$rf->place_id]['area']=$rf->area;
			$geofence[$rf->place_id]['radius']=$rf->radius;
		}
		
		$res = '<div class="row">
		<div class="col-md-12">
          <!-- BAR CHART -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <i class="fa fa-bar-chart-o"></i>

              <h3 class="box-title">Project '.$proyek[$n]['nama'].'</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
							<div class="row">
									<label  class="col-sm-2 control-label">Proyek</label>
								<div class="col-xs-4">
									<select class="form-control" name="id_place" id="id_place" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">';
										
										foreach($proyek as $proy)
										{
											$res .= '<option value="/dashboard/index/'.$proy['id_place'].'" '.(($n==$proy['id_place'])?'selected':'').'>'.$proy['nama'].'</option>';
										}
										
			$res .='	</select>
								</div>
									<label  class="col-sm-2 control-label">Lantai</label>
								<div class="col-xs-4">
									<select class="form-control" name="id_place" id="id_place" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
										<?php
										foreach($floor as $flo)
										{
											echo '<option value="/dashboard/index/'.$proy['id_place'].'/'.$flo['id_floor'].'" '.(($nf==$flo['id_floor'])?'selected':'').'>'.$flo['nama'].'</option>';
										}
										?>
									</select>
								</div>
							</div>
							</br>
               <div id="peta"></div>
            </div>
            <!-- /.box-body-->
          </div>
          <!-- /.box -->
        </div>
		
		
		</div>';
		
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */