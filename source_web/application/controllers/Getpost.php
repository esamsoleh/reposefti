<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class Getpost extends REST_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	public function absen_post()
	{
		$serial_number = $this->post('serial_number');
		$nip = $this->post('nip');
		$tanggal = $this->post('tanggal');
		if(isset($serial_number) && $serial_number != '' && isset($nip) && $nip != '' && isset($tanggal) && $tanggal != '')
		{
			$array_insert = array(
				'serial_number' => $serial_number,
				'nip' => $nip,
				'tanggal' => $tanggal,
			);
			$this->db->insert('absensi_log',$array_insert);
			$insert_id = $this->db->insert_id();
			
			if(isset($insert_id) && $insert_id != NULL && $insert_id != '')
			{
				$this->response([
                    'status' => TRUE,
                    'message' => 'Data was inserted'
				], REST_Controller::HTTP_CREATED);
			}				
		}
		else
		{
			$this->response([
                    'status' => FALSE,
                    'message' => 'No data were inserted'
            ], REST_Controller::HTTP_NOT_FOUND);
		}
		
	}
	
	public function absen_get()
	{
		set_time_limit(0);
		$datas = array();
        $s = "select * from absensi_log ";
		$q = $this->db->query($s);
		foreach($q->result_array() as $row)
		{
			$datas[] = $row;
		}
        // Check if the users data store contains users (in case the database result returns NULL)
        if ($datas)
        {
            // Set the response and exit
            $this->response($datas, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
        else
        {
            // Set the response and exit
            $this->response([
                    'status' => FALSE,
                    'message' => 'No Data were found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
	}
	
	public function statusHelm_post()
	{
		date_default_timezone_set('Asia/Jakarta');
		$alamat_aplikasi = $this->db->query("select value as alamat_aplikasi from t_setting where id = 1 ")->row()->alamat_aplikasi;
		$nip = $this->post('nip');
		$lat = $this->post('lat');
		$long = $this->post('long');
		$status_helm = $this->post('status_helm');
		$floor = $this->post('floor');
		if(isset($nip) && $nip != '' && isset($status_helm) && $status_helm != '' && $lat != '' && $lat != 0 && $long != '' && $long != 0 )
		{
			$array_insert = array(
					'nip' => $nip,
					'latitude' => $lat,
					'longitude' => $long,
					'status_helm' => $status_helm,
					'floor' => $floor,
				);
			$sc = "select id,status_helm from d_status_helm where nip = '$nip' ";
			$qc = $this->db->query($sc);
			if($qc->num_rows() > 0)
			{
				$id=$qc->row()->id;
				
				$array_insert['update_date'] = date('Y-m-d H:i:s');
				$this->db->where('id',$id);
				$this->db->update('d_status_helm',$array_insert);
				$insert_id = $id;
				$status_helm_sebelum = $qc->row()->status_helm;
			}
			else
			{
				$array_insert['added_date'] = date('Y-m-d H:i:s');
				$this->db->insert('d_status_helm',$array_insert);
				$insert_id = $this->db->insert_id();
				$status_helm_sebelum = $status_helm;
			}
			
			if($status_helm_sebelum == 1)
			{
				$sn = "select nama from d_pegawai where nip = '$nip' ";
				$qn = $this->db->query($sn);
				$nama = $qn->row()->nama;
				$nama = explode(" ",$nama);
				$nama = $nama[0];
				if($status_helm == 0)
				{
					$ch = curl_init();
					curl_setopt($ch,CURLOPT_URL, $alamat_aplikasi.'notifikasi/index.php?nip='.$nip.'&nama='.$nama.'&kategori=helm');			   
					$result = curl_exec($ch);
					curl_close($ch);
				}
				elseif($status_helm == -1)
				{
					$ch = curl_init();
					curl_setopt($ch,CURLOPT_URL, $alamat_aplikasi.'/notifikasi/index.php?nip='.$nip.'&nama='.$nama.'&kategori=helm_terputus');			   
					$result = curl_exec($ch);
					curl_close($ch);
				}
			}
			
			if($status_helm_sebelum == 0)
			{
				$sn = "select nama from d_pegawai where nip = '$nip' ";
				$qn = $this->db->query($sn);
				$nama = $qn->row()->nama;
				$nama = explode(" ",$nama);
				$nama = $nama[0];
				if($status_helm == 1)
				{
					$ch = curl_init();
					curl_setopt($ch,CURLOPT_URL, $alamat_aplikasi.'/notifikasi/index.php?nip='.$nip.'&nama='.$nama.'&kategori=pakai_helm');			   
					$result = curl_exec($ch);
					curl_close($ch);
				}
				elseif($status_helm == -1)
				{
					$ch = curl_init();
					curl_setopt($ch,CURLOPT_URL, $alamat_aplikasi.'/notifikasi/index.php?nip='.$nip.'&nama='.$nama.'&kategori=helm_terputus');			   
					$result = curl_exec($ch);
					curl_close($ch);
				}
			}
			
			/* if($status_helm_sebelum == -1)
			{
				$sn = "select nama from d_pegawai where nip = '$nip' ";
				$qn = $this->db->query($sn);
				$nama = $qn->row()->nama;
				$nama = explode(" ",$nama);
				$nama = $nama[0];
				if($status_helm == 0)
				{
					$ch = curl_init();
					curl_setopt($ch,CURLOPT_URL, 'http://ppas.rtmekanika.com/notifikasi/index.php?nip='.$nip.'&nama='.$nama.'&kategori=helm');			   
					$result = curl_exec($ch);
					curl_close($ch);
				}
				elseif($status_helm == 1)
				{
					$ch = curl_init();
					curl_setopt($ch,CURLOPT_URL, 'http://ppas.rtmekanika.com/notifikasi/index.php?nip='.$nip.'&nama='.$nama.'&kategori=pakai_helm');			   
					$result = curl_exec($ch);
					curl_close($ch);
				}
				
			} */
			
			$sc1 = "select id,status_helm 
					from r_status_helm 
					where nip = '$nip' 
					and waktu = (select max(waktu) from r_status_helm where nip = '$nip' ) ";
			$qc1 = $this->db->query($sc1);
			if($qc1->num_rows() > 0)
			{
				$status_helm_sebelum = $qc1->row()->status_helm;
				if($status_helm != $status_helm_sebelum)
				{
					$array_insert['waktu'] = date('Y-m-d H:i:s');
					$this->db->insert('r_status_helm',$array_insert);
				}
			
			}
			else
			{
				$array_insert['waktu'] = date('Y-m-d H:i:s');
				$this->db->insert('r_status_helm',$array_insert);
			}
			
			
			if(isset($insert_id) && $insert_id != NULL && $insert_id != '')
			{
				$this->response([
                    'status' => TRUE,
                    'message' => 'Data was inserted'
				], REST_Controller::HTTP_CREATED);
			}				
		}
		else
		{
			$this->response([
                    'status' => FALSE,
                    'message' => 'No data were inserted'
            ], REST_Controller::HTTP_NOT_FOUND);
		}
		
	}
	
	public function waktu_get()
	{
		set_time_limit(0);
		date_default_timezone_set('Asia/Jakarta');
		$datas['waktu_server'] = date('Y-m-d H:i:s');
        // Check if the users data store contains users (in case the database result returns NULL)
        if ($datas)
        {
            // Set the response and exit
            $this->response($datas, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
        else
        {
            // Set the response and exit
            $this->response([
                    'status' => FALSE,
                    'message' => 'No Data were found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
	}
	
	public function statusKoneksi_get()
	{
		$this->response([
            'status' => TRUE,
            'message' => 'Konek'
		], REST_Controller::HTTP_CREATED);
		
	}
	
	public function statusGate_post()
	{
		$serial_number = $this->post('serial_number');
		$ip = $this->post('ip');
		if(isset($serial_number) && $serial_number != '' )
		{
			$array_insert = array(
					'serial_number' => $serial_number,
					'ip' => $ip
				);
			$sc = "select id from d_status_gate where serial_number = '$serial_number' ";
			$qc = $this->db->query($sc);
			if($qc->num_rows() > 0)
			{
				$id=$qc->row()->id;
				$array_insert['waktu'] = date('Y-m-d H:i:s');
				$this->db->where('id',$id);
				$this->db->update('d_status_gate',$array_insert);
				$insert_id = $id;
			}
			else
			{
				$array_insert['waktu'] = date('Y-m-d H:i:s');
				$this->db->insert('d_status_gate',$array_insert);
				$insert_id = $this->db->insert_id();
			}
						
			if(isset($insert_id) && $insert_id != NULL && $insert_id != '')
			{
				$this->response([
                    'status' => TRUE,
                    'message' => 'Data was inserted'
				], REST_Controller::HTTP_CREATED);
			}				
		}
		else
		{
			$this->response([
                    'status' => FALSE,
                    'message' => 'No data were inserted'
            ], REST_Controller::HTTP_NOT_FOUND);
		}
		
	}

	public function statusGeofence_post()
	{
		$nip = $this->post('nip');
		$lat = $this->post('lat');
		$lng = $this->post('lng');
		$status = $this->post('status');
		$waktu = date('Y-m-d H:i:s');
		if(isset($nip) && $nip != '' && isset($status) && $status != '' && $lat != '' && $lat != 0 && $lng != '' && $lng != 0 )
		{
			$array_insert = array(
					'nip' => $nip,
					'lat' => $lat,
					'lng' => $lng,
					'status' => $status,
				);
			$sc = "select id,status from d_status_geofence where nip = '$nip' ";
			$qc = $this->db->query($sc);
			if($qc->num_rows() > 0)
			{
				$id=$qc->row()->id;
				
				$array_insert['update_date'] = $waktu;
				$this->db->where('id',$id);
				$this->db->update('d_status_geofence',$array_insert);
				$insert_id = $id;
				$status_sebelum = $qc->row()->status;
			}
			else
			{
				$array_insert['added_date'] = $waktu;
				$array_insert['update_date'] = $waktu;
				$this->db->insert('d_status_geofence',$array_insert);
				$insert_id = $this->db->insert_id();
				$status_sebelum = $status;
			}
			
			if($status_sebelum == 1)
			{
				$sn = "select nama from d_pegawai where nip = '$nip' ";
				$qn = $this->db->query($sn);
				$nama = $qn->row()->nama;
				$nama = explode(" ",$nama);
				$nama = $nama[0];
				if($status_helm == 0)
				{
					$ch = curl_init();
					curl_setopt($ch,CURLOPT_URL, $alamat_aplikasi.'/notifikasi/index.php?nip='.$nip.'&nama='.$nama.'&kategori=lokasi');			   
					$result = curl_exec($ch);
					curl_close($ch);
				}
			}
						
			if($status != $status_sebelum)
			{
				$sc1 = "select id,status 
					from r_status_geofence 
					where nip = '$nip' 
					and waktu = (select max(waktu) from r_status_geofence where nip = '$nip' ) ";
				$qc1 = $this->db->query($sc1);
				if($qc1->num_rows() > 0)
				{
					$status = $qc1->row()->status;
					if($status != $status_sebelum)
					{
						$array_insert['waktu'] = $waktu;
						$this->db->insert('r_status_geofence',$array_insert);
					}
				
				}
				else
				{
					$array_insert['waktu'] = $waktu;
					$this->db->insert('r_status_geofence',$array_insert);
				}
			}
			
			if(isset($insert_id) && $insert_id != NULL && $insert_id != '')
			{
				$this->response([
                    'status' => TRUE,
                    'message' => 'Data was inserted'
				], REST_Controller::HTTP_CREATED);
			}				
		}
		else
		{
			$this->response([
                    'status' => FALSE,
                    'message' => 'No data were inserted'
            ], REST_Controller::HTTP_NOT_FOUND);
		}
		
	}
	
	public function statusEmergency_post()
	{
		$alamat_aplikasi = $this->db->query("select value as alamat_aplikasi from t_setting where id = 1 ")->row()->alamat_aplikasi;
		$nip = $this->post('nip');
		$lat = $this->post('lat');
		$lng = $this->post('lng');
		$status_emergency = $this->post('status_emergency');
		$waktu = date('Y-m-d H:i:s');
		if(isset($nip) && $nip != '' && isset($status_emergency) && $status_emergency != '' && $lat != '' && $lat != 0 && $lng != '' && $lng != 0 )
		{
			$array_insert = array(
					'nip' => $nip,
					'lat' => $lat,
					'lng' => $lng,
					'status_emergency' => $status_emergency,
				);
			$sc = "select id,status_emergency from d_status_emergency where nip = '$nip' ";
			$qc = $this->db->query($sc);
			if($qc->num_rows() > 0)
			{
				$id=$qc->row()->id;
				
				$array_insert['update_date'] = $waktu;
				$this->db->where('id',$id);
				$this->db->update('d_status_emergency',$array_insert);
				$insert_id = $id;
				$status_sebelum = $qc->row()->status_emergency;
			}
			else
			{
				$array_insert['added_date'] = $waktu;
				$array_insert['update_date'] = $waktu;
				$this->db->insert('d_status_emergency',$array_insert);
				$insert_id = $this->db->insert_id();
				$status_sebelum = $status_emergency;
			}
			
			if($status_sebelum == 0)
			{
				$sn = "select nama from d_pegawai where nip = '$nip' ";
				$qn = $this->db->query($sn);
				$nama = $qn->row()->nama;
				$nama = explode(" ",$nama);
				$nama = $nama[0];
				if($status_emergency == 1)
				{
					$ch = curl_init();
					curl_setopt($ch,CURLOPT_URL, $alamat_aplikasi.'/notifikasi/index.php?lat='.$lat.'&lng='.$lng.'&nip='.$nip.'&nama='.$nama.'&kategori=darurat');			   
					$result = curl_exec($ch);
					curl_close($ch);
				}
			}
			
			if($status_emergency != $status_sebelum)
			{
				$sc1 = "select id,status_emergency 
					from r_status_emergency 
					where nip = '$nip' 
					and waktu = (select max(waktu) from r_status_emergency where nip = '$nip' ) ";
				$qc1 = $this->db->query($sc1);
				if($qc1->num_rows() > 0)
				{
					$status_emergency = $qc1->row()->status_emergency;
					if($status_emergency != $status_sebelum)
					{
						$array_insert['waktu'] = $waktu;
						$this->db->insert('r_status_emergency',$array_insert);
					}
				
				}
				else
				{
					$array_insert['waktu'] = $waktu;
					$this->db->insert('r_status_emergency',$array_insert);
				}
			}
			
			if(isset($insert_id) && $insert_id != NULL && $insert_id != '')
			{
				$this->response([
                    'status' => TRUE,
                    'message' => 'Data was inserted'
				], REST_Controller::HTTP_CREATED);
			}				
		}
		else
		{
			$this->response([
                    'status' => FALSE,
                    'message' => 'No data were inserted'
            ], REST_Controller::HTTP_NOT_FOUND);
		}
		
	}
	
	public function statusDepartment_post()
	{
		$nip = $this->post('nip');
		$lat = $this->post('lat');
		$lng = $this->post('lng');
		$nama_department = $this->post('nama_department');
		$waktu = date('Y-m-d H:i:s');
		if(isset($nip) && $nip != '' && isset($nama_department) && $nama_department != '' && $lat != '' && $lat != 0 && $lng != '' && $lng != 0 )
		{
			$array_insert = array(
					'nip' => $nip,
					'lat' => $lat,
					'lng' => $lng,
					'nama_department' => $nama_department,
				);
			$sc = "select id,nama_department from d_status_department where nip = '$nip' ";
			$qc = $this->db->query($sc);
			if($qc->num_rows() > 0)
			{
				$id=$qc->row()->id;
				
				$array_insert['update_date'] = $waktu;
				$this->db->where('id',$id);
				$this->db->update('d_status_department',$array_insert);
				$insert_id = $id;
				$status_sebelum = $qc->row()->nama_department;
			}
			else
			{
				$array_insert['added_date'] = $waktu;
				$array_insert['update_date'] = $waktu;
				$this->db->insert('d_status_department',$array_insert);
				$insert_id = $this->db->insert_id();
				$status_sebelum = $nama_department;
			}
			
			/* if($status_sebelum == 0)
			{
				$sn = "select nama from d_pegawai where nip = '$nip' ";
				$qn = $this->db->query($sn);
				$nama = $qn->row()->nama;
				$nama = explode(" ",$nama);
				$nama = $nama[0];
				if($status_emergency == 1)
				{
					$ch = curl_init();
					curl_setopt($ch,CURLOPT_URL, $alamat_aplikasi.'/notifikasi/index.php?nip='.$nip.'&nama='.$nama.'&kategori=darurat');			   
					$result = curl_exec($ch);
					curl_close($ch);
				}
			} */
			
			if($nama_department != $status_sebelum)
			{
				$sc1 = "select id,nama_department 
					from r_status_department
					where nip = '$nip' 
					and waktu = (select max(waktu) from r_status_department where nip = '$nip' ) ";
				$qc1 = $this->db->query($sc1);
				if($qc1->num_rows() > 0)
				{
					$status_department = $qc1->row()->status_department;
					if($status_department != $status_sebelum)
					{
						$array_insert['waktu'] = $waktu;
						$this->db->insert('r_status_department',$array_insert);
					}
				
				}
				else
				{
					$array_insert['waktu'] = $waktu;
					$this->db->insert('r_status_department',$array_insert);
				}
			}
			
			if(isset($insert_id) && $insert_id != NULL && $insert_id != '')
			{
				$this->response([
                    'status' => TRUE,
                    'message' => 'Data was inserted'
				], REST_Controller::HTTP_CREATED);
			}				
		}
		else
		{
			$this->response([
                    'status' => FALSE,
                    'message' => 'No data were inserted'
            ], REST_Controller::HTTP_NOT_FOUND);
		}
		
	}
	
	public function akunProximy_get()
	{
		set_time_limit(0);
		$datas = array();
        $s = "select value as token_akun from t_setting where (id = 5) ";
	//	echo $s;
		$q = $this->db->query($s);
		foreach($q->result() as $row)
		{
			$datas['token_akun'] = $row->token_akun;
		}
        // Check if the users data store contains users (in case the database result returns NULL)
        if ($datas)
        {
            // Set the response and exit
            $this->response($datas, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
        else
        {
            // Set the response and exit
            $this->response([
                    'status' => FALSE,
                    'message' => 'No Data were found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
	}
}
