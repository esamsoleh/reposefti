<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class DownloadData extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	public function data()
	{
		$akun = $this->db->query("select value as akun from t_setting where id = 2 ")->row()->akun;
		$password = $this->db->query("select value as password from t_setting where id = 3 ")->row()->password;
		$fields = array(
			'email' => $akun,
			'password' => $password,
		);
		$fields_string = '';
		foreach($fields as $key=>$value) 
		{ 
			$fields_string .= $key.'='.$value.'&'; 
		}
		rtrim($fields_string, '&');
		
		//start login
		$ch = curl_init();
		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL, 'https://api.proximi.fi/core_auth/login');
		curl_setopt($ch,CURLOPT_POST, count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, TRUE);

		$result = curl_exec($ch);
		curl_close($ch);
		
		$result = json_decode($result,TRUE);
		$token = $result['token'];
		//end login
		
		//start update project place
		$ch = curl_init();
		$authorization = "Authorization: Bearer ".$token;
		curl_setopt($ch,CURLOPT_URL, 'https://api.proximi.fi/core/places');
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, TRUE);  
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));        
		$result = curl_exec($ch);
		$result = json_decode($result);
		foreach($result as $rest)
		{
			$data = array(
				'id_place' => $rest->id,
				'nama' => $rest->name,
				'alamat' => $rest->address,
				'lat' => $rest->location->lat,
				'lng' => $rest->location->lng,
				'zoom' => $rest->location->zoom,
				'created_at' => date('Y-m-d H:i:s',strtotime($rest->createdAt)),
				'updated_at' => date('Y-m-d H:i:s',strtotime($rest->updatedAt)),
			);
			
			
			$sc = "select id from d_project where id_place = '$rest->id' ";
			$qc = $this->db->query($sc);
			if($qc->num_rows() > 0)
			{
				$rc = $qc->row();
				$updated_at = date('Y-m-d H:i:s',strtotime($rest->updatedAt));
				$sc1 = "select id from d_project where id_place = '$rest->id' and updated_at = '$updated_at' ";
				//echo $sc1."\n";
				$qc1 = $this->db->query($sc1);
				if($qc1->num_rows() == 0)
				{
					$this->db->where('id',$rc->id);
					$this->db->update('d_project',$data);
				}
				
			}
			else
			{
				$this->db->insert('d_project',$data);
			}
		}
		curl_close($ch);
		//echo $this->db->last_query();
		//end update project place
		
		//start project floor
		$ch = curl_init();
		$authorization = "Authorization: Bearer ".$token;
		curl_setopt($ch,CURLOPT_URL, 'https://api.proximi.fi/core/floors');
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, TRUE);  
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));        
		$result = curl_exec($ch);
		$result = json_decode($result);
		foreach($result as $rest)
		{
			$data = array(
				'id_place' => $rest->place_id,
				'nama' => $rest->name,
				'id_floor' => $rest->id,
				'anchors' => json_encode($rest->anchors),
				'added_at' => date('Y-m-d H:i:s',strtotime($rest->createdAt)),
				'updated_at' => date('Y-m-d H:i:s',strtotime($rest->updatedAt)),
				'id_project' => $this->get_id_project($rest->place_id),
			);
			
			
			$sc = "select id from d_site where id_floor = '$rest->id' ";
			$qc = $this->db->query($sc);
			if($qc->num_rows() > 0)
			{
				$rc = $qc->row();
				$updated_at = date('Y-m-d H:i:s',strtotime($rest->updatedAt));
				$sc1 = "select id from d_site where id_floor = '$rest->id' and updated_at = '$updated_at' ";
				//echo $sc1."\n";
				$qc1 = $this->db->query($sc1);
				if($qc1->num_rows() == 0)
				{
					$this->db->where('id',$rc->id);
					$this->db->update('d_site',$data);
				}
				
			}
			else
			{
				$this->db->insert('d_site',$data);
			}
		}
		curl_close($ch);
		//echo $this->db->last_query();
		//end update project floor
		
		//start all department
		$ch = curl_init();
		$authorization = "Authorization: Bearer ".$token;
		curl_setopt($ch,CURLOPT_URL, 'https://api.proximi.fi/core/departments');
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, TRUE);  
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));        
		$result = curl_exec($ch);
		$result = json_decode($result);
		$array_id_department = array();
		foreach($result as $rest)
		{
			$array_id_department[] = $rest->id;
		}
		curl_close($ch);
		//end update department
	//	print_r($array_id_department);
		foreach($array_id_department as $id_department)
		{
			//echo 'id_department:'.$id_department.'</br>';
			//start department
			$ch = curl_init();
			$authorization = "Authorization: Bearer ".$token;
			$url = 'https://api.proximi.fi/core/departments/'.$id_department;
		//	echo $url;
			curl_setopt($ch,CURLOPT_URL, $url);
			curl_setopt($ch,CURLOPT_RETURNTRANSFER, TRUE);  
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));        
			$result = curl_exec($ch);
			$result = json_decode($result);
			$rest = $result;
		//	print_r($result);
			
				$data = array(
					'createdAt' => date('Y-m-d H:i:s',strtotime($rest->createdAt)),
					'floor_id' => ($rest->floor_id),
					'id' => ($rest->id),
					'name' => ($rest->name),
					'organization_id' => ($rest->organization_id),
					'place_id' => ($rest->place_id),
				//	'tags' => ($rest->tags),
					'updatedAt' => date('Y-m-d H:i:s',strtotime($rest->updatedAt)),
				);
				//print_r($data);
				
				$sc = "select id from d_department where id = '$rest->id' ";
				$qc = $this->db->query($sc);
				if($qc->num_rows() > 0)
				{
					$rc = $qc->row();
					$updatedAt = date('Y-m-d H:i:s',strtotime($rest->updatedAt));
					$sc1 = "select id from d_department where id = '$rest->id' and updatedAt = '$updatedAt' ";
					//echo $sc1."\n";
					$qc1 = $this->db->query($sc1);
					if($qc1->num_rows() == 0)
					{
						$this->db->where('id',$rc->id);
						$this->db->update('d_department',$data);
					}
					
				}
				else
				{
					$this->db->insert('d_department',$data);
				}
			
			
			curl_close($ch);
			//echo $this->db->last_query();
		}
		//end update department
		
		//start project input
		$ch = curl_init();
		$authorization = "Authorization: Bearer ".$token;
		curl_setopt($ch,CURLOPT_URL, 'https://api.proximi.fi/core/inputs');
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, TRUE);  
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));        
		$result = curl_exec($ch);
		$result = json_decode($result);
		//print_r($result);
		foreach($result as $rest)
		{
		//	print_r($rest);
		//	echo '</br>';
			$pl = $this->get_place($rest->department_id);
		//	echo 'pl';
		//	print_r($pl);
			$data = array(
				'id_input' => $rest->id,
				'name' => $rest->name,
				'department_id' => $rest->department_id,
				'type' => $rest->type,
				'id_place' => $pl->place_id,
				'id_floor' => $pl->floor_id,
		//		'triggerFloorChange' => $rest->triggerFloorChange,
		//		'triggerVenueChange' => $rest->triggerVenueChange,
				'lat' => $rest->data->marker->lat,
				'lng' => $rest->data->marker->lng,
		//		'height' => $rest->data->height,
				'createdAt' => date('Y-m-d H:i:s',strtotime($rest->createdAt)),
				'updatedAt' => date('Y-m-d H:i:s',strtotime($rest->updatedAt)),
			);
			
			
			$sc = "select id from d_input where id_input = '$rest->id' ";
			$qc = $this->db->query($sc);
			if($qc->num_rows() > 0)
			{
				$rc = $qc->row();
				$updatedAt = date('Y-m-d H:i:s',strtotime($rest->updatedAt));
				$sc1 = "select id from d_input where id_input = '$rest->id' and updatedAt = '$updatedAt' ";
				echo $sc1."\n";
				$qc1 = $this->db->query($sc1);
				if($qc1->num_rows() > 0)
				{
					$this->db->where('id',$rc->id);
					$this->db->update('d_input',$data);
				}
				
			}
			else
			{
				$this->db->insert('d_input',$data);
			}
		}
		curl_close($ch);
		echo $this->db->last_query();
		//end update project input
		
		//start visitor
		/* $ch = curl_init();
		$authorization = "Authorization: Bearer ".$token;
		curl_setopt($ch,CURLOPT_URL, 'https://api.proximi.fi/core/visitors');
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, TRUE);  // Prepare the authorisation token
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization )); // Inject the token into the header
       
		$result = curl_exec($ch);
		$result = json_decode($result,TRUE);
		
		foreach($result as $res)
		{
			if($res['id'] != '')
			{
				$visitors[] = $res['id'];
			}
		}
		
		curl_close($ch);
		
		foreach($visitors as $vis)
		{
			//cek visitor
			$ch = curl_init();
			$authorization = "Authorization: Bearer ".$token;
			curl_setopt($ch,CURLOPT_URL, 'https://api.proximi.fi/analytics/visitor/'.$vis);
			curl_setopt($ch,CURLOPT_RETURNTRANSFER, TRUE);  // Prepare the authorisation token
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization )); // Inject the token into the header
		   
			$result = curl_exec($ch);
			$result = json_decode($result);
			if(isset($result->lastPosition) && $result->lastPosition != '' && $result->lastPosition != NULL)
			{
				print_r($result->lastPosition)."\n";
					$data = array(
						'id_aktivitas_visitor' => $result->lastPosition->id,
						'id_visitor' => $result->lastPosition->visitor_id,
						'id_place' => $result->lastPosition->place_id,
						'id_floor' => $result->lastPosition->floor_id,
				//		'firstSeen' => $result->lastPosition->firstSeen,
				//		'lastSeen' => $result->lastPosition->lastSeen,
						'lat' => $result->lastPosition->location->lat,
						'lng' => $result->lastPosition->location->lng,
						'akurasi' => $result->lastPosition->location->accuracy,
						'createdAt' => date('Y-m-d H:i:s',strtotime($result->lastPosition->createdAt)),
						'updatedAt' => date('Y-m-d H:i:s',strtotime($result->lastPosition->updatedAt)),
					);
					
					
					$sc = "select id from d_visitor where id_aktivitas_visitor = '$result->lastPosition->id' ";
					$qc = $this->db->query($sc);
					if($qc->num_rows() > 0)
					{
						$rc = $qc->row();
						$updatedAt = date('Y-m-d H:i:s',strtotime($result->lastPosition->updatedAt));
						$sc1 = "select id from d_visitor where id_aktivitas_visitor = '$result->lastPosition->id' and updatedAt = '$updatedAt' ";
						//echo $sc1."\n";
						$qc1 = $this->db->query($sc1);
						if($qc1->num_rows() == 0)
						{
							$this->db->where('id',$rc->id);
							$this->db->update('d_visitor',$data);
						}
						
					}
					else
					{
						$this->db->insert('d_visitor',$data);
					}
				
			}
			
			curl_close($ch);
		} */
		
		//end visitor
		
		//start GEOFENCE
		$ch = curl_init();
		$authorization = "Authorization: Bearer ".$token;
		curl_setopt($ch,CURLOPT_URL, 'https://api.proximi.fi/core/geofences');
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, TRUE);  
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));        
		$result = curl_exec($ch);
		$result = json_decode($result);
		foreach($result as $rest)
		{
			$data = array(
				'address' => $rest->address,
				'area' => json_encode($rest->area),
				'createdAt' => date('Y-m-d H:i:s',strtotime($rest->createdAt)),
				'department_id' => ($rest->department_id),
				'id_geofence' => ($rest->id),
				'name' => ($rest->name),
				'organization_id' => ($rest->organization_id),
				'place_id' => ($rest->place_id),
				'radius' => ($rest->radius),
				'updatedAt' => date('Y-m-d H:i:s',strtotime($rest->updatedAt)),
				'id_project' => $this->get_id_project($rest->place_id),
			);
			
			
			$sc = "select id from d_geofence where id_geofence = '$rest->id' ";
			$qc = $this->db->query($sc);
			if($qc->num_rows() > 0)
			{
				$rc = $qc->row();
				$updatedAt = date('Y-m-d H:i:s',strtotime($rest->updatedAt));
				$sc1 = "select id from d_geofence where id_geofence = '$rest->id' and updatedAt = '$updatedAt' ";
				//echo $sc1."\n";
				$qc1 = $this->db->query($sc1);
				if($qc1->num_rows() == 0)
				{
					$this->db->where('id',$rc->id);
					$this->db->update('d_geofence',$data);
				}
				
			}
			else
			{
				$this->db->insert('d_geofence',$data);
			}
		}
		curl_close($ch);
		//echo $this->db->last_query();
		//end update GEOFENCE
		
		
		
		
		
	}
	
	function get_id_project($id_place)
	{
		$s = "select id from d_project where id_place = '$id_place' ";
		return $this->db->query($s)->row()->id;
	}
	
	function get_place($department_id)
	{
		$s = "select place_id,floor_id from d_department where id = '$department_id' ";
		return $this->db->query($s)->row();
	}
	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */