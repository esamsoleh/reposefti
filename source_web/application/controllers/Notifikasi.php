<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notifikasi extends CI_Controller {
	public $apps;
	var $nama_user;
	var $file_foto;
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
		if (!$this->ion_auth->logged_in())
			{
				redirect('/auth/login', 'refresh');
			}
		$this->apps = 'dashboard';
		$this->nama_user = $this->ion_auth->user()->row()->first_name.' '.$this->ion_auth->user()->row()->last_name;
		//$this->file_foto = get_foto();
		$this->file_foto = $this->ion_auth->user()->row()->foto;
		$this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		
	}
	
	public function total()
	{
		$groups = $this->ion_auth->get_users_groups()->result();
		$total = 0;
		foreach($groups as $group)
		{
			$id_group = $group->id;
			switch($id_group)
			{
				 case 1:
					$spesasan = "select id,nip from d_status_helm where status_helm = 0 group by nip";
					$qspesasan = $this->db->query($spesasan);
					if($qspesasan->num_rows() > 0)
					{
						$jmlpesanan = $qspesasan->num_rows();
					}
					else
					{
						$jmlpesanan = 0;
					}
					
					$total = $total + $jmlpesanan;
				break;
				
				/* case 3:
				//proyek masuk
					$spesasan = "select count(id) as jml_proyek from d_project where status = 1 and jenis = 0";
					$qspesasan = $this->db->query($spesasan);
					$jmlpesanan = $qspesasan->row()->jml_proyek;
					$total = $total + $jmlpesanan;
				break;
				case 4:
				//penawaran disetujui
					$stawar = "select count(id) as jml_tawar from d_penawaran where status = 1 ";
					$qtawar = $this->db->query($stawar);
					$jmltawar = $qtawar->row()->jml_tawar;
					$total = $total + $jmltawar;
				
				//invoice dp dibayar sebagaian
					$sbayarsebagian = "select count(id) as jml_bayar_sebagian from d_invoice where status = 1 and process = 0 ";
					$qbayarsebagian = $this->db->query($sbayarsebagian);
					$jml_bayar_sebagian = $qbayarsebagian->row()->jml_bayar_sebagian;
					$total = $total + $jml_bayar_sebagian;
				
				//project siap ditagih
					$sditagih = "select count(id) as jml_tagih from d_project where tagih = 1 and status = 1 and invoice_pelunasan = 0 and tipe = 1";
					$qsditagih = $this->db->query($sditagih);
					$jml_bayar_sditagih = $qsditagih->row()->jml_tagih;
					$total = $total + $jml_bayar_sditagih;
				break;
				case 5:
				//proyek harus diproses
					$spesasan = "select count(id) as jml_proyek from d_project where status = 1 and status_finish IS NULL";
					$qspesasan = $this->db->query($spesasan);
					$jmlpesanan = $qspesasan->row()->jml_proyek;
					$total = $total + $jmlpesanan;
				break;
				case 6:
				//hasil stok harus dicatat
					$spesasan = "select count(id) as jml_proyek from d_project where tipe = 2 and status = 1 and status_finish IS NOT NULL";
					$qspesasan = $this->db->query($spesasan);
					$jmlpesanan = $qspesasan->row()->jml_proyek;
					$total = $total + $jmlpesanan;
				break;
				case 7:
				//invoice dp masuk
					$sinvd = "select count(id) as jml_inv_dp from d_invoice where status = 0 ";
					$qinvd = $this->db->query($sinvd);
					$jmlinvdp = $qinvd->row()->jml_inv_dp;
					$total = $total + $jmlinvdp;
				//invoice pelunasan masuk
					$sinvd = "select count(id) as jml_inv_dp from d_invoice_pelunasan where status = 0 ";
					$qinvd = $this->db->query($sinvd);
					$jmlinvdp = $qinvd->row()->jml_inv_dp;
					$total = $total + $jmlinvdp;
				break;
				case 8:
				//delivery
					$sinvd = "select count(a.id) as jml_delivery 
								from d_project a
								left join d_invoice c on(a.id_invoice = c.id)
								left join d_pelanggan b on(c.id_pelanggan = b.id)
								left join d_delivery d on(a.id = d.id_project)
								left join d_invoice_pelunasan e on(e.id_project = a.id)
								where 1 = 1 
								and a.jenis = 1 
								and a.tipe=1
								and a.status_finish = 1
								and e.status = 1
								and a.status = 1
								";
					$qinvd = $this->db->query($sinvd);
					$jmlinvdp = $qinvd->row()->jml_delivery;
					$total = $total + $jmlinvdp;
				break; */
			}
		}
		
		echo $total;
	}
	
	public function item()
	{
		$rest = '';
		$groups = $this->ion_auth->get_users_groups()->result();
		$total = 0;
		foreach($groups as $group)
		{
			$id_group = $group->id;
			switch($id_group)
			{
				case 1:
					$spesanan = "select id,nip from d_status_helm where status_helm = 0 ";
					$qspesanan = $this->db->query($spesanan);
					$nqspesanan = $qspesanan->num_rows();
					if($nqspesanan > 0)
					{
						foreach($qspesanan->result() as $peg)
						{
							$rest .= '<li>
								<a href="#">
								  <i class="fa fa-warning text-yellow">Pegawai dengan NIP:'.$peg->nip.' Tidak Menggunakan Helm</i>
								</a>
							  </li>';
						}
						
					}
				break;
				
				/* case 3:
					$spesanan = "select id from d_project where status = 1 and jenis = 0 ";
					$qspesanan = $this->db->query($spesanan);
					$nqspesanan = $qspesanan->num_rows();
					if($nqspesanan > 0)
					{
						$rest .= '<a href="/pesanan/daftar_proses" class="list-group-item">
										<div class="media">
											<div class="media-left p-r-10">
												<em class="fa fa-diamond bg-primary"></em>
											</div>
											<div class="media-body">
												<h5 class="media-heading">Ada <b>'.$nqspesanan.'</b> Pesanan Masuk.</h5>
												<p class="m-0">
													Segera Proses Pesanan Yang Masuk. 
												</p>
											</div>
										</div>
									</a>';
					}
				break;
				case 4:
					//penawaran 
					$stawar = "select id from d_penawaran where status = 1 ";
					$qtawar = $this->db->query($stawar);
					$ntawar = $qtawar->num_rows();
					if($ntawar > 0)
					{
						$rest .= '<a href="/invoice/" class="list-group-item">
										<div class="media">
											<div class="media-left p-r-10">
												<em class="fa fa-diamond bg-primary"></em>
											</div>
											<div class="media-body">
												<h5 class="media-heading">Ada <b>'.$ntawar.'</b> Penawaran Yang Disetujui Pelanggan.</h5>
												<p class="m-0">
													Segera Buat Invoice DP. 
												</p>
											</div>
										</div>
									</a>';
					}
					
					//bayar sebagian
					$sbayarsebagian = "select id from d_invoice where status = 1 and process = 0 ";
					$qsbayarsebagian = $this->db->query($sbayarsebagian);
					$nqsbayarsebagian = $qsbayarsebagian->num_rows();
					if($nqsbayarsebagian > 0)
					{
						$rest .= '<a href="/pesanan/" class="list-group-item">
										<div class="media">
											<div class="media-left p-r-10">
												<em class="fa fa-diamond bg-primary"></em>
											</div>
											<div class="media-body">
												<h5 class="media-heading">Ada <b>'.$nqsbayarsebagian.'</b> Invoice Dibayar Sebagian.</h5>
												<p class="m-0">
													Segera Buat Projek Baru. 
												</p>
											</div>
										</div>
									</a>';
					}
					
					//siap tagih
					$siaptagih = "select id from d_project where tagih = 1 and status = 1  and invoice_pelunasan = 0 and tipe = 1";
					$qsiaptagih = $this->db->query($siaptagih);
					$nqsiaptagih = $qsiaptagih->num_rows();
					if($nqsiaptagih > 0)
					{
						$rest .= '<a href="/invoice/pelunasan" class="list-group-item">
										<div class="media">
											<div class="media-left p-r-10">
												<em class="fa fa-diamond bg-primary"></em>
											</div>
											<div class="media-body">
												<h5 class="media-heading">Ada <b>'.$nqsiaptagih.'</b> Pesanan Siap Ditagih.</h5>
												<p class="m-0">
													Segera Buat Invoice Penagihan. 
												</p>
											</div>
										</div>
									</a>';
					}
				break;
				case 5:
					$sproyek = "select id from d_project where status = 1 and jenis = 1 and status_finish IS NULL ";
					$qsproyek = $this->db->query($sproyek);
					$nsproyek = $qsproyek->num_rows();
					if($nsproyek > 0)
					{
						$rest .= '<a href="/proyek/proses" class="list-group-item">
										<div class="media">
											<div class="media-left p-r-10">
												<em class="fa fa-diamond bg-primary"></em>
											</div>
											<div class="media-body">
												<h5 class="media-heading">Ada <b>'.$nsproyek.'</b> Proyek Ongoing.</h5>
												<p class="m-0">
													Segera Proses Yang Ongoing. 
												</p>
											</div>
										</div>
									</a>';
					}
				break;
				case 6:
					$sproyek = "select id from d_project where tipe = 2 and status = 1 and status_finish IS NOT NULL ";
					$qsproyek = $this->db->query($sproyek);
					$nsproyek = $qsproyek->num_rows();
					if($nsproyek > 0)
					{
						$rest .= '<a href="/persediaanProduk" class="list-group-item">
										<div class="media">
											<div class="media-left p-r-10">
												<em class="fa fa-diamond bg-primary"></em>
											</div>
											<div class="media-body">
												<h5 class="media-heading">Ada <b>'.$nsproyek.'</b> Stok Yang Harus Dicatat.</h5>
												<p class="m-0">
													Segera Catat Hasil Produksi Stok. 
												</p>
											</div>
										</div>
									</a>';
					}
				break;
				case 7:
					//invoice dp
					$sinvd = "select id from d_invoice where status = 0 ";
					$qinvd = $this->db->query($sinvd);
					$ninvd = $qinvd->num_rows();
					if($ninvd > 0)
					{
						$rest .= '<a href="/invoice/proses" class="list-group-item">
										<div class="media">
											<div class="media-left p-r-10">
												<em class="fa fa-diamond bg-primary"></em>
											</div>
											<div class="media-body">
												<h5 class="media-heading">Ada <b>'.$ninvd.'</b> Invoice DP Yang Harus Diproses.</h5>
												<p class="m-0">
													Segera Proses Inovoice DP. 
												</p>
											</div>
										</div>
									</a>';
					}
					//invoice pelunasan
					$sinvd = "select id from d_invoice_pelunasan where status = 0";
					$qinvd = $this->db->query($sinvd);
					$ninvd = $qinvd->num_rows();
					if($ninvd > 0)
					{
						$rest .= '<a href="/invoice/proses_pelunasan" class="list-group-item">
										<div class="media">
											<div class="media-left p-r-10">
												<em class="fa fa-diamond bg-primary"></em>
											</div>
											<div class="media-body">
												<h5 class="media-heading">Ada <b>'.$ninvd.'</b> Invoice Pelunasan Yang Harus Diproses.</h5>
												<p class="m-0">
													Segera Proses Inovoice Pelunasan. 
												</p>
											</div>
										</div>
									</a>';
					}
				break;
				case 8:
					//delivery
					$sinvd = "select a.id 
								from d_project a
								left join d_invoice c on(a.id_invoice = c.id)
								left join d_pelanggan b on(c.id_pelanggan = b.id)
								left join d_delivery d on(a.id = d.id_project)
								left join d_invoice_pelunasan e on(e.id_project = a.id)
								where 1 = 1 
								and a.jenis = 1 
								and a.tipe=1
								and a.status_finish = 1
								and e.status = 1
								and a.status = 1
								";
					$qinvd = $this->db->query($sinvd);
					$ninvd = $qinvd->num_rows();
					if($ninvd > 0)
					{
						$rest .= '<a href="/delivery" class="list-group-item">
										<div class="media">
											<div class="media-left p-r-10">
												<em class="fa fa-diamond bg-primary"></em>
											</div>
											<div class="media-body">
												<h5 class="media-heading">Ada <b>'.$ninvd.'</b> Pengiriman Yang Harus Diproses.</h5>
												<p class="m-0">
													Segera Proses Pengiriman. 
												</p>
											</div>
										</div>
									</a>';
					}
					
				break; */
			}
		}
		
		echo $rest;
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */